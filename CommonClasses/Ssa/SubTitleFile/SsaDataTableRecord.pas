unit SsaDataTableRecord;

interface
uses Classes,SysUtils;

type TSsaDataTableRecord=class
  private const
    separatorColon:char=':';
    separatorComma:char=',';
  private
    FTitle:string;
    FValues:TStrings;
    FContent:string;
    function getFields(index: integer): string;
  public
    constructor Create;
    destructor Destroy;override;

    property Title:string read FTitle write FTitle;
    property Values:TStrings read FValues write FValues;
    property Fields[index:integer]:string read getFields;default;
    property Content:string read FContent write FContent;

    procedure ParseAsHeader(const s:string);
    procedure ParseAsContent(const s:string; const headerCount:integer);
end;

implementation
uses CommonDefs,StrUtils;
{ TSsaTableRecord }

{$REGION '����������'}
    constructor TSsaDataTableRecord.Create;
    begin
        inherited;
        FValues:=TStringList.Create;
    end;

    destructor TSsaDataTableRecord.Destroy;
    begin
        FValues.Free;
        inherited;
    end;
{$ENDREGION}

procedure TSsaDataTableRecord.ParseAsHeader(const s: string);
var
    buffer:string;
    bufferLines:TStrings;
    i:integer;
    position:integer;
begin
    FContent:=s;

    //Init buffer
    buffer:=Trim(s);
    position:=pos(separatorColon,buffer);
    buffer[position]:=separatorComma;
    buffer:=StringReplace(buffer,separatorComma,strNewLine,[rfReplaceAll]);

    //transfer into TStrings
    bufferLines:=TStringList.Create;
    bufferLines.Text:=buffer;
    for i := 0 to bufferLines.Count - 1 do begin
        bufferLines[i]:=Trim(bufferLines[i]);
    end;

    //set Title
    if bufferLines.Count>0 then begin
        FTitle:=bufferLines[0];
    end else begin
        FTitle:='';
    end;

    //set Values
    if bufferLines.Count>0 then begin
        Values.Text:=bufferLines.Text;
    end else begin
        Values.Clear;
    end;

    //free objects
    bufferLines.Free;
end;

procedure TSsaDataTableRecord.ParseAsContent(const s: string; const headerCount: integer);
var
    i:integer;
    position:integer;
    buffer:string;
begin
    buffer:=TrimLeft(s);
    if buffer='' then exit;

    //Title
    position:=pos(separatorColon,buffer);
    buffer[position]:=separatorComma;
    Title:=Trim(LeftStr(buffer,position-1));

    //Values
    Values.Clear;
    for i := 0 to headerCount - 1 do begin
        if i<>headerCount-1 then begin
            position:=pos(separatorComma,buffer);
            Values.Append(Trim(LeftStr(buffer,position-1)));
            Delete(buffer,1,position);
        end else begin
            Values.Append(buffer);
        end;
    end;
end;

function TSsaDataTableRecord.getFields(index: integer): string;
begin
    if index<Values.Count then begin
        exit(Values[index]);
    end else begin
        exit('');
    end;
end;

{function TSsaDataTableRecord.getTailContent(index: integer): string;
var
    buffer:string;
    i:integer;
begin
    if index<Values.Count then begin
        buffer:='';
        for i := index to Values.Count - 1 do begin
            buffer:=buffer + separatorComma + Values[i];
        end;
        Delete(buffer,1,1);
        exit(buffer);
    end else begin
        exit('');
    end;
end;}

end.
