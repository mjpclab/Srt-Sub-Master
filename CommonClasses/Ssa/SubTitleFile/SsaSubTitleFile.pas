unit SsaSubTitleFile;

interface
uses SubTitleFile,SubTitleData,SsaSubTitleData,SubtitleDataTable,SsaDialogueEventDataTable,TimeManager,SsaTimeManager;

type TSsaSubTitleFile=class(TSubTitleFile)
  private
    FSsaData:TSsaSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property SsaData:TSsaSubTitleData read FSsaData write FSsaData;
end;

implementation

{ TSsaSubTitleFile }

procedure TSsaSubTitleFile.CreateData;
begin
    SsaData:=TSsaSubTitleData.Create;
    Data:=SsaData;
end;

procedure TSsaSubTitleFile.DestroyData;
begin
    SsaData.Free;
end;

end.
