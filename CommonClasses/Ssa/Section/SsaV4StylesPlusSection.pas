unit SsaV4StylesPlusSection;

interface
uses SsaV4StylesSection,Classes;

type TSsaV4StylesPlusSection=class(TSsaV4StylesSection)
  private const
    theSectionName:string='V4 Styles+';
  protected
    function getSectionName: string;override;
  public
    procedure ParseContent(const content:TStrings);override;
    procedure SerializeContent;override;
end;

implementation

{ TSsaV4StylesPlusSection }

function TSsaV4StylesPlusSection.getSectionName: string;
begin
    exit(theSectionName);
end;

procedure TSsaV4StylesPlusSection.ParseContent(const content: TStrings);
begin
    //Not Implemented
end;

procedure TSsaV4StylesPlusSection.SerializeContent;
begin
    //Not Implemented
end;

end.
