unit SsaScriptInfoSection;

interface
uses SsaSection,Classes;

type TSsaScriptInfoSection=class(TSsaSection)
  private const
    theSectionName:string='Script Info';
  protected
    function getSectionName: string;override;
  public
    procedure ParseContent(const content:TStrings);override;
    procedure SerializeContent;override;
end;

implementation

{ TSsaScriptInfoSection }

function TSsaScriptInfoSection.getSectionName: string;
begin
    exit(theSectionName);
end;

procedure TSsaScriptInfoSection.ParseContent(const content: TStrings);
begin
    //Not Implemented
end;

procedure TSsaScriptInfoSection.SerializeContent;
begin
    //Not Implemented
end;

end.
