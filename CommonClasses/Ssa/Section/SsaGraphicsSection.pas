unit SsaGraphicsSection;

interface
uses SsaSection,Classes;

type TSsaGraphicsSection=class(TSsaSection)
  private const
    theSectionName:string='Graphics';
  protected
    function getSectionName: string;override;
  public
    procedure ParseContent(const content:TStrings);override;
    procedure SerializeContent;override;
end;

implementation

{ TSsaGraphicsSection }

function TSsaGraphicsSection.getSectionName: string;
begin
    exit(theSectionName);
end;

procedure TSsaGraphicsSection.ParseContent(const content: TStrings);
begin
    //Not Implemented
end;

procedure TSsaGraphicsSection.SerializeContent;
begin
    //Not Implemented
end;

end.
