unit SsaV4StylesSection;

interface
uses SsaSection,Classes;

type TSsaV4StylesSection=class(TSsaSection)
  private const
    theSectionName:string='V4 Styles';
  protected
    function getSectionName: string;override;
  public
    procedure ParseContent(const content:TStrings);override;
    procedure SerializeContent;override;
end;

implementation

{ TSsaV4StylesSection }

function TSsaV4StylesSection.getSectionName: string;
begin
    exit(theSectionName);
end;

procedure TSsaV4StylesSection.ParseContent(const content: TStrings);
begin
    //Not Implemented
end;

procedure TSsaV4StylesSection.SerializeContent;
begin
    //Not Implemented
end;

end.
