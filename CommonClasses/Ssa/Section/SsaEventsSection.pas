unit SsaEventsSection;

interface
uses SsaSection,SsaDialogueEventDataTable,SsaCommentEventDataTable,SsaPictureEventDataTable,SsaSoundEventDataTable,SsaMovieEventDataTable,SsaCommandEventDataTable
    ,Classes;

type TSsaEventsSection=class(TSsaSection)
  private
    FDialogueEvents: TSsaDialogueEventDataTable ;
    FCommentEvents: TSsaCommentEventDataTable ;
    FPictureEvents: TSsaPictureEventDataTable ;
    FSoundEvents: TSsaSoundEventDataTable ;
    FMovieEvents: TSsaMovieEventDataTable ;
    FCommandEvents: TSsaCommandEventDataTable ;
  public
    property DialogueEvents: TSsaDialogueEventDataTable read FDialogueEvents write FDialogueEvents ;
    property CommentEvents: TSsaCommentEventDataTable read FCommentEvents write FCommentEvents ;
    property PictureEvents: TSsaPictureEventDataTable read FPictureEvents write FPictureEvents ;
    property SoundEvents: TSsaSoundEventDataTable read FSoundEvents write FSoundEvents ;
    property MovieEvents: TSsaMovieEventDataTable read FMovieEvents write FMovieEvents ;
    property CommandEvents: TSsaCommandEventDataTable read FCommandEvents write FCommandEvents ;

    constructor Create;
    destructor Destroy;override;

    procedure Clear;
    procedure DisableControls;
    procedure EnableControls;
    procedure RememberPosition;
    procedure ReturnRememberedPosition;
    procedure RememberBookmark;
    procedure ReturnRememberedBookmark;
    procedure DisableIndexFieldNames;
    procedure EnableIndexFieldNames;
  private const
    theSectionName:string='Events';
  protected
    function getSectionName: string;override;
  public
    procedure ParseContent(const content:TStrings);override;
    procedure SerializeContent;override;
end;

implementation
uses SsaDataTableRecord,SsaEventColumnIndexList,SsaEventColumnNameList,SsaEventDataTableNameList
    ,SysUtils;
{ TEventsSection }

{$REGION '构造与析构'}
    constructor TSsaEventsSection.Create;
    begin
        inherited;
        DialogueEvents:= TSsaDialogueEventDataTable.Create(nil);
        CommentEvents:= TSsaCommentEventDataTable.Create(nil);
        PictureEvents:= TSsaPictureEventDataTable.Create(nil);
        SoundEvents:= TSsaSoundEventDataTable.Create(nil);
        MovieEvents:= TSsaMovieEventDataTable.Create(nil);
        CommandEvents:= TSsaCommandEventDataTable.Create(nil);
    end;

    destructor TSsaEventsSection.Destroy;
    begin
        DialogueEvents.Free;
        CommentEvents.Free;
        PictureEvents.Free;
        SoundEvents.Free;
        MovieEvents.Free;
        CommandEvents.Free;
        inherited;
    end;
{$ENDREGION}

{$REGION '方法覆盖'}
    function TSsaEventsSection.getSectionName: string;
    begin
        exit(theSectionName);
    end;

    procedure TSsaEventsSection.ParseContent(const content: TStrings);
    var
        s:string;
        rowContent:TSsaDataTableRecord;
        tableNames:TSsaEventDataTableNameList;
    begin
        tableNames:=TSsaEventDataTableNameList.Create;

        {$REGION '初始化ColumnIndex'}
        DialogueEvents.ParseColumnIndex(content);
        CommentEvents.ParseColumnIndex(content);
        PictureEvents.ParseColumnIndex(content);
        SoundEvents.ParseColumnIndex(content);
        MovieEvents.ParseColumnIndex(content);
        CommandEvents.ParseColumnIndex(content);
        {$ENDREGION}

        {$REGION '将数据传递给各表'}
        DialogueEvents.Clear;
        CommentEvents.Clear;
        PictureEvents.Clear;
        SoundEvents.Clear;
        MovieEvents.Clear;
        CommandEvents.Clear;

        rowContent:=TSsaDataTableRecord.Create;
        for s in content do begin
            rowContent.ParseAsContent(s,DialogueEvents.ContentColumnCount);

            if rowContent.Title=tableNames.Dialogue then
                DialogueEvents.ParseRecord(rowContent)
            else if rowContent.Title=tableNames.Comment then
                CommentEvents.ParseRecord(rowContent)
            else if rowContent.Title=tableNames.Picture then
                PictureEvents.ParseRecord(rowContent)
            else if rowContent.Title=tableNames.Sound then
                SoundEvents.ParseRecord(rowContent)
            else if rowContent.Title=tableNames.Movie then
                MovieEvents.ParseRecord(rowContent)
            else if rowContent.Title=tableNames.Command then
                CommandEvents.ParseRecord(rowContent)
            ;
        end;

        rowContent.Free;
        {$ENDREGION}

        tableNames.Free;
    end;

    procedure TSsaEventsSection.SerializeContent;
    begin
        TextContent.Clear;
        TextContent.Add('[Events]');
        TextContent.Add('Format: Marked, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text');

        DialogueEvents.SerializeContent;
        CommentEvents.SerializeContent;
        PictureEvents.SerializeContent;
        SoundEvents.SerializeContent;
        MovieEvents.SerializeContent;
        CommandEvents.SerializeContent;

        TextContent.AddStrings(DialogueEvents.TextContent);
        TextContent.AddStrings(CommentEvents.TextContent);
        TextContent.AddStrings(PictureEvents.TextContent);
        TextContent.AddStrings(SoundEvents.TextContent);
        TextContent.AddStrings(MovieEvents.TextContent);
        TextContent.AddStrings(CommandEvents.TextContent);

    end;
{$ENDREGION}

{$REGION 'Facade'}
    procedure TSsaEventsSection.Clear;
    begin
        DialogueEvents.Clear;
        CommentEvents.Clear;
        PictureEvents.Clear;
        SoundEvents.Clear;
        MovieEvents.Clear;
        CommandEvents.Clear;

        TextContent.Clear;
    end;

    procedure TSsaEventsSection.DisableControls;
    begin
        DialogueEvents.DisableControls;
        CommentEvents.DisableControls;
        PictureEvents.DisableControls;
        SoundEvents.DisableControls;
        MovieEvents.DisableControls;
        CommandEvents.DisableControls;
    end;

    procedure TSsaEventsSection.EnableControls;
    begin
        DialogueEvents.EnableControls;
        CommentEvents.EnableControls;
        PictureEvents.EnableControls;
        SoundEvents.EnableControls;
        MovieEvents.EnableControls;
        CommandEvents.EnableControls;
    end;

    procedure TSsaEventsSection.RememberPosition;
    begin
        DialogueEvents.RememberPosition;
        CommentEvents.RememberPosition;
        PictureEvents.RememberPosition;
        SoundEvents.RememberPosition;
        MovieEvents.RememberPosition;
        CommandEvents.RememberPosition;
    end;

    procedure TSsaEventsSection.ReturnRememberedPosition;
    begin
        DialogueEvents.ReturnRememberedPosition;
        CommentEvents.ReturnRememberedPosition;
        PictureEvents.ReturnRememberedPosition;
        SoundEvents.ReturnRememberedPosition;
        MovieEvents.ReturnRememberedPosition;
        CommandEvents.ReturnRememberedPosition;
    end;

    procedure TSsaEventsSection.RememberBookmark;
    begin
        DialogueEvents.RememberBookmark;
        CommentEvents.RememberBookmark;
        PictureEvents.RememberBookmark;
        SoundEvents.RememberBookmark;
        MovieEvents.RememberBookmark;
        CommandEvents.RememberBookmark;
    end;

    procedure TSsaEventsSection.ReturnRememberedBookmark;
    begin
        DialogueEvents.ReturnRememberedBookmark;
        CommentEvents.ReturnRememberedBookmark;
        PictureEvents.ReturnRememberedBookmark;
        SoundEvents.ReturnRememberedBookmark;
        MovieEvents.ReturnRememberedBookmark;
        CommandEvents.ReturnRememberedBookmark;
    end;

    procedure TSsaEventsSection.DisableIndexFieldNames;
    begin
        DialogueEvents.DisableIndexFieldNames;
        CommentEvents.DisableIndexFieldNames;
        PictureEvents.DisableIndexFieldNames;
        SoundEvents.DisableIndexFieldNames;
        MovieEvents.DisableIndexFieldNames;
        CommandEvents.DisableIndexFieldNames;
    end;

    procedure TSsaEventsSection.EnableIndexFieldNames;
    begin
        DialogueEvents.EnableIndexFieldNames;
        CommentEvents.EnableIndexFieldNames;
        PictureEvents.EnableIndexFieldNames;
        SoundEvents.EnableIndexFieldNames;
        MovieEvents.EnableIndexFieldNames;
        CommandEvents.EnableIndexFieldNames;
    end;
{$ENDREGION}


end.
