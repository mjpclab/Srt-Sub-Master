unit SsaFontsSection;

interface
uses SsaSection,Classes;

type TSsaFontsSection=class(TSsaSection)
  private const
    theSectionName:string='Fonts';
  protected
    function getSectionName: string;override;
  public
    procedure ParseContent(const content:TStrings);override;
    procedure SerializeContent;override;
end;

implementation

{ TSsaFontsSection }

function TSsaFontsSection.getSectionName: string;
begin
    exit(theSectionName);
end;

procedure TSsaFontsSection.ParseContent(const content: TStrings);
begin
    //Not Implemented
end;

procedure TSsaFontsSection.SerializeContent;
begin
    //Not Implemented
end;

end.
