unit SsaSectionCollection;

interface
uses SsaScriptInfoSection,SsaV4StylesSection,SsaV4StylesPlusSection,SsaEventsSection,SsaFontsSection,SsaGraphicsSection
    ,Classes;

type TSsaSectionCollection=class
  private
    FScriptInfoSection:TSsaScriptInfoSection;
    FV4StylesSection:TSsaV4StylesSection;
    FV4StylesPlusSection:TSsaV4StylesPlusSection;
    FEventsSection:TSsaEventsSection;
    FFontsSection:TSsaFontsSection;
    FGraphicsSection:TSsaGraphicsSection;

    FTextContent:TStrings;
  public
    property TextContent:TStrings read FTextContent write FTextContent;

    property ScriptInfoSection :TSsaScriptInfoSection read FScriptInfoSection write FScriptInfoSection;
    property V4StylesSection :TSsaV4StylesSection read FV4StylesSection write FV4StylesSection;
    property V4StylesPlusSection :TSsaV4StylesPlusSection read FV4StylesPlusSection write FV4StylesPlusSection;
    property EventsSection :TSsaEventsSection read FEventsSection write FEventsSection;
    property FontsSection :TSsaFontsSection read FFontsSection write FFontsSection;
    property GraphicsSection :TSsaGraphicsSection read FGraphicsSection write FGraphicsSection;

    constructor Create;
    destructor Destroy;override;
    procedure ParseContent(const content:TStrings);
    procedure SerializeContent;

    procedure Clear;
    procedure DisableControls;
    procedure EnableControls;
    procedure RememberPosition;
    procedure ReturnRememberedPosition;
    procedure RememberBookmark;
    procedure ReturnRememberedBookmark;
    procedure DisableIndexFieldNames;
    procedure EnableIndexFieldNames;
end;

implementation
uses IniFiles;


constructor TSsaSectionCollection.Create;
begin
    inherited;
    ScriptInfoSection := TSsaScriptInfoSection.Create;
    V4StylesSection := TSsaV4StylesSection.Create;
    V4StylesPlusSection := TSsaV4StylesPlusSection.Create;
    EventsSection := TSsaEventsSection.Create;
    FontsSection := TSsaFontsSection.Create;
    GraphicsSection := TSsaGraphicsSection.Create;

    TextContent:=TStringList.Create;
end;

destructor TSsaSectionCollection.Destroy;
begin
    ScriptInfoSection.Free;
    V4StylesSection.Free;
    V4StylesPlusSection.Free;
    EventsSection.Free;
    FontsSection.Free;
    GraphicsSection.Free;

    TextContent.Free;
    inherited;
end;

procedure TSsaSectionCollection.ParseContent(const content: TStrings);
var
    ini:TMemIniFile;
    buffer:TStrings;
begin
    TextContent.Assign(content);

    ini:=TMemIniFile.Create('');
    ini.SetStrings(TextContent);

    buffer:=TStringList.Create;

    ini.ReadSectionValues(ScriptInfoSection.SectionName,buffer);
    ScriptInfoSection.ParseContent(buffer);

    ini.ReadSectionValues(V4StylesSection.SectionName,buffer);
    V4StylesSection.ParseContent(buffer);

    ini.ReadSectionValues(V4StylesPlusSection.SectionName,buffer);
    V4StylesPlusSection.ParseContent(buffer);

    ini.ReadSectionValues(EventsSection.SectionName,buffer);
    EventsSection.ParseContent(buffer);

    ini.ReadSectionValues(FontsSection.SectionName,buffer);
    FontsSection.ParseContent(buffer);

    ini.ReadSectionValues(GraphicsSection.SectionName,buffer);
    GraphicsSection.ParseContent(buffer);

    ini.Free;
    buffer.Free;
end;

procedure TSsaSectionCollection.SerializeContent;
begin
    TextContent.Clear;

    ScriptInfoSection.SerializeContent;
    V4StylesSection.SerializeContent;
    V4StylesPlusSection.SerializeContent;
    EventsSection.SerializeContent;
    FontsSection.SerializeContent;
    GraphicsSection.SerializeContent;

    TextContent.AddStrings(ScriptInfoSection.TextContent);
    TextContent.AddStrings(V4StylesSection.TextContent);
    TextContent.AddStrings(V4StylesPlusSection.TextContent);
    TextContent.AddStrings(EventsSection.TextContent);
    TextContent.AddStrings(FontsSection.TextContent);
    TextContent.AddStrings(GraphicsSection.TextContent);
end;

procedure TSsaSectionCollection.Clear;
begin
    EventsSection.Clear;
    TextContent.Clear;
end;

procedure TSsaSectionCollection.DisableControls;
begin
    EventsSection.DisableControls;
end;

procedure TSsaSectionCollection.EnableControls;
begin
    EventsSection.EnableControls;
end;

procedure TSsaSectionCollection.RememberPosition;
begin
    EventsSection.RememberPosition;
end;

procedure TSsaSectionCollection.ReturnRememberedPosition;
begin
    EventsSection.ReturnRememberedPosition;
end;

procedure TSsaSectionCollection.RememberBookmark;
begin
    EventsSection.RememberBookmark;
end;

procedure TSsaSectionCollection.ReturnRememberedBookmark;
begin
    EventsSection.ReturnRememberedBookmark;
end;

procedure TSsaSectionCollection.DisableIndexFieldNames;
begin
    EventsSection.DisableIndexFieldNames;
end;

procedure TSsaSectionCollection.EnableIndexFieldNames;
begin
    EventsSection.EnableIndexFieldNames;
end;

end.
