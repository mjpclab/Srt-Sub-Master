unit SsaSection;

interface
uses Classes;

type TSsaSection=class
  protected
    FTextContent:TStrings;
    function getSectionName: string;virtual;abstract;
  public
    constructor Create;
    destructor Destroy;override;

    property TextContent:TStrings read FTextContent write FTextContent;

    procedure ParseContent(const content:TStrings);virtual;abstract;
    procedure SerializeContent;virtual;abstract;
    property SectionName:string read getSectionName;
end;

implementation

{ TSsaSection }

constructor TSsaSection.Create;
begin
    inherited;
    FTextContent:=TStringList.Create;
end;

destructor TSsaSection.Destroy;
begin
    FTextContent.Free;
    inherited;
end;

end.
