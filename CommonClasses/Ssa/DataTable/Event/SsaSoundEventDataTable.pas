unit SsaSoundEventDataTable;

interface
uses SsaEventDataTable,SsaEventDataTableNameList;

type TSsaSoundEventDataTable=class(TSsaEventDataTable)
  protected
    function getEventName:string;override;
end;

implementation

{ TSsaSoundEventDataTable }

function TSsaSoundEventDataTable.getEventName: string;
begin
    exit(TSsaEventDataTableNameList.Sound);
end;

end.
