unit SsaEventColumnNameList;

interface

type TSsaEventColumnNameList=class
  public const
    Format:string='Format';

    Marked:string='Marked';
    Layer:string='Layer';
    TimeStart:string='Start';
    TimeEnd:string='End';
    Style:string='Style';
    Name:string='Name';
    MarginL:string='MarginL';
    MarginR:string='MarginR';
    MarginV:string='MarginV';
    Effect:string='Effect';
    Text:string='Text';
end;

implementation

end.
