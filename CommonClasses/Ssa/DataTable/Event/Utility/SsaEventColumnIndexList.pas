unit SsaEventColumnIndexList;

interface
uses Classes;

type TSsaEventColumnIndexList=class
  private
    FFormat:integer;

    FMarked:integer;
    FLayer:integer;
    FTimeStart:integer;
    FTimeEnd:integer;
    FStyle:integer;
    FName:integer;
    FMarginL:integer;
    FMarginR:integer;
    FMarginV:integer;
    FEffect:integer;
    FText:integer;
    function getColumns(columnName: string): integer;
    procedure setColumns(columnName: string; const Value: integer);
    function getAvailableColumnCount: integer;
  public
    property Format:integer read FFormat write FFormat ;

    property Marked:integer read FMarked write FMarked ;
    property Layer:integer read FLayer write FLayer ;
    property TimeStart:integer read FTimeStart write FTimeStart ;
    property TimeEnd:integer read FTimeEnd write FTimeEnd ;
    property Style:integer read FStyle write FStyle ;
    property Name:integer read FName write FName ;
    property MarginL:integer read FMarginL write FMarginL ;
    property MarginR:integer read FMarginR write FMarginR ;
    property MarginV:integer read FMarginV write FMarginV ;
    property Effect:integer read FEffect write FEffect ;
    property Text:integer read FText write FText ;
    property Columns[columnName:string]:integer read getColumns write setColumns;default;
    property AvailableColumnCount:integer read getAvailableColumnCount;

    procedure ResetIndex;
    procedure Assign(source:TSsaEventColumnIndexList);
  end;

implementation
uses SsaEventColumnNameList,SysUtils;
{ TSsaEventColumnIndexList }

procedure TSsaEventColumnIndexList.ResetIndex;
begin
    FFormat:=0;

    FMarked:=-1;
    FLayer:=-1;
    FTimeStart:=-1;
    FTimeEnd:=-1;
    FStyle:=-1;
    FName:=-1;
    FMarginL:=-1;
    FMarginR:=-1;
    FMarginV:=-1;
    FEffect:=-1;
    FText:=-1;
end;

function TSsaEventColumnIndexList.getAvailableColumnCount: integer;
var
    availCount:integer;
begin
    availCount:=0;

    if FFormat >=0 then Inc(availCount);
    if FMarked >=0 then Inc(availCount);
    if FLayer >=0 then Inc(availCount);
    if FTimeStart >=0 then Inc(availCount);
    if FTimeEnd >=0 then Inc(availCount);
    if FStyle >=0 then Inc(availCount);
    if FName >=0 then Inc(availCount);
    if FMarginL >=0 then Inc(availCount);
    if FMarginR >=0 then Inc(availCount);
    if FMarginV >=0 then Inc(availCount);
    if FEffect >=0 then Inc(availCount);
    if FText >=0 then Inc(availCount);

    exit(availCount);
end;

procedure TSsaEventColumnIndexList.Assign(source: TSsaEventColumnIndexList);
begin
    self.Marked:=source.Marked;
    self.Layer:=source.Layer;
    self.TimeStart:=source.TimeStart;
    self.TimeEnd:=source.TimeEnd;
    self.Style:=source.Style;
    self.Name:=source.Name;
    self.MarginL:=source.MarginL;
    self.MarginR:=source.MarginR;
    self.MarginV:=source.MarginV;
    self.Effect:=source.Effect;
    self.Text:=source.Text;
end;

function TSsaEventColumnIndexList.getColumns(columnName: string): integer;
begin
    if CompareText(columnName,TSsaEventColumnNameList.Format)=0 then
        exit(self.Format)
    else if CompareText(columnName,TSsaEventColumnNameList.Layer)=0 then
        exit(self.Layer)
    else if CompareText(columnName,TSsaEventColumnNameList.TimeStart)=0 then
        exit(self.TimeStart)
    else if CompareText(columnName,TSsaEventColumnNameList.TimeEnd)=0 then
        exit(self.TimeEnd)
    else if CompareText(columnName,TSsaEventColumnNameList.Style)=0 then
        exit(self.Style)
    else if CompareText(columnName,TSsaEventColumnNameList.Name)=0 then
        exit(self.Name)
    else if CompareText(columnName,TSsaEventColumnNameList.MarginL)=0 then
        exit(self.MarginL)
    else if CompareText(columnName,TSsaEventColumnNameList.MarginR)=0 then
        exit(self.MarginR)
    else if CompareText(columnName,TSsaEventColumnNameList.MarginV)=0 then
        exit(self.MarginV)
    else if CompareText(columnName,TSsaEventColumnNameList.Effect)=0 then
        exit(self.Effect)
    else if CompareText(columnName,TSsaEventColumnNameList.Text)=0 then
        exit(self.Text)
    else
        exit(-1);

end;

procedure TSsaEventColumnIndexList.setColumns(columnName: string; const Value: integer);
begin
    if CompareText(columnName,TSsaEventColumnNameList.Format)=0 then
        self.Format:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.Layer)=0 then
        self.Layer:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.TimeStart)=0 then
        self.TimeStart:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.TimeEnd)=0 then
        self.TimeEnd:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.Style)=0 then
        self.Style:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.Name)=0 then
        self.Name:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.MarginL)=0 then
        self.MarginL:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.MarginR)=0 then
        self.MarginR:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.MarginV)=0 then
        self.MarginV:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.Effect)=0 then
        self.Effect:=Value
    else if CompareText(columnName,TSsaEventColumnNameList.Text)=0 then
        self.Text:=Value
    ;
end;

end.
