unit SsaMovieEventDataTable;

interface
uses SsaEventDataTable,SsaEventDataTableNameList;

type TSsaMovieEventDataTable=class(TSsaEventDataTable)
  protected
    function getEventName:string;override;
end;

implementation

{ TSsaMovieEventDataTable }

function TSsaMovieEventDataTable.getEventName: string;
begin
    exit(TSsaEventDataTableNameList.Movie);
end;

end.
