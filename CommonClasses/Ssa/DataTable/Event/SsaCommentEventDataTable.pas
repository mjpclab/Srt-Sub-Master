unit SsaCommentEventDataTable;

interface
uses SsaEventDataTable,SsaEventDataTableNameList;

type TSsaCommentEventDataTable=class(TSsaEventDataTable)
  protected
    function getEventName:string;override;
end;

implementation

{ TSsaCommentEventDataTable }

function TSsaCommentEventDataTable.getEventName: string;
begin
    exit(TSsaEventDataTableNameList.Comment);
end;

end.
