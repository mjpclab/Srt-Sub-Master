unit SsaDialogueEventDataTable;

interface
uses SsaEventDataTable,SsaEventDataTableNameList;

type TSsaDialogueEventDataTable=class(TSsaEventDataTable)
  protected
    function getEventName:string;override;
end;

implementation

{ TSsaDialogueEventDataTable }

function TSsaDialogueEventDataTable.getEventName: string;
begin
    exit(TSsaEventDataTableNameList.Dialogue);
end;

end.
