unit SsaEventDataTable;

interface
uses SubTitleDataTable,SsaEventColumnIndexList,SsaDataTableRecord,Classes;

type TSsaEventDataTable=class(TSubTitleDataTable)
  private const
    MarginDigits:Integer=4;
  private
    FColumnIndex:TSsaEventColumnIndexList;
    FContentColumnCount:integer;

    procedure InitializeFieldDefs;

    function getCurrentFormat: string;
    procedure setCurrentFormat(const Value: string);
    function getCurrentMarked: integer;
    procedure setCurrentMarked(const Value: integer);
    function getCurrentLayer: integer;
    procedure setCurrentLayer(const Value: integer);
    function getCurrentTimeStart: string;
    procedure setCurrentTimeStart(const Value: string);
    function getCurrentTimeEnd: string;
    procedure setCurrentTimeEnd(const Value: string);
    function getCurrentStyle: string;
    procedure setCurrentStyle(const Value: string);
    function getCurrentName: string;
    procedure setCurrentName(const Value: string);
    function getCurrentMarginL: integer;
    procedure setCurrentMarginL(const Value: integer);
    function getCurrentMarginR: integer;
    procedure setCurrentMarginR(const Value: integer);
    function getCurrentMarginV: integer;
    procedure setCurrentMarginV(const Value: integer);
    function getCurrentEffect: string;
    procedure setCurrentEffect(const Value: string);
    function getCurrentText: string;
    procedure setCurrentText(const Value: string);
    function getCurrentMarkedString: string;
    function getCurrentMarginLString: string;
    function getCurrentMarginRString: string;
    function getCurrentMarginVString: string;
  public const
    FormatField:string='Format';
    MarkedField:string='Marked';
    LayerField:string='Layer';
    TimeStartField:string='TimeStart';
    TimeEndField:string='TimeEnd';
    StyleField:string='Style';
    NameField:string='Name';
    MarginLField:string='MarginL';
    MarginRField:string='MarginR';
    MarginVField:string='MarginV';
    EffectField:string='Effect';
    TextField:string='Text';

    FormatFieldIndex:integer=0;
    MarkedFieldIndex:integer=1;
    LayerFieldIndex:integer=2;
    TimeStartFieldIndex:integer=3;
    TimeEndFieldIndex:integer=4;
    StyleFieldIndex:integer=5;
    NameFieldIndex:integer=6;
    MarginLFieldIndex:integer=7;
    MarginRFieldIndex:integer=8;
    MarginVFieldIndex:integer=9;
    EffectFieldIndex:integer=10;
    TextFieldIndex:integer=11;
  protected
    function getEventName:string;virtual;abstract;
    procedure Initialize;override;
    procedure Finalize;override;
  public
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;

    property CurrentFormat:string read getCurrentFormat write setCurrentFormat;
    property CurrentMarked:integer read getCurrentMarked write setCurrentMarked;
    property CurrentLayer:integer read getCurrentLayer write setCurrentLayer;
    property CurrentTimeStart:string read getCurrentTimeStart write setCurrentTimeStart;
    property CurrentTimeEnd:string read getCurrentTimeEnd write setCurrentTimeEnd;
    property CurrentStyle:string read getCurrentStyle write setCurrentStyle;
    property CurrentName:string read getCurrentName write setCurrentName;
    property CurrentMarginL:integer read getCurrentMarginL write setCurrentMarginL;
    property CurrentMarginR:integer read getCurrentMarginR write setCurrentMarginR;
    property CurrentMarginV:integer read getCurrentMarginV write setCurrentMarginV;
    property CurrentEffect:string read getCurrentEffect write setCurrentEffect;
    property CurrentText:string read getCurrentText write setCurrentText;

    property CurrentMarkedString:string read getCurrentMarkedString;
    property CurrentMarginLString:string read getCurrentMarginLString;
    property CurrentMarginRString:string read getCurrentMarginRString;
    property CurrentMarginVString:string read getCurrentMarginVString;

    property ColumnIndex:TSsaEventColumnIndexList read FColumnIndex write FColumnIndex;
    property ContentColumnCount:integer read FContentColumnCount write FContentColumnCount;
    property EventName:string read getEventName;

    procedure ParseContent(const content:TStrings);override;
    procedure ParseColumnIndex(const content:TStrings);virtual;
    procedure ParseRecord(const row:TSsaDataTableRecord);virtual;
    procedure SerializeContent;override;
end;

implementation
uses SsaEventColumnNameList,CommonDefs,DB,SysUtils;
{ TSsaEventDataTable }

{$REGION '构造与析构'}
    constructor TSsaEventDataTable.Create(AOwner:TComponent);
    begin
        inherited;
        FColumnIndex:=TSsaEventColumnIndexList.Create;
    end;

    destructor TSsaEventDataTable.Destroy;
    begin
        FColumnIndex.Free;
        inherited;
    end;

{$ENDREGION}

{$REGION '属性'}
    function TSsaEventDataTable.getCurrentFormat: string;
    begin
        exit(Fields[FormatFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentFormat(const Value: string);
    begin
        Fields[FormatFieldIndex].AsString:=value;
    end;

    function TSsaEventDataTable.getCurrentMarked: integer;
    begin
        exit(Fields[MarkedFieldIndex].AsInteger);
    end;

    function TSsaEventDataTable.getCurrentMarkedString: string;
    begin
        exit(Fields[MarkedFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentMarked(const Value: integer);
    begin
        Fields[MarkedFieldIndex].AsInteger:=value;
    end;

    function TSsaEventDataTable.getCurrentLayer: integer;
    begin
        exit(Fields[LayerFieldIndex].AsInteger);
    end;

    procedure TSsaEventDataTable.setCurrentLayer(const Value: integer);
    begin
        Fields[MarkedFieldIndex].AsInteger:=value;
    end;

    function TSsaEventDataTable.getCurrentTimeStart: string;
    begin
        exit(Fields[TimeStartFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentTimeStart(const Value: string);
    begin
        Fields[TimeStartFieldIndex].AsString:=value;
    end;

    function TSsaEventDataTable.getCurrentTimeEnd: string;
    begin
        exit(Fields[TimeEndFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentTimeEnd(const Value: string);
    begin
        Fields[TimeEndFieldIndex].AsString:=value;
    end;

    function TSsaEventDataTable.getCurrentStyle: string;
    begin
        exit(Fields[StyleFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentStyle(const Value: string);
    begin
        Fields[StyleFieldIndex].AsString:=value;
    end;

    function TSsaEventDataTable.getCurrentName: string;
    begin
        exit(Fields[NameFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentName(const Value: string);
    begin
        Fields[NameFieldIndex].AsString:=value;
    end;

    function TSsaEventDataTable.getCurrentMarginL: integer;
    begin
        exit(Fields[MarginLFieldIndex].AsInteger);
    end;

    function TSsaEventDataTable.getCurrentMarginLString: string;
    var
        value:string;
    begin
        value:=Fields[MarginLFieldIndex].asString;
        value:=StringOfChar('0',4-Length(value)) + value;
        exit(value);
    end;

    procedure TSsaEventDataTable.setCurrentMarginL(const Value: integer);
    begin
        Fields[MarginLFieldIndex].AsInteger:=value;
    end;

    function TSsaEventDataTable.getCurrentMarginR: integer;
    begin
         exit(Fields[MarginRFieldIndex].AsInteger);
    end;

    function TSsaEventDataTable.getCurrentMarginRString: string;
    var
        value:string;
    begin
        value:=Fields[MarginRFieldIndex].asString;
        value:=StringOfChar('0',4-Length(value)) + value;
        exit(value);
    end;

    procedure TSsaEventDataTable.setCurrentMarginR(const Value: integer);
    begin
        Fields[MarginRFieldIndex].AsInteger:=value;
    end;

    function TSsaEventDataTable.getCurrentMarginV: integer;
    begin
         exit(Fields[MarginVFieldIndex].AsInteger);
    end;

    function TSsaEventDataTable.getCurrentMarginVString: string;
    var
        value:string;
    begin
        value:=Fields[MarginVFieldIndex].asString;
        value:=StringOfChar('0',4-Length(value)) + value;
        exit(value);
    end;

    procedure TSsaEventDataTable.setCurrentMarginV(const Value: integer);
    begin
        Fields[MarginVFieldIndex].AsInteger:=value;
    end;

    function TSsaEventDataTable.getCurrentEffect: string;
    begin
        exit(Fields[EffectFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentEffect(const Value: string);
    begin
        Fields[EffectFieldIndex].AsString:=value;
    end;

    function TSsaEventDataTable.getCurrentText: string;
    begin
        exit(Fields[TextFieldIndex].AsString);
    end;

    procedure TSsaEventDataTable.setCurrentText(const Value: string);
    begin
        Fields[TextFieldIndex].AsString:=value;
    end;
{$ENDREGION}

{$REGION '初始化/终结'}
    procedure TSsaEventDataTable.Initialize;
    begin
        InitializeFieldDefs;

        IndexFieldNamesForSort:='TimeStart;TimeEnd';
        EnableIndexFieldNames;

        self.CreateDataTable;
        self.Open;
        self.LogChanges:=false;
    end;

    procedure TSsaEventDataTable.InitializeFieldDefs;
    begin
        with self.FieldDefs.AddFieldDef do begin
            Name:=FormatField;
            DisplayName:='类别';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=MarkedField;
            DisplayName:='标记';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=LayerField;
            DisplayName:='层';
            DataType:=ftInteger;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=TimeStartField;
            DisplayName:='起始时间';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=TimeEndField;
            DisplayName:='结束时间';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=StyleField;
            DisplayName:='样式';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=NameField;
            DisplayName:='人名';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=MarginLField;
            DisplayName:='左偏移';
            DataType:=ftInteger;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=MarginRField;
            DisplayName:='右偏移';
            DataType:=ftInteger;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=MarginVField;
            DisplayName:='垂直偏移';
            DataType:=ftInteger;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=EffectField;
            DisplayName:='效果';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=TextField;
            DisplayName:='内容';
            DataType:=ftMemo;
        end;
    end;

    procedure TSsaEventDataTable.Finalize;
    begin
        self.Close;
    end;

{$ENDREGION}

procedure TSsaEventDataTable.ParseContent(const content: TStrings);
var
    s:string;
    row:TSsaDataTableRecord;
begin
    inherited;

    row:=TSsaDataTableRecord.Create;
    TextContent.Assign(content);
    self.Clear;

    ParseColumnIndex(content);

    for s in TextContent do begin
        row.ParseAsContent(s,ColumnIndex.AvailableColumnCount);
        ParseRecord(row);
    end;
end;

procedure TSsaEventDataTable.ParseColumnIndex(const content: TStrings);
var
    s:string;
    row:TSsaDataTableRecord;
    i:integer;
begin
    FContentColumnCount:=0;
    row:=TSsaDataTableRecord.Create;
    ColumnIndex.ResetIndex;

    for s in content do begin
        row.ParseAsHeader(s);
        if CompareText(row.Title,TSsaEventColumnNameList.Format)=0 then begin     //找到了Format行
            for i := 0 to row.Values.Count-1 do begin
                ColumnIndex[row.Values[i]]:=i;
                Inc(FContentColumnCount);
            end;

            break;
        end;
    end;

    row.Free;
end;

procedure TSsaEventDataTable.ParseRecord(const row: TSsaDataTableRecord);
begin
    if row.Title=self.EventName then begin
        Append;

        if ColumnIndex.Format>=0 then CurrentFormat:=row[ColumnIndex.Format];
        if ColumnIndex.Marked>=0 then CurrentMarked:=StrToIntDef(row[ColumnIndex.Marked],0);
        if ColumnIndex.Layer>=0 then CurrentLayer:=StrToIntDef(row[ColumnIndex.Layer],0);
        if ColumnIndex.TimeStart>=0 then CurrentTimeStart:=row[ColumnIndex.TimeStart];
        if ColumnIndex.TimeEnd>=0 then CurrentTimeEnd:=row[ColumnIndex.TimeEnd];
        if ColumnIndex.Style>=0 then CurrentStyle:=row[ColumnIndex.Style];
        if ColumnIndex.Name>=0 then CurrentName:=row[ColumnIndex.Name];
        if ColumnIndex.MarginL>=0 then CurrentMarginL:=StrToIntDef(row[ColumnIndex.MarginL],0);
        if ColumnIndex.MarginR>=0 then CurrentMarginR:=StrToIntDef(row[ColumnIndex.MarginR],0);
        if ColumnIndex.MarginV>=0 then CurrentMarginV:=StrToIntDef(row[ColumnIndex.MarginV],0);
        if ColumnIndex.Effect>=0 then CurrentEffect:=row[ColumnIndex.Effect];
        if ColumnIndex.Text>=0 then begin
            CurrentText:=StringReplace(
                row[ColumnIndex.Text],'\N',strNewLine,[rfReplaceAll]
            );
        end;

        Post;
    end;
end;

procedure TSsaEventDataTable.SerializeContent;
begin
    TextContent.Clear;

    DisableControls;
    RememberPosition;
    First;
    while not Eof do begin
        TextContent.Append(Format(
            '%s: Marked=%d,%s,%s,%s,%s,%s,%s,%s,%s,%s',
            [
                EventName,
                CurrentMarked,
                CurrentTimeStart,
                CurrentTimeEnd,
                CurrentStyle,
                CurrentName,
                CurrentMarginLString,
                CurrentMarginRString,
                CurrentMarginVString,
                CurrentEffect,
                CurrentText
            ]
        ));

        Next;
    end;
    ReturnRememberedPosition;
    EnableControls;
end;

end.
