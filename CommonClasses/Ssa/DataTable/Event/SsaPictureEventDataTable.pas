unit SsaPictureEventDataTable;

interface
uses SsaEventDataTable,SsaEventDataTableNameList;

type TSsaPictureEventDataTable=class(TSsaEventDataTable)
  protected
    function getEventName:string;override;
end;

implementation

{ TSsaPictureEventDataTable }

function TSsaPictureEventDataTable.getEventName: string;
begin
    exit(TSsaEventDataTableNameList.Picture);
end;

end.
