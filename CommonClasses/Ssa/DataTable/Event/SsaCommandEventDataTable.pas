unit SsaCommandEventDataTable;

interface
uses SsaEventDataTable,SsaEventDataTableNameList;

type TSsaCommandEventDataTable=class(TSsaEventDataTable)
  protected
    function getEventName:string;override;
end;

implementation

{ TSsaCommandEventDataTable }

function TSsaCommandEventDataTable.getEventName: string;
begin
    exit(TSsaEventDataTableNameList.Command);
end;

end.
