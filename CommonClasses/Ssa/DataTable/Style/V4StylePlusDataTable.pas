unit V4StylePlusDataTable;

interface
uses SubTitleDataTable,Classes;

type TV4StylePlusDataTable=class(TSubTitleDataTable)
  private
    procedure InitializeFieldDefs;
  protected
    procedure Initialize;override;
    procedure Finalize;override;
  public
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;
end;

implementation

{ TV4StylePlusDataTable }

constructor TV4StylePlusDataTable.Create(AOwner: TComponent);
begin
    inherited;

end;

destructor TV4StylePlusDataTable.Destroy;
begin

    inherited;
end;

procedure TV4StylePlusDataTable.Initialize;
begin
    InitializeFieldDefs;

    self.CreateDataTable;
    self.Open;
    self.LogChanges:=false;
end;

procedure TV4StylePlusDataTable.InitializeFieldDefs;
begin
    //
end;

procedure TV4StylePlusDataTable.Finalize;
begin
    self.Close;
end;

end.
