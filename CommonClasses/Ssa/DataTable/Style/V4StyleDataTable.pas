unit V4StyleDataTable;

interface
uses SubTitleDataTable,Classes;

type TV4StyleDataTable=class(TSubTitleDataTable)
  private
    procedure InitializeFieldDefs;
  protected
    procedure Initialize;override;
    procedure Finalize;override;
  public
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;
end;

implementation

{ TV4StyleDataTable }

constructor TV4StyleDataTable.Create(AOwner: TComponent);
begin
    inherited;

end;

destructor TV4StyleDataTable.Destroy;
begin

    inherited;
end;

procedure TV4StyleDataTable.Initialize;
begin
    InitializeFieldDefs;

    self.CreateDataTable;
    self.Open;
    self.LogChanges:=false;
end;

procedure TV4StyleDataTable.InitializeFieldDefs;
begin
    //
end;

procedure TV4StyleDataTable.Finalize;
begin
    self.Close;
end;

end.
