unit SsaEventDataTable;

interface
uses DataTable,Classes;

type TSsaEventDataTable=class(TDataTable)
  protected
    FContentLines:TStrings;
    property ContentLines:TStrings read FContentLines write FContentLines;
  public
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;

    procedure ParseContent(const content:TStrings);virtual;abstract;
end;

implementation

{ TSsaEventDataTable }

constructor TSsaEventDataTable.Create(AOwner: TComponent);
begin
    inherited;
    FContentLines:=TStringList.Create;
end;

destructor TSsaEventDataTable.Destroy;
begin
    FContentLines.Free;
    inherited;
end;

end.
