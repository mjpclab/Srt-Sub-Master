unit SsaSubTitleData;
interface

uses SubTitleData,SubTitleDataTable,SsaSectionCollection,TimeManager,SsaTimeManager
    ,Classes;

type TSsaSubTitleData=class(TSubTitleData)
  private
    FSections:TSsaSectionCollection;
  protected
    function getSubTitle: TSubTitleDataTable;override;
    procedure setSubTitle(const Value: TSubTitleDataTable);override;
  public
    property Sections:TSsaSectionCollection read FSections write FSections;

    constructor Create;
    destructor Destroy;override;

    function CreateTimeManager:TTimeManager;override;

    class function getPureTextContent(ssaContent:string):string;
    class function ToSsaContent(pureTextContent:string):string;

    procedure ParseContent;override;
    procedure SerializeContent;override;

    procedure Clear;override;
    procedure DisableControls;override;
    procedure EnableControls;override;
    procedure RememberPosition;override;
    procedure ReturnRememberedPosition;override;
    procedure RememberBookmark;override;
    procedure ReturnRememberedBookmark;override;
    procedure DisableIndexFieldNames;override;
    procedure EnableIndexFieldNames;override;
end;

implementation
uses SsaDialogueEventDataTable,CommonDefs,
    SysUtils;

{ TSsaData }

constructor TSsaSubTitleData.Create;
begin
    inherited;
    Sections:=TSsaSectionCollection.Create;
end;

destructor TSsaSubTitleData.Destroy;
begin
    Sections.Free;
    inherited;
end;

function TSsaSubTitleData.getSubTitle: TSubTitleDataTable;
begin
    Exit(Sections.EventsSection.DialogueEvents);
end;

procedure TSsaSubTitleData.setSubTitle(const Value: TSubTitleDataTable);
begin
    Sections.EventsSection.DialogueEvents:=TSsaDialogueEventDataTable(Value);
end;

function TSsaSubTitleData.CreateTimeManager: TTimeManager;
var
    timeMan:TSsaTimeManager;
begin
    timeMan:=TSsaTimeManager.Create;
    exit(timeMan);
end;

class function TSsaSubTitleData.getPureTextContent(ssaContent: string): string;
var
    buffer:string;
    pos1,pos2:integer;
begin
    buffer:=ssaContent;

    while true do begin
        pos1:=pos('{',buffer);
        pos2:=pos('}',buffer);

        if (pos1>0) and (pos2>pos1) then begin
            Delete(buffer,pos1,pos2-pos1+1);
        end else begin
            break;
        end;
    end;

    buffer:=StringReplace(buffer,'\n',strNewLine,[rfReplaceall]);

    Exit(buffer);
end;

class function TSsaSubTitleData.ToSsaContent(pureTextContent: string): string;
var
    buffer:string;
begin
    buffer:=StringReplace(pureTextContent,strNewLine,'\n',[rfReplaceall]);
    Exit(buffer);
end;

procedure TSsaSubTitleData.ParseContent;
begin
    Sections.ParseContent(TextContent);
end;

procedure TSsaSubTitleData.SerializeContent;
begin
    Sections.SerializeContent;
    TextContent.Text:=Sections.TextContent.Text;
end;

procedure TSsaSubTitleData.Clear;
begin
    Sections.Clear;
    TextContent.Clear;
end;

procedure TSsaSubTitleData.DisableControls;
begin
    Sections.DisableControls;
end;

procedure TSsaSubTitleData.EnableControls;
begin
    Sections.EnableControls;
end;

procedure TSsaSubTitleData.RememberPosition;
begin
    Sections.RememberPosition;
end;

procedure TSsaSubTitleData.ReturnRememberedPosition;
begin
    Sections.ReturnRememberedPosition;
end;

procedure TSsaSubTitleData.RememberBookmark;
begin
    Sections.RememberBookmark;
end;

procedure TSsaSubTitleData.ReturnRememberedBookmark;
begin
    Sections.ReturnRememberedBookmark;
end;

procedure TSsaSubTitleData.DisableIndexFieldNames;
begin
    Sections.DisableIndexFieldNames;
end;

procedure TSsaSubTitleData.EnableIndexFieldNames;
begin
    Sections.EnableIndexFieldNames;
end;

end.

