unit SubTitleFileTransformer;

interface
uses FileTypeRecognizer;

type TSubTitleFileTransformer=class
  private
    FSourceFileName:string;
    FDestFileName:string;

    fileRec:TFileTypeRecognizer;
  public
    property SourceFileName:string read FSourceFileName write FSourceFileName;
    property DestFileName:string read FDestFileName write FDestFileName;

    procedure Transform;

    constructor Create;
    destructor Destroy;override;
end;

implementation
uses
    CommonFromOtherSubTitleFile,CommonFromSrtSubTitleFile,CommonFromSsaSubTitleFile,
    CommonToOtherSubTitleFile,CommonToSrtSubTitleFile,CommonToSsaSubTitleFile;

{ TSubTitleFileTransformer }

constructor TSubTitleFileTransformer.Create;
begin
    inherited;
    fileRec:=TFileTypeRecognizer.Create;
end;

destructor TSubTitleFileTransformer.Destroy;
begin
    fileRec.Free;
    inherited;
end;

procedure TSubTitleFileTransformer.Transform;
var
    fromFile:TCommonFromOtherSubTitleFile;
    toFile:TCommonToOtherSubTitleFile;
begin
    fileRec.FileName:=SourceFileName;
    if fileRec.IsSrtFile then begin
        fromFile:=TCommonFromSrtSubTitleFile.Create;
    end else if fileRec.IsSsaOrAssFile then begin
        fromFile:=TCommonFromSsaSubTitleFile.Create;
    end else begin
        exit;
    end;

    fileRec.FileName:=DestFileName;
    if fileRec.IsSrtFile then begin
        toFile:=TCommonToSrtSubTitleFile.Create;
    end else if fileRec.IsSsaOrAssFile then begin
        toFile:=TCommonToSsaSubTitleFile.Create;
    end else begin
        fromFile.Free;
        exit;
    end;

    fromfile.LoadFromFile(SourceFileName);
    tofile.Data.SubTitle.CopyFrom(fromfile.Data.SubTitle);
    try
        tofile.SaveToFile(DestFileName);
    finally
        fromFile.Free;
        toFile.Free;
    end;
end;

end.
