unit CommonTimeManager;

interface
uses TimeManager;

type TCommonTimeManager=class(TTimeManager)
  public
    function ToString:string;override;
    procedure Parse(const timeString:string);override;
end;

implementation

{ TCommonTimeManager }
uses SysUtils,StrUtils;

function TCommonTimeManager.ToString: string;
const
    timeFormatPattern:string='%s:%s:%s.%s';
begin
    exit(
        Format(timeFormatPattern,[
            IntToStr(self.Hour),
            self.MinuteString,
            self.SecondString,
            LeftStr(self.MilliSecondString,2)
        ])
    );
end;

procedure TCommonTimeManager.Parse(const timeString: string);
const
    separatorTime:char=':';
    separatorMs:char='.';
var
    currentPos:integer;
    buffer:string;

    intHour,intMinute,intSecond,intMilliSecond:integer;
    strHour,strMinute,strSecond,strMilliSecond:string;
begin
    buffer:=timeString;

    //提取小时
    currentPos:=Pos(separatorTime,buffer);
    strHour:=LeftStr(buffer,currentPos-1);
    intHour:=StrToIntDef(strHour,0);
    buffer:=RightStr(buffer,Length(buffer)-currentPos);

    //提取分钟
    currentPos:=Pos(separatorTime,buffer);
    strMinute:=LeftStr(buffer,currentPos-1);
    intMinute:=StrToIntDef(strMinute,0);
    buffer:=RightStr(buffer,Length(buffer)-currentPos);

    //提取秒
    currentPos:=Pos(separatorMs,buffer);
    strSecond:=LeftStr(buffer,currentPos-1);
    intSecond:=StrToIntDef(strSecond,0);
    buffer:=RightStr(buffer,Length(buffer)-currentPos);

    //提取毫秒
    strMilliSecond:=LeftStr(buffer,3);
    strMilliSecond:=strMilliSecond+DupeString('0',3-Length(strMilliSecond));
    intMilliSecond:=StrToIntDef(strMilliSecond,0);

    //装配
    self.Hour:=intHour;
    self.Minute:=intMinute;
    self.Second:=intSecond;
    self.MilliSecond:=intMilliSecond;
end;


end.
