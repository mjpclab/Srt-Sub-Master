unit FileNameTransformer;

interface

type TFileNameTransformer=class
  private
    FSourceFileName:string;
    FDestFileName:string;
    FDestFileExt:string;
    procedure setDestFileExt(const Value: string);
    function getSourceFileExt: string;
  public
    property SourceFileName:string read FSourceFileName write FSourceFileName;
    property SourceFileExt:string read getSourceFileExt;
    property DestFileName:string read FDestFileName;
    property DestFileExt:string read FDestFileExt write setDestFileExt;
end;

implementation
uses SysUtils,StrUtils;

{ TFileNameTransformer }

{ TFileNameTransformer }

function TFileNameTransformer.getSourceFileExt: string;
begin
    Exit(ExtractFileExt(FSourceFileName));
end;

procedure TFileNameTransformer.setDestFileExt(const Value: string);

begin
    FDestFileExt := Value;

    FDestFileName:=FSourceFileName;
    if Length(SourceFileExt)=4 then FDestFileName:=LeftStr(FDestFileName,Length(FDestFileName)-4);
    FDestFileName:=FDestFileName + Value;
end;

end.
