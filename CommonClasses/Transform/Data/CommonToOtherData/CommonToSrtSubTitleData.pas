unit CommonToSrtSubTitleData;

interface
uses CommonToOtherSubTitleData,SrtSubTitleData;

type TCommonToSrtSubTitleData=class(TCommonToOtherSubTitleData)
  private
    FDestSrtData:TSrtSubTitleData;
  public
    property DestSrtData:TSrtSubTitleData read FDestSrtData write FDestSrtData;

    procedure CreateDestData;override;
    procedure DestroyDestData;override;

    procedure SerializeContent;override;
end;

implementation
uses TimeManager,SrtSubTitleDataTable;

{ TCommonToSrtSubTitleData }

procedure TCommonToSrtSubTitleData.CreateDestData;
begin
    DestSrtData:=TSrtSubTitleData.Create;
    DestData:=DestSrtData;
end;

procedure TCommonToSrtSubTitleData.DestroyDestData;
begin
    DestSrtData.Free;
end;

procedure TCommonToSrtSubTitleData.SerializeContent;
var
    commonTime:TTimeManager;
    destTime:TTimeManager;
    destSrtSubTitle:TSrtSubTitleDataTable;
begin
    commonTime:=self.CreateTimeManager;
    destTime:=DestData.CreateTimeManager;

    CommonSubTitle.DisableControls;
    CommonSubTitle.DisableIndexFieldNames;
    CommonSubTitle.RememberPosition;
    with CommonSubTitle do begin
        First;

        destSrtSubTitle:=DestSrtData.SrtSubTitle;
        destSrtSubTitle.DisableControls;
        destSrtSubTitle.DisableIndexFieldNames;
        destSrtSubTitle.RememberPosition;
        destSrtSubTitle.Clear;
        while not Eof do begin
            destSrtSubTitle.Append;

            commonTime.Parse(CurrentTimeFrom);
            destTime.Assign(commonTime);
            destSrtSubTitle.CurrentTimeFrom:=destTime.ToString;

            commonTime.Parse(CurrentTimeTo);
            destTime.Assign(commonTime);
            destSrtSubTitle.CurrentTimeTo:=destTime.ToString;

            destSrtSubTitle.CurrentContent:=CurrentContent;

            destSrtSubTitle.Post;
            Next;
        end;
        DestSrtData.SerializeContent;
        self.TextContent.Text:=DestSrtData.TextContent.Text;

        destSrtSubTitle.ReturnRememberedPosition;
        destSrtSubTitle.EnableIndexFieldNames;
        destSrtSubTitle.EnableControls;

        commonTime.Free;
        destTime.Free;
    end;

    CommonSubTitle.ReturnRememberedPosition;
    CommonSubTitle.EnableIndexFieldNames;
    CommonSubTitle.EnableControls;
end;


end.
