unit CommonToOtherSubTitleData;

interface
uses CommonSubTitleData,SubTitleData;

type TCommonToOtherSubTitleData=class(TCommonSubTitleData)
  private
    FDestData:TSubTitleData;
  public
    property DestData:TSubTitleData read FDestData write FDestData;

    procedure CreateDestData;virtual;
    procedure DestroyDestData;virtual;

    constructor Create;
    destructor Destroy;override;
end;

implementation

{ TCommonToOtherSubTitleData }

constructor TCommonToOtherSubTitleData.Create;
begin
    inherited;
    CreateDestData;
end;

destructor TCommonToOtherSubTitleData.Destroy;
begin
    DestroyDestData;
    inherited;
end;

procedure TCommonToOtherSubTitleData.CreateDestData;
begin
    //
end;

procedure TCommonToOtherSubTitleData.DestroyDestData;
begin
    //
end;

end.
