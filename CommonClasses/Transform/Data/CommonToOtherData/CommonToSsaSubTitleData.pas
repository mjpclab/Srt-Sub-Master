unit CommonToSsaSubTitleData;

interface
uses CommonToOtherSubTitleData,SsaSubTitleData;

type TCommonToSsaSubTitleData=class(TCommonToOtherSubTitleData)
  private
    FDestSsaData:TSsaSubTitleData;
  public
    property DestSsaData:TSsaSubTitleData read FDestSsaData write FDestSsaData;

    procedure CreateDestData;override;
    procedure DestroyDestData;override;

    procedure SerializeContent;override;
end;

implementation
uses TimeManager,SsaDialogueEventDataTable;

{ TCommonToSsaSubTitleData }

procedure TCommonToSsaSubTitleData.CreateDestData;
begin
    DestSsaData:=TSsaSubTitleData.Create;
    DestData:=DestSsaData;
end;

procedure TCommonToSsaSubTitleData.DestroyDestData;
begin
    DestSsaData.Free;
end;

procedure TCommonToSsaSubTitleData.SerializeContent;
var
    commonTime:TTimeManager;
    destTime:TTimeManager;
    destSsaDialog:TSsaDialogueEventDataTable;
begin
    commonTime:=self.CreateTimeManager;
    destTime:=DestSsaData.CreateTimeManager;

    CommonSubTitle.DisableControls;
    CommonSubTitle.DisableIndexFieldNames;
    CommonSubTitle.RememberPosition;
    with CommonSubTitle do begin
        First;

        destSsaDialog:=DestSsaData.Sections.EventsSection.DialogueEvents;
        destSsaDialog.DisableControls;
        destSsaDialog.DisableIndexFieldNames;
        destSsaDialog.RememberPosition;
        destSsaDialog.Clear;
        while not Eof do begin
            destSsaDialog.Append;

            destSsaDialog.CurrentMarked:=0;

            commonTime.Parse(CurrentTimeFrom);
            destTime.Assign(commonTime);
            destSsaDialog.CurrentTimeStart:=destTime.ToString;

            commonTime.Parse(CurrentTimeTo);
            destTime.Assign(commonTime);
            destSsaDialog.CurrentTimeEnd:=destTime.ToString;

            destSsaDialog.CurrentStyle:='*Default';
            destSsaDialog.CurrentName:='NTP';
            destSsaDialog.CurrentMarginL:=0;
            destSsaDialog.CurrentMarginR:=0;
            destSsaDialog.CurrentMarginV:=0;
            destSsaDialog.CurrentEffect:='';
            destSsaDialog.CurrentText:=CurrentContent;

            destSsaDialog.Post;
            Next;
        end;

        DestSsaData.SerializeContent;
        self.TextContent.Text:=DestSsaData.TextContent.Text;

        destSsaDialog.ReturnRememberedPosition;
        destSsaDialog.EnableIndexFieldNames;
        destSsaDialog.EnableControls;

        commonTime.Free;
        destTime.Free;
    end;

    CommonSubTitle.ReturnRememberedPosition;
    CommonSubTitle.EnableIndexFieldNames;
    CommonSubTitle.EnableControls;
end;


end.
