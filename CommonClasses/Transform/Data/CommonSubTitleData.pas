unit CommonSubTitleData;

interface
uses SubTitleData,SubTitleDataTable,CommonSubTitleDataTable,TimeManager,CommonTimeManager;

type TCommonSubTitleData=class(TSubTitleData)
  private
    FCommonSubTitle:TCommonSubTitleDataTable;
  protected
    function getSubTitle: TSubTitleDataTable;override;
    procedure setSubTitle(const Value: TSubTitleDataTable);override;
  public
    constructor Create;
    destructor Destroy;override;

    property CommonSubTitle:TCommonSubTitleDataTable read FCommonSubTitle write FCommonSubTitle;

    function CreateTimeManager:TTimeManager;override;


    procedure ParseContent;override;
    procedure SerializeContent;override;
end;

implementation

{ TCommonSubTitleData }

constructor TCommonSubTitleData.Create;
begin
    inherited;
    CommonSubTitle:=TCommonSubTitleDataTable.Create(nil);
end;

destructor TCommonSubTitleData.Destroy;
begin
    CommonSubTitle.Free;
    inherited;
end;

function TCommonSubTitleData.getSubTitle: TSubTitleDataTable;
begin
    Exit(FCommonSubTitle);
end;

procedure TCommonSubTitleData.setSubTitle(const Value: TSubTitleDataTable);
begin
    FCommonSubTitle:=TCommonSubTitleDataTable(Value);
end;

function TCommonSubTitleData.CreateTimeManager: TTimeManager;
var
    timeMan:TCommonTimeManager;
begin
    timeMan:=TCommonTimeManager.Create;
    exit(timeMan);
end;

procedure TCommonSubTitleData.ParseContent;
begin
    CommonSubTitle.ParseContent(TextContent);
end;

procedure TCommonSubTitleData.SerializeContent;
begin
    CommonSubTitle.SerializeContent;
    TextContent.Text:=CommonSubTitle.TextContent.Text;
end;

end.
