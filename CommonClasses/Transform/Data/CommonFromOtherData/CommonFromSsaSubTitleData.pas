unit CommonFromSsaSubTitleData;

interface
uses CommonFromOtherSubTitleData,SsaSubTitleData;

type TCommonFromSsaSubTitleData=class(TCommonFromOtherSubTitleData)
  private
    FSourceSsaData:TSsaSubTitleData;
  public
    property SourceSsaData:TSsaSubTitleData read FSourceSsaData write FSourceSsaData;

    procedure CreateSourceData;override;
    procedure DestroySourceData;override;

    procedure ParseContent;override;
end;

implementation
uses TimeManager;

{ TCommonFromSsaSubTitleData }

procedure TCommonFromSsaSubTitleData.CreateSourceData;
begin
    SourceSsaData:=TSsaSubTitleData.Create;
    SourceData:=SourceSsaData;
end;

procedure TCommonFromSsaSubTitleData.DestroySourceData;
begin
    SourceSsaData.Free;
end;

procedure TCommonFromSsaSubTitleData.ParseContent;
var
    commonTime:TTimeManager;
    sourceTime:TTimeManager;
begin
    commonTime:=self.CreateTimeManager;
    sourceTime:=SourceData.CreateTimeManager;

    SourceSsaData.TextContent.Text:=self.TextContent.Text;
    SourceSsaData.ParseContent;
    SourceSsaData.DisableControls;
    SourceSsaData.DisableIndexFieldNames;
    SourceSsaData.RememberPosition;
    with SourceSsaData.Sections.EventsSection.DialogueEvents do begin
        First;

        CommonSubTitle.DisableControls;
        CommonSubTitle.DisableIndexFieldNames;
        CommonSubTitle.Clear;
        while not Eof do begin
            CommonSubTitle.Append;

            sourceTime.Parse(CurrentTimeStart);
            commonTime.Assign(sourceTime);
            CommonSubTitle.CurrentTimeFrom:=commonTime.ToString;

            sourceTime.Parse(CurrentTimeEnd);
            commonTime.Assign(sourceTime);
            CommonSubTitle.CurrentTimeTo:=commonTime.ToString;

            CommonSubTitle.CurrentContent:=SourceSsaData.getPureTextContent(CurrentText);

            CommonSubTitle.Post;
            Next;
        end;
        CommonSubTitle.EnableIndexFieldNames;
        CommonSubTitle.EnableControls;

        commonTime.Free;
        sourceTime.Free;
    end;

    SourceSsaData.ReturnRememberedPosition;
    SourceSsaData.EnableIndexFieldNames;
    SourceSsaData.EnableControls;
end;

end.
