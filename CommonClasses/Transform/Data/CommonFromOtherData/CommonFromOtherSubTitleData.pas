unit CommonFromOtherSubTitleData;

interface
uses CommonSubTitleData,SubTitleData;

type TCommonFromOtherSubTitleData=class(TCommonSubTitleData)
  private
    FSourceData:TSubTitleData;
  public
    property SourceData:TSubTitleData read FSourceData write FSourceData;

    procedure CreateSourceData;virtual;
    procedure DestroySourceData;virtual;

    constructor Create;
    destructor Destroy;override;
end;

implementation

{ TCommonFromOtherSubTitleData }

constructor TCommonFromOtherSubTitleData.Create;
begin
    inherited;
    CreateSourceData;
end;

destructor TCommonFromOtherSubTitleData.Destroy;
begin
    DestroySourceData;
    inherited;
end;

procedure TCommonFromOtherSubTitleData.CreateSourceData;
begin
    //Not Implemented
end;

procedure TCommonFromOtherSubTitleData.DestroySourceData;
begin
    //Not Implemented
end;

end.
