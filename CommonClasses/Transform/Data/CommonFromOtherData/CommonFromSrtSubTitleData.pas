unit CommonFromSrtSubTitleData;

interface
uses CommonFromOtherSubTitleData,SrtSubTitleData;

type TCommonFromSrtSubTitleData=class(TCommonFromOtherSubTitleData)
  private
    FSourceSrtData:TSrtSubTitleData;
  public
    property SourceSrtData:TSrtSubTitleData read FSourceSrtData write FSourceSrtData;

    procedure CreateSourceData;override;
    procedure DestroySourceData;override;

    procedure ParseContent;override;
end;

implementation
uses TimeManager;

{ TCommonFromSrtSubTitleData }

procedure TCommonFromSrtSubTitleData.CreateSourceData;
begin
    SourceSrtData:=TSrtSubTitleData.Create;
    SourceData:=SourceSrtData;
end;

procedure TCommonFromSrtSubTitleData.DestroySourceData;
begin
    SourceSrtData.Free;
end;

procedure TCommonFromSrtSubTitleData.ParseContent;
var
    commonTime:TTimeManager;
    sourceTime:TTimeManager;
begin
    commonTime:=self.CreateTimeManager;
    sourceTime:=SourceData.CreateTimeManager;

    SourceSrtData.TextContent.Text:=Self.TextContent.Text;
    SourceSrtData.ParseContent;
    SourceSrtData.DisableControls;
    SourceSrtData.DisableIndexFieldNames;
    SourceSrtData.RememberPosition;
    with SourceSrtData.SrtSubTitle do begin
        First;

        CommonSubTitle.DisableControls;
        CommonSubTitle.DisableIndexFieldNames;
        CommonSubTitle.Clear;
        while not Eof do begin
            CommonSubTitle.Append;

            sourceTime.Parse(CurrentTimeFrom);
            commonTime.Assign(sourceTime);
            CommonSubTitle.CurrentTimeFrom:=commonTime.ToString;

            sourceTime.Parse(CurrentTimeTo);
            commonTime.Assign(sourceTime);
            CommonSubTitle.CurrentTimeTo:=commonTime.ToString;

            CommonSubTitle.CurrentContent:=CurrentContent;

            CommonSubTitle.Post;
            Next;
        end;
        CommonSubTitle.EnableIndexFieldNames;
        CommonSubTitle.EnableControls;

        commonTime.Free;
        sourceTime.Free;
    end;
    SourceSrtData.ReturnRememberedPosition;
    SourceSrtData.EnableIndexFieldNames;
    SourceSrtData.EnableControls;
end;

end.
