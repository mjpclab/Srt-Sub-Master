unit CommonFromOtherSubTitleFile;

interface
uses CommonSubTitleFile,CommonSubTitleData,CommonFromOtherSubTitleData;

type TCommonFromOtherSubTitleFile=class(TCommonSubTitleFile)
  private
    FCommonFromOtherData:TCommonFromOtherSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property CommonFromOtherData:TCommonFromOtherSubTitleData read FCommonFromOtherData write FCommonFromOtherData;
end;

implementation

{ TCommonFromOtherSubTitleFile }

procedure TCommonFromOtherSubTitleFile.CreateData;
begin
    CommonFromOtherData:=TCommonFromOtherSubTitleData.Create;
    CommonData:=CommonFromOtherData;
    Data:=CommonFromOtherData;
end;

procedure TCommonFromOtherSubTitleFile.DestroyData;
begin
    CommonFromOtherData.Free;
end;

end.
