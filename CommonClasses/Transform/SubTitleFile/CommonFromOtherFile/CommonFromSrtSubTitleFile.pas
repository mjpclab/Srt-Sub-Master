unit CommonFromSrtSubTitleFile;

interface
uses CommonFromOtherSubTitleFile,CommonFromSrtSubTitleData,CommonSubTitleData;

type TCommonFromSrtSubTitleFile=class(TCommonFromOtherSubTitleFile)
  private
    FCommonFromSrtData:TCommonFromSrtSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property CommonFromSrtData:TCommonFromSrtSubTitleData read FCommonFromSrtData write FCommonFromSrtData;
end;

implementation

{ TCommonFromSrtSubTitleFile }

procedure TCommonFromSrtSubTitleFile.CreateData;
begin
    CommonFromSrtData:=TCommonFromSrtSubTitleData.Create;
    CommonFromOtherData:=CommonFromSrtData;
    CommonData:=CommonFromSrtData;
    Data:=CommonFromSrtData;
end;

procedure TCommonFromSrtSubTitleFile.DestroyData;
begin
    CommonFromSrtData.Free;
end;

end.
