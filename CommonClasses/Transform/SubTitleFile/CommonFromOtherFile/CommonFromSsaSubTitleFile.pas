unit CommonFromSsaSubTitleFile;

interface
uses CommonFromOtherSubTitleFile,SsaSubTitleData,CommonFromSsaSubTitleData;

type TCommonFromSsaSubTitleFile=class(TCommonFromOtherSubTitleFile)
  private
    FCommonFromSsaData:TCommonFromSsaSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property CommonFromSsaData:TCommonFromSsaSubTitleData read FCommonFromSsaData write FCommonFromSsaData;
end;

implementation
uses CommonSubTitleData;

{ TCommonFromSsaSubTitleFile }

procedure TCommonFromSsaSubTitleFile.CreateData;
begin
    CommonFromSsaData:=TCommonFromSsaSubTitleData.Create;
    CommonFromOtherData:=CommonFromSsaData;
    CommonData:=CommonFromSsaData;
    Data:=CommonFromSsaData;
end;

procedure TCommonFromSsaSubTitleFile.DestroyData;
begin
    CommonFromSsaData.Free;
end;

end.
