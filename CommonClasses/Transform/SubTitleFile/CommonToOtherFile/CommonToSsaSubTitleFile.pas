unit CommonToSsaSubTitleFile;

interface
uses CommonToOtherSubTitleFile,CommonToSsaSubTitleData,SsaSubTitleData;

type TCommonToSsaSubTitleFile=class(TCommonToOtherSubTitleFile)
  private
    FCommonToSsaData:TCommonToSsaSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property CommonToSsaData:TCommonToSsaSubTitleData read FCommonToSsaData write FCommonToSsaData;
end;

implementation
uses CommonSubTitleData;

{ TCommonToSsaSubTitleFile }

procedure TCommonToSsaSubTitleFile.CreateData;
begin
    CommonToSsaData:=TCommonToSsaSubTitleData.Create;
    CommonToOtherData:=CommonToSsaData;
    CommonData:=CommonToSsaData;
    Data:=CommonToSsaData;
end;

procedure TCommonToSsaSubTitleFile.DestroyData;
begin
    CommonToSsaData.Free;
end;

end.
