unit CommonToOtherSubTitleFile;

interface
uses CommonSubTitleFile,CommonToOtherSubTitleData;

type TCommonToOtherSubTitleFile=class(TCommonSubTitleFile)
  private
    FCommonToOtherData:TCommonToOtherSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property CommonToOtherData:TCommonToOtherSubTitleData read FCommonToOtherData write FCommonToOtherData;
end;

implementation

{ TCommonToOtherSubTitleFile }
uses CommonSubTitleData;

procedure TCommonToOtherSubTitleFile.CreateData;
begin
    CommonToOtherData:=TCommonToOtherSubTitleData.Create;
    CommonData:=CommonToOtherData;
    Data:=CommonToOtherData;
end;

procedure TCommonToOtherSubTitleFile.DestroyData;
begin
    CommonToOtherData.Free;
end;

end.
