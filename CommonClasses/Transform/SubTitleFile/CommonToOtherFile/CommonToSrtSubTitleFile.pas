unit CommonToSrtSubTitleFile;

interface
uses CommonToOtherSubTitleFile,CommonToSrtSubTitleData,SrtSubTitleData;

type TCommonToSrtSubTitleFile=class(TCommonToOtherSubTitleFile)
  private
    FCommonToSrtData:TCommonToSrtSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property CommonToSrtData:TCommonToSrtSubTitleData read FCommonToSrtData write FCommonToSrtData;
end;

implementation
uses CommonSubTitleData;

{ TCommonToSrtSubTitleFile }

procedure TCommonToSrtSubTitleFile.CreateData;
begin
    CommonToSrtData:=TCommonToSrtSubTitleData.Create;
    CommonToOtherData:=CommonToSrtData;
    CommonData:=CommonToSrtData;
    Data:=CommonToSrtData;
end;

procedure TCommonToSrtSubTitleFile.DestroyData;
begin
    CommonToSrtData.Free;
end;

end.
