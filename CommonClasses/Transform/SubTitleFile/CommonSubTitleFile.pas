unit CommonSubTitleFile;

interface
uses CommonSubTitleData,SubTitleFile;

type TCommonSubTitleFile=class(TSubTitleFile)
  private
    FCommonData:TCommonSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property CommonData:TCommonSubTitleData read FCommonData write FCommonData;
end;

implementation

{ TCommonSubTitleFile }

procedure TCommonSubTitleFile.CreateData;
begin
    CommonData:=TCommonSubTitleData.Create;
    Data:=CommonData;
end;

procedure TCommonSubTitleFile.DestroyData;
begin
    CommonData.Free;
end;

end.
