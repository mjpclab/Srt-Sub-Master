unit CommonSubTitleDataTable;

interface
uses SubTitleDataTable,CommonTimeManager
    ,Classes;

type TCommonSubTitleDataTable=class(TSubTitleDataTable)
  private
    procedure InitializeFieldDefs;

    function getCurrentTimeFrom: string;
    procedure setCurrentTimeFrom(const Value: string);
    function getCurrentTimeTo: string;
    procedure setCurrentTimeTo(const Value: string);
    function getCurrentContent: string;
    procedure setCurrentContent(const Value: string);
  protected
    procedure Initialize;override;
    procedure Finalize;override;
  public const
    TimeFromField:string='timefrom';
    TimeToField:string='timeto';
    ContentField:string='content';

    TimeFromFieldIndex:integer=0;
    TimeToFieldIndex:integer=1;
    ContentFieldIndex:integer=2;

  public
    property CurrentTimeFrom:string read getCurrentTimeFrom write setCurrentTimeFrom;
    property CurrentTimeTo:string read getCurrentTimeTo write setCurrentTimeTo;
    property CurrentContent:string read getCurrentContent write setCurrentContent;

    procedure ParseContent(const content:TStrings);override;
    procedure SerializeContent;override;
end;

implementation
uses DB;

{ TCommonSubTitleDataTable }

procedure TCommonSubTitleDataTable.Initialize;
begin
    InitializeFieldDefs;

    IndexFieldNamesForSort:='timefrom;timeto';
    EnableIndexFieldNames;

    self.CreateDataTable;
    Self.Open;
    Self.LogChanges:=false;
end;

procedure TCommonSubTitleDataTable.InitializeFieldDefs;
begin
    with self.FieldDefs.AddFieldDef do begin
        Name:=TimeFromField;
        DisplayName:='开始时间';
        DataType:=ftString;
    end;
    with self.FieldDefs.AddFieldDef do begin
        Name:=TimeToField;
        DisplayName:='结束时间';
        DataType:=ftString;
    end;
    with self.FieldDefs.AddFieldDef do begin
        Name:=ContentField;
        DisplayName:='字幕内容';
        DataType:=ftMemo;
    end;
end;

procedure TCommonSubTitleDataTable.Finalize;
begin
    self.Close;
end;

procedure TCommonSubTitleDataTable.ParseContent(const content: TStrings);
begin
    //Not Implemented
end;

procedure TCommonSubTitleDataTable.SerializeContent;
begin
    //Not Implemented
end;

{$REGION '属性相关'}
    function TCommonSubTitleDataTable.getCurrentTimeFrom: string;
    begin
        exit(self.Fields[TimeFromFieldIndex].AsString);
    end;

    procedure TCommonSubTitleDataTable.setCurrentTimeFrom(const Value: string);
    begin
        self.Fields[TimeFromFieldIndex].AsString:=Value;
    end;

    function TCommonSubTitleDataTable.getCurrentTimeTo: string;
    begin
        exit(self.Fields[TimeToFieldIndex].AsString);
    end;

    procedure TCommonSubTitleDataTable.setCurrentTimeTo(const Value: string);
    begin
        self.Fields[TimeToFieldIndex].AsString:=Value;
    end;

    function TCommonSubTitleDataTable.getCurrentContent: string;
    begin
        exit(self.Fields[ContentFieldIndex].AsString);
    end;

    procedure TCommonSubTitleDataTable.setCurrentContent(const Value: string);
    begin
        self.Fields[ContentFieldIndex].AsString:=Value;
    end;
{$ENDREGION}

end.
