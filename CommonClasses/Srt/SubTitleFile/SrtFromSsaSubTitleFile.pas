unit SrtFromSsaSubTitleFile;

interface
uses SrtSubTitleFile,SrtFromSsaSubTitleData;

type TSrtFromSsaSubTitleFile=class(TSrtSubTitleFile)
  private
    FSrtFromSsaData:TSrtFromSsaSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property SrtFromSsaData:TSrtFromSsaSubTitleData read FSrtFromSsaData write FSrtFromSsaData;
end;


implementation

{ TSrtFromSsaSubTitleFile }

procedure TSrtFromSsaSubTitleFile.CreateData;
begin
    SrtFromSsaData:=TSrtFromSsaSubTitleData.Create;
    SrtData:=SrtFromSsaData;
    Data:=SrtFromSsaData;
end;

procedure TSrtFromSsaSubTitleFile.DestroyData;
begin
    SrtFromSsaData.Free;
end;

end.
