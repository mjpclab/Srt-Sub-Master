unit SrtSubTitleFile;

interface
uses SrtSubTitleDataTable,SubTitleFile,SubTitleData,SrtSubTitleData;

type TSrtSubTitleFile=class(TSubTitleFile)
  private
    FSrtData:TSrtSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property SrtData:TSrtSubTitleData read FSrtData write FSrtData;
end;

implementation

{ TSrtSubTitleFile }

procedure TSrtSubTitleFile.CreateData;
begin
    SrtData:=TSrtSubTitleData.Create;
    Data:=SrtData;
end;

procedure TSrtSubTitleFile.DestroyData;
begin
    SrtData.Free;
end;

end.
