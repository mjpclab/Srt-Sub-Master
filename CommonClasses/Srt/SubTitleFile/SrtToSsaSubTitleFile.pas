unit SrtToSsaSubTitleFile;

interface
uses SrtSubTitleFile,SrtToSsaSubTitleData;

type TSrtToSsaSubTitleFile=class(TSrtSubTitleFile)
  private
    FSrtToSsaData:TSrtToSsaSubTitleData;
  protected
    procedure CreateData;override;
    procedure DestroyData;override;
  public
    property SrtToSsaData:TSrtToSsaSubTitleData read FSrtToSsaData write FSrtToSsaData;
end;

implementation

{ TSrtToSsaSubTitleFile }

procedure TSrtToSsaSubTitleFile.CreateData;
begin
    SrtToSsaData:=TSrtToSsaSubTitleData.Create;
    SrtData:=SrtToSsaData;
    Data:=SrtToSsaData;
end;

procedure TSrtToSsaSubTitleFile.DestroyData;
begin
    SrtToSsaData.Free;
end;

end.
