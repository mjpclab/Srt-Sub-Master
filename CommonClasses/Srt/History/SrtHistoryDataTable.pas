unit SrtHistoryDataTable;

interface
uses DataTable,
    Classes,DB;

type TSrtHistoryDataTable=class(TDataTable)
  private
    procedure InitializeFieldDefs;
  private const
    NoField:string='no';
    DescriptionField:string='description';
    EarlyRecNoField:string='earlyrecno';
    LaterRecNoField:string='laterrecno';

    NoFieldIndex:integer=0;
    DescriptionFieldIndex:integer=1;
    EarlyRecNoFieldIndex:integer=2;
    LaterRecNoFieldIndex:integer=3;
    function getCurrentNo: integer;
    procedure setCurrentNo(const Value: integer);
    function getCurrentDescription: WideString;
    procedure setCurrentDescription(const Value: WideString);
    function getCurrentEarlyRecNo: integer;
    function getCurrentLaterRecNo: integer;
    procedure setCurrentEarlyRecNo(const Value: integer);
    procedure setCurrentLaterRecNo(const Value: integer);
  protected
    procedure Initialize;override;
    procedure Finalize;override;
  public
    property CurrentNo:integer read getCurrentNo write setCurrentNo;
    property CurrentDescription:WideString read getCurrentDescription write setCurrentDescription;
    property CurrentEarlyRecNo:integer read getCurrentEarlyRecNo write setCurrentEarlyRecNo;
    property CurrentLaterRecNo:integer read getCurrentLaterRecNo write setCurrentLaterRecNo;
end;

implementation

{ TSrtHistoryDataTable }

{$REGION '初始化/终结'}
    procedure TSrtHistoryDataTable.Initialize;
    begin
        InitializeFieldDefs;
        self.CreateDataTable;
        self.Open;
        self.LogChanges:=false;
    end;

    procedure TSrtHistoryDataTable.InitializeFieldDefs;
    begin
        with self.FieldDefs.AddFieldDef do begin
            Name:=NoField;
            DisplayName:='序号';
            DataType:=ftInteger;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=DescriptionField;
            DisplayName:='步骤';
            DataType:=ftWideString;
            Size:=10;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=EarlyRecNoField;
            DisplayName:='之前的焦点行';
            DataType:=ftInteger;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=LaterRecNoField;
            DisplayName:='之后的焦点行';
            DataType:=ftInteger;
        end;
    end;

    procedure TSrtHistoryDataTable.Finalize;
    begin
        self.Close;
    end;
{$ENDREGION}

{$REGION '属性'}
    function TSrtHistoryDataTable.getCurrentNo: integer;
    begin
        exit(self.Fields[NoFieldIndex].AsInteger);
    end;

    procedure TSrtHistoryDataTable.setCurrentNo(const Value: integer);
    begin
        self.Fields[NoFieldIndex].AsInteger:=Value;
    end;

    function TSrtHistoryDataTable.getCurrentDescription: WideString;
    begin
        exit(self.Fields[DescriptionFieldIndex].AsWideString);
    end;

    procedure TSrtHistoryDataTable.setCurrentDescription(const Value: WideString);
    begin
        self.Fields[DescriptionFieldIndex].AsWideString:=Value;
    end;

    function TSrtHistoryDataTable.getCurrentEarlyRecNo: integer;
    begin
        exit(self.Fields[EarlyRecNoFieldIndex].AsInteger);
    end;

    procedure TSrtHistoryDataTable.setCurrentEarlyRecNo(const Value: integer);
    begin
        self.Fields[EarlyRecNoFieldIndex].AsInteger:=Value;
    end;

    function TSrtHistoryDataTable.getCurrentLaterRecNo: integer;
    begin
        exit(self.Fields[LaterRecNoFieldIndex].AsInteger);
    end;

    procedure TSrtHistoryDataTable.setCurrentLaterRecNo(const Value: integer);
    begin
        self.Fields[LaterRecNoFieldIndex].AsInteger:=Value;
    end;

{$ENDREGION}

end.
