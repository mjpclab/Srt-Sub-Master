unit SrtHistoryManager;

interface
uses SrtHistoryDataTable,SrtSubTitleDataTable,
    Generics.Collections;

type TSrtHistoryManager=class
  private
    synchronousStepNo:integer;
    FHistoryDataTable:TSrtHistoryDataTAble;
    FHistorySubTitles:TObjectList<TSrtSubTitleDataTable>;
  private
    procedure RemoveBehindCurrent;
    function getCurrentSubTitle: TSrtSubTitleDataTable;
    function getCanGoToNextStep: boolean;
    function getCanGoToPrevStep: boolean;
  public
    property HistoryDataTable:TSrtHistoryDataTable read FHistoryDataTable write FHistoryDataTable;
    property HistorySubTitles:TObjectList<TSrtSubTitleDataTable> read FHistorySubTitles write FHistorySubTitles;
    property CurrentSubTitle:TSrtSubTitleDataTable read getCurrentSubTitle;
    property CanGoToPrevStep:boolean read getCanGoToPrevStep;
    property CanGoToNextStep:boolean read getCanGoToNextStep;


    constructor Create;
    destructor Destroy;override;

    procedure Add(const description:string;const changedRecNo:integer;const subtitle:TSrtSubTitleDataTable);
    function GoToStep(const stepNo:integer;SubTitle:TSrtSubTitleDataTable):boolean;
    function GoToCurrentStep(SubTitle:TSrtSubTitleDataTable):boolean;
    function GoToPrevStep(SubTitle:TSrtSubTitleDataTable):boolean;
    function GoToNextStep(SubTitle:TSrtSubTitleDataTable):boolean;
    procedure ClearSteps;
end;

implementation

{ TSrtHistoryManager }

{$REGION '构造与析构'}
    constructor TSrtHistoryManager.Create;
    begin
        inherited;
        FHistoryDataTable:=TSrtHistoryDataTable.Create(nil);
        FHistorySubTitles:=TObjectList<TSrtSubTitleDataTable>.Create;
        ClearSteps;
    end;

    destructor TSrtHistoryManager.Destroy;
    begin
        FHistoryDataTable.Free;
        FHistorySubTitles.Free;
        inherited;
    end;
{$ENDREGION}

{$REGION '属性'}
    function TSrtHistoryManager.getCurrentSubTitle: TSrtSubTitleDataTable;
    begin
        exit(HistorySubTitles[HistoryDataTable.RecNo-1]);
    end;

    function TSrtHistoryManager.getCanGoToPrevStep: boolean;
    begin
        exit(synchronousStepNo>1);
    end;

    function TSrtHistoryManager.getCanGoToNextStep: boolean;
    begin
        exit(synchronousStepNo<HistoryDataTable.RecordCount);
    end;
{$ENDREGION}

procedure TSrtHistoryManager.Add(const description: string; const changedRecNo: integer; const subtitle: TSrtSubTitleDataTable);
var
    newEarlyRecNo:integer;
    newLaterRecNo:integer;
    historySubtitle:TSrtSubTitleDataTable;
begin
    RemoveBehindCurrent;

    with HistoryDataTable do begin
        synchronousStepNo:=RecordCount+1;

        if (Eof=false) and (Bof=false) then begin
            newEarlyRecNo:=CurrentLaterRecNo;
        end else begin
            newEarlyRecNo:=1;
        end;

        if changedRecNo>0 then begin
            newLaterRecNo:=changedRecNo;
        end else begin
            newlaterRecNo:=1;
        end;

        DisableControls;
        Append;
        CurrentNo:=RecordCount+1;
        CurrentDescription:=description;
        CurrentEarlyRecNo:=newEarlyRecNo;
        CurrentLaterRecNo:=newlaterRecNo;
        Post;

        subtitle.DisableControls;
        historySubtitle:=TSrtSubTitleDataTable.Create(nil);
        historySubtitle.CopyFrom(subtitle);
        //historySubtitle.Data:=subtitle.Data;
        HistorySubTitles.Add(historySubtitle);
        subtitle.EnableControls;

        EnableControls;
    end;
end;

procedure TSrtHistoryManager.RemoveBehindCurrent;
var
    stepNo:integer;
begin
    with HistoryDataTable do begin
        if RecNo=-1 then exit;
        if RecNo=RecordCount then exit;

        DisableControls;
        for stepNo := RecordCount downto RecNo+1 do begin
            RecNo:=stepNo;
            Delete;

            HistorySubTitles.Delete(stepNo-1);
        end;
        EnableControls;
    end;
end;

function TSrtHistoryManager.GoToStep(const stepNo:integer;SubTitle:TSrtSubTitleDataTable):boolean;
begin
    if (synchronousStepNo<>stepNo) and (stepNo>=1) and (stepNo<=HistoryDataTable.RecordCount) then begin
        synchronousStepNo:=stepNo;

        HistoryDataTable.RecNo:=stepNo;
        SubTitle.DisableControls;
        SubTitle.CopyFrom(HistorySubTitles[stepNo-1]);
        //SubTitle.Data:=HistorySubTitles[stepNo-1].Data;
        if (HistoryDataTable.CurrentLaterRecNo>=1) and (HistoryDataTable.CurrentLaterRecNo<=SubTitle.RecordCount) then begin
            SubTitle.RecNo:=HistoryDataTable.CurrentLaterRecNo;
        end;
        SubTitle.EnableControls;

        exit(true);
    end else begin
        exit(false);
    end;
end;

function TSrtHistoryManager.GoToCurrentStep(SubTitle: TSrtSubTitleDataTable):boolean;
begin
    Result:=GoToStep(HistoryDataTable.RecNo,SubTitle);
end;

function TSrtHistoryManager.GoToPrevStep(SubTitle: TSrtSubTitleDataTable): boolean;
begin
    Result:=GoToStep(synchronousStepNo-1,SubTitle);
end;

function TSrtHistoryManager.GoToNextStep(SubTitle: TSrtSubTitleDataTable): boolean;
begin
    Result:=GoToStep(synchronousStepNo+1,SubTitle);
end;

procedure TSrtHistoryManager.ClearSteps;
begin
    synchronousStepNo:=0;
    HistoryDataTable.Clear;
    HistorySubTitles.Clear;
end;

end.
