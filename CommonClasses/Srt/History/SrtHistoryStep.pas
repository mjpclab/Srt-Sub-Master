unit SrtHistoryStep;

interface
uses SrtSubTitleDataTable;

type TSrtHistoryStep=class
  private
    FOldRecNo,FNewRecNo:integer;
    FData:TSrtSubTitleDataTable;
  public
    property OldRecNo:integer read FOldRecNo write FOldRecNo;
    property NewRecNo:integer read FNewRecNo write FNewRecNo;
    property Data:TSrtSubTitleDataTable read FData write FData;
end;

implementation

end.
