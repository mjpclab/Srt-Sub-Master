unit SrtToSsaFileExportExecuter;

interface
uses SrtFileExportExecuter;

type TSrtToSsaFileExportExecuter=class(TSrtFileExportExecuter)
  protected
    procedure DoExportToFile;override;
end;

implementation
uses SrtToSsaSubTitleFile;

{ TSrtToSsaFileExportExecuter }

procedure TSrtToSsaFileExportExecuter.DoExportToFile;
var
    srtfile:TSrtToSsaSubTitleFile;
    aRecNo:integer;
begin
    SrtSubTitle.DisableControls;

    srtfile:=TSrtToSsaSubTitleFile.Create;
    srtfile.Data.DisableControls;

    //copy records
    for aRecNo in RecNos do begin
        SrtSubTitle.RecNo:=aRecNo;

        srtfile.Data.SubTitle.Append;
        srtfile.Data.SubTitle.CopyFields(SrtSubTitle);
        srtfile.Data.SubTitle.Post;
    end;

    //reset NOs
    with srtfile.SrtData.SrtSubTitle do begin
        First;
        while not Eof do begin
            Edit;
            CurrentNo:=RecNo;
            Post;
            Next;
        end;
    end;

    //save and free
    srtfile.FileName:=self.FileName;
    srtfile.SaveToFile;
    srtfile.Free;

    SrtSubTitle.EnableControls;
end;

end.
