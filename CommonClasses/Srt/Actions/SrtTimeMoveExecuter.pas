unit SrtTimeMoveExecuter;

interface
uses SrtTimeMoveExecuterBase;

type TSrtTimeMoveExecuter=class(TSrtTimeMoveExecuterBase)
  protected
    procedure WriteMovedTime;override;
end;

implementation

{ TSrtTimeMoveExecuter }

procedure TSrtTimeMoveExecuter.WriteMovedTime;
begin
    WriteMovedTimeFromToTmpTimeFrom;
    WriteMovedTimeToToTmpTimeTo;
end;

end.
