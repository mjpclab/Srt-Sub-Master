unit SrtTimeScaleExecuter;

interface
uses SrtActionExecuter,SrtTimeManager,SrtSubTitleDataTable,Generics.Collections;

type TSrtTimeScaleExecuter=class(TSrtActionExecuter)
  private
    FRecNos:TList<integer>;
    FNewTimeMin,FNewTimeMax:TSrtTimeManager;
    FOldTimeMin,FOldTimeMax:TSrtTimeManager;
    timeMan1,timeMan2:TSrtTimeManager;

    property OldTimeMin:TSrtTimeManager read FOldTimeMin write FOldTimeMin;
    property OldTimeMax:TSrtTimeManager read FOldTimeMax write FOldTimeMax;

    procedure DoScaleToTmpTime;
  public
    constructor Create(initSubTitle:TSrtSubTitleDataTable);override;
    destructor Destroy;override;


    property RecNos:TList<integer> read FRecNos write FRecNos;
    property NewTimeMin:TSrtTimeManager read FNewTimeMin write FNewTimeMin;
    property NewTimeMax:TSrtTimeManager read FNewTimeMax write FNewTimeMax;

    procedure ScaleTime;
end;

implementation

{ TTimeScaleExecuter }

constructor TSrtTimeScaleExecuter.Create(initSubTitle: TSrtSubTitleDataTable);
begin
    inherited;
    OldTimeMin:=TSrtTimeManager.Create;
    OldTimeMax:=TSrtTimeManager.Create;
    timeMan1:=TSrtTimeManager.Create;
    timeMan2:=TSrtTimeManager.Create;
end;

destructor TSrtTimeScaleExecuter.Destroy;
begin
    OldTimeMin.Free;
    OldTimeMax.Free;
    timeMan1.Free;
    timeMan2.Free;
    inherited;
end;

procedure TSrtTimeScaleExecuter.ScaleTime;
begin
    if RecNos.Count>1 then begin
        SrtSubTitle.DisableControls;
        SrtSubTitle.RememberPosition;

        DoScaleToTmpTime;
        SrtSubTitle.MoveTmpTimeToTime;

        SrtSubTitle.ReturnRememberedPosition;
        SrtSubTitle.EnableControls;
    end;
end;

procedure TSrtTimeScaleExecuter.DoScaleToTmpTime;
var
    deltaMinMS,oldSpanMS,newSpanMS:integer;
    currentOldSpanMS:integer;
    offset:integer;
    aRecNo:integer;
begin
    with SrtSubTitle do begin
        RecNo:=RecNos[0];
        OldTimeMin.Parse(CurrentTimeFrom);

        RecNo:=RecNos[RecNos.Count-1];
        OldTimeMax.Parse(CurrentTimeTo);

        deltaMinMS:=NewTimeMin.TotalMilliSeconds-OldTimeMin.TotalMilliSeconds;
        oldSpanMS:=OldTimeMax.TotalMilliSeconds-OldTimeMin.TotalMilliSeconds;
        newSpanMS:=NewTimeMax.TotalMilliSeconds-NewTimeMin.TotalMilliSeconds;

        for aRecNo in RecNos do begin
            RecNo:=aRecNo;
            Edit;

            timeMan1.Parse(CurrentTimeFrom);
            currentOldSpanMS:=timeMan1.TotalMilliSeconds-OldTimeMin.TotalMilliSeconds;
            offset:=Round(newSpanMs/oldSpanMS*currentOldSpanMS);
            offset:=offset + deltaMinMS;
            timeMan1.Assign(OldTimeMin);
            timeMan1.AddMilliSeconds(offset);
            CurrentTmpTimeFrom:=timeMan1.ToString;

            timeMan2.Parse(CurrentTimeTo);
            currentOldSpanMS:=timeMan2.TotalMilliSeconds-OldTimeMin.TotalMilliSeconds;
            offset:=Round(newSpanMs/oldSpanMS*currentOldSpanMS);
            offset:=offset + deltaMinMS;
            timeMan2.Assign(OldTimeMin);
            timeMan2.AddMilliSeconds(offset);
            CurrentTmpTimeTo:=timeMan2.ToString;

            Post;
        end;

    end;
end;

end.
