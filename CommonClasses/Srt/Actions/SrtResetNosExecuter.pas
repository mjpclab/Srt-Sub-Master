unit SrtResetNosExecuter;

interface
uses SrtActionExecuter;

type TResetNosExecuter=class(TSrtActionExecuter)
  public
    procedure ResetNos;
end;

implementation

{ TResetNosExecuter }

procedure TResetNosExecuter.ResetNos;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        First;
        while not Eof do begin
            Edit;
            CurrentNo:=RecNo;
            Post;
            Next;
        end;

        ReturnRememberedPosition;
        EnableControls;
    end;
end;

end.
