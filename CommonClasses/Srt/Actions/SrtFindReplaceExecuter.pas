unit SrtFindReplaceExecuter;

interface
uses SrtActionExecuter;

type FindMode=(fdInclude,fdStartsWith,fdEndsWith);

type TFindReplaceExecuter=class(TSrtActionExecuter)
  private
    FMode:FindMode;
    FFindText:string;
    FCaseSensitive:boolean;

    function IsMatch:boolean;
  public
    property Mode:FindMode read FMode write FMode;
    property FindText:string read FFindText write FFindText;
    property CaseSensitive:boolean read FCaseSensitive write FCaseSensitive;

    function FindFirst:boolean;
    function FindLast:boolean;
    function FindPrior:boolean;
    function FindNext:boolean;
    function FindCurrentOrNext:boolean;

    function ReplaceCurrent(const replaceText:string):boolean;
    function ReplaceAll(const replaceText:string):boolean;
end;
implementation

{ TFindReplaceExecuter }
uses SysUtils,StrUtils;

function TFindReplaceExecuter.IsMatch: boolean;
var
    mContent,mFind:string;
begin
    with SrtSubTitle do begin
        if not CaseSensitive then begin
            mContent:=LowerCase(CurrentContent);
            mFind:=LowerCase(FindText);
        end else begin
            mContent:=CurrentContent;
            mFind:=FindText;
        end;

        if (Mode=fdInclude) and (Pos(mFind,mContent)>0) then begin
            exit(true);
        end else if (Mode=fdStartsWith) and (Pos(mFind,mContent)=1) then begin
            exit(true);
        end else if (Mode=fdEndsWith) and (Pos(mFind,mContent)=Length(mContent)-Length(mFind)+1) then begin
            exit(true);
        end else begin
            exit(false);
        end;

    end;
end;

function TFindReplaceExecuter.FindFirst:boolean;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;
        First;

        Result:=false;
        while not Eof do begin
            if IsMatch then begin
                Result:=true;
                break;
            end;
            Next;
        end;

        if Result=false then ReturnRememberedPosition;
        EnableControls;
    end;
end;

function TFindReplaceExecuter.FindLast:boolean;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;
        Last;

        Result:=false;
        while not Bof do begin
            if IsMatch then begin
                Result:=true;
                break;
            end;
            Prior;
        end;

        if Result=false then ReturnRememberedPosition;
        EnableControls;
    end;
end;

function TFindReplaceExecuter.FindPrior:boolean;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;
        Prior;         //only difference from FindLast

        Result:=false;
        while not Bof do begin
            if IsMatch then begin
                Result:=true;
                break;
            end;
            Prior;
        end;

        if Result=false then ReturnRememberedPosition;
        EnableControls;
    end;
end;

function TFindReplaceExecuter.FindNext:boolean;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;
        Next;        //only difference from FindFirst

        Result:=false;
        while not Eof do begin
            if IsMatch then begin
                Result:=true;
                break;
            end;
            Next;
        end;

        if Result=false then ReturnRememberedPosition;
        EnableControls;
    end;
end;

function TFindReplaceExecuter.FindCurrentOrNext: boolean;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;
        //Next;        //only difference from FindFirst

        Result:=false;
        while not Eof do begin
            if IsMatch then begin
                Result:=true;
                break;
            end;
            Next;
        end;

        if Result=false then ReturnRememberedPosition;
        EnableControls;
    end;
end;

function TFindReplaceExecuter.ReplaceCurrent(const replaceText:string): boolean;
var
    flags:TReplaceFlags;
begin
    flags:=[rfReplaceAll];
    if not CaseSensitive then flags:=flags + [rfIgnoreCase];

    if IsMatch then begin
        with SrtSubTitle do begin
            Edit;
            CurrentContent:=StringReplace(CurrentContent,FindText,replaceText,flags);
            Post;

            if IsMatch=False then self.FindNext;    //如果已处于筛选状态，那么下一条会自动上移，则IsMatch已经为TRUE，不用向下查找
            Result:=true;
        end;
    end else if self.FindNext then begin
        Result:=true;
    end else begin
        Result:=false;
    end;
end;

function TFindReplaceExecuter.ReplaceAll(const replaceText:string): boolean;
var
    flags:TReplaceFlags;
begin
    flags:=[rfReplaceAll];
    if not CaseSensitive then flags:=flags + [rfIgnoreCase];

    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        if self.FindFirst then begin
            while self.FindCurrentOrNext do begin
                Edit;
                CurrentContent:=StringReplace(CurrentContent,FindText,replaceText,flags);
                Post;
            end;

            Result:=true;
        end else begin
            ReturnRememberedPosition;
            Result:=false;
        end;

        EnableControls;
    end;
end;

end.
