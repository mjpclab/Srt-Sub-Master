unit SrtFileMergeExecuterBase;

interface
uses SrtActionExecuter,SrtTimeManager,SrtSubTitleDataTable,SrtSubTitleFile,SrtFromSsaSubTitleFile,FileTypeRecognizer,CommonDefs;

type TSrtTimeMergeMode=(tgmEditingTime,tgmImportingTime,tgmMidTime);

type TFileMergeExecuterBase=class(TSrtActionExecuter)
  private
    srtFile:TSrtSubTitleFile;
    ssaFile:TSrtFromSsaSubTitleFile;
    fileRecognizer:TFileTypeRecognizer;
    timeMan1:TSrtTimeManager;
    timeMan2:TSrtTimeManager;
    timeMan3:TSrtTimeManager;
    importFileMan:TSrtSubTitleFile;

    FImportFileName:string;
    FTimeMergeMode:TSrtTimeMergeMode;
    FSeparator:string;
    FMergePosition:MergePosition;
    FImportUnmatched:boolean;

    procedure setImportFileName(const Value: string);
    procedure CalcCurrentTimeFromImportingTimeToTmp;
    procedure CalcCurrentTimeFromMidTimeToTmp;
    function getImportSubTitle: TSrtSubTitleDataTable;
    procedure setImportSubTitle(const Value: TSrtSubTitleDataTable);
  protected
    procedure CalcCurrentTimeToTmp;
    procedure MergeCurrentContent;
    procedure DoMerge;virtual;
  public
    property ImportFileName:string read FImportFileName write setImportFileName;
    property ImportSubTitle:TSrtSubTitleDataTable read getImportSubTitle write setImportSubTitle;
    property TimeMergeMode:TSrtTimeMergeMode read FTimeMergeMode write FTimeMergeMode;
    property Separator:string read FSeparator write FSeparator;
    property MergePosition:MergePosition read FMergePosition write FMergePosition;
    property ImportUnmatched:boolean read FImportUnmatched write FImportUnmatched;

    constructor Create(initSubTitle:TSrtSubTitleDataTable);override;
    destructor Destroy;override;

    procedure Merge;
end;

implementation

{ TFileMergeExecuter }

{$REGION '构造与析构'}
    constructor TFileMergeExecuterBase.Create(initSubTitle: TSrtSubTitleDataTable);
    begin
        inherited;
        srtFile:=TSrtSubTitleFile.Create;
        ssaFile:=TSrtFromSsaSubTitleFile.Create;
        fileRecognizer:=TFileTypeRecognizer.Create;
        timeMan1:=TSrtTimeManager.Create;
        timeMan2:=TSrtTimeManager.Create;
        timeMan3:=TSrtTimeManager.Create;
    end;

    destructor TFileMergeExecuterBase.Destroy;
    begin
        srtFile.Free;
        ssaFile.Free;
        fileRecognizer.Free;
        timeMan1.Free;
        timeMan2.Free;
        timeMan3.Free;
        inherited;
    end;
{$ENDREGION}

procedure TFileMergeExecuterBase.setImportFileName(const Value: string);
begin
    FImportFileName := Value;

    fileRecognizer.FileName:=Value;
    if fileRecognizer.IsSrtFile then begin
        importFileMan:=srtFile;
    end else if fileRecognizer.IsSsaOrAssFile then begin
        importFileMan:=ssaFile;
    end else begin
        importFileMan:=srtFile;
    end;

    importFileMan.LoadFromFile(Value);
end;

function TFileMergeExecuterBase.getImportSubTitle: TSrtSubTitleDataTable;
begin
    exit(importFileMan.SrtData.SrtSubTitle);
end;

procedure TFileMergeExecuterBase.setImportSubTitle(const Value: TSrtSubTitleDataTable);
begin
    importFileMan.SrtData.SrtSubTitle:=Value;
end;

procedure TFileMergeExecuterBase.CalcCurrentTimeToTmp;
begin
    //tamEditingTime 以现有时间为准，无需调整，故不作处理

    case TimeMergeMode of
        tgmImportingTime:CalcCurrentTimeFromImportingTimeToTmp;
        tgmMidTime:CalcCurrentTimeFromMidTimeToTmp;
    end;
end;

procedure TFileMergeExecuterBase.CalcCurrentTimeFromImportingTimeToTmp;
begin
    with SrtSubTitle do begin
        Edit;
        CurrentTmpTimeFrom:=ImportSubTitle.CurrentTimeFrom;
        CurrentTmpTimeTo:=ImportSubTitle.CurrentTimeTo;
        Post;
    end;
end;

procedure TFileMergeExecuterBase.CalcCurrentTimeFromMidTimeToTmp;
begin
    SrtSubTitle.Edit;

    timeMan1.Parse(SrtSubTitle.CurrentTimeFrom);
    timeMan2.Parse(ImportSubTitle.CurrentTimeFrom);
    timeMan3.TotalMilliSeconds:=Round((timeMan1.TotalMilliSeconds+timeMan2.TotalMilliSeconds)/2);
    SrtSubTitle.CurrentTmpTimeFrom:=timeMan3.ToString;

    timeMan1.Parse(SrtSubTitle.CurrentTimeTo);
    timeMan2.Parse(ImportSubTitle.CurrentTimeTo);
    timeMan3.TotalMilliSeconds:=Round((timeMan1.TotalMilliSeconds+timeMan2.TotalMilliSeconds)/2);
    SrtSubTitle.CurrentTmpTimeTo:=timeMan3.ToString;

    SrtSubTitle.Post;
end;

procedure TFileMergeExecuterBase.Merge;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;
        BackupFilter;

        DoMerge;

        RestoreFilter;
        ReturnRememberedPosition;
        EnableControls;
    end;
end;

procedure TFileMergeExecuterBase.DoMerge;
begin
    //to be overrided in derrived class
end;

procedure TFileMergeExecuterBase.MergeCurrentContent;
begin
    with SrtSubTitle do begin
        Edit;

        case MergePosition of
            mpInsertBefore:
                CurrentContent:=
                    ImportSubTitle.CurrentContent
                    + Separator
                    + SrtSubTitle.CurrentContent
                ;
            mpAppendAfter:
                CurrentContent:=
                    SrtSubTitle.CurrentContent
                    + Separator
                    + ImportSubTitle.CurrentContent
                ;
        end;

        Post;
    end;
end;

end.
