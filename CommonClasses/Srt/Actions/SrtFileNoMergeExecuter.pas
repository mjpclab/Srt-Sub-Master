unit SrtFileNoMergeExecuter;

interface
uses SrtFileMergeExecuterBase;

type TFileNoMergeExecuter=class(TFileMergeExecuterBase)
  private
    procedure FilterSrtSubTitleByNo(no:integer);
  protected
    procedure DoMerge;override;
end;

implementation
uses SysUtils;

{ TFileNoMergeExecuter }

procedure TFileNoMergeExecuter.DoMerge;
begin
    while not ImportSubTitle.Eof do begin
        FilterSrtSubTitleByNo(ImportSubTitle.CurrentNo);
        if not SrtSubTitle.Eof then begin
            while not SrtSubTitle.Eof do begin
                CalcCurrentTimeToTmp;
                MergeCurrentContent;
                SrtSubTitle.Next;
            end;
        end else if ImportUnmatched then begin
            SrtSubTitle.Append;
            SrtSubTitle.CopyFields(ImportSubTitle);
            SrtSubTitle.Post;
        end;

        ImportSubTitle.Next;
    end;

    SrtSubTitle.MoveTmpTimeToTime;
end;

procedure TFileNoMergeExecuter.FilterSrtSubTitleByNo(no: integer);
begin
    with SrtSubTitle do begin
        Filter:=NoField + '=' + IntToStr(no);
        Filtered:=true;
        First;
    end;
end;

end.
