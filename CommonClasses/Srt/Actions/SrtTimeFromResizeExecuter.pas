unit SrtTimeFromResizeExecuter;


interface
uses SrtTimeMoveExecuterBase;

type TSrtTimeFromResizeExecuter=class(TSrtTimeMoveExecuterBase)
  protected
    procedure WriteMovedTime;override;
end;

implementation

{ TSrtTimeFromResizeExecuter }

procedure TSrtTimeFromResizeExecuter.WriteMovedTime;
begin
    WriteMovedTimeFromToTmpTimeFrom;
end;

end.
