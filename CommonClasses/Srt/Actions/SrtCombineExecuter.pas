unit SrtCombineExecuter;

interface
uses SrtActionExecuter,CommonDefs,Generics.Collections;

type TSrtCombineExecuter=class(TSrtActionExecuter)
  private
    FRecNos:TList<Integer>;
    FSeparator:string;
    property Separator:string read FSeparator write FSeparator;

    procedure Combine;
    procedure DoCombine;
  public
    property RecNos:TList<Integer> read FRecNos write FRecNos;

    procedure CombineSingleLine;
    procedure CombineMultiLine;
end;

implementation

{ TSrtCombineExecuter }

procedure TSrtCombineExecuter.CombineMultiLine;
begin
    Separator:=strNewLine;
    Combine;
end;

procedure TSrtCombineExecuter.CombineSingleLine;
begin
    Separator:='';
    Combine;
end;

procedure TSrtCombineExecuter.Combine;
begin
    if RecNos.Count>1 then begin
        with SrtSubTitle do begin
            DisableControls;

            DoCombine;

            EnableControls;
        end;
    end;
end;

procedure TSrtCombineExecuter.DoCombine;
var
    //firstTimeFrom:string;
    lastTimeTo:string;
    buffer:string;
    i:integer;
begin
    buffer:='';

    with SrtSubTitle do begin
        for i := RecNos.Count-1 downto 0 do begin
            RecNo:=RecNos[i];
            if i=RecNos.Count-1 then lastTimeTo:=CurrentTimeTo;
            //if i=0 then firstTimeFrom:=CurrentTimeFrom;

            if CurrentContent<>'' then begin
                if buffer<>'' then buffer:=Separator+buffer;
                buffer:=CurrentContent+buffer;
            end;

            if i<>0 then Delete;
        end;

        Edit;
        CurrentTimeTo:=lastTimeTo;
        CurrentContent:=buffer;
        Post;
    end;
end;

end.
