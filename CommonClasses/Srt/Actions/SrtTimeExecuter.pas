unit SrtTimeExecuter;

interface
uses SrtActionExecuter,SrtTimeManager,SrtSubTitleDataTable;

type TSrtTimeExecuter=class(TSrtActionExecuter)
  private
    timeMan:TSrtTimeManager;
  public
    function GetTimeSeconds(timeString:string):Double;
    function GetTimeString(timeSeconds:Double):string;

    function GetCurrentTimeFromSeconds:Double;
    function GetCurrentTimeToSeconds:Double;

    constructor Create(initSubTitle:TSrtSubTitleDataTable);override;
    destructor Destroy;override;
end;

implementation
uses SysUtils;
{ TTimeExecuter }


constructor TSrtTimeExecuter.Create(initSubTitle: TSrtSubTitleDataTable);
begin
    inherited;
    timeMan:=TSrtTimeManager.Create;
end;

destructor TSrtTimeExecuter.Destroy;
begin
    timeMan.Free;
    inherited;
end;

function TSrtTimeExecuter.GetTimeSeconds(timeString: string): Double;
begin
    timeMan.Parse(timeString);
    Result:=timeMan.TotalMilliSeconds / MSecsPerSec;
end;

function TSrtTimeExecuter.GetTimeString(timeSeconds: Double): string;
begin
    timeMan.TotalMilliSeconds:=Trunc(timeSeconds*MSecsPerSec);
    Result:=timeMan.ToString;
end;

function TSrtTimeExecuter.GetCurrentTimeFromSeconds: Double;
begin
    exit(GetTimeSeconds(SrtSubTitle.CurrentTimeFrom));
end;

function TSrtTimeExecuter.GetCurrentTimeToSeconds: Double;
begin
    exit(GetTimeSeconds(SrtSubTitle.CurrentTimeTo));
end;

end.
