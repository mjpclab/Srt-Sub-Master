unit SrtDeleteEmptyExecuter;

interface
uses SrtActionExecuter,Generics.Collections,SysUtils;

type TSrtDeleteEmptyExecuter=class(TSrtActionExecuter)
  private
    FRecNos:TList<integer>;
    FIncludeWhiteSpace:boolean;

    procedure DoDeleteEmptySubTitle;
    procedure ProcessEachDelete;
  public
    property IncludeWhiteSpace:boolean read FIncludeWhiteSpace write FIncludeWhiteSpace;
    property RecNos:TList<integer> read FRecNos write FRecNos;

    procedure DeleteEmptySubTitle;
end;


implementation

{ TSrtDeleteEmptyExecuter }

procedure TSrtDeleteEmptyExecuter.DeleteEmptySubTitle;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        DoDeleteEmptySubTitle;

        ReturnRememberedPosition;
        EnableControls;
    end;
end;

procedure TSrtDeleteEmptyExecuter.DoDeleteEmptySubTitle;
var
    i:Integer;
begin
    with SrtSubTitle do begin
        for i := RecNos.Count-1 downto 0 do begin
            RecNo:=RecNos[i];
            ProcessEachDelete;
        end;
    end;
end;

procedure TSrtDeleteEmptyExecuter.ProcessEachDelete;
begin
    with SrtSubTitle do begin
        if CurrentContent='' then begin
            Delete;
        end else if IncludeWhiteSpace then begin
            if Trim(CurrentContent)='' then Delete;
        end;
    end;
end;

end.
