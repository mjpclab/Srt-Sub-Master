unit SrtAffixExecuterBase;

interface
uses SrtActionExecuter,Generics.Collections;

type TSrtAffixExecuterBase=class(TSrtActionExecuter)
  private
    FPrefix:string;
    FPostfix:string;
    FRecNos:TList<integer>;

    function getPostfixLength: integer;
    function getPrefixLength: integer;
  public
    property Prefix:string read FPrefix write FPrefix;
    property Postfix:string read FPostfix write FPostfix;
    property PrefixLength:integer read getPrefixLength;
    property PostfixLength:integer read getPostfixLength;
    property RecNos:TList<integer> read FRecNos write FRecNos;
end;
implementation

{ TAffixExecuter }

function TSrtAffixExecuterBase.getPrefixLength: integer;
begin
    exit(Length(Prefix));
end;

function TSrtAffixExecuterBase.getPostfixLength: integer;
begin
    exit(Length(Postfix));
end;

end.
