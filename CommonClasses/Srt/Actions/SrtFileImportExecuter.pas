unit SrtFileImportExecuter;

interface
uses SrtActionExecuter;

type TSrtFileImportExecuter=class(TSrtActionExecuter)
  public
    procedure ImportFromFile(const filename:string);
end;

implementation
uses SrtSubTitleFile;

{ TFileImportExecuter }

procedure TSrtFileImportExecuter.ImportFromFile(const filename: string);
var
    srtfile:TSrtSubTitleFile;
begin
    srtfile:=TSrtSubTitleFile.Create;
    srtfile.Data.DisableControls;
    srtfile.Data.TextContent.LoadFromFile(filename);
    //srtfile.Data.ParseContent;

    SrtSubTitle.DisableControls;
    with srtfile.Data.SubTitle do begin
        while not EOF do begin
            SrtSubTitle.Append;
            SrtSubTitle.CopyFields(srtfile.Data.SubTitle);
            SrtSubTitle.CurrentNo:=SrtSubTitle.RecordCount+1;
            SrtSubTitle.Post;
            Next;
        end;
    end;
    SrtSubTitle.EnableControls;

    srtfile.Free;
end;

end.
