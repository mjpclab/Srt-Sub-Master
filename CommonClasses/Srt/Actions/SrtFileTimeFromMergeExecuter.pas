unit SrtFileTimeFromMergeExecuter;

interface
uses SrtFileMergeExecuterBase;

type TFileTimeFromMergeExecuter=class(TFileMergeExecuterBase)
  protected
    procedure DoMerge;override;
end;

implementation

{ TFileTimeFromMergeExecuter }

procedure TFileTimeFromMergeExecuter.DoMerge;
begin
    while (SrtSubTitle.Eof=false) and (ImportSubTitle.Eof=false) do begin
        if SrtSubTitle.CurrentTimeFrom=ImportSubTitle.CurrentTimeFrom then begin
            CalcCurrentTimeToTmp;
            MergeCurrentContent;
            SrtSubTitle.Next;
        end else if SrtSubTitle.CurrentTimeFrom>ImportSubTitle.CurrentTimeFrom then begin
            ImportSubTitle.Next;
        end else if SrtSubTitle.CurrentTimeFrom<ImportSubTitle.CurrentTimeFrom then begin
            SrtSubTitle.Next;
        end;
    end;
    SrtSubTitle.MoveTmpTimeToTime;

    if ImportUnmatched then begin
        while ImportSubTitle.Eof=false do begin
            SrtSubTitle.Append;
            SrtSubTitle.CopyFields(ImportSubTitle);
            SrtSubTitle.Post;

            ImportSubTitle.Next;
        end;
    end;
end;

end.
