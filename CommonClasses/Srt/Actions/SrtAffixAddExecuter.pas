unit SrtAffixAddExecuter;

interface
uses SrtAffixExecuterBase;

type TSrtAffixAddExecuter=class(TSrtAffixExecuterBase)
  private
    procedure DoAddAffix;
  public
    procedure AddAffix;
end;
implementation

{ TAffixAddExecuter }

procedure TSrtAffixAddExecuter.AddAffix;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        DoAddAffix;

        ReturnRememberedPosition;
        EnableControls;
    end;
end;

procedure TSrtAffixAddExecuter.DoAddAffix;
var
    aRecNo:integer;
begin
    for aRecNo in RecNos do begin
        with SrtSubTitle do begin
            RecNo:=aRecNo;
            Edit;
            CurrentContent:=Prefix + CurrentContent + Postfix;
            Post;
        end;
    end;
end;

end.
