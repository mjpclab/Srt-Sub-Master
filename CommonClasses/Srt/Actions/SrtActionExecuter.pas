unit SrtActionExecuter;

interface
uses SrtSubTitleDataTable;

type TSrtActionExecuter=class
  private
    FSrtSubTitle:TSrtSubTitleDataTable;
  public
    constructor Create(initSubTitle:TSrtSubTitleDataTable);virtual;

    property SrtSubTitle:TSrtSubTitleDataTable read FSrtSubTitle write FSrtSubTitle;
end;

implementation

{ TActionExecuter }

constructor TSrtActionExecuter.Create(initSubTitle: TSrtSubTitleDataTable);
begin
    SrtSubTitle:=initSubTitle;
end;

end.
