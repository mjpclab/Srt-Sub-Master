unit SrtTimeMoveExecuterBase;

interface
uses SrtActionExecuter,SrtTimeManager,SrtSubTitleDataTable,Generics.Collections,DB;

type TSrtTimeMoveDirection=(tmdBackward,tmdForward);

type TSrtTimeMoveExecuterBase=class(TSrtActionExecuter)
  private
    FRecNos:TList<integer>;
    FDirection:TSrtTimeMoveDirection;
    FOffsetTime:TSrtTimeManager;
    timeMan:TSrtTimeManager;

    procedure DoWriteMovedTime(const sourceTimeField,destTimeField:integer);
  protected
    procedure WriteMovedTime;virtual;abstract;
    procedure WriteMovedTimeFromToTmpTimeFrom;
    procedure WriteMovedTimeToToTmpTimeTo;
  public
    property RecNos:TList<integer> read FRecNos write FRecNos;
    property Direction:TSrtTimeMoveDirection read FDirection write FDirection;
    property OffsetTime:TSrtTimeManager read FOffsetTime write FOffsetTime;

    constructor Create(initSubTitle:TSrtSubTitleDataTable);override;
    destructor Destroy;override;

    procedure MoveTime;
end;

implementation

{ TTimeMoveExecuter }

constructor TSrtTimeMoveExecuterBase.Create(initSubTitle: TSrtSubTitleDataTable);
begin
    inherited;
    timeMan:=TSrtTimeManager.Create;
end;

destructor TSrtTimeMoveExecuterBase.Destroy;
begin
    timeMan.Free;
    inherited;
end;

procedure TSrtTimeMoveExecuterBase.MoveTime;
begin
    SrtSubTitle.DisableControls;
    SrtSubTitle.RememberPosition;

    WriteMovedTime;
    SrtSubTitle.MoveTmpTimeToTime;

    SrtSubTitle.ReturnRememberedPosition;
    SrtSubTitle.EnableControls;
end;

procedure TSrtTimeMoveExecuterBase.WriteMovedTimeFromToTmpTimeFrom;
begin
    DoWriteMovedTime(SrtSubTitle.TimeFromFieldIndex,SrtSubTitle.TmpTimeFromFieldIndex);
end;

procedure TSrtTimeMoveExecuterBase.WriteMovedTimeToToTmpTimeTo;
begin
    DoWriteMovedTime(SrtSubTitle.TimeToFieldIndex,SrtSubTitle.TmpTimeToFieldIndex);
end;

procedure TSrtTimeMoveExecuterBase.DoWriteMovedTime(const sourceTimeField,destTimeField: integer);
var
    aRecNo:integer;
begin
    with SrtSubTitle do begin
        for aRecNo in RecNos do begin
            RecNo:=aRecNo;
            timeMan.Parse(Fields[sourceTimeField].AsString);

            case Direction of
                tmdBackward:
                    timeMan.ReduceTime(OffsetTime);
                tmdForward:
                    timeMan.AddTime(OffsetTime);
            end;

            Edit;
            Fields[destTimeField].AsString:=timeMan.ToString;
            Post;
        end;
    end;
end;

end.
