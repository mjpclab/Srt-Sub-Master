unit SrtMoveToPlayerTimeExecuter;

interface
uses SrtActionExecuter,SrtTimeManager,Generics.Collections;

type TSrtMoveToPlayerTimeExecuter=class(TSrtActionExecuter)
  private
    FCurrentPlayerMilliSeconds:integer;
    FRecNos:TList<integer>;
    function getCurrentPlayerSeconds: Double;
    procedure setCurrentPlayerSeconds(const Value: Double);
    procedure doMoveToPlayerTime;
  public
    property CurrentPlayerMilliSeconds:integer read FCurrentPlayerMilliSeconds write FCurrentPlayerMilliSeconds;
    property CurrentPlayerSeconds:Double read getCurrentPlayerSeconds write setCurrentPlayerSeconds;
    property RecNos:TList<integer> read FRecNos write FRecNos;

    procedure MoveToPlayerTime;
end;

implementation

{ TSrtMoveToPlayerTimeExecuter }

function TSrtMoveToPlayerTimeExecuter.getCurrentPlayerSeconds: Double;
begin
    Exit(FCurrentPlayerMilliSeconds/1000);
end;

procedure TSrtMoveToPlayerTimeExecuter.setCurrentPlayerSeconds(const Value: Double);
begin
    FCurrentPlayerMilliSeconds:=Trunc(Value*1000);
end;

procedure TSrtMoveToPlayerTimeExecuter.MoveToPlayerTime;
begin
    with SrtSubTitle do begin
        DisableControls;

        RememberPosition;
        doMoveToPlayerTime;
        ReturnRememberedPosition;
        MoveTmpTimeToTime;

        EnableControls;
    end;

end;

procedure TSrtMoveToPlayerTimeExecuter.doMoveToPlayerTime;
var
    timeMan1,timeMan2:TSrtTimeManager;
    deltaMilliSeconds:integer;
    iRecNo:integer;
begin
    timeMan1:=TSrtTimeManager.Create;
    timeMan2:=TSrtTimeManager.Create;

    with SrtSubTitle do begin
        //����deltaMs
        timeMan1.ParseFormatted(CurrentTimeFrom);
        deltaMilliSeconds:=CurrentPlayerMilliSeconds-timeMan1.TotalMilliSeconds;

        //ƽ��ʱ��
        for iRecNo in RecNos do begin
            RecNo:=iRecNo;
            Edit;

            timeMan1.ParseFormatted(CurrentTimeFrom);
            timeMan1.AddMilliSeconds(deltaMilliSeconds);
            CurrentTmpTimeFrom:=timeMan1.ToString;

            timeMan2.ParseFormatted(CurrentTimeTo);
            timeMan2.AddMilliSeconds(deltaMilliSeconds);
            CurrentTmpTimeTo:=timeMan2.ToString;

            Post;
        end;

    end;

    timeMan1.Free;
    timeMan2.Free;
end;

end.
