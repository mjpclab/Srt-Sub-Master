unit SrtTimeToResizeExecuter;


interface
uses SrtTimeMoveExecuterBase;

type TSrtTimeToResizeExecuter=class(TSrtTimeMoveExecuterBase)
  protected
    procedure WriteMovedTime;override;
end;

implementation

{ TSrtTimeToResizeExecuter }

procedure TSrtTimeToResizeExecuter.WriteMovedTime;
begin
    WriteMovedTimeToToTmpTimeTo;
end;

end.
