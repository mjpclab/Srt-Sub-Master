unit SrtTrimExecuter;

interface
uses SrtActionExecuter,SrtSubTitleDataTable,Generics.Collections,Classes,SysUtils;

type TSrtTrimExecuter=class(TSrtActionExecuter)
  private
    FRecNos:TList<integer>;
    FTrimLeft,FTrimRight,FTrimEmptyLine:boolean;

    bufferSource,bufferDest:TStrings;
    procedure DoTrim;
    procedure ProcessEachTrim;
  public
    constructor Create(initSubTitle:TSrtSubTitleDataTable);override;
    destructor Destroy;override;

    property RecNos:TList<integer> read FRecNos write FRecNos;
    property TrimLeft:boolean read FTrimLeft write FTrimLeft;
    property TrimRight:boolean read FTrimRight write FTrimRight;
    property TrimEmptyLine:boolean read FTrimEmptyLine write FTrimEmptyLine;

    procedure Trim;
end;

implementation

{ TSrtTrimExecuter }

{$REGION '����������'}
    constructor TSrtTrimExecuter.Create(initSubTitle: TSrtSubTitleDataTable);
    begin
        inherited;
        bufferSource:=TStringList.Create;
        bufferDest:=TStringList.Create;
    end;

    destructor TSrtTrimExecuter.Destroy;
    begin
        bufferSource.Free;
        bufferDest.Free;
        inherited;
    end;
{$ENDREGION}

procedure TSrtTrimExecuter.Trim;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        DoTrim;

        ReturnRememberedPosition;
        EnableControls;
    end;
end;

procedure TSrtTrimExecuter.DoTrim;
var
    aRecNo:integer;
begin
    with SrtSubTitle do begin
        for aRecNo in RecNos do begin
            RecNo:=aRecNo;
            ProcessEachTrim;
        end;
    end;
end;

procedure TSrtTrimExecuter.ProcessEachTrim;
var
    i:integer;
    currentLine:string;
begin
    with SrtSubTitle do begin
        bufferSource.Text:=CurrentContent;
        bufferDest.Clear;

        for i := 0 to bufferSource.Count - 1 do begin
            currentLine:=bufferSource[i];
            if self.TrimLeft then currentLine:=SysUtils.TrimLeft(currentLine);
            if self.TrimRight then currentLine:=SysUtils.TrimRight(currentLine);
            if (self.TrimEmptyLine=false) or (currentLine<>'') then begin
                bufferDest.Append(currentLine);
            end;
        end;

        Edit;
        CurrentContent:=bufferDest.Text;
        Post;
    end;
end;

end.
