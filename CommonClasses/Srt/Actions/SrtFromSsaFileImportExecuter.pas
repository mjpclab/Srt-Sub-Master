unit SrtFromSsaFileImportExecuter;

interface

uses SrtActionExecuter;

type TSrtFromSsaFileImportExecuter=class(TSrtActionExecuter)
  public
    procedure ImportFromFile(const filename:string);
end;

implementation
uses SsaSubTitleFile,SrtFromSsaSubTitleFile,CommonDefs,SysUtils;

{ TSrtFromSsaFileImportExecuter }

procedure TSrtFromSsaFileImportExecuter.ImportFromFile(const filename: string);
var
    importFile:TSrtFromSsaSubTitleFile;
begin
    importFile:=TSrtFromSsaSubTitleFile.Create;
    importFile.LoadFromFile(filename);
    importFile.Data.DisableControls;
    //importFile.Data.ParseContent;

    SrtSubTitle.DisableControls;
    with importFile.SrtData.SrtSubTitle do begin
        while not EOF do begin
            SrtSubTitle.Append;

            SrtSubTitle.CurrentNo:=SrtSubTitle.RecordCount+1;
            SrtSubTitle.CurrentTimeFrom:=CurrentTimeFrom;
            SrtSubTitle.CurrentTimeTo:=CurrentTimeTo;
            SrtSubTitle.CurrentContent:=CurrentContent;

            SrtSubTitle.Post;
            Next;
        end;
    end;
    SrtSubTitle.EnableControls;

    importFile.Free;
end;

end.
