unit SrtActionCommander;

interface
uses SubTitleFile,SrtSubTitleFile,SrtSubTitleDataTable,SrtTimeExecuter,SrtTimeLocateExecuter,
    SrtResetNosExecuter,
    SrtAddRecordExecuter,
    SrtFilterExecuter,SrtFindReplaceExecuter,
    SrtAffixAddExecuter,SrtAffixRemoveExecuter,SrtAffixStringDeleteExecuter,
    SrtTrimExecuter,SrtDeleteEmptyExecuter,
    SrtTimeMoveExecuterBase,SrtTimeMoveExecuter,SrtTimeFromResizeExecuter,SrtTimeToResizeExecuter,SrtTimeScaleExecuter,
    SrtMoveToPlayerTimeExecuter,
    SrtCombineExecuter,SrtSplitExecuter,
    SrtFileImportExecuter,SrtTextImportExecuter,SrtFromSsaFileImportExecuter,SrtFileExportExecuter,SrtToSsaFileExportExecuter,SrtFileMergeExecuterBase,SrtFileSequenceMergeExecuter,SrtFileNoMergeExecuter,SrtFileTimeFromMergeExecuter,
    SrtTimeManager,
    SrtHistoryManager,
    FileTypeRecognizer,SrtFromSsaSubTitleFile,SrtToSsaSubTitleFile,
    CommonDefs,
    SingleTableActionCommander,DataTable,DB,SysUtils,Generics.Collections;

type TSrtActionCommander=class(TSingleTableActionCommander)
  private
    timeExecuter:TSrtTimeExecuter;
    timeLocateExecuter:TSrtTimeLocateExecuter;

    FSrtSubTitleFile:TSrtSubTitleFile;
    FHistoryManager:TSrtHistoryManager;

    ssaFile:TSrtFromSsaSubTitleFile;
    toSsaFile:TSrtToSsaSubTitleFile;
    fileRecognizer:TFileTypeRecognizer;
    function getSrtSubTitle: TSrtSubTitleDataTable;
  protected
    function getSubTitleFile: TSubTitleFile;override;
    function getSubTitle: TDataTable;override;
  public
    constructor Create;
    destructor Destroy;override;

    procedure ExecuteNewFile;override;
    procedure ExecuteOpenFile(const openFileName:string);override;
    procedure ExecuteSaveFile;override;
    procedure ExecuteSaveFileAs(const saveFileName:string);override;
    procedure ExecuteAddCurrentToHistory(const description:string);overload;
    procedure ExecuteAddCurrentToHistory(const description:string;const changedRecNo:integer);overload;
    procedure ExecuteGoToCurrentHistoryStep;
    procedure ExecuteGoToPrevHistoryStep;
    procedure ExecuteGoToNextHistoryStep;
    procedure ExecuteClearHistory;override;

    procedure ExecuteAppendAfterCurrent;
    procedure ExecuteAppendMultiAfterCurrent(const recordNumber:Integer);
    procedure ExecuteInsertBeforeCurrent;
    procedure ExecuteInsertMultiBeforeCurrent(const recordNumber:Integer);

    procedure ExecuteFilter(const mode:FilterMode;const caseSensitive:boolean;const findText:string);
    procedure ExecuteClearFilter;
    function ExecuteFindFirst(const mode:FindMode;const caseSensitive:boolean;const findText:string):boolean;
    function ExecuteFindLast(const mode:FindMode;const caseSensitive:boolean;const findText:string):boolean;
    function ExecuteFindPrior(const mode:FindMode;const caseSensitive:boolean;const findText:string):boolean;
    function ExecuteFindNext(const mode:FindMode;const caseSensitive:boolean;const findText:string):boolean;
    function ExecuteReplaceCurrent(const mode:FindMode;const caseSensitive:boolean;const findText:string;const replaceText:string):boolean;
    function ExecuteReplaceAll(const mode:FindMode;const caseSensitive:boolean;const findText:string;const replaceText:string):boolean;

    procedure ExecuteAddAffix(const prefix,postfix:string;const recNos:TList<integer>);
    procedure ExecuteRemoveAffix(const prefix,postfix:string;const caseSensitive:boolean;const recNos:TList<integer>);
    procedure ExecuteDeleteAffix(const delPrefixLength,delPostfixLength:integer;const recNos:TList<integer>);
    procedure ExecuteTrimWhiteSpace(const trimLeft,trimRight,trimEmptyLine:Boolean;const recNos:TList<integer>);
    procedure ExecuteDeleteEmptySubTitle(const includeWhiteSpaceLine:boolean;const recNos:TList<integer>);

    procedure ExecuteResetNos;
    function ExecuteGetCurrentTimeFromSeconds:Double;
    procedure ExecuteLocateTime(const refSeconds:Double);
    function ExecuteGetTimeString(const timeSeconds:double):string;
    function ExecuteGetDisplayingSubtitle(const refSeconds:Double):string;

    procedure ExecuteMoveTime(
        const direction:TSrtTimeMoveDirection;
        const offsettime:TSrtTimeManager;
        const recNos:TList<integer>
    );
    procedure ExecuteResizeTimeFrom(
        const direction:TSrtTimeMoveDirection;
        const offsettime:TSrtTimeManager;
        const recNos:TList<integer>
    );
    procedure ExecuteResizeTimeTo(
        const direction:TSrtTimeMoveDirection;
        const offsettime:TSrtTimeManager;
        const recNos:TList<integer>
    );
    procedure ExecuteScaleTime(
        const newTimeMin:TSrtTimeManager;
        const newTimeMax:TSrtTimeManager;
        const recNos:TList<integer>
    );

    procedure ExecuteMoveToPlayerTime(const currentPlayerSeconds:Double;const recNos:TList<Integer>);

    procedure ExecuteCombineSingleLine(const recNos:TList<Integer>);
    procedure ExecuteCombineMultiLine(const recNos:TList<Integer>);
    procedure ExecuteSplitCurrent(const splited: TList<string>);


    procedure ExecuteImportFile(const filename:string);
    procedure ExecuteImportText(const text,separator:string);
    procedure ExecuteExportFile(const filename:string;const recNos:TList<integer>);
    procedure ExecuteMergeBySequence(const timeMergeMode:TSrtTimeMergeMode;const separator:string;const mergePosition:MergePosition; const importUnmatched:boolean;const importFileName:string);
    procedure ExecuteMergeByNo(const timeMergeMode:TSrtTimeMergeMode;const separator:string;const mergePosition:MergePosition; const importUnmatched:boolean;const importFileName:string);
    procedure ExecuteMergeByTimeFrom(const timeMergeMode:TSrtTimeMergeMode;const separator:string;const mergePosition:MergePosition; const importUnmatched:boolean;const importFileName:string);

    property SrtSubFile:TSrtSubTitleFile read FSrtSubTitleFile write FSrtSubTitleFile;
    property SrtSubTitle:TSrtSubTitleDataTable read getSrtSubTitle;
    property HistoryManager:TSrtHistoryManager read FHistoryManager write FHistoryManager;
end;

implementation

{ TSrtActionCommander }

{$REGION '构造与析构'}
    constructor TSrtActionCommander.Create;
    begin
        inherited;
        SrtSubFile:=TSrtSubTitleFile.Create;
        timeExecuter:=TSrtTimeExecuter.Create(self.SrtSubTitle);
        timeLocateExecuter:=TSrtTimeLocateExecuter.Create(self.SrtSubTitle);
        FHistoryManager:=TSrtHistoryManager.Create;
        ssaFile:=TSrtFromSsaSubTitleFile.Create;
        toSsaFile:=TSrtToSsaSubTitleFile.Create;
        fileRecognizer:=TFileTypeRecognizer.Create;
    end;

    destructor TSrtActionCommander.Destroy;
    begin
        timeExecuter.Free;
        timeLocateExecuter.Free;
        SrtSubFile.Free;
        FHistoryManager.Free;
        ssaFile.Free;
        toSsaFile.Free;
        fileRecognizer.Free;
        inherited;
    end;

{$ENDREGION}

{$REGION '用于属性'}
    function TSrtActionCommander.getSubTitleFile: TSubTitleFile;
    begin
        exit(SrtSubFile);
    end;

    function TSrtActionCommander.getSrtSubTitle: TSrtSubTitleDataTable;
    begin
        exit(SrtSubFile.SrtData.SrtSubTitle);
    end;

    function TSrtActionCommander.getSubTitle: TDataTable;
    begin
        exit(SrtSubFile.Data.SubTitle);
    end;
{$ENDREGION}

{$REGION '操作历史'}
    procedure TSrtActionCommander.ExecuteAddCurrentToHistory(const description:string);
    begin
        if not IsHistorySaved then begin
            ExecuteAddCurrentToHistory(description,SrtSubtitle.RecNo);
        end;
    end;

    procedure TSrtActionCommander.ExecuteAddCurrentToHistory(const description: string; const changedRecNo: integer);
    begin
        if not IsHistorySaved then begin
            IsHistorySaved:=true;
            HistoryManager.Add(description,changedRecNo,SrtSubTitle);
            timeLocateExecuter.SrtSubTitle:=HistoryManager.HistorySubTitles.Last;
        end else begin
        end;
    end;

    procedure TSrtActionCommander.ExecuteGoToCurrentHistoryStep;
    begin
        if HistoryManager.GoToCurrentStep(SrtSubTitle) then begin
            timeLocateExecuter.SrtSubTitle:=HistoryManager.CurrentSubTitle;
            IsModified:=true;
        end;
    end;

    procedure TSrtActionCommander.ExecuteGoToPrevHistoryStep;
    begin
        if HistoryManager.GoToPrevStep(SrtSubTitle) then begin
            timeLocateExecuter.SrtSubTitle:=HistoryManager.CurrentSubTitle;
            IsModified:=true;
        end;
    end;

    procedure TSrtActionCommander.ExecuteGoToNextHistoryStep;
    begin
        if HistoryManager.GoToNextStep(SrtSubTitle) then begin
            timeLocateExecuter.SrtSubTitle:=HistoryManager.CurrentSubTitle;
            IsModified:=true;
        end;
    end;

    procedure TSrtActionCommander.ExecuteClearHistory;
    begin
        HistoryManager.ClearSteps;
        timeLocateExecuter.SrtSubTitle:=self.SrtSubTitle;
    end;
{$ENDREGION}

{$REGION '文件操作'}
    procedure TSrtActionCommander.ExecuteNewFile;
    begin
        inherited;
        self.ExecuteAddCurrentToHistory('新建');
    end;

    procedure TSrtActionCommander.ExecuteOpenFile(const openFileName: string);
    begin
        fileRecognizer.FileName:=openFileName;
        if fileRecognizer.IsSrtFile then begin
            inherited;
        end else if fileRecognizer.IsSsaOrAssFile then begin
            inherited ExecuteNewFile;
            ssaFile.LoadFromFile(openFileName);
            ssaFile.Data.DisableControls;
            ssaFile.Data.DisableIndexFieldNames;

            SrtSubTitle.DisableControls;
            SrtSubTitle.Clear;
            SrtSubTitle.CopyFrom(ssaFile.SrtData.SrtSubTitle);
            SrtSubTitle.EnableControls;
            SrtSubFile.FileName:=openFileName;

            ssaFile.Data.EnableIndexFieldNames;
            ssaFile.Data.EnableControls;
        end else begin
            inherited;
        end;

        self.ExecuteAddCurrentToHistory('打开');
    end;

    procedure TSrtActionCommander.ExecuteSaveFile;
    begin
        with SrtSubTitle do begin
            DisableControls;
            RememberPosition;
            BackupFilter;
            ClearFilter;

            ExecuteResetNos;
            inherited;

            RestoreFilter;
            ReturnRememberedPosition;
            EnableControls;
        end;
    end;

    procedure TSrtActionCommander.ExecuteSaveFileAs(const saveFileName: string);
    begin
        with SrtSubTitle do begin
            DisableControls;
            RememberPosition;
            BackupFilter;
            ClearFilter;

            ExecuteResetNos;
            inherited;

            RestoreFilter;
            ReturnRememberedPosition;
            EnableControls;
        end;
    end;

{$ENDREGION}

{$REGION '编辑'}
    procedure TSrtActionCommander.ExecuteAppendAfterCurrent;
    var
        executer:TSrtAddRecordExecuter;
    begin
        executer:=TSrtAddRecordExecuter.Create(self.SrtSubTitle);
        executer.AppendAfterCurrent;
        executer.Free;

        self.ExecuteAddCurrentToHistory('新增记录(后置)');
    end;

    procedure TSrtActionCommander.ExecuteAppendMultiAfterCurrent(const recordNumber: Integer);
    var
        executer:TSrtAddRecordExecuter;
    begin
        executer:=TSrtAddRecordExecuter.Create(self.SrtSubTitle);
        executer.AppendMultiAfterCurrent(recordNumber);
        executer.Free;

        self.ExecuteAddCurrentToHistory('新增多条记录(后置)');
    end;

    procedure TSrtActionCommander.ExecuteInsertBeforeCurrent;
    var
        executer:TSrtAddRecordExecuter;
    begin
        executer:=TSrtAddRecordExecuter.Create(self.SrtSubTitle);
        executer.InsertBeforeCurrent;
        executer.Free;

        self.ExecuteAddCurrentToHistory('新增记录(前置)');
    end;

    procedure TSrtActionCommander.ExecuteInsertMultiBeforeCurrent(const recordNumber: Integer);
    var
        executer:TSrtAddRecordExecuter;
    begin
        executer:=TSrtAddRecordExecuter.Create(self.SrtSubTitle);
        executer.InsertMultiBeforeCurrent(recordNumber);
        executer.Free;

        self.ExecuteAddCurrentToHistory('新增多条记录(前置)');
    end;
{$ENDREGION}

{$REGION '查找替换'}
    procedure TSrtActionCommander.ExecuteFilter(const mode: FilterMode;const caseSensitive:boolean; const findText: string);
    var
        executer:TFilterExecuter;
    begin
        if findText<>'' then begin
            executer:=TFilterExecuter.Create(self.SrtSubTitle);
            executer.Mode:=mode;
            executer.CaseSensitive:=caseSensitive;
            executer.FindText:=findText;
            executer.FilterSubTitle;
            executer.Free;
        end else begin
            ExecuteClearFilter;
        end;
    end;

    procedure TSrtActionCommander.ExecuteClearFilter;
    begin
        with SrtSubTitle do begin
            RememberBookmark;
            ClearFilter;
            ReturnRememberedBookmark;
        end;
    end;

    function TSrtActionCommander.ExecuteFindFirst(const mode: FindMode;const caseSensitive:boolean; const findText: string):boolean;
    var
        executer:TFindReplaceExecuter;
    begin
        executer:=TFindReplaceExecuter.Create(self.SrtSubTitle);
        executer.Mode:=mode;
        executer.CaseSensitive:=caseSensitive;
        executer.FindText:=findText;
        Result:=executer.FindFirst;
        executer.Free;
    end;

    function TSrtActionCommander.ExecuteFindLast(const mode: FindMode;const caseSensitive:boolean; const findText: string):boolean;
    var
        executer:TFindReplaceExecuter;
    begin
        executer:=TFindReplaceExecuter.Create(self.SrtSubTitle);
        executer.Mode:=mode;
        executer.CaseSensitive:=caseSensitive;
        executer.FindText:=findText;
        Result:=executer.FindLast;
        executer.Free;
    end;

    function TSrtActionCommander.ExecuteFindPrior(const mode: FindMode; const caseSensitive:boolean;const findText: string):boolean;
    var
        executer:TFindReplaceExecuter;
    begin
        executer:=TFindReplaceExecuter.Create(self.SrtSubTitle);
        executer.Mode:=mode;
        executer.CaseSensitive:=caseSensitive;
        executer.FindText:=findText;
        Result:=executer.FindPrior;
        executer.Free;
    end;

    function TSrtActionCommander.ExecuteFindNext(const mode: FindMode; const caseSensitive:boolean;const findText: string):boolean;
    var
        executer:TFindReplaceExecuter;
    begin
        executer:=TFindReplaceExecuter.Create(self.SrtSubTitle);
        executer.Mode:=mode;
        executer.CaseSensitive:=caseSensitive;
        executer.FindText:=findText;
        Result:=executer.FindNext;
        executer.Free;
    end;

    function TSrtActionCommander.ExecuteReplaceCurrent(const mode: FindMode; const caseSensitive: boolean; const findText, replaceText: string): boolean;
    var
        executer:TFindReplaceExecuter;
    begin
        executer:=TFindReplaceExecuter.Create(self.SrtSubTitle);
        executer.Mode:=mode;
        executer.CaseSensitive:=caseSensitive;
        executer.FindText:=findText;
        Result:=executer.ReplaceCurrent(replaceText);
        executer.Free;

        self.ExecuteAddCurrentToHistory('替换一条');
    end;

    function TSrtActionCommander.ExecuteReplaceAll(const mode: FindMode; const caseSensitive: boolean; const findText, replaceText: string): boolean;
    var
        executer:TFindReplaceExecuter;
    begin
        executer:=TFindReplaceExecuter.Create(self.SrtSubTitle);
        executer.Mode:=mode;
        executer.CaseSensitive:=caseSensitive;
        executer.FindText:=findText;
        Result:=executer.ReplaceAll(replaceText);
        executer.Free;

        self.ExecuteAddCurrentToHistory('替换全部');
    end;

{$ENDREGION}

{$REGION '调整'}
    procedure TSrtActionCommander.ExecuteResetNos;
    var
        executer:TResetNosExecuter;
    begin
        executer:=TResetNosExecuter.Create(self.SrtSubTitle);
        executer.ResetNos;
        executer.Free;

        self.ExecuteAddCurrentToHistory('整理序号');
    end;

    procedure TSrtActionCommander.ExecuteMoveTime(
        const direction:TSrtTimeMoveDirection;
        const offsettime:TSrtTimeManager;
        const recNos:TList<integer>
    );
    var
        executer:TSrtTimeMoveExecuter;
    begin
        executer:=TSrtTimeMoveExecuter.Create(self.SrtSubTitle);
        executer.Direction:=direction;
        executer.OffsetTime:=offsetTime;
        executer.RecNos:=recNos;
        executer.MoveTime;
        executer.Free;

        self.ExecuteAddCurrentToHistory('平移时间');
    end;

    procedure TSrtActionCommander.ExecuteResizeTimeFrom(
        const direction: TSrtTimeMoveDirection;
        const offsettime: TSrtTimeManager;
        const recNos: TList<integer>
    );
    var
        executer:TSrtTimeFromResizeExecuter;
    begin
        executer:=TSrtTimeFromResizeExecuter.Create(self.SrtSubTitle);
        executer.Direction:=direction;
        executer.OffsetTime:=offsetTime;
        executer.RecNos:=recNos;
        executer.MoveTime;
        executer.Free;

        self.ExecuteAddCurrentToHistory('伸缩起始时间');
    end;

    procedure TSrtActionCommander.ExecuteResizeTimeTo(
        const direction: TSrtTimeMoveDirection;
        const offsettime: TSrtTimeManager;
        const recNos: TList<integer>
    );
    var
        executer:TSrtTimeToResizeExecuter;
    begin
        executer:=TSrtTimeToResizeExecuter.Create(self.SrtSubTitle);
        executer.Direction:=direction;
        executer.OffsetTime:=offsetTime;
        executer.RecNos:=recNos;
        executer.MoveTime;
        executer.Free;

        self.ExecuteAddCurrentToHistory('伸缩结束时间');
    end;

    procedure TSrtActionCommander.ExecuteScaleTime(
        const newTimeMin, newTimeMax: TSrtTimeManager;
        const recNos: TList<integer>
    );
    var
        executer:TSrtTimeScaleExecuter;
    begin
        executer:=TSrtTimeScaleExecuter.Create(self.SrtSubTitle);
        executer.NewTimeMin:=newTimeMin;
        executer.NewTimeMax:=newTimeMax;
        executer.RecNos:=recNos;
        executer.ScaleTime;
        executer.Free;

        self.ExecuteAddCurrentToHistory('时间轴缩放');
    end;

    procedure TSrtActionCommander.ExecuteMoveToPlayerTime(const currentPlayerSeconds: Double; const recNos: TList<Integer>);
    var
        executer:TSrtMoveToPlayerTimeExecuter;
    begin
        if recNos.Count>0 then begin
            executer:=TSrtMoveToPlayerTimeExecuter.Create(Self.SrtSubTitle);
            executer.CurrentPlayerSeconds:=currentPlayerSeconds;
            executer.RecNos:=recNos;
            executer.MoveToPlayerTime;
            executer.Free;

            self.ExecuteAddCurrentToHistory('按播放器时间平移');
        end;
    end;
{$ENDREGION}

{$REGION '组合拆分'}
    procedure TSrtActionCommander.ExecuteCombineSingleLine(const recNos: TList<Integer>);
    var
        executer:TSrtCombineExecuter;
    begin
        executer:=TSrtCombineExecuter.Create(self.SrtSubTitle);
        executer.RecNos:=recNos;
        executer.CombineSingleLine;
        executer.Free;

        self.ExecuteAddCurrentToHistory('组合为单行');
    end;

    procedure TSrtActionCommander.ExecuteCombineMultiLine(const recNos: TList<Integer>);
    var
        executer:TSrtCombineExecuter;
    begin
        executer:=TSrtCombineExecuter.Create(self.SrtSubTitle);
        executer.RecNos:=recNos;
        executer.CombineMultiLine;
        executer.Free;

        self.ExecuteAddCurrentToHistory('组合为多行');
    end;

    procedure TSrtActionCommander.ExecuteSplitCurrent(const splited: TList<string>);
    var
        executer:TSrtSplitExecuter;
    begin
        if splited.Count>1 then begin
            executer:=TSrtSplitExecuter.Create(self.SrtSubTitle);
            executer.SplitedSubs:=splited;
            executer.SplitCurrent;
            executer.Free;

            self.ExecuteAddCurrentToHistory('拆分字幕');
        end;
    end;

{$ENDREGION}

{$REGION '时间同步'}
    function TSrtActionCommander.ExecuteGetCurrentTimeFromSeconds: Double;
    begin
        exit(timeExecuter.GetCurrentTimeFromSeconds);
    end;

    procedure TSrtActionCommander.ExecuteLocateTime(const refSeconds: Double);
    begin
        timeLocateExecuter.LocateTime(refSeconds);
        if
        (self.SrtSubTitle.RecNo<>timeLocateExecuter.SrtSubTitle.RecNo)
        and
        (self.SrtSubTitle.RecordCount>=timeLocateExecuter.SrtSubTitle.RecNo)
        and
        (timeLocateExecuter.SrtSubTitle.RecNo<>0)
        then begin
            self.SrtSubTitle.RecNo:=timeLocateExecuter.SrtSubTitle.RecNo;
        end;
    end;

    function TSrtActionCommander.ExecuteGetTimeString(const timeSeconds: double): string;
    begin
        exit(timeExecuter.GetTimeString(timeSeconds));
    end;

    function TSrtActionCommander.ExecuteGetDisplayingSubtitle(const refSeconds: Double):string;
    begin
        exit(timeLocateExecuter.GetDisplayingSubtitle(refSeconds));
    end;

{$ENDREGION}

{$REGION '文本处理'}
    procedure TSrtActionCommander.ExecuteAddAffix(const prefix, postfix: string;
      const recNos: TList<integer>);
    var
        executer:TSrtAffixAddExecuter;
    begin
        executer:=TSrtAffixAddExecuter.Create(self.SrtSubTitle);
        executer.Prefix:=prefix;
        executer.Postfix:=postfix;
        executer.RecNos:=recNos;
        executer.AddAffix;
        executer.Free;

        self.ExecuteAddCurrentToHistory('添加前缀后缀');
    end;

    procedure TSrtActionCommander.ExecuteRemoveAffix(const prefix, postfix: string;
      const caseSensitive: boolean; const recNos: TList<integer>);
    var
        executer:TSrtAffixRemoveExecuter;
    begin
        executer:=TSrtAffixRemoveExecuter.Create(self.SrtSubTitle);
        executer.Prefix:=prefix;
        executer.Postfix:=postfix;
        executer.CaseSensitive:=caseSensitive;
        executer.RecNos:=recNos;
        executer.RemoveAffix;
        executer.Free;

        self.ExecuteAddCurrentToHistory('删除前缀后缀');
    end;

    procedure TSrtActionCommander.ExecuteDeleteAffix(const delPrefixLength,
      delPostfixLength: integer; const recNos: TList<integer>);
    var
        executer:TSrtAffixStringDeleteExecuter;
    begin
        executer:=TSrtAffixStringDeleteExecuter.Create(self.SrtSubTitle);
        executer.DeletePrefixLength:=delPrefixLength;
        executer.DeletePostfixLength:=delPostfixLength;
        executer.RecNos:=recNos;
        executer.DeleteAffix;
        executer.Free;

        self.ExecuteAddCurrentToHistory('删除前后n个字符');
    end;

    procedure TSrtActionCommander.ExecuteTrimWhiteSpace(const trimLeft, trimRight, trimEmptyLine: Boolean;const recNos:TList<integer>);
    var
        executer:TSrtTrimExecuter;
    begin
        executer:=TSrtTrimExecuter.Create(self.SrtSubTitle);
        executer.TrimLeft:=trimLeft;
        executer.TrimRight:=trimRight;
        executer.TrimEmptyLine:=trimEmptyLine;
        executer.RecNos:=recNos;
        executer.Trim;
        executer.Free;

        self.ExecuteAddCurrentToHistory('清除空白文本');
    end;

    procedure TSrtActionCommander.ExecuteDeleteEmptySubTitle(const includeWhiteSpaceLine: boolean;const recNos:TList<integer>);
    var
        executer:TSrtDeleteEmptyExecuter;
    begin
        executer:=TSrtDeleteEmptyExecuter.Create(self.SrtSubTitle);
        executer.IncludeWhiteSpace:=includeWhiteSpaceLine;
        executer.RecNos:=recNos;
        executer.DeleteEmptySubTitle;
        executer.Free;
        self.ExecuteAddCurrentToHistory('删除空字幕');
    end;

{$ENDREGION}

{$REGION '导入导出'}
    procedure TSrtActionCommander.ExecuteImportFile(const filename: string);
    var
        executerSrt:TSrtFileImportExecuter;
        executerSsa:TSrtFromSsaFileImportExecuter;
    begin
        fileRecognizer.FileName:=filename;

        if fileRecognizer.IsSrtFile then begin
            executerSrt:=TSrtFileImportExecuter.Create(self.SrtSubTitle);
            executerSrt.ImportFromFile(filename);
            executerSrt.Free;
        end else if fileRecognizer.IsSsaOrAssFile then begin
            executerSsa:=TSrtFromSsaFileImportExecuter.Create(self.SrtSubTitle);
            executerSsa.ImportFromFile(filename);
            executerSsa.Free;
        end else begin
            executerSrt:=TSrtFileImportExecuter.Create(self.SrtSubTitle);
            executerSrt.ImportFromFile(filename);
            executerSrt.Free;
        end;

        self.ExecuteAddCurrentToHistory('导入');
    end;

    procedure TSrtActionCommander.ExecuteImportText(const text, separator: string);
    var
        executer:TSrtTextImportExecuter;
    begin
        executer:=TSrtTextImportExecuter.Create(self.SrtSubTitle);
        executer.Text:=text;
        executer.Separator:=separator;
        executer.ImportText;
        executer.Free;

        self.ExecuteAddCurrentToHistory('导入文本');
    end;

    procedure TSrtActionCommander.ExecuteExportFile(
        const filename: string;
        const recNos: TList<integer>
    );
    var
        srtExecuter:TSrtFileExportExecuter;
        ssaExecuter:TSrtToSsaFileExportExecuter;
    begin

        fileRecognizer.FileName:=filename;

        if fileRecognizer.IsSrtFile then begin
            srtExecuter:=TSrtFileExportExecuter.Create(self.SrtSubTitle);
            srtExecuter.FileName:=filename;
            srtExecuter.RecNos:=recNos;
            srtExecuter.ExportToFile;
            srtExecuter.Free;
        end else if fileRecognizer.IsSsaOrAssFile then begin
            ssaExecuter:=TSrtToSsaFileExportExecuter.Create(self.SrtSubTitle);
            ssaExecuter.FileName:=filename;
            ssaExecuter.RecNos:=recNos;
            ssaExecuter.ExportToFile;
            ssaExecuter.Free;
        end else begin
            srtExecuter:=TSrtFileExportExecuter.Create(self.SrtSubTitle);
            srtExecuter.FileName:=filename;
            srtExecuter.RecNos:=recNos;
            srtExecuter.ExportToFile;
            srtExecuter.Free;
        end;


        self.ExecuteAddCurrentToHistory('导出');
    end;

    procedure TSrtActionCommander.ExecuteMergeBySequence(const timeMergeMode: TSrtTimeMergeMode; const separator: string; const mergePosition:MergePosition; const importUnmatched: boolean;const importFileName: string);
    var
        executer:TFileSequenceMergeExecuter;
    begin
        executer:=TFileSequenceMergeExecuter.Create(self.SrtSubTitle);
        executer.TimeMergeMode:=timeMergeMode;
        executer.Separator:=separator;
        executer.MergePosition:=mergePosition;
        executer.ImportUnmatched:=importUnmatched;
        executer.ImportFileName:=importFileName;
        executer.Merge;
        executer.Free;

        self.ExecuteAddCurrentToHistory('按顺序合成');
    end;

    procedure TSrtActionCommander.ExecuteMergeByNo(const timeMergeMode: TSrtTimeMergeMode; const separator: string; const mergePosition:MergePosition; const importUnmatched: boolean;const importFileName: string);
    var
        executer:TFileNoMergeExecuter;
    begin
        executer:=TFileNoMergeExecuter.Create(self.SrtSubTitle);
        executer.TimeMergeMode:=timeMergeMode;
        executer.Separator:=separator;
        executer.MergePosition:=mergePosition;
        executer.ImportUnmatched:=importUnmatched;
        executer.ImportFileName:=importFileName;
        executer.Merge;
        executer.Free;

        self.ExecuteAddCurrentToHistory('按字幕序号合成');
    end;

    procedure TSrtActionCommander.ExecuteMergeByTimeFrom(const timeMergeMode: TSrtTimeMergeMode; const separator: string; const mergePosition:MergePosition; const importUnmatched: boolean;const importFileName: string);
    var
        executer:TFileTimeFromMergeExecuter;
    begin
        executer:=TFileTimeFromMergeExecuter.Create(self.SrtSubTitle);
        executer.TimeMergeMode:=timeMergeMode;
        executer.Separator:=separator;
        executer.MergePosition:=mergePosition;
        executer.ImportUnmatched:=importUnmatched;
        executer.ImportFileName:=importFileName;
        executer.Merge;
        executer.Free;

        self.ExecuteAddCurrentToHistory('按起始时间合成');
    end;

{$ENDREGION}


end.
