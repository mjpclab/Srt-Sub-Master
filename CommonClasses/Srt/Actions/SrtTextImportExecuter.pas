unit SrtTextImportExecuter;

interface
uses SrtActionExecuter;

type TSrtTextImportExecuter=class(TSrtActionExecuter)
  private
    FText:string;
    FSeparator:string;

    procedure DoImportText;
  public
    property Text:string read FText write FText;
    property Separator:string read FSeparator write FSeparator;

    procedure ImportText;
end;

implementation
uses StrUtils;
{ TSrtTextImportExecuter }

procedure TSrtTextImportExecuter.ImportText;
begin
    with SrtSubTitle do begin
        DisableControls;

        DoImportText;

        EnableControls;
    end;
end;

procedure TSrtTextImportExecuter.DoImportText;
var
    charPos:integer;
    textLength:Integer;
    separatorLength:Integer;
    nextSeparatorPos:integer;
    currentSub:string;
begin
    charPos:=1;
    textLength:=length(self.Text);
    separatorLength:=length(self.Separator);

    while charPos<=textLength do begin
        nextSeparatorPos:=PosEx(Separator,self.Text,charPos);
        if nextSeparatorPos>charPos then begin                //当前位置以后是正文，接着才是separator
            currentSub:=MidStr(self.Text,charPos,nextSeparatorPos-charPos);
            SrtSubTitle.Append;
            srtsubtitle.CurrentContent:=currentSub;
            srtsubtitle.Post;

            charPos:=nextSeparatorPos+separatorLength;
        end else if nextSeparatorPos=charPos then begin       //当前位置就是separator
            SrtSubTitle.Append;
            srtsubtitle.CurrentContent:='';
            srtsubtitle.Post;

            charPos:=nextSeparatorPos+separatorLength;
        end else begin                                  //当前位置仅剩正文
            currentSub:=MidStr(self.Text,charPos,textLength-charPos+1);
            SrtSubTitle.Append;
            srtsubtitle.CurrentContent:=currentSub;
            srtsubtitle.Post;

            charPos:=textLength+1;
        end;

    end;

end;

end.
