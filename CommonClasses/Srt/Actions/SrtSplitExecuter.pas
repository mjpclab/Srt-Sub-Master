unit SrtSplitExecuter;

interface
uses SrtActionExecuter,Generics.Collections;

type TSrtSplitExecuter=class(TSrtActionExecuter)
  private
    FSplitedSubs:TList<string>;

    procedure doSplitCurrent;
  public
    property SplitedSubs:TList<string> read FSplitedSubs write FSplitedSubs;

    procedure SplitCurrent;
end;

implementation
uses SrtTimeManager;

{ TSrtSplitExecuter }

procedure TSrtSplitExecuter.SplitCurrent;
begin
    with SrtSubTitle do begin
        DisableControls;
        doSplitCurrent;
        EnableControls;
    end;
end;

procedure TSrtSplitExecuter.doSplitCurrent;
var
    timeMan1,timeMan2:TSrtTimeManager;
    totalChars,currentTotalChars:integer;
    totalMilliSeconds:integer;
    iString:string;
begin
    timeMan1:=TSrtTimeManager.Create;
    timeMan2:=TSrtTimeManager.Create;

    //计算总字数
    totalChars:=0;
    for iString in FSplitedSubs do begin
        totalChars:=totalChars+Length(iString);
    end;

    //计算总时长(ms)
    timeMan1.Parse(SrtSubTitle.CurrentTimeFrom);
    timeMan2.Parse(SrtSubTitle.CurrentTimeTo);
    totalMilliSeconds:=timeMan2.TotalMilliSeconds-timeMan1.TotalMilliSeconds;

    //删原字幕
    SrtSubTitle.Delete;

    //逐一添加新字幕
    currentTotalChars:=0;
    with SrtSubTitle do begin
        for iString in FSplitedSubs do begin
            Append;

            timeMan2.TotalMilliSeconds:=timeMan1.TotalMilliSeconds + Trunc(currentTotalchars/totalChars*totalMilliSeconds);
            if currentTotalChars<>0 then timeMan2.AddMilliSeconds(1);
            CurrentTimeFrom:=timeMan2.ToString;

            currentTotalChars:=currentTotalChars+Length(iString);
            if currentTotalChars<>totalChars then begin
                timeMan2.TotalMilliSeconds:=timeMan1.TotalMilliSeconds + Trunc(currentTotalchars/totalChars*totalMilliSeconds);
            end else begin
                timeMan2.TotalMilliSeconds:=timeMan1.TotalMilliSeconds+totalMilliSeconds;
            end;
            CurrentTimeTo:=timeMan2.ToString;

            CurrentContent:=iString;
            Post;
        end;
    end;

    timeMan1.Free;
    timeMan2.Free;
end;

end.
