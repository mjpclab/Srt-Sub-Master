unit SrtAddRecordExecuter;

interface
uses SrtActionExecuter;

type TSrtAddRecordExecuter=class(TSrtActionExecuter)
  public
    procedure AppendAfterCurrent;
    procedure AppendMultiAfterCurrent(const recordNumber:Integer);
    procedure InsertBeforeCurrent;
    procedure InsertMultiBeforeCurrent(const recordNumber:Integer);
end;

implementation
uses SrtTimeManager;

{ TSrtAddRecordExecuter }

procedure TSrtAddRecordExecuter.AppendAfterCurrent;
begin
    AppendMultiAfterCurrent(1);
end;

procedure TSrtAddRecordExecuter.AppendMultiAfterCurrent(const recordNumber: Integer);
var
    timeMan:TSrtTimeManager;
    i:integer;
begin
    timeMan:=TSrtTimeManager.Create;
    with SrtSubTitle do begin
        DisableControls;

        if Eof=false then begin
            timeMan.Parse(CurrentTimeTo);
            //timeMan.AddMilliSeconds(1);
        end;

        for i := 1 to recordNumber do begin
            Append;
            CurrentTimeFrom:=timeMan.ToString;
            CurrentTimeTo:=timeMan.ToString;
            Post;
        end;

        EnableControls;
    end;
    timeMan.Free;
end;

procedure TSrtAddRecordExecuter.InsertBeforeCurrent;
begin
    InsertMultiBeforeCurrent(1);
end;

procedure TSrtAddRecordExecuter.InsertMultiBeforeCurrent(const recordNumber: Integer);
var
    timeMan:TSrtTimeManager;
    i:integer;
begin
    timeMan:=TSrtTimeManager.Create;
    with SrtSubTitle do begin
        DisableControls;

        if Eof=false then begin
            timeMan.Parse(CurrentTimeFrom);
            //timeMan.AddMilliSeconds(-1);
        end;

        for i := 1 to recordNumber do begin
            Insert;
            CurrentTimeFrom:=timeMan.ToString;
            CurrentTimeTo:=timeMan.ToString;
            Post;
        end;

        EnableControls;
    end;
    timeMan.Free;
end;

end.
