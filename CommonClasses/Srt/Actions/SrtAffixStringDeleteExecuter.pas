unit SrtAffixStringDeleteExecuter;

interface
uses SrtActionExecuter,Generics.Collections;

type TSrtAffixStringDeleteExecuter=class(TSrtActionExecuter)
  private
    FRecNos:TList<integer>;
    FDeletePrefixLength:integer;
    FDeletePostfixLength:integer;

    procedure DoDeleteAffix;
  public
    procedure DeleteAffix;

    property RecNos:TList<integer> read FRecNos write FRecNos;
    property DeletePrefixLength:integer read FDeletePrefixLength write FDeletePrefixLength;
    property DeletePostfixLength:integer read FDeletePostfixLength write FDeletePostfixLength;
end;

implementation
uses StrUtils;
{ TAffixStringDeleteExecuter }

procedure TSrtAffixStringDeleteExecuter.DeleteAffix;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        DoDeleteAffix;

        ReturnRememberedPosition;
        EnableControls;
    end;
end;

procedure TSrtAffixStringDeleteExecuter.DoDeleteAffix;
var
    aRecNo:integer;
    buffer:string;
begin
    for aRecNo in RecNos do begin
        with SrtSubTitle do begin
            RecNo:=aRecNo;
            Edit;

            buffer:=CurrentContent;
            buffer:=RightStr(buffer,Length(buffer)-DeletePrefixLength);
            buffer:=LeftStr(buffer,Length(buffer)-DeletePostfixLength);
            CurrentContent:=buffer;

            Post;
        end;
    end;
end;

end.
