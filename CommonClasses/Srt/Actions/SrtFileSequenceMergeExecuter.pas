unit SrtFileSequenceMergeExecuter;

interface
uses SrtFileMergeExecuterBase;

type TFileSequenceMergeExecuter=class(TFileMergeExecuterBase)
  protected
    procedure DoMerge;override;
end;

implementation

{ TFileSequenceMergeExecuter }

procedure TFileSequenceMergeExecuter.DoMerge;
begin
    SrtSubTitle.First;
    ImportSubTitle.First;

    while (SrtSubTitle.Eof=false) and (ImportSubTitle.Eof=false) do begin
        CalcCurrentTimeToTmp;
        MergeCurrentContent;

        SrtSubTitle.Next;
        ImportSubTitle.Next;
    end;
    SrtSubTitle.MoveTmpTimeToTime;

    if ImportUnmatched then begin
        while ImportSubTitle.Eof=false do begin
            SrtSubTitle.Append;
            SrtSubTitle.CopyFields(ImportSubTitle);
            SrtSubTitle.Post;

            ImportSubTitle.Next;
        end;
    end;

end;

end.
