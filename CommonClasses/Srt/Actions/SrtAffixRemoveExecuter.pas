unit SrtAffixRemoveExecuter;

interface
uses SrtAffixExecuterBase;

type TSrtAffixRemoveExecuter=class(TSrtAffixExecuterBase)
  private
    FCaseSensitive:boolean;

    procedure DoRemoveCaseSensitiveAffix;
    procedure DoRemoveCaseInSensitiveAffix;
  public
    procedure RemoveAffix;

    property CaseSensitive:boolean read FCaseSensitive write FCaseSensitive;
end;
implementation

{ TAffixRemoveExecuter }
uses StrUtils,SysUtils;

procedure TSrtAffixRemoveExecuter.RemoveAffix;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        if not CaseSensitive then
            DoRemoveCaseInSensitiveAffix
        else
            DoRemoveCaseSensitiveAffix;

        ReturnRememberedPosition;
        EnableControls;
    end;
end;

procedure TSrtAffixRemoveExecuter.DoRemoveCaseSensitiveAffix;
var
    aRecNo:integer;
begin
    for aRecNo in RecNos do begin
        with SrtSubTitle do begin
            RecNo:=aRecNo;
            Edit;

            if LeftStr(CurrentContent,PrefixLength)=Prefix then begin
                CurrentContent:=RightStr(CurrentContent,CurrentContentLength-PrefixLength);
            end;

            if RightStr(CurrentContent,PostfixLength)=Postfix then begin
                CurrentContent:=LeftStr(CurrentContent,CurrentContentLength-PostfixLength);
            end;

            Post;
        end;
    end;
end;

procedure TSrtAffixRemoveExecuter.DoRemoveCaseInSensitiveAffix;
var
    aRecNo:integer;
begin
    for aRecNo in RecNos do begin
        with SrtSubTitle do begin
            RecNo:=aRecNo;
            Edit;

            if LowerCase(LeftStr(CurrentContent,PrefixLength))=LowerCase(Prefix) then begin
                CurrentContent:=RightStr(CurrentContent,CurrentContentLength-PrefixLength);
            end;

            if LowerCase(RightStr(CurrentContent,PostfixLength))=LowerCase(Postfix) then begin
                CurrentContent:=LeftStr(CurrentContent,CurrentContentLength-PostfixLength);
            end;

            Post;
        end;
    end;
end;

end.
