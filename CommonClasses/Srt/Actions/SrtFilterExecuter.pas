unit SrtFilterExecuter;

interface
uses SrtActionExecuter;

type FilterMode=(fmInclude,fmStartsWith,fmEndsWith);

type TFilterExecuter=class(TSrtActionExecuter)
  private
    FMode:FilterMode;
    FFindText:string;
    FCaseSensitive:boolean;
  public
    property Mode:FilterMode read FMode write FMode;
    property FindText:string read FFindText write FFindText;
    property CaseSensitive:boolean read FCaseSensitive write FCaseSensitive;

    procedure FilterSubTitle;
end;

implementation

{ TFilterExecuter }
uses SysUtils,DB;

procedure TFilterExecuter.FilterSubTitle;
const
    anyLengthChar:string='%';
begin
    with SrtSubTitle do begin
        if CaseSensitive then begin
            FilterOptions:=[];
        end else begin
            FilterOptions:=[foCaseInsensitive];
        end;

        case Mode of
            fmInclude: Filter:=ContentField + ' LIKE ' + QuotedStr(anyLengthChar + FindText + anyLengthChar);
            fmStartsWith: Filter:=ContentField + ' LIKE ' + QuotedStr(FindText + anyLengthChar);
            fmEndsWith: Filter:=ContentField + ' LIKE ' + QuotedStr(anyLengthChar + FindText);
        end;

        Filtered:=true;
    end;
end;

end.
