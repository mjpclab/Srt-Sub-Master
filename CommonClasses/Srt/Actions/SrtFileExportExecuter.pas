unit SrtFileExportExecuter;

interface
uses SrtActionExecuter,Generics.Collections;

type TSrtFileExportExecuter=class(TSrtActionExecuter)
  private
    FFileName:string;
    FRecNos:TList<integer>;
  protected
    procedure DoExportToFile;virtual;
  public
    property FileName:string read FFileName write FFileName;
    property RecNos:TList<integer> read FRecNos write FRecNos;

    procedure ExportToFile;
end;

implementation
uses SrtSubTitleFile;
{ TFileExportExecuter }

procedure TSrtFileExportExecuter.ExportToFile;
begin
    with SrtSubTitle do begin
        DisableControls;
        RememberPosition;

        DoExportToFile;

        ReturnRememberedPosition;
        EnableControls;
    end;
end;

procedure TSrtFileExportExecuter.DoExportToFile;
var
    srtfile:TSrtSubTitleFile;
    aRecNo:integer;
begin
    SrtSubTitle.DisableControls;

    srtfile:=TSrtSubTitleFile.Create;
    srtfile.Data.DisableControls;

    //copy records
    for aRecNo in RecNos do begin
        SrtSubTitle.RecNo:=aRecNo;

        srtfile.Data.SubTitle.Append;
        srtfile.Data.SubTitle.CopyFields(SrtSubTitle);
        srtfile.Data.SubTitle.Post;
    end;

    //reset NOs
    with srtfile.SrtData.SrtSubTitle do begin
        First;
        while not Eof do begin
            Edit;
            CurrentNo:=RecNo;
            Post;
            Next;
        end;
    end;

    //save and free
    srtfile.FileName:=self.FileName;
    srtfile.SaveToFile;
    srtfile.Free;

    SrtSubTitle.EnableControls;
end;

end.
