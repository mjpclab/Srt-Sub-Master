unit SrtTimeManager;

interface
uses TimeManager;

type TSrtTimeManager=class(TTimeManager)
  public
    function ToString:string;override;
    procedure Parse(const timeString:string);override;
    procedure ParseFormatted(const timeString:string);override;
end;

implementation

{ TSrtTimeManager }
uses SysUtils,StrUtils;

function TSrtTimeManager.ToString: string;
const
    timeFormatPattern:string='%s:%s:%s,%s';
begin
    exit(
        Format(timeFormatPattern,[self.HourString,self.MinuteString,self.SecondString,self.MilliSecondString])
    );
end;

procedure TSrtTimeManager.Parse(const timeString: string);
const
    separatorTime:char=':';
    separatorMs1:char=',';
    separatorMs2:char='.';
var
    currentPos:integer;
    buffer:string;

    intHour,intMinute,intSecond,intMilliSecond:integer;
    strHour,strMinute,strSecond,strMilliSecond:string;
begin
    buffer:=timeString;

    //提取小时
    currentPos:=Pos(separatorTime,buffer);
    strHour:=LeftStr(buffer,currentPos-1);
    intHour:=StrToIntDef(strHour,0);
    //buffer:=RightStr(buffer,Length(buffer)-currentPos);
    Delete(buffer,1,currentPos);

    //提取分钟
    currentPos:=Pos(separatorTime,buffer);
    strMinute:=LeftStr(buffer,currentPos-1);
    intMinute:=StrToIntDef(strMinute,0);
    //buffer:=RightStr(buffer,Length(buffer)-currentPos);
    Delete(buffer,1,currentPos);

    //提取秒
    currentPos:=Pos(separatorMs1,buffer);
    if currentPos<1 then currentPos:=Pos(separatorMs2,buffer);
    strSecond:=LeftStr(buffer,currentPos-1);
    intSecond:=StrToIntDef(strSecond,0);
    //buffer:=RightStr(buffer,Length(buffer)-currentPos);
    Delete(buffer,1,currentPos);

    //提取毫秒
    strMilliSecond:=LeftStr(buffer,3);
    strMilliSecond:=strMilliSecond+DupeString('0',3-Length(strMilliSecond));
    intMilliSecond:=StrToIntDef(strMilliSecond,0);

    //装配
    self.Hour:=intHour;
    self.Minute:=intMinute;
    self.Second:=intSecond;
    self.MilliSecond:=intMilliSecond;
end;

procedure TSrtTimeManager.ParseFormatted(const timeString: string);
var
    intHour,intMinute,intSecond,intMilliSecond:integer;
begin
    intHour:=StrToInt(LeftStr(timeString,2));
    intMinute:=StrToInt(MidStr(timeString,4,2));
    intSecond:=StrToInt(MidStr(timeString,7,2));
    intMilliSecond:=StrToInt(RightStr(timeString,3));

    //装配
    self.Hour:=intHour;
    self.Minute:=intMinute;
    self.Second:=intSecond;
    self.MilliSecond:=intMilliSecond;
end;

end.
