unit SrtSubTitleDataTable;

interface
uses SrtTimeManager,SubTitleDataTable,DB,Classes,Variants,Generics.Collections,SysUtils,StrUtils;

type TTimeRange=array[0..1] of string;

type SrtRecord=record
    Id:integer;
    TimeFrom:string;
    TimeTo:string;
    Content:string;
end;

type TSrtSubTitleDataTable=class(TSubTitleDataTable)
  private
    timeMan:TSrtTimeManager;

    procedure InitializeFieldDefs;
    function getCurrentNo: integer;
    procedure setCurrentNo(const Value: integer);
    function getCurrentNoString: string;
    function getCurrentTimeFrom: string;
    procedure setCurrentTimeFrom(const Value: string);
    function getCurrentTimeTo: string;
    procedure setCurrentTimeTo(const Value: string);
    function getCurrentContent: string;
    procedure setCurrentContent(const Value: string);
    function getCurrentContentLength: integer;

    function getCurrentTmpTimeFrom: string;
    procedure setCurrentTmpTimeFrom(const Value: string);
    function getCurrentTmpTimeTo: string;
    procedure setCurrentTmpTimeTo(const Value: string);
  protected
    procedure DoAfterInsert;override;
    //procedure DoAfterPost;override;
    procedure Initialize;override;
    procedure Finalize;override;
  public const
    NoField:string='no';
    TimeFromField:string='timefrom';
    TimeToField:string='timeto';
    ContentField:string='content';
    TmpTimeFromField:string='tmptimefrom';
    TmpTimeToField:string='tmptimeto';

    NoFieldIndex:integer=0;
    TimeFromFieldIndex:integer=1;
    TimeToFieldIndex:integer=2;
    ContentFieldIndex:integer=3;
    TmpTimeFromFieldIndex:integer=4;
    TmpTimeToFieldIndex:integer=5;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy;override;

    procedure MoveTmpTimeToTime;
    procedure MoveTmpTimeFromToTimeFrom;
    procedure MoveTmpTimeToToTimeTo;

    property CurrentNoString:string read getCurrentNoString;
    property CurrentNo:integer read getCurrentNo write setCurrentNo;
    property CurrentTimeFrom:string read getCurrentTimeFrom write setCurrentTimeFrom;
    property CurrentTimeTo:string read getCurrentTimeTo write setCurrentTimeTo;
    property CurrentContent:string read getCurrentContent write setCurrentContent;
    property CurrentContentLength:integer read getCurrentContentLength;
    property CurrentTmpTimeFrom:string read getCurrentTmpTimeFrom write setCurrentTmpTimeFrom;
    property CurrentTmpTimeTo:string read getCurrentTmpTimeTo write setCurrentTmpTimeTo;
    {$REGION '序列化'}
      private const
        timeRangeSeparator:string='-->';
        lineBreaker:string=#13#10;
      private
        function parseContentToSegments(const content:TStrings):TObjectList<TStrings>;
        function parseSegmentsToRecords(const segments:TObjectList<TStrings>):TList<SrtRecord>;
        function parseRecord(segment:TStrings):SrtRecord;
        function parseId(const id:string):integer;
        function parseTimeRange(const timeRange:string):TTimeRange;
        function parseTime(const timeString:string):string;
        function parseTextContent(const segment:TStrings;const startLineIndex:integer):string;
        procedure RecordsToSubTitleData(records:TList<SrtRecord>);
      public
        procedure ParseContent(const content:TStrings);override;
        procedure SerializeContent;override;
    {$ENDREGION}
end;

implementation

{ TSrtSubTitleDataTable }


{$REGION '构造与析构'}
    constructor TSrtSubTitleDataTable.Create(AOwner: TComponent);
    begin
        inherited;
        timeMan:=TSrtTimeManager.Create;
    end;

    destructor TSrtSubTitleDataTable.Destroy;
    begin
        timeMan.Free;
        inherited;
    end;
{$ENDREGION}

{$REGION '序列化'}
    procedure TSrtSubTitleDataTable.ParseContent(const content: TStrings);
    var
        segments:TObjectList<TStrings>;
        records:TList<SrtRecord>;
    begin
        TextContent.Text:=TrimLeft(content.Text);
    
        segments:=parseContentToSegments(TextContent);
        records:=parseSegmentsToRecords(segments);
    
        self.Clear;
        RecordsToSubTitleData(records);
    
        segments.Free;
        records.Free;
    end;
    
    function TSrtSubTitleDataTable.parseContentToSegments(const content:TStrings):TObjectList<TStrings>;
    const
        separatorTime:char=':';
        separatorMs1:char=',';
        separatorMs2:char='.';
    var
        recordSegments:TObjectList<TStrings>;
        i:integer;
        number:Integer;
        currentLine,nextLine,nextNextLine:string;
        endOfRecordFound:boolean;
    begin
        recordSegments:=TObjectList<TStrings>.Create;
    
        endOfRecordFound:=false;
        recordSegments.Add(TStringList.Create);
    
        for i := 0 to content.Count - 1 do begin
            currentLine:=Trim(content[i]);

            if currentLine='' then begin
                if i+1<content.Count then begin
                    nextLine:=content[i+1];
                end else begin
                    nextLine:='';
                end;

                if i+2<content.Count then begin
                    nextNextLine:=content[i+2];
                end else begin
                    nextNextLine:='';
                end;

                if TryStrToInt(nextLine,number)
                and (Pos(separatorTime,nextNextLine)>0)
                and ((Pos(separatorMs1,nextNextLine)>0) or (Pos(separatorMs2,nextNextLine)>0))
                and (Pos(timeRangeSeparator,nextNextLine)>0) then begin
                    endOfRecordFound:=true;
                end;
            end;
    
            if endOfRecordFound=False then begin
                recordSegments.Items[recordSegments.Count-1].Append(content[i]);
            end else begin
                recordSegments.Add(TStringList.Create);
                endOfRecordFound:=false;
            end;
        end;
    
        exit(recordSegments);
    end;

    function TSrtSubTitleDataTable.parseSegmentsToRecords(const segments:TObjectList<TStrings>):TList<SrtRecord>;
    var
        records:TList<SrtRecord>;
        i:integer;
    begin
        records:=TList<SrtRecord>.Create;
    
        for i := 0 to segments.Count - 1 do begin
            try
                records.Add(parseRecord(segments[i]));
            except
                //忽略解析错误
            end;
        end;
    
        exit(records);
    end;
    
    function TSrtSubTitleDataTable.parseRecord(segment: TStrings): SrtRecord;
    var
        aRecord:SrtRecord;
        timeRange:TTimeRange;
    const
        idLineIndex:integer=0;
        timeRangeLineIndex:integer=1;
        contentStartLineIndex:integer=2;
    begin
    
        aRecord.Id:=parseId(segment.Strings[idLineIndex]);
    
        timeRange:=parseTimeRange(segment.Strings[timeRangeLineIndex]);
        aRecord.TimeFrom:=timeRange[0];
        aRecord.TimeTo:=timeRange[1];
        
        aRecord.Content:=parseTextContent(segment,contentStartLineIndex);
    
        exit(aRecord);
    end;
    
    function TSrtSubTitleDataTable.parseId(const id: string): integer;
    begin
        exit(StrToIntDef(id,0));
    end;
    
    function TSrtSubTitleDataTable.parseTimeRange(const timeRange: string): TTimeRange;
    var
        separatorPos:integer;
        time1,time2:string;
        resultRange:TTimeRange;
    begin
        separatorPos:=pos(timeRangeSeparator,timeRange);
        time1:=Trim(LeftStr(timeRange,separatorPos-1));
        time2:=Trim(RightStr(timeRange,Length(timeRange)-(separatorPos+Length(timeRangeSeparator))));

        resultRange[0]:=parseTime(time1);
        resultRange[1]:=parseTime(time2);
        exit(resultRange);
    end;
    
    function TSrtSubTitleDataTable.parseTime(const timeString: string): string;
    begin
        timeMan.Parse(timeString);
        exit(timeMan.ToString);
    end;
    
    function TSrtSubTitleDataTable.parseTextContent(const segment: TStrings;
      const startLineIndex: integer): string;
    var
        buffer:TStrings;
    begin
        buffer:=TStringList.Create;
        buffer.Assign(segment);
        buffer.Delete(0);           //删除序号
        buffer.Delete(0);           //删除时间段

        Result:=buffer.Text;
        if RightStr(Result,2)=lineBreaker then Result:=LeftStr(Result,Length(Result)-2);    //去除最末回车换行
        buffer.Free;
    end;
    
    procedure TSrtSubTitleDataTable.RecordsToSubTitleData(records:TList<SrtRecord>);
    var
        i:integer;
        currentRecord:SrtRecord;
    begin
        self.DisableControls;
        self.DisableIndexFieldNames;

        for i := 0 to records.Count - 1 do begin
            currentRecord:=records.Items[i];

            Append;
            CurrentNo:=currentRecord.Id;
            CurrentTimeFrom:=currentRecord.TimeFrom;
            CurrentTimeTo:=currentRecord.TimeTo;
            CurrentContent:=currentRecord.Content;
            //Self.Fields[NoFieldIndex].AsInteger:=currentRecord.Id;
            //Self.Fields[TimeFromFieldIndex].AsString:=currentRecord.TimeFrom;
            //Self.Fields[TimeToFieldIndex].AsString:=currentRecord.TimeTo;
            //Self.Fields[ContentFieldIndex].AsString:=currentRecord.Content;
            Post;
        end;

        self.EnableIndexFieldNames;
        self.EnableControls;
    end;
    
    procedure TSrtSubTitleDataTable.SerializeContent;
    begin
        TextContent.Clear;

        DisableControls;
        RememberPosition;
        First;
        while not Eof do begin
            if RecNo<>1 then TextContent.Append('');

            TextContent.Append(CurrentNoString);
            TextContent.Append(
                CurrentTimeFrom
                + ' ' + timeRangeSeparator + ' '
                + CurrentTimeTo
            );
            TextContent.Append(CurrentContent);

            Next;
        end;
        ReturnRememberedPosition;
        EnableControls;
    end;
{$ENDREGION}

{$REGION '初始化/终结'}
    procedure TSrtSubTitleDataTable.Initialize;
    begin
        InitializeFieldDefs;

        IndexFieldNamesForSort:='timefrom;timeto';
        EnableIndexFieldNames;

        self.CreateDataTable;
        Self.Open;
        Self.LogChanges:=false;
    end;

    procedure TSrtSubTitleDataTable.InitializeFieldDefs;
    begin
        with self.FieldDefs.AddFieldDef do begin
            Name:=NoField;
            DisplayName:='序号';
            DataType:=ftInteger;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=TimeFromField;
            DisplayName:='开始时间';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=TimeToField;
            DisplayName:='结束时间';
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=ContentField;
            DisplayName:='字幕内容';
            DataType:=ftMemo;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=TmpTimeFromField;
            DisplayName:=TmpTimeFromField;
            DataType:=ftString;
        end;
        with self.FieldDefs.AddFieldDef do begin
            Name:=TmpTimeToField;
            DisplayName:=TmpTimeToField;
            DataType:=ftString;
        end;
    end;

    procedure TSrtSubTitleDataTable.Finalize;
    begin
        self.Close;
    end;
{$ENDREGION}

{$REGION '属性相关'}
    function TSrtSubTitleDataTable.getCurrentNoString: string;
    begin
        exit(self.Fields[NoFieldIndex].AsString);
    end;

    function TSrtSubTitleDataTable.getCurrentNo: integer;
    begin
        exit(self.Fields[NoFieldIndex].AsInteger);
    end;

    procedure TSrtSubTitleDataTable.setCurrentNo(const Value: integer);
    begin
        self.Fields[NoFieldIndex].AsInteger:=Value;
    end;

    function TSrtSubTitleDataTable.getCurrentTimeFrom: string;
    begin
        exit(self.Fields[TimeFromFieldIndex].AsString);
    end;

    procedure TSrtSubTitleDataTable.setCurrentTimeFrom(const Value: string);
    begin
        self.Fields[TimeFromFieldIndex].AsString:=Value;
    end;

    function TSrtSubTitleDataTable.getCurrentTimeTo: string;
    begin
        exit(self.Fields[TimeToFieldIndex].AsString);
    end;

    procedure TSrtSubTitleDataTable.setCurrentTimeTo(const Value: string);
    begin
        self.Fields[TimeToFieldIndex].AsString:=Value;
    end;

    function TSrtSubTitleDataTable.getCurrentContent: string;
    begin
        exit(self.Fields[ContentFieldIndex].AsString);
    end;

    procedure TSrtSubTitleDataTable.setCurrentContent(const Value: string);
    begin
        self.Fields[ContentFieldIndex].AsString:=Value;
    end;

    function TSrtSubTitleDataTable.getCurrentContentLength: integer;
    begin
        exit(Length(CurrentContent));
    end;

    function TSrtSubTitleDataTable.getCurrentTmpTimeFrom: string;
    begin
        exit(self.Fields[TmpTimeFromFieldIndex].AsString);
    end;

    procedure TSrtSubTitleDataTable.setCurrentTmpTimeFrom(const Value: string);
    begin
        self.Fields[TmpTimeFromFieldIndex].AsString:=Value;
    end;

    function TSrtSubTitleDataTable.getCurrentTmpTimeTo: string;
    begin
        exit(self.Fields[TmpTimeToFieldIndex].AsString);
    end;

    procedure TSrtSubTitleDataTable.setCurrentTmpTimeTo(const Value: string);
    begin
        self.Fields[TmpTimeToFieldIndex].AsString:=Value;
    end;

{$ENDREGION}

procedure TSrtSubTitleDataTable.DoAfterInsert;
const
    initTime:string='00:00:00,000';
begin
    inherited;
    CurrentNo:=RecordCount+1;
    CurrentTimeFrom:=initTime;
    CurrentTimeTo:=initTime;
end;

procedure TSrtSubTitleDataTable.MoveTmpTimeFromToTimeFrom;
var
    curBookmark:TBytes;
begin
    if Fields[TmpTimeFromFieldIndex].IsNull=false then begin
        Edit;
        CurrentTimeFrom:=CurrentTmpTimeFrom;
        Fields[TmpTimeFromFieldIndex].Value:=null;
        Post;
    end;
    curBookmark:=self.Bookmark;

    BackupFilter;
    Filter:=tmpTimeFromField + ' IS NOT NULL';
    Filtered:=true;

    DisableIndexFieldNames;

    First;
    while not Eof do begin
        Edit;
        CurrentTimeFrom:=CurrentTmpTimeFrom;
        Fields[TmpTimeFromFieldIndex].Value:=null;

        Post;
        //Next;     //Post以后，在filter中消失，下一条自动变为当前行
    end;

    EnableIndexFieldNames;
    RestoreFilter;
    self.Bookmark:=curBookmark;
end;

procedure TSrtSubTitleDataTable.MoveTmpTimeToToTimeTo;
var
    curBookmark:TBytes;
begin
    if Fields[TmpTimeToFieldIndex].IsNull=false then begin
        Edit;
        CurrentTimeTo:=CurrentTmpTimeTo;
        Fields[TmpTimeToFieldIndex].Value:=null;
        Post;
    end;
    curBookmark:=self.Bookmark;

    BackupFilter;
    Filter:=tmpTimeToField + ' IS NOT NULL';
    Filtered:=true;

    DisableIndexFieldNames;

    First;
    while not Eof do begin
        Edit;
        CurrentTimeTo:=CurrentTmpTimeTo;
        Fields[TmpTimeToFieldIndex].Value:=null;

        Post;
        //Next;     //Post以后，在filter中消失，下一条自动变为当前行
    end;

    EnableIndexFieldNames;
    RestoreFilter;
    self.Bookmark:=curBookmark;
end;

procedure TSrtSubTitleDataTable.MoveTmpTimeToTime;
begin
    MoveTmpTimeFromToTimeFrom;
    MoveTmpTimeToToTimeTo;
end;

end.
