unit SrtFromSsaSubTitleData;

interface
uses SrtSubTitleData,SsaSubTitleData,TimeManager;

type TSrtFromSsaSubTitleData=class(TSrtSubTitleData)
  private
    ssaData:TSsaSubTitleData;
    srtTime:TTimeManager;
    ssaTime:TTimeManager;
  public
    constructor Create;
    destructor Destroy;override;

    procedure ParseContent;override;
end;

implementation

{ TSrtFromSsaSubTitleFile }
uses SsaDialogueEventDataTable,SysUtils,CommonDefs;

{$REGION '����������'}
    constructor TSrtFromSsaSubTitleData.Create;
    begin
        inherited;
        ssaData:=TSsaSubTitleData.Create;
        srtTime:=Self.CreateTimeManager;
        ssaTime:=ssaData.CreateTimeManager;
    end;

    destructor TSrtFromSsaSubTitleData.Destroy;
    begin
        ssaData.Free;
        srtTime.Free;
        SsaTime.Free;
        inherited;
    end;
{$ENDREGION}

procedure TSrtFromSsaSubTitleData.ParseContent;
var
    SsaDialogueEvent:TSsaDialogueEventDataTable;
begin
    ssaData.TextContent.Text:=TextContent.Text;
    ssaData.ParseContent;
    SsaDialogueEvent:=ssaData.Sections.EventsSection.DialogueEvents;

    with SsaDialogueEvent do begin
        DisableControls;
        DisableIndexFieldNames;
        RememberPosition;
        First;

        SrtSubTitle.DisableControls;
        SrtSubTitle.DisableIndexFieldNames;
        SrtSubTitle.Clear;
        while not Eof do begin
            SrtSubTitle.Append;

            SrtSubTitle.CurrentNo:=RecNo;

            ssaTime.Parse(CurrentTimeStart);
            srtTime.Assign(ssaTime);
            SrtSubTitle.CurrentTimeFrom:=srtTime.ToString;

            ssaTime.Parse(CurrentTimeEnd);
            srtTime.Assign(ssaTime);
            SrtSubTitle.CurrentTimeTo:=srtTime.ToString;

            SrtSubTitle.CurrentContent:=ssaData.getPureTextContent(CurrentText);

            SrtSubTitle.Post;
            Next;
        end;
        SrtSubTitle.EnableIndexFieldNames;
        SrtSubTitle.EnableControls;

        ReturnRememberedPosition;
        EnableIndexFieldNames;
        EnableControls;

    end;
end;

end.
