unit SrtSubTitleData;

interface
uses SubTitleData,SubTitleDataTable,SrtSubTitleDataTable,TimeManager,SrtTimeManager
    ,Classes,Generics.Collections;


type TSrtSubTitleData=class(TSubTitleData)
  private
    FSrtSubTitle:TSrtSubTitleDataTable;
  protected
    function getSubTitle: TSubTitleDataTable;override;
    procedure setSubTitle(const Value: TSubTitleDataTable);override;
  public
    constructor Create;
    destructor Destroy;override;

    property SrtSubTitle:TSrtSubTitleDataTable read FSrtSubTitle write FSrtSubTitle;

    function CreateTimeManager:TTimeManager;override;

    procedure ParseContent;override;
    procedure SerializeContent;override;
end;


implementation
uses SysUtils,StrUtils;

{ TSrtSubTitleData }

constructor TSrtSubTitleData.Create;
begin
    inherited;
    SrtSubTitle:=TSrtSubTitleDataTable.Create(nil);
end;

destructor TSrtSubTitleData.Destroy;
begin
    SrtSubTitle.Free;
    inherited;
end;

function TSrtSubTitleData.getSubTitle: TSubTitleDataTable;
begin
    exit(FSrtSubTitle);
end;

procedure TSrtSubTitleData.setSubTitle(const Value: TSubTitleDataTable);
begin
    FSrtSubTitle:=TSrtSubTitleDataTable(Value);
end;

function TSrtSubTitleData.CreateTimeManager: TTimeManager;
var
    timeMan:TSrtTimeManager;
begin
    timeMan:=TSrtTimeManager.Create;
    exit(timeMan);
end;

procedure TSrtSubTitleData.ParseContent;
begin
    SrtSubTitle.ParseContent(TextContent);
end;

procedure TSrtSubTitleData.SerializeContent;
begin
    SrtSubTitle.serializeContent;
    TextContent.Text:=SrtSubTitle.TextContent.Text;
end;

end.
