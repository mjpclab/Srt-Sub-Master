unit SrtToSsaSubTitleData;

interface
uses SrtSubTitleData,SsaSubTitleData,TimeManager;

type TSrtToSsaSubTitleData=class(TSrtSubTitleData)
  private
    ssaData:TSsaSubTitleData;
    srtTime:TTimeManager;
    ssaTime:TTimeManager;
  public
    constructor Create;
    destructor Destroy;override;

    procedure SerializeContent;override;
end;

implementation
uses SsaDialogueEventDataTable;

{ TSrtToSsaSubTitleFile }

constructor TSrtToSsaSubTitleData.Create;
begin
    inherited;
    ssaData:=TSsaSubTitleData.Create;
    srtTime:=self.CreateTimeManager;
    ssaTime:=ssaData.CreateTimeManager;
end;

destructor TSrtToSsaSubTitleData.Destroy;
begin
    ssaData.Free;
    srtTime.Free;
    ssaTime.Free;
    inherited;
end;

procedure TSrtToSsaSubTitleData.SerializeContent;
var
    ssaDialog:TSsaDialogueEventDataTable;
begin
    with SrtSubTitle do begin
        DisableControls;
        DisableIndexFieldNames;
        RememberPosition;
        First;

        ssaDialog:=ssaData.Sections.EventsSection.DialogueEvents;
        ssaDialog.Clear;
        ssaDialog.DisableControls;
        ssaDialog.DisableIndexFieldNames;
        while not Eof do begin
            ssaDialog.Append;

            ssaDialog.CurrentMarked:=0;

            srtTime.Parse(CurrentTimeFrom);
            ssaTime.Assign(srtTime);
            ssaDialog.CurrentTimeStart:=ssaTime.ToString;

            srtTime.Parse(CurrentTimeTo);
            ssaTime.Assign(srtTime);
            ssaDialog.CurrentTimeEnd:=ssaTime.ToString;

            ssaDialog.CurrentStyle:='*Default';
            ssaDialog.CurrentName:='NTP';
            ssaDialog.CurrentMarginL:=0;
            ssaDialog.CurrentMarginR:=0;
            ssaDialog.CurrentMarginV:=0;
            ssaDialog.CurrentEffect:='';
            ssaDialog.CurrentText:=TSsaSubTitleData.ToSsaContent(CurrentContent);
            //ssaDialog.CurrentText:=CurrentContent;


            ssaDialog.Post;
            Next;
        end;

        ssaData.SerializeContent;
        self.TextContent.Text:=ssaData.TextContent.Text;

        ssaDialog.EnableIndexFieldNames;
        ssaDialog.EnableControls;

        ReturnRememberedPosition;
        EnableIndexFieldNames;
        EnableControls;
    end;
end;


end.
