unit FileTypeRecognizer;

interface

type TFileTypeRecognizer=class
  private
    FFileName:string;
  public
    property FileName:string read FFileName write FFileName;

    function IsSrtFile:boolean;
    function IsSsaFile:boolean;
    function IsAssFile:boolean;
    function IsSsaOrAssFile:boolean;
end;

implementation

{ TFileTypeRecognizer }
uses SysUtils;

function TFileTypeRecognizer.IsSrtFile:boolean;
begin
    exit(
        CompareText(ExtractFileExt(FileName),'.srt')=0
    );
end;

function TFileTypeRecognizer.IsSsaFile:boolean;
begin
    exit(
        CompareText(ExtractFileExt(FileName),'.ssa')=0
    );
end;

function TFileTypeRecognizer.IsAssFile: boolean;
begin
    exit(
        CompareText(ExtractFileExt(FileName),'.ass')=0
    );
end;

function TFileTypeRecognizer.IsSsaOrAssFile: boolean;
begin
    exit(
        (CompareText(ExtractFileExt(FileName),'.ssa')=0)
        or
        (CompareText(ExtractFileExt(FileName),'.ass')=0)
    );
end;

end.
