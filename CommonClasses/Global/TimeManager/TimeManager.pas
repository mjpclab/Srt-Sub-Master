unit TimeManager;

interface
uses SysUtils,StrUtils;

type TTimeManager=class(TObject)
  private
    FHour,FMinute,FSecond,FMilliSecond:integer;

    procedure setMilliSecond(const Value: integer);
    procedure setSecond(const Value: integer);
    procedure setMinute(const Value: integer);
    procedure setHour(const Value: integer);

    function getHourString: string;
    function getMilliSecondString: string;
    function getMinuteString: string;
    function getSecondString: string;
    function getTotalMilliSeconds: integer;
    procedure setTotalMilliSeconds(const Value: integer);
  public
    property Hour:integer read FHour write setHour;
    property Minute:integer read FMinute write setMinute;
    property Second:integer read FSecond write setSecond;
    property MilliSecond:integer read FMilliSecond write setMilliSecond;
    property TotalMilliSeconds:integer read getTotalMilliSeconds write setTotalMilliSeconds;

    procedure AddTime(const Value:TTimeManager);
    procedure ReduceTime(const Value:TTimeManager);
    procedure AddHours(const Value:integer);
    procedure AddMinutes(const Value:integer);
    procedure AddSeconds(const Value:integer);
    procedure AddMilliSeconds(const Value:integer);

    procedure SetZero;
    function ToString:string;override;
    procedure Parse(const timeString:string);virtual;
    procedure ParseFormatted(const timeString:string);virtual;
    procedure Assign(const source:TTimeManager);
  protected
    function formatTimeNumber(number,digits:integer):string;
    property HourString:string read getHourString;
    property MinuteString:string read getMinuteString;
    property SecondString:string read getSecondString;
    property MilliSecondString:string read getMilliSecondString;
end;

implementation

{ TTimeManager }

{$REGION '设置属性'}
    procedure TTimeManager.setHour(const Value: integer);
    begin
        if Value>=0 then begin
            FHour := Value;
        end else begin
            FHour:=0;
            FMinute:=0;
            FSecond:=0;
            FMilliSecond:=0;
        end;
    end;

    procedure TTimeManager.setMinute(const Value: integer);
    var
        borrow:integer;
    begin
        if Value>=0 then begin
            FMinute := Value mod MinsPerHour;
            Hour:=Hour + Value div MinsPerHour;
        end else begin
            borrow:=-Value div MinsPerHour;
            if (-Value mod MinsPerHour)>0 then inc(borrow);
            FMinute:=Value + borrow*MinsPerHour;
            Hour:=Hour-borrow;
        end;
    end;

    procedure TTimeManager.setSecond(const Value: integer);
    var
        borrow:integer;
    begin
        if Value>=0 then begin
            FSecond := Value mod SecsPerMin;
            Minute:=Minute + Value div SecsPerMin;
        end else begin
            borrow:=-Value div SecsPerMin;
            if (-Value mod SecsPerMin)>0 then Inc(borrow);
            FSecond:=Value + borrow*SecsPerMin;
            Minute:=Minute-borrow;
        end;
    end;

    procedure TTimeManager.setMilliSecond(const Value: integer);
    var
        borrow:integer;
    begin
        if Value>=0 then begin
            FMilliSecond := Value mod MSecsPerSec;
            Second:=Second + Value div MSecsPerSec;
        end else begin
            borrow:=-Value div MSecsPerSec;
            if (-Value mod MSecsPerSec)>0 then Inc(borrow);
            FMilliSecond:=Value + borrow*MSecsPerSec;
            Second:=Second-borrow;
        end;
    end;

    procedure TTimeManager.setTotalMilliSeconds(const Value: integer);
    begin
        SetZero;
        MilliSecond:=Value;
    end;

    function TTimeManager.getTotalMilliSeconds: integer;
    begin
        exit(Hour*SecsPerHour*MSecsPerSec+Minute*SecsPerMin*MSecsPerSec+Second*MSecsPerSec+MilliSecond);
    end;

{$ENDREGION}

{$REGION '增加量'}
    procedure TTimeManager.AddTime(const Value: TTimeManager);
    begin
        MilliSecond:=MilliSecond+Value.TotalMilliSeconds;
    end;

    procedure TTimeManager.ReduceTime(const Value: TTimeManager);
    begin
        MilliSecond:=MilliSecond-Value.TotalMilliSeconds;
    end;

    procedure TTimeManager.AddHours(const Value: integer);
    begin
        Hour:=Hour+Value;
    end;

    procedure TTimeManager.AddMinutes(const Value: integer);
    begin
        Minute:=Minute+Value;
    end;

    procedure TTimeManager.AddSeconds(const Value: integer);
    begin
        Second:=Second+Value;
    end;

    procedure TTimeManager.AddMilliSeconds(const Value: integer);
    begin
        MilliSecond:=MilliSecond+Value;
    end;
{$ENDREGION}

{$REGION '格式化相关'}
    function TTimeManager.formatTimeNumber(number, digits: integer): string;
    const
        prefix:char='0';
    var
        strNum:string;
    begin
        strNum:=IntToStr(number);
        strNum:=StringOfChar(prefix,digits-Length(strNum)) + strNum;
        exit(strNum);
    end;

    function TTimeManager.getHourString: string;
    begin
        exit(FormatTimeNumber(Hour,2));
    end;

    function TTimeManager.getMinuteString: string;
    begin
        exit(FormatTimeNumber(Minute,2));
    end;

    function TTimeManager.getSecondString: string;
    begin
        exit(FormatTimeNumber(Second,2));
    end;

    function TTimeManager.getMilliSecondString: string;
    begin
        exit(FormatTimeNumber(MilliSecond,3));
    end;

    function TTimeManager.ToString: string;
    const
        timeFormatPattern:string='%s:%s:%s:%s';
    begin
        exit(
            Format(timeFormatPattern,[self.HourString,self.MinuteString,self.SecondString,self.MilliSecondString])
        );
    end;
{$ENDREGION}

procedure TTimeManager.Assign(const source: TTimeManager);
begin
    FHour:=source.Hour;
    FMinute:=source.Minute;
    FSecond:=source.Second;
    FMilliSecond:=source.MilliSecond;
end;

procedure TTimeManager.SetZero;
begin
    Hour:=0;
    Minute:=0;
    Second:=0;
    MilliSecond:=0;
end;

procedure TTimeManager.Parse(const timeString: string);
begin

end;

procedure TTimeManager.ParseFormatted(const timeString: string);
begin

end;

end.
