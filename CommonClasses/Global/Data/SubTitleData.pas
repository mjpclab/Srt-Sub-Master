unit SubTitleData;

interface
uses SubTitleDataTable,TimeManager,Classes;

type TSubTitleData=class
  private
    FTextContent:TStrings;
  protected
    function getSubTitle: TSubTitleDataTable;virtual;abstract;
    procedure setSubTitle(const Value: TSubTitleDataTable);virtual;abstract;
  public
    property TextContent:TStrings read FTextContent write FTextContent;
    property SubTitle:TSubTitleDataTable read getSubTitle write setSubTitle;

    constructor Create;
    destructor Destroy;override;

    function CreateTimeManager:TTimeManager;virtual;abstract;

    procedure Clear;virtual;
    procedure DisableControls;virtual;
    procedure EnableControls;virtual;
    procedure RememberPosition;virtual;
    procedure ReturnRememberedPosition;virtual;
    procedure RememberBookmark;virtual;
    procedure ReturnRememberedBookmark;virtual;
    procedure DisableIndexFieldNames;virtual;
    procedure EnableIndexFieldNames;virtual;

    procedure ParseContent;virtual;abstract;
    procedure SerializeContent;virtual;abstract;
end;

implementation

{ TSsaData }

constructor TSubTitleData.Create;
begin
    inherited;
    FTextContent:=TStringList.Create;
end;

destructor TSubTitleData.Destroy;
begin
    FTextContent.Free;
    inherited;
end;

procedure TSubTitleData.Clear;
begin
    FTextContent.Clear;
    SubTitle.Clear;
end;

procedure TSubTitleData.DisableControls;
begin
    SubTitle.DisableControls;
end;

procedure TSubTitleData.EnableControls;
begin
    SubTitle.EnableControls;
end;

procedure TSubTitleData.RememberPosition;
begin
    SubTitle.RememberPosition;
end;

procedure TSubTitleData.ReturnRememberedPosition;
begin
    subtitle.ReturnRememberedPosition;
end;

procedure TSubTitleData.RememberBookmark;
begin
    SubTitle.RememberBookmark;
end;

procedure TSubTitleData.ReturnRememberedBookmark;
begin
    SubTitle.ReturnRememberedBookmark;
end;

procedure TSubTitleData.DisableIndexFieldNames;
begin
    SubTitle.DisableIndexFieldNames;
end;

procedure TSubTitleData.EnableIndexFieldNames;
begin
    SubTitle.EnableIndexFieldNames;
end;

end.
