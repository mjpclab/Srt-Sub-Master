unit ActionCommander;

interface
uses DataTable,SubTitleFile,Classes;

type TActionCommander=class
  private
    FIsNew:boolean;
    FIsModified:boolean;
    FIsHistorySaved:boolean;
    FAutoSave:boolean;

    function getFileName: string;

    function getCanAutoSave: boolean;
  protected
    function getSubTitleFile: TSubTitleFile;virtual;abstract;
    function getSubTitle: TDataTable;virtual;abstract;
  public

    property SubFile:TSubTitleFile read getSubTitleFile;
    property SubTitle:TDataTable read getSubTitle;
    property FileName:string read getFileName;

    property IsNew:boolean read FIsNew write FIsNew;
    property IsModified:boolean read FIsModified write FIsModified;
    property IsHistorySaved:boolean read FIsHistorySaved write FIsHistorySaved;
    property AutoSave:boolean read FAutoSave write FAutoSave;
    property CanAutoSave:boolean read getCanAutoSave;

    procedure ExecuteNewFile;virtual;
    procedure ExecuteOpenFile(const openFileName:string);virtual;
    procedure ExecuteSaveFile;virtual;
    procedure ExecuteSaveFileAs(const saveFileName:string);virtual;
    procedure ExecuteClearHistory;virtual;abstract;
    procedure ExecuteAutoSave;
end;

implementation

{ TActionCommander }

function TActionCommander.getFileName: string;
begin
    exit(SubFile.FileName);
end;

procedure TActionCommander.ExecuteNewFile;
begin
    IsNew:=true;
    IsModified:=false;

    ExecuteClearHistory;
    IsHistorySaved:=false;
end;

procedure TActionCommander.ExecuteOpenFile(const openFileName: string);
begin
    IsNew:=false;
    IsModified:=false;

    ExecuteClearHistory;
    IsHistorySaved:=false;
end;

procedure TActionCommander.ExecuteSaveFile;
begin
    if not IsNew then begin
        SubFile.SaveToFile;
        IsModified:=false;
    end else begin
        raise EInvalidOperation.Create('新建文件不能使用保存命令。请改用另存为命令。');
    end;
end;

procedure TActionCommander.ExecuteSaveFileAs(const saveFileName: string);
begin
    SubFile.SaveToFile(saveFileName);
    IsNew:=false;
    IsModified:=false;
end;

procedure TActionCommander.ExecuteAutoSave;
begin
    if CanAutoSave then begin
        self.ExecuteSaveFile;
    end;
end;

function TActionCommander.getCanAutoSave: boolean;
begin
    exit((AutoSave) and (IsModified) and (IsNew=false));
end;

end.
