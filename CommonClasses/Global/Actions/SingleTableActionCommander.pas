unit SingleTableActionCommander;

interface
uses ActionCommander,DB;

type TSingleTableActionCommander=class(TActionCommander)
  private
    procedure SubTitleChanged(DataSet:TDataSet);
  protected
    procedure HookSubTitleChanged;virtual;
    procedure UnHookSubTitleChanged;virtual;
  public
    procedure AfterConstruction;override;
    procedure ExecuteNewFile;override;
    procedure ExecuteOpenFile(const openFileName:string);override;
end;

implementation

procedure TSingleTableActionCommander.AfterConstruction;
begin
    inherited;
    IsNew:=true;
    IsModified:=false;

    HookSubTitleChanged;
end;

procedure TSingleTableActionCommander.HookSubTitleChanged;
begin
    SubTitle.AfterPost:=SubTitleChanged;
    SubTitle.AfterDelete:=SubTitleChanged;
end;

procedure TSingleTableActionCommander.UnHookSubTitleChanged;
begin
    SubTitle.AfterPost:=nil;
    SubTitle.AfterDelete:=nil;
end;

procedure TSingleTableActionCommander.SubTitleChanged(DataSet: TDataSet);
begin
    IsModified:=true;
    IsHistorySaved:=false;
end;

procedure TSingleTableActionCommander.ExecuteNewFile;
begin
    inherited;

    UnHookSubTitleChanged;
    SubFile.NewFile;
    SubTitle.ClearFilter;
    HookSubTitleChanged;
end;

procedure TSingleTableActionCommander.ExecuteOpenFile(const openFileName: string);
begin
    inherited;

    UnHookSubTitleChanged;
    SubFile.LoadFromFile(openFileName);
    //SubTitle.ClearFilter;
    HookSubTitleChanged;
end;

end.
