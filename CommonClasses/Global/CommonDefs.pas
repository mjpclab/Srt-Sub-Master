unit CommonDefs;

interface

type ActionScope=(asCurrent,asSelected,asEntire,asFirstToCurrent,asCurrentToLast,asCurrentAndPrev,asCurrentAndNext);

type MergePosition=(mpInsertBefore,mpAppendAfter);

const strNewLine:string=#13#10;

implementation

end.
