unit SubTitleDataTable;

interface
uses DataTable,Classes;

type TSubTitleDataTable=class(TDataTable)
  private
    FTextContent:TStrings;
  public
    property TextContent:TStrings read FTextContent write FTextContent;

    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;

    procedure ParseContent(const content:TStrings);virtual;abstract;
    procedure SerializeContent;virtual;abstract;

end;

implementation

{ TSubTitleDataTable }

constructor TSubTitleDataTable.Create(AOwner: TComponent);
begin
    inherited;
    FTextContent:=TStringList.Create;
end;

destructor TSubTitleDataTable.Destroy;
begin
    FTextContent.Free;
    inherited;
end;

end.
