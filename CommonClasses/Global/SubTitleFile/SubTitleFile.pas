unit SubTitleFile;

interface
uses SubTitleDataTable,SubTitleData,Classes;

type TSubTitleFile=class(TObject)
  private
    FFileName:string;
    FData:TSubTitleData;
  protected
    procedure CreateData;virtual;abstract;
    procedure DestroyData;virtual;abstract;
  public
    constructor Create;
    destructor Destroy;override;

    procedure NewFile();
    procedure LoadFromFile;overload;virtual;
    procedure LoadFromFile(const loadFileName:string);overload;
    procedure SaveToFile();overload;virtual;
    procedure SaveToFile(const saveFileName:string);overload;

    property FileName:string read FFileName write FFileName;
    property Data:TSubTitleData read FData write FData;
end;
implementation

{ TSubFile }

constructor TSubTitleFile.Create;
begin
    inherited;
    CreateData;
end;

destructor TSubTitleFile.Destroy;
 begin
    DestroyData;
    inherited;
end;

procedure TSubTitleFile.NewFile;
begin
    FileName:='';
    Data.Clear;
end;

procedure TSubTitleFile.LoadFromFile(const loadFileName: string);
begin
    FileName:=loadFileName;
    LoadFromFile;
end;
procedure TSubTitleFile.LoadFromFile;
begin
    Data.Clear;
    Data.DisableControls;
    Data.TextContent.LoadFromFile(FileName);
    Data.ParseContent;
    Data.SubTitle.First;
    Data.EnableControls;
end;

procedure TSubTitleFile.SaveToFile(const saveFileName: string);
begin
    FileName:=saveFileName;
    SaveToFile;
end;

procedure TSubTitleFile.SaveToFile;
begin
    Data.DisableControls;
    Data.RememberPosition;
    Data.SerializeContent;
    Data.TextContent.SaveToFile(FileName);
    Data.ReturnRememberedPosition;
    Data.EnableControls;
end;

end.
