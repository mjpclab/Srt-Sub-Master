unit SingleTableSubTitleFile;

interface
uses SubTitleFile;

type TSingleTableSubTitleFile=class(TSubTitleFile)
  public
    procedure LoadFromFile;override;
    procedure SaveToFile;override;

    procedure parseFile;override;
    procedure serializeFile;override;
end;

implementation

procedure TSingleTableSubTitleFile.LoadFromFile;
begin
    inherited;


end;

procedure TSingleTableSubTitleFile.SaveToFile;
begin
    

    inherited;
end;

end.
