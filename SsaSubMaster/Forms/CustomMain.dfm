object frmCustomMain: TfrmCustomMain
  Left = 0
  Top = 0
  Caption = 'frmCustomMain'
  ClientHeight = 392
  ClientWidth = 563
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 563
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 0
    object Button1: TButton
      Left = 0
      Top = 0
      Width = 145
      Height = 41
      Caption = 'open'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 152
      Top = 0
      Width = 129
      Height = 41
      Caption = 'Button2'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object gridSub: TcxGrid
    Left = 0
    Top = 41
    Width = 563
    Height = 351
    Align = alClient
    TabOrder = 1
    object grdSub: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dscSub
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object gridSubLevel1: TcxGridLevel
      GridView = grdSub
    end
  end
  object dlgOpen: TOpenDialog
    DefaultExt = 'ssa'
    Filter = 'ssa file|*.ssa'
    Left = 392
  end
  object dscSub: TDataSource
    Left = 176
    Top = 248
  end
end
