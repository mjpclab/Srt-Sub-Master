unit CustomMain;

interface

uses
    SsaSubtitleFile,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfrmCustomMain = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    dlgOpen: TOpenDialog;
    grdSub: TcxGridDBTableView;
    gridSubLevel1: TcxGridLevel;
    gridSub: TcxGrid;
    dscSub: TDataSource;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    ssafile:TSsaSubTitleFile;
  public
    { Public declarations }
  end;

var
  frmCustomMain: TfrmCustomMain;

implementation

{$R *.dfm}
uses SsaTimeManager;
procedure TfrmCustomMain.Button2Click(Sender: TObject);
var
    ssaTime:TSsaTimeManager;
begin
    ssaTime:=TSsaTimeManager.Create;

   ssaTime.Parse('1:2:3.456');
    self.Caption:=ssaTime.ToString;
    ssaTime.free;
end;

procedure TfrmCustomMain.FormCreate(Sender: TObject);
begin
    ssafile:=TSsaSubTitleFile.Create;
    dscSub.DataSet:=ssafile.Data.Sections.EventsSection.DialogueEvents;
    grdsub.DataController.CreateAllItems(true);
end;

procedure TfrmCustomMain.FormDestroy(Sender: TObject);
begin
    ssafile.Free;
end;

procedure TfrmCustomMain.Button1Click(Sender: TObject);
begin
    if dlgopen.Execute then begin
        ssafile.Data.Sections.EventsSection.AllDisableControls;
        ssafile.LoadFromFile(dlgopen.FileName);
        ssafile.Data.Sections.EventsSection.AllEnableControls;
    end;
end;

end.
