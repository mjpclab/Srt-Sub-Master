Srt Sub Master 源代码说明

开发工具：Delphi XE 7
需要安装的第三方控件包：
1.DevExpressVCL (build 14.2.2或更高，并且保证已经编译并安装了其中的Skin)
2.导入Windows Media Player ActiveX组件（Component->Import Coponent->Import ActiveX Control->windows media player->Generate Component Wrappers->Install to new package）。

源代码目录说明：
SrtSubMaster\ Srt Sub Master源代码主项目文件
CommonClasses\ 项目所需的公共类库，请保持该目录和主项目目录在一起（在同一父目录下）
SsaSubMaster\ 测试SSA字幕导入功能的临时项目




对于从本代码衍生出的编译版本，MJ PC Lab并不要求强制开源，不过为了软件能更好地发展，建议开发者能将自己的成果公布，让大家一起来创造出更好的软件！

MJ PC Lab
2011.06.11
update 2015.03.08