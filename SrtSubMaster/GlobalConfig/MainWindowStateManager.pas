unit MainWindowStateManager;

interface
uses WindowStateManager,CustomMain,IniFiles;

type TMainWindowStateManager=class(TWindowStateManager)
  private const
    SectionName:string='MainWindowState';
  private
    FSubtitleGridWidth:integer;
    FEditPanelHeight:integer;
    FAutoSave:boolean;
    FSyncSubToVideo:boolean;
    FSyncVideoToSub:boolean;
  public
    property SubtitleGridWidth:integer read FSubtitleGridWidth write FSubtitleGridWidth;
    property EditPanelHeight:integer read FEditPanelHeight write FEditPanelHeight;
    property AutoSave:boolean read FAutoSave write FAutoSave;
    property SyncSubToVideo:boolean read FSyncSubToVideo write FSyncSubToVideo;
    property SyncVideoToSub:boolean read FSyncVideoToSub write FSyncVideoToSub;

    constructor Create;

    procedure SaveConfig(const IniFile:TIniFile);override;
    procedure LoadConfig(const IniFile:TIniFile);override;
    procedure UpdatePropertyToWindow(frm:TfrmCustomMain);reintroduce;
    procedure UpdateWindowToProperty(frm:TfrmCustomMain);reintroduce;
end;
implementation

{ TMainWindowStateManager }

constructor TMainWindowStateManager.Create;
begin
    inherited;
    self.IniSectionName:=SectionName;
end;

procedure TMainWindowStateManager.LoadConfig(const IniFile: TIniFile);
begin
    inherited;
    SubtitleGridWidth:=IniFile.ReadInteger(IniSectionName,'SubtitleGridWidth',500);
    EditPanelHeight:=IniFile.ReadInteger(IniSectionName,'EditPanelHeight',150);
    AutoSave:=IniFile.ReadBool(IniSectionName,'AutoSave',false);
    SyncSubToVideo:=IniFile.ReadBool(IniSectionName,'SyncSubToVideo',false);
    SyncVideoToSub:=IniFile.ReadBool(IniSectionName,'SyncVideoToSub',false);
end;

procedure TMainWindowStateManager.SaveConfig(const IniFile: TIniFile);
begin
    inherited;
    IniFile.WriteInteger(IniSectionName,'SubtitleGridWidth',SubtitleGridWidth);
    IniFile.WriteInteger(IniSectionName,'EditPanelHeight',EditPanelHeight);
    IniFile.WriteBool(IniSectionName,'AutoSave',AutoSave);
    IniFile.WriteBool(IniSectionName,'SyncSubToVideo',SyncSubToVideo);
    IniFile.WriteBool(IniSectionName,'SyncVideoToSub',SyncVideoToSub);
end;

procedure TMainWindowStateManager.UpdatePropertyToWindow(frm: TfrmCustomMain);
begin
    inherited UpdatePropertyToWindow(frm);
    frm.SubtitleGridWidth:=self.SubtitleGridWidth;
    frm.EditPanelHeight:=self.EditPanelHeight;
    frm.AutoSave:=self.AutoSave;
    frm.SyncSubToVideo:=self.SyncSubToVideo;
    frm.SyncVideoToSub:=self.SyncVideoToSub;
end;

procedure TMainWindowStateManager.UpdateWindowToProperty(frm: TfrmCustomMain);
begin
    inherited UpdateWindowToProperty(frm);
    self.SubtitleGridWidth:=frm.SubtitleGridWidth;
    self.EditPanelHeight:=frm.EditPanelHeight;
    self.AutoSave:=frm.AutoSave;
    self.SyncSubToVideo:=frm.SyncSubToVideo;
    self.SyncVideoToSub:=frm.SyncVideoToSub;
end;

end.
