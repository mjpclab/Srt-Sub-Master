unit FileSessionManager;

interface

type TFileSessionManager=class
  private
    FPrevSubToVideoPosition:Double;
    FPrevVideoToSubPosition:Double;

    FPrevSubToVideoAvailable:Boolean;
    FPrevVideoToSubAvailable:Boolean;
  public
    property PrevSubToVideoPosition:Double read FPrevSubToVideoPosition write FPrevSubToVideoPosition;
    property PrevVideoToSubPosition:Double read FPrevVideoToSubPosition write FPrevVideoToSubPosition;

    property PrevSubToVideoAvailable:Boolean read FPrevSubToVideoAvailable write FPrevSubToVideoAvailable;
    property PrevVideoToSubAvailable:Boolean read FPrevVideoToSubAvailable write FPrevVideoToSubAvailable;

    procedure Reset;

    constructor Create;
end;

implementation

{ TFileSessionManager }

constructor TFileSessionManager.Create;
begin
    inherited;

    Reset;
end;

procedure TFileSessionManager.Reset;
begin
    PrevSubToVideoPosition:=0;
    PrevVideoToSubPosition:=0;

    PrevSubToVideoAvailable:=false;
    PrevVideoToSubAvailable:=false;
end;

end.
