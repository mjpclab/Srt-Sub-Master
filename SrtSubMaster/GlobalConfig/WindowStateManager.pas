unit WindowStateManager;

interface
uses SysUtils,IniFiles,Forms,Windows;

type TWindowStateManager=class
  private
    FIniSectionName:string;
    FLeft,FTop,FWidth,FHeight:integer;
    FWindowState:TWindowState;
  public
    property IniSectionName:string read FIniSectionName write FIniSectionName;
    property Left:integer read FLeft write FLeft;
    property Top:integer read FTop write FTop;
    property Width:integer read FWidth write FWidth;
    property Height:integer read FHeight write FHeight;
    property WindowState:TWindowState read FWindowState write FWindowState;

    procedure SaveConfig(const IniFile:TIniFile);virtual;
    procedure LoadConfig(const IniFile:TIniFile);virtual;
    procedure UpdatePropertyToWindow(frm:TForm);virtual;
    procedure UpdateWindowToProperty(frm:TForm);virtual;
end;
implementation

{ TWindowStateManager }

procedure TWindowStateManager.LoadConfig(const IniFile: TIniFile);
begin
    Left:=IniFile.ReadInteger(IniSectionName,'Left',0);
    Top:=IniFile.ReadInteger(IniSectionName,'Top',0);
    Width:=IniFile.ReadInteger(IniSectionName,'Width',860);
    Height:=IniFile.ReadInteger(IniSectionName,'Height',650);
    WindowState:=TWindowState(IniFile.ReadInteger(IniSectionName,'WindowState',0));
end;

procedure TWindowStateManager.SaveConfig(const IniFile: TIniFile);
begin
    IniFile.WriteInteger(IniSectionName,'Left',Left);
    IniFile.WriteInteger(IniSectionName,'Top',Top);
    IniFile.WriteInteger(IniSectionName,'Width',Width);
    IniFile.WriteInteger(IniSectionName,'Height',Height);
    IniFile.WriteInteger(IniSectionName,'WindowState',Integer(WindowState));
end;

procedure TWindowStateManager.UpdatePropertyToWindow(frm: TForm);
//var
//    tmpHeight1,tmpHeight2,deltaHeight:integer;
begin
//    tmpHeight1:=frm.Height;
//    frm.Height:=tmpHeight1;
//    tmpheight2:=frm.Height;
//    deltaHeight:=tmpHeight1-tmpHeight2;

    frm.Left:=self.Left;
    frm.Top:=self.Top;
    frm.Width:=self.Width;
    frm.Height:=self.Height;
    if self.WindowState<>wsMinimized then frm.WindowState:=self.WindowState;
end;

procedure TWindowStateManager.UpdateWindowToProperty(frm: TForm);
var
    place: TWindowPlacement;
begin
    place.length := sizeof(TWindowPlacement);
    GetWindowPlacement(frm.Handle, @place);

    self.WindowState:=frm.WindowState;
    self.Left:=place.rcNormalPosition.Left;
    self.Top:=place.rcNormalPosition.Top;
    self.Width:=place.rcNormalPosition.Right-place.rcNormalPosition.Left;
    self.Height:=place.rcNormalPosition.Bottom-place.rcNormalPosition.Top;
end;

end.
