unit GlobalConfig;

interface
uses RecentFileListManager,MainWindowStateManager,FileSessionManager,
    IniFiles,SysUtils;

type TGlobalConfig=class
  private
    FRecentFilesManager:TRecentFileListManager;
    FMainWindowStateManager:TMainWindowStateManager;
    FFileSessionManager:TFileSessionManager;

    function getConfigFileName:string;
  public
    property RecentFilesManager:TRecentFileListManager read FRecentFilesManager write FRecentFilesManager;
    property MainWindowStateManager:TMainWindowStateManager read FMainWindowStateManager write FMainWindowStateManager;
    property FileSessionManager:TFileSessionManager read FFileSessionManager write FFileSessionManager;

    constructor Create;
    destructor Destroy;override;

    procedure SaveConfig;
    procedure LoadConfig;
end;

implementation

{ TGlobalConfig }

function TGlobalConfig.getConfigFileName: string;
begin
    exit(ChangeFileExt(ParamStr(0),'.ini'));
end;

{$REGION '����������'}
    constructor TGlobalConfig.Create;
    const
        DefaultMaxRecentFiles:integer=15;
    begin
        inherited;
        RecentFilesManager:=TRecentFileListManager.Create;
        RecentFilesManager.MaxFileCount:=DefaultMaxRecentFiles;
        MainWindowStateManager:=TMainWindowStateManager.Create;
        FileSessionManager:=TFileSessionManager.Create;
    end;

    destructor TGlobalConfig.Destroy;
    begin
        RecentFilesManager.Free;
        MainWindowStateManager.Free;
        FileSessionManager.Free;
        inherited;
    end;
{$ENDREGION}


procedure TGlobalConfig.SaveConfig;
var
    IniFile:TIniFile;
begin
    IniFile:=TIniFile.Create(getConfigFileName);

    try
        RecentFilesManager.SaveConfig(IniFile);
        MainWindowStateManager.SaveConfig(IniFile);
    except

    end;

    IniFile.Free;
end;

procedure TGlobalConfig.LoadConfig;
var
    IniFile:TIniFile;
begin
    IniFile:=TIniFile.Create(getConfigFileName);

    RecentFilesManager.LoadConfig(IniFile);
    MainWindowStateManager.LoadConfig(IniFile);

    IniFile.Free;
end;


end.
