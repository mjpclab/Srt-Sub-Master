unit Main;

interface

uses
  SrtTimeManager,SrtSubTitleFile,SrtActionCommander,GlobalConfig,
  CommonDefs,
  SrtFilterExecuter,SrtFindReplaceExecuter,
  ShellAPI,Generics.Collections,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CustomMain, dxSkinsCore, dxSkinOffice2007Black,
 ActnList, PlatformDefaultStyleActnCtrls, ActnMan,
  dxSkinsdxRibbonPainter, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxMaskEdit, cxLookAndFeelPainters, Menus, dxSkinsdxBarPainter,
  cxDropDownEdit, cxTextEdit, cxCheckBox, dxBar, dxRibbon, dxRibbonGallery,
  ExtCtrls, ImgList, cxLookAndFeels, dxSkinsForm, cxBarEditItem, cxMemo,
  OleCtrls, WMPLib_TLB, cxPC, StdCtrls, cxButtons, cxDBEdit, cxContainer,
  cxGroupBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxSplitter, dxStatusBar, dxRibbonStatusBar, cxLabel, dxRibbonSkins,
  dxRibbonCustomizationForm, cxNavigator, dxBarBuiltInMenu, System.Actions,
  dxBarApplicationMenu;

type
  TfrmMain = class(TfrmCustomMain)
    ActionManager1: TActionManager;
    actNewFile: TAction;
    actOpenFile: TAction;
    actSaveFile: TAction;
    actSaveFileAs: TAction;
    actResetNos: TAction;
    actOpenVideoFile: TAction;
    actCloseVideoFile: TAction;
    actNewRec: TAction;
    actNewRecAndPlayerToTimeFrom: TAction;
    actPlayerToTimeFrom: TAction;
    actPlayerToTimeTo: TAction;
    actUpdateRec: TAction;
    actCancelUpdateRec: TAction;
    actDeleteRec: TAction;
    actMoveTime: TAction;
    actImport: TAction;
    actExport: TAction;
    actAddAffix: TAction;
    actRemoveAffix: TAction;
    actDeleteAffixString: TAction;
    actResizeTimeFrom: TAction;
    actResizeTimeTo: TAction;
    actAbout: TAction;
    actSubHome: TAction;
    actPcLabHome: TAction;
    actScaleTime: TAction;
    actFilter: TAction;
    actClearFilter: TAction;
    actFindFirst: TAction;
    actFindLast: TAction;
    actFindPrior: TAction;
    actFindNext: TAction;
    actMergeBySequence: TAction;
    actMergeByNo: TAction;
    actMergeByTimeFrom: TAction;
    actReplaceCurrent: TAction;
    actReplaceAll: TAction;
    actExit: TAction;
    actPlayerPlay: TAction;
    actPlayerPause: TAction;
    actPlayerFastForward: TAction;
    actPlayerFastBackward: TAction;
    actPlayerStop: TAction;
    actSyncSubToVideoAuto: TAction;
    actSyncVideoToSubAuto: TAction;
    actAutoSave: TAction;
    actUndo: TAction;
    actRedo: TAction;
    actSyncSubToVideoNow: TAction;
    actSyncVideoToSubNow: TAction;
    actPlayerToTimeToAndNextRec: TAction;
    btnPlayerToTimeToAndNextRec: TdxBarLargeButton;
    actTrimWhiteSpace: TAction;
    actDeleteEmptySubTitle: TAction;
    actImportText: TAction;
    actCombineMultiLineSelected: TAction;
    actCombineMultiLineUp: TAction;
    actCombineMultiLineDown: TAction;
    actCombineSingleLineSelected: TAction;
    actCombineSingleLineUp: TAction;
    actCombineSingleLineDown: TAction;
    actSplitSub: TAction;
    actMoveCurrentToPlayerTime: TAction;
    actMoveFirstToCurrentToPlayerTime: TAction;
    actMoveCurrentToLastToPlayerTime: TAction;
    actMoveSelectedToPlayerTime: TAction;
    actMoveAllToPlayerTime: TAction;
    actNewRecBefore: TAction;
    actFocusUp: TAction;
    actFocusDown: TAction;
    actPrevVideoToSub: TAction;
    actPrevSubToVideo: TAction;
    procedure actSaveFileAsExecute(Sender: TObject);
    procedure actSaveFileExecute(Sender: TObject);
    procedure actOpenFileExecute(Sender: TObject);
    procedure actNewFileExecute(Sender: TObject);
    procedure actResetNosExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actCloseVideoFileExecute(Sender: TObject);
    procedure actOpenVideoFileExecute(Sender: TObject);
    procedure wmplayerPositionChange(ASender: TObject; oldPosition,
      newPosition: Double);
    procedure syncTimerTimer(Sender: TObject);
    procedure grdSubFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actNewRecExecute(Sender: TObject);
    procedure actNewRecAndPlayerToTimeFromExecute(Sender: TObject);
    procedure actPlayerToTimeToExecute(Sender: TObject);
    procedure actUpdateRecExecute(Sender: TObject);
    procedure actCancelUpdateRecExecute(Sender: TObject);
    procedure actDeleteRecExecute(Sender: TObject);
    procedure btnTimeFromInc1000Click(Sender: TObject);
    procedure btnTimeFromDec1000Click(Sender: TObject);
    procedure btnTimeFromInc100Click(Sender: TObject);
    procedure btnTimeFromDec100Click(Sender: TObject);
    procedure btnTimeToInc1000Click(Sender: TObject);
    procedure btnTimeToDec1000Click(Sender: TObject);
    procedure btnTimeToInc100Click(Sender: TObject);
    procedure btnTimeToDec100Click(Sender: TObject);
    procedure actMoveTimeExecute(Sender: TObject);
    procedure actImportExecute(Sender: TObject);
    procedure actExportExecute(Sender: TObject);
    procedure actDeleteAffixStringExecute(Sender: TObject);
    procedure actRemoveAffixExecute(Sender: TObject);
    procedure actAddAffixExecute(Sender: TObject);
    procedure actPlayerToTimeFromExecute(Sender: TObject);
    procedure actResizeTimeToExecute(Sender: TObject);
    procedure actResizeTimeFromExecute(Sender: TObject);
    procedure actSubHomeExecute(Sender: TObject);
    procedure actPcLabHomeExecute(Sender: TObject);
    procedure actScaleTimeExecute(Sender: TObject);
    procedure actAboutExecute(Sender: TObject);
    procedure edtFilterKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFilterChange(Sender: TObject);
    procedure actFilterExecute(Sender: TObject);
    procedure actClearFilterExecute(Sender: TObject);
    procedure actFindFirstExecute(Sender: TObject);
    procedure actFindLastExecute(Sender: TObject);
    procedure actFindPriorExecute(Sender: TObject);
    procedure actFindNextExecute(Sender: TObject);
    procedure actMergeBySequenceExecute(Sender: TObject);
    procedure actMergeByNoExecute(Sender: TObject);
    procedure actMergeByTimeFromExecute(Sender: TObject);
    procedure actReplaceAllExecute(Sender: TObject);
    procedure actReplaceCurrentExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure appMenuExtraPaneItemClick(Sender: TObject; AIndex: Integer);
    procedure actPlayerFastForwardExecute(Sender: TObject);
    procedure actPlayerPlayExecute(Sender: TObject);
    procedure actPlayerPauseExecute(Sender: TObject);
    procedure actPlayerFastBackwardExecute(Sender: TObject);
    procedure actPlayerStopExecute(Sender: TObject);
    procedure actSyncSubToVideoAutoExecute(Sender: TObject);
    procedure actSyncVideoToSubAutoExecute(Sender: TObject);
    procedure actAutoSaveExecute(Sender: TObject);
    procedure dscSourceDataChange(Sender: TObject; Field: TField);
    procedure displayingSubTimerTimer(Sender: TObject);
    procedure grdHistoryFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure actUndoExecute(Sender: TObject);
    procedure actRedoExecute(Sender: TObject);
    procedure actSyncSubToVideoNowExecute(Sender: TObject);
    procedure actSyncVideoToSubNowExecute(Sender: TObject);
    procedure actPlayerToTimeToAndNextRecExecute(Sender: TObject);
    procedure actTrimWhiteSpaceExecute(Sender: TObject);
    procedure actDeleteEmptySubTitleExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actImportTextExecute(Sender: TObject);
    procedure actCombineMultiLineSelectedExecute(Sender: TObject);
    procedure actCombineMultiLineUpExecute(Sender: TObject);
    procedure actCombineMultiLineDownExecute(Sender: TObject);
    procedure actCombineSingleLineSelectedExecute(Sender: TObject);
    procedure actCombineSingleLineUpExecute(Sender: TObject);
    procedure actCombineSingleLineDownExecute(Sender: TObject);
    procedure actSplitSubExecute(Sender: TObject);
    procedure actMoveCurrentToPlayerTimeExecute(Sender: TObject);
    procedure actMoveFirstToCurrentToPlayerTimeExecute(Sender: TObject);
    procedure actMoveCurrentToLastToPlayerTimeExecute(Sender: TObject);
    procedure actMoveSelectedToPlayerTimeExecute(Sender: TObject);
    procedure actMoveAllToPlayerTimeExecute(Sender: TObject);
    procedure actNewRecBeforeExecute(Sender: TObject);
    procedure actFocusUpExecute(Sender: TObject);
    procedure actFocusDownExecute(Sender: TObject);
    procedure actPrevVideoToSubExecute(Sender: TObject);
    procedure actPrevSubToVideoExecute(Sender: TObject);
  private const
    FindNotFoundTitle:PWideChar='查找';
    FindNotFoundContent:PwideChar='找不到匹配项。';
    RecentNotFoundTitle:PWideChar='打开最近项目';
    RecentNotFoundContent:PWideChar='找不到文件，可能已被删除或移动。';
  private
    srtCommander:TSrtActionCommander;
    globalConfig:TGlobalConfig;
    timeMan:TSrtTimeManager;
    function getRecNos(const scope:ActionScope):TList<integer>;

    procedure refreshRecentFiles;

    procedure openFile(const fileName:string);
    function getFileName: string;
    procedure setFileName(const Value: string);
    function confirmSave:boolean;

    procedure postPrevEditing;

    procedure moveToPlayerTime(const scope:ActionScope);

    function getFilterMode:FilterMode;
    function getFindMode:FindMode;

    procedure doSyncSubToVideo;
    procedure doSyncVideoToSub(position:Double);
    procedure selectFocusedGridItem;
    function getCurrentRecNo:TList<integer>;
    function getSelectedRecNos:TList<integer>;
    function getFirstToCurrentRecNos:TList<integer>;
    function getCurrentToLastRecNos:TList<integer>;
    function getCurrentAndPrevRecNos:TList<integer>;
    function getCurrentAndNextRecNos:TList<integer>;

    property FileName:string read getFileName write setFileName;
  public
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;
  end;

  var
    frmMain:TfrmMain;

implementation
uses
    StrUtils
    ,TimeMoveOptions,ExportOptions,MergeOptions
    ,SplitOptions
    ,TextImportOptions
    ,AddAffixOptions,RemoveAffixOptions,DeleteAffixStringOptions
    ,TrimOptions,DeleteEmptyOptions
    ,SrtTimeMoveExecuter,TimeFromResizeOptions,TimeToResizeOptions,TimeScaleOptions
    ,About;

{$R *.dfm}

{ TfrmMain }

procedure TfrmMain.FormCreate(Sender: TObject);
begin
//    inherited;

//    globalConfig.LoadConfig;
//    refreshRecentFiles;
//    globalConfig.MainWindowStateManager.UpdatePropertyToWindow(self);
//    为计算窗体deltaHeight，改为在onshow中处理

    actNewFile.Execute;

    if FileExists(ParamStr(1)) then begin
        openFile(ParamStr(1));
    end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
    globalConfig.LoadConfig;
    refreshRecentFiles;
    globalConfig.MainWindowStateManager.UpdatePropertyToWindow(self);
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    self.Hide;

    syncTimer.Enabled:=false;
    syncTimer.OnTimer:=nil;
    displayingSubTimer.Enabled:=false;
    displayingSubTimer.OnTimer:=nil;
    actCloseVideoFile.Execute;
    Application.ProcessMessages;

    globalConfig.MainWindowStateManager.UpdateWindowToProperty(self);
    globalConfig.SaveConfig;
end;

procedure TfrmMain.refreshRecentFiles;
var
    fileName:string;
    barItem:TdxBarExtraPaneItem;
begin
    appMenu.ExtraPane.Items.Clear;

    for fileName in globalConfig.RecentFilesManager.Files do begin
        barItem:=appMenu.ExtraPane.Items.add;
        barItem.Text:=ExtractFileName(fileName);
        barItem.Text:=fileName;
    end;
end;

procedure TfrmMain.actExitExecute(Sender: TObject);
begin
    self.Close;
end;



{$REGION '构造与析构'}
    constructor TfrmMain.Create(AOwner: TComponent);
    begin
        inherited;
        srtCommander:=TSrtActionCommander.Create;
        dscSubtitle.DataSet:=srtCommander.SubTitle;
        dscHistory.DataSet:=srtCommander.HistoryManager.HistoryDataTable;

        globalConfig:=TGlobalConfig.Create;

        timeMan:=TSrtTimeManager.Create;
    end;

    destructor TfrmMain.Destroy;
    begin
        srtCommander.Free;
        globalConfig.Free;
        timeMan.Free;
        inherited;
    end;

{$ENDREGION}

{$REGION '属性相关'}
    function TfrmMain.getFileName: string;
    begin
        exit(statusBar.Panels[0].Text);
    end;

    procedure TfrmMain.setFileName(const Value: string);
    begin
        statusBar.Panels[0].Text:=Value;
    end;

{$ENDREGION}

{$REGION '历史记录'}
    //返回
    procedure TfrmMain.grdHistoryFocusedRecordChanged(
        Sender: TcxCustomGridTableView; APrevFocusedRecord,
        AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    begin
        srtCommander.ExecuteGotoCurrentHistoryStep;
        selectFocusedGridItem;

        actUndo.Enabled:=srtCommander.HistoryManager.CanGoToPrevStep;
        actRedo.Enabled:=srtCommander.HistoryManager.CanGoToNextStep;
    end;

    procedure TfrmMain.actUndoExecute(Sender: TObject);
    begin
        srtCommander.ExecuteGoToPrevHistoryStep;
    end;

    procedure TfrmMain.actRedoExecute(Sender: TObject);
    begin
        srtCommander.ExecuteGoToNextHistoryStep;
    end;

{$ENDREGION}

{$REGION '文件'}
    function TfrmMain.confirmSave: boolean;
    const
        confirmTitle:PWideChar='保存';
        confirmContent:PWideChar='当前字幕已更改，是否保存？';
    var
        userReply:integer;
    begin
        userReply:=MessageBox(self.Handle,confirmContent,confirmTitle,MB_YESNOCANCEL or MB_ICONQUESTION);
        if userReply=IDYES then begin
            actSaveFile.Execute;
            exit(not srtCommander.IsModified);
        end else if userReply=IDNO then begin
            exit(true);
        end else begin
            exit(false);
        end;
    end;

    procedure TfrmMain.actNewFileExecute(Sender: TObject);
    begin
        postPrevEditing;

        if (srtCommander.IsModified=false) or (confirmSave) then begin
            srtCommander.ExecuteNewFile;
            FileName:=ExtractFileName(srtCommander.FileName);
            actClearFilter.Execute;
            globalConfig.FileSessionManager.Reset;
        end;
    end;

    procedure TfrmMain.openFile(const fileName:string);
    begin
        srtCommander.ExecuteOpenFile(fileName);
        with grdSub.DataController do begin
            ClearSelection;
            if RowCount>0 then SelectRows(FocusedRecordIndex,FocusedRecordIndex);
        end;
        self.FileName:=ExtractFileName(srtCommander.FileName);
        globalConfig.FileSessionManager.Reset;
        globalConfig.RecentFilesManager.AddFileName(srtCommander.FileName);
        refreshRecentFiles;
    end;

    procedure TfrmMain.actOpenFileExecute(Sender: TObject);
    begin
        postPrevEditing;
        actClearFilter.Execute;

        if (srtCommander.IsModified=false) or (confirmSave) then begin
            dlgOpen.FileName:='';
            if dlgOpen.Execute then begin
                openFile(dlgOpen.FileName);
            end;
        end;
    end;

    procedure TfrmMain.appMenuExtraPaneItemClick(Sender: TObject; AIndex: Integer);
    var
        fileName:string;
    begin
        postPrevEditing;
        fileName:=appMenu.ExtraPane.Items[AIndex].Text;

        if (srtCommander.IsModified=false) or (confirmSave) then begin
            if FileExists(fileName) then begin
                openFile(fileName);
            end else begin
                MessageBox(self.Handle,RecentNotFoundContent,RecentNotFoundTitle,MB_OK or MB_ICONERROR);
                globalConfig.RecentFilesManager.RemoveFileName(fileName);
                refreshRecentFiles;
            end;
        end;
    end;

    procedure TfrmMain.actSaveFileExecute(Sender: TObject);
    begin
        if not srtCommander.IsNew then begin
            postPrevEditing;
            srtCommander.ExecuteSaveFile;
        end else begin
            actSaveFileAs.Execute;
        end;
    end;

    procedure TfrmMain.actSaveFileAsExecute(Sender: TObject);
    var
        fileExt:string;
    begin
        if(Self.FileName<>'') then begin
            fileExt:=ExtractFileExt(self.FileName);
            dlgSave.FileName:=LeftStr(Self.FileName,Length(Self.FileName)-length(fileExt)) + '-副本';
        end else begin
            dlgSave.FileName:='';
        end;

        if dlgSave.Execute then begin
            postPrevEditing;
            srtCommander.ExecuteSaveFileAs(dlgSave.FileName);
            globalConfig.RecentFilesManager.AddFileName(dlgSave.FileName);
            refreshRecentFiles;
            FileName:=ExtractFileName(srtCommander.FileName);
        end;
    end;

    procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    begin
        postPrevEditing;

        if srtCommander.IsModified then begin
            CanClose:=confirmSave;
        end;
    end;
{$ENDREGION}

{$REGION '播放器控制'}
    procedure TfrmMain.actPlayerPlayExecute(Sender: TObject);
    begin
        wmplayer.controls.play;
    end;

    procedure TfrmMain.actPlayerPauseExecute(Sender: TObject);
    begin
        wmplayer.controls.pause;
    end;

    procedure TfrmMain.actPlayerStopExecute(Sender: TObject);
    begin
        wmplayer.controls.stop;
    end;

    procedure TfrmMain.actPlayerFastBackwardExecute(Sender: TObject);
    begin
        wmplayer.controls.fastReverse;
    end;

    procedure TfrmMain.actPlayerFastForwardExecute(Sender: TObject);
    begin
        wmplayer.controls.fastForward;
    end;
{$ENDREGION}

{$REGION '参考视频文件'}
    procedure TfrmMain.actOpenVideoFileExecute(Sender: TObject);
    begin
        if dlgOpenVideoFile.Execute then begin
            wmplayer.URL:=dlgOpenVideoFile.FileName;
            wmplayer.controls.stop;
        end;
    end;

    procedure TfrmMain.actCloseVideoFileExecute(Sender: TObject);
    begin
        wmplayer.close;
        wmplayer.URL:='';
    end;

{$ENDREGION}

{$REGION '查找替换'}
    function TfrmMain.getFilterMode: FilterMode;
    begin
        if drpFilter.ItemIndex = 0 then begin
            exit(fmInclude);
        end else if drpFilter.ItemIndex = 1 then begin
            exit(fmStartsWith);
        end else if drpFilter.ItemIndex = 2 then begin
            exit(fmEndsWith);
        end else begin
            exit(fmInclude);
        end;
    end;

    procedure TfrmMain.actFilterExecute(Sender: TObject);
    begin
        srtCommander.ExecuteFilter(getFilterMode,chkFilterCaseSensitive.CurEditValue ,edtFilter.CurText);
        selectFocusedGridItem;
    end;

    procedure TfrmMain.actClearFilterExecute(Sender: TObject);
    begin
        drpFilter.ItemIndex:=0;
        srtCommander.ExecuteClearFilter;
        selectFocusedGridItem;
    end;

    procedure TfrmMain.edtFilterKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    begin
        if Key=13 then begin
            //Key:=0;
            actFilter.Execute;
        end;
    end;

    procedure TfrmMain.edtFilterChange(Sender: TObject);
    begin
        if chkRapidFilter.CurEditValue=true then begin
            actFilter.Execute;
        end;
    end;


/////////////查找//////////////

    function TfrmMain.getFindMode: FindMode;
    begin
        if drpFilter.ItemIndex = 0 then begin
            exit(fdInclude);
        end else if drpFilter.ItemIndex = 1 then begin
            exit(fdStartsWith);
        end else if drpFilter.ItemIndex = 2 then begin
            exit(fdEndsWith);
        end else begin
            exit(fdInclude);
        end;
    end;

    procedure TfrmMain.actFindFirstExecute(Sender: TObject);
    var
        found:boolean;
    begin
        found:=srtCommander.ExecuteFindFirst(getFindMode,chkFilterCaseSensitive.CurEditValue,edtFilter.CurText);
        if found then begin
            selectFocusedGridItem;
        end else begin
            MessageBox(self.Handle,FindNotFoundContent,FindNotFoundTitle,MB_OK or MB_ICONWARNING)
        end;
    end;

    procedure TfrmMain.actFindLastExecute(Sender: TObject);
    var
        found:boolean;
    begin
        found:=srtCommander.ExecuteFindLast(getFindMode,chkFilterCaseSensitive.CurEditValue,edtFilter.CurText);
        if found then begin
            selectFocusedGridItem;
        end else begin
            MessageBox(self.Handle,FindNotFoundContent,FindNotFoundTitle,MB_OK or MB_ICONWARNING)
        end;
    end;

    procedure TfrmMain.actFindPriorExecute(Sender: TObject);
    var
        found:boolean;
    begin
        found:=srtCommander.ExecuteFindPrior(getFindMode,chkFilterCaseSensitive.CurEditValue,edtFilter.CurText);
        if found then begin
            selectFocusedGridItem;
        end else begin
            MessageBox(self.Handle,FindNotFoundContent,FindNotFoundTitle,MB_OK or MB_ICONWARNING);
        end;
    end;

    procedure TfrmMain.actFindNextExecute(Sender: TObject);
    var
        found:boolean;
    begin
        found:=srtCommander.ExecuteFindNext(getFindMode,chkFilterCaseSensitive.CurEditValue,edtFilter.CurText);
        if found then begin
            selectFocusedGridItem;
        end else begin
            MessageBox(self.Handle,FindNotFoundContent,FindNotFoundTitle,MB_OK or MB_ICONWARNING);
        end;
    end;

    procedure TfrmMain.actReplaceCurrentExecute(Sender: TObject);
    var
        found:boolean;
    begin
        found:=srtCommander.ExecuteReplaceCurrent(getFindMode,chkFilterCaseSensitive.CurEditValue,edtFilter.CurText,edtReplace.CurText);
        if found then begin
            selectFocusedGridItem;
        end else begin
            MessageBox(self.Handle,FindNotFoundContent,FindNotFoundTitle,MB_OK or MB_ICONWARNING);
        end;
    end;

    procedure TfrmMain.actReplaceAllExecute(Sender: TObject);
    var
        found:boolean;
    begin
        found:=srtCommander.ExecuteReplaceAll(getFindMode,chkFilterCaseSensitive.CurEditValue,edtFilter.CurText,edtReplace.CurText);
        if found then begin
            selectFocusedGridItem;
        end else begin
            MessageBox(self.Handle,FindNotFoundContent,FindNotFoundTitle,MB_OK or MB_ICONWARNING);
        end;
    end;

{$ENDREGION}

{$REGION '文本处理'}
    procedure TfrmMain.actAddAffixExecute(Sender: TObject);
    var
        frm:TfrmAddAffixOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmAddAffixOptions.Create(nil);
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtcommander.ExecuteAddAffix(frm.Prefix,frm.Postfix,recNos);
            recNos.Free;
        end;
        frm.Free;
    end;

    procedure TfrmMain.actRemoveAffixExecute(Sender: TObject);
    var
        frm:TfrmRemoveAffixOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmRemoveAffixOptions.Create(nil);
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtcommander.ExecuteRemoveAffix(frm.Prefix,frm.Postfix,not frm.IgnoreCase,recNos);
            recNos.Free;
        end;
        frm.Free;
    end;

    procedure TfrmMain.actDeleteAffixStringExecute(Sender: TObject);
    var
        frm:TfrmDeleteAffixStringOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmDeleteAffixStringOptions.Create(nil);
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtcommander.ExecuteDeleteAffix(frm.DeletePrefixLength,frm.DeletePostfixLength,recNos);
            recNos.Free;
        end;
        frm.Free;
    end;

    procedure TfrmMain.actTrimWhiteSpaceExecute(Sender: TObject);
    var
        frm:TfrmTrimOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmTrimOptions.Create(nil);
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtcommander.ExecuteTrimWhiteSpace(frm.TrimLeft,frm.TrimRight,frm.TrimEmptyLine,recNos);
            recNos.Free;
        end;

        frm.Free;
    end;

    procedure TfrmMain.actDeleteEmptySubTitleExecute(Sender: TObject);
    var
        frm:TfrmDeleteEmptyOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmDeleteEmptyOptions.Create(nil);
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtCommander.ExecuteDeleteEmptySubTitle(frm.IncludeWhiteSpaceLine,recNos);
            recNos.Free;
        end;

        frm.Free;
    end;

{$ENDREGION}

{$REGION '编辑'}
    procedure TfrmMain.actResetNosExecute(Sender: TObject);
    begin
        postPrevEditing;
        SrtCommander.ExecuteResetNos;
    end;

    procedure TfrmMain.postPrevEditing;
    begin
        with srtCommander.SrtSubTitle do begin
            if State in[dsInsert,dsEdit] then begin
                Post;
            end;
        end;
        srtCommander.ExecuteAddCurrentToHistory('编辑');
    end;

    procedure TfrmMain.actNewRecExecute(Sender: TObject);
    begin
        postPrevEditing;
        srtCommander.ExecuteAppendAfterCurrent;
    end;

    procedure TfrmMain.actNewRecBeforeExecute(Sender: TObject);
    begin
        postPrevEditing;
        srtCommander.ExecuteInsertBeforeCurrent;
    end;

    procedure TfrmMain.actNewRecAndPlayerToTimeFromExecute(Sender: TObject);
    begin
        with srtCommander.SrtSubTitle do begin
            postPrevEditing;

            Append;
            CurrentTimeFrom:=srtCommander.ExecuteGetTimeString(wmplayer.controls.currentPosition);
            Post;
            srtCommander.ExecuteAddCurrentToHistory('新增为起始时间');
        end;
        selectFocusedGridItem;
    end;

    procedure TfrmMain.actPlayerToTimeFromExecute(Sender: TObject);
    begin
        with srtCommander do begin
            SrtSubTitle.Edit;
            SrtSubTitle.CurrentTimeFrom:=ExecuteGetTimeString(wmplayer.controls.currentPosition);
            SrtSubTitle.Post;
            srtCommander.ExecuteAddCurrentToHistory('设为起始时间');
        end;

        selectFocusedGridItem;
    end;

    procedure TfrmMain.actPlayerToTimeToExecute(Sender: TObject);
    begin
        postPrevEditing;
        with srtCommander do begin
            SrtSubTitle.Edit;
            SrtSubTitle.CurrentTimeTo:=ExecuteGetTimeString(wmplayer.controls.currentPosition);
            SrtSubTitle.Post;
            srtCommander.ExecuteAddCurrentToHistory('设为结束时间');
        end;
    end;

    procedure TfrmMain.actPlayerToTimeToAndNextRecExecute(Sender: TObject);
    begin
        actPlayerToTimeTo.Execute;
        with srtCommander do begin
            if SrtSubTitle.RecNo<SrtSubTitle.RecordCount then begin
                SrtSubTitle.Next;
                SelectFocusedGridItem;
            end;
        end;
    end;

    procedure TfrmMain.actUpdateRecExecute(Sender: TObject);
    begin
        postPrevEditing;
    end;

    procedure TfrmMain.actCancelUpdateRecExecute(Sender: TObject);
    begin
        srtCommander.SrtSubTitle.Cancel;
    end;

    procedure TfrmMain.actDeleteRecExecute(Sender: TObject);
    const
        confirmTitle:PWideChar='删除记录';
        confirmContent:PWideChar='确实要删除选定记录吗？';
    var
        userReply:integer;
    begin
        userReply:=MessageBox(self.Handle,confirmContent,confirmTitle,MB_OKCANCEL or MB_ICONWARNING);
        if userReply=ID_OK then begin
            grdSub.DataController.DeleteSelection;
            srtCommander.ExecuteAddCurrentToHistory('删除记录');
        end;
    end;

{$ENDREGION}

{$REGION '自动保存'}
    procedure TfrmMain.actAutoSaveExecute(Sender: TObject);
    begin
        srtCommander.AutoSave:=btnAutoSave.Down;

        postPrevEditing;
        srtCommander.ExecuteAutoSave;
    end;

    procedure TfrmMain.dscSourceDataChange(Sender: TObject; Field: TField);
    begin
        srtCommander.ExecuteAutoSave;
    end;

{$ENDREGION}

{$REGION '调整时间'}
    procedure TfrmMain.actMoveTimeExecute(Sender: TObject);
    var
        frm:TfrmTimeMoveOptions;
        recNos:TList<integer>;
    begin
        frm:=TFrmTimeMoveOptions.Create(nil);
        frm.edtOffsetTime.OnKeyPress:=edtTimeKeyPress;
        frm.edtOffsetTime.Properties.OnValidate:=edtTimePropertiesValidate;

        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtCommander.ExecuteMoveTime(
                frm.Direction,
                frm.OffsetTime,
                recNos
            );

            recNos.Free;
        end;

        frm.Free;
    end;

    procedure TfrmMain.actResizeTimeFromExecute(Sender: TObject);
    var
        frm:TfrmTimeFromResizeOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmTimeFromResizeOptions.Create(nil);
        frm.edtOffsetTime.OnKeyPress:=edtTimeKeyPress;
        frm.edtOffsetTime.Properties.OnValidate:=edtTimePropertiesValidate;

        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtCommander.ExecuteResizeTimeFrom(
                frm.Direction,
                frm.OffsetTime,
                recNos
            );

            recNos.Free;
        end;

        frm.Free;
    end;

    procedure TfrmMain.actResizeTimeToExecute(Sender: TObject);
    var
        frm:TfrmTimeToResizeOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmTimeToResizeOptions.Create(nil);
        frm.edtOffsetTime.OnKeyPress:=edtTimeKeyPress;
        frm.edtOffsetTime.Properties.OnValidate:=edtTimePropertiesValidate;

        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtCommander.ExecuteResizeTimeTo(
                frm.Direction,
                frm.OffsetTime,
                recNos
            );

            recNos.Free;
        end;

        frm.Free;
    end;

    procedure TfrmMain.actScaleTimeExecute(Sender: TObject);
    var
        frm:TfrmTimeScaleOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmTimeScaleOptions.Create(nil);
        frm.edtNewTimeMin.OnKeyPress:=edtTimeKeyPress;
        frm.edtNewTimeMax.OnKeyPress:=edtTimeKeyPress;
        frm.edtNewTimeMin.Properties.OnValidate:=edtTimePropertiesValidate;
        frm.edtNewTimeMax.Properties.OnValidate:=edtTimePropertiesValidate;

        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtCommander.ExecuteScaleTime(
                frm.NewTimeMin,
                frm.NewTimeMax,
                recNos
            );

            recNos.Free;
        end;

        frm.Free;
    end;

{$ENDREGION}

{$REGION '平移到播放器时间'}
    procedure TfrmMain.moveToPlayerTime(const scope: ActionScope);
    var
        RecNos:TList<Integer>;
    begin
        postPrevEditing;

        RecNos:=getRecNos(scope);
        srtCommander.ExecuteMoveToPlayerTime(wmplayer.controls.currentPosition,RecNos);
        RecNos.Free;
    end;

    procedure TfrmMain.actMoveCurrentToPlayerTimeExecute(Sender: TObject);
    begin
        moveToPlayerTime(asCurrent);
    end;

    procedure TfrmMain.actMoveFirstToCurrentToPlayerTimeExecute(Sender: TObject);
    begin
        moveToPlayerTime(asFirstToCurrent);
    end;

    procedure TfrmMain.actMoveCurrentToLastToPlayerTimeExecute(Sender: TObject);
    begin
        moveToPlayerTime(asCurrentToLast);
    end;

    procedure TfrmMain.actMoveSelectedToPlayerTimeExecute(Sender: TObject);
    begin
        moveToPlayerTime(asSelected);
    end;

    procedure TfrmMain.actMoveAllToPlayerTimeExecute(Sender: TObject);
    begin
        moveToPlayerTime(asEntire);
    end;
{$ENDREGION}

{$REGION '时间微调'}
    procedure TfrmMain.btnTimeFromInc1000Click(Sender: TObject);
    begin
        if edtTimeFrom.EditingText<>'' then begin
            timeMan.Parse(edtTimeFrom.Text);
            timeMan.AddSeconds(1);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeFrom:=timeMan.ToString;
            end;
        end;
    end;

    procedure TfrmMain.btnTimeFromInc100Click(Sender: TObject);
    begin
        if edtTimeFrom.EditingText<>'' then begin
            timeMan.Parse(edtTimeFrom.Text);
            timeMan.AddMilliSeconds(100);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeFrom:=timeMan.ToString;
            end;
        end;
    end;

    procedure TfrmMain.btnTimeFromDec1000Click(Sender: TObject);
    begin
        if edtTimeFrom.EditingText<>'' then begin
            timeMan.Parse(edtTimeFrom.Text);
            timeMan.AddSeconds(-1);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeFrom:=timeMan.ToString;
            end;
        end;
    end;

    procedure TfrmMain.btnTimeFromDec100Click(Sender: TObject);
    begin
        if edtTimeFrom.EditingText<>'' then begin
            timeMan.Parse(edtTimeFrom.Text);
            timeMan.AddMilliSeconds(-100);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeFrom:=timeMan.ToString;
            end;
        end;
    end;

    procedure TfrmMain.btnTimeToInc1000Click(Sender: TObject);
    begin
        if edtTimeTo.EditingText<>'' then begin
            timeMan.Parse(edtTimeTo.Text);
            timeMan.AddSeconds(1);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeTo:=timeMan.ToString;
            end;
        end;
    end;

    procedure TfrmMain.btnTimeToInc100Click(Sender: TObject);
    begin
        if edtTimeTo.EditingText<>'' then begin
            timeMan.Parse(edtTimeTo.Text);
            timeMan.AddMilliSeconds(100);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeTo:=timeMan.ToString;
            end;
        end;
    end;

    procedure TfrmMain.btnTimeToDec1000Click(Sender: TObject);
    begin
        if edtTimeTo.EditingText<>'' then begin
            timeMan.Parse(edtTimeTo.Text);
            timeMan.AddSeconds(-1);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeTo:=timeMan.ToString;
            end;
        end;
    end;

    procedure TfrmMain.btnTimeToDec100Click(Sender: TObject);
    begin
        if edtTimeTo.EditingText<>'' then begin
            timeMan.Parse(edtTimeTo.Text);
            timeMan.AddMilliSeconds(-100);
            with srtCommander.SrtSubTitle do begin
                Edit;
                CurrentTimeTo:=timeMan.ToString;
            end;
        end;
    end;
{$ENDREGION}

{$REGION '组合拆分'}
    procedure TfrmMain.actCombineMultiLineSelectedExecute(Sender: TObject);
    var
        recNos:TList<integer>;
    begin
        postPrevEditing;

        recNos:=getSelectedRecNos;
        srtCommander.ExecuteCombineMultiLine(recNos);
        recNos.Free;
    end;

    procedure TfrmMain.actCombineMultiLineUpExecute(Sender: TObject);
    var
        recNos:TList<integer>;
    begin
        postPrevEditing;

        recNos:=getCurrentAndPrevRecNos;
        srtCommander.ExecuteCombineMultiLine(recNos);
        recNos.Free;
    end;

    procedure TfrmMain.actCombineMultiLineDownExecute(Sender: TObject);
    var
        recNos:TList<integer>;
    begin
        postPrevEditing;

        recNos:=getCurrentAndNextRecNos;
        srtCommander.ExecuteCombineMultiLine(recNos);
        recNos.Free;
    end;

    procedure TfrmMain.actCombineSingleLineSelectedExecute(Sender: TObject);
    var
        recNos:TList<integer>;
    begin
        postPrevEditing;

        recNos:=getSelectedRecNos;
        srtCommander.ExecuteCombineSingleLine(recNos);
        recNos.Free;
    end;

    procedure TfrmMain.actCombineSingleLineUpExecute(Sender: TObject);
    var
        recNos:TList<integer>;
    begin
        postPrevEditing;

        recNos:=getCurrentAndPrevRecNos;
        srtCommander.ExecuteCombineSingleLine(recNos);
        recNos.Free;
    end;

    procedure TfrmMain.actCombineSingleLineDownExecute(Sender: TObject);
   var
        recNos:TList<integer>;
    begin
        postPrevEditing;

        recNos:=getCurrentAndNextRecNos;
        srtCommander.ExecuteCombineSingleLine(recNos);
        recNos.Free;
    end;

    procedure TfrmMain.actSplitSubExecute(Sender: TObject);
    var
        frm:TfrmSplitOptions;
        list:TList<string>;
    begin
        if srtCommander.SubTitle.Eof=false then begin
            postPrevEditing;

            frm:=TfrmSplitOptions.Create(nil);
            frm.Text:=srtCommander.SrtSubTitle.CurrentContent;
            frm.ShowModal;
            if frm.ModalResult=mrOK then begin
                list:=frm.GetSplitedSub;
                srtCommander.ExecuteSplitCurrent(list);
                list.Free;
            end;

            frm.Free;
        end;
    end;

{$ENDREGION}

{$REGION '时间同步'}
    procedure TfrmMain.actSyncSubToVideoAutoExecute(Sender: TObject);
    begin
        if btnSyncSubToVideo.Down then begin
            grdSub.OnFocusedRecordChanged:=grdSubFocusedRecordChanged;
        end else begin
            grdSub.OnFocusedRecordChanged:=nil;
        end;
    end;

    procedure TfrmMain.actSyncSubToVideoNowExecute(Sender: TObject);
    begin
        doSyncSubToVideo;
       with globalConfig.FileSessionManager do begin
           PrevSubToVideoPosition:=wmplayer.controls.currentPosition;
           PrevSubToVideoAvailable:=true;
       end;
    end;

    procedure TfrmMain.actSyncVideoToSubAutoExecute(Sender: TObject);
    begin
        if SyncVideoToSub then begin
            syncTimer.OnTimer:=syncTimerTimer;
            syncTimer.Enabled:=true;
            //wmplayer.OnPositionChange:=wmplayerPositionChange;
        end else begin
            syncTimer.Enabled:=false;
            syncTimer.OnTimer:=nil;
            //wmplayer.OnPositionChange:=nil;
        end;
    end;

    procedure TfrmMain.actSyncVideoToSubNowExecute(Sender: TObject);
    begin
       doSyncVideoToSub(wmplayer.controls.currentPosition);
       with globalConfig.FileSessionManager do begin
           PrevVideoToSubPosition:=wmplayer.controls.currentPosition;
           PrevVideoToSubAvailable:=true;
       end;
    end;

    procedure TfrmMain.grdSubFocusedRecordChanged(Sender: TcxCustomGridTableView;
        APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
        ANewItemRecordFocusingChanged: Boolean);
     begin
        doSyncSubToVideo;
    end;

    procedure TfrmMain.syncTimerTimer(Sender: TObject);
    begin
        doSyncVideoToSub(wmplayer.controls.currentPosition);
    end;

    procedure TfrmMain.wmplayerPositionChange(ASender: TObject; oldPosition,
      newPosition: Double);
    begin
        doSyncVideoToSub(newPosition);
    end;

    procedure TfrmMain.doSyncSubToVideo;
    var
        posChangeEvent:TWindowsMediaPlayerPositionChange;
        currentTimeFrom:Double;
    begin
        if not srtCommander.SrtSubTitle.Eof then begin
            posChangeEvent:=wmplayer.OnPositionChange;
            wmplayer.OnPositionChange:=nil;

            currentTimeFrom:=srtCommander.ExecuteGetCurrentTimeFromSeconds;
            wmplayer.controls.currentPosition:=currentTimeFrom;
            if wmplayer.controls.currentPosition<=currentTimeFrom then
                wmplayer.controls.currentPosition:=currentTimeFrom+0.01;

            wmplayer.OnPositionChange:=posChangeEvent;
        end;
    end;

    procedure TfrmMain.doSyncVideoToSub(position: Double);
    var
        recChangeEvent:TcxGridFocusedRecordChangedEvent;
    begin
        if wmplayer.openState<>wmposPlaylistOpenNoMedia then begin
            recChangeEvent:=grdSub.OnFocusedRecordChanged;
            grdSub.OnFocusedRecordChanged:=nil;

            srtCommander.ExecuteLocateTime(position);

            selectFocusedGridItem;
            grdSub.OnFocusedRecordChanged:=recChangeEvent;
        end;
    end;

    procedure TfrmMain.displayingSubTimerTimer(Sender: TObject);
    begin
        mnoDisplayingSubtitle.Text:=srtCommander.ExecuteGetDisplayingSubtitle(wmplayer.controls.currentPosition);
    end;

    procedure TfrmMain.actPrevVideoToSubExecute(Sender: TObject);
    begin
        with globalConfig.FileSessionManager do begin
            if PrevVideoToSubAvailable then begin
                wmplayer.controls.currentPosition:=PrevVideoToSubPosition;
                doSyncVideoToSub(PrevVideoToSubPosition);
            end;
        end;
    end;

    procedure TfrmMain.actPrevSubToVideoExecute(Sender: TObject);
    begin
        with globalConfig.FileSessionManager do begin
            if PrevSubToVideoAvailable then begin
                wmplayer.controls.currentPosition:=PrevSubToVideoPosition;
                doSyncVideoToSub(PrevSubToVideoPosition);
            end;
        end;
    end;

{$ENDREGION}

{$REGION '导入导出'}
    procedure TfrmMain.actImportExecute(Sender: TObject);
    begin
        if dlgImport.Execute then begin
            postPrevEditing;
            srtCommander.ExecuteImportFile(dlgImport.FileName);
        end;
    end;

    procedure TfrmMain.actImportTextExecute(Sender: TObject);
    var
        frm:TfrmTextImportOptions;
    begin
        frm:=TfrmTextImportOptions.Create(nil);
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;
            srtCommander.ExecuteImportText(frm.Text, frm.Separator);
        end;
        frm.Free;
    end;

    procedure TfrmMain.actExportExecute(Sender: TObject);
    var
        frm:TfrmExportOptions;
        recNos:TList<integer>;
    begin
        frm:=TfrmExportOptions.Create(nil);
        dlgExport.FileName:=LeftStr(FileName,Length(FileName)-Length(ExtractFileExt(FileName)));
        frm.ExportDialog:=dlgExport;
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;

            recNos:=getRecNos(frm.Scope);
            srtCommander.ExecuteExportFile(frm.FileName,recNos);
            recNos.Free;
        end;

        frm.Free;
    end;

    procedure TfrmMain.actMergeBySequenceExecute(Sender: TObject);
    var
        frm:TfrmMergeOptions;
    begin
        frm:=TfrmMergeOptions.Create(nil);
        frm.ImportDialog:=self.dlgImport;
        frm.SubCaption:=actMergeBySequence.Caption;
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;
            srtCommander.ExecuteMergeBySequence(
                frm.TimeMergeMode
                ,frm.Separator
                ,frm.MergePosition
                ,frm.ImportUnmatched
                ,frm.ImportFileName
            );
        end;

        frm.Free;
    end;

    procedure TfrmMain.actMergeByNoExecute(Sender: TObject);
    var
        frm:TfrmMergeOptions;
    begin
        frm:=TfrmMergeOptions.Create(nil);
        frm.ImportDialog:=self.dlgImport;
        frm.SubCaption:=actMergeByNo.Caption;
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;
            srtCommander.ExecuteMergeByNo(
                frm.TimeMergeMode
                ,frm.Separator
                ,frm.MergePosition
                ,frm.ImportUnmatched
                ,frm.ImportFileName
            );
        end;

        frm.Free;
    end;

    procedure TfrmMain.actMergeByTimeFromExecute(Sender: TObject);
    var
        frm:TfrmMergeOptions;
    begin
        frm:=TfrmMergeOptions.Create(nil);
        frm.ImportDialog:=self.dlgImport;
        frm.SubCaption:=actMergeByTimeFrom.Caption;
        frm.ShowModal;
        if frm.ModalResult=mrOK then begin
            postPrevEditing;
            srtCommander.ExecuteMergeByTimeFrom(
                frm.TimeMergeMode
                ,frm.Separator
                ,frm.MergePosition
                ,frm.ImportUnmatched
                ,frm.ImportFileName
            );
        end;

        frm.Free;
    end;

{$ENDREGION}

{$REGION '选区相关'}
    procedure TfrmMain.selectFocusedGridItem;
    var
        i:integer;
    begin
        with grdSub.DataController do begin
            for i := Controller.SelectedRecordCount - 1 downto 0 do begin
                if Controller.SelectedRecords[i].Index<>FocusedRowIndex then begin
                    Controller.SelectedRecords[i].Selected:=false;
                end;
            end;

            if FocusedRowIndex>=0 then SelectRows(FocusedRowIndex,FocusedRowIndex);
        end;
    end;

	{rocedure TfrmMain.focusSelectedGridItem;
  	begin
        with grdSub.DataController do begin
            if Controller.SelectedRecordCount>0 then FocusedRecordIndex:=Controller.SelectedRecords[0].Index;
        end;
  	end;}

    function TfrmMain.getCurrentRecNo: TList<integer>;
    var
        RecNos:TList<integer>;
    begin
        RecNos:=TList<integer>.Create;
        if grdSub.DataController.FocusedRecordIndex>=0 then begin
            RecNos.Add(grdSub.DataController.FocusedRecordIndex+1);
        end;
        Exit(RecNos);
    end;

    function TfrmMain.getSelectedRecNos: TList<integer>;
    var
        RecNos:TList<integer>;
        i:integer;
    begin
        RecNos:=TList<integer>.Create;

        with grdSub.DataController.Controller do begin
            for i := 0 to SelectedRecordCount-1 do begin
                recNos.Add(SelectedRecords[i].RecordIndex+1);
            end;
        end;

        exit(RecNos);
    end;

    function TfrmMain.getFirstToCurrentRecNos: TList<integer>;
    var
        RecNos:TList<integer>;
        i:integer;
    begin
        RecNos:=TList<integer>.Create;
        if grdSub.DataController.FocusedRecordIndex>=0 then begin
            for i := 0 to grdSub.DataController.FocusedRecordIndex do begin
                RecNos.Add(i+1);
            end;
        end;
        exit(RecNos);
    end;

    function TfrmMain.getCurrentToLastRecNos: TList<integer>;
    var
        RecNos:TList<integer>;
        i:integer;
    begin
        RecNos:=TList<integer>.Create;
        if grdSub.DataController.FocusedRecordIndex>=0 then begin
            for i := grdSub.DataController.FocusedRecordIndex to grdSub.DataController.RecordCount-1 do begin
                RecNos.Add(i+1);
            end;
        end;
        exit(RecNos);
    end;

    function TfrmMain.getCurrentAndPrevRecNos: TList<integer>;
    var
        RecNos:TList<integer>;
        currentRecNo:Integer;
    begin
        RecNos:=TList<integer>.Create;
        if grdSub.DataController.FocusedRecordIndex>=0 then begin

            currentRecNo:=grdSub.DataController.FocusedRecordIndex+1;
            RecNos.Add(currentRecNo);

            if currentRecNo>1 then begin
                RecNos.Insert(0,currentRecNo-1);
            end;
        end;
        exit(RecNos);
    end;

    function TfrmMain.getCurrentAndNextRecNos: TList<integer>;
    var
        RecNos:TList<integer>;
        currentRecNo:Integer;
    begin
        RecNos:=TList<integer>.Create;
        if grdSub.DataController.FocusedRecordIndex>=0 then begin

            currentRecNo:=grdSub.DataController.FocusedRecordIndex+1;
            RecNos.Add(currentRecNo);

            if grdSub.DataController.RecordCount>currentRecNo then begin
                RecNos.Add(currentRecNo+1);
            end;
        end;
        exit(RecNos);
    end;

    function TfrmMain.getRecNos(const scope:ActionScope): TList<integer>;
    begin
        case scope of
            asCurrent: Exit(getCurrentRecNo);
            asSelected: exit(getSelectedRecNos);
            asEntire: exit(srtCommander.SrtSubTitle.GetAllRecNos);
            asFirstToCurrent: Exit(getFirstToCurrentRecNos);
            asCurrentToLast: Exit(getCurrentToLastRecNos);
            asCurrentAndPrev: Exit(getCurrentAndPrevRecNos);
            asCurrentAndNext: Exit(getCurrentAndNextRecNos);
            else raise Exception.Create('Unexpected Scope');
        end;
    end;

{$ENDREGION}

{$REGION '帮助'}
    procedure TfrmMain.actAboutExecute(Sender: TObject);
    var
        frm:TfrmAbout;
    begin
        frm:=TfrmAbout.Create(nil);
        frm.ShowModal;
        frm.Free;
    end;

    procedure TfrmMain.actSubHomeExecute(Sender: TObject);
    begin
        ShellExecute(self.Handle,'open','http://mjpclab.net/submaster',nil,nil,sw_shownormal);
    end;

    procedure TfrmMain.actPcLabHomeExecute(Sender: TObject);
    begin
        ShellExecute(self.Handle,'open','http://mjpclab.net/',nil,nil,sw_shownormal);
    end;
{$ENDREGION}

{$REGION '移动光标'}
    procedure TfrmMain.actFocusUpExecute(Sender: TObject);
    begin
        with grdSub.DataController do begin
            if FocusedRowIndex>0 then begin
                FocusedRowIndex:=FocusedRowIndex-1;
                selectFocusedGridItem;
                if mmoContent.Focused then mmoContent.SelectAll;
            end;
        end;
    end;

    procedure TfrmMain.actFocusDownExecute(Sender: TObject);
    begin
        with grdSub.DataController do begin
            if FocusedRowIndex<RecordCount-1 then begin
                FocusedRowIndex:=FocusedRowIndex+1;
                selectFocusedGridItem;
                if mmoContent.Focused then mmoContent.SelectAll;
            end;
        end;
    end;

{$ENDREGION}


end.
