unit TimeFromResizeOptions;

interface

uses
  CommonDefs,SrtTimeManager,SrtTimeMoveExecuterBase,SrtTimeFromResizeExecuter,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  dxRibbonForm, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue, dxSkinCaramel,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, cxControls, cxContainer, cxEdit,
  cxGroupBox, dxSkinsdxRibbonPainter, cxClasses, dxRibbon, ExtCtrls, jpeg,
  cxImage, cxRadioGroup, Menus, StdCtrls, cxButtons, cxTextEdit, cxMaskEdit,
  cxGraphics, cxLookAndFeels;

type
  TfrmTimeFromResizeOptions = class(TdxRibbonForm)
    grpOffsetTime: TcxGroupBox;
    grpDirection: TcxRadioGroup;
    cxGroupBox1: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    edtOffsetTime: TcxMaskEdit;
    grpScope: TcxRadioGroup;
    Image1: TImage;
  private
    { Private declarations }
    FOffsetTime:TSrtTimeManager;
    function getScope: ActionScope;
    function getDirection: TSrtTimeMoveDirection;
    function getOffsetTime: TSrtTimeManager;
  public
    { Public declarations }
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;

    property Scope:ActionScope read getScope;
    property Direction:TSrtTimeMoveDirection read getDirection;
    property OffsetTime:TSrtTimeManager read getOffsetTime;
  end;

implementation

{$R *.dfm}

{ TfrmTimeMoveOptions }

constructor TfrmTimeFromResizeOptions.Create(AOwner: TComponent);
begin
    inherited;
    FOffsetTime:=TSrtTimeManager.Create;
end;

destructor TfrmTimeFromResizeOptions.Destroy;
begin
    FOffsetTime.Free;
    inherited;
end;

function TfrmTimeFromResizeOptions.getScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmTimeFromResizeOptions.getDirection: TSrtTimeMoveDirection;
begin
    case grpDirection.ItemIndex of
        0: exit(tmdBackward);
        1: exit(tmdForward);
        else raise Exception.Create('Unexpected Direction');
    end;
end;

function TfrmTimeFromResizeOptions.getOffsetTime: TSrtTimeManager;
begin
    FOffsetTime.Parse(edtOffsetTime.EditingText);
    exit(FOffsetTime);
end;

end.
