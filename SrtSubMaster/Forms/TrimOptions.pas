unit TrimOptions;

interface

uses
    CommonDefs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2007Black, cxGroupBox,
  cxRadioGroup, Menus, StdCtrls, cxButtons, cxCheckGroup;

type
  TfrmTrimOptions = class(TForm)
    grpScope: TcxRadioGroup;
    grpTrimOptions: TcxCheckGroup;
    grpOperation: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
  private const
    indexTrimLeft:Integer=0;
    indexTrimRight:Integer=1;
    indexTrimEmptyLine:Integer=2;
  private
    function getScope: ActionScope;
    function getTrimEmptyLine: boolean;
    function getTrimLeft: Boolean;
    function getTrimRight: boolean;
    { Private declarations }
  public
    { Public declarations }
    property Scope:ActionScope read getScope;
    property TrimLeft:Boolean read getTrimLeft;
    property TrimRight:boolean read getTrimRight;
    property TrimEmptyLine:boolean read getTrimEmptyLine;
  end;


implementation

{$R *.dfm}

{ TfrmTrimOptions }

function TfrmTrimOptions.getScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmTrimOptions.getTrimLeft: Boolean;
begin
    exit(grpTrimOptions.States[indexTrimLeft]=cbsChecked);
end;

function TfrmTrimOptions.getTrimRight: boolean;
begin
    exit(grpTrimOptions.States[indexTrimRight]=cbsChecked);
end;

function TfrmTrimOptions.getTrimEmptyLine: boolean;
begin
    exit(grpTrimOptions.States[indexTrimEmptyLine]=cbsChecked);
end;

end.
