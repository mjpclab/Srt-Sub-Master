inherited frmMain: TfrmMain
  Caption = 'Srt Sub Master'
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inherited dxRibbon: TdxRibbon
    inherited tabGeneral: TdxRibbonTab
      Index = 0
    end
    inherited tabEdit: TdxRibbonTab
      Index = 1
    end
    inherited tabAdjust: TdxRibbonTab
      Index = 2
    end
    inherited tabCombine: TdxRibbonTab
      Index = 3
    end
    inherited tabTextProcess: TdxRibbonTab [4]
      Index = 4
    end
    inherited tabFindReplace: TdxRibbonTab [5]
      Index = 5
    end
    inherited tabImport: TdxRibbonTab
      Index = 6
    end
    inherited tabHelp: TdxRibbonTab
      Index = 7
    end
  end
  inherited pnlMain: TPanel
    inherited grpEdit: TcxGroupBox
      inherited edtTimeTo: TcxDBMaskEdit
        Properties.OnValidate = nil
        Style.IsFontAssigned = True
      end
      inherited edtTimeFrom: TcxDBMaskEdit
        Properties.OnValidate = nil
        Style.IsFontAssigned = True
      end
      inherited btnTimeFromInc1000: TcxButton
        OnClick = btnTimeFromInc1000Click
      end
      inherited btnTimeFromDec1000: TcxButton
        OnClick = btnTimeFromDec1000Click
      end
      inherited btnTimeFromInc100: TcxButton
        OnClick = btnTimeFromInc100Click
      end
      inherited btnTimeFromDec100: TcxButton
        OnClick = btnTimeFromDec100Click
      end
      inherited btnTimeToInc1000: TcxButton
        OnClick = btnTimeToInc1000Click
      end
      inherited btnTimeToDec1000: TcxButton
        OnClick = btnTimeToDec1000Click
      end
      inherited btnTimeToInc100: TcxButton
        OnClick = btnTimeToInc100Click
      end
      inherited btnTimeToDec100: TcxButton
        OnClick = btnTimeToDec100Click
      end
    end
    inherited cxPageControl1: TcxPageControl
      inherited tabPreview: TcxTabSheet
        inherited wmplayer: TWindowsMediaPlayer
          ControlData = {
            000300000800000000000500000000000000F03F030000000000050000000000
            0000000008000200000000000300010000000B00FFFF0300000000000B00FFFF
            08000200000000000300320000000B00000008000A000000660075006C006C00
            00000B0000000B0000000B00FFFF0B00FFFF0B00000008000200000000000800
            020000000000080002000000000008000200000000000B000000122400009A15
            0000}
        end
      end
      inherited tabHistory: TcxTabSheet
        inherited gridHistory: TcxGrid
          Width = 349
          Height = 248
          inherited grdHistory: TcxGridDBTableView
            OnFocusedRecordChanged = grdHistoryFocusedRecordChanged
            OptionsView.HeaderAutoHeight = True
          end
        end
      end
    end
  end
  inherited barManager: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited barFrequentlyUsed: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnNewFile'
        end
        item
          Visible = True
          ItemName = 'btnOpenFile'
        end
        item
          Visible = True
          ItemName = 'btnSaveFile'
        end
        item
          Visible = True
          ItemName = 'btnSaveFileAs'
        end
        item
          Visible = True
          ItemName = 'btnOpenVideoFile'
        end
        item
          Visible = True
          ItemName = 'btnUndo'
        end
        item
          Visible = True
          ItemName = 'btnRedo'
        end>
    end
    inherited barEdit: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 124
      FloatClientHeight = 448
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnNewRec'
        end
        item
          Visible = True
          ItemName = 'btnNewRecAndPlayerToTimeFrom'
        end
        item
          Visible = True
          ItemName = 'btnPlayerToTimeFrom'
        end
        item
          Visible = True
          ItemName = 'btnPlayerToTimeTo'
        end
        item
          Visible = True
          ItemName = 'btnPlayerToTimeToAndNextRec'
        end
        item
          Visible = True
          ItemName = 'btnUpdateRec'
        end
        item
          Visible = True
          ItemName = 'btnCancelUpdateRec'
        end
        item
          Visible = True
          ItemName = 'btnDeleteRec'
        end>
    end
    inherited barVideoFile: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 195
      FloatClientWidth = 64
      FloatClientHeight = 397
    end
    inherited barHelp: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 101
      FloatClientHeight = 168
    end
    inherited barAdjustTime: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 88
      FloatClientHeight = 280
    end
    inherited barImportExport: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 64
      FloatClientHeight = 168
    end
    inherited barFile: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 52
      FloatClientHeight = 229
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnNewFile'
        end
        item
          Visible = True
          ItemName = 'btnOpenFile'
        end
        item
          Visible = True
          ItemName = 'btnSaveFile'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnSaveFileAs'
        end>
    end
    inherited barReplace: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 130
      FloatClientHeight = 80
    end
    inherited barFilter: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 133
      FloatClientHeight = 184
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 133
          Visible = True
          ItemName = 'drpFilter'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 133
          Visible = True
          ItemName = 'edtFilter'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 117
          Visible = True
          ItemName = 'chkFilterCaseSensitive'
        end
        item
          Visible = True
          ItemName = 'btnLargeFilter'
        end
        item
          Visible = True
          ItemName = 'btnLargeFind'
        end>
    end
    inherited barAffix: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 107
      FloatClientHeight = 168
    end
    inherited barMerge: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 100
      FloatClientHeight = 168
    end
    inherited barExit: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 550
      FloatClientWidth = 51
      FloatClientHeight = 56
    end
    inherited barEditUtility: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 64
      FloatClientHeight = 173
    end
    inherited barSynchronize: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 116
      FloatClientHeight = 149
    end
    inherited barTextOther: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 88
      FloatClientHeight = 112
    end
    inherited barCombineMultiLine: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 152
      FloatClientHeight = 112
    end
    inherited barCombineSingleLine: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 152
      FloatClientHeight = 112
    end
    inherited barSplitSub: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 88
      FloatClientHeight = 56
    end
    inherited barMoveToPlayerTime: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 212
      FloatClientHeight = 168
    end
    inherited btnOpenVideoFile: TdxBarLargeButton
      Action = actOpenVideoFile
    end
    inherited btnCloseVideoFile: TdxBarLargeButton
      Action = actCloseVideoFile
    end
    inherited btnSyncSubToVideo: TdxBarButton
      Action = actSyncSubToVideoAuto
    end
    inherited btnSyncVideoToSub: TdxBarButton
      Action = actSyncVideoToSubAuto
    end
    inherited btnNewRec: TdxBarLargeButton
      Action = actNewRec
    end
    inherited btnNewRecAndPlayerToTimeFrom: TdxBarLargeButton
      Action = actNewRecAndPlayerToTimeFrom
    end
    inherited btnPlayerToTimeTo: TdxBarLargeButton
      Action = actPlayerToTimeTo
      Caption = #35774#20026#32467#26463#26102#38388
    end
    inherited btnUpdateRec: TdxBarLargeButton
      Action = actUpdateRec
    end
    inherited btnAbout: TdxBarLargeButton
      Action = actAbout
      ImageIndex = 51
    end
    inherited btnHomePage: TdxBarLargeButton
      Action = actSubHome
    end
    inherited btnCancelUpdateRec: TdxBarLargeButton
      Action = actCancelUpdateRec
    end
    inherited btnDeleteRec: TdxBarLargeButton
      Action = actDeleteRec
    end
    inherited btnMoveTime: TdxBarLargeButton
      Action = actMoveTime
    end
    inherited btnImport: TdxBarLargeButton
      Action = actImport
    end
    inherited btnNewFile: TdxBarLargeButton
      Action = actNewFile
    end
    inherited btnOpenFile: TdxBarLargeButton
      Action = actOpenFile
    end
    inherited btnSaveFile: TdxBarLargeButton
      Action = actSaveFile
    end
    inherited btnSaveFileAs: TdxBarLargeButton
      Action = actSaveFileAs
    end
    inherited btnResetNos: TdxBarLargeButton
      Action = actResetNos
    end
    inherited btnExport: TdxBarLargeButton
      Action = actExport
    end
    inherited btnFilter: TdxBarButton
      Action = actFilter
    end
    inherited btnClearFilter: TdxBarButton
      Action = actClearFilter
    end
    inherited btnAddAffix: TdxBarLargeButton
      Action = actAddAffix
    end
    inherited btnRemoveAffix: TdxBarLargeButton
      Action = actRemoveAffix
    end
    inherited btnDeleteAffixString: TdxBarLargeButton
      Action = actDeleteAffixString
    end
    inherited btnPlayerToTimeFrom: TdxBarLargeButton
      Action = actPlayerToTimeFrom
    end
    inherited btnResizeTimeFrom: TdxBarLargeButton
      Action = actResizeTimeFrom
    end
    inherited btnResizeTimeTo: TdxBarLargeButton
      Action = actResizeTimeTo
    end
    inherited btnWebsite: TdxBarLargeButton
      Action = actPcLabHome
    end
    inherited btnScaleTime: TdxBarLargeButton
      Action = actScaleTime
    end
    inherited chkRapidFilter: TcxBarEditItem
      Properties.OnEditValueChanged = edtFilterChange
    end
    inherited btnFindFirst: TdxBarButton
      Action = actFindFirst
    end
    inherited btnFindNext: TdxBarButton
      Action = actFindNext
    end
    inherited btnFindPrior: TdxBarButton
      Action = actFindPrior
    end
    inherited btnFindLast: TdxBarButton
      Action = actFindLast
    end
    inherited btnLargeFilter: TdxBarLargeButton
      Action = actFilter
    end
    inherited btnLargeFind: TdxBarLargeButton
      Action = actFindNext
    end
    inherited chkFilterCaseSensitive: TcxBarEditItem
      Properties.OnChange = edtFilterChange
    end
    inherited btnMergeBySequence: TdxBarLargeButton
      Action = actMergeBySequence
    end
    inherited btnMergeByNo: TdxBarLargeButton
      Action = actMergeByNo
    end
    inherited btnMergeByTimeFrom: TdxBarLargeButton
      Action = actMergeByTimeFrom
    end
    inherited btnReplace: TdxBarLargeButton
      Action = actReplaceCurrent
    end
    inherited btnReplaceAll: TdxBarLargeButton
      Action = actReplaceAll
    end
    inherited btnExit: TdxBarLargeButton
      Action = actExit
    end
    inherited btnPlayerPlay: TdxBarLargeButton
      Action = actPlayerPlay
    end
    inherited btnPlayerPause: TdxBarLargeButton
      Action = actPlayerPause
    end
    inherited btnPlayerFastBackward: TdxBarLargeButton
      Action = actPlayerFastBackward
    end
    inherited btnPlayerFastForward: TdxBarLargeButton
      Action = actPlayerFastForward
    end
    inherited btnPlayerStop: TdxBarLargeButton
      Action = actPlayerStop
    end
    inherited btnAutoSave: TdxBarLargeButton
      Action = actAutoSave
      ImageIndex = -1
    end
    inherited btnUndo: TdxBarLargeButton
      Action = actUndo
    end
    inherited btnRedo: TdxBarLargeButton
      Action = actRedo
    end
    inherited btnSyncSubToVideoNow: TdxBarButton
      Action = actSyncSubToVideoNow
    end
    inherited btnSyncVideoToSubNow: TdxBarButton
      Action = actSyncVideoToSubNow
    end
    inherited btnTrimWhiteSpace: TdxBarLargeButton
      Action = actTrimWhiteSpace
    end
    inherited btnDeleteEmptySubTitle: TdxBarLargeButton
      Action = actDeleteEmptySubTitle
    end
    inherited btnImportText: TdxBarLargeButton
      Action = actImportText
      ImageIndex = -1
    end
    inherited btnCombineMultiLineSelected: TdxBarLargeButton
      Action = actCombineMultiLineSelected
    end
    inherited btnCombineMultiLineUp: TdxBarLargeButton
      Action = actCombineMultiLineUp
    end
    inherited btnCombineMultiLineDown: TdxBarLargeButton
      Action = actCombineMultiLineDown
    end
    inherited btnCombineSingleLineSelected: TdxBarLargeButton
      Action = actCombineSingleLineSelected
    end
    inherited btnCombineSingleLineUp: TdxBarLargeButton
      Action = actCombineSingleLineUp
    end
    inherited btnCombineSingleLineDown: TdxBarLargeButton
      Action = actCombineSingleLineDown
    end
    inherited btnSplitSub: TdxBarLargeButton
      Action = actSplitSub
    end
    object btnPlayerToTimeToAndNextRec: TdxBarLargeButton [90]
      Action = actPlayerToTimeToAndNextRec
      Category = 0
    end
    inherited btnMoveCurrentToPlayerTime: TdxBarLargeButton
      Action = actMoveCurrentToPlayerTime
    end
    inherited btnMoveFirstToCurrentToPlayerTime: TdxBarLargeButton
      Action = actMoveFirstToCurrentToPlayerTime
    end
    inherited btnMoveCurrentToLastToPlayerTime: TdxBarLargeButton
      Action = actMoveCurrentToLastToPlayerTime
    end
    inherited btnMoveSelectedToPlayerTime: TdxBarLargeButton
      Action = actMoveSelectedToPlayerTime
    end
    inherited btnMoveAllToPlayerTime: TdxBarLargeButton
      Action = actMoveAllToPlayerTime
    end
    inherited btnNewRec2: TdxBarButton
      Action = actNewRec
    end
    inherited btnNewRecBefore: TdxBarButton
      Action = actNewRecBefore
    end
    inherited edtFilter: TdxBarEdit
      OnCurChange = edtFilterChange
      OnKeyDown = edtFilterKeyDown
    end
    inherited drpFilter: TdxBarCombo
      OnChange = edtFilterChange
    end
    inherited btnPrevVideoToSub: TdxBarButton
      Action = actPrevVideoToSub
    end
    inherited btnPrevSubToVideo: TdxBarButton
      Action = actPrevSubToVideo
    end
  end
  inherited dscSubtitle: TDataSource
    OnDataChange = dscSourceDataChange
    Left = 240
  end
  inherited imgLargeIcons: TcxImageList
    FormatVersion = 1
  end
  inherited imgSmallIcons: TcxImageList
    FormatVersion = 1
  end
  inherited syncTimer: TTimer
    OnTimer = syncTimerTimer
  end
  inherited findGallery: TdxRibbonDropDownGallery
    Left = 152
  end
  inherited appMenu: TdxBarApplicationMenu
    ExtraPane.OnItemClick = appMenuExtraPaneItemClick
  end
  inherited displayingSubTimer: TTimer
    OnTimer = displayingSubTimerTimer
  end
  inherited dscHistory: TDataSource
    Left = 552
    Top = 240
  end
  inherited newRecGallery: TdxRibbonDropDownGallery
    Top = 376
  end
  object ActionManager1: TActionManager
    LargeImages = imgLargeIcons
    Images = imgSmallIcons
    Left = 400
    Top = 184
    StyleName = 'Platform Default'
    object actNewFile: TAction
      Category = #24120#35268
      Caption = #26032#24314
      Hint = #26032#24314
      ImageIndex = 0
      OnExecute = actNewFileExecute
    end
    object actOpenFile: TAction
      Category = #24120#35268
      Caption = #25171#24320
      Hint = #25171#24320
      ImageIndex = 1
      ShortCut = 16463
      OnExecute = actOpenFileExecute
    end
    object actSaveFile: TAction
      Category = #24120#35268
      Caption = #20445#23384
      Hint = #20445#23384
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = actSaveFileExecute
    end
    object actSaveFileAs: TAction
      Category = #24120#35268
      Caption = #21478#23384#20026
      Hint = #21478#23384#20026
      ImageIndex = 3
      OnExecute = actSaveFileAsExecute
    end
    object actResetNos: TAction
      Category = #35843#25972
      Caption = #25972#29702#24207#21495
      ImageIndex = 4
      OnExecute = actResetNosExecute
    end
    object actOpenVideoFile: TAction
      Category = #24120#35268
      Caption = #25171#24320#35270#39057
      Hint = #25171#24320#35270#39057
      ImageIndex = 5
      OnExecute = actOpenVideoFileExecute
    end
    object actCloseVideoFile: TAction
      Category = #24120#35268
      Caption = #20851#38381#35270#39057
      ImageIndex = 6
      OnExecute = actCloseVideoFileExecute
    end
    object actSyncVideoToSubAuto: TAction
      Category = #32534#36753
      Caption = #33258#21160#21516#27493#23383#24149#8592#35270#39057
      OnExecute = actSyncVideoToSubAutoExecute
    end
    object actSyncSubToVideoAuto: TAction
      Category = #32534#36753
      Caption = #33258#21160#21516#27493#23383#24149#8594#35270#39057
      OnExecute = actSyncSubToVideoAutoExecute
    end
    object actSyncVideoToSubNow: TAction
      Category = #32534#36753
      Caption = #31435#21363#21516#27493#23383#24149#8592#35270#39057
      Hint = #31435#21363#21516#27493#23383#24149#8592#35270#39057'(Ctrl+'#8592')'
      SecondaryShortCuts.Strings = (
        'Ctrl+Left')
      OnExecute = actSyncVideoToSubNowExecute
    end
    object actSyncSubToVideoNow: TAction
      Category = #32534#36753
      Caption = #31435#21363#21516#27493#23383#24149#8594#35270#39057
      Hint = #31435#21363#21516#27493#23383#24149#8594#35270#39057'(Ctrl+'#8594')'
      SecondaryShortCuts.Strings = (
        'Ctrl+Right')
      OnExecute = actSyncSubToVideoNowExecute
    end
    object actPrevVideoToSub: TAction
      Category = #32534#36753
      Caption = #19978#27425#31435#21363#21516#27493#28857' '
      OnExecute = actPrevVideoToSubExecute
    end
    object actPrevSubToVideo: TAction
      Category = #32534#36753
      Caption = #19978#27425#31435#21363#21516#27493#28857
      OnExecute = actPrevSubToVideoExecute
    end
    object actNewRec: TAction
      Category = #32534#36753
      Caption = #26032#22686#35760#24405
      Hint = #26032#22686#35760#24405
      ImageIndex = 7
      ShortCut = 114
      OnExecute = actNewRecExecute
    end
    object actNewRecBefore: TAction
      Category = #32534#36753
      Caption = #26032#22686#35760#24405'('#21069#32622')'
      ShortCut = 8306
      OnExecute = actNewRecBeforeExecute
    end
    object actNewRecAndPlayerToTimeFrom: TAction
      Category = #32534#36753
      Caption = #26032#22686#20026#36215#22987#26102#38388
      Hint = #26032#22686#35760#24405#65292#24182#23558#25773#25918#22120#26102#38388#20316#20026#36215#22987#26102#38388
      ImageIndex = 8
      ShortCut = 115
      OnExecute = actNewRecAndPlayerToTimeFromExecute
    end
    object actPlayerToTimeFrom: TAction
      Category = #32534#36753
      Caption = #35774#20026#36215#22987#26102#38388
      Hint = #23558#24403#21069#35760#24405#30340#24320#22987#26102#38388#35774#20026#25773#25918#22120#26102#38388
      ImageIndex = 13
      ShortCut = 116
      OnExecute = actPlayerToTimeFromExecute
    end
    object actPlayerToTimeTo: TAction
      Category = #32534#36753
      Caption = #35774#32622#20026#32467#26463#26102#38388
      Hint = #23558#24403#21069#35760#24405#30340#32467#26463#26102#38388#35774#20026#25773#25918#22120#26102#38388
      ImageIndex = 9
      ShortCut = 117
      OnExecute = actPlayerToTimeToExecute
    end
    object actPlayerToTimeToAndNextRec: TAction
      Category = #32534#36753
      Caption = #35774#20026#32467#26463#26102#38388#24182#19979#31227
      Hint = #23558#24403#21069#35760#24405#30340#32467#26463#26102#38388#35774#20026#25773#25918#22120#26102#38388#65292#24182#23558#20809#26631#31227#21160#21040#19979#19968#26465#23383#24149
      ImageIndex = 38
      ShortCut = 118
      OnExecute = actPlayerToTimeToAndNextRecExecute
    end
    object actUpdateRec: TAction
      Category = #32534#36753
      Caption = #25552#20132#26356#25913
      Hint = #30830#35748#24403#21069#35760#24405#30340#20462#25913
      ImageIndex = 10
      ShortCut = 119
      OnExecute = actUpdateRecExecute
    end
    object actCancelUpdateRec: TAction
      Category = #32534#36753
      Caption = #21462#28040#26356#25913
      Hint = #21462#28040#24403#21069#35760#24405#30340#20462#25913
      ImageIndex = 11
      OnExecute = actCancelUpdateRecExecute
    end
    object actDeleteRec: TAction
      Category = #32534#36753
      Caption = #21024#38500#35760#24405
      Hint = #21024#38500#22810#26465#36873#23450#30340#35760#24405
      ImageIndex = 12
      OnExecute = actDeleteRecExecute
    end
    object actMoveTime: TAction
      Category = #35843#25972
      Caption = #24179#31227#26102#38388
      Hint = #24179#31227#26102#38388
      ImageIndex = 16
      ShortCut = 16453
      OnExecute = actMoveTimeExecute
    end
    object actImport: TAction
      Category = #23548#20837#23548#20986
      Caption = #23548#20837
      Hint = #23558#22806#37096#23383#24149#36861#21152#21040#24403#21069#32534#36753#29615#22659#20013
      ImageIndex = 21
      OnExecute = actImportExecute
    end
    object actImportText: TAction
      Category = #23548#20837#23548#20986
      Caption = #23548#20837#25991#26412
      ImageIndex = 41
      OnExecute = actImportTextExecute
    end
    object actExport: TAction
      Category = #23548#20837#23548#20986
      Caption = #23548#20986
      ImageIndex = 22
      OnExecute = actExportExecute
    end
    object actAddAffix: TAction
      Category = #25991#26412#22788#29702
      Caption = #28155#21152#21069#32512#21518#32512
      ImageIndex = 19
      OnExecute = actAddAffixExecute
    end
    object actRemoveAffix: TAction
      Category = #25991#26412#22788#29702
      Caption = #21024#38500#21069#32512#21518#32512
      ImageIndex = 20
      OnExecute = actRemoveAffixExecute
    end
    object actDeleteAffixString: TAction
      Category = #25991#26412#22788#29702
      Caption = #21024#38500#21069#21518'n'#20010#23383#31526
      ImageIndex = 20
      OnExecute = actDeleteAffixStringExecute
    end
    object actResizeTimeFrom: TAction
      Category = #35843#25972
      Caption = #20280#32553#36215#22987#26102#38388
      ImageIndex = 17
      OnExecute = actResizeTimeFromExecute
    end
    object actResizeTimeTo: TAction
      Category = #35843#25972
      Caption = #20280#32553#32456#27490#26102#38388
      ImageIndex = 18
      OnExecute = actResizeTimeToExecute
    end
    object actAbout: TAction
      Category = #24110#21161
      Caption = #20851#20110
      ImageIndex = 26
      OnExecute = actAboutExecute
    end
    object actSubHome: TAction
      Category = #24110#21161
      Caption = #23383#24149#32534#36753#22120#20027#39029
      ImageIndex = 14
      OnExecute = actSubHomeExecute
    end
    object actPcLabHome: TAction
      Category = #24110#21161
      Caption = 'MJ PC Lab'#20027#39029
      ImageIndex = 51
      OnExecute = actPcLabHomeExecute
    end
    object actScaleTime: TAction
      Category = #35843#25972
      Caption = #26102#38388#36724#32553#25918
      ImageIndex = 23
      OnExecute = actScaleTimeExecute
    end
    object actFilter: TAction
      Category = #26597#25214#26367#25442
      Caption = #31579#36873
      ImageIndex = 24
      OnExecute = actFilterExecute
    end
    object actClearFilter: TAction
      Category = #26597#25214#26367#25442
      Caption = #28165#38500#31579#36873
      OnExecute = actClearFilterExecute
    end
    object actFindFirst: TAction
      Category = #26597#25214#26367#25442
      Caption = #26597#25214#31532#19968#20010
      OnExecute = actFindFirstExecute
    end
    object actFindLast: TAction
      Category = #26597#25214#26367#25442
      Caption = #26597#25214#26368#21518#19968#20010
      OnExecute = actFindLastExecute
    end
    object actFindPrior: TAction
      Category = #26597#25214#26367#25442
      Caption = #21521#21069#26597#25214
      Hint = #21521#21069#26597#25214'(Shift+'#8593')'
      SecondaryShortCuts.Strings = (
        'Shift+Up')
      OnExecute = actFindPriorExecute
    end
    object actFindNext: TAction
      Category = #26597#25214#26367#25442
      Caption = #21521#21518#26597#25214
      Hint = #21521#21518#26597#25214'(Shift+'#8595')'
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Shift+Down')
      OnExecute = actFindNextExecute
    end
    object actMergeBySequence: TAction
      Category = #23548#20837#23548#20986
      Caption = #25353#39034#24207#21512#25104
      Hint = #23545#27599#26465#23383#24149#25353#39034#24207#19982#23548#20837#25991#20214#32452#21512
      ImageIndex = 27
      OnExecute = actMergeBySequenceExecute
    end
    object actMergeByNo: TAction
      Category = #23548#20837#23548#20986
      Caption = #25353#23383#24149#24207#21495#21512#25104
      Hint = #23545#27599#26465#23383#24149#25353#24207#21495#19982#23548#20837#25991#20214#32452#21512
      ImageIndex = 27
      OnExecute = actMergeByNoExecute
    end
    object actMergeByTimeFrom: TAction
      Category = #23548#20837#23548#20986
      Caption = #25353#36215#22987#26102#38388#21512#25104
      Hint = #23545#27599#26465#23383#24149#25353#36215#22987#26102#38388#19982#23548#20837#25991#20214#32452#21512
      ImageIndex = 27
      OnExecute = actMergeByTimeFromExecute
    end
    object actReplaceCurrent: TAction
      Category = #26597#25214#26367#25442
      Caption = #26367#25442
      ImageIndex = 28
      OnExecute = actReplaceCurrentExecute
    end
    object actReplaceAll: TAction
      Category = #26597#25214#26367#25442
      Caption = #20840#37096#26367#25442
      ImageIndex = 28
      OnExecute = actReplaceAllExecute
    end
    object actExit: TAction
      Caption = #36864#20986
      ImageIndex = 29
      OnExecute = actExitExecute
    end
    object actPlayerPlay: TAction
      Category = #24120#35268
      Caption = #25773#25918
      Hint = #25773#25918
      ImageIndex = 31
      SecondaryShortCuts.Strings = (
        'Alt+S')
      ShortCut = 8304
      OnExecute = actPlayerPlayExecute
    end
    object actPlayerPause: TAction
      Category = #24120#35268
      Caption = #26242#20572
      Hint = #26242#20572
      ImageIndex = 32
      SecondaryShortCuts.Strings = (
        'Alt+D')
      ShortCut = 8305
      OnExecute = actPlayerPauseExecute
    end
    object actPlayerFastForward: TAction
      Category = #24120#35268
      Caption = #24555#36827
      Hint = #24555#36827
      ImageIndex = 34
      SecondaryShortCuts.Strings = (
        'Alt+F')
      ShortCut = 8306
      OnExecute = actPlayerFastForwardExecute
    end
    object actPlayerFastBackward: TAction
      Category = #24120#35268
      Caption = #24555#36864
      Hint = #24555#36864
      ImageIndex = 33
      SecondaryShortCuts.Strings = (
        'Alt+A')
      ShortCut = 8307
      OnExecute = actPlayerFastBackwardExecute
    end
    object actPlayerStop: TAction
      Category = #24120#35268
      Caption = #20572#27490
      Hint = #20572#27490
      ImageIndex = 35
      ShortCut = 8308
      OnExecute = actPlayerStopExecute
    end
    object actAutoSave: TAction
      Category = #32534#36753
      Caption = #33258#21160#20445#23384
      ImageIndex = 30
      OnExecute = actAutoSaveExecute
    end
    object actUndo: TAction
      Category = #32534#36753
      Caption = #25764#38144
      ImageIndex = 36
      OnExecute = actUndoExecute
    end
    object actRedo: TAction
      Category = #32534#36753
      Caption = #37325#20570
      ImageIndex = 37
      OnExecute = actRedoExecute
    end
    object actTrimWhiteSpace: TAction
      Category = #25991#26412#22788#29702
      Caption = #28165#38500#31354#30333#25991#26412
      Hint = #28165#38500#23383#24149#25991#26412#20004#36793#30340#31354#30333#25110#31354#34892
      ImageIndex = 39
      OnExecute = actTrimWhiteSpaceExecute
    end
    object actDeleteEmptySubTitle: TAction
      Category = #25991#26412#22788#29702
      Caption = #21024#38500#31354#23383#24149
      Hint = #21024#38500#27809#26377#20869#23481#30340#23383#24149
      ImageIndex = 40
      OnExecute = actDeleteEmptySubTitleExecute
    end
    object actCombineMultiLineSelected: TAction
      Category = #32452#21512#25286#20998
      Caption = #32452#21512#36873#20013#35760#24405
      Hint = #32452#21512#36873#20013#35760#24405
      ImageIndex = 42
      ShortCut = 16455
      OnExecute = actCombineMultiLineSelectedExecute
    end
    object actCombineMultiLineUp: TAction
      Category = #32452#21512#25286#20998
      Caption = #21521#19978#32452#21512
      Hint = #21521#19978#32452#21512
      ImageIndex = 43
      OnExecute = actCombineMultiLineUpExecute
    end
    object actCombineMultiLineDown: TAction
      Category = #32452#21512#25286#20998
      Caption = #21521#19979#32452#21512
      Hint = #21521#19979#32452#21512
      ImageIndex = 44
      OnExecute = actCombineMultiLineDownExecute
    end
    object actCombineSingleLineSelected: TAction
      Category = #32452#21512#25286#20998
      Caption = #32452#21512#36873#20013#35760#24405
      Hint = #32452#21512#36873#20013#35760#24405
      ImageIndex = 42
      ShortCut = 49223
      OnExecute = actCombineSingleLineSelectedExecute
    end
    object actCombineSingleLineUp: TAction
      Category = #32452#21512#25286#20998
      Caption = #21521#19978#32452#21512
      Hint = #21521#19978#32452#21512
      ImageIndex = 43
      OnExecute = actCombineSingleLineUpExecute
    end
    object actCombineSingleLineDown: TAction
      Category = #32452#21512#25286#20998
      Caption = #21521#19979#32452#21512
      Hint = #21521#19979#32452#21512
      ImageIndex = 44
      OnExecute = actCombineSingleLineDownExecute
    end
    object actSplitSub: TAction
      Category = #32452#21512#25286#20998
      Caption = #25286#20998#24403#21069#23383#24149
      ImageIndex = 45
      OnExecute = actSplitSubExecute
    end
    object actMoveCurrentToPlayerTime: TAction
      Category = #35843#25972
      Caption = #24179#31227#24403#21069
      ImageIndex = 46
      OnExecute = actMoveCurrentToPlayerTimeExecute
    end
    object actMoveFirstToCurrentToPlayerTime: TAction
      Category = #35843#25972
      Caption = #24179#31227#31532#19968#26465#21040#24403#21069
      ImageIndex = 47
      OnExecute = actMoveFirstToCurrentToPlayerTimeExecute
    end
    object actMoveCurrentToLastToPlayerTime: TAction
      Category = #35843#25972
      Caption = #24179#31227#24403#21069#21040#26368#21518#19968#26465
      ImageIndex = 48
      OnExecute = actMoveCurrentToLastToPlayerTimeExecute
    end
    object actMoveSelectedToPlayerTime: TAction
      Category = #35843#25972
      Caption = #24179#31227#36873#20013#35760#24405
      ImageIndex = 49
      OnExecute = actMoveSelectedToPlayerTimeExecute
    end
    object actMoveAllToPlayerTime: TAction
      Category = #35843#25972
      Caption = #24179#31227#20840#37096
      ImageIndex = 50
      OnExecute = actMoveAllToPlayerTimeExecute
    end
    object actFocusUp: TAction
      Category = #31227#21160#20809#26631
      Caption = #20809#26631#19978#31227
      Hint = #20809#26631#19978#31227'(Ctrl+'#8593')'
      SecondaryShortCuts.Strings = (
        'Ctrl+Up')
      OnExecute = actFocusUpExecute
    end
    object actFocusDown: TAction
      Category = #31227#21160#20809#26631
      Caption = #20809#26631#19979#31227
      Hint = #20809#26631#19979#31227'(Ctrl+'#8595')'
      SecondaryShortCuts.Strings = (
        'Ctrl+Down')
      OnExecute = actFocusDownExecute
    end
  end
end
