unit AddAffixOptions;

interface

uses
  CommonDefs,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,dxRibbonForm, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue,
  dxSkinCaramel, dxSkinOffice2007Black, dxSkinOffice2007Blue, Menus, StdCtrls,
  cxButtons, cxGroupBox, cxControls, cxContainer, cxEdit, cxRadioGroup,
  cxTextEdit, cxGraphics, cxLookAndFeels;

type
  TfrmAddAffixOptions = class(TdxRibbonForm)
    grpScope: TcxRadioGroup;
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    edtPrefix: TcxTextEdit;
    cxGroupBox3: TcxGroupBox;
    edtPostfix: TcxTextEdit;
  private
    function getPostfix: string;
    function getPrefix: string;
    function getScope: ActionScope;
    { Private declarations }
  public
    property Scope:ActionScope read getScope;
    property Prefix:string read getPrefix;
    property Postfix:string read getPostfix;

  end;

implementation

{$R *.dfm}

{ TfrmAddAffixOptions }

function TfrmAddAffixOptions.getScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmAddAffixOptions.getPrefix: string;
begin
    exit(edtPrefix.Text);
end;

function TfrmAddAffixOptions.getPostfix: string;
begin
    exit(edtPostfix.Text);
end;

end.
