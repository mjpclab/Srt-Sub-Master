unit DeleteEmptyOptions;

interface

uses
    CommonDefs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2007Black, Menus, StdCtrls,
  cxButtons, cxGroupBox, cxRadioGroup, cxCheckBox;

type
  TfrmDeleteEmptyOptions = class(TForm)
    grpScope: TcxRadioGroup;
    grpOptions: TcxGroupBox;
    grpOperation: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    chkIncludeWhiteSpaceLine: TcxCheckBox;
  private
    function getIncludeWhiteSpaceLine: boolean;
    function getScope: ActionScope;
    { Private declarations }
  public
    { Public declarations }
    property Scope:ActionScope read getScope;
    property IncludeWhiteSpaceLine:boolean read getIncludeWhiteSpaceLine;
  end;


implementation

{$R *.dfm}

{ TfrmDeleteEmptyOptions }

function TfrmDeleteEmptyOptions.getScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmDeleteEmptyOptions.getIncludeWhiteSpaceLine: boolean;
begin
    Exit(chkIncludeWhiteSpaceLine.Checked);
end;

end.
