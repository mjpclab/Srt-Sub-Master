unit TimeScaleOptions;

interface

uses
  CommonDefs,SrtTimeManager,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue, dxSkinCaramel,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, Menus, cxTextEdit, cxMaskEdit,
  StdCtrls, cxButtons, cxGroupBox, cxControls, cxContainer, cxEdit,
  cxRadioGroup, ExtCtrls, cxGraphics, cxLookAndFeels;

type
  TfrmTimeScaleOptions = class(TForm)
    Image1: TImage;
    grpScope: TcxRadioGroup;
    cxGroupBox1: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    grpOffsetTime: TcxGroupBox;
    edtNewTimeMin: TcxMaskEdit;
    cxGroupBox2: TcxGroupBox;
    edtNewTimeMax: TcxMaskEdit;
  private
    FNewTimeMin,FNewTimeMax:TSrtTimeManager;

    function getScope: ActionScope;
    function getNewTimeMax: TSrtTimeManager;
    function getNewTimeMin: TSrtTimeManager;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;

    property NewTimeMin:TSrtTimeManager read getNewTimeMin write FNewTimeMin;
    property NewTimeMax:TSrtTimeManager read getNewTimeMax write FNewTimeMax;
    property Scope:ActionScope read getScope;
  end;


implementation

{$R *.dfm}

{ TfrmTimeScaleOptions }

constructor TfrmTimeScaleOptions.Create(AOwner: TComponent);
begin
    inherited;
    FNewTimeMin:=TSrtTimeManager.Create;
    FNewTimeMax:=TSrtTimeManager.Create;
end;

destructor TfrmTimeScaleOptions.Destroy;
begin
    FNewTimeMin.Free;
    FNewTimeMax.Free;
    inherited;
end;

function TfrmTimeScaleOptions.getScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmTimeScaleOptions.getNewTimeMin: TSrtTimeManager;
begin
    FNewTimeMin.Parse(edtNewTimeMin.Text);
    exit(FNewTimeMin);
end;

function TfrmTimeScaleOptions.getNewTimeMax: TSrtTimeManager;
begin
    FNewTimeMax.Parse(edtNewTimeMax.Text);
    exit(FNewTimeMax);
end;

end.
