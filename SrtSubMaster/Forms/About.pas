unit About;

interface

uses
  ShellAPI,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs ,dxRibbonform, dxSkinsCore, dxSkinBlue, dxSkinCaramel,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, cxControls, cxContainer, cxEdit,
  cxLabel, ExtCtrls, StdCtrls, Menus, cxLookAndFeelPainters, cxButtons,
  cxTextEdit, GIFImg, cxGraphics, cxLookAndFeels;

type
  TfrmAbout = class(TdxRibbonForm)
    Image1: TImage;
    cxLabel1: TcxLabel;
    lblVersion: TLabel;
    btnClose: TcxButton;
    lblHomePage: TLabel;
    Image2: TImage;
    Label1: TLabel;
    procedure lblHomePageClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TfrmAbout.lblHomePageClick(Sender: TObject);
begin
    ShellExecute(self.Handle,'open','http://mjpclab.net/',nil,nil,sw_shownormal);
end;

end.
