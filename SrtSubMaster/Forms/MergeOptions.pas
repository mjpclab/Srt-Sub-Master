unit MergeOptions;

interface

uses
  SrtFileMergeExecuterBase,CommonDefs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  dxRibbonForm, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue, dxSkinCaramel,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, Menus, cxGraphics, cxMaskEdit,
  cxDropDownEdit, StdCtrls, cxButtons, cxTextEdit, cxControls, cxContainer,
  cxEdit, cxGroupBox, cxMemo, cxCheckBox, cxLabel, cxRadioGroup, cxLookAndFeels;

type
  TfrmMergeOptions = class(TdxRibbonForm)
    cxGroupBox1: TcxGroupBox;
    edtFileName: TcxTextEdit;
    btnBrowse: TcxButton;
    cxGroupBox2: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    cxGroupBox3: TcxGroupBox;
    drpSeparator: TcxComboBox;
    mmoSeparator: TcxMemo;
    cxGroupBox4: TcxGroupBox;
    chkImportUnmatched: TcxCheckBox;
    cxLabel1: TcxLabel;
    grpTimeAdjustMode: TcxRadioGroup;
    grpMergePosition: TcxRadioGroup;
    procedure btnBrowseClick(Sender: TObject);
    procedure drpSeparatorPropertiesChange(Sender: TObject);
  private
    FImportDialog:TOpenDialog;
    FSubCaption:string;

    function getImportFileName: string;
    function getSeparator: string;
    function getImportUnmatched: boolean;
    procedure setSubCaption(const Value: string);
    function getTimeMergeMode: TSrtTimeMergeMode;
    function getMergePosition: MergePosition;
    { Private declarations }
  public
    { Public declarations }
    property ImportDialog:TOpenDialog read FImportDialog write FImportDialog;
    property SubCaption:string read FSubCaption write setSubCaption;

    property TimeMergeMode:TSrtTimeMergeMode read getTimeMergeMode;
    property Separator:string read getSeparator;
    property MergePosition:MergePosition read getMergePosition;
    property ImportUnmatched:boolean read getImportUnmatched;
    property ImportFileName:string read getImportFileName;
  end;


implementation

{$R *.dfm}

procedure TfrmMergeOptions.setSubCaption(const Value: string);
const
    BaseCaption:string='�ϲ����� - ';
begin
    FSubCaption := Value;
    self.Caption:=BaseCaption + Value;
end;

function TfrmMergeOptions.getTimeMergeMode: TSrtTimeMergeMode;
begin
    case grpTimeAdjustMode.ItemIndex of
        0: exit(tgmEditingTime);
        1: exit(tgmImportingTime);
        2: exit(tgmMidTime);
        else exit(tgmEditingTime);
    end;
end;

procedure TfrmMergeOptions.btnBrowseClick(Sender: TObject);
begin
    if ImportDialog.Execute then edtFileName.Text:=ImportDialog.FileName;
end;

function TfrmMergeOptions.getSeparator: string;
begin
    case drpSeparator.ItemIndex of
        0: exit('');
        1: exit(#13#10);
        2: exit(mmoSeparator.Text);
        else exit('');
    end;
end;

function TfrmMergeOptions.getMergePosition: MergePosition;
begin
    case grpMergePosition.ItemIndex of
        0: Exit(mpInsertBefore);
        1: Exit(mpAppendAfter);
        else raise Exception.Create('Unexpected Option');
    end;
end;

function TfrmMergeOptions.getImportUnmatched: boolean;
begin
    exit(chkImportUnmatched.Checked);
end;

function TfrmMergeOptions.getImportFileName: string;
begin
    exit(edtFileName.Text);
end;

procedure TfrmMergeOptions.drpSeparatorPropertiesChange(Sender: TObject);
begin
    mmoSeparator.Visible:=(drpSeparator.ItemIndex=drpSeparator.Properties.Items.Count-1);
end;

end.
