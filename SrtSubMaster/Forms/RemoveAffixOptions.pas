unit RemoveAffixOptions;

interface

uses
  CommonDefs,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,dxRibbonForm, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue,
  dxSkinCaramel, dxSkinOffice2007Black, dxSkinOffice2007Blue, Menus, StdCtrls,
  cxButtons, cxGroupBox, cxControls, cxContainer, cxEdit, cxRadioGroup,
  cxTextEdit, cxCheckBox, cxGraphics, cxLookAndFeels;

type
  TfrmRemoveAffixOptions = class(TdxRibbonForm)
    grpScope: TcxRadioGroup;
    cxGroupBox1: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    cxGroupBox2: TcxGroupBox;
    edtPrefix: TcxTextEdit;
    cxGroupBox3: TcxGroupBox;
    edtPostfix: TcxTextEdit;
    cxGroupBox4: TcxGroupBox;
    chkIgnoreCase: TcxCheckBox;
  private
    function getIgnoreCase: boolean;
    function getPostfix: string;
    function getPrefix: string;
    function getScope: ActionScope;
    { Private declarations }
  public
    property Scope:ActionScope read getScope;
    property Prefix:string read getPrefix;
    property Postfix:string read getPostfix;
    property IgnoreCase:boolean read getIgnoreCase;
  end;

implementation

{$R *.dfm}

{ TfrmRemoveAffixOptions }

function TfrmRemoveAffixOptions.getScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmRemoveAffixOptions.getPostfix: string;
begin
    exit(edtPostfix.Text);
end;

function TfrmRemoveAffixOptions.getPrefix: string;
begin
    exit(edtPrefix.Text);
end;

function TfrmRemoveAffixOptions.getIgnoreCase: boolean;
begin
    exit(chkIgnoreCase.Checked);
end;

end.
