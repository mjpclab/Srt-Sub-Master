object frmSplitOptions: TfrmSplitOptions
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #25286#20998#23383#24149
  ClientHeight = 468
  ClientWidth = 774
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object gridSplitSubs: TcxGrid
    Left = 0
    Top = 0
    Width = 289
    Height = 468
    Align = alLeft
    TabOrder = 0
    object grdSplitSubs: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dscSplit
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = #24050#25286#20998' 0 '#27573
          Kind = skCount
          FieldName = 'content'
          Column = colSplitSubsContent
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHidingOnGrouping = False
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.NoDataToDisplayInfoText = '('#35831#28155#21152#25286#20998#23383#24149')'
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      object colSplitSubsRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object colSplitSubsContent: TcxGridDBColumn
        Caption = #25286#20998#30340#23383#24149
        DataBinding.FieldName = 'content'
        HeaderAlignmentHorz = taCenter
        HeaderAlignmentVert = vaCenter
      end
    end
    object cxgrdlvlGrid1Level1: TcxGridLevel
      GridView = grdSplitSubs
    end
  end
  object split1: TcxSplitter
    Left = 289
    Top = 0
    Width = 4
    Height = 468
    HotZoneClassName = 'TcxSimpleStyle'
    ResizeUpdate = True
    Control = gridSplitSubs
  end
  object pnl1: TPanel
    Left = 293
    Top = 0
    Width = 481
    Height = 468
    Align = alClient
    TabOrder = 2
    object edtSourceSub: TcxMemo
      Left = 1
      Top = 85
      Align = alClient
      Properties.ReadOnly = True
      TabOrder = 0
      Height = 329
      Width = 479
    end
    object dock1: TdxBarDockControl
      Left = 1
      Top = 1
      Width = 479
      Height = 28
      Align = dalTop
      BarManager = barManager1
    end
    object status1: TdxStatusBar
      Left = 1
      Top = 442
      Width = 479
      Height = 25
      Panels = <
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          Bevel = dxpbNone
          Fixed = False
          Text = #35831#36880#19968#23558#20809#26631#31227#33267#35201#25286#20998#30340#39318#27573#23383#24149#26411#23614#65292#28982#21518#21333#20987#8220#28155#21152#25286#20998#8221#12290
        end>
      PaintStyle = stpsUseLookAndFeel
      LookAndFeel.NativeStyle = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object dock2: TdxBarDockControl
      Left = 1
      Top = 414
      Width = 479
      Height = 28
      Align = dalBottom
      BarManager = barManager1
    end
    object dock3: TdxBarDockControl
      Left = 1
      Top = 29
      Width = 479
      Height = 28
      Align = dalTop
      BarManager = barManager1
    end
    object dock4: TdxBarDockControl
      Left = 1
      Top = 57
      Width = 479
      Height = 28
      Align = dalTop
      BarManager = barManager1
    end
  end
  object barManager1: TdxBarManager
    AllowReset = False
    AlwaysSaveText = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = cxImageList1
    LookAndFeel.NativeStyle = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 360
    Top = 296
    DockControlHeights = (
      0
      0
      0
      0)
    object barManager1Bar1: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockControl = dock1
      DockedDockControl = dock1
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 704
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnAddSplitByCursor'
        end
        item
          BeginGroup = True
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnSplitByLine'
        end
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnSplitAllByLine'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object barManager1Bar2: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockControl = dock2
      DockedDockControl = dock2
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 749
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnOK'
        end
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnCancel'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object barManager1Bar3: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = '1'
      CaptionButtons = <>
      DockControl = dock3
      DockedDockControl = dock3
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 772
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 76
          Visible = True
          ItemName = 'edtCustomSeparator'
        end
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnSplitByCustom'
        end
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnSplitAllByCustom'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object barManager1Bar4: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = 'Custom 3'
      CaptionButtons = <>
      DockControl = dock4
      DockedDockControl = dock4
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnRemoveSplit'
        end
        item
          UserDefine = [udPaintStyle]
          UserPaintStyle = psCaptionGlyph
          Visible = True
          ItemName = 'btnRemoveAllSplit'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object btnAddSplitByCursor: TdxBarButton
      Caption = #25353#20809#26631#25286#20998
      Category = 0
      Hint = #25353#20809#26631#25286#20998
      Visible = ivAlways
      ImageIndex = 0
      OnClick = btnAddSplitByCursorClick
    end
    object btnRemoveSplit: TdxBarButton
      Caption = #31227#38500#25286#20998
      Category = 0
      Hint = #31227#38500#25286#20998
      Visible = ivAlways
      ImageIndex = 1
      OnClick = btnRemoveSplitClick
    end
    object btnOK: TdxBarButton
      Caption = #30830#23450
      Category = 0
      Hint = #30830#23450
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000040B0510205028803860408000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000236C2FC0208830FF4AA559F01C302040000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000061308202C873BF040C860FF40C860FF30A850FF408F5BD0030A05100000
        0000000000000000000000000000000000000000000000000000000000000613
        0820309840FF40C860FF60E880FF59D278F060D070FF299345E0153F1C700000
        0000000000000000000000000000000000000000000000000000000000002C96
        3BF050D060FF60E880FF4D9A58B00A1A0C2054A860C060C870FF2C9D3BF00411
        0620000000000000000000000000000000000000000000000000000000007ECB
        8CE080E890FF5BB668D00000000000000000050D061054A860C060C870FF2985
        37E06E9A79B00000000000000000000000000000000000000000000000001B2D
        21303865467000000000000000000000000000000000050D061068D278F050C0
        60FF4A9D59F05070588000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000050D061068D2
        78F040B860FF68AC78F050705880000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000000000050D
        071062C470E040B860FF78B487F0324337500000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000050D0710589A63B040B860FF304B36600000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000121D142051825A9028733CA00000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      OnClick = btnOKClick
    end
    object btnCancel: TdxBarButton
      Caption = #21462#28040
      Category = 0
      Hint = #21462#28040
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000003182000010C100000000000000000000000000000
        00000000000000021010001BD2E0000210100000000000000000000000000000
        00000000000000042430000D9AE0000324300000000000000000000000000000
        000001031010172FC0C01030FFFF0E25F0F00002101000000000000000000000
        000000031820001190C00018C0FF000EA5F00003182000000000000000000000
        000000000000020310101D43F0F01030FFFF0028FFFF00063030000000000004
        24300014A8E00018D0FF0016B4F0000630400000000000000000000000000000
        00000000000000000000020310101D43F0F01030FFFF001DF0F0000427300014
        B6E00020E0FF0016C3F000031820000000000000000000000000000000000000
        000000000000000000000000000004072020263AD0D01038FFFF0020F0FF001D
        E1F00013A9D00003182000000000000000000000000000000000000000000000
        000000000000000000000000000000000000141B50502048FFFF1030FFFF001D
        E1F0000427300000000000000000000000000000000000000000000000000000
        0000000000000000000000000000101840404060FFFF3050FFFF1D3BF0F01038
        FFFF0020F0FF00041E2000000000000000000000000000000000000000000000
        000000000000000000000C1230304060FFFF4058FFFF3B61F0F0101640403B59
        F0F02040FFFF0020F0FF00062D30000000000000000000000000000000000000
        000000000000080C20205070FFFF5078FFFF405BD0D0080C2020000000000C10
        30303B59F0F03048FFFF0020F0FF00062D300000000000000000000000000000
        0000040610106078FFFF6078FFFF5970F0F00406101000000000000000000000
        0000080B20203B59F0F03050FFFF0028FFFF00041E2000000000000000000000
        000000000000080C20205978F0F0040610100000000000000000000000000000
        000000000000080B20203753E0E0020620200000000000000000000000000000
        0000000000000000000004061010000000000000000000000000000000000000
        00000000000000000000080B2020000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      OnClick = btnCancelClick
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object btnSplitByLine: TdxBarButton
      Caption = #25286#20998#19968#34892
      Category = 0
      Hint = #25286#20998#19968#34892
      Visible = ivAlways
      ImageIndex = 0
      OnClick = btnSplitByLineClick
    end
    object edtCustomSeparator: TdxBarEdit
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
    end
    object btnSplitByCustom: TdxBarButton
      Caption = #25353#31526#21495#25286#20998#19968#27425
      Category = 0
      Hint = #25353#31526#21495#25286#20998#19968#27425
      Visible = ivAlways
      ImageIndex = 0
      OnClick = btnSplitByCustomClick
    end
    object btnSplitAllByLine: TdxBarButton
      Caption = #25286#20998#20840#37096#34892
      Category = 0
      Hint = #25286#20998#20840#37096#34892
      Visible = ivAlways
      ImageIndex = 0
      OnClick = btnSplitAllByLineClick
    end
    object btnSplitAllByCustom: TdxBarButton
      Caption = #25353#31526#21495#25286#20998#20840#37096
      Category = 0
      Hint = #25353#31526#21495#25286#20998#20840#37096
      Visible = ivAlways
      ImageIndex = 0
      OnClick = btnSplitAllByCustomClick
    end
    object btnRemoveAllSplit: TdxBarButton
      Caption = #31227#38500#20840#37096#25286#20998
      Category = 0
      Hint = #31227#38500#20840#37096#25286#20998
      Visible = ivAlways
      ImageIndex = 1
      OnClick = btnRemoveAllSplitClick
    end
  end
  object mem1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 24
    Top = 296
    object fieldContent: TMemoField
      FieldName = 'content'
      BlobType = ftMemo
    end
  end
  object dscSplit: TDataSource
    DataSet = mem1
    Left = 88
    Top = 296
  end
  object TdxBarSeparator
    Category = -1
    Visible = ivAlways
  end
  object TdxBarSeparator
    Category = -1
    Visible = ivAlways
  end
  object cxImageList1: TcxImageList
    FormatVersion = 1
    DesignInfo = 19398832
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000B09880FF604830FF604830FFD07850FFB05830FFB05830FFB058
          30FF604830FF604830FF604830FF604830FF604830FF604830FF000000000000
          000000000000B09880FFFFF8FFFFF0E0E0FFE08860FFF0A070FFF09060FFB058
          30FFFFF8FFFFF0E0E0FFC0A080FFFFF8FFFFF0E0E0FF604830FF000000000000
          000000000000B09890FFFFF0F0FFFFF0F0FFE08860FFF0A070FFF09060FFB058
          30FFFFF0F0FFFFF0F0FFC0A090FFFFF0F0FFF0E0E0FF604830FF000000000000
          000000000000B09890FFC0A8A0FFC0A890FFE08860FFF0A080FFF09070FFB058
          30FFC0A8A0FFC0A890FFC0A890FFC0A090FFC0A090FF604830FF000000000000
          000000000000B0A090FFFFF8F0FFFFF0E0FFE08860FFF0A880FFF09870FFB058
          30FFFFF8F0FFFFF8F0FFC0A890FFFFF8F0FFF0E0E0FF604830FF18120C406048
          30FF00000000B0A090FFFFF0F0FFFFF0F0FFE08860FFF0A880FFF09870FFB060
          30FFFFF8F0FFFFF8F0FFC0A890FFFFF8F0FFF0E0E0FF604830FF806860FF6048
          30FF604830FF604830FF604830FFC0A890FFE08860FFF0A880FFF0A070FFB060
          30FFC0A8A0FFC0A890FFC0A890FFC0A090FFC0A090FF604830FF120D09308070
          60FF00000000C0A890FFFFF8F0FFFFF0E0FFE08860FFF0A880FFF0A070FFB060
          30FFFFF8F0FFFFF8F0FFC0A890FFFFF8F0FFF0E0E0FF604830FF000000000000
          000000000000C0A890FFFFF8F0FFFFF0F0FFE08860FFF0A890FFF0A080FFB060
          40FFFFF8F0FFFFF8F0FFC0A890FFFFF8F0FFF0E0E0FF604830FF000000000000
          000000000000C0A8A0FFC0A8A0FFC0A890FFE08860FFF0B090FFF0A080FFB060
          40FFC0A8A0FFC0A890FFC0A890FFC0A090FFC0A090FF604830FF000000000000
          000000000000C0A8A0FFFFFFFFFFFFFFFFFFE08860FFF0B090FFF0A080FFB060
          40FFFFFFFFFFFFFFFFFFD0C0B0FFFFF8F0FFF0E0E0FF604830FF000000000000
          000000000000C0B0A0FFFFFFFFFFFFFFFFFFE08860FFF0B090FFF0B090FFB060
          40FFFFFFFFFFFFFFFFFFE0D0C0FFFFF8F0FFFFF0F0FF604830FF000000000000
          000000000000C0B0A0FFC0B0A0FFC0A8A0FFE08860FFE08860FFE08860FFE088
          60FFC0A890FFC0A090FFB09890FFB09880FFB09880FFB09880FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000080B097FF3060
          47FF306047FF306047FF306047FF306047FF50D076FF30B056FF30B056FF30B0
          56FF306047FF306047FF306047FF00000000000000000000000080B097FFFFFF
          F8FFE0F0E0FF80C09FFFFFFFF8FFE0F0E0FF60E086FF70F09EFF60F08EFF30B0
          56FFFFFFF8FFE0F0E0FF306047FF00000000000000000000000090B097FFFFFF
          FFFFF0FFF0FF90C09FFFF0FFF0FFF0FFF0FF60E086FF70F09EFF60F08EFF30B0
          56FFF0FFF0FFE0F0E0FF306047FF00000000000000000000000090B09FFFA0C0
          A7FF90C0A7FF90C0A7FFA0C0A7FF90C0A7FF60E086FF80F09EFF70F08EFF30B0
          56FF90C09FFF90C09FFF306047FF00000000000000000000000090B09FFFFFFF
          FFFFE0FFEFFF90C0A7FFF0FFF8FFF0FFF8FF60E086FF80F0A6FF70F096FF30B0
          56FFF0FFF8FFE0F0E0FF306047FF00000000000000000000000090B09FFFFFFF
          FFFFF0FFF0FF90C0A7FFF0FFF8FFF0FFF8FF60E086FF80F0A6FF70F096FF30B0
          5EFFF0FFF8FFE0F0E0FF306047FF0000000040704FFF789083C090C0A7FFA0C0
          A7FF90C0A7FF90C0A7FFA0C0A7FF90C0A7FF60E086FF80F0A6FF70F09EFF30B0
          5EFF90C09FFF306047FF306047FF306047FF40704FFF40704FFF90C0A7FFFFFF
          FFFFE0FFEFFF90C0A7FFF0FFF8FFF0FFF8FF60E086FF80F0A6FF70F09EFF30B0
          5EFFF0FFF8FFE0F0E0FF306047FF0000000060906FFF50605780A0C0A7FFFFFF
          FFFFF0FFF0FF90C0A7FFF0FFF8FFF0FFF8FF60E086FF90F0A6FF80F09EFF40B0
          5EFFF0FFF8FFE0F0E0FF306047FF000000000000000000000000A0C0AFFFA0C0
          A7FF90C0A7FF90C0A7FFA0C0A7FF90C0A7FF60E086FF90F0AEFF80F09EFF40B0
          5EFF90C09FFF90C09FFF306047FF000000000000000000000000A0C0AFFFFFFF
          FFFFFFFFFFFFB0D0BFFFFFFFFFFFFFFFFFFF60E086FF90F0AEFF80F09EFF40B0
          5EFFF0FFF8FFE0F0E0FF306047FF000000000000000000000000A0C0AFFFFFFF
          FFFFFFFFFFFFC0E0CFFFFFFFFFFFFFFFFFFF60E086FF90F0AEFF90F0AEFF40B0
          5EFFF0FFF8FFF0FFF0FF306047FF000000000000000000000000A0C0AFFFA0C0
          AFFFA0C0AFFFA0C0A7FF90C0A7FF90C0A7FF60E086FF60E086FF60E086FF60E0
          86FF90B097FF80B097FF80B097FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
end
