unit DeleteAffixStringOptions;

interface

uses
  CommonDefs,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,dxRibbonForm, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue,
  dxSkinCaramel, dxSkinOffice2007Black, dxSkinOffice2007Blue, Menus, StdCtrls,
  cxButtons, cxGroupBox, cxControls, cxContainer, cxEdit, cxRadioGroup,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxGraphics, cxLookAndFeels;

type
  TfrmDeleteAffixStringOptions = class(TdxRibbonForm)
    grpScope: TcxRadioGroup;
    cxGroupBox1: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    cxGroupBox2: TcxGroupBox;
    cxGroupBox3: TcxGroupBox;
    edtPrefix: TcxSpinEdit;
    edtPostfix: TcxSpinEdit;
  private
    function getDeletePostfixlength: integer;
    function getDeletePrefixLength: integer;
    function getScope: ActionScope;
    { Private declarations }
  public
    property Scope:ActionScope read getScope;
    property DeletePrefixLength:integer read getDeletePrefixLength;
    property DeletePostfixLength:integer read getDeletePostfixLength;
  end;

implementation

{$R *.dfm}

{ TfrmDeleteAffixStringOptions }

function TfrmDeleteAffixStringOptions.getScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmDeleteAffixStringOptions.getDeletePrefixLength: integer;
begin
    exit(edtPrefix.Value);
end;

function TfrmDeleteAffixStringOptions.getDeletePostfixLength: integer;
begin
    exit(edtPostfix.Value);
end;

end.
