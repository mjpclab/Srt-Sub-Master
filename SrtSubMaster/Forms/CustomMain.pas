unit CustomMain;

interface

uses
  dxRibbonForm , Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms,Generics.Collections,    Dialogs,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridDBTableView, cxGridTableView, cxGrid,
  dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxSkinsCore,
  dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, cxLookAndFeels, dxSkinsForm,
  cxBlobEdit,
  dxBarExtItems, ImgList, dxBarExtDBItems,
  ActnList, PlatformDefaultStyleActnCtrls, ActnMan,
  cxSplitter, ExtCtrls, OleCtrls,
  WMPLib_TLB, StdCtrls, cxEditRepositoryItems, cxMaskEdit, cxContainer, cxLabel,
  cxTextEdit, cxDBEdit, cxMemo, cxLookAndFeelPainters, cxGroupBox,
  cxBarEditItem, cxCheckBox, Menus, cxButtons, cxDropDownEdit, dxRibbonGallery,
  cxPC, dxSkinOffice2007Black, dxRibbonSkins, dxRibbonCustomizationForm,
  cxNavigator, dxBarBuiltInMenu, dxBarApplicationMenu;


type
  TfrmCustomMain = class(TdxRibbonForm)
    tabGeneral: TdxRibbonTab;
    dxRibbon: TdxRibbon;
    statusBar: TdxRibbonStatusBar;
    grdSub: TcxGridDBTableView;
    gridSubLevel1: TcxGridLevel;
    gridSub: TcxGrid;
    barManager: TdxBarManager;
    dxSkinController1: TdxSkinController;
    dscSubtitle: TDataSource;
    cxLookAndFeelController1: TcxLookAndFeelController;
    grdColNo: TcxGridDBColumn;
    grdColTimeFrom: TcxGridDBColumn;
    grdColTimeTo: TcxGridDBColumn;
    grdColContent: TcxGridDBColumn;
    imgLargeIcons: TcxImageList;
    btnOpen: TdxBarLargeButton;
    btnSave: TdxBarLargeButton;
    btnSaveAs: TdxBarLargeButton;
    dlgOpen: TOpenDialog;
    dlgSave: TSaveDialog;
    barFrequentlyUsed: TdxBar;
    imgSmallIcons: TcxImageList;
    btnNew: TdxBarLargeButton;
    wmplayer: TWindowsMediaPlayer;
    cxSplitter1: TcxSplitter;
    grpEdit: TcxGroupBox;
    mmoContent: TcxDBMemo;
    lblContent: TLabel;
    edtTimeTo: TcxDBMaskEdit;
    lblTimeTo: TLabel;
    lblTimeFrom: TLabel;
    edtTimeFrom: TcxDBMaskEdit;
    tabEdit: TdxRibbonTab;
    barEdit: TdxBar;
    barVideoFile: TdxBar;
    btnOpenVideoFile: TdxBarLargeButton;
    btnCloseVideoFile: TdxBarLargeButton;
    dlgOpenVideoFile: TOpenDialog;
    syncTimer: TTimer;
    btnSyncSubToVideo: TdxBarButton;
    btnSyncVideoToSub: TdxBarButton;
    btnNewRec: TdxBarLargeButton;
    btnNewRecAndPlayerToTimeFrom: TdxBarLargeButton;
    btnPlayerToTimeTo: TdxBarLargeButton;
    btnUpdateRec: TdxBarLargeButton;
    tabHelp: TdxRibbonTab;
    barHelp: TdxBar;
    btnAbout: TdxBarLargeButton;
    btnHomePage: TdxBarLargeButton;
    btnCancelUpdateRec: TdxBarLargeButton;
    btnDeleteRec: TdxBarLargeButton;
    btnTimeFromInc1000: TcxButton;
    btnTimeFromDec1000: TcxButton;
    btnTimeFromInc100: TcxButton;
    btnTimeFromDec100: TcxButton;
    btnTimeToInc1000: TcxButton;
    btnTimeToDec1000: TcxButton;
    btnTimeToInc100: TcxButton;
    btnTimeToDec100: TcxButton;
    tabAdjust: TdxRibbonTab;
    barAdjustTime: TdxBar;
    btnMoveTime: TdxBarLargeButton;
    dlgImport: TOpenDialog;
    tabImport: TdxRibbonTab;
    barImportExport: TdxBar;
    btnImport: TdxBarLargeButton;
    barFile: TdxBar;
    btnNewFile: TdxBarLargeButton;
    btnOpenFile: TdxBarLargeButton;
    btnSaveFile: TdxBarLargeButton;
    btnSaveFileAs: TdxBarLargeButton;
    btnResetNos: TdxBarLargeButton;
    btnExport: TdxBarLargeButton;
    tabFindReplace: TdxRibbonTab;
    barReplace: TdxBar;
    barFilter: TdxBar;
    tabTextProcess: TdxRibbonTab;
    barAffix: TdxBar;
    btnFilter: TdxBarButton;
    btnClearFilter: TdxBarButton;
    btnAddAffix: TdxBarLargeButton;
    btnRemoveAffix: TdxBarLargeButton;
    btnDeleteAffixString: TdxBarLargeButton;
    btnPlayerToTimeFrom: TdxBarLargeButton;
    cxSplitter2: TcxSplitter;
    btnResizeTimeFrom: TdxBarLargeButton;
    btnResizeTimeTo: TdxBarLargeButton;
    btnWebsite: TdxBarLargeButton;
    btnScaleTime: TdxBarLargeButton;
    barMerge: TdxBar;
    chkRapidFilter: TcxBarEditItem;
    findGallery: TdxRibbonDropDownGallery;
    btnFindFirst: TdxBarButton;
    btnFindNext: TdxBarButton;
    btnFindPrior: TdxBarButton;
    btnFindLast: TdxBarButton;
    filterGallery: TdxRibbonDropDownGallery;
    btnLargeFilter: TdxBarLargeButton;
    btnLargeFind: TdxBarLargeButton;
    dxBarSeparator1: TdxBarSeparator;
    dxBarSeparator2: TdxBarSeparator;
    chkFilterCaseSensitive: TcxBarEditItem;
    btnMergeBySequence: TdxBarLargeButton;
    btnMergeByNo: TdxBarLargeButton;
    btnMergeByTimeFrom: TdxBarLargeButton;
    btnReplace: TdxBarLargeButton;
    btnReplaceAll: TdxBarLargeButton;
    appMenu: TdxBarApplicationMenu;
    barMergeMenu: TdxBarSubItem;
    barExit: TdxBar;
    btnExit: TdxBarLargeButton;
    btnPlayerPlay: TdxBarLargeButton;
    btnPlayerPause: TdxBarLargeButton;
    btnPlayerFastBackward: TdxBarLargeButton;
    btnPlayerFastForward: TdxBarLargeButton;
    btnPlayerStop: TdxBarLargeButton;
    dlgExport: TSaveDialog;
    pnlMain: TPanel;
    btnAutoSave: TdxBarLargeButton;
    cxPageControl1: TcxPageControl;
    tabPreview: TcxTabSheet;
    tabHistory: TcxTabSheet;
    cxSplitter3: TcxSplitter;
    mnoDisplayingSubtitle: TcxMemo;
    displayingSubTimer: TTimer;
    gridHistory: TcxGrid;
    grdHistory: TcxGridDBTableView;
    gridHistoryLevel1: TcxGridLevel;
    dscHistory: TDataSource;
    colNo: TcxGridDBColumn;
    colDescription: TcxGridDBColumn;
    btnUndo: TdxBarLargeButton;
    btnRedo: TdxBarLargeButton;
    btnSyncSubToVideoNow: TdxBarButton;
    btnSyncVideoToSubNow: TdxBarButton;
    barEditUtility: TdxBar;
    barSynchronize: TdxBar;
    btnTrimWhiteSpace: TdxBarLargeButton;
    barTextOther: TdxBar;
    btnDeleteEmptySubTitle: TdxBarLargeButton;
    btnImportText: TdxBarLargeButton;
    tabCombine: TdxRibbonTab;
    barCombineMultiLine: TdxBar;
    barCombineSingleLine: TdxBar;
    btnCombineMultiLineSelected: TdxBarLargeButton;
    btnCombineMultiLineUp: TdxBarLargeButton;
    btnCombineMultiLineDown: TdxBarLargeButton;
    btnCombineSingleLineSelected: TdxBarLargeButton;
    btnCombineSingleLineUp: TdxBarLargeButton;
    btnCombineSingleLineDown: TdxBarLargeButton;
    barSplitSub: TdxBar;
    btnSplitSub: TdxBarLargeButton;
    barMoveToPlayerTime: TdxBar;
    btnMoveCurrentToPlayerTime: TdxBarLargeButton;
    btnMoveFirstToCurrentToPlayerTime: TdxBarLargeButton;
    btnMoveCurrentToLastToPlayerTime: TdxBarLargeButton;
    btnMoveSelectedToPlayerTime: TdxBarLargeButton;
    btnMoveAllToPlayerTime: TdxBarLargeButton;
    newRecGallery: TdxRibbonDropDownGallery;
    btnNewRec2: TdxBarButton;
    btnNewRecBefore: TdxBarButton;
    edtFilter: TdxBarEdit;
    drpFilter: TdxBarCombo;
    edtReplace: TdxBarEdit;
    btnPrevVideoToSub: TdxBarButton;
    btnPrevSubToVideo: TdxBarButton;
    procedure edtTimeKeyPress(Sender: TObject; var Key: Char);
    procedure edtTimePropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
  private
    function getSubtitleGridWidth: integer;
    procedure setSubtitleGridWidth(const Value: integer);
    function getEditPanelHeight: integer;
    procedure setEditPanelHeight(const Value: integer);
    function getAutoSave: boolean;
    function getSyncSubToVideo: boolean;
    function getSyncVideoToSub: boolean;
    procedure setAutoSave(const Value: boolean);
    procedure setSyncSubToVideo(const Value: boolean);
    procedure setSyncVideoToSub(const Value: boolean);
  protected
  public
    property SubtitleGridWidth:integer read getSubtitleGridWidth write setSubtitleGridWidth;
    property EditPanelHeight:integer read getEditPanelHeight write setEditPanelHeight;

    property AutoSave:boolean read getAutoSave write setAutoSave;
    property SyncSubToVideo:boolean read getSyncSubToVideo write setSyncSubToVideo;
    property SyncVideoToSub:boolean read getSyncVideoToSub write setSyncVideoToSub;
  end;

implementation
uses StrUtils;

{$R *.dfm}

procedure TfrmCustomMain.edtTimePropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
const
    defaultPattern:string='00:00:00,000';
var
    deltaLength:Integer;
begin
    if Error then begin
        deltaLength:=Length(defaultPattern)-Length(DisplayValue);
        if deltaLength>0 then begin
            DisplayValue:=DisplayValue + RightStr(defaultPattern,deltaLength);
        end;

        Error:=false;
    end;
end;

procedure TfrmCustomMain.edtTimeKeyPress(Sender: TObject; var Key: Char);
var
    editBox:TcxCustomTextEdit;
begin
    editBox:=Sender as TcxCustomTextEdit;
    if editBox<>nil then begin
        editBox.SelLength:=1;
    end;
end;


//SubtitleGridWidth
function TfrmCustomMain.getSubtitleGridWidth: integer;
begin
    exit(gridSub.Width);
end;

procedure TfrmCustomMain.setSubtitleGridWidth(const Value: integer);
begin
    gridSub.Width:=Value;
end;

//EditPanelHeight
function TfrmCustomMain.getEditPanelHeight: integer;
begin
    exit(grpEdit.Height);
end;

procedure TfrmCustomMain.setEditPanelHeight(const Value: integer);
begin
    grpEdit.Height:=Value;
end;

//AutoSave
function TfrmCustomMain.getAutoSave: boolean;
begin
    exit(btnAutoSave.Down);
end;

procedure TfrmCustomMain.setAutoSave(const Value: boolean);
begin
    if btnAutoSave.Down<>Value then btnAutoSave.Click;
end;

//SyncSubToVideo
function TfrmCustomMain.getSyncSubToVideo: boolean;
begin
    exit(btnSyncSubToVideo.Down);
end;

procedure TfrmCustomMain.setSyncSubToVideo(const Value: boolean);
begin
    if btnSyncSubToVideo.Down<>Value then btnSyncSubToVideo.Click;
end;

//SyncVideoToSub
function TfrmCustomMain.getSyncVideoToSub: boolean;
begin
    exit(btnSyncVideoToSub.Down);
end;

procedure TfrmCustomMain.setSyncVideoToSub(const Value: boolean);
begin
    if btnSyncVideoToSub.Down<>Value then btnSyncVideoToSub.Click;
end;

end.

