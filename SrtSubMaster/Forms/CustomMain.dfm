object frmCustomMain: TfrmCustomMain
  Left = 0
  Top = 0
  Caption = 'SRT Sub Master (Custom)'
  ClientHeight = 623
  ClientWidth = 855
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 640
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  Icon.Data = {
    0000010001002020000001002000A81000001600000028000000200000004000
    0000010020000000000080100000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000010000000600000009000000090000000900000009000000090000
    0009000000080000000500000002000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000030000000D000000170000001C0000001C0000001C0000001C0000
    001C00000019000000140000000E000000080000000400000001000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000DEA789ACC0703BFFBD6D3CFFB56A40FFAC6943FFA46844FFA16748FF9A64
    48DE8959409661422F5200000020000000190000001000000006000000010000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000E7B7A00BD8B3A1A7A47153FFD47D4CFFE07B42FFD07440FFCB7647FFAC71
    50FFA16D50FF9E6B4EFF775C50E645413F790000001D00000011000000050000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000E8C7BA17D3B4A4C9AC7654FFCE7F50FFD68459FFB37450FFB071
    4BFFC88865FFBA8567FF856D61FF64656AF729323F8C0000001A0000000C0000
    0002000000000000000000000000000000000000000000000000000000000000
    0001000000030000000800000009000000090000000900000009000000090000
    00070000000200000000F3CCB32DC3A594E4AE6D46FFAA7558FFB5866DFFAF79
    58FFAA6D4BFFB28063FF9B7C6BFF506979FF264963D40C21314F000000140000
    0006000000000000000000000000000000000000000000000000000000000000
    00010000000700000012000000190000001C0000001C00000019000000150000
    000E000000050000000000000000E1BFAC4FA7816CF4A76C4AFFA9795DFFB487
    6BFFB38972FFA17C67FF66808FFF3E6A87FF2A5A77FF1B405AA60000081E0000
    000A00000001000000000000000000000000000000000000000000000000A267
    3841A96A3CFFA96A3CFFA96A3CFFA96A3CFFA96A3CFFA96A3CFFA96A3CFF0000
    0008000000020000000000000000FF7D7D02DDB7A281AD7E62FFAC7352FFA272
    58FF6E7B81FF5D7E94FF598097FF446D88FF2E5D7AFF3D545FFF000000200000
    0010000000040000000000000000000000000000000000000000000000000000
    0000A5653520AA6B3CD3D68655FFD2804EFFA66434CE60321331000000050000
    000000000000000000000000000000000000C4B1A81BB397879F967E70F4809A
    A9FF6198B9FF387BA8FF336789FF326383FF2A5A77FF365463FC4C3E317B0000
    0017000000090000000100000000000000000000000000000000000000000000
    0000000000009D5D2D10AC6D3FF0B97548FF7D451A6400000014000000050000
    00000000000000000000000000000000000000000000000000005F81979F3D72
    93FF6B9AB6FF669CBDFF5788A6FF306182FF326783FF496676FC3B3D3DAE060C
    0C2A000000120000000600000001000000000000000000000000000000000000
    00000000000000000000A7683A71C37F53FF975A2C9A00000019000000090000
    00010000000000000000000000000000000000000000000000007E5F46697172
    72FF427294FF558098FF40708FFF27587AFF43728AFC87969EF1716D6AFF2B2A
    288B0000001E0000000F00000004000000000000000000000000000000000000
    000000000000000000009D5D2D10AB6C3DF0A66639E20000001B0000000F0000
    00030000000000000000000000000000000000000000000000019F5E2EA3C175
    42FFB3876DFF947A6BFF6A767DFF698BA0FFCFD8DEFCB8B8B8FB8B8A8BFF5D5D
    5DF2262626660000001B0000000D000000030000000000000000000000000000
    0000000000000000000000000000A6683AA0BA774BFF7E481F63000000180000
    000D0000000900000009000000090000000900000009532D091BA76533FFDC84
    51FFDC8351FFDB8452FFC98B66FF838889FED0CCCAFF9E9E9EFF959596FF8F8F
    8FFF545454EC19191949000000190000000B0000000200000000000000000000
    0000000000000000000000000000A8683830B37244FF9D6133B60000001F0000
    001D0000001B0000001A0000001A0000001A0000001B8D4E218EBB6E3BFFDA80
    4EFFDA804EFFDF8A5CFFD59771FF8F7767B68B8B8BFECDCECEFFD8D8D8FE9E9E
    9EFF7E7E7EFF474747DA13131335000000150000000800000001000000000000
    000000000000000000000000000000000000A8693BC0CC8459FFA96A3CFFA96A
    3CFFA96A3CFFA96A3CFFA96A3CFFA96A3CFFA96A3CFFB76731FFD77E4CFFD77F
    4BFFD77F4BFFE8A680FFAC6B3DF25338252797979585AAA9A8FFB3B3B3FF9494
    94FFA2A2A2FF737373FF383838AD070707240000001200000006000000010000
    000000000000000000000000000000000000A6673A61BB794BFFCC7E4DFB934B
    16C6954C16C3954D17C1954D17C1954D17C1C97540F7CF7944FFD67B49FFD67C
    48FFD88251FFDEA27BFF9D6135AA0000000E89898921767675C7A6A6A5FFE4E5
    E4FFC0C0C0FF9E9E9EFF666666FF2C2C2C850000001D0000000F000000030000
    00000000000000000000000000000000000000000000A8693BEFA46637E20000
    001A0000000E000000030000000000000002A26133C2D99162FFEFA679FFEFA6
    79FFF7BC9AFFBC7E50FF774A274300000008000000008383833B8D8D8CECECEC
    ECFFEEEEEFFFB2B2B2FF939393FF565656FA2222226300000018000000070000
    00000000000000000000000000000000000000000000A8683B90BA7B4EFF7841
    195300000015000000060000000084441324AD6B3AFFEDA376FFEEA576FFEFA6
    79FFEDB99AFF9F6133C7000000100000000300000000000000008484846E9D9C
    9BF8F7F7F7FFE3E3E2FFAAAAAAFF878787FF454545E70000001B000000090000
    00000000000000000000000000000000000000000000A5653520B17344FF9B5C
    2EA7000000190000000B000000029A582684C77E4DFFECA173FFECA373FFF3B4
    8FFFCD9168FF8F593270000000090000000100000000000000008787870D7777
    77B4B9B9B9FFF5F5F6FED5D5D5FEA7A7A7FF767676F500000019000000080000
    0000000000000000000000000000000000000000000000000000A8693BC0AB6E
    3FF1371D06280000001100000007A36233E1E09264FFEC9C6FFFEC9F70FFF4BE
    9EFFA26132E30000001300000005000000000000000000000000000000008F89
    892A808080E2DCDCDCFFF3F3F3FFC9C8C8FD9B9B9CE100000015000000060000
    0000000000000000000000000000000000000000000000000000A6673A61C180
    54FF874E206F00000016884A175AB97445FFE9986BFFEA996CFFEFAA83FFD798
    71FF975D348D0000000C00000002000000000000000000000000000000000000
    00008A8A876F939292F6F0F0F0FFEAEAE8F3BABABAC10000000F000000040000
    000000000000000000000000000000000000000000000000000000000000A869
    3BEFA06032C40000001A985827B7D28354FFE99767FFE99769FFF7BC9BFFAD6D
    3DFF673F21330000000600000000000000000000000000000000000000000000
    00008B8B8B099E9D9BBAA3A29FDBE7E7E4B9C9C3BB7800000008000000010000
    000000000000000000000000000000000000000000000000000000000000A868
    3B90B47548FF67381346B06D3DFFE69264FFE79464FFEB9E72FFE1A47EFF9D61
    37AA0000000E0000000300000000000000000000000000000000000000000000
    000000000000A8A8A81E908E8BABB3B2B2996666661600000005000000000000
    000000000000000000000000000000000000000000000000000000000000A565
    3520B07144FFB37144E7DC8857FFE59161FFE69160FFF5B793FFB97646FF8051
    2C52000000080000000100000000000000000000000000000000000000000000
    00000000000000000000A8A8A827979696B30000000400000002000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000A8693BC0ECB697FFE69062FFE58F5FFFE89667FFEBAE8AFFA06335D50000
    0011000000040000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000A4653951CB8C65FFEDA67DFFE58F5FFFF1AF8AFFC18051FF8E5831710000
    000A000000010000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000A7693BE0F2B796FFE69163FFF1B594FFA76536F2462913230000
    0005000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000A7673B80D89A73FFF4B897FFD38E64FF9A60359C0000000D0000
    0002000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000A5653520B17144FFF8C0A1FFAC6837FF673F2133000000070000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000A7693AB0D48759FFA06438B60000000D000000030000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000A4653951B8784AFF9760344600000005000000010000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000AA6F48410000000100000001000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FF80
    0FFFFF8001FFFF8000FFFF8000FFFFC0007F0020007F0030003F0030003F80F8
    001FC0FE000FE07E000FE07C0007F0000003F0000001F8000000F8000000FC10
    0400FC100600FC000600FE000F00FE000F80FF001F80FF001FC1FF001FE1FF80
    3FFFFF803FFFFFC07FFFFFC07FFFFFC0FFFFFFE0FFFFFFE0FFFFFFF1FFFF}
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 12
  object dxRibbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 855
    Height = 170
    ApplicationButton.Menu = appMenu
    BarManager = barManager
    ColorSchemeName = 'Office2007Black'
    PopupMenuItems = []
    QuickAccessToolbar.Toolbar = barFrequentlyUsed
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object tabGeneral: TdxRibbonTab
      Active = True
      Caption = #24120#35268
      Groups = <
        item
          ToolbarName = 'barFile'
        end
        item
          ToolbarName = 'barVideoFile'
        end
        item
          ToolbarName = 'barExit'
        end>
      Index = 0
    end
    object tabEdit: TdxRibbonTab
      Caption = #32534#36753
      Groups = <
        item
          ToolbarName = 'barEditUtility'
        end
        item
          ToolbarName = 'barSynchronize'
        end
        item
          ToolbarName = 'barEdit'
        end>
      Index = 1
    end
    object tabAdjust: TdxRibbonTab
      Caption = #35843#25972
      Groups = <
        item
          ToolbarName = 'barAdjustTime'
        end
        item
          ToolbarName = 'barMoveToPlayerTime'
        end>
      Index = 2
    end
    object tabCombine: TdxRibbonTab
      Caption = #32452#21512#25286#20998
      Groups = <
        item
          ToolbarName = 'barCombineSingleLine'
        end
        item
          ToolbarName = 'barCombineMultiLine'
        end
        item
          ToolbarName = 'barSplitSub'
        end>
      Index = 3
    end
    object tabFindReplace: TdxRibbonTab
      Caption = #26597#25214#26367#25442
      Groups = <
        item
          ToolbarName = 'barFilter'
        end
        item
          ToolbarName = 'barReplace'
        end>
      Index = 4
    end
    object tabTextProcess: TdxRibbonTab
      Caption = #25991#26412#22788#29702
      Groups = <
        item
          ToolbarName = 'barAffix'
        end
        item
          ToolbarName = 'barTextOther'
        end>
      Index = 5
    end
    object tabImport: TdxRibbonTab
      Caption = #23548#20837#23548#20986
      Groups = <
        item
          ToolbarName = 'barImportExport'
        end
        item
          ToolbarName = 'barMerge'
        end>
      Index = 6
    end
    object tabHelp: TdxRibbonTab
      Caption = #24110#21161
      Groups = <
        item
          ToolbarName = 'barHelp'
        end>
      Index = 7
    end
  end
  object statusBar: TdxRibbonStatusBar
    Left = 0
    Top = 600
    Width = 855
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Bevel = dxpbRaised
        Width = 20
      end>
    Ribbon = dxRibbon
    Font.Charset = GB2312_CHARSET
    Font.Color = clDefault
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
  end
  object pnlMain: TPanel
    Left = 0
    Top = 170
    Width = 855
    Height = 430
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 6
    ExplicitTop = 168
    ExplicitHeight = 432
    object cxSplitter1: TcxSplitter
      Left = 0
      Top = 276
      Width = 855
      Height = 4
      AlignSplitter = salBottom
      InvertDirection = True
      MinSize = 140
      ResizeUpdate = True
      Control = grpEdit
      ExplicitTop = 278
    end
    object cxSplitter2: TcxSplitter
      Left = 494
      Top = 0
      Width = 4
      Height = 276
      MinSize = 1
      ResizeUpdate = True
      Control = gridSub
      ExplicitHeight = 278
    end
    object gridSub: TcxGrid
      Left = 0
      Top = 0
      Width = 494
      Height = 276
      Align = alLeft
      BorderStyle = cxcbsNone
      Constraints.MinWidth = 400
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #23435#20307
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      LookAndFeel.NativeStyle = False
      ExplicitHeight = 278
      object grdSub: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.First.Visible = True
        Navigator.Buttons.PriorPage.Visible = True
        Navigator.Buttons.Prior.Visible = True
        Navigator.Buttons.Next.Visible = True
        Navigator.Buttons.NextPage.Visible = True
        Navigator.Buttons.Last.Visible = True
        Navigator.Buttons.Insert.Visible = True
        Navigator.Buttons.Append.Visible = False
        Navigator.Buttons.Delete.Visible = True
        Navigator.Buttons.Edit.Visible = True
        Navigator.Buttons.Post.Visible = True
        Navigator.Buttons.Cancel.Visible = True
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = True
        Navigator.Buttons.GotoBookmark.Visible = True
        Navigator.Buttons.Filter.Visible = True
        DataController.DataSource = dscSubtitle
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.InfoText = #21333#20987#20197#28155#21152#34892
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.PullFocusing = True
        OptionsCustomize.ColumnGrouping = False
        OptionsCustomize.ColumnMoving = False
        OptionsData.Appending = True
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.NoDataToDisplayInfoText = '('#26080#23383#24149#25968#25454')'
        OptionsView.CellAutoHeight = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderAutoHeight = True
        OptionsView.HeaderHeight = 24
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 18
        object grdColNo: TcxGridDBColumn
          Caption = #24207#21495
          DataBinding.FieldName = 'no'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          HeaderAlignmentHorz = taCenter
          MinWidth = 55
          Options.Editing = False
          Options.Filtering = False
          Options.Grouping = False
          Options.HorzSizing = False
          Width = 55
        end
        object grdColTimeFrom: TcxGridDBColumn
          Caption = #36215#22987#26102#38388
          DataBinding.FieldName = 'timefrom'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.AutoSelect = False
          Properties.MaskKind = emkRegExprEx
          Properties.EditMask = '\d{2}:[0-5]\d:[0-5]\d,\d{3}'
          Properties.UseLeftAlignmentOnEditing = False
          HeaderAlignmentHorz = taCenter
          MinWidth = 94
          Options.Filtering = False
          Options.Grouping = False
          Options.HorzSizing = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 94
        end
        object grdColTimeTo: TcxGridDBColumn
          Caption = #32456#27490#26102#38388
          DataBinding.FieldName = 'timeto'
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.AutoSelect = False
          Properties.MaskKind = emkRegExprEx
          Properties.EditMask = '\d{2}:[0-5]\d:[0-5]\d,\d{3}'
          Properties.UseLeftAlignmentOnEditing = False
          HeaderAlignmentHorz = taCenter
          MinWidth = 94
          Options.Filtering = False
          Options.Grouping = False
          Options.HorzSizing = False
          Width = 94
        end
        object grdColContent: TcxGridDBColumn
          Caption = #20869#23481
          DataBinding.FieldName = 'content'
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Grouping = False
          Options.Sorting = False
          Width = 229
        end
      end
      object gridSubLevel1: TcxGridLevel
        GridView = grdSub
      end
    end
    object grpEdit: TcxGroupBox
      Left = 0
      Top = 280
      Align = alBottom
      Style.BorderStyle = ebsNone
      TabOrder = 3
      ExplicitTop = 282
      DesignSize = (
        855
        150)
      Height = 150
      Width = 855
      object lblContent: TLabel
        Left = 250
        Top = 21
        Width = 24
        Height = 12
        Caption = #20869#23481
        FocusControl = edtTimeFrom
        Transparent = True
      end
      object lblTimeTo: TLabel
        Left = 28
        Top = 77
        Width = 48
        Height = 12
        Caption = #32456#27490#26102#38388
        FocusControl = edtTimeTo
        Transparent = True
      end
      object lblTimeFrom: TLabel
        Left = 28
        Top = 21
        Width = 48
        Height = 12
        Caption = #36215#22987#26102#38388
        FocusControl = edtTimeFrom
        Transparent = True
      end
      object mmoContent: TcxDBMemo
        Left = 288
        Top = 17
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'content'
        DataBinding.DataSource = dscSubtitle
        Properties.ScrollBars = ssVertical
        TabOrder = 10
        Height = 119
        Width = 545
      end
      object edtTimeTo: TcxDBMaskEdit
        Left = 94
        Top = 73
        DataBinding.DataField = 'timeto'
        DataBinding.DataSource = dscSubtitle
        ParentFont = False
        Properties.AutoSelect = False
        Properties.MaskKind = emkRegExprEx
        Properties.EditMask = '\d{2}:[0-5]\d:[0-5]\d,\d{3}'
        Properties.MaxLength = 0
        Properties.OnValidate = edtTimePropertiesValidate
        Style.Font.Charset = GB2312_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -14
        Style.Font.Name = #23435#20307
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        OnKeyPress = edtTimeKeyPress
        Width = 129
      end
      object edtTimeFrom: TcxDBMaskEdit
        Left = 94
        Top = 17
        DataBinding.DataField = 'timefrom'
        DataBinding.DataSource = dscSubtitle
        ParentFont = False
        Properties.AutoSelect = False
        Properties.MaskKind = emkRegExprEx
        Properties.EditMask = '\d{2}:[0-5]\d:[0-5]\d,\d{3}'
        Properties.MaxLength = 0
        Properties.OnValidate = edtTimePropertiesValidate
        Style.Font.Charset = GB2312_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -14
        Style.Font.Name = #23435#20307
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        OnKeyPress = edtTimeKeyPress
        Width = 129
      end
      object btnTimeFromInc1000: TcxButton
        Left = 126
        Top = 38
        Width = 33
        Height = 29
        Hint = #36215#22987#26102#38388'+1'#31186
        Caption = '+1'
        TabOrder = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnTimeFromDec1000: TcxButton
        Left = 94
        Top = 38
        Width = 33
        Height = 29
        Hint = #36215#22987#26102#38388'-1'#31186
        Caption = '-1'
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnTimeFromInc100: TcxButton
        Left = 190
        Top = 38
        Width = 33
        Height = 29
        Hint = #36215#22987#26102#38388'+0.1'#31186
        Caption = '+0.1'
        TabOrder = 4
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnTimeFromDec100: TcxButton
        Left = 158
        Top = 38
        Width = 33
        Height = 29
        Hint = #36215#22987#26102#38388'-0.1'#31186
        Caption = '-0.1'
        TabOrder = 3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnTimeToInc1000: TcxButton
        Left = 126
        Top = 94
        Width = 33
        Height = 29
        Hint = #32456#27490#26102#38388'+1'#31186
        Caption = '+1'
        TabOrder = 7
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnTimeToDec1000: TcxButton
        Left = 94
        Top = 94
        Width = 33
        Height = 29
        Caption = '-1'
        TabOrder = 6
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnTimeToInc100: TcxButton
        Left = 190
        Top = 94
        Width = 33
        Height = 29
        Hint = #32456#27490#26102#38388'+0.1'#31186
        Caption = '+0.1'
        TabOrder = 9
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnTimeToDec100: TcxButton
        Left = 158
        Top = 94
        Width = 33
        Height = 29
        Hint = #32456#27490#26102#38388'-0.1'#31186
        Caption = '-0.1'
        TabOrder = 8
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object cxPageControl1: TcxPageControl
      Left = 498
      Top = 0
      Width = 357
      Height = 276
      Align = alClient
      TabOrder = 4
      Properties.ActivePage = tabPreview
      Properties.CustomButtons.Buttons = <>
      Properties.TabWidth = 50
      ExplicitHeight = 278
      ClientRectBottom = 272
      ClientRectLeft = 4
      ClientRectRight = 353
      ClientRectTop = 24
      object tabPreview: TcxTabSheet
        Caption = #39044#35272
        ImageIndex = 0
        object wmplayer: TWindowsMediaPlayer
          Left = 0
          Top = 0
          Width = 349
          Height = 209
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 245
          ExplicitHeight = 240
          ControlData = {
            000300000800000000000500000000000000F03F030000000000050000000000
            0000000008000200000000000300010000000B00FFFF0300000000000B00FFFF
            08000200000000000300320000000B00000008000A000000660075006C006C00
            00000B0000000B0000000B00FFFF0B00FFFF0B00000008000200000000000800
            020000000000080002000000000008000200000000000B000000F82300006515
            0000}
        end
        object cxSplitter3: TcxSplitter
          Left = 0
          Top = 209
          Width = 349
          Height = 4
          AlignSplitter = salBottom
          MinSize = 10
          ResizeUpdate = True
          Control = mnoDisplayingSubtitle
          ExplicitTop = 207
          ExplicitWidth = 348
        end
        object mnoDisplayingSubtitle: TcxMemo
          Left = 0
          Top = 213
          Align = alBottom
          Properties.Alignment = taCenter
          Properties.ReadOnly = True
          Properties.ScrollBars = ssVertical
          Style.BorderStyle = ebsNone
          TabOrder = 2
          Height = 35
          Width = 349
        end
      end
      object tabHistory: TcxTabSheet
        Caption = #21382#21490
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 245
        object gridHistory: TcxGrid
          Left = 0
          Top = 0
          Width = 348
          Height = 246
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 245
          object grdHistory: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dscHistory
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnGrouping = False
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnSorting = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsView.NoDataToDisplayInfoText = '('#26080#21382#21490#35760#24405')'
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            object colNo: TcxGridDBColumn
              Caption = #24207#21495
              DataBinding.FieldName = 'no'
              GroupSummaryAlignment = taCenter
              HeaderAlignmentHorz = taCenter
              MinWidth = 64
              Options.Editing = False
              Options.Filtering = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
            end
            object colDescription: TcxGridDBColumn
              Caption = #27493#39588
              DataBinding.FieldName = 'description'
              GroupSummaryAlignment = taCenter
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Grouping = False
              Options.Moving = False
            end
          end
          object gridHistoryLevel1: TcxGridLevel
            GridView = grdHistory
          end
        end
      end
    end
  end
  object barManager: TdxBarManager
    AlwaysSaveText = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Microsoft YaHei UI'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = imgSmallIcons
    ImageOptions.LargeImages = imgLargeIcons
    LookAndFeel.SkinName = 'Office2007Black'
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 120
    Top = 184
    DockControlHeights = (
      0
      0
      0
      0)
    object barFrequentlyUsed: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      Caption = #24120#29992
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 748
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnNew'
        end
        item
          ViewLevels = [ivlLargeIconWithText]
          Visible = True
          ItemName = 'btnOpen'
        end
        item
          Visible = True
          ItemName = 'btnOpenVideoFile'
        end
        item
          ViewLevels = [ivlLargeIconWithText]
          Visible = True
          ItemName = 'btnSave'
        end
        item
          Visible = True
          ItemName = 'btnSaveFileAs'
        end
        item
          Visible = True
          ItemName = 'btnUndo'
        end
        item
          Visible = True
          ItemName = 'btnRedo'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barEdit: TdxBar
      Caption = #32534#36753
      CaptionButtons = <>
      DockedLeft = 414
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnNewRec'
        end
        item
          Visible = True
          ItemName = 'btnNewRecAndPlayerToTimeFrom'
        end
        item
          Visible = True
          ItemName = 'btnPlayerToTimeFrom'
        end
        item
          Visible = True
          ItemName = 'btnPlayerToTimeTo'
        end
        item
          Visible = True
          ItemName = 'btnUpdateRec'
        end
        item
          Visible = True
          ItemName = 'btnCancelUpdateRec'
        end
        item
          Visible = True
          ItemName = 'btnDeleteRec'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barVideoFile: TdxBar
      Caption = #21442#32771#35270#39057#25991#20214
      CaptionButtons = <>
      DockedLeft = 187
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnOpenVideoFile'
        end
        item
          Visible = True
          ItemName = 'btnCloseVideoFile'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnPlayerPlay'
        end
        item
          Visible = True
          ItemName = 'btnPlayerPause'
        end
        item
          Visible = True
          ItemName = 'btnPlayerFastBackward'
        end
        item
          Visible = True
          ItemName = 'btnPlayerFastForward'
        end
        item
          Visible = True
          ItemName = 'btnPlayerStop'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barHelp: TdxBar
      Caption = #24110#21161
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 869
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnAbout'
        end
        item
          Visible = True
          ItemName = 'btnHomePage'
        end
        item
          Visible = True
          ItemName = 'btnWebsite'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barAdjustTime: TdxBar
      Caption = #26102#38388
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnResetNos'
        end
        item
          Visible = True
          ItemName = 'btnMoveTime'
        end
        item
          Visible = True
          ItemName = 'btnResizeTimeFrom'
        end
        item
          Visible = True
          ItemName = 'btnResizeTimeTo'
        end
        item
          Visible = True
          ItemName = 'btnScaleTime'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barImportExport: TdxBar
      Caption = #23548#20837#23548#20986
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnImport'
        end
        item
          Visible = True
          ItemName = 'btnImportText'
        end
        item
          Visible = True
          ItemName = 'btnExport'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barFile: TdxBar
      Caption = #23383#24149#25991#20214
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnNewFile'
        end
        item
          Visible = True
          ItemName = 'btnOpenFile'
        end
        item
          Visible = True
          ItemName = 'btnSaveFile'
        end
        item
          Visible = True
          ItemName = 'btnSaveFileAs'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barReplace: TdxBar
      Caption = #26367#25442
      CaptionButtons = <>
      DockedLeft = 305
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 130
          Visible = True
          ItemName = 'edtReplace'
        end
        item
          Visible = True
          ItemName = 'btnReplace'
        end
        item
          Visible = True
          ItemName = 'btnReplaceAll'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barFilter: TdxBar
      Caption = #31579#36873#19982#26597#25214
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 133
          Visible = True
          ItemName = 'drpFilter'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 133
          Visible = True
          ItemName = 'edtFilter'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 115
          Visible = True
          ItemName = 'chkFilterCaseSensitive'
        end
        item
          Visible = True
          ItemName = 'btnLargeFilter'
        end
        item
          Visible = True
          ItemName = 'btnLargeFind'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barAffix: TdxBar
      Caption = #21069#32512#21518#32512
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnAddAffix'
        end
        item
          Visible = True
          ItemName = 'btnRemoveAffix'
        end
        item
          Visible = True
          ItemName = 'btnDeleteAffixString'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barMerge: TdxBar
      Caption = #21512#25104#23548#20837
      CaptionButtons = <>
      DockedLeft = 157
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnMergeBySequence'
        end
        item
          Visible = True
          ItemName = 'btnMergeByNo'
        end
        item
          Visible = True
          ItemName = 'btnMergeByTimeFrom'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barExit: TdxBar
      Caption = #20851#38381#36864#20986
      CaptionButtons = <>
      DockedLeft = 542
      DockedTop = 0
      FloatLeft = 879
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnExit'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barEditUtility: TdxBar
      Caption = #32534#36753#24120#29992
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 889
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnUndo'
        end
        item
          Visible = True
          ItemName = 'btnRedo'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnAutoSave'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barSynchronize: TdxBar
      Caption = #21516#27493
      CaptionButtons = <>
      DockedLeft = 165
      DockedTop = 0
      FloatLeft = 889
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Position = ipContinuesRow
          Visible = True
          ItemName = 'btnSyncVideoToSub'
        end
        item
          Visible = True
          ItemName = 'btnSyncVideoToSubNow'
        end
        item
          Visible = True
          ItemName = 'btnPrevVideoToSub'
        end
        item
          BeginGroup = True
          Position = ipBeginsNewColumn
          Visible = True
          ItemName = 'btnSyncSubToVideo'
        end
        item
          Visible = True
          ItemName = 'btnSyncSubToVideoNow'
        end
        item
          Visible = True
          ItemName = 'btnPrevSubToVideo'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barTextOther: TdxBar
      Caption = #31354#30333#28165#38500
      CaptionButtons = <>
      DockedLeft = 292
      DockedTop = 0
      FloatLeft = 889
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnTrimWhiteSpace'
        end
        item
          Visible = True
          ItemName = 'btnDeleteEmptySubTitle'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barCombineMultiLine: TdxBar
      Caption = #32452#21512#20026#22810#34892
      CaptionButtons = <>
      DockedLeft = 225
      DockedTop = 0
      FloatLeft = 889
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnCombineMultiLineSelected'
        end
        item
          Visible = True
          ItemName = 'btnCombineMultiLineUp'
        end
        item
          Visible = True
          ItemName = 'btnCombineMultiLineDown'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barCombineSingleLine: TdxBar
      Caption = #32452#21512#20026#21333#34892
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 889
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnCombineSingleLineSelected'
        end
        item
          Visible = True
          ItemName = 'btnCombineSingleLineUp'
        end
        item
          Visible = True
          ItemName = 'btnCombineSingleLineDown'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barSplitSub: TdxBar
      Caption = #25286#20998
      CaptionButtons = <>
      DockedLeft = 450
      DockedTop = 0
      FloatLeft = 889
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnSplitSub'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object barMoveToPlayerTime: TdxBar
      Caption = #25353#25773#25918#22120#26102#38388#24179#31227
      CaptionButtons = <>
      DockedLeft = 389
      DockedTop = 0
      FloatLeft = 889
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnMoveCurrentToPlayerTime'
        end
        item
          Visible = True
          ItemName = 'btnMoveFirstToCurrentToPlayerTime'
        end
        item
          Visible = True
          ItemName = 'btnMoveCurrentToLastToPlayerTime'
        end
        item
          Visible = True
          ItemName = 'btnMoveSelectedToPlayerTime'
        end
        item
          Visible = True
          ItemName = 'btnMoveAllToPlayerTime'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object btnOpen: TdxBarLargeButton
      Caption = #25171#24320
      Category = 0
      Hint = #25171#24320
      Visible = ivAlways
      LargeImageIndex = 1
      ShortCut = 16463
    end
    object btnSave: TdxBarLargeButton
      Caption = #20445#23384
      Category = 0
      Hint = #20445#23384
      Visible = ivAlways
      LargeImageIndex = 2
      ShortCut = 16467
    end
    object btnSaveAs: TdxBarLargeButton
      Caption = #21478#23384#20026
      Category = 0
      Hint = #21478#23384#20026
      Visible = ivAlways
      LargeImageIndex = 3
    end
    object btnNew: TdxBarLargeButton
      Caption = #26032#24314
      Category = 0
      Hint = #26032#24314
      Visible = ivAlways
      LargeImageIndex = 0
    end
    object btnOpenVideoFile: TdxBarLargeButton
      Caption = #25171#24320#35270#39057
      Category = 0
      Hint = #25171#24320#35270#39057
      Visible = ivAlways
      LargeImageIndex = 5
    end
    object btnCloseVideoFile: TdxBarLargeButton
      Caption = #20851#38381#35270#39057
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 6
    end
    object btnSyncSubToVideo: TdxBarButton
      Align = iaClient
      Caption = #33258#21160#21516#27493#23383#24149#8594#35270#39057
      Category = 0
      Hint = #33258#21160#21516#27493#23383#24149#8594#35270#39057
      Visible = ivAlways
      ButtonStyle = bsChecked
    end
    object btnSyncVideoToSub: TdxBarButton
      Align = iaClient
      Caption = #33258#21160#21516#27493#23383#24149#8592#35270#39057
      Category = 0
      Hint = #33258#21160#21516#27493#23383#24149#8592#35270#39057
      Visible = ivAlways
      ButtonStyle = bsChecked
    end
    object btnNewRec: TdxBarLargeButton
      Caption = #26032#22686#35760#24405
      Category = 0
      Hint = #26032#22686#35760#24405
      Visible = ivAlways
      ButtonStyle = bsDropDown
      DropDownMenu = newRecGallery
      LargeImageIndex = 7
      ShortCut = 114
    end
    object btnNewRecAndPlayerToTimeFrom: TdxBarLargeButton
      Caption = #26032#22686#20026#36215#22987#26102#38388
      Category = 0
      Hint = #26032#22686#35760#24405#65292#24182#23558#25773#25918#22120#26102#38388#20316#20026#36215#22987#26102#38388
      Visible = ivAlways
      LargeImageIndex = 8
      ShortCut = 115
    end
    object btnPlayerToTimeTo: TdxBarLargeButton
      Caption = #35774#32622#20026#32467#26463#26102#38388
      Category = 0
      Hint = #23558#24403#21069#35760#24405#30340#32467#26463#26102#38388#35774#20026#25773#25918#22120#26102#38388
      Visible = ivAlways
      LargeImageIndex = 9
      ShortCut = 117
    end
    object btnUpdateRec: TdxBarLargeButton
      Caption = #25552#20132#26356#25913
      Category = 0
      Hint = #30830#35748#24403#21069#35760#24405#30340#20462#25913
      Visible = ivAlways
      LargeImageIndex = 10
      ShortCut = 118
    end
    object btnAbout: TdxBarLargeButton
      Caption = #20851#20110
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 26
      SyncImageIndex = False
      ImageIndex = 26
    end
    object btnHomePage: TdxBarLargeButton
      Caption = #23383#24149#32534#36753#22120#20027#39029
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 14
    end
    object btnCancelUpdateRec: TdxBarLargeButton
      Caption = #21462#28040#26356#25913
      Category = 0
      Hint = #21462#28040#24403#21069#35760#24405#30340#20462#25913
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object btnDeleteRec: TdxBarLargeButton
      Caption = #21024#38500#35760#24405
      Category = 0
      Hint = #21024#38500#22810#26465#36873#23450#30340#35760#24405
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object btnMoveTime: TdxBarLargeButton
      Caption = #24179#31227#26102#38388
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 16
    end
    object btnImport: TdxBarLargeButton
      Caption = #23548#20837
      Category = 0
      Hint = #23558#22806#37096#23383#24149#36861#21152#21040#24403#21069#32534#36753#29615#22659#20013
      Visible = ivAlways
      LargeImageIndex = 21
    end
    object btnNewFile: TdxBarLargeButton
      Caption = #26032#24314
      Category = 0
      Hint = #26032#24314
      Visible = ivAlways
      LargeImageIndex = 0
    end
    object btnOpenFile: TdxBarLargeButton
      Caption = #25171#24320
      Category = 0
      Hint = #25171#24320
      Visible = ivAlways
      LargeImageIndex = 1
      ShortCut = 16463
    end
    object btnSaveFile: TdxBarLargeButton
      Caption = #20445#23384
      Category = 0
      Hint = #20445#23384
      Visible = ivAlways
      LargeImageIndex = 2
      ShortCut = 16467
    end
    object btnSaveFileAs: TdxBarLargeButton
      Caption = #21478#23384#20026
      Category = 0
      Hint = #21478#23384#20026
      Visible = ivAlways
      LargeImageIndex = 3
    end
    object btnResetNos: TdxBarLargeButton
      Caption = #25972#29702#24207#21495
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 4
    end
    object btnExport: TdxBarLargeButton
      Caption = #23548#20986
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 22
    end
    object btnFilter: TdxBarButton
      Caption = #31579#36873
      Category = 0
      Visible = ivAlways
      ImageIndex = 24
    end
    object btnClearFilter: TdxBarButton
      Caption = #28165#38500#31579#36873
      Category = 0
      Visible = ivAlways
    end
    object btnAddAffix: TdxBarLargeButton
      Caption = #28155#21152#21069#32512#21518#32512
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 19
    end
    object btnRemoveAffix: TdxBarLargeButton
      Caption = #21024#38500#21069#32512#21518#32512
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 20
    end
    object btnDeleteAffixString: TdxBarLargeButton
      Caption = #21024#38500#21069#21518'n'#20010#23383#31526
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 20
    end
    object btnPlayerToTimeFrom: TdxBarLargeButton
      Caption = #35774#20026#36215#22987#26102#38388
      Category = 0
      Hint = #23558#24403#21069#35760#24405#30340#24320#22987#26102#38388#35774#20026#25773#25918#22120#26102#38388
      Visible = ivAlways
      LargeImageIndex = 13
      ShortCut = 116
    end
    object btnResizeTimeFrom: TdxBarLargeButton
      Caption = #20280#32553#36215#22987#26102#38388
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 17
    end
    object btnResizeTimeTo: TdxBarLargeButton
      Caption = #20280#32553#32456#27490#26102#38388
      Category = 0
      Hint = #20280#32553#32456#27490#26102#38388
      Visible = ivAlways
      LargeImageIndex = 18
    end
    object btnWebsite: TdxBarLargeButton
      Caption = 'MJ PC Lab'#20027#39029
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 51
    end
    object btnScaleTime: TdxBarLargeButton
      Caption = #26102#38388#36724#32553#25918
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 23
    end
    object chkRapidFilter: TcxBarEditItem
      Caption = #25935#25463#31579#36873
      Category = 0
      Hint = #25935#25463#31579#36873
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.FullFocusRect = True
      InternalEditValue = False
    end
    object btnFindFirst: TdxBarButton
      Caption = #26597#25214#31532#19968#20010
      Category = 0
      Visible = ivAlways
    end
    object btnFindNext: TdxBarButton
      Caption = #21521#21518#26597#25214
      Category = 0
      Hint = #21521#21518#26597#25214'(Ctrl+'#8595')'
      Visible = ivAlways
      ImageIndex = 25
    end
    object btnFindPrior: TdxBarButton
      Caption = #21521#21069#26597#25214
      Category = 0
      Hint = #21521#21069#26597#25214'(Ctrl+'#8593')'
      Visible = ivAlways
    end
    object btnFindLast: TdxBarButton
      Caption = #26597#25214#26368#21518#19968#20010
      Category = 0
      Visible = ivAlways
    end
    object btnLargeFilter: TdxBarLargeButton
      Caption = #31579#36873
      Category = 0
      Visible = ivAlways
      ButtonStyle = bsDropDown
      DropDownMenu = filterGallery
      LargeImageIndex = 24
    end
    object btnLargeFind: TdxBarLargeButton
      Caption = #21521#21518#26597#25214
      Category = 0
      Hint = #21521#21518#26597#25214'(Ctrl+'#8595')'
      Visible = ivAlways
      ButtonStyle = bsDropDown
      DropDownMenu = findGallery
      LargeImageIndex = 25
    end
    object dxBarSeparator1: TdxBarSeparator
      Caption = #31579#36873#25805#20316
      Category = 0
      Hint = #31579#36873#25805#20316
      Visible = ivAlways
    end
    object dxBarSeparator2: TdxBarSeparator
      Caption = #36873#39033
      Category = 0
      Hint = #36873#39033
      Visible = ivAlways
    end
    object chkFilterCaseSensitive: TcxBarEditItem
      Caption = #21306#20998#22823#23567#20889
      Category = 0
      Hint = #21306#20998#22823#23567#20889
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
      InternalEditValue = False
    end
    object btnMergeBySequence: TdxBarLargeButton
      Caption = #25353#39034#24207#21512#25104
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 27
    end
    object btnMergeByNo: TdxBarLargeButton
      Caption = #25353#23383#24149#24207#21495#21512#25104
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 27
    end
    object btnMergeByTimeFrom: TdxBarLargeButton
      Caption = #25353#36215#22987#26102#38388#21512#25104
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 27
    end
    object btnReplace: TdxBarLargeButton
      Caption = #26367#25442
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 28
    end
    object btnReplaceAll: TdxBarLargeButton
      Caption = #20840#37096#26367#25442
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 28
    end
    object barMergeMenu: TdxBarSubItem
      Caption = #21512#24182#23548#20837
      Category = 0
      Visible = ivAlways
      ImageIndex = 27
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnMergeBySequence'
        end
        item
          Visible = True
          ItemName = 'btnMergeByNo'
        end
        item
          Visible = True
          ItemName = 'btnMergeByTimeFrom'
        end>
    end
    object btnExit: TdxBarLargeButton
      Caption = #36864#20986
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 29
    end
    object btnPlayerPlay: TdxBarLargeButton
      Caption = #25773#25918
      Category = 0
      Hint = #25773#25918
      Visible = ivAlways
      LargeImageIndex = 31
      ShortCut = 8304
    end
    object btnPlayerPause: TdxBarLargeButton
      Caption = #26242#20572
      Category = 0
      Hint = #26242#20572
      Visible = ivAlways
      LargeImageIndex = 32
      ShortCut = 8305
    end
    object btnPlayerFastBackward: TdxBarLargeButton
      Caption = #24555#36864
      Category = 0
      Hint = #24555#36864
      Visible = ivAlways
      LargeImageIndex = 33
      ShortCut = 8307
    end
    object btnPlayerFastForward: TdxBarLargeButton
      Caption = #24555#36827
      Category = 0
      Hint = #24555#36827
      Visible = ivAlways
      LargeImageIndex = 34
      ShortCut = 8306
    end
    object btnPlayerStop: TdxBarLargeButton
      Caption = #20572#27490
      Category = 0
      Hint = #20572#27490
      Visible = ivAlways
      LargeImageIndex = 35
      ShortCut = 8308
    end
    object btnAutoSave: TdxBarLargeButton
      Caption = #33258#21160#20445#23384
      Category = 0
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 30
      SyncImageIndex = False
      ImageIndex = -1
    end
    object btnUndo: TdxBarLargeButton
      Caption = #25764#38144
      Category = 0
      Hint = #25764#38144
      Visible = ivAlways
      LargeImageIndex = 36
    end
    object btnRedo: TdxBarLargeButton
      Caption = #37325#20570
      Category = 0
      Hint = #37325#20570
      Visible = ivAlways
      LargeImageIndex = 37
    end
    object btnSyncSubToVideoNow: TdxBarButton
      Align = iaClient
      Caption = #31435#21363#21516#27493#23383#24149#8594#35270#39057
      Category = 0
      Visible = ivAlways
    end
    object btnSyncVideoToSubNow: TdxBarButton
      Align = iaClient
      Caption = #31435#21363#21516#27493#23383#24149#8592#35270#39057
      Category = 0
      Visible = ivAlways
    end
    object btnTrimWhiteSpace: TdxBarLargeButton
      Caption = #28165#38500#31354#30333#25991#26412
      Category = 0
      Hint = #28165#38500#31354#30333#25991#26412
      Visible = ivAlways
      LargeImageIndex = 39
    end
    object btnDeleteEmptySubTitle: TdxBarLargeButton
      Caption = #21024#38500#31354#23383#24149
      Category = 0
      Hint = #21024#38500#31354#23383#24149
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object btnImportText: TdxBarLargeButton
      Caption = #23548#20837#25991#26412
      Category = 0
      Hint = #23548#20837#25991#26412
      Visible = ivAlways
      LargeImageIndex = 41
      SyncImageIndex = False
      ImageIndex = -1
    end
    object btnCombineMultiLineSelected: TdxBarLargeButton
      Caption = #32452#21512#36873#20013#35760#24405
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 42
    end
    object btnCombineMultiLineUp: TdxBarLargeButton
      Caption = #21521#19978#32452#21512
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 43
    end
    object btnCombineMultiLineDown: TdxBarLargeButton
      Caption = #21521#19979#32452#21512
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 44
    end
    object btnCombineSingleLineSelected: TdxBarLargeButton
      Caption = #32452#21512#36873#20013#35760#24405
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 42
    end
    object btnCombineSingleLineUp: TdxBarLargeButton
      Caption = #21521#19978#32452#21512
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 43
    end
    object btnCombineSingleLineDown: TdxBarLargeButton
      Caption = #21521#19979#32452#21512
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 44
    end
    object btnSplitSub: TdxBarLargeButton
      Caption = #25286#20998#24403#21069#23383#24149
      Category = 0
      Hint = #25286#20998#24403#21069#23383#24149
      Visible = ivAlways
      LargeImageIndex = 45
    end
    object btnMoveCurrentToPlayerTime: TdxBarLargeButton
      Caption = #24179#31227#24403#21069
      Category = 0
      Hint = #24179#31227#24403#21069
      Visible = ivAlways
      LargeImageIndex = 46
    end
    object btnMoveFirstToCurrentToPlayerTime: TdxBarLargeButton
      Caption = #24179#31227#31532#19968#26465#21040#24403#21069
      Category = 0
      Hint = #24179#31227#31532#19968#26465#21040#24403#21069
      Visible = ivAlways
      LargeImageIndex = 47
    end
    object btnMoveCurrentToLastToPlayerTime: TdxBarLargeButton
      Caption = #24179#31227#24403#21069#21040#26368#21518#19968#26465
      Category = 0
      Hint = #24179#31227#24403#21069#21040#26368#21518#19968#26465
      Visible = ivAlways
      LargeImageIndex = 48
    end
    object btnMoveSelectedToPlayerTime: TdxBarLargeButton
      Caption = #24179#31227#36873#20013#35760#24405
      Category = 0
      Hint = #24179#31227#36873#20013#35760#24405
      Visible = ivAlways
      LargeImageIndex = 49
    end
    object btnMoveAllToPlayerTime: TdxBarLargeButton
      Caption = #24179#31227#20840#37096
      Category = 0
      Hint = #24179#31227#20840#37096
      Visible = ivAlways
      LargeImageIndex = 50
    end
    object btnNewRec2: TdxBarButton
      Caption = #26032#22686#35760#24405
      Category = 0
      Hint = #26032#22686#35760#24405
      Visible = ivAlways
    end
    object btnNewRecBefore: TdxBarButton
      Caption = #26032#22686#35760#24405'('#21069#32622')'
      Category = 0
      Hint = #26032#22686#35760#24405'('#21069#32622')'
      Visible = ivAlways
    end
    object edtFilter: TdxBarEdit
      Caption = #26597#25214#20869#23481
      Category = 0
      Hint = #26597#25214#20869#23481
      Visible = ivAlways
    end
    object drpFilter: TdxBarCombo
      Caption = #26597#25214#26041#24335
      Category = 0
      Hint = #26597#25214#26041#24335
      Visible = ivAlways
      Text = #21253#21547
      ShowEditor = False
      Items.Strings = (
        #21253#21547
        #20197'...'#24320#22836
        #20197'...'#32467#23614)
      ItemIndex = 0
    end
    object edtReplace: TdxBarEdit
      Caption = #26367#25442#20869#23481
      Category = 0
      Hint = #26367#25442#20869#23481
      Visible = ivAlways
    end
    object btnPrevVideoToSub: TdxBarButton
      Align = iaClient
      Caption = #19978#27425#31435#21363#21516#27493#28857
      Category = 0
      Hint = #19978#27425#31435#21363#21516#27493#28857
      Visible = ivAlways
    end
    object btnPrevSubToVideo: TdxBarButton
      Align = iaClient
      Caption = #19978#27425#31435#21363#21516#27493#28857
      Category = 0
      Hint = #19978#27425#31435#21363#21516#27493#28857
      Visible = ivAlways
    end
  end
  object dxSkinController1: TdxSkinController
    SkinName = 'Office2007Black'
    Left = 152
    Top = 248
  end
  object dscSubtitle: TDataSource
    Left = 248
    Top = 344
  end
  object cxLookAndFeelController1: TcxLookAndFeelController
    SkinName = 'Office2007Black'
    Left = 104
    Top = 232
  end
  object imgLargeIcons: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 19923344
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000060000000800000008000000080000
          0008000000080000000800000008000000080000000800000008000000080000
          0008000000080000000800000008000000080000000800000008000000080000
          0008000000060000000200000000000000000000000000000000000000000000
          0000000000000000000000000006000000110000001700000017000000170000
          0017000000170000001700000017000000170000001700000017000000170000
          0017000000170000001700000017000000170000001700000017000000170000
          0017000000110000000600000000000000000000000000000000000000000000
          00000000000000000000B39C8CFFA89787FF917D6CFF816C58FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B49D8CFFFCF8F6FFFBF8F5FFFCF6F4FFFCF6F4FFFBF5
          F2FFFBF4F1FFFAF3F0FFFAF3EFFFF9F2EEFFF9F1EDFFF9F1ECFFF9F0EBFFF8EF
          EAFFF9EFEAFFF8EEE9FFF7EDE8FFF7EEE7FFF7EDE7FFF7ECE7FFF7ECE6FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B49D8CFFFCF8F7FFFCF7F5FFFBF7F4FFFBF6F3FFFBF5
          F2FFFBF5F1FFFAF4F0FFFAF3EFFFFAF2EEFFFAF2EDFFF9F1ECFFF9F0EBFFF8EF
          EAFFF8EEE9FFF8EFE9FFF8EEE8FFF7EDE7FFF8EDE7FFF7EDE7FFF7ECE7FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B49D8CFFFCF9F6FFFCF8F5FFFCF7F4FFFCF6F3FFFBF5
          F2FFFBF4F1FFFAF4F1FFFAF3EFFFFAF3EEFFFAF2EDFFF9F1EDFFF9F0ECFFF8F0
          EBFFF8EFEAFFF8EFEAFFF7EEE8FFF7EDE8FFF8EDE7FFF7EDE7FFF7EDE7FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B49D8DFFFDF9F7FFFCF7F5FFFBF7F4FFFBF6F4FFFBF5
          F2FFFBF5F1FFFBF4F0FFFAF3F0FFFAF3EFFFF9F1EEFFF9F1EDFFF9F0ECFFF9F0
          EBFFF8EFEAFFF9EEE9FFF8EEE9FFF7EDE8FFF8EEE8FFF8EDE8FFF8EDE7FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B59D8DFFFDF8F6FFFCF7F5FFFCF7F5FFFBF6F4FFFBF5
          F2FFFBF5F2FFFAF5F1FFFAF3EFFFFAF2EFFFF9F2EEFFFAF1EDFFF9F0ECFFF9F0
          EBFFF9EFEBFFF8EFEAFFF8EEE9FFF8EFE9FFF8EEE8FFF7EEE8FFF8EEE8FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B59E8EFFFDF9F7FFFDF8F6FFFCF8F5FFFBF6F4FFFBF6
          F3FFFBF5F2FFFAF4F1FFFAF3F0FFFAF3EFFFFAF2EFFFF9F1EEFFF9F1ECFFF8F1
          ECFFF9F0EBFFF9EFEAFFF8EFEAFFF8EFE9FFF8EEE9FFF8EFE9FFF8EEE9FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B69E8FFFFCF8F7FFFDF8F6FFFCF8F5FFFCF6F4FFFBF6
          F3FFFAF5F2FFFBF5F2FFFAF4F0FFFAF3F0FFFAF2EFFFF9F2EDFFF9F1EEFFF9F1
          ECFFF9F0ECFFF9F0EBFFF8EFEBFFF9EFEBFFF8EFEAFFF8EFE9FFF8EFE9FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B69F8FFFFCF9F7FFFCF9F6FFFCF7F5FFFCF7F4FFFBF7
          F3FFFBF6F2FFFBF5F2FFFAF4F1FFFAF4F0FFFAF4EFFFFAF3EEFFFAF1EEFFF9F1
          EDFFF9F1ECFFF9F0ECFFF8F0ECFFF8F0EBFFF8F0EAFFF9EFEAFFF8EFEAFF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B6A090FFFCF9F7FFFCF9F7FFFCF8F6FFFCF8F5FFFBF7
          F4FFFBF6F3FFFBF5F2FFFAF5F1FFFBF4F1FFFAF4F0FFFAF3F0FFFAF2EEFFF9F2
          EEFFF9F2EDFFFAF1EDFFF9F1EDFFF9F0ECFFF9F0ECFFF8F0EBFFF8F0EBFF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B6A091FFFDF9F8FFFCF9F8FFFCF9F7FFFCF8F6FFFCF7
          F5FFFBF6F4FFFBF6F3FFFBF6F2FFFBF5F1FFFBF4F1FFFAF4F0FFFAF3EFFFF9F2
          EFFFFAF2EDFFFAF2EEFFF9F2EDFFF9F1EDFFF9F0EDFFF9F1ECFFF9F1ECFF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B6A091FFFDFBF9FFFDF9F7FFFDF9F7FFFCF8F6FFFCF8
          F6FFFCF7F4FFFCF6F3FFFBF5F3FFFBF5F2FFFBF5F1FFFAF4F1FFFAF3F0FFFAF3
          EFFFF9F2EFFFFAF2EFFFF9F2EEFFF9F2EDFFFAF1EDFFF9F1EDFFF9F2EDFF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B6A292FFFDFAF8FFFCFAF8FFFDFAF7FFFCF8F6FFFCF8
          F6FFFCF7F5FFFBF6F4FFFBF6F3FFFBF5F3FFFBF5F2FFFBF4F2FFFAF4F0FFFAF3
          F0FFFAF4F0FFFAF3EFFFF9F2EEFFFAF2EFFFFAF2EEFFF9F2EEFFF9F2EEFF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B7A293FFFDFAF9FFFDFAF8FFFDFAF7FFFDF9F7FFFCF9
          F6FFFCF8F6FFFCF8F5FFFCF7F4FFFBF7F4FFFBF6F3FFFBF5F2FFFBF4F1FFFAF5
          F1FFFAF4F0FFFAF4F0FFFAF3F0FFFAF3EFFFFAF3EFFFFAF3EFFFF9F3F0FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B7A293FFFDFBFAFFFDFBF9FFFDFAF8FFFCF9F7FFFCF9
          F7FFFCF8F6FFFCF8F6FFFCF7F5FFFBF7F4FFFBF6F4FFFBF6F3FFFBF5F2FFFBF5
          F1FFFBF5F1FFFBF4F1FFFBF4F1FFFBF4F1FFFBF4F0FFFAF4F0FFFAF4F0FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B9A393FFFDFCFAFFFDFBFAFFFDFAF9FFFCF9F8FFFCF9
          F8FFFCF9F7FFFDF9F7FFFCF8F6FFFCF7F5FFFBF6F4FFFCF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFBF5F2FFFAF5F2FFFBF5F1FFFAF5F2FFFBF5F1FFFBF4F1FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B9A395FFFEFCFBFFFDFBFAFFFDFBF9FFFDFBF8FFFDF9
          F8FFFDFAF7FFFCF9F7FFFDF8F7FFFCF8F6FFFCF7F5FFFBF7F4FFFCF7F4FFFCF6
          F4FFFCF6F3FFFBF6F3FFFBF6F3FFFBF5F3FFFBF5F2FFFBF5F2FFFBF5F2FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B9A495FFFEFCFCFFFEFBFBFFFDFBFAFFFEFAFAFFFDFA
          F9FFFDFAF9FFFDFAF8FFFDF9F8FFFDF9F6FFFCF9F6FFFCF8F6FFFCF8F5FFFBF7
          F5FFFBF7F4FFFBF7F4FFFBF6F3FFFCF6F3FFFCF6F3FFFBF6F3FFFCF6F3FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000B9A596FFFEFDFCFFFEFCFBFFFEFBFBFFFEFBFAFFFDFB
          F9FFFDFAF9FFFDFAF8FFFDF9F8FFFCF9F8FFFCF9F7FFFCF9F6FFFDF9F6FFFCF7
          F6FFFCF7F5FFFCF7F5FFFCF7F4FFFBF7F5FFFCF7F4FFFCF6F4FFFBF6F4FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000BAA596FFFEFDFCFFFEFDFCFFFEFDFBFFFDFCFAFFFDFB
          FAFFFEFBF9FFFDFBF9FFFDFAF8FFFDFAF8FFFDF9F7FFFCF9F7FFFCF9F7FFFCF8
          F6FFFDF9F6FFFCF8F6FFFCF8F6FFFCF8F6FFFCF7F5FFFCF7F5FFFCF8F5FF806D
          59FF000000170000000800000000000000000000000000000000000000000000
          00000000000000000000BBA596FFFFFDFDFFFEFDFCFFFEFDFCFFFEFCFCFFFEFC
          FAFFFDFBFAFFFDFBF9FFFDFBF9FFFDFAF9FFFDFAF8FFFCFAF8FFFDFAF8FFFCF9
          F7FFFDF9F7FFFCF8F7FFFCF8F7FFFDF9F6FFFDF8F6FFFCF8F7FFFCF9F6FF806D
          59FF000000140000000600000000000000000000000000000000000000000000
          00000000000000000000BAA797FFFEFEFDFFFEFEFDFFFFFDFDFFFEFDFCFFFEFC
          FBFFFEFCFBFFFEFCFBFFFEFBFBFFFEFBFAFFFDFAF9FFFDFAF9FFFDFAF8FFFDFA
          F8FFB49C8CFF988270FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF0000000B0000000300000000000000000000000000000000000000000000
          00000000000000000000BBA798FFFEFEFEFFFEFEFDFFFEFDFDFFFEFEFCFFFEFD
          FCFFFEFDFCFFFEFDFBFFFEFCFBFFFDFCFAFFFDFBFAFFFDFBFAFFFEFBF9FFFDFB
          F9FFB49C8CFFFFFFFFFFEEDBCEFFECD2BFFFE4C5B0FFD9BAA4FF503A26FF1A16
          1335000000030000000000000000000000000000000000000000000000000000
          00000000000000000000BBA798FFFEFEFEFFFEFEFEFFFFFDFEFFFEFDFDFFFFFD
          FCFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFBFFFEFCFBFFFDFCFAFFFEFBFAFFFEFB
          FAFFB49C8CFFFFFFFFFFFFEEE4FFF5E1D3FFE1C6B2FF654E36FF1A1613350000
          0003000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BBA799FFFFFFFFFFFFFEFFFFFFFEFEFFFEFEFDFFFFFE
          FDFFFEFEFDFFFEFEFCFFFEFDFCFFFEFCFCFFFEFDFCFFFDFCFBFFFDFCFBFFFEFC
          FAFFB49C8CFFFFFFFFFFFFEEE4FFE9D4C5FF6C553EFF1A161335000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BBA799FFFFFFFFFFFFFFFFFFFFFFFEFFFEFFFEFFFFFE
          FEFFFEFEFEFFFFFEFDFFFFFDFDFFFEFDFCFFFEFDFCFFFEFDFCFFFEFCFBFFFEFD
          FBFFB49C8CFFFFFFFFFFF3E0D5FF836E59FF1A16133500000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BCA899FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFF
          FEFFFFFFFEFFFEFEFEFFFEFEFDFFFFFEFDFFFFFDFDFFFEFDFDFFFEFDFCFFFEFD
          FCFFB49C8CFFF3E0D5FF9C8674FF1A1613340000000300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BCA89AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFEFFFFFEFEFFFFFEFEFFFFFFFEFFFFFEFEFFFFFDFDFFFEFEFDFFFEFE
          FDFFB49C8CFFBDA89AFF13100E2A000000030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BCA89AFFBBA89AFFBBA799FFBBA698FFBAA696FFBAA4
          96FFB9A495FFB8A294FFB8A292FFB6A191FFB69F90FFB59E8FFFB59D8DFFB49D
          8CFFB49C8CFF2823203B00000003000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000050000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000800000005000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0005000000120000001B0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001A00000012000000080000
          0002000000000000000000000000000000000000000000000000000000003A5C
          7CA75E88ADFF5E88ADFF5F84ADFF5F85ADFF5C82A9FF5981A6FF5A8AB4FF5890
          B5FF5990B5FF5889B5FF5B8CB2FF588AB4FF588AB1FF588AAEFF5888AEFF5889
          B1FF588AB4FF5A8BB5FF5895BEFF5490BAFF24374B8B03040526000000130000
          0006000000000000000000000000000000000000000000000000000000006E97
          C1FF709FC6FF9CE1FCFF61C5EFFF60C4F0FF60C4F0FF5FC4F0FF60C4F0FF60C4
          EFFF60C4EFFF60C4F0FF60C4F0FF5FC4F0FF60C4F0FF60C5F0FF60C4F0FF60C4
          F0FF5FC4F0FF60C4EFFF5FC4F0FF60C4F0FF52A4CDFD3A4F66AD0000001E0000
          000F00000003000000000000000000000000000000000000000000000000799B
          C5FF79B2D4FFA3E3FCFF7CD1F6FF62C6F1FF62C5F0FF62C6F0FF62C5F1FF62C6
          F1FF62C6F1FF62C6F1FF62C6F0FF62C5F1FF62C5F1FF62C5F1FF62C5F1FF62C6
          F1FF62C5F0FF63C5F1FF62C5F1FF62C5F1FF60C3EEFF4874A1FD13181C460000
          001900000009000000010000000000000000000000000000000000000000799D
          C7FF84C7E3FF7EB4D6FFAAE6FEFF68C9F3FF65C7F1FF65C7F2FF65C7F2FF65C7
          F2FF65C8F2FF65C7F2FF65C7F2FF65C7F2FF65C7F2FF65C7F2FF65C7F2FF65C8
          F2FF65C7F2FF65C7F2FF65C7F1FF65C8F2FF65C7F2FF58ADD6FC446389DA0609
          0A2B010101140000000500000000000000000000000000000000000000007A9D
          C8FF9DDBF2FF73A7CAFFA4E4FFFF8DDAFAFF68C9F3FF69C9F4FF68C9F3FF68C9
          F3FF68C9F3FF68C9F3FF68C9F3FF68C9F4FF68C9F3FF68C9F3FF68C9F3FF69C9
          F3FF68C9F3FF68C9F3FF69C9F3FF68C9F4FF69C9F3FF68C8F2FF4A7BAAFD283A
          457A040607230101010D00000002000000000000000000000000000000007B9F
          C9FFA6EAFEFF74BDDDFF7BB3D5FFABE7FFFF73CFF7FF6BCBF5FF6CCBF4FF6BCB
          F4FF6CCBF4FF6CCBF4FF6BCBF4FF6BCBF5FF6BCBF4FF6BCBF5FF6BCBF5FF6CCB
          F5FF6BCBF4FF6CCBF4FF6BCBF4FF6BCBF5FF6CCBF5FF6BCBF4FF60BBE2FC4A6F
          99F10A1011340102021700000007000000000000000000000000000000007CA1
          CCFFA2EBFFFF77CDEBFF74A8C7FF9FE3FEFF9BE0FDFF6FCDF5FF6FCDF6FF6ECD
          F6FF6FCDF6FF6FCDF6FF6FCDF6FF6FCDF5FF6ECDF6FF6ECDF6FF6FCDF6FF6FCD
          F6FF6FCDF6FF6FCDF6FF6FCDF6FF6FCDF6FF6FCDF6FF6FCDF6FF6FCDF6FF528B
          B4FC385162A3030505230000000E000000030000000000000000000000007CA3
          CDFFAEEEFFFF8CDFF9FF73BDDCFF78ADCEFFA9E6FFFF7FD5FAFF72CFF7FF72CF
          F7FF72CFF8FF72CFF8FF72CFF8FF72CFF7FF72CFF7FF72CFF7FF72CFF8FF72CF
          F7FF72CFF7FF72CFF7FF72CFF7FF72CEF7FF72CFF7FF72CFF7FF72CFF7FF6CC7
          EFFE4D78A5FC0F14173D01010118000000080000000100000000000000007EA5
          CFFFB6EEFFFF9AE9FFFF76CBEAFF75A8C7FF9BE2FFFFA5E5FFFF76D1F9FF75D1
          F8FF75D1F8FF75D1F8FF75D0F9FF75D1F9FF75D1F8FF75D1F8FF75D1F9FF75D0
          F8FF75D1F8FF75D1F8FF75D0F9FF75D1F8FF75D1F9FF75D1F8FF75D1F9FF75D1
          F8FF5EA1CBFC46617EC7040405230000000F0000000400000000000000007FA7
          D2FFB8F1FFFF99E9FFFF81DBF7FF74C1E0FF73B3D8FFA5E5FFFF8ADAFCFF78D2
          F9FF77D2FAFF78D2F9FF77D2FAFF77D2FAFF77D2F9FF77D2FAFF78D2F9FF77D2
          FAFF77D2F9FF77D2F9FF77D2FAFF78D2F9FF77D2F9FF77D2FAFF77D1FAFF77D2
          F9FF76D1F8FF507DAEFE1A242A570203031D0000000B000000020000000080A9
          D3FFB9F0FFFF9CEAFFFF8CE6FEFF78CCEAFF72BEE2FF89CEF4FFA9E6FFFF7BD4
          FBFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3
          FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3
          FAFF79D3FAFF6CB9DFFB527298E7090C0D2E02030317000000050000000081AB
          D5FFB8F0FFFF9FEBFFFF90E7FEFF82D9F4FF73CAEAFF6FB3DDFFA1E4FFFF92DD
          FDFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3
          FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3FAFF79D3
          FAFF79D3FAFF79D3FAFF4F84B9FD3147578F050606200000000A0000000082AC
          D7FFB9F0FFFFA3ECFFFF95E8FFFF8BE5FDFF7CD2EFFF70C7E8FF90C3ECFFA4E2
          FDFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8
          FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8FFFFAEE8
          FFFFAEE8FFFFAEE8FFFF74B7D9F44581B3F809090920000000080000000083AF
          D9FFBCF1FFFFA6EDFFFFA5ECFFFF93E8FEFF9CEAFFFF9FEBFFFF8FD7F8FF8CB8
          E3FF8CB8E3FF8CB8E3FF8AB7E3FF89B7E3FF88B7E3FF86B7E3FF85B6E4FF84B7
          E4FF83B5E4FF82B5E4FF81B6E5FF80B5E5FF78AEE5FF78AEE5FF78AEE5FF78AE
          E5FF70A7DEFF68A1D6FF528AC5EF4E92D3F60404040C000000020000000084B1
          DCFFBDF2FFFFA8EDFFFFA8EDFFFFA7EEFFFFA6ECFFFFA0ECFFFF9CEAFFFF94E7
          FFFF8CE4FFFF86E3FFFF81E3FFFF7DE2FFFF78E3FFFF75E0FFFF73DCFAFF71DC
          FAFF6CD9F8FF6CD7F6FF69D4F6FF69D1F1FF66CDF0FF548DB9F10000001B0000
          00090000000000000000000000000000000000000000000000000000000085B2
          DDFFBDF2FFFFAAEFFFFFA8EEFFFFA8EDFFFFA8EDFFFFA5ECFFFFA1EBFFFF99E9
          FFFF93E8FFFF8DE6FFFF8CE6FFFF97EAFFFF95E9FFFF90E7FFFF8DE7FEFF89E4
          FDFF88E1FAFF85DEFBFF83DCF8FF81D9F7FF7CD4F6FF588AC4FF000000130000
          00060000000000000000000000000000000000000000000000000000000086B3
          DEFFBEF2FFFFA8EFFFFFA7EFFFFFA9EFFFFFACEEFFFFA9EEFFFFA5EBFFFF9FEB
          FFFF9BE9FFFF9CE8FFFFA3E3FBFF8DC6F0FF81B6E5FF81B6E5FF80B5E5FF7FB5
          E5FF7FB5E5FF7CB4E4FF78AEE4FF77ADE1FF75B1E5FF5C93B2CB0000000C0000
          000B0000000900000007000000020000000000000000000000000000000086B4
          DFFFB3E6FAFFB1EFFFFFA8EEFFFFACEFFFFFB0F0FFFFACEEFFFFA7EFFFFFA1EC
          FFFFA0EBFFFFA5E2FAFF8CBCE6FE2A3B4A600000000600000001000000000000
          00000000000000000000000000000000000000000000000000040000000E0000
          00170000001B0000001500000007000000000000000000000000000000003349
          5D6B8DBEE5FFB8EBFCFFBFF2FFFFC1F2FFFFC1F3FFFFC2F2FFFFBFF1FFFFB6F0
          FFFFACE6FAFF90C1E9FF435A728B000000070000000100000000000000000000
          00000000000200000004000000020000000000000000675445CB80634FFF7F5D
          49FF7F5A44FF0000001B00000009000000000000000000000000000000000000
          0000627F9BAA8CB8E3FF8CB8E3FF8BB8E3FF8BB8E3FF89B8E3FF88B7E3FF87B7
          E4FF86B7E4FF3E53687900000004000000010000000000000000000000000000
          0000000000040000000A0000000A0000000500000003000000064A3A30987F63
          52FD7F5E4AFF0000001700000009000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000302
          0106554438AD15110E3300000010000000100000000F1E1814497F6352FD3F31
          2986816552FF0000000D00000006000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001D17133B6C5546D82E241E642E241E682E241E696B5346D944352C8E0000
          00066A5A4DCC0000000400000002000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001F18143F6C5446D7816554FF6C5546D82D231D5F000000040000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000C0A6A7FFA55B6CFFA96270FFA45E6CFF934556FFA6A3
          A3FFA4A1A1FFA19C9CFF9C9797FF979393FF948D8DFF8E8888FF8A8383FF857D
          7CFF807978FF807978FF807978FF994759FF994759FF994759FF994759FF9947
          59FF994759FF994759FF994756FF000000000000000000000000000000000000
          000000000000D2C1C1FFB28181FFAF6D79FFB8717FFFB7707DFFAA6472FFEFEF
          F1FFEFEFF1FF995D68FFB8717EFFB06B78FFE1E0E0FFDDDADBFFD7D5D6FFD4D1
          D2FFCFCCCCFFCAC6C6FFDBD8D8FF8E4A59FFA05565FFA05565FFBF848EFFB972
          7FFFB8727EFFB8717EFF994557FF000000000000000000000000000000000000
          000000000000AE6A7AFFAF6D79FFBA7481FFB97380FFB8737FFFAA6472FFE9E9
          EBFFECEBEFFF995D68FFB8717EFFB06B78FFE1DFE1FFDCDADDFFD7D5D6FFD3D0
          D1FFCDCACBFFC8C3C3FFD9D6D6FF8E4A59FF9F5464FFA05565FFBF848EFFB973
          80FFB9737FFFB9727FFF9B4457FF000000000000000000000000000000000000
          000000000000AE6A7AFFC38D95FFBA7682FFBA7481FFB97480FFAA6472FFE5E4
          E7FFE9E9EBFF9A5D69FFB8737FFFB06B78FFE5E5E7FFE2E0E3FFDDDBDDFFD8D5
          D7FFD3D0D1FFCDCACBFFDDDADBFF8B4857FF9D5363FF9F5464FFBF8590FFBA74
          80FFBA7481FFBA7481FF9B4558FF000000000000000000000000000000000000
          000000000000AE6A7AFFC58F96FFBA7783FFBA7682FFB97481FFAA6472FFE0DF
          E1FFE4E2E6FF9B5F6BFFB97580FFB06B78FFE9EAECFFE7E5E8FFE2E1E4FFDDDC
          DEFFD9D6D8FFD4D0D2FFE0DFDFFF894554FF9B5061FF9D5363FFBF8590FFBA74
          81FFBA7481FFBA7481FF9A4759FF000000000000000000000000000000000000
          000000000000AE6A7AFFC58F96FFBB7784FFBA7784FFBA7582FFAA6472FFDAD9
          DBFFE0DEE0FF9B5F6BFFBA7582FFB06B78FFEDECEFFFEAE9EDFFE6E6E9FFE2E1
          E4FFDFDDDFFFDAD8DAFFE5E2E4FF864252FF994E5FFF9B5061FFC18691FFB974
          80FFBA7481FFBA7481FF9C4A5CFF000000000000000000000000000000000000
          000000000000AE6A7AFFC58F98FFBC7886FFBB7785FFBB7783FFAA6472FFD5D2
          D4FFDBD8DAFF9B5F6BFF925864FF925864FFEAEAEDFFEDEDF0FFEAEAEDFFE8E7
          E9FFE4E2E6FFDFDDDFFFE8E7E9FF864252FF974C5DFF994E5FFFC18691FFB972
          7FFFB97380FFB97380FF9C4A5CFF000000000000000000000000000000000000
          000000000000AE6A7AFFC49098FFBC7986FFBD7985FFBC7784FFAA6472FFCFCC
          CCFFD4D1D3FFD9D8D9FFDFDCDFFFE2E1E4FFE7E5E8FFEAEAEDFFEDECEFFFEAEB
          EEFFE8E7EAFFE4E2E6FFECEAECFF864252FF864252FF864252FFC28791FFB772
          7FFFB8727EFFB9727FFF9D4E5EFF000000000000000000000000000000000000
          000000000000AE6A7AFFC79198FFBD7A87FFBD7987FFBC7985FFBB7785FFAA64
          72FFAA6472FFAA6472FFAA6472FFAA6472FFAA6472FFAA6472FFAA6472FFAA64
          72FFAA6472FFAA6472FFAA6472FFAA6472FFAA6472FFB77A84FFB76F7DFFB770
          7DFFB7707EFFB8717EFF9D4E5EFF000000000000000000000000000000000000
          000000000000AE6A7AFFC6929AFFBE7B88FFBE7B88FFBC7986FFBC7886FFBB77
          85FFBB7784FFBA7682FFB97481FFB97480FFB97380FFB8717EFFB8717EFFB76F
          7DFFB76D7CFFB76D7BFFB76D7BFFB66D7BFFB56C7AFFB56C7AFFB66D7BFFB76F
          7DFFB76F7DFFB7707DFF9F4F60FF000000000000000000000000000000000000
          000000000000AE6A7AFFC6929AFFBE7B88FFBE7B88FFBC7986FFBC7886FFBB77
          85FFBB7784FFBA7682FFB97481FFB97480FFB97380FFB8717EFFB76F7DFFB76D
          7CFFB76D7CFFB76C7AFFB66C7AFFB66D7BFFB56C7AFFB56C7AFFB56C7AFFB66C
          7AFFB66C7AFFB76D7CFF9F4F60FF000000000000000000000000000000000000
          000000000000AE6A7AFFC6929AFFBF7F8AFFBC7884FFA75E70FFA75E70FFA75E
          70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E
          70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E
          70FFB66B7AFFB66C7BFF9F5162FF000000000000000000000000000000000000
          000000000000AE6A7AFFC7949CFFBF7F8AFFAF6F7BFFEBE1E1FFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFEFDFFFDFEFDFFFDFDFCFFFCFC
          FCFFFBFCFBFFFBFBFBFFFAFAFAFFF9F9F9FFF9F9F9FFF8F8F8FFE8D1D1FFA75E
          70FFB76D7BFFB66C7AFF9F5262FF000000000000000000000000000000000000
          000000000000AE6A7AFFC9959DFFC0808BFFAF6F7BFFFEFFFEFFFCFCFBFFFBFB
          FAFFFAFBFAFFFAFAFAFFF9FAF9FFF9F9F9FFF9F9F9FFF9F9F8FFF8F8F8FFF8F8
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F6F6FFF7F6F6FFF9F9F9FFA75E
          70FFB96F7DFFB66B7AFFA05363FF000000000000000000000000000000000000
          000000000000AE6A7AFFC9959DFFC1818DFFAF6F7BFFFEFFFEFFE1E1E1FFDFDF
          DFFFDDDDDDFFDCDCDCFFDADAD9FFD9D9D9FFD7D7D6FFD5D6D5FFD4D4D3FFD2D3
          D2FFD1D1D1FFD0D0D0FFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFF9FAF9FFA75E
          70FFBA707EFFB66B79FFA15466FF000000000000000000000000000000000000
          000000000000AE6A7AFFC9969EFFC1818DFFAF6F7BFFFEFFFEFFFCFCFCFFFCFC
          FBFFFBFBFBFFFBFBFAFFFBFAFAFFFAFAFAFFF9F9F9FFF9F9F9FFF9F9F9FFF9F9
          F8FFF8F8F8FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F6FFFBFBFBFFA75E
          70FFB97480FFB56B79FFA25666FF000000000000000000000000000000000000
          000000000000AE6A7AFFC9989FFFC2828EFFAF6F7BFFFEFFFEFFE4E4E4FFE3E3
          E3FFE2E2E1FFE0E0E0FFDEDDDEFFDCDCDCFFDADADAFFD9D9D9FFD7D8D8FFD6D5
          D5FFD4D4D4FFD2D3D3FFD1D2D1FFD0D0D0FFCFCFCFFFCFCFCFFFFCFCFBFFA75E
          70FFBA7581FFB56B79FFA55969FF000000000000000000000000000000000000
          000000000000AE6A7AFFC9989FFFC2838EFFAF6F7BFFFEFFFEFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFBFFFBFBFBFFFBFBFAFFFAFAFAFFFAFAF9FFF9F9F9FFF9F9
          F9FFF9F9F8FFF8F9F8FFF8F8F8FFF8F7F7FFF7F7F7FFF7F7F7FFFCFCFCFFA75E
          70FFBA7783FFB66C7AFFA55969FF000000000000000000000000000000000000
          000000000000AE6A7AFFCB98A0FFC2838EFFAF6F7BFFFEFFFEFFE6E6E6FFE6E5
          E6FFE4E4E4FFE3E3E3FFE1E2E2FFE0E0E0FFDEDEDEFFDCDCDCFFDBDBDBFFD9D9
          D9FFD8D8D8FFD6D6D6FFD4D5D5FFD3D3D3FFD1D1D1FFD0D0D0FFFDFDFCFFA75E
          70FFBC7783FFB76D7BFFA55D6CFF000000000000000000000000000000000000
          000000000000AE6A7AFFCB98A0FFC2838EFFAF6F7BFFFEFFFEFFFDFDFDFFFDFD
          FCFFFCFDFCFFFCFCFCFFFCFCFBFFFBFCFBFFFBFBFBFFFBFBFAFFFAFAFAFFFAFA
          F9FFF9F9F9FFF9F9F9FFF9F9F9FFF8F8F8FFF8F8F8FFF7F7F7FFFDFEFDFFA75E
          70FFBD7985FFB76F7DFFA65F6EFF000000000000000000000000000000000000
          000000000000AE6A7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFFE6E6E6FFE6E6
          E6FFE6E6E6FFE5E5E6FFE4E5E5FFE4E3E3FFE2E2E2FFE1E0E0FFDFDFDFFFDDDD
          DDFFDBDBDCFFDADAD9FFD9D8D9FFD7D6D7FFD5D5D5FFD3D4D3FFFEFEFEFFA75E
          70FFBE7A86FFB7707DFFA7606EFF000000000000000000000000000000000000
          000000000000AE6A7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFFFEFEFDFFFEFE
          FDFFFDFDFCFFFDFDFCFFFCFDFCFFFCFCFCFFFCFCFBFFFCFCFBFFFBFBFAFFFBFB
          FAFFFAFAF9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F8FFF8F8F8FFFEFFFEFFA75E
          70FFB38088FFB8717FFFA7606EFF000000000000000000000000000000000000
          000000000000AE6A7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFFE6E6E6FFE6E6
          E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFE5E5E5FFE4E4E3FFE2E3E3FFE1E1
          E1FFE0DFDFFFDEDDDEFFDCDBDCFFDADADAFFD9D9D9FFD7D8D7FFFEFFFEFFA75E
          70FF996E75FF9E5A68FFA9606EFF000000000000000000000000000000000000
          000000000000AE6A7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFFFEFFFEFFFEFE
          FEFFFEFEFDFFFEFEFDFFFDFDFCFFFDFDFCFFFCFDFCFFFCFCFCFFFCFCFBFFFBFB
          FBFFFBFBFAFFFAFAFAFFFAFAF9FFFAFAF9FFF9F9F9FFF9F9F9FFFEFFFEFFA75E
          70FFB0757FFF9E5A68FFAA6471FF000000000000000000000000000000000000
          000000000000AE6A7AFFCA969EFFC2838EFFAF6F7BFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFA75E
          70FFC77F8BFFC77F8BFFAA6472FF000000000000000000000000000000000000
          000000000000AE6A7AFFAE6A7AFFAE6A7AFFAE6A7AFFD9D6D6FFD9D6D6FFD9D6
          D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6
          D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFA75E
          70FFA75E70FFA75E70FFA75E70FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000007F7F86FF76767DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000076767DFF30307AFF30307AFF63637AFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000797A
          7BFF878997FF5E5FAEFF5C5DCBFF6667D5FF878997FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000676B67FF7F8C
          81FFD5D5D5FFD3D5D6FF898AD9FF6B6BAAFFB3B3B4FF00000000000000000000
          0000C7ADAEFFA55B6CFFA96270FFA55E6CFF974658FFA4A2A2FFA4A2A2FFA19C
          9CFF9C9797FF979393FF948D8DFF8E8888FF8A8383FF867F7DFF827A79FF827A
          79FF974658FF974658FF974658FF8C4151FF803C4BFF583A48FF5A8252FF275E
          19FF929D90FFCACACDFF9D9EA3FF9D9EA3FF000000000000000000000000D7C6
          C6FFB38185FFAF6D79FFB8717EFFB7707DFFAA6472FFEEEEF1FFEEEEF1FF995E
          69FFB8717EFFB06B78FFE0DFE1FFDDDADBFFD8D5D6FFD4D1D3FFCFCCCCFFDBD8
          D8FF914C5BFFA05464FF914C5BFF854654FF624A58FF5DA4CDFFB9CFD7FF6C92
          60FF275E19FF808B83FFA4A2A2FF00000000000000000000000000000000AE6A
          7AFFAF6D79FFBA7481FFB97380FFB7737FFFAA6472FFE9E9EBFFECEBEFFF995E
          69FFB8717EFFB06B78FFE0DFE1FFDDDBDDFFD7D5D6FFD2D0D2FFCDC9CAFFD8D5
          D6FF8B4857FF914C5BFF854654FF61525FFF5AA0C9FFA4EBFFFFAAEEFFFFB0C3
          CAFF638064FF9C9797FF0000000000000000000000000000000000000000AE6A
          7AFFC48E96FFBA7682FFBA7481FFB97480FFAA6472FFE5E4E7FFE9E9EBFF995E
          69FFB7737FFFB06B78FFE6E5E8FFE2E1E2FFDDDBDDFFD8D5D6FFD2D0D2FFDAD7
          D9FF7E424FFF854654FF533D4CFF559AC2FF9EE8FFFFA4EBFFFFA5F4FFFF5CA7
          C8FF8B9093FF000000000000000000000000000000000000000000000000AE6A
          7AFFC48E96FFBB7784FFBA7682FFBA7481FFAA6472FFE0DFE1FFE5E3E6FF9B5F
          6BFFB97480FFB06B78FFEAEAEDFFE6E5E8FFE2E1E4FFDDDCDEFFD6D3D5FFCBCB
          CBFF723A46FF4A3444FF5194BEFF99E4FCFF9EE8FFFFA5F4FFFF5AADCFFF5B35
          44FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC48E96FFBB7784FFBB7784FFBA7582FFAA6472FFDADADBFFDEDEE0FF9B5F
          6BFFBA7582FFB06B78FFEDECEFFFEAEAEDFFE6E5E8FFDEDEE0FFC9C8CAFFBEBC
          BEFF4A3444FF4C8BB3FF95E2FDFF96E4FFFF9FF0FFFF56ADCFFF6A4E5BFF9746
          58FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC48E96FFBC7986FFBB7784FFBC7783FFAA6472FFD6D3D5FFDBD8DAFF9B5F
          6BFF925864FF925864FFEAEAEDFFEDECEFFFE6E5E8FFD4D1D3FFBEBCBEFF7682
          8BFF4886AEFF90E0FAFF91E3FFFF9AEFFFFF56ADCFFF6A4E5BFFB5707DFF9C4D
          5DFF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC69199FFBC7986FFBC7986FFBC7784FFAA6472FFCFCCCCFFD4D1D3FFDAD7
          D9FFDFDCDFFFE2E1E4FFE6E5E8FFE6E5E8FFD7D6D9FFC3C3C5FF76828BFF4886
          AEFF8DDCF9FF8ADFFFFF93EAFFFF52ACD1FF6A4E5BFFB46F7BFFB97380FF9C4D
          5DFF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC69199FFBE7A87FFBC7986FFBC7986FFBB7784FFAA6472FFAA6472FFAA64
          72FFAA6472FFAA6472FFAA6472FF9A5B67FF8D535EFF533D4CFF437DA6FF88DA
          F8FF85DDFFFF93EAFFFF4CADD2FF624A58FFB26E7AFFB7707EFFB8717EFF9F4E
          60FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC69199FFBE7C88FFBE7A87FFBC7986FFBC7986FFBB7784FFBB7784FFBA76
          82FFBA7481FFB5727DFFB26E7AFF995E69FF5A4453FF3E739BFF84D7F7FF7CD9
          FFFF8BE5FFFF4CADD2FF624A58FFB16977FFB76E7CFFB76E7CFFB7707DFF9F4E
          60FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC69199FFBE7C88FFBE7A87FFBC7986FFBC7986FFBB7784FFBB7784FFBA76
          82FFB7737FFFA76973FF995E69FF6A4E5BFF3E739BFF7FD3F4FF7CD9FFFF8BE5
          FFFF44ADD1FF624A58FFB16977FFB56C7AFFB66C7AFFB66C7AFFB76E7CFF9F4E
          60FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC7949CFFBE7C88FFBC7884FFA75E70FFA75E70FFA75E70FFA75E70FFA75E
          70FF9B5768FF8B4F5EFF634152FF3E739BFF81C2E1FF74D4FFFF83E1FFFF44AD
          D1FF5A4453FFA25B6DFFA75E70FFA75E70FFA75E70FFB66B7AFFB66C7AFF9F52
          62FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC7949CFFC0808BFFAF6F7BFFEBE1E1FFFEFFFEFFFEFFFEFFFEFFFEFFF7F7
          F6FFDDDEDDFFAEB0B0FF7599BDFFB7E0FBFF9FD0F4FF91E3FFFF40B1D5FF8394
          9AFFF3F3F3FFF9F9F9FFF9F9F9FFE8D1D1FFA75E70FFB76E7CFFB66C7AFF9F52
          62FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC9969EFFC0808BFFAF6F7BFFFEFFFEFFFCFCFBFFFBFBFAFFFAFBFAFFE2E1
          E2FFCBCBCBFF748597FFB2DBFEFFC9E7FFFFBADCFFFF52ACD1FF83949AFFF1F1
          F1FFF7F7F7FFF7F7F7FFF7F7F6FFF9F9F9FFA75E70FFB96F7DFFB66B7AFFA054
          64FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC9969EFFC0808BFFAF6F7BFFFEFFFEFFFCFCFCFFFCFCFBFFFBFBFBFFD6D6
          D5FF8D9297FF8FBBE2FFC1E2FFFF9FCBF7FF6289AEFF8D9297FFF1F1F1FFF8F8
          F8FFF7F7F7FFF7F7F7FFF7F7F7FFFBFBFBFFA75E70FFB97480FFB66B7AFFA054
          64FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC9989FFFC2838EFFAF6F7BFFFEFFFEFFDEDEDEFFDEDEDEFFDDDDDDFF9A9B
          9BFF6F8DACFFAED8FFFF749FCCFF566779FF9A9B9BFFD5D5D5FFD5D5D5FFD5D5
          D5FFD5D5D5FFD4D4D4FFD4D4D4FFFCFCFBFFA75E70FFBA7582FFB66B7AFFA559
          69FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC9989FFFC2838EFFAF6F7BFFFEFFFEFFFCFCFCFFFCFCFCFFFCFCFCFF797A
          7BFF7599BDFF607D99FFABAEB0FFF3F3F3FFF9F9F9FFF9F9F9FFF9F9F8FFF8F9
          F8FFF8F8F8FFF7F7F7FFF7F7F7FFFCFCFCFFA75E70FFBB7784FFB66C7AFFA559
          69FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFCB98A0FFC2838EFFAF6F7BFFFEFFFEFF26BC26FF26BC26FF26BC26FF3635
          34FF838485FFC8C9C8FFDADADBFFDADADBFFDADADBFFD8D8D8FFD8D8D8FFD7D7
          D7FFD5D6D6FFD5D5D5FFD4D4D4FFFDFDFCFFA75E70FFBC7783FFB76E7CFFA55E
          6CFF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFCB98A0FFC2838EFFAF6F7BFFFEFFFEFFFDFDFDFFFDFDFCFFFCFDFCFFFCFC
          FCFFFCFCFBFFFBFCFBFFFBFBFBFFFBFBFAFFFAFAFAFFFAFAF9FFF9F9F9FFF9F9
          F9FFF9F9F9FFF8F8F8FFF8F8F8FFFDFEFDFFA75E70FFBC7986FFB7707DFFA760
          6EFF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFF26BC26FF26BC26FF26BC26FF26BC
          26FF26BC26FF26BC26FF26BC26FF26BC26FF26BC26FF26BC26FF26BC26FF26BC
          26FF26BC26FF26BC26FF26BC26FFFEFEFEFFA75E70FFBE7A86FFB7707DFFA760
          6EFF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFFFEFEFDFFFEFEFDFFFDFDFCFFFDFD
          FCFFFCFDFCFFFCFCFCFFFCFCFBFFFCFCFBFFFBFBFAFFFBFBFAFFFAFAF9FFF9F9
          F9FFF9F9F9FFF9F9F9FFF9F9F8FFFEFFFEFFA75E70FFB38185FFB8717EFFA760
          6EFF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFFCBCBCBFFCBCBCBFFCBCBCBFFCBCB
          CBFFCBCBCBFFCBCBCBFFCACACAFFC8C9C8FFC8C9C8FFC7C6C6FFC7C6C6FFC3C3
          C5FFC1C1C1FFC1C1C1FFBFBFBFFFFEFFFEFFA75E70FF996E75FF9E5A68FFA760
          6EFF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFCB9BA1FFC2838EFFAF6F7BFFFEFFFEFFFEFFFEFFFEFEFEFFFEFEFDFFFEFE
          FDFFFDFDFCFFFDFDFCFFFCFDFCFFFCFCFCFFFCFCFBFFFBFBFBFFFBFBFAFFFAFA
          FAFFFAFAF9FFFAFAF9FFF9F9F9FFFEFFFEFFA75E70FFB0757FFF9E5A68FFAA64
          72FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFC9969EFFC2838EFFAF6F7BFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFA75E70FFC77F8BFFC77F8BFFAA64
          72FF00000000000000000000000000000000000000000000000000000000AE6A
          7AFFAE6A7AFFAE6A7AFFAE6A7AFFBFBDBDFFBFBDBDFFBFBDBDFFBFBDBDFFBFBD
          BDFFBFBDBDFFBFBDBDFFBFBDBDFFBFBDBDFFBFBDBDFFBFBDBDFFBFBDBDFFBFBD
          BDFFBFBDBDFFBFBDBDFFBFBDBDFFBFBDBDFFA75E70FFA75E70FFA75E70FFA75E
          70FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000070000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C000000150000000700000000BEAD
          A0FFAD9D8EFF988575FF8B7966FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000001C0000000900000000BEAD
          A0FFFAF2EFFFF9F1EEFFF9F1EDFFF9F1EDFFF9F0ECFFF9EFECFFF9F0EBFFF9EF
          EBFFF8EFEAFFF8EEEAFFF8EEEAFFF8EDE9FFF7EEE9FFC0AC9EFFFCF8F6FFFCF8
          F5FFFCF7F5FFFCF6F4FFFBF6F4FFFBF6F3FFFBF6F3FFFBF5F2FFFAF5F2FFFBF4
          F1FFFAF4F0FFFAF3F0FFFAF3F0FF806D59FF0000001C0000000900000000BEAD
          A0FFFAF3EEFFFAF2EEFFF9F1EEFFF9F4F0FFFBF4F3FFFCF6F3FFFBF6F3FFFAF3
          F1FFF8F2EEFFF8EFEBFFF8EEEAFFF7EEEAFFF8EDE9FFC0AD9EFFFCF8F7FFFCF8
          F7FFFCF8F6FFFCF7F5FFFCF7F4FFFBF6F4FFFCF6F3FFFBF5F3FFFBF5F2FFFAF4
          F2FFFBF4F1FFFAF3F0FFFAF3F0FF806D59FF0000001C0000000900000000BEAD
          A0FFF9F3EFFFFAF2EEFFFAF5F1FFFCF9F7FFFEFBFBFFFEFDFCFFFEFDFCFFFDFB
          FAFFFBF6F4FFF8F2EEFFF8EEEAFFF8EFEAFFF8EDE9FFC0AE9FFFFCF8F8FFFCF8
          F6FFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFBF4F1FFFAF3F0FF806D59FF0000001C0000000900000000BEAD
          A0FFF9F3EFFFFAF2F0FFDFD9D3FFC9B5A7FF834F30FF834F30FF834F30FF834F
          30FFF3F0EDFFDBD5CDFFCDC3BAFFF8EFEAFFF8EDE9FFC0AE9FFFFCF8F8FFFCF8
          F6FFFCF8F6FFFCF7F5FFFCF7F5FFFCF6F5FFFBF6F3FFFBF5F3FFFAF6F3FFFBF5
          F2FFFAF4F1FFFBF4F1FFFAF3F0FF806D59FF0000001C0000000900000000BEAD
          A0FFFAF3F0FFFAF3EFFFFBF5F3FFFDFCFAFFB1907DFF9A6F56FFB1917DFF9264
          49FFDFD1C9FFFBF6F3FFF9F0EEFFF9EEEAFFF8EEEAFFC1AEA0FFFDFAF8FFFCF9
          F7FFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFAF4F1FFFAF3F1FF806D59FF0000001C0000000900000000BEAD
          A0FFFAF4F1FFFAF3EFFFD5CBC3FFE6E1DBFFF8F5F4FFA98570FFD0BCB0FFF4F1
          EEFFD7C8BEFFDCD6D0FFD0C5BAFFF8EFEAFFF8EEEAFFC2AEA0FFFCFAF8FFFCF9
          F8FFFCF9F7FFFCF8F7FFFCF8F6FFFCF7F5FFFCF7F5FFFBF7F4FFFBF6F4FFFCF6
          F3FFFBF5F2FFFBF5F2FFFAF4F2FF806D59FF0000001C0000000900000000BEAD
          A0FFFAF4F1FFFAF3F0FFFAF3F0FFFBF6F4FFFEFBFAFFF0E8E5FF835031FFD8C7
          BDFFFDFBFAFFFBF4F2FFF8F0ECFFF9EFEBFFF8EFEBFFC2AFA1FFFDFAF8FFFDF9
          F8FFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFBF5F3FFFAF4F2FF806D59FF0000001C0000000900000000BEAD
          A0FFFAF4F1FFFAF3F0FFD6CDC4FFDDD5CEFFECE7E3FFFCFCFBFF9B7259FF9265
          4AFFF4F1EFFFDAD3CCFFCDC2B7FFF8EFECFFF8EFEAFFC8B5A7FFFDFAF9FFFCFA
          F9FFFDF9F8FFFDF9F7FFFCF8F7FFFCF8F6FFFCF8F6FFFCF7F6FFFBF6F5FFFCF7
          F4FFFBF6F4FFFBF5F3FFFBF5F3FF806D59FF0000001C0000000900000000BEAD
          A0FFFBF4F2FFFBF4F2FFFAF6F3FFEFE6E2FFAF8D79FFD3C1B6FF8F6145FF9569
          4EFFFEFCFBFFFBF4F2FFF9F0ECFFF9F0ECFFF8EFEBFFC8B5A7FFFDFBFAFFFDFA
          F9FFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFBF6F4FFFBF5F3FF806D59FF0000001C0000000900000000BEAD
          A0FFFBF4F2FFFAF4F2FFD7CEC6FFE9E3DFFFC9B4A7FF94664CFF8B5B3EFFC9B3
          A6FFECE8E4FFD4CCC4FFCAC0B7FFF8F0ECFFF9F0EBFFC8B5A7FFFEFBFAFFFDFB
          F9FFFDFBF9FFFDFAF9FFFCFAF7FFFDF9F7FFFCF8F7FFFCF8F7FFFCF8F6FFFCF7
          F5FFFBF7F5FFFCF7F4FFFBF6F3FF806D59FF0000001C0000000900000000BEAD
          A0FFFBF6F3FFFBF5F2FFFAF5F2FFFBF7F5FFFDFBFAFFF7F3F0FFF7F2F0FFFDFB
          F9FFFCF6F4FFF9F2F0FFF9F1EDFFF9F0EDFFF9F0ECFFC8B5A7FFFEFBFBFFFDFB
          FAFFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFCF7F4FFFBF6F4FF806D59FF0000001C0000000900000000BEAD
          A0FFFBF6F3FFFBF5F2FFFBF4F2FFFAF5F1FFFBF7F4FFFBF7F5FFFCF7F5FFFAF5
          F2FFFAF2F0FFFAF2EEFFFAF1EDFFF9F1EDFFF8F1EDFFC8B5A7FFFEFCFBFFFDFC
          FAFFFDFBFAFFFDFAF9FFFDFAF9FFFDF9F8FFFCF9F7FFFCF8F7FFFCF8F7FFFCF8
          F6FFFCF7F6FFFBF7F5FFFCF7F4FF806D59FF0000001C0000000900000000BEAD
          A0FFC9B7A8FFC9B7A8FFC8B5A7FFC7B7AAFFC9B7A9FFCCBAAEFFCCB9ADFFC7B6
          A7FFC4B5A6FFC4B1A2FFC3B0A2FFC2AFA1FFC2AFA0FFC9B8A9FFC9B8A9FFC9B8
          A9FFC9B8A9FFC9B8A9FFC9B8A9FFC9B8A9FFC9B8A9FFC9B8A9FFC9B8A9FFC9B8
          A9FFC9B8A9FFC9B8A9FFC9B8A9FF806D59FF0000001C0000000900000000BEAD
          A0FFFBF7F4FFFBF6F4FFFBF5F3FFFBF5F3FFFAF8F5FFFBF8F6FFFBF7F5FFFAF6
          F3FFFAF3F0FFFAF2EFFFF9F2EFFFF9F1EEFFF9F1EDFFC8B5A7FFFEFCFCFFFEFC
          FBFFFEFBFBFFFDFBFAFFFEFBF9FFFDFAF9FFFDFAF9FFFDF9F9FFFDFAF8FFFCF9
          F7FFFDF9F6FFFCF8F6FFFCF7F6FF806D59FF0000001C0000000900000000BEAD
          A0FFFCF7F4FFFBF6F4FFFBF6F3FFFBF8F6FFFEFBFAFFFEFDFCFFFEFDFCFFFDFB
          F9FFFBF6F5FFF9F3F0FFF9F2EFFFFAF2EFFFFAF2EEFFC8B5A7FFFEFDFCFFFDFC
          FBFFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFCF8F6FFFCF8F6FF806D59FF0000001C0000000900000000BEAD
          A0FFFCF7F5FFFBF7F5FFFCF7F5FFFDFAF9FFB1907CFF834F30FF834F30FFA985
          70FFFDFAF8FFFAF5F3FFFAF2F0FFF9F3EFFFF9F2EFFFC8B5A7FFFEFDFCFFFEFD
          FCFFFEFDFBFFFEFBFBFFFEFCFBFFFDFBFAFFFDFAF9FFFDFAFAFFFDF9F8FFFCF9
          F9FFFCF9F7FFFCF9F7FFFCF8F6FF806D59FF0000001C0000000900000000BEAD
          A0FFFCF7F6FFFBF7F5FFD2C8BEFFE3DDD8FFF9F8F7FF9A6F56FF834F30FFFBFA
          F9FFE6DFDAFFD0C6BEFFCABFB4FFF9F2F0FFFAF3EFFFC8B5A7FFFEFDFDFFFEFD
          FCFFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFDF9F8FFFCF9F7FF806D59FF0000001C0000000900000000BFAE
          A1FFFCF8F6FFFCF8F6FFFBF7F5FFFCFAF8FFFEFDFDFFA27A63FF834F30FFFFFE
          FEFFFCF9F7FFFAF4F1FFFBF3F0FFFAF3F0FFFAF2F0FFC8B5A8FFFFFDFEFFFEFD
          FDFFFFFDFCFFFEFCFCFFFEFCFCFFFDFCFBFFFEFCFBFFFDFBFAFFFDFAF9FFFDFB
          F9FFFDF9F9FFFDFAF8FFFDF9F7FF806D59FF0000001C0000000900000000C1B0
          A2FFFCF8F7FFFDF8F6FFD0C5BEFFDED7CFFFF7F5F4FFA27C64FF834F30FFF8F7
          F5FFDFD8D1FFCDC3B8FFCABFB4FFFAF4F0FFFAF3F0FFC8B6A8FFFFFEFDFFFEFD
          FDFFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFCF9F8FFFCF9F8FF806D59FF0000001C0000000900000000C2B0
          A3FFFDF9F8FFFCF9F7FFFCF8F6FFFCFAF9FFFEFEFDFFA47E68FF845132FFFEFD
          FDFFFCF8F7FFFAF5F2FFFAF4F2FFFAF4F1FFFAF3F0FFC9B7A9FFFEFEFEFFFFFE
          FEFFFEFEFDFFFEFDFDFFFEFDFCFFFDFCFCFFFEFCFCFFFDFCFBFFFEFBFAFFFDFB
          FAFFFDFBF9FFFDFAF9FFFDFAF8FF806D59FF0000001C0000000900000000C2B2
          A5FFFCF9F8FFFDF9F7FFD3C8C1FFE5E0DAFFD0BDB1FF9F775FFF875536FFF7F5
          F4FFDDD6D0FFCDC3B8FFCABFB4FFFBF4F2FFFAF4F1FFC9B8A9FFFEFFFEFFFFFE
          FEFFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFDFBF9FFFDFAF9FF806D59FF0000001C0000000900000000C3B2
          A5FFFCFAF9FFFDFAF7FFFCF9F7FFFDFBFAFFDACBC1FFA8846EFF89583BFFFEFC
          FCFFFCF9F7FFFCF6F4FFFBF5F3FFFBF5F2FFFBF4F2FFCAB8A9FFFEFEFFFFFFFE
          FEFFFFFEFEFFFFFEFDFFFFFDFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFBFFFDFC
          FBFFFDFCFAFFFDFBFAFFFDFAF9FF806D59FF0000001C0000000900000000C5B3
          A6FFFDFAF9FFFCFAF9FFD0C5BBFFD9D0C8FFE8E2DDFFF2EEECFFE4DBD5FFE4DE
          D8FFD4C9C1FFCAC0B8FFCABFB4FFFBF5F2FFFAF5F2FFCAB9AAFFFFFFFFFFFFFE
          FEFFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0B5FFCAC0
          B5FFCABFB4FFFEFCFAFFFEFBFAFF8B7966FF0000001C0000000900000000C5B4
          A6FFFDFBF9FFFDFAF9FFFDF9F8FFFCF9F7FFFDF9F8FFFCFBF9FFFCFAF9FFFBF8
          F8FFFBF7F5FFFCF6F5FFFCF6F3FFFBF6F3FFFAF5F2FFCBB9AAFFFFFEFFFFFFFF
          FEFFFFFFFEFFFEFEFEFFFEFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFFEFCFCFFFEFC
          FBFFFEFBFBFFFEFCFBFFFEFBFAFF988575FF0000001C0000000900000000C5B4
          A7FFFEFAFAFFFDFAF9FFFDFAF9FFFDF9F8FFFDF9F8FFFCF9F7FFFDF8F7FFFCF8
          F6FFFCF8F5FFFCF6F5FFFBF7F4FFFCF6F4FFFBF5F4FFCBBAABFFFFFFFFFFFFFE
          FFFFFFFFFEFFFFFEFEFFFFFFFEFFFFFEFDFFFEFEFEFFFEFDFDFFFEFDFDFFFEFD
          FCFFFEFCFCFFFDFCFBFFFEFCFBFFAD9D8EFF000000150000000700000000C6B5
          A8FFC5B4A7FFC4B4A7FFC4B3A7FFC4B3A6FFC3B1A4FFC2B1A4FFC1B0A3FFC0AF
          A1FFBFAEA1FFBEAC9FFFBDAB9EFFBCAA9DFFBBA99CFFBAA89AFFB9A89AFFB8A7
          98FFB8A598FFB7A597FFB6A396FFB5A295FFB4A194FFB3A193FFB29F92FFB29E
          90FFB09E90FFB09C8FFFAF9C8EFFAF9C8EFF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          00000000000000000000000000160000009D000000B3000000B3000000B30000
          00B3000000B3000000B3000000B3000000B3000000B3000000B3000000B30000
          00B3000000B3000000B3000000B3000000B3000000B3000000B3000000B30000
          00B3000000B3000000B30000009D000000160000000000000000000000000000
          00000000000000000000AF8E92FFC2A9ABFFBEA6A8FFBCA3A7FFBAA2A4FFB8A0
          A3FFB59EA2FFB49DA1FFB29C9FFFB09B9EFFAF9A9CFFAD999BFFAC979AFFA996
          99FFA99597FFA69496FFA59395FFA39294FFA28F93FFA18E91FF9F8D90FF9F8E
          90FFA18F8AFF75615BFF0000009D000000160000000000000000000000000000
          00000000000000000000C1AEB2FFEFEDECFFF7EEE8FFF5EDE6FFF5ECE5FFF5EC
          E6FFF5EBE5FFF5EBE4FFF4EBE4FFF4EAE3FFF5EBE2FFF5EAE1FFF4EAE0FFF4E9
          E1FFF4E9E0FFF4E8DFFFF5E8DEFFF4E8DDFFF4E7DDFFF4E7DCFFF4E6DBFFF6E8
          DBFFF9EBDEFFA18F8AFF0000009D000000160000000000000000000000000000
          00000000000000000000C0ADB2FFEEF0ECFFFAF3E8FFF8F1E5FFF8F0E4FFF7F0
          E3FFF7EDE1FFF7ECDEFFF6EDDDFFF6EBDCFFF5EADAFFF5E9D8FFF6E8D7FFF3E7
          D5FFF3E7D4FFF3E6D2FFF2E4D1FFF2E4CFFFF1E3CDFFF1E3CCFFF2E1CBFFF3E2
          C9FFF6E8DBFF9F8E90FF0000009D000000160000000000000000000000000000
          00000000000000000000C2AEB3FFEEEFECFFFAF3E8FFF7F0E6FFF7EFE4FFF7EE
          E3FFF6EEE2FFF6ECE0FFF6EBDDFFF5EBDDFFF5EADCFFF4E9DAFFF4E8D8FFF4E7
          D7FFF2E6D4FFF2E6D4FFF2E5D1FFF1E2D1FFF1E3CEFFF0E2CCFFF0E2CBFFF2E1
          CBFFF4E6DBFF9F8D90FF0000009D000000160000000000000000000000000000
          00000000000000000000C4B0B5FFEEF0EEFFFAF3EAFFF8F1E7FFF7F0E6FFF7EF
          E4FFF7EEE3FFF6EEE2FFF6ECE0FFF6EBDDFFF5EBDDFFF5EADCFFF4E9DAFFF4E8
          D7FFF4E7D7FFF1E6D5FFF2E6D4FFF2E5D2FFF1E2D1FFF1E3CEFFF0E2CCFFF1E3
          CCFFF4E7DCFFA18E91FF0000009D000000160000000000000000000000000000
          00000000000000000000C6B3B6FFF0F1EFFFF9F3ECFFF8F2E9FFF9F1E7FFF9F2
          E8FFF9F1E6FFF9EFE4FFF7EFE3FFF6ECE0FFF6EBDEFFF5EBDDFFF5EADCFFF4E9
          DAFFF4E8D8FFF4E7D7FFF1E6D5FFF2E6D4FFF2E5D2FFF1E2D1FFF1E3CEFFF1E3
          CDFFF4E7DDFFA28F93FF0000009D000000160000000000000000000000000000
          00000000000000000000C8B5B8FFF1F2F0FFFCF6EDFFF9F2ECFFFBF5ECFFFFF8
          EEFFFFF8EEFFFEF6EAFFFDF3E8FFFAF2E5FFF9EFE3FFF8EDE0FFF6ECDEFFF6EA
          DCFFF4E9DAFFF4E8D8FFF4E7D7FFF2E6D5FFF2E6D4FFF2E5D2FFF1E3D1FFF2E4
          CFFFF4E8DDFFA39294FF0000009D000000160000000000000000000000000000
          00000000000000000000C9B7B9FFF1F3F1FFFCF6EEFFFDF6EEFFFFFAF3FF4A49
          47FF605F5BFF99968FFFD5D0C7FFFFFAEEFFFFF9ECFFFFF5E8FFFDF1E4FFFAF0
          E1FFF8EDDFFFF7ECDCFFF6EADAFFF5E8D8FFF2E6D5FFF2E6D4FFF2E5D2FFF2E4
          D1FFF5E8DEFFA59395FF0000009D000000160000000000000000000000000000
          00000000000000000000CBB9BBFFF2F2F3FFFCF7F0FFFDF7F0FFF5EFE7FF6263
          64FFDADBDCFFC6C7C8FF4D4D4FFF1B1E1FFF3C3C39FF605D59FFA9A29AFFE4DB
          CFFFFFF6E7FFFFF4E5FFFDF1E1FFFAEEDDFFF9EBDBFFF5E9D8FFF5E9D6FFF5E8
          D4FFF5E8DFFFA69496FF0000009D000000160000000000000000000000000000
          00000000000000000000CDBBBCFFF3F3F4FFFDF8F3FFFEF9F3FFC0BDB9FF9091
          8EFFACADADFFB2B3B5FFC5C6C8FFD9DADCFFFFFFFFFFB2B3B5FF777879FF7678
          79FF2A2B2CFF3B3A37FF726E67FFA9A298FFE2D7C8FFFFF2E0FFFCEFDDFFFAEE
          DAFFF7ECE2FFA99599FF0000009D000000160000000000000000000000000000
          00000000000000000000CEBDBEFFF4F6F5FFFEFAF5FFFEFAF7FFDBD3C1FFFBEF
          D5FFDCD6C5FF56544DFFDAD6C7FFB3B3AAFFB8B8B4FF979997FF9E9FA0FFC7C7
          C9FFFFFFFFFFD8D9DBFFB0B1B3FFB1B2B3FF4C4D4FFF2F3031FF292A27FF928B
          80FFFBEFE6FFAB979AFF0000009D000000160000000000000000000000000000
          00000000000000000000D1BFBFFFF5F7F6FFFFFBF8FFFFFDFCFFD3B88BFFF4D5
          A1FF796C58FFB09C7DFFF7E0BBFFF5E4C6FFF7EBD3FFFEF7E4FF7F7D78FF7D7A
          73FFC9C7BEFFB7B7B3FFA7A8A9FFD6D7D9FFDBDCDDFFDBDCDEFFFFFFFFFF4A47
          42FFFBF1E7FFAE999CFF0000009D000000160000000000000000000000000000
          00000000000000000000D3C2C1FFF6F8F8FFFFFEFCFFE1E2E8FFE9B345FFF1C1
          67FF2E2518FFF3C878FFEEC785FFECCA92FFEFD3A1FFF8DEB4FF302D28FFFAE4
          BDFFF6E4C6FFF6E9D0FFF9F1DEFFFFFCEDFF323230FFBABAB3FF919390FFA8A1
          96FFFBEFE6FFAF9B9EFF0000009D000000160000000000000000000000000000
          00000000000000000000D4C3C2FFF6F8FAFFFFFFFFFFD6C6A2FFE1A528FFECB1
          36FF2C220AFFEDB43CFFE8B346FFE8B754FFECBE68FFF5C97EFF2E261AFFF3CC
          86FFEECC92FFEDD09EFFF1D9AEFFFBE5C0FF302C26FFFDEDD0FFEAE1CCFFEDE3
          D5FFF8EDE5FFB09B9DFF0000009D000000160000000000000000000000000000
          00000000000000000000D7C5C5FFF8FAFDFFFFFFFFFFC8A564FFD99822FFB07E
          22FF6D4E16FFE1A531FFE0A532FFE1A834FFE8B037FFA9802CFF74581DFFEDB7
          45FFE8B751FFE9BB65FFEEC57AFF9A8258FF89734FFFF4D297FFD5C09DFFFAF0
          E3FFF6EBE5FFB09B9FFF0000009D000000160000000000000000000000000000
          00000000000000000000D9C9C7FFFBFEFFFFFFFFFFFF966616FFD18914FF5537
          07FFA67017FFD59424FFD49727FFD89A2AFFE0A22FFF5F4515FFB28227FFE2A7
          33FFE1A833FFE4AC35FFEDB53DFF513F18FFCD9D3CFFEDB952FFC6B390FFF9F1
          E4FFF5ECE5FFB29C9FFF0000009D000000160000000000000000000000000000
          00000000000000000000DBCCC9FFFEFFFFFFD8DADBFF616469FFB0AFB0FF8887
          89FFA27F4BFF926419FFAC700FFFCF8B19FFD6921FFF291A03FFD99625FFD697
          27FFD79929FFDA9F2EFFE3A731FF2B2009FFE6AB30FFE3A629FFD9CFBDFFFAF0
          E5FFF6ECE5FFB59DA1FF0000009D000000160000000000000000000000000000
          00000000000000000000DCCECAFFFFFFFFFF737676FF4C4D4EFF8A8C8EFFFFFF
          FFFFDBDEE3FFDBDFE6FFEFF4FDFF99999BFFA2947DFF33302BFF956519FFAD71
          0FFFCD8818FFD18E1EFFC98C20FF48310AFFDA9824FFBD851DFFFBF7F2FFF9F0
          E5FFF5EBE5FFB69EA2FF0000009D000000160000000000000000000000000000
          00000000000000000000DFD1CBFFFEFFFFFFFFFFFFFFD8D9D8FF9F9F9EFF6060
          60FF515353FF1B1E1FFF4C4E50FFB3B5B8FFC7CACDFFDDE0E4FFFFFFFFFFB3B8
          C0FFADADB0FFB3A691FF6F5833FF7B5A25FFAF700AFFC09755FFFDF6EFFFF9F2
          E6FFF5ECE6FFB8A1A4FF0000009D000000160000000000000000000000000000
          00000000000000000000E0D1CCFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFDBDAD7FF9C9C9AFF727271FF3D3E3EFF3F4142FF4C4E
          51FF8A8C8FFFB3B5BAFFFFFFFFFFDDE0E6FFC6CAD1FF838382FFFDF6EEFFFAF1
          E6FFF4EAE3FFB99FA2FF0000009D000000160000000000000000000000000000
          00000000000000000000E2D3CEFFFBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFEFFFFFFFEFFFFFFFDFFFFFFFDFFFFFFFEFFFFFFFEFFFFFFFEFFE9E6
          E1FFABA8A5FF757571FF414241FF2F3133FF353739FFD0CDC6FFFCF5EBFFF9F2
          E8FFF2E7E1FFBA9DA0FF0000009D000000160000000000000000000000000000
          00000000000000000000E4D5CFFFFCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
          FEFFFEFEFDFFFEFDFBFFFFFDFBFFFEFCF9FFFFFCF8FFFFFCF8FFFFFBF7FFFFFC
          F7FFFFFDF7FFFFFEF7FFFFFEF7FFFFFFF7FFE3DFD8FFFEF8EFFFFBF3E9FFF9F3
          E8FFF1E4DFFFBA9D9FFF0000009D000000160000000000000000000000000000
          00000000000000000000E6D9D1FFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFDFEFFFEFEFDFFFEFDFBFFFEFCFAFFFDFBF8FFFDFAF6FFFCF9F5FFFDF8
          F5FFFDF8F3FFFDF8F3FFFDF8F2FFFDF7F0FFFEF7EFFFFAF4EEFFF9F4EBFFFBF5
          EAFFF0E1DCFFBB9B9EFF0000009D000000160000000000000000000000000000
          00000000000000000000E7DBD2FFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFDFEFFFEFEFDFFFEFDFBFFFEFCFAFFFDFBF8FFFDFAF6FFFCF9
          F5FFFCF8F4FFFCF7F2FFFBF6F1FFFBF6F0FFFAF7F1FFF8F4EFFFF7F3ECFFF8F3
          E9FFF0E0D7FFBC9A9EFF0000009A000000160000000000000000000000000000
          00000000000000000000EADDD4FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFDFEFFFEFEFDFFFEFDFBFFFEFCFAFFFDFBF8FFFDFA
          F6FFFCF9F5FFFCF8F4FFFCF7F2FFFDF9F4FFDFD6D9FFD8D0D5FFDBCDCDFFDCCA
          C5FFDCC5BCFFB7949BFF0000007D000000140000000000000000000000000000
          00000000000000000000EBDFD5FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFFFEFEFDFFFEFDFBFFFEFCFAFFFDFB
          F8FFFDFAF6FFFCF9F5FFFCF9F5FFFFFBF8FFC9A38BFFE4AA2BFFDA9C1EFFD18F
          17FFC9850DFF9C6E4CF200000028000000000000000000000000000000000000
          00000000000000000000EDE1D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFFFEFEFDFFFEFDFBFFFEFC
          FAFFFDFBF8FFFDFAF6FFFCFAF6FFFFFDFAFFCEB1AAFFEDD0A0FFECC686FFEEC4
          76FFB08B6AEF0000002700000000000000000000000000000000000000000000
          00000000000000000000EFE5DAFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFEFDFFFEFD
          FBFFFEFCFAFFFDFBF8FFFEFBF7FFFFFEFBFFD0B7B0FFEFD7ADFFEFCD93FFB391
          79EF000000270000000000000000000000000000000000000000000000000000
          00000000000000000000F2E7DBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FEFFFFFEFCFFFFFDFBFFFFFEFBFFFFFFFCFFD3BFB8FFF3DFBAFFB69785EF0000
          0027000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F4EADFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFEFF
          FFFFFDFFFFFFFDFEFFFFFCFDFFFFFBFDFFFFFBFEFEFFFAFDFEFFF9FCFDFFF8F9
          FCFFF7FAFAFFF6F9F9FFF6F9FAFFF7FCFBFFD4C0BAFFAE9390EE000000270000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F0E2CFFFF4EADFFFF1E7DBFFEFE5D8FFEDE1D7FFEBDF
          D5FFE9DDD3FFE7DBD2FFE5D8D1FFE3D5CFFFE2D3CCFFDFD1CBFFDDCFC9FFDBCB
          C8FFD9CAC6FFD7C7C5FFD7C6C4FFD5C7C5FF9E7F7CDA00000013000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000101
          01020101010D0101012801010150010101720101017C010101740101015D0101
          0149010101400101013B0101013801010136010101320101012E0101012A0101
          0125010101210101011D0101011901031125011BD2E20103111F0101010E0101
          010B01010109010101070101010601052534010E99E001042532000000000101
          0101020202070606061401010182010101FC010101FE010101F1010101CF0101
          01AC010101890101016A0101014C0101012E05050513050505100505050F0404
          040D0404040C0404040B040613191830C1C21130FFFF0F25F0F0020412170202
          0206020202050202020402041923011290C00118BFFF010FA4F0000000000000
          00000000000011111112010101FF010101FF171717FF050505FF010101FF0101
          01FF010101FF010101FF010101FF010101FF010101FF010101E7070707CB2F2F
          2FAF24242493434343773030305A2A2B384A1F44F2F21130FFFF0128FFFF0107
          313000000000010525300114A7E00118D0FF0116B3F001073140000000000000
          00000000000022222246010101FF9E9E9EFFFFFFFFFFFCFCFCFFF5F5F5FFD9D9
          D9FFBABABAFF9E9E9EFF838383FF676767FF4A4A4AFF2E2E2EFF141414FF0101
          01FF010101FF010101FF010101FF010101FF030411FF1D43F0FF1130FFFF011D
          F0FD161A3DCC0518BAF40120E0FF0418C6F50104192000000000000000000000
          00000000000032323262010101FFF5F5F5FFEEEEEEFFEEEEEDFFEDECE9FFEBEA
          E8FFEFEFEFFFEDEDEDFFEDEDEDFFEEEEEEFFF0F0F0FFF4F4F4FFF6F6F6FFF8F8
          F8FFE3E3E3FFC9C9C9FFABABABFF8F8F8FFF727272FF53566FFF384CE2FF1138
          FFFF0120F0FF031FE3FF0517ADFF010419FF1818188300000000000000000000
          0000000000002C2C2C7D010101FFF3F3F3FFF4F4F3FF9C9C9BFF506383FF4E5E
          7BFF7E7C7AFFCCCAC7FFE8E8E8FFE6E6E6FFE3E3E4FFC9CBCEFFD1D3D5FFD7D9
          DAFFDDDEDFFFE2E2E3FFE6E7E7FFE6E6E6FFE8E8E8FFEAEAEAFFB4BBF1FF2048
          FFFF1130FFFF0F2BEFFF9397BAFF575757FF010101E300000000000000000000
          0000000000001E1E1E9A191919FFF2F2F1FFCBCAC7FF0139C2FF0144D3FF0143
          D2FF0145CCFF2B4574FFCCCAC5FFECEEEEFFB3A497FF946336FF856240FF7F63
          4CFF7A6959FF766F67FF7B7B7BFFD6D6D6FFDDDDDDFFB4BCE4FF4060FFFF3050
          FFFF2A48FDFF1138FFFF0120F0FF777B94FF010101D400000000000000000000
          000000000000272727B7353535FFF4F4F4FFFFFDFAFF516B9EFF9DA7B7FFA5AB
          B5FF144DB6FF0145D3FF7E828DFFF2F1F1FFC7C9CBFFCA782FFFDC853AFFDE86
          39FFDB8134FFDC8133FFAA6528FFCFD3D7FFC1C7E5FF4060FFFF4058FFFF3F67
          F4FF6D7C9EFF4866FDFF2040FFFF0120F0FF1F244CC500000000000000000000
          000000000000010101D2545454FFF3F3F3FFF6F6F6FFFEFCF9FFFBFBF9FFFFFF
          FEFF647DAAFF0145D3FF62769DFFF5F3F1FFF1F3F5FFAA8B6FFFE08638FF805E
          41FFA69C92FFB09883FFA48E7DFFCFD5EAFF5070FFFF5078FFFF5677E6FF0992
          27FF4C8152FFC3C5E7FF4968FEFF3048FFFF0120F0FF01072E30000000000000
          000000000000010101EF6D6D6DFFF0F0F0FFF8F8F8FFF9F9F9FFD8D6D1FF9E99
          90FF164CAEFF0144D5FF9FA5AFFFF5F4F3FFF0F0F0FFD4D9DDFFC37833FFCB7C
          36FF91979BFFF0F1F3FFDEE1ECFF6078FFFF6078FFFF677EFEFF5B916DFF019A
          0CFF698B6EFFE4E2E4FFDCDFF5FF3E5CF2FF3050FFFF0128FFFF000000000000
          000000000000010101FF828282FFF3F3F3FFFAFAFAFFFFFFFDFF315AA6FF0144
          D3FF0149D2FF4D6A9FFFFBF8F3FFF2F2F2FFF1F1F1FFF4F6F8FFAD9581FFE588
          37FF886647FFDADDDFFFEAEAEAFFD3D7EBFF6784FDFFE3E0EFFF458E4EFF019B
          0BFF839486FFE5E4E5FFEAEAEAFF15182DFF3D5AE6EC03072120000000000000
          000013131318010101FF989898FFF8F8F8FFFCFCFCFFFFFFFFFF788CB3FF3467
          C7FF0E50CDFF82878CFFFAF9F8FFF5F5F5FFF3F3F3FFF3F3F3FFE1E7EBFFBC77
          38FFD27F36FF95999BFFEFEFEFFFE9E9E9FFE0E2ECFFF3EDF2FF2A8635FF019D
          0BFFA09FA0FFE7E7E7FFD1D1D1FF010101FF26293E5D00000000000000000000
          000026262635010101FFA9A9A9FFFBFBFBFFFFFFFDFFD8D6D1FFFFFFFFFFAAAF
          B8FF0551DFFF32589EFFFCF9F5FFF8F8F8FFF6F7F8FFF2F2F3FFF9FAFCFFB79C
          84FFE78937FF927860FFEEF0F1FFF0F0F0FFC0BBBFFFE8DFE7FF12811EFF0198
          0EFFB8AEB7FFECECECFFB6B6B6FF010101FF2121212900000000000000000000
          000027272751010101FFAEAEAEFFFFFFFFFF8595B2FF19479CFF6A717CFF345A
          A0FF014BDFFF496BA8FFFFFFFCFFF4F5F7FF998675FF878481FFEAEEF1FFB1A9
          A2FFE78938FFA6774DFFEEF1F3FFFBF5FAFF147820FF4B654FFF0B8C19FF028C
          12FFCFC5CEFFF0F0F0FF9B9B9BFF010101FF0C0C0C0E00000000000000000000
          000043434370010101FFBABABAFFFFFFFFFF7084ADFF2363DBFF0B57E5FF0149
          E0FF0141BEFFD1D1D3FFFFFFFFFFC8C7C4FFE78D3FFFCC7F3BFF84674CFFAF75
          41FFED8C39FFAE8E74FFF5F8F9FFF8F3F8FF3E8E48FF20B432FF01A012FF0880
          14FFE7DFE6FFF4F4F4FF7F7F7FFF010101FF0000000000000000000000000000
          00002B2B2B890E0E0EFFB7B7B7FFFFFFFFFFFFFFFFFFD6D5D4FF95A3BEFF9AA6
          BFFFF0ECE5FFFFFFFFFFFDFDFDFFFFFFFFFFBAA99CFFCC8A52FFF39C52FFE491
          48FFAD7B4EFFE3E6EAFFF6F6F6FFF4F4F4FFF4EBF3FF56955DFF3ABD49FF167F
          21FFEFE8EEFFF9F9F9FF646464FF010101E30000000000000000000000000000
          0000424242A51A1A1AFFAEAEAEFFE9E9E9FFF3F3F3FFFDFDFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFEEF2F5FFC3C9CDFFD0D5
          D9FFFEFFFFFFFAFAFAFFF8F8F8FFF7F7F7FFF6F6F6FFFEF6FDFF579A5FFF358D
          3FFFF7F2F6FFFCFCFCFF484848FF010101C80000000000000000000000000000
          0000303030D0010101FF707070FF919191FFA1A1A1FFA3A3A3FFA2A2A2FFB3B3
          B3FFB8B8B8FFCECECEFFD7D7D7FFE0E0E0FFECECECFFF2F2F2FFF9F9F9FFFBFB
          FBFFFAFAFAFFFAFAFAFFF9F9F9FFF9F9F9FFF7F7F7FFF8F8F8FFECE8ECFFD8D4
          D7FFF4F4F4FFFFFFFFFF292929FF242424AB0000000000000000000000001A1A
          1A252C2C2CFF010101FF010101FF010101FF010101FF090909FF202020FF3232
          32FF3E3E3EFF525252FF666666FF7B7B7BFF919191FFA7A7A7FFBBBBBBFFB8B8
          B8FFBEBEBEFFC4C4C4FFCECECEFFD7D7D7FFE0E0E0FFE8E8E8FFEDEDEDFFF2F2
          F2FFF0F0F0FFFFFFFFFF0D0D0DFF1C1C1C8F0000000000000000000000002525
          254F3A3A3AFF767676FF141414FF010101FF313131FF010101FF010101FF3F3F
          3FFFA5A5A5FF818181FF686868FF1D1D1DFF010101FF010101FF010101FF0F0F
          0FFF1A1A1AFF313131FF494949FF616161FF757575FF898989FF9D9D9DFFB9B9
          B9FFC8C8C8FF9D9D9DFF010101FF323232740000000000000000000000003434
          346A484848FF898989FF111111FF121212FFD5D5D5FF0A0A0AFF010101FF0808
          08FFFFFFFFFFFFFFFFFFFFFFFFFFC1C1C1FF010101FF010101FF1E1E1EFFF7F7
          F7FFD5D5D5FFB8B8B8FF666666FF010101FF010101FF030303FF2E2E2EFF1010
          10FF060606FF010101FF010101FF2B2B2B570000000000000000000000002B2B
          2B873C3C3CFF010101FF010101FF010101FF010101FF010101FF161616FF0404
          04FF989898FFCDCDCDFFD7D7D7FFEFEFEFFF010101FF010101FF010101FFE4E4
          E4FFFFFFFFFFFFFFFFFFFDFDFDFF010101FF010101FF010101FFFFFFFFFFFFFF
          FFFFF9F9F9FFD6D6D6FF010101FF2828283D0000000000000000000000003C3C
          3CA4535353FF8C8C8CFF010101FF010101FF282828FF1818189B131313190E0E
          0E3B1414145C1E1E1E7718181896101010B4050505CE070707EC010101FF4343
          43FF909090FFA8A8A8FFCBCBCBFF292929FF020202FF010101FF979797FFF6F6
          F6FFF6F6F6FFFFFFFFFF111111FF1515151C0000000000000000000000002828
          28C44C4C4CFF888888FF2B2B2BFF232323FF2F2F2FFF010101FF010101E02424
          2499303030530B0B0B0C00000000000000000000000000000000000000000B0B
          0B16171717312424244C1414146B202020852E2E2EA4111111BD090909DB4A4A
          4AF85F5F5FFF7E7E7EFF363636FF030303030000000000000000000000000101
          01B4606060FC868686FFA5A5A5FF808080FF010101FF010101FF010101FF5959
          59FFEEEEEEFF959595FF4F4F4FDB3E3E3E952D2D2D4F0B0B0B0C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000004040404181818220D0D0D3D000000000000000000000000000000000000
          0000000000001A1A1A371F1F1F7D0E0E0EC2111111FE131313FF141414FFEEEE
          EEFFF6F6F6FFFFFFFFFFFFFFFFFF656565FF010101FF010101FF010101D53838
          388F2626264A0707070700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000C0C0C3D101010821C1C
          1CC7636363FFA8A8A8FF909090FF010101FF010101FF010101FF9F9F9FFFFFFF
          FFFFD6D6D6FF868686FF363636D02525258A2C2C2C4404040403000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000030303031A1A1A421A1A1A880B0B0BCC010101FF333333FFE4E4E4FFEDED
          EDFFFBFBFBFFFFFFFFFF2E2E2EFF010101FF010101FF030303FF4B4B4BC93636
          36842525253E0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000050505050D0D0D481212128C2424
          24D2737373FF4D4D4DFF010101FF010101FF010101FFDADADAFFFFFFFFFFF4F4
          F4FFC8C8C8FF3D3D3DFF19191921000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000606060B1A1A1A4C20202091010101D7333333FFBEBEBEFFDFDFDFFFF3F3
          F3FFF4F4F4FF010101F200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000A0A0A0E0E0E0E530C0C0C983434
          34DC1E1E1EFF010101B200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000050505150B0B0B2200000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF008000000080000000E0000020E0000001E0000001E0000001E0000001E000
          0001E0000000E0000000E0000000C0000001C0000001C0000001C0000003C000
          0003C0000003C00000038000000380000003800000038000000380000003800F
          80038000FFC7E0000FFFFE0000FFFFC0001FFFFC0007FFFFC00FFFFFFC0FFFFF
          FFCF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000007000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFF9885
          75FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFC2B0
          A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0
          A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF988575FF00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFF00000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          0005000000020000000000000000000000000000000000000007000000150000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001500000007000000000000
          00000000000000000000000000000000000000000000000000040000000E0000
          00120000000700000000000000000000000000000000B88971FFA8694AFFA869
          4AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA869
          4AFFA8694AFFA8694AFFA8694AFFA8694AFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000318110C49634935FF0000
          001D0000001100000009000000090000000900000007B88971FFE8A583FFE9A4
          82FFE9A381FFE8A380FFE9A37EFFEAA17DFFA8694AFFE99F79FFEA9E77FFEA9D
          76FFEB9D75FFEB9B73FFEB9B72FFA8694AFF0000001C00000009000000000000
          0000000000000000000000000000000000000B0806236B523FFF634935FF0000
          0021000000180000001300000013000000130000000EB88971FFE8A584FFE8A4
          82FFE9A381FFE9A380FFE9A37FFFE9A27EFFA8694AFFEA9F7AFFEA9F78FFEB9D
          77FFEB9D75FFEA9C74FFEB9B73FFA8694AFF0000001C00000009000000000000
          000000000000000000000000000000000000634935FF867A6FFF867A6FFF6349
          35FF634935FF634935FF634935FF634935FF00000007B88971FFE8A584FFE8A4
          83FFE8A482FFE9A481FFE9A380FFE9A27FFFA8694AFFEAA07AFFE99F78FFEA9E
          77FFEA9D76FFEB9C75FFEA9B73FFA8694AFF0000001C00000009000000000000
          000000000000000000000000000000000000150F0B37745E4CFF634935FF0000
          00120000000700000000000000000000000000000000B88971FFE8A584FFE9A5
          83FFE9A582FFE9A481FFE9A380FFE9A27FFFA8694AFFEAA07BFFEA9F79FFEA9F
          78FFEA9E77FFEA9C75FFEA9C73FFA8694AFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000016100B39634935FF0000
          00050000000200000000000000000000000000000000B88971FFE8A584FFE8A5
          83FFE8A583FFE9A482FFE8A480FFE9A37FFFA8694AFFEAA07BFFEAA07AFFEA9F
          79FFEA9E77FFEA9C75FFEA9C74FFA8694AFF0000001500000007000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          00090000000900000009000000090000000900000009B88971FFB88971FFB889
          71FFB88971FFB88971FFB88971FFB88971FFB88971FFB88971FFB88971FFB889
          71FFB88971FFB88971FFB88971FFB88971FF0000000700000002000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFF9885
          75FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF988575FF00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFF00000007000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000007000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C00000015000000070000
          0000000000000000000000000000001300300000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFF9885
          75FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF0000001C000000090000
          0000000000000000000000000000006800FF0015003000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000007000FF007000FF00130030000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000007000FF30B030FF006800FF001500300000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000007800FF30B830FF30B030FF007000FF0016
          0030000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF806D59FF0000001C000000090000
          0000000000000000000000000000007800FF20D020FF20C820FF70E070FF0078
          00FF000000000000000000000000000000000000000000000000B49D8CFFC2B0
          A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0
          A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FF806D59FF0000001C000000090000
          0000000000000000000000000000007800FF40D830FF80E080FF008800FF0016
          0030000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000008000FF80E080FF009000FF001900300000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000008800FF009800FF001B0030000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000008800FF001C003000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF988575FF00000015000000070000
          0000000000000000000000000000001900300000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFF00000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          0005000000020000000000000000000000000000000000000007000000150000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001500000007000000000000
          00000000000000000000000000000000000000000000000000040000000E0000
          00120000000700000000000000000000000000000000B88971FFA8694AFFA869
          4AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA869
          4AFFA8694AFFA8694AFFA8694AFFA8694AFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000318110C49634935FF0000
          001D0000001100000009000000090000000900000007B88971FFE8A583FFE9A4
          82FFE9A381FFE8A380FFE9A37EFFEAA17DFFA8694AFFE99F79FFEA9E77FFEA9D
          76FFEB9D75FFEB9B73FFEB9B72FFA8694AFF0000001C00000009000000000000
          0000000000000000000000000000000000000B0806236B523FFF634935FF0000
          0021000000180000001300000013000000130000000EB88971FFE8A584FFE8A4
          82FFE9A381FFE9A380FFE9A37FFFE9A27EFFA8694AFFEA9F7AFFEA9F78FFEB9D
          77FFEB9D75FFEA9C74FFEB9B73FFA8694AFF0000001C00000009000000000000
          000000000000000000000000000000000000634935FF867A6FFF867A6FFF6349
          35FF634935FF634935FF634935FF634935FF00000007B88971FFE8A584FFE8A4
          83FFE8A482FFE9A481FFE9A380FFE9A27FFFA8694AFFEAA07AFFE99F78FFEA9E
          77FFEA9D76FFEB9C75FFEA9B73FFA8694AFF0000001C00000009000000000000
          000000000000000000000000000000000000150F0B37745E4CFF634935FF0000
          00120000000700000000000000000000000000000000B88971FFE8A584FFE9A5
          83FFE9A582FFE9A481FFE9A380FFE9A27FFFA8694AFFEAA07BFFEA9F79FFEA9F
          78FFEA9E77FFEA9C75FFEA9C73FFA8694AFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000016100B39634935FF0000
          00050000000200000000000000000000000000000000B88971FFE8A584FFE8A5
          83FFE8A583FFE9A482FFE8A480FFE9A37FFFA8694AFFEAA07BFFEAA07AFFEA9F
          79FFEA9E77FFEA9C75FFEA9C74FFA8694AFF0000001500000007000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          00090000000900000009000000090000000900000009B88971FFB88971FFB889
          71FFB88971FFB88971FFB88971FFB88971FFB88971FFB88971FFB88971FFB889
          71FFB88971FFB88971FFB88971FFB88971FF0000000700000002000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFF9885
          75FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF988575FF00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFF00000007000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000300000005000000070000000800000009000000090000
          0008000000070000000500000003000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          00050000000A0000000F00000014000000170000001A0000001B0000001B0000
          001A00000017000000140000000F0000000A0000000500000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000050000000B0C0B
          0A2B2B262369433B369C554C46C5625850E46B5E57F86B5E57F861574EE45447
          44C64038339E2823216E0B0908360000001A000000130000000B000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000200000007151311393C36328D6057
          52D9938880FFB4AAA1FFD1C7BFFFE7DED6FFF6EDE5FFF6EDE5FFE7DDD6FFCFC5
          BEFFB2A79FFF8F847CFF5C534BDA38322D9213110F460000001A000000110000
          0007000000020000000000000000000000000000000000000000000000000000
          00000000000000000000000000020000000A332E2A74655C56D7A59C94FFD7CD
          C6FFFDF4EDFFFDF4ECFFFDF4ECFFFDF3ECFFFDF3ECFFFCF3ECFFFCF3ECFFFCF3
          ECFFFCF3ECFFFDF3EAFFD5CBC2FFA1958FFF5F544ED92E29257D0000001F0000
          0014000000090000000200000000000000000000000000000000000000000000
          000000000000000000020C0B0A20433C3895938781FFCEC4BEFFFDF4EDFFFDF4
          ECFFF9F1E9FFFDF4EDFFFDF3EDFFFCF4ECFF908985FF7C736EFFFCF3EAFFFCF3
          EAFFFCF3EAFFFCF2EAFFF1E8E1FFFDF3EAFFCBC0B8FF8B8079FF3C36319D0908
          0735000000150000000900000002000000000000000000000000000000000000
          0000000000010D0B0A1E49423DA0A29992FFE5DDD6FFFDF5EEFF928B87FF7E76
          70FFFDF4EDFFFDF4EDFFFDF4EDFFFDF4ECFF99948FFF8C8680FFFDF3ECFFFDF3
          EAFFFDF3EAFFFCF3EAFF928A86FF817A73FFFDF2EAFFE3DAD0FF9C9189FF413A
          34A7090807350000001400000007000000010000000000000000000000000000
          000000000006443E3B94A49B94FFEDE6DDFFFDF5EEFFFDF5EEFF9B9490FF8D87
          81FFFDF4EDFFFDF5EDFFFDF4ECFFFDF4ECFFFDF4ECFFFDF3ECFFFDF4ECFFFDF4
          ECFFFDF3ECFFFCF4EAFF9B9490FF8D8781FFFCF3EAFFFCF2EAFFEAE2D9FF9C92
          89FF3C36319D0000001F00000011000000050000000000000000000000000000
          000235312D6F968D86FFE7E1D8FFFDF5EEFFFDF6EEFFFDF5EEFFFDF5EEFFFDF5
          EEFFFDF5EEFFFCF4EEFFFCF4EDFFFCF4EDFFFDF4ECFFFCF4ECFFFCF4ECFFFCF4
          ECFFFCF4EAFFFCF4ECFFFCF3EAFFFCF3EAFFFCF4EAFFFCF3EAFFFCF3EAFFE5DB
          D1FF8D827CFF2E29257D0000001A0000000B0000000200000000000000001716
          142F706861D5DED5CDFFFEF6EFFFFEF5EFFFFDF5EEFFFDF5EEFFFDF5EEFFFDF4
          EDFFFDF4EDFFFDF5EDFFFDF4EDFFFCF4EDFFFDF4EDFFFDF4EDFFFDF4EDFFFCF4
          ECFFFDF4ECFFFCF4EAFFFCF4ECFFFCF4ECFFFCF4EAFFFCF3EAFFFCF4E9FFFCF3
          E9FFCEC3BBFF60564ED913110F4600000013000000050000000000000001433D
          3985C3B9B0FFFEF5EFFF96918BFF857C76FFFEF5EEFFFEF5EEFFFDF5EDFFFDF5
          EDFFFDF5EDFFFDF5EDFFFDF5EDFFFDF5EDFFFCF5EDFFFCF4ECFFFDF4ECFFFCF4
          ECFFFCF3ECFFFCF4EAFFFDF3EAFFFDF3EAFFFCF3EAFFFCF3EAFF96908BFF8880
          7AFFFCF3EAFFA79C94FF38322E920000001A0000000A000000010F0D0C1D786F
          69D7EDE3DCFFFEF6EFFF9F9994FF938C87FFFEF6EEFFFDF5EEFFFDF5EEFFFEF5
          EEFFFEF5EEFFFEF5EDFFFDF5EEFFFDF5EDFFFDF5EDFFFDF5ECFFFDF5ECFFFDF5
          ECFFFDF4ECFFFCF4ECFFFDF4EAFFFCF3EAFFFDF4EAFFFCF3EAFF9F9994FF938C
          86FFFCF3EAFFD8D0C7FF5F544EDB0B0909360000000F00000003312D2A5BBBB2
          A9FFFEF8EFFFFEF6EFFFFEF6EFFFFDF6EFFFFDF6EEFFFEF6EEFFFDF6EEFFFDF6
          EEFFFDF5EDFFFDF5EEFFFDF5EDFFFDF4EDFFFDF4EDFFFDF5EDFFFDF4ECFFFDF4
          EDFFFDF4ECFFFDF4ECFFFDF4ECFFFDF4EAFFFDF4EAFFFDF3EAFFFCF3EAFFFCF4
          EAFFFCF3EAFFFBF2E9FF968B85FF2823216E000000140000000550494591D9D0
          C9FFFEF6EFFFFEF6EFFFFEF6EFFFFDF6EFFFFEF6EFFFFEF5EEFFFDF5EEFFFDF5
          EEFFFDF5EDFFFDF5EDFFFDF5EDFFFDF5EDFFFDF5EDFFFDF4EDFFFDF4ECFFFDF4
          ECFFFDF5ECFFFDF4ECFFFDF4ECFFFDF4ECFFFCF4ECFFFCF4ECFFFCF4EAFFFCF4
          EAFFFCF4E9FFFCF4E9FFBBB0AAFF4038359E00000017000000076D655FBEEAE3
          DBFFFEF8EFFFFEF6EFFFFEF6EFFFFEF6EEFFFEF6EEFFFEF6EEFFFEF6EEFFFDF5
          EEFFFEF5EEFFFDF5EEFFFDF5EDFFFDF5EDFFFDF4ECFFFDF4EDFFFDF4ECFFFDF4
          ECFFFDF4ECFFFDF4ECFFFDF4ECFFFDF4EAFFFDF4ECFFFDF4EAFFFDF3EAFFFDF3
          EAFFFCF3EAFFFCF3EAFFD9D0C6FF554B44C60000001A00000008897E76E1F6EF
          E7FFFEF6EFFFFEF6EFFFFEF6EFFFFEF6EEFFFEF6EEFFFEF6EEFFFEF5EEFFFEF5
          EEFFFDF5EDFFFEF5EEFFFEF5EDFFFDF5EDFFC9BFB6FFB2A89FFFDFD7CDFFFDF5
          ECFFFDF5ECFFFDF4ECFFFDF4ECFFFDF4ECFFFDF4EAFFFDF4EAFFFDF4EAFFFDF4
          EAFFFCF3EAFFFDF3EAFFEEE4DAFF625850E40000001B000000099D918BF7FCF5
          EDFFFEF6EFFF9D9691FF89827DFFFEF6EEFFFEF6EEFFFEF6EEFFFEF6EEFFFEF6
          EEFFFEF8F0FFFFF9F2FFFFFAF4FFFFFBF4FF96908BFFA5A09DFF8C857FFFB4AC
          A7FFFEF6F1FFFEF6EFFFFDF4ECFFFDF4ECFFFDF4ECFFFDF4ECFFFDF4EAFFA29B
          95FF928A84FFFDF4EAFFF8EFE5FF6C6159F80000001B00000009A3978EF7FCF4
          EDFFFEF6EEFFADA7A2FFA19B95FFFEF6EEFFFEF6EEFFFDF5EEFFFFF9F2FFFFFB
          F5FFFFFCF9FFFFFCF8FFFFFBF6FFFFFBF6FF958F8AFF786F69FFB2ACA8FF938C
          88FF8C847FFFFCF6F2FFFEF9F2FFFEF6EFFFFDF4ECFFFDF4EAFFFDF4EAFFA9A2
          9DFF9E9691FFFDF4EAFFFBF1E7FF6D6159F80000001A00000008948B81E1F8F1
          E8FFFEF6EEFFFEF6EEFFFEF6EEFFFEF6EDFFFEF6EFFFFFFAF4FFFFFDFAFFFFFD
          FAFFFFFDFAFFFFFDF9FFFFFCF9FFFFFCF9FF999490FF786F6AFFFEFAF5FFC1BC
          B8FFA19B97FF807672FFF1EDE7FFFFFBF6FFFEF8F1FFFDF4ECFFFDF4EAFFFDF4
          EAFFFCF4EAFFFDF4EAFFF2E9DFFF645952E400000017000000077A716BBDEFE6
          DFFFFEF6EEFFFEF6EEFFFEF6EEFFFEF6EDFFFFFBF5FFFFFEFCFFFFFEFCFFFFFE
          FCFFFFFEFBFFFFFEFAFFFFFEFBFFFFFDFAFFA09996FF7F7672FFFFFDF9FFFFFC
          F9FFD8D3D0FFA9A4A0FF817A75FFE1D9D5FFFFFCF6FFFEF8F1FFFDF4EAFFFDF4
          EAFFFDF4EAFFFDF3EAFFE4DBD1FF574D48C500000014000000055A534F8FE1D9
          D0FFFEF5EDFFFEF6EDFFFEF5EDFFFFFAF3FFFFFFFDFFFFFFFCFFFFFFFCFFFFFF
          FCFFFFFFFCFFFFFEFBFFFFFEFBFFFFFEFBFFA59F9CFF877E7BFFFFFDFAFFFFFD
          F9FFFFFDF9FFF2EEEAFFACA8A4FF8F8782FFC5BFBBFFFFFCF6FFFEF8EFFFFDF3
          EAFFFDF3E9FFFDF3E9FFCDC4BAFF433D389C0000000F0000000336322F59CBC1
          B8FFFEF5EDFFFEF5EDFFFEF5EDFFFFFEF9FFFFFFFEFFFFFFFDFFFFFFFDFFFFFF
          FCFFFFFFFDFFFFFFFDFFFFFFFCFFFFFFFCFFA8A19EFF89817DFFFFFEFBFFFFFE
          FBFFFFFDFAFFFFFEF9FFFFFBF9FFBEB8B5FF9D9692FFB0A9A5FFFFFAF4FFFDF4
          E9FFFCF4E9FFFDF4E9FFABA198FF2C2724690000000A00000001100F0E1A8F85
          7ED7F3ECE3FFFDF5ECFFD5CBC1FF88827FFFFFFFFEFFFFFFFFFFFFFFFEFFFFFF
          FEFFFFFFFDFFFFFFFDFFFFFFFDFFFFFFFCFFACA8A3FF918A85FFFFFFFCFFFFFE
          FBFFFFFEFBFFFFFEFBFFFFFDFBFFFFFDFBFFDDD9D6FF948C87FF97928FFFCDC4
          BAFFFCF3E8FFEFE6DCFF6C635EDB0C0B0A2B000000050000000000000000514B
          4781D9D1C9FFFDF5ECFFDED6CCFFA09B98FFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
          FEFFFFFFFFFFFFFFFEFFFFFFFEFFFFFFFDFFB2ADABFF97918CFFFFFFFDFFFFFF
          FCFFFFFFFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFBFFFFFCF8FFADA9A5FFDAD0
          C6FFFCF3E9FFC5BCB3FF3D37338D0000000B0000000200000000000000001B19
          182B928982D5F0E8DFFFFDF5EAFFFFFEFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFEFFFFFFFEFFFFFFFEFFB7B3AFFF9E9794FFFFFFFEFFFFFF
          FDFFFFFFFDFFFFFFFDFFFFFEFDFFFFFFFCFFFFFFFDFFFFFEFBFFFFFBF5FFFCF3
          E9FFEDE3DAFF756B65D916141239000000050000000000000000000000000000
          0000423E3A69CBC2BAFFF9F2E8FFFFFAF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBAB6B4FFA59F9CFFFFFFFFFFFFFF
          FFFFFFFFFEFFFFFFFEFFFFFFFEFFFFFFFDFFFFFFFCFFFFFFFCFFFEF8F1FFF8EF
          E5FFB3AAA0FF342F2C7400000007000000010000000000000000000000000000
          00000000000158534F8DD7CEC6FFFBF3E9FFFFFEF9FFFFFFFFFFABA7A4FF918C
          88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBBB7B5FFAAA4A1FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9D9895FF918C89FFFFFFFEFFFFFBF5FFFAF1E7FFC7BE
          B5FF47403C950000000A00000002000000000000000000000000000000000000
          0000000000000F0E0D18615B5599D6CDC4FFFAF3E9FFFFFDF9FFB5B2B0FFA5A2
          A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCB8B6FFB2ACA9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFB0ADACFFA9A5A4FFFFFCF6FFF9F0E6FFCBC2B9FF4E48
          44A00D0B0B200000000200000000000000000000000000000000000000000000
          000000000000000000000F0E0D18514C468DC5BBB3FFF1EAE1FFFFFAF4FFFFFF
          FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB6B3B0FFA7A29EFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFEFAFFFFF9F2FFF0E7DDFFBEB4ACFF4A433F940D0C
          0B1E000000020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000013D39366989817BD5DCD3CBFFF4EE
          E5FFFFF9F2FFFFFDF8FFFFFFFCFFFFFFFFFFB5B0AFFFA8A4A2FFFFFFFEFFFFFF
          FCFFFFFBF8FFFFF9F1FFF3EAE3FFD8CFC6FF847C73D73934306F000000060000
          0001000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001A18172B4B464281887F
          79D7CFC5BEFFE5DCD6FFF3EAE2FFFAF1E7FFFCF3E8FFFCF3E8FFFAF1E7FFF2E9
          E1FFE4DBD2FFCCC2BAFF837C73D847423D851816152F00000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000100F
          0E1A35312E5957504C8F766D68BD918980E1A79B91F7A69990F78F877EE1736B
          65BE544E4991332F2C5B0F0E0D1D000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000020000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          00090000000C0000000700000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000030001000F0020
          006F0000001E0000001200000006000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000300030012318D2AFF0693
          03FF003000940000001E00000011000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000300040014328B2BFF0CC908FF0DD9
          0AFF069304FF0019005F0000001D000000120000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000300060017368D2FFF0DCA0AFF0DD90AFF0ED8
          0CFF10D710FF079005FF0C4D16840000001E0000001200000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000030008001B368D2FFF37D334FF1ED91BFF0CD407FF10D7
          0EFF12D613FF13D416FF078D06FF0C4D16840000001E00000012000000070000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000003000B0021358C2EFF30D12EFF3BDA39FF31D62FFFA2FCA1FF93FD
          93FF13D416FF17D21BFF1BCE23FF078A07FF0C4D16840000001E000000120000
          0007000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0003000E0028338C2CFF2ECB2BFF3AD138FF36CE34FFA3FBA3FF379D2FFF0581
          03E093FA92FF1AD022FF1CCF24FF20CB2CFF078707FF0E52188A0000001E0000
          0012000000060000000100000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000000D
          0024328B2BFF29C127FF3AC838FF35C634FFA4F7A4FF379D2FFF012601560624
          0B37379D2FFF9DF5A7FF1ECC2AFF21CB2EFF24C635FF078506FF0C4D17840000
          001E000000120000000600000001000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000379D
          2FFF91E690FF3AC038FF36BE34FFA4F1A4FF379D2FFF00130033000000030000
          000006240B33379D2FFF9BF4A7FF24C933FF27C837FF28C33CFF068306FF0C4D
          17840000001E0000001200000006000000010000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000001B
          0040379D2FFF90E290FF92E091FF379D2FFF000A002400000003000000000000
          00000000000006220A30379D2FFFA2F3ACFF29C63BFF2BC43FFF2CC042FF0682
          05FF0C4D17840000001E00000012000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000004
          000B001E0046379D2FFF379D2FFF000500140000000300000000000000000000
          0000000000000000000006220A30379D2FFFA1F2ABFF2DC343FF2EC246FF2DBD
          45FF068104FF0C4D17840000001E000000120000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000300080005000E00000002000000000000000000000000000000000000
          000000000000000000000000000006220A30379D2FFFBAF3C1FF4CD95FFF4DD9
          5FFF4CD95DFF047F02FF0C4D17840000001E0000001200000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000006220A30379D2FFFB7F4C0FF47D8
          5BFF47D75CFF47D85CFF047F02FF0C4D16840000001E00000012000000060000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B3C1255379D2FFFB6F4
          BEFF44D558FF44D559FF44D559FF047F02FF0C4E16840000001E000000120000
          0006000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000B3C1255379D
          2FFFB3F4BDFF41D356FF41D356FF41D356FF047F02FF0C4D16840000001E0000
          0012000000060000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000B3C
          1255379D2FFFB2F4BBFF40D056FF40D056FF40CF56FF047F02FF0C4E16840000
          001E000000120000000600000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000B3C1255379D2FFFB0F6BAFF3DCE54FF3DCE55FF3DCE55FF047F02FF0C4E
          16840000001E0000001200000006000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000B3C1155379D2FFFAFF6B8FF3ACB50FF3ACB50FF3ACB50FF047F
          02FF0C4E16840000001E00000012000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000B3C1155379D2FFFB2F4BBFF3ACB50FF3ACB50FF3ACB
          50FF047F02FF0D4F17860000001E000000120000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000B3C1155379D2FFFB0F6BAFF3ACB50FF3ACB
          50FF3ACB50FF047F02FF0C4E16840000001E0000001200000006000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E4F1870379D2FFFAFF6B8FF3ACB
          50FF3ACB50FF3ACB50FF047F02FF0C4E16830000001800000009000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000D471465379D2FFFAFF6
          B8FF39C951FF39C951FF39C951FF047F02FF07290C4800000005000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000B3C1155379D
          2FFF9EEAABFF9EEAABFF047F02FF0B3F11650000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000B3C
          1255379D2FFF047F02FF0B3F1361000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000007260B3607260B3700000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000050000000700000009000000090000
          0008000000060000000200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000001000000040000000A0000000F00000015000000190000
          0019000000150000000E00000009000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000002172D39063468830952A4CD0B66CBFE0C66CAFD0B5AB5E40742
          87B2021225490000001F0000001B000000140000000B00000004000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000010304031B3645094892B70D67CDFF0D6F
          D4FF0D67CDFF0C68CDFF073A75A2000204270000001A0000000F000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000021428320D68
          CDFF2096F4FF3BA8FCFF0F71D7FF0D67CDFF031A345C0000001D000000100000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000216
          2D380C68CDFF2E9EF7FF54B8FFFF3FA5FAFF0C69CEFF042346700000001A0000
          000A000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000073A74920A6DD3FF48AFFFFF53B8FFFF3CA7FCFF0C67CDFF000102230000
          0010000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000003172E3C0C68CDFF2AA2FDFF3AADFFFF37ACFFFF0C69CFFF0423466F0000
          0017000000070000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000210202B0C68CDFF229BF9FF2FA7FFFF2FA7FFFF1380E4FF094E9ECE0000
          001B000000090000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000030000
          0001000000000000000000000000000000000000000000000000000000000000
          0001031831420C68CEFF259DFBFF2AA4FFFF2BA3FFFF2FA3FCFF0D67CDFF0000
          001C000000090000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000050000000B0000
          0007000000010000000000000000000000000000000000000000000000010000
          000505284F6D0A6BD1FF269FFEFF27A2FEFF27A1FEFF41AFFEFF0D67CDFF0000
          001C000000090000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000053B7493000000150000
          001100000005000000010000000000000000000000000000000100000005031E
          3B550B6ACFFF1D93F3FF259CFCFF259CFCFF269EFCFF43B1FFFF0D68CDFF0000
          001A000000080000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000076DD1FF032244660000
          001D000000110000000500000001000000000000000100000005031B37510A6A
          CFFF1B8FF1FF239AF9FF2499F9FF2499F9FF269CFAFF2496F1FF0C67CDFF0000
          0014000000050000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000371D5FF0472D7FF0322
          446C0000001D000000100000000500000001000000050421425D0A6BCFFF188C
          EFFF2296F7FF2296F7FF2296F7FF2196F7FF34A6FDFF0B6FD4FF0633668D0000
          000D000000020000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074D8FF0E93F5FF0472
          D7FF0321416A0000001D000000110000000A0321415C096AD0FF168AECFF2193
          F5FF2194F5FF2194F4FF2194F5FF2193F5FF4CB5FFFF0C69D0FF0319324C0000
          0005000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074D8FF17A2FFFF0E93
          F5FF0472D7FF0321416A0000001E031E3C5D096BD1FF1488ECFF2091F3FF2091
          F3FF2091F3FF2093F3FF2093F3FF4BB4FFFF0B6BD1FF04244764000000060000
          0001000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074D9FF4BB4FFFF139D
          FBFF0D93F4FF0472D7FF042B5580096BD1FF0A8BEDFF17A4FFFF17A4FFFF17A4
          FFFF17A4FFFF17A4FFFF88CEFFFF096BD1FF0528517000000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074D8FF51BAFFFF17A2
          FFFF17A2FFFF0E93F5FF0470D3FF0B8DEFFF16A2FFFF16A3FFFF17A2FFFF17A3
          FFFF16A3FFFF8ACEFFFF086BD0FF052B56760000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074D9FF52B9FFFF17A1
          FFFF17A1FFFF16A0FFFF0B8DEFFF16A0FFFF16A0FFFF16A0FFFF16A1FFFF16A1
          FFFF8BCFFFFF086CD1FF052D597A000000070000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074D9FF52B7FFFF159E
          FFFF169EFFFF169EFFFF159EFFFF169FFFFF159EFFFF169EFFFF159EFFFF8BCE
          FFFF076DD2FF06305F8100000007000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074DAFF52B5FFFF159C
          FFFF159CFFFF159CFFFF159CFFFF159CFFFF159CFFFF159CFFFF8BCEFFFF086C
          D1FF063161870000000A00000002000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000076D9FF51B3FFFF149A
          FFFF1499FFFF1499FFFF1599FFFF149AFFFF1499FFFF8BCEFFFF0470D5FF0329
          527B0000001E0000001100000006000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000076D9FF6FBEFFFF1497
          FFFF1497FFFF1397FFFF1397FFFF1496FFFF1497FFFF1397FFFF0B8AF3FF0472
          D7FF032447700000001E00000012000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000074D9FF6FBEFFFF1395
          FFFF1395FFFF1395FFFF1294FFFF1395FFFF1394FFFF1394FFFF1395FFFF0B8A
          F5FF0472D7FF032A537D0000001E000000120000000700000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000076D9FF6FBBFFFF1393
          FFFF1293FFFF1392FFFF1393FFFF1293FFFF1292FFFF1393FFFF1292FFFF1392
          FFFF0B89F5FF0472D8FF042F5D870000001E0000001200000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000076D9FF8BCEFFFF6EBA
          FFFF6EBAFFFF6EBAFFFF6EBAFFFF6EBAFFFF6EBAFFFF6EBAFFFF6EBAFFFF4FAC
          FFFF4FACFFFF43A8FFFF0473D9FF042E5B7F0000001400000008000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000076D9FF0076D9FF0076
          D9FF0076D9FF0076D9FF0076D9FF0074D9FF0076D9FF0076D9FF0076D9FF0074
          D9FF0074D9FF0074D9FF0371D7FF076DD3FF021C384B00000004000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000004000000080000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000030000
          0003000000010000000000000000000000000000000000000000000000000000
          0000000000000000000504082539000000170000001200000007000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000060000000D0000
          000D000000060000000100000000000000000000000000000000000000000000
          000000000004050A2D422942E8FF0B1768900000001F00000013000000070000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000507105878050B41650000
          001C000000110000000600000001000000000000000000000000000000000000
          0001040928372C45ECFF7186FFFF3A52FAFF0B1767910000001F000000130000
          0007000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000100000005040A344D2B42DFFF3148E7FF050D
          4A740000001E0000001200000006000000010000000000000000000000000308
          20282B44EAFF7186FFFF7186FFFF7186FFFF3952F9FF0B1767910000001F0000
          0013000000070000000100000000000000000000000000000000000000000000
          0000000000000000000100000005040A344D2B42E0FF7186FFFF7186FFFF3249
          ECFF0711638E000000190000000A000000010000000000000000000000000204
          1418243BE0FFB3BEFFFF687DFFFF687DFFFF677CFFFF3750F6FF0B1767910000
          001E000000120000000600000001000000000000000000000000000000000000
          000000000001000000050409354D2942DEFF677CFFFF687DFFFF687DFFFF697E
          FFFF3047E6FF050C446000000006000000010000000000000000000000000000
          0000060C343F2540E5FFB0BBFFFF5970FFFF6177FFFF7083FFFF344AEDFF0A14
          5B840000001E0000001200000006000000010000000000000000000000000000
          000100000005040A354D293FDAFF5E74FFFF6378FFFF5E74FFFF5970FFFF2E45
          E3FF060D49670000000600000001000000000000000000000000000000000000
          000000000000060C323C263FE2FFAEB9FFFF6175FFFF687CFFFF6377FFFF2E46
          E7FF0A145B840000001E00000012000000060000000100000000000000010000
          0005050B364D283ED8FF596FFFFF5E74FFFF5E74FFFF5B70FFFF2B42DFFF050C
          435F000000050000000100000000000000000000000000000000000000000000
          00000000000000000000060C323C243FE1FFAAB5FFFF5166FEFF5166FEFF5167
          FEFF2C45E3FF0A145B840000001E00000012000000060000000200000005050B
          354D253BD5FF4C62FDFF586DFFFF586DFFFF4D65FEFF283FDAFF050D435F0000
          0005000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000060B2F39243CE1FFA6B1FFFF4359FBFF4359
          FBFF4258FAFF283FDEFF0A145B840000001E000000120000000A050B364E243A
          D1FF4059FAFF485EFFFF485EFFFF4057F9FF263CD5FF060C415C000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000060B2F39233CDEFFA1ADFFFF394F
          F7FF3D53FDFF374DF4FF253BD6FF0A145B840000001E060C36552237CFFF384E
          F5FF3D53FDFF3D53FDFF374DF4FF2339D2FF050D3E5900000005000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000050B2D36223CDDFF9BA9
          FFFF344CF0FF3850F6FF3048EAFF2238D3FF0B16618A2236CDFF334AEEFF384F
          F6FF3850F6FF3248ECFF2236CEFF060D3F590000000500000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000050A2A33213A
          DDFF98A5FFFF3248EAFF364CF0FF2D42E2FF2237CDFF3147E9FF364CF0FF364C
          F0FF2F44E5FF2138CCFF060D3B53000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000050A
          2A33213ADCFF959FFFFF364CF0FF364CF0FF364CF0FF364CF0FF364CF0FF2F44
          E5FF2135CCFF060C3B5600000006000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001050A28352339DDFF364CF0FF364CF0FF364CF0FF364CF0FF3348EBFF2135
          CCFF060C385E0000001500000007000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0005080F3C532945E5FF5972FFFF5972FFFF5972FFFF5972FFFF5871FFFF2237
          CDFF0A145B840000001E00000012000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000060911
          425A2945E4FF4F6AFFFF4F68FFFF506AFFFFA8B5FFFF4E68FFFF4F6BFFFF4E6B
          FFFF2138CDFF0A145B840000001E000000120000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000060A12475F2B47
          E6FF4A65FFFF4A65FFFF4A65FFFFA5B1FFFF314BEBFFA5B1FFFF4A65FFFF4A63
          FFFF4A65FFFF2138CDFF0A155B840000001E0000001200000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001000000060C154F682C48E7FF4761
          FDFF4760FDFF4761FDFFA4B0FFFF2640E2FF0D1A68842A44E4FFA3B1FFFF4761
          FEFF4760FDFF4760FEFF2138CDFF0A155B840000001E00000012000000060000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000070D1858712D47E6FF445EFBFF445D
          FBFF445DFBFFA1AEFFFF2740E2FF060C304500000004091146552A43E3FFA1AE
          FFFF445EFAFF445DFBFF445DFBFF2136CDFF0A155C840000001E000000120000
          0006000000010000000000000000000000000000000000000000000000000000
          00000000000000000001000000070E195D762E48E8FF415AF8FF415BF7FF415A
          F7FF9EABFFFF2741E2FF060D304500000004000000000000000009114655293F
          E2FF9EAAFFFF415AF8FF415AF8FF415AF8FF2136CDFF0A155B840000001E0000
          0012000000060000000100000000000000000000000000000000000000000000
          000000000001000000070F1D667F2F49E8FF3F55F4FF3F56F5FF3F55F5FF9BA8
          FFFF2642E1FF070C304500000004000000000000000000000000000000000911
          46552841E1FF9BA8FFFF3F56F5FF3F56F5FF3F56F4FF2037CCFF0A155C840000
          001E000000120000000600000001000000000000000000000000000000000000
          000100000006111E6B842F48EAFF3C52F2FF3C52F2FF3C53F2FF98A5FFFF2743
          E2FF060C2C3F0000000400000000000000000000000000000000000000000000
          0000091146552741E1FF98A6FFFF3C53F2FF3C52F2FF3C52F2FF2035CCFF0A15
          5C840000001E0000001200000006000000000000000000000000000000000000
          0001111F6B803049E7FF3950EFFF3A50EFFF3950EFFF95A3FFFF2741E1FF060B
          2C3E000000040000000000000000000000000000000000000000000000000000
          00000000000009124755263FE1FF95A3FFFF3950EFFF3950EFFF3950EFFF2035
          CCFF0A155C830000001800000009000000010000000000000000000000000A13
          404A2B45E4FF949FFFFF374DEDFF374DEDFF94A2FFFF2841E1FF060C293B0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000009124755263EDFFF94A2FFFF374DEDFF374DEDFF374D
          EDFF1D32C6FF050B314800000005000000000000000000000000000000000000
          00000A13414B2A46E5FF929DFFFF929DFFFF2841E1FF060B293B000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000009124755253EDEFF929DFFFF929DFFFF2034
          CAFF09134B650000000600000001000000000000000000000000000000000000
          0000000000000A123F492844E3FF2542E1FF060B273400000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000091247552039D9FF223ADDFF0912
          4B60000000050000000100000000000000000000000000000000000000000000
          00000000000000000000060A252A020512160000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002051418060B2D370000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000050000000300000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0007000000110000000E00000005000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000276F
          35FF07160A490000001E00000010000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000296F
          37FF33833EF60E221063000201220000000F0000000500000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000377F
          45FF44C746FF358F41FF08180B560000001E0000000F00000005000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000327E
          46FF56EA59FF44C746FF257B2EFA091C0C5C0000001E00000010000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003280
          45FF58EC58FF58D358FF45C046FF257D2FFC091C0C600000001E0000000F0000
          0005000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003480
          43FF58E95DFF56CF59FF59CE59FF46BB46FF257C2EFE08190B580000001F0000
          0010000000050000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003480
          48FF5AE35CFF57CC57FF57CC57FF57CC57FF46B946FF257B2DFF091C0C5E0000
          001D0000000F0000000500000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003581
          46FF5AE35CFF5AC55AFF57C757FF56C75AFF57C757FF49B449FF237A2BFF0817
          0A560000001D0000000F00000005000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003681
          4AFF5AE35CFF58C358FF58C358FF58C358FF58C358FF58C358FF4BB34BFF2478
          2CFF081A0A5B0000001D0000000F000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003682
          48FF5AE35CFF58C358FF58C358FF58C358FF58C358FF58C358FF58C358FF4CB4
          4CFF247A29FF071609520000001E0000000F0000000500000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003782
          4AFF5AE35CFF58C358FF58C358FF58C358FF58C358FF58C358FF58C358FF58C3
          58FF50B550FF287034FF081709570000001E0000000F00000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003782
          4AFF5AE35CFF57C757FF57C757FF57C757FF57C757FF57C757FF57C757FF57C7
          57FF57C757FF50BB50FF297134FF071608500001001800000007000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003684
          4AFF58ED58FF56D75BFF56D75BFF56D75BFF56D75BFF56D75BFF58D658FF56D7
          5BFF58D658FF58D658FF48E748FF237529FF040C052C00000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003785
          4DFF58ED58FF56D75BFF56D75BFF56D75BFF56D75BFF56D75BFF58D658FF56D7
          5BFF58D658FF48E748FF31883CFF061207390000000400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003884
          4EFF58ED58FF56D75BFF56D75BFF56D75BFF56D75BFF56D75BFF58D658FF56D7
          5BFF4EED4EFF35873DFF0614083C000000040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003884
          51FF59EA5EFF58D458FF58D458FF56D45CFF58D458FF56D457FF58D45DFF4EED
          4EFF348640FF0611083500000004000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003B84
          4FFF5CE95CFF57D35DFF57D358FF57D358FF57D358FF57D358FF56E85BFF3586
          43FF040D06290000000300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003C84
          50FF58E95EFF59CF5CFF59CF5CFF59CF5CFF59CF5CFF59E55EFF378645FF030A
          0524000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003A86
          4EFF5AE45FFF55CE5AFF59CE5CFF58CE5EFF57E45FFF388746FF030A05230000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003B87
          4DFF5AE35FFF57CD5CFF59CA5DFF58DE5DFE3A864CFF03090420000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003B87
          4FFF5CE35CFF59C75EFF57DD5DFE37884BFF0309052100000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003B87
          52FF5CDF61FF59D95EFD398A4AFF030905200000000300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003E86
          52FF5ADA5FFE398B4AFF040A0522000000030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003984
          52FF3A884DFE040A062200000003000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003278
          48F9050E08210000000300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          0101010101060101010901010109010101090101010901010109010101090101
          0108010101050101010200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          01030101010D010101170101011C0101011C0101011C0101011C0101011C0101
          0119010101140101010E01010108010101040101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009671
          5DACC0713CFFBD6E3DFFB56B41FFAC6A44FFA46945FFA16849FF87583FDE5135
          2796201610520101012001010119010101100101010601010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000B09
          080B8E766AA7A47254FFD47E4DFFE07C43FFD07541FFCB7748FFAC7251FFA16E
          51FF9E6C4FFF6C5449E622201F790101011D0101011101010105000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000016131217A78E82C9AC7755FFCE8051FFD6845AFFB37551FFB0724CFFC888
          66FFBA8568FF856E62FF616267F7171C238C0101011A0101010C010101020000
          0000000000000000000000000000000000000000000000000000010101010101
          0103010101080101010901010109010101090101010901010109010101070101
          0102000000002C25202DAF9485E4AE6E47FFAA7659FFB5866EFFAF7A59FFAA6E
          4CFFB28164FF9B7D6CFF516A7AFF203D53D4050B104F01010114010101060000
          0000000000000000000000000000000000000000000000000000010101010101
          010701010112010101190101011C0101011C01010119010101150101010E0101
          01050000000000000000463C364FA07C68F4A76D4BFFA97A5EFFB4876CFFB389
          73FFA17D68FF67818FFF3F6B87FF2B5B78FF132B3BA60101021E0101010A0101
          01010000000000000000000000000000000000000000000000002A1B0F41A96B
          3DFFA96B3DFFA96B3DFFA96B3DFFA96B3DFFA96B3DFFA96B3DFF010101080101
          0102000000000000000003020202705D5381AD7F63FFAC7453FFA27359FF6F7C
          81FF5E7F94FF5A8197FF456E88FF2F5E7BFF3E5560FF01010120010101100101
          010400000000000000000000000000000000000000000000000000000000160E
          08208D5932D3D68656FFD2814FFF87512BCE130B053101010105000000000000
          00000000000000000000000000001614131B705F559F90796CF4819AA9FF6298
          B9FF397CA8FF346889FF336483FF2B5B78FF365462FC261F197B010101170101
          0109010101010000000000000000000000000000000000000000000000000000
          00000B070410A2673CF0B97649FF321C0B640101011401010105000000000000
          000000000000000000000000000000000000000000003C515F9F3E7393FF6C9A
          B6FF679CBDFF5888A6FF316282FF336883FF496575FC292A2AAE0203032A0101
          0112010101060101010100000000000000000000000000000000000000000000
          0000000000004B2F1B71C38054FF5C371B9A0101011901010109010101010000
          0000000000000000000000000000000000000000000035281E69727373FF4373
          94FF568198FF41718FFF28597BFF437189FC808E96F1726E6BFF1818178B0101
          011E0101010F0101010400000000000000000000000000000000000000000000
          0000000000000B070410A1663AF0945B33E20101011B0101010F010101030000
          00000000000000000000000000000000000001010101663D1EA3C17643FFB387
          6EFF947B6CFF6B777EFF6A8BA0FFCDD6DCFCB5B5B5FB8B8A8BFF595959F21010
          10660101011B0101010D01010103000000000000000000000000000000000000
          00000000000000000000694225A0BA784CFF321D0D63010101180101010D0101
          0109010101090101010901010109010101090A06021BA76634FFDC8452FFDC83
          52FFDB8453FFC98B67FF838889FED0CCCAFF9E9E9EFF959596FF8F8F8FFF4E4E
          4EEC08080849010101190101010B010101020000000000000000000000000000
          0000000000000000000021150C30B37345FF714625B60101011F0101011D0101
          011B0101011A0101011A0101011A0101011B4F2C138EBB6F3CFFDA814FFFDA81
          4FFFDF8A5DFFD59772FF67564AB68B8B8BFECDCECEFFD7D7D7FE9E9E9EFF7F7F
          7FFF3D3D3DDA0505053501010115010101080101010100000000000000000000
          00000000000000000000000000007F502DC0CC845AFFA96B3DFFA96B3DFFA96B
          3DFFA96B3DFFA96B3DFFA96B3DFFA96B3DFFB76832FFD77F4DFFD7804CFFD780
          4CFFE8A681FFA4663BF20E0A07274F4F4E85AAA9A8FFB3B3B3FF949494FFA2A2
          A2FF747474FF272727AD02020224010101120101010601010101000000000000
          000000000000000000000000000040281761BB7A4CFFC97D4CFB733B12C6723B
          12C3713B12C1713B12C1713B12C1C3723FF7CF7A45FFD67C4AFFD67D49FFD882
          52FFDEA27CFF694124AA0101010E131313215D5D5CC7A6A6A5FFE4E5E4FFC0C0
          C0FF9E9E9EFF676767FF181818850101011D0101010F01010103000000000000
          0000000000000000000000000000000000009E6338EF925B32E20101011A0101
          010E0101010300000000010101027C4B28C2D99163FFEFA67AFFEFA67AFFF7BC
          9AFFBC7F51FF20140B4301010108000000001F1F1F3B838382ECECECECFFEEEE
          EFFFB2B2B2FF939393FF555555FA0E0E0E630101011801010107000000000000
          000000000000000000000000000000000000603C2290BA7C4FFF281609530101
          01150101010600000000140B0424AD6C3BFFEDA377FFEEA577FFEFA67AFFEDB9
          9AFF7D4C29C7010101100101010300000000000000003A3A3A6E999897F8F7F7
          F7FFE3E3E2FFAAAAAAFF878787FF3F3F3FE70101011B01010109000000000000
          000000000000000000000000000000000000160E0820B17445FF663D1FA70101
          01190101010B01010102502E1584C77F4EFFECA174FFECA374FFF3B48FFFCD91
          69FF40281770010101090101010100000000000000000808080D555555B4B9B9
          B9FFF4F4F5FED4D4D4FEA7A7A7FF727272F50101011901010108000000000000
          000000000000000000000000000000000000000000007F502DC0A2693CF10A06
          0228010101110101010790572EE1E09265FFEC9C70FFEC9F71FFF4BE9EFF9157
          2DE30101011301010105000000000000000000000000000000001817172A7272
          72E2DCDCDCFFF3F3F3FFC8C7C7FD89898AE10101011501010106000000000000
          0000000000000000000000000000000000000000000040281761C18155FF3C23
          0F6F01010116311B095AB97546FFE9986CFFEA996DFFEFAA83FFD79872FF5434
          1E8D0101010C0101010200000000000000000000000000000000000000003D3D
          3C6F8E8D8DF6F0F0F0FFDFDFDDF38D8D8DC10101010F01010104000000000000
          00000000000000000000000000000000000000000000000000009E6338EF7C4B
          27C40101011A6E401DB7D28355FFE99768FFE9976AFFF7BC9BFFAD6E3EFF160E
          0833010101060000000000000000000000000000000000000000000000000606
          0609747372BA8C8C89DBA8A8A6B95F5C59780101010801010101000000000000
          0000000000000000000000000000000000000000000000000000603C2290B476
          49FF1D100646B06E3EFFE69265FFE79465FFEB9E73FFE1A47FFF694126AA0101
          010E010101030000000000000000000000000000000000000000000000000000
          00001515151E61605EAB6C6B6B990A0A0A160101010500000000000000000000
          0000000000000000000000000000000000000000000000000000160E0820B072
          45FFA3673EE7DC8858FFE59162FFE69161FFF5B793FFB97747FF2A1B0F520101
          0108010101010000000000000000000000000000000000000000000000000000
          0000000000001B1B1B276B6A6AB3010101040101010200000000000000000000
          0000000000000000000000000000000000000000000000000000000000007F50
          2DC0ECB697FFE69063FFE58F60FFE89668FFEBAE8AFF86532DD5010101110101
          0104000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003521
          1351CB8C66FFEDA67EFFE58F60FFF1AF8AFFC18152FF402817710101010A0101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000935D35E0F2B796FFE69164FFF1B594FF9F6034F20B070423010101050000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000055351F80D89A74FFF4B897FFD38E65FF5F3C219C0101010D010101020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000160E0820B17245FFF8C0A1FFAC6938FF160E083301010107000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000744929B0D4875AFF734829B60101010D01010103000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000035211351B8794BFF2A1B0F460101010501010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002C1D1341010101010101010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FF800FFFFF8001FFFF8000FFFF8000FFFFC0007F0020007F0030003F0030
          003F80F8001FC0FE000FE07E000FE07C0007F0000003F0000001F8000000F800
          0000FC100400FC100600FC000600FE000F00FE000F80FF001F80FF001FC1FF00
          1FE1FF803FFFFF803FFFFFC07FFFFFC07FFFFFC0FFFFFFE0FFFFFFE0FFFFFFF1
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EADDDD00F4EEEE000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FBF8F800CBABAB00C5958300B9928B00AC828000C7A8A800E5D2D200FAF3
          F300000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FBF8
          F800C39A9700E0AC8400EDC09700F0E2CD00EFE1CC00E1CBB700D6B8A900D5AD
          A400C89C9C00DABABA00EADDDD00FAF3F3000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000F4EEEE00CFA7
          9D00E6B78C00EDBC8A00EEC49B00F2E7D800F0E2CD00EFE1CC00EDDEC500EDDE
          C500B3818100B8858500BD8A8A00C1908D00BF949100CBABAB00DECBCB00F4EE
          EE00000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000F4EEEE00CFA29600EDC0
          9700EDC09700EEBF8D00EFC89F00F3E8D900F2E7D800F0E2CD00EFE1CC00EDDE
          C500A4727200AA787800AE7C7C00C0958B00E6D0A900DABF9E00C7A48B00B48A
          7E00BF9491000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000EAD7D700D6AA9700F2CE9C00F1CB
          9900F1CB9900EEBF8D00F0CBA300F3E8D900F3E8D900F2E7D800F0E2CD00EFE1
          CC009967670099676700A06E6E00B58A8100E8D4B100E6D0A900E5CFA600E5CF
          A600B38D8D000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000F0E0E000DFB89E00F5D8A500F3D2A000F3D2
          A000F1CB9900F1CB9900F1CFA800F7EFE600F3E8D900F3E8D900F2E7D800F0E2
          CD00996767009967670099676700AD837C00E8D4B100E8D4B100E6D0A900E5CF
          A600B38D8D000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000DABF9E00ECCEA800F5DAA800F5D8A500F3D2
          A000F2CE9C00F1CB9900F1CFA800F8F1E900F7EFE600F3E8D900F3E8D900F2E7
          D800996767009967670099676700AD837C00EAD9BD00E8D4B100E8D4B100E6D0
          A900B38D8D000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000DFB89E00ECCEA800F7E0AE00F5D8A500F5D8
          A500F3D2A000F3D2A000F4D6B000F8F1E900F8F1E900F7EFE600F3E8D900F2E7
          D800996767009967670099676700AD837C00EDDEC500EAD8BA00E8D4B100E8D4
          B100B38D8D000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000DFB89E00EDD4AE00F7E0AE00F7E0AE00F3D2
          A000EDBC8A00EDBC8A00F4D6B000FAF6F200F8F1E900F8F1E900F7EFE600F3E8
          D900996767009967670099676700AD837C00EDDEC500EAD9BD00EAD9BD00E8D4
          B100B38D8D000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000DABF9E00F4D6B000F9E8B500F2CE9C0091A9
          C1003596F900458FE400CFBFB500FBF8F800FAF6F200F8F1E900F8F1E900F7EF
          E600E4D3C900D6BFB400C5A69E00BF9C9300EEE0C900EDDEC500EAD9BD00EAD9
          BD00B38D8D000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000DFB89E00E8D4B100EFC89F0075B3E00043A8
          FF00399EFF003596F9004595EE00DAEDFF00FBF8F800FAF6F200F8F1E900F7F0
          E700F7EFE600F3E8D900F2E7D800F2E7D800F0E2CD00EDDEC500EDDEC500EAD9
          BD00B38D8D000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000DFB89E00CBB6A3006AC6F70059BEFF004FB4
          FF0043A8FF003CA1FF003596F9004595EE00DAEDFF00FAF6F200FAF6F200F8F1
          E900F7EFE600F7EFE600F3E8D900F2E7D800F0E2CD00F0E2CD00EDDEC500EDDE
          C500C2939300F0E0E00000000000000000000000000000000000000000000000
          0000000000000000000000000000CBB6A3005FC4FF0067CCFF0067CCFF0059BE
          FF0050B5FF0043A8FF003CA1FF003599FE004596F100EAF6FF00FBF8F800FAF6
          F200F8F1E900F7EFE600F7EFE600F3E8D900F3E8D900F2E7D800EDDEC500B89B
          A500C0949B00F0E0E00000000000000000000000000000000000000000000000
          00000000000000000000AEDAFF004FB4FF0059BEFF005FC4FF0067CCFF0067CC
          FF005FC4FF0050B5FF0048ADFF003CA1FF003599FE005DA6F600F2F9FE00FBF8
          F800FAF6F200F8F1E900F7EFE600F7EFE600F3E8D900ECE1D4009E92AC009E86
          A000E1D5DB000000000000000000000000000000000000000000000000000000
          00000000000000000000DAEDFF0048ADFF0048ADFF0059BEFF0059BEFF0067CC
          FF0067CCFF005FC4FF0050B5FF004FB4FF003CA1FF00399EFF005CA8F800F2F9
          FE00FAF6F200FAF6F200F8F1E900F7EFE600F3E8D9009697B8007A79A600D4D0
          DD00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000DCEFFF004EADFF0048ADFF0050B5FF0059BE
          FF0067CCFF0067CCFF005FC4FF0059BEFF004FB4FF003CA1FF00399EFF005CA8
          F800F2F9FE00FAF6F200FAF6F200F8F1E9008C99C200586BAB00C8CBDF000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000DAEDFF0048ADFF0048ADFF0050B5
          FF0059BEFF0061C6FF0067CCFF005FC4FF0059BEFF004FB4FF0041A6FF00399E
          FF005CA8F800F2F9FE00FAF6F20088A7D800486EBA00BCC6E100000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DAEDFF0048ADFF0048AD
          FF0050B5FF0059BEFF0061C6FF0067CCFF0061C6FF0059BEFF004FB4FF0043A8
          FF003CA1FF005CA8F80083B8F3003E83DC00B9CDED0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000DAEDFF0062B5
          FF0043A8FF0050B5FF0059BEFF0061C6FF0067CCFF0067CCFF0059BEFF004FB4
          FF0043A8FF003CA1FF003599FE006898EE000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000F2F9
          FE0062B5FF0043A8FF004FB4FF0059BEFF005FC4FF0067CCFF0061C6FF005FC4
          FF0050B5FF00388AEC003271E9006486EA000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000F2F9FE004EADFF0041A6FF004FB4FF0050B5FF0071CAFF00AAE3FF00CFEF
          FF00777DCD00010199002446D1006486EA000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000F2F9FE00AAD7FF00B9DFFF00EAF6FF0000000000000000000000
          00008080CD00010199002446D1006486EA000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000C0C0DD008080B3009697B800D0D0E3000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFC03FFFFF8003FFFF00
          003FFE00001FFC00001FF800001FF800001FF800001FF800001FF800001FF800
          001FF800000FF800000FF000001FF000003FF800007FFC0000FFFE0001FFFF00
          03FFFF8003FFFFC003FFFFE1C3FFFFFFC3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000056686EFF56686EFF506067FF4A5A60FF4552
          58FF3F4B52FF39454BFF343E45FF2F393FFF2A3339FF252E33FF212A2FFF1E25
          2BFF1B2227FF181E24FF161C21FF141A1FFF12181DFF11161BFF222C34FF0F14
          19FF0F1419FF0F1419FF0F1419FF0F1419FF0F1419FF0F151AFF000000000000
          000000000000000000006C899AFF56686EFF506067FF4A5A60FF455258FF3F4B
          52FF39454BFF343E45FF2F393FFF2A3339FF252E33FF212A2FFF1E252BFF1B22
          27FF181E24FF161C21FF141A1FFF12181DFF11161BFF10151AFF232D35FF232D
          35FF256B85FF267693FF287B98FF2B87A5FF3094B4FF10151AFF000000000000
          000000000000000000006D8A9BFF8AE1F6FF88E0F5FF87DFF4FF85DEF3FF84DE
          F3FF83DDF3FF81DCF3FF80DBF2FF7EDAF2FF7CD9F1FF7CD9F0FF7AD8F0FF79D7
          EFFF78D7EFFF76D5EFFF76D5EEFF74D4EDFF74D4EEFF73D4EDFF3B4C56FF3578
          90FF202931FF266E88FF297D9AFF2D88A7FF2F8EAEFF10161BFF000000000000
          000000000000000000006F8C9DFF8AE1F6FF89E1F5FF88E0F5FF86DFF4FF85DE
          F4FF84DDF3FF82DCF2FF80DBF3FF7FDAF1FF7EDAF1FF7CD9F1FF7BD9F0FF7AD8
          F0FF79D7F0FF78D6EFFF77D5EFFF75D5EFFF74D4EEFF73D4EDFF5C7684FF4AA9
          C9FF34758DFF202930FF266E8AFF2A809FFF2B83A2FF12181DFF000000000000
          00000000000000000000708E9FFF8BE2F7FF8AE1F6FF89E0F5FF87E0F5FF86DE
          F4FF84DEF4FF83DDF4FF81DCF3FF80DBF2FF7FDBF2FF7EDAF1FF7CD9F1FF7BD8
          F0FF7AD8F0FF78D7F0FF78D6EFFF76D5EEFF75D5EEFF74D4EEFF658191FF64B9
          D5FF4AA9C9FF337289FF1F282FFF266F8BFF277997FF13191EFF000000000000
          000000000000000000007190A1FF8DE3F6FF8BE1F6FF8AE1F6FF88E0F5FF87DF
          F5FF86DEF4FF85DEF4FF82DCF3FF81DCF3FF80DBF2FF7EDAF2FF7DDAF1FF7BD9
          F1FF7BD8F0FF79D7F0FF78D6EFFF77D6EFFF76D6EEFF75D5EEFF698797FF87D0
          E5FF64B9D6FF4AA9C9FF336F86FF1F282FFF256F89FF141A1FFF000000000000
          000000000000000000007393A3FF8EE3F7FF8CE3F6FF8BE1F6FF89E1F6FF88E0
          F5FF86DFF5FF85DEF4FF83DEF3FF83DDF3FF81DCF2FF80DAF2FF7EDAF1FF7CD9
          F1FF7CD9F1FF7AD8F0FF79D7EFFF77D6EFFF77D5EFFF75D5EEFF698797FF9BDE
          EFFF87D0E5FF64B9D5FF4AA9C9FF336F86FF1E272EFF161C21FF000000000000
          000000000000000000007495A4FF8FE4F8FF8DE3F7FF8CE2F6FF8AE1F6FF89E1
          F5FF87E0F5FF86DFF4FF85DEF4FF83DEF3FF82DDF3FF80DCF3FF80DBF1FF7DDA
          F1FF7DD9F1FF7CD8F0FF7AD7F0FF79D7EFFF78D6EFFF76D6EFFF698797FF6987
          97FF678494FF5C7684FF4C626EFF3B4C56FF2A363EFF1B2329FF000000000000
          000000000000000000007697A7FF90E4F8FF8EE3F7FF8DE2F7FF8CE2F6FF8AE1
          F6FF89E0F5FF87E0F4FF86DFF4FF84DDF4FF83DDF3FF82DCF3FF80DBF2FF7FDB
          F2FF7ED9F1FF7CD9F0FF7BD9F0FF7AD7F0FF78D7F0FF78D6EFFF76D5EEFF76D5
          EEFF74D5EDFF73D4EEFF73D4EEFF72D3EDFF181E24FF1A2126FF000000000000
          00000000000000000000779AAAFF90E4F8FF8FE4F8FF8EE3F7FF8CE2F7FF8BE2
          F6FF8AE1F6FF89E0F5FF87E0F5FF86DFF4FF84DDF4FF83DDF3FF81DCF3FF80DB
          F2FF7EDAF1FF7DD9F1FF7CD9F0FF7BD8F0FF79D8F0FF78D7F0FF77D6EFFF76D5
          EFFF75D5EEFF74D4EDFF73D4EEFF73D3EDFF1A2126FF1C2329FF000000000000
          00000000000000000000789CACFF91E5F8FF90E4F8FF8FE4F8FF8DE3F6FF8CE2
          F6FF8BE2F6FF89E1F6FF88E0F5FF86DFF5FF86DEF4FF83DEF3FF83DCF3FF81DC
          F2FF80DAF2FF7EDAF1FF7DD9F1FF7BD9F1FF7AD8F0FF79D7F0FF78D7EFFF76D6
          EEFF75D5EEFF74D5EEFF74D4EEFF73D3EEFF1C2329FF1F272CFF000000000000
          000000000000000000007B9EAEFF92E5F8FF90E5F8FF90E4F8FF8EE3F8FF8DE3
          F6FF8CE2F6FF8AE1F6FF89E0F5FF88E0F5FF87DFF5FF85DEF4FF83DDF3FF82DC
          F3FF81DCF2FF7FDBF2FF7DD9F1FF7CD9F0FF7BD9F0FF7AD8F0FF78D7EFFF78D6
          EFFF77D6EEFF76D5EEFF74D5EEFF73D4EEFF1F272CFF232B31FF000000000000
          000000000000000000007CA1B0FF93E6F9FF92E6F9FF91E4F8FF8FE4F7FF8EE4
          F7FF8DE3F7FF8CE1F6FF8AE1F5FF89E0F6FF87E0F5FF86DFF4FF84DEF4FF83DD
          F3FF81DCF2FF80DBF2FF7FDAF2FF7DD9F1FF7CD9F1FF7BD8F0FF7AD7F0FF78D7
          EFFF78D6EEFF76D5EFFF75D5EEFF74D4EEFF232B31FF272F35FF000000000000
          000000000000000000007EA3B3FF93E7F9FF93E6F9FF91E5F8FF90E0F2FF90E3
          F6FF8EE3F7FF8DE2F7FF8BE1F6FF8AE1F6FF88E0F6FF87DFF4FF86DFF4FF84DD
          F3FF83DDF3FF82DCF3FF80DBF2FF7EDAF1FF7DD9F1FF7CD8F0FF7AD8F0FF79D7
          F0FF78D6EFFF77D6EEFF76D5EEFF75D5EEFF272F35FF2A343AFF000000000000
          000000000000000000007FA5B6FF94E7FAFF93E6F9FF92E6F9FF9F3E0FFF93C3
          CAFF8EE4F8FF8EE3F7FF8DE2F6FF8BE2F6FF8AE0F6FF88DFF5FF86DFF5FF85DF
          F4FF83DDF3FF83DDF3FF81DCF2FF80DBF2FF7EDAF1FF7DDAF0FF7BD8F0FF7AD8
          F0FF79D7EFFF78D6EFFF77D5EFFF76D5EEFF2A343AFF2F393FFF000000000000
          0000000000000000000081A8B7FF94E7F9FF94E6F9FF93E6F9FF9F3E0FFFA362
          3EFF92C9D3FF8FE4F7FF8DE3F7FF8CE2F6FF8BE1F6FF89E0F5FF88E0F5FF87DF
          F4FF85DEF4FF84DEF3FF82DCF2FF81DCF2FF7FDAF2FF7EDAF1FF7CD9F1FF7BD9
          F0FF7AD7F0FF79D7F0FF78D6EFFF76D5EFFF2F393FFF333F45FF000000000000
          0000000000000000000081AAB9FF95E7FAFF95E7F9FF94E6F9FFA03F10FFD17C
          4DFFA0532AFF91CFDAFF8EE3F7FF8CE3F7FF8CE2F7FF8AE1F6FF89E0F5FF87E0
          F4FF86DFF4FF85DDF4FF83DDF3FF82DCF2FF80DBF2FF7FDBF2FF7ED9F1FF7CD8
          F1FF7BD8F0FF79D7F0FF78D7EFFF77D6EFFF333F45FF39454BFF00000000BB63
          37FFB55C2FFFB15728FFAE5325FFAC5021FFA94B1DFFA64819FFA44416FFD17C
          4DFFDD8557FFA04B20FF91D4E2FF8EE3F7FF8DE2F7FF8BE2F6FF89E1F5FF88E0
          F5FF88DFF5FF85DEF4FF84DEF3FF82DDF3FF81DCF2FF80DBF2FF7EDBF2FF7DDA
          F1FF7CD8F0FF7BD8F1FF79D7F0FF79D7EFFF39454BFF3E4B51FF00000000C673
          46FFE1A07AFFCD784DFFCC7850FFCD7B55FFD08159FFD1875FFFD28153FFD17C
          4DFFE18758FFD27A4CFFA0481DFF90DAE9FF8DE3F6FF8CE2F6FF8BE1F6FF8AE1
          F6FF88DFF5FF87DFF4FF86DEF4FF84DDF3FF82DDF3FF81DBF2FF7FDBF1FF7EDA
          F2FF7CD9F1FF7BD8F1FF7AD8F0FF79D7F0FF3E4B51FF445159FF00000000D17F
          54FFECA77CFFDD8D66FFDD8D66FFDD8D66FFDD8D66FFDD8D66FFDD8D66FFDD8D
          66FFDD8D66FFDD8D66FFC66D41FFA14C21FF90DCEEFF8DE3F7FF8CE2F6FF8BE2
          F6FF89E1F5FF88DFF5FF86DFF5FF85DEF4FF84DDF4FF82DCF3FF81DBF2FF7FDB
          F2FF7EDAF2FF7DD9F0FF7CD8F0FF7AD8F0FF445159FF49595FFF00000000D889
          5FFFEAA981FFDB8B65FFDB8B65FFDB8B65FFDB8B65FFDB8B65FFDB8B65FFDB8B
          65FFDB8B65FFDB8B65FFDB8B65FFDEA079FF9F532DFF8EE0F3FF8DE3F7FF8CE2
          F6FF8AE1F5FF89E0F5FF87DFF5FF86DFF4FF84DDF3FF83DDF4FF82DCF2FF81DB
          F3FF7FDBF1FF7ED9F1FF7CD9F0FF7BD9F1FF49595FFF56686EFF00000000DD92
          68FFEEC3A4FFDC996FFFDC996FFFDC996FFFDC996FFFDC996FFFDC996FFFDC99
          6FFFDC996FFFDD9B72FFE8B998FFB66336FF83979FFF7DA2B2FF7BA1B0FF7A9E
          AEFF799CACFF779AAAFF7698A8FF7596A6FF7393A5FF7292A3FF7190A0FF708E
          9FFF6F8D9DFF6E8B9CFF6E8A9BFF6D899AFF6C8899FF0000000000000000E7A4
          7FFFEDC8A8FFEDC8A9FFEDC8A9FFEDC7A8FFEDC7A7FFECC7A7FFECC4A3FFEBBC
          94FFDB9D70FFE8BA95FFBA6939F8160903220000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000ECB1
          93FFE9A983FFDF946DFFDA8B61FFD6875CFFD2855AFFD7976FFFE2B395FFEBC3
          A3FFE7BA96FFC37547F8180B0522000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000D6976EFFEBC3
          A0FFC87A4FF7190D062200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000D8916AFFC981
          57F61A0E09220000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000E3AB89FF1D14
          0F22000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000050000000200000000000000000000000000000000000000000000
          0000000000020000000500000002000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E0000000700000000000000000000000000000000000000000000
          000000000007000000100000000E0000000A0000000A0000000A0000000A0000
          000A0000000A0000000700000002000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF000000130000000B00000004000000050000000200000002000000050000
          000492867DFF000000190000001E0000001C0000001C0000001C0000001C0000
          001C0000001C0000001500000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF0000000F0000000C0000000F000000120000000700000007000000120000
          000F90847BFF887260FF88725FFF88725FFF88725EFF88725EFF86705CFF856F
          5BFF836D59FF826C58FF816B57FF806A56FF7E6854FF7D6753FF7B6551FF7964
          4FFF78634EFF78634EFF755F4BFF0000001C0000000000000000000000009286
          7DFF0000000F1E0C02589B3C0FFF000000260000001A9F3E0FFF1F0C02640000
          00238A7F76FF8B7664FFF5EFECFFF5EFECFFF7F1EEFFF9F2EFFFFBF3F0FFFAF3
          F0FFFAF2EFFFFAF2EEFFF9F1EEFFF9F1EDFFF9F0ECFFF9EFEBFFF9EFEAFFF8EE
          EAFFF8EEE9FFF8EEE9FF78634EFF0000001C0000000000000000000000000000
          000B1909025AA6532AFF92390EFF0000003C000000389F3E0FFFAB552BFF1909
          026D00000028877261FFF1EBE9FFF2ECEAFFF5F0EDFFFAF4F1FFFCF6F3FFFBF5
          F2FFFAF3F0FFFAF3F0FFFAF2EFFFFAF2EFFFFAF2EEFFF9F1EDFFF9F0ECFFF9F0
          ECFFF9EFEBFFF9EFEBFF7C6652FF0000001C000000000000000000000001816C
          5FFF903A11ECBB6337FFB85F33FFAB4E1FFFA84A1BFFAB552BFFDA9871FF913B
          11EE756255FF847060FFEBE7E6FFECE8E7FFF2EDECFFF9F3F1FFFDF6F4FFFCF6
          F4FFFBF5F2FFFBF4F1FFFAF4F1FFFAF3F0FFFAF2F0FFFAF2EFFFFAF2EEFFF9F1
          EEFFF9F1EDFFF9F1EDFF806A56FF0000001C00000000000000000E0501249F43
          17FFEAA581FBC67346FFEDB696FFE8AC89FFE7A985FFE7A985FFE6A581FFEAA5
          80FC9E4217FF896D5BFFF2EEEDFFF3EFEEFFF6F3F1FFFAF7F5FFFDF9F7FFFDF8
          F6FFFCF6F4FFFCF6F3FFFBF5F3FFFBF5F2FFFAF4F1FFFAF3F0FFFAF3F0FFFAF2
          EFFFFAF2EFFFFAF2EFFF836D59FF0000001C000000000000000000000000927C
          6DFFC67346FFD8916AFFD8916AFFD8916AFFD8916AFFDA9871FFE6A581FFC673
          46FF8A7568FF998371FFF8F5F5FFF8F5F5FFFAF7F6FFFCF8F8FFFDF9F8FFFDF9
          F7FFFCF8F6FFFCF7F5FFFCF7F5FFFCF6F4FFFCF6F3FFFBF5F2FFFBF4F1FFFAF4
          F1FFFAF3F0FFFAF3F0FF87715DFF0000001C0000000000000000000000000000
          000B1A0E0839C67346FFD8916AFF0000001100000007D8916AFFC67346FF1A0E
          08390000000F9B8574FFF6F4F4FFF6F4F4FFF9F7F7FFFCF9F9FFFEFAFAFFFEFA
          F9FFFDF9F8FFFDF9F7FFFCF8F7FFFCF8F6FFFCF7F5FFFCF6F4FFFCF6F3FFFBF5
          F3FFFBF5F2FFFBF5F2FF8C7562FF000000150000000000000000000000009286
          7DFF000000191D140F34D8916AFF0000000500000002D8916AFF1D140F2F0000
          000991857DFF9B8575FF9F8776FF9F8776FFA18977FFA28B78FFA28B78FFA189
          77FF9F8775FF9E8674FF9C8572FF9A8470FF99826FFF98816EFF967F6CFF957E
          6BFF947D6AFF947D6AFF907966FF000000070000000000000000000000009387
          7DFF0000000E0000000700000000000000000000000000000000000000000000
          000093877DFF0000000E00000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF0000000A0000000500000000000000000000000000000000000000000000
          000093877DFF0000000A00000005000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E0000000700000000000000000000000000000000000000000000
          0000000000070000000E00000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF000000130000000A00000000000000000000000000000000000000000000
          000093877DFF000000130000000A000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF0000000E0000000700000000000000000000000000000000000000000000
          000093877DFF0000000E00000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF0000000A0000000500000000000000000000000000000000000000000000
          000093877DFF0000000A00000005000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E0000000700000000000000000000000000000000000000000000
          0000000000070000000E00000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF000000130000000A00000000000000000000000000000000000000000000
          000093877DFF000000130000000A000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF0000000E0000000700000000000000000000000000000000000000000000
          000093877DFF0000000E00000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009387
          7DFF0000000C0000001600000014000000140000001400000014000000140000
          001493877DFF000000160000000E0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C00000015000000070000000000000000AC94
          82FFAC9482FFAC9482FFAA9280FFA78F7DFFA48D7AFFA18977FF9D8673FF9982
          6FFF947E6AFF907966FF8C7562FF87715DFF836C59FF7E6854FF7A6450FF7661
          4CFF735D49FF705B46FF6D5843FF6B5641FF6B5641FF6B5641FF6B5641FF6B56
          41FF6B5641FF6B5641FF6B5641FF0000001C0000000A0000000000000000AC94
          82FFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFCFCFFFDFBFBFFFDFA
          F9FFFCF8F7FFFCF7F5FFFBF5F3FFFAF3F0FFFAF2EEFFF9F0ECFFF9EFEAFFF8ED
          E8FFF8ECE6FFF7EBE5FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EA
          E4FFF7EAE4FFF7EAE4FF6B5641FF0000001C0000000A0000000000000000AC94
          82FFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFCFCFFFDFB
          FBFFFDFAF9FFFCF8F7FFFCF7F5FFFBF5F3FFFAF3F0FFFAF2EEFFF9F0ECFFF9EF
          EAFFF8EDE8FFF8ECE6FFF7EBE5FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EA
          E4FFF7EAE4FFF7EAE4FF6B5641FF0000001C0000000A0000000000000000AC94
          82FFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFC
          FCFFFDFBFBFFFDFAF9FFFCF8F7FFFCF7F5FFFBF5F3FFFAF3F0FFFAF2EEFFF9F0
          ECFFF9EFEAFFF8EDE8FFF8ECE6FFF7EBE5FFF7EAE4FFF7EAE4FFF7EAE4FFF7EA
          E4FFF7EAE4FFF7EAE4FF6B5641FF0000001C0000000A0000000000000000AC94
          82FFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFD
          FDFFFEFCFCFFFDFBFBFFFDFAF9FFFCF8F7FFFCF7F5FFFBF5F3FFFAF3F0FFFAF2
          EEFFF9F0ECFFF9EFEAFFF8EDE8FFF8ECE6FFF7EBE5FFF7EAE4FFF7EAE4FFF7EA
          E4FFF7EAE4FFF7EAE4FF6B5641FF0000001C0000000A0000000000000000AC94
          82FFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFD
          FDFFFEFDFDFFFEFCFCFFFDFBFBFFFDFAF9FFFCF8F7FFFCF7F5FFFBF5F3FFFAF3
          F0FFFAF2EEFFF9F0ECFFF9EFEAFFF8EDE8FFF8ECE6FFF7EBE5FFF7EAE4FFF7EA
          E4FFF7EAE4FFF7EAE4FF6B5641FF00000015000000070000000000000000AC94
          82FFAC9482FFAC9482FFAC9482FFAC9482FFAC9482FFAC9482FFAC9482FFAC94
          82FFAA9280FFA78F7DFFA48D7AFFA18977FF9D8673FF99826FFF947E6AFF9079
          66FF8C7562FF87715DFF836C59FF7E6854FF7A6450FF76614CFF735D49FF705B
          46FF6D5843FF6B5641FF6B5641FF000000070000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000050000000200000000000000000000000000000000000000000000
          0000000000020000000500000002000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000070000000A0000000A0000000A0000000A0000000A0000000A0000
          000E000000100000000700000000000000000000000000000000000000000000
          0000000000070000000E00000007000000000000000000000000000000150000
          001C000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000001C0000001C0000001C0000001C0000001C0000
          001E0000001992867DFF00000004000000050000000200000002000000050000
          00040000000B0000001393877DFF0000000000000000000000000000001C755F
          4BFF78634EFF78634EFF79644FFF7A6450FF7C6652FF7D6753FF7F6855FF806A
          56FF816B57FF826C58FF836D59FF856F5BFF86705CFF87715DFF87715DFF8872
          5FFF887260FF90847BFF0000000F000000120000000700000007000000120000
          000F0000000C0000000F93877DFF0000000000000000000000000000001C7863
          4EFFF8EEE9FFF8EEE9FFF8EEE9FFF9EEEAFFF9EFEBFFF9F0ECFFF9F0ECFFF9F1
          EDFFF9F1EEFFFAF2EEFFFAF2EFFFFAF2F0FFFAF3F0FFFAF3F0FFFAF3F0FFF5EF
          ECFF8B7664FF8A7F76FF000000231F0C02649F3E0FFF0000001A000000269B3C
          0FFF1E0C02580000000F92867DFF0000000000000000000000000000001C7C66
          52FFF9EFEBFFF9EFEBFFF9EFEBFFF9F0ECFFF9F1EDFFF9F1EEFFFAF2EEFFFAF2
          EFFFFAF2EFFFFAF2F0FFFAF3F0FFFAF4F1FFFBF4F1FFFBF5F2FFFBF5F2FFF1EB
          E9FF877261FF000000281909026DAB552BFF9F3E0FFF000000380000003C9239
          0EFFA6532AFF1909025A0000000B0000000000000000000000000000001C806A
          56FFF9F1EDFFF9F1EDFFF9F1EDFFFAF2EEFFFAF2EFFFFAF2EFFFFAF2F0FFFAF3
          F0FFFAF4F1FFFBF4F1FFFBF5F2FFFBF5F3FFFBF5F3FFFBF5F3FFFBF5F3FFEBE7
          E6FF847060FF756255FF913B11EEDA9871FFAB552BFFA84A1BFFAB4E1FFFB85F
          33FFBB6337FF903A11EC816C5FFF0000000100000000000000000000001C836D
          59FFFAF2EFFFFAF2EFFFFAF2EFFFFAF2EFFFFAF3F0FFFAF4F1FFFBF4F1FFFBF5
          F2FFFBF5F3FFFCF6F3FFFCF6F4FFFCF7F5FFFCF7F5FFFCF8F6FFFCF8F6FFF2EE
          EDFF896D5BFF9E4217FFEAA580FCE6A581FFE7A985FFE7A985FFE8AC89FFEDB6
          96FFC67346FFEAA581FB9F4317FF0E05012400000000000000000000001C8771
          5DFFFAF3F0FFFAF3F0FFFAF3F0FFFBF4F1FFFBF5F2FFFBF5F3FFFCF6F3FFFCF6
          F4FFFCF7F5FFFCF7F5FFFCF8F6FFFCF9F7FFFDF9F7FFFDF9F8FFFDF9F8FFF8F5
          F5FF998371FF8A7568FFC67346FFE6A581FFDA9871FFD8916AFFD8916AFFD891
          6AFFD8916AFFC67346FF927C6DFF000000000000000000000000000000158C75
          62FFFBF5F2FFFBF5F2FFFBF5F2FFFCF5F3FFFCF6F4FFFCF7F5FFFCF7F5FFFCF8
          F6FFFCF8F7FFFDF9F7FFFDF9F8FFFDF9F9FFFDFAF9FFFDFAFAFFFDFAFAFFF6F4
          F4FF9B8574FF0000000F1A0E0839C67346FFD8916AFF0000000700000011D891
          6AFFC67346FF1A0E08390000000B000000000000000000000000000000079079
          66FF947D6AFF947D6AFF957E6BFF957E6BFF97806DFF98816EFF99826FFF9B84
          71FF9C8572FF9E8674FF9F8775FFA08976FFA18A77FFA28B78FFA28B78FF9F87
          76FF9B8575FF91857DFF000000091D140F2FD8916AFF0000000200000005D891
          6AFF1D140F340000001992867DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E93877DFF00000000000000000000000000000000000000000000
          0000000000070000000E93877DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00050000000A93877DFF00000000000000000000000000000000000000000000
          0000000000050000000A93877DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E0000000700000000000000000000000000000000000000000000
          0000000000070000000E00000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000A0000001393877DFF00000000000000000000000000000000000000000000
          00000000000A0000001393877DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E93877DFF00000000000000000000000000000000000000000000
          0000000000070000000E93877DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00050000000A93877DFF00000000000000000000000000000000000000000000
          0000000000050000000A93877DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E0000000700000000000000000000000000000000000000000000
          0000000000070000000E00000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000A0000001393877DFF00000000000000000000000000000000000000000000
          00000000000A0000001393877DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00070000000E93877DFF00000000000000000000000000000000000000000000
          0000000000070000000E93877DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000E0000001693877DFF00000014000000140000001400000014000000140000
          0014000000160000000C93877DFF000000000000000000000000000000150000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C00000015000000070000000000000000000000000000001C6B56
          41FF6B5641FF6B5641FF6B5641FF6B5641FF6B5641FF6B5641FF6B5641FF6D58
          43FF705B46FF735D49FF76614CFF7A6450FF7E6854FF836C59FF87715DFF8C75
          62FF907966FF947E6AFF99826FFF9D8673FFA18977FFA48D7AFFA78F7DFFAA92
          80FFAC9482FFAC9482FFAC9482FF0000000000000000000000000000001C6B56
          41FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EA
          E4FFF7EBE5FFF8ECE6FFF8EDE8FFF9EFEAFFF9F0ECFFFAF2EEFFFAF3F0FFFBF5
          F3FFFCF7F5FFFCF8F7FFFDFAF9FFFDFBFBFFFEFCFCFFFEFDFDFFFEFDFDFFFEFD
          FDFFFEFDFDFFFEFDFDFFAC9482FF0000000000000000000000000000001C6B56
          41FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EB
          E5FFF8ECE6FFF8EDE8FFF9EFEAFFF9F0ECFFFAF2EEFFFAF3F0FFFBF5F3FFFCF7
          F5FFFCF8F7FFFDFAF9FFFDFBFBFFFEFCFCFFFEFDFDFFFEFDFDFFFEFDFDFFFEFD
          FDFFFEFDFDFFFEFDFDFFAC9482FF0000000000000000000000000000001C6B56
          41FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EBE5FFF8EC
          E6FFF8EDE8FFF9EFEAFFF9F0ECFFFAF2EEFFFAF3F0FFFBF5F3FFFCF7F5FFFCF8
          F7FFFDFAF9FFFDFBFBFFFEFCFCFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFD
          FDFFFEFDFDFFFEFDFDFFAC9482FF0000000000000000000000000000001C6B56
          41FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EBE5FFF8ECE6FFF8ED
          E8FFF9EFEAFFF9F0ECFFFAF2EEFFFAF3F0FFFBF5F3FFFCF7F5FFFCF8F7FFFDFA
          F9FFFDFBFBFFFEFCFCFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFD
          FDFFFEFDFDFFFEFDFDFFAC9482FF000000000000000000000000000000156B56
          41FFF7EAE4FFF7EAE4FFF7EAE4FFF7EAE4FFF7EBE5FFF8ECE6FFF8EDE8FFF9EF
          EAFFF9F0ECFFFAF2EEFFFAF3F0FFFBF5F3FFFCF7F5FFFCF8F7FFFDFAF9FFFDFB
          FBFFFEFCFCFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFDFFFEFD
          FDFFFEFDFDFFFEFDFDFFAC9482FF000000000000000000000000000000076B56
          41FF6B5641FF6D5843FF705B46FF735D49FF76614CFF7A6450FF7E6854FF836C
          59FF87715DFF8C7562FF907966FF947E6AFF99826FFF9D8673FFA18977FFA48D
          7AFFA78F7DFFAA9280FFAC9482FFAC9482FFAC9482FFAC9482FFAC9482FFAC94
          82FFAC9482FFAC9482FFAC9482FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0003000000060000000600000004000000040000000100000000000000000000
          0000000000000000000100000004000000040000000400000004000000040000
          0004000000040000000400000004000000040000000400000004000000040000
          0004000000040000000400000003000000000000000000000000000000000201
          010C050202180301011B00000010000000100000000700000000000000000000
          0000000000000000000400000010000000100000001000000010000000100000
          0010000000100000001000000010000000100000001000000010000000100000
          001000000010000000100000000F0000000300000000000000000D0603184922
          0C8752270BAE3419068D09030027010000150000000700000000000000000000
          0000000000060000000D00000010000000100000001000000010000000100000
          0010000000100000001000000010000000100000001000000010000000100000
          001000000010000000100000000F0000000C00000005000000005B2F1870A25D
          32CFA85827EB7B360FCF3A1803870401002D0000000700000000000000000000
          005F000000830000008700000087000000870000008700000087000000870000
          0087000000870000008700000087000000870000008700000087000000870000
          00870000008700000087000000870000002700000003000000009D5530B4D796
          68E9DB8955F7B75723ED7D350BDB0A0401420000000700000000000000000000
          008F000000BF000000BF000000BF000000BF000000BF000000BF000000BF0000
          00BF000000BF000000BF000000BF000000BF000000BF000000BF000000BF0000
          00BF000000BF000000BF000000BF0000002F00000000000000007E482D90C6A3
          89D8E8B48AF7D08047F380320BCF0F06033F0000000700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002A1D1830925E
          48A5B16542CD8D4723AE35190B4B040100150000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000030202082013
          0D4A28130B601D0E074B0502010F000000030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0006000000080000000800000008000000080000000300000000000000000000
          0000000000000000000200000008000000080000000800000008000000080000
          0008000000080000000800000008000000080000000800000008000000080000
          00080000000800000008000000070000000100000000000000000201000C0A05
          02390A050145070300390200001B000000120000000700000000000000000000
          0000000000020000000700000010000000100000001000000010000000100000
          0010000000100000001000000010000000100000001000000010000000100000
          001000000010000000100000000F000000060000000100000000241209308748
          23C08F471BE75D270BB71709033F0200001B0000000700000000000000000000
          0000000000080000001000000010000000100000001000000010000000100000
          0010000000100000001000000010000000100000001000000010000000100000
          00100000001000000010000000100000001000000007000000008445239CC575
          45E3C96935F9A14415EA632B0BC30B0400450000000D00000000000000000000
          008F000000C1000000C3000000C3000000C3000000C3000000C3000000C30000
          00C3000000C3000000C3000000C3000000C3000000C3000000C3000000C30000
          00C3000000C3000000C3000000C300000033000000010000000098542FA8D396
          6FE3E29864FBC66B30F783360CDF110802490000000B00000000000000000000
          005F0000007F0000007F0000007F0000007F0000007F0000007F0000007F0000
          007F0000007F0000007F0000007F0000007F0000007F0000007F0000007F0000
          007F0000007F0000007F0000007F0000001F0000000000000000673F2A70B58A
          6FC4D69D70EBBF753CE36F2E0EA70D0702320000000500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000E0A09107746
          3388A85631C8914821B32612082F0201000B0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000040704
          03220A0502320804022C0201000B000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00090000000C0000000C0000000C0000000C0000000500000000000000000000
          000000000000000000030000000C0000000C0000000C0000000C0000000C0000
          000C0000000C0000000C0000000C0000000C0000000C0000000C0000000C0000
          000C0000000C0000000C0000000B000000020000000000000000090402182412
          0766271205801A0B036907020127000000150000000700000000000000000000
          0000000000040000000A00000010000000100000001000000010000000100000
          0010000000100000001000000010000000100000001000000010000000100000
          001000000010000000100000000F000000090000000300000000452313589A56
          2ACCA6531EF376320CD02B120667030100250000000700000000000000000000
          002F000000450000004B0000004B0000004B0000004B0000004B0000004B0000
          004B0000004B0000004B0000004B0000004B0000004B0000004B0000004B0000
          004B0000004B0000004B0000004B0000001B0000000500000000B66840D0E09B
          62F3DF7F3FFFB8480FF77C360CDF090300430000000700000000000000000000
          00BF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
          00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
          00FF000000FF000000FF000000FE0000003F0000000000000000965635ACCFA0
          7BE1E4A171F9CA6D38F47F330CD30E0603400000000700000000000000000000
          002F0000003F0000003F0000003F0000003F0000003F0000003F0000003F0000
          003F0000003F0000003F0000003F0000003F0000003F0000003F0000003F0000
          003F0000003F0000003F0000003F0000000F00000000000000004D3326589A6D
          53ACC17E58DBAF6237D35B290F7F080301250000000300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000705050C3D25
          1A5D5A321D90512B1786150A0423010000080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000020000000700000009000000090000
          0009000000090000000700000005000000070000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000070000001300000015000000130000
          0013000000130000000E0000000C000000150000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001500000007000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000735C4AFF735C4AFF735C4AFF735C4AFF735C
          4AFF735C4AFF00000007B49D8DFFA29181FF917D6CFF816C58FF76614CFF7661
          4CFF76614CFF76614CFF76614CFF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8DFFFAF3F0FFFAF3EFFFFAF1EFFFFAF1EEFFFAF1
          EDFFFAF1EDFFFAF0EDFFF9F0ECFF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8DFFFAF3F0FFFAF3EFFFFAF1EFFFFAF1EEFFFAF1
          EDFFFAF1EDFFFAF0EDFFF9F0ECFF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8DFFFAF3F0FFFAF3F0FFF78A52FFF78A52FFF78A
          52FFF78A52FFF9F0ECFFFAF3F0FF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8DFFFBF4F1FFFBF4F1FFFAF4F1FFFAF4F0FFFAF2
          EFFFFBF3EEFFFAF2EEFFFAF1EEFF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8CFFC5B3A4FFC5B2A3FFC5B2A4FFC4B2A4FFC4B1
          A2FFC4B1A2FFC3B1A2FFC3B0A2FF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0004000000030000000100000000735C4AFF0000000E00000007000000000000
          00000000000000000000B49D8CFFFBF6F2FFFBF5F2FFFBF4F2FFFBF4F1FFFBF4
          F1FFFAF3F0FFFAF3EFFFFAF3EFFF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000000000040000
          000D0000000F0000000B00000009735C4AFF0000000E0000000C000000090000
          00090000000700000002B49D8DFFFBF6F4FFFCF5F3FFFBF5F2FFFBF5F1FFFAF4
          F1FFFAF4F0FFFBF3F0FFFAF3F0FF76614CFF0000001C00000009000000000000
          00000000000000000000000000000000000000000000000000000C12A1A8070A
          5E6B0000001B0000001D0000001C0000001C0000001C0000001C0000001C0000
          001C0000001500000007B39D8CFFFCF6F4FFFCF6F4FFF78A52FFF78A52FFF78A
          52FFF78A52FFFBF3F0FFFBF3F0FF76614CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000090E797D141E
          F8FF6E67ACFF917D6CFF816C58FF76614CFF76614CFF76614CFF76614CFF7661
          4CFF0000001C00000009B49D8DFFFCF7F4FFFCF6F4FFFCF6F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFBF4F1FFFBF4F1FF76614CFF0000001C00000009000000000000
          000000000000000000000000000000000000000000000000000000000000615C
          C4FF151FF9FFADACF5FFFCF5F2FFFBF4F1FFFAF3EFFFF9F2EEFFF9F2EDFF7661
          4CFF0000001C00000009B49D8CFFC7B5A7FFC7B4A6FFC7B4A5FFC6B3A5FFC6B3
          A5FFC5B4A5FFC5B3A4FFC5B3A4FF76614CFF0000001C00000009000000000000
          000000000000000000000000000000000000000000000000000000000000B49F
          91FF8083F8FF1620F9FFB0B0F5FFFBF6F2FFFBF4F1FFFBF4F0FFFAF3EFFF7661
          4CFF0000001C00000009B49D8DFFFCF8F6FFFCF7F5FFFCF7F5FFFCF7F4FFFCF6
          F4FFFCF6F3FFFCF6F3FFFBF5F2FF76614CFF0000001C00000009000000000000
          000000000000000000000000000000000000000000000000000000000000B49F
          91FFFDF9F7FF7D80F8FF151FF9FFB6B5F5FFFBF6F3FFFAF4F1FFFAF4F0FF7661
          4CFF0000001C00000009B49D8CFFFCF8F7FFFCF8F6FFFCF7F5FFFDF7F5FFFCF6
          F5FFFCF7F4FFFBF6F3FFFCF6F3FF76614CFF0000001C00000009000000000000
          000000000000000000000000000000000000000000000000000000000000B8A3
          95FFFDF9F8FFFDF9F8FF797CF8FF1620F9FFBAB9F5FFFCF6F2FFFBF5F1FF7661
          4CFF0000001C00000009B49D8CFFFEFBFAFFFEFBFAFFF78A52FFF78A52FFF78A
          52FFF78A52FFFDF9F7FFFDF9F7FF76614CFF0000001C00000009000000000000
          000000000000000000000000000000000000000000000000000000000000BCA7
          98FFFEFBFAFFFDF9F8FFFEF9F7FF7176F8FF1720F9FFBFBEF5FFFBF6F3FF7661
          4CFF0000001C00000009B49D8DFFFDF9F8FFFDF9F7FFFDF8F7FFFCF9F6FFFCF8
          F6FFFCF7F5FFFCF7F5FFFCF7F5FF76614CFF0000001C00000009000000000000
          000000000000000000000000000000000001000000010000000100000000BFAA
          9BFFFEFBFAFFFEFAF9FFFDFAF9FFFEFAF8FF6D72F8FF1620F9FFC3C2F5FF7861
          4CFF0000001D0000000AB49D8CFFC8B7A8FFC8B7A8FFC8B7A7FFC7B6A7FFC7B5
          A7FFC7B6A6FFC7B5A6FFC6B5A6FF76614CFF0000001C00000009000000000000
          000000000000000000000000000100000005000000080000000400000000BFAA
          9BFFFEFBFAFFFEFBFAFFFDFBFAFFFDFAF8FFFDF9F8FF6A6FF8FF1720F9FF7567
          89FF0000001B0000000DB49D8CFFFEFAF8FFFDFAF8FFFDFAF7FFFDFAF8FFFCF8
          F7FFFDF8F7FFFCF8F6FFFCF7F5FF76614CFF0000001C00000009000000000000
          000000000000000000010000000508200E56000000130000000800000000BFAA
          9BFFC0AC9DFFC0AC9DFFC1AC9EFFC0AC9DFFC0AB9CFFBEAA9BFF4E4DD8FF161F
          F7FF030531400000000BB49D8CFFFDFAF9FFFEFBF9FFFEFAF8FFFDFAF8FFFDFA
          F7FFFDF9F7FFFCF9F7FFFCF8F6FF76614CFF0000001C00000009000000000000
          000000000001000000050A2210531B5C2CFF0000001D00000010000000090000
          0009000000070000000200000000000000070000000E00000007000000000C13
          A6AA090E797E00000004B49D8CFFFEFBFAFFFEFBFAFFF78A52FFF78A52FFF78A
          52FFF78A52FFFDF9F7FFFDF9F7FF76614CFF0000001C00000009000000000000
          0001000100070E2A165A1F6030FF348149FF000000230000001F0000001C0000
          001C000000150000000700000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8DFFFDFCFAFFFEFBFAFFFEFBF9FFFEFBF9FFFEFA
          F9FFFDFAF8FFFDF9F8FFFDF9F8FF76614CFF0000001C00000009000000000102
          0108112E185B2A753EFF6CBC83FF519F67FF287B3FFD1C6832FA1A5B2BFF1D62
          2FFF0000001C0000000900000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8CFFCAB9A9FFCAB9A9FFCAB8A9FFCAB8AAFFCAB8
          A9FFC9B7A8FFC9B7A8FFC8B7A8FF76614CFF0000001C00000009000000011B35
          235245955AFF7ACE91FF63B279FF63B279FF63B279FF67B77EFF67B77EFF236D
          36FF0000001C0000000900000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8CFFFEFBFAFFFEFCFAFFFEFCFAFFFEFBFAFFFDFA
          FAFFFEFBF9FFFEFAF9FFFDFAF8FF76614CFF0000001C000000090C1710205EB3
          76FF8BF2AAFF75C68CFF75C68CFF75C68CFF75C68CFF75C68CFF75C68CFF2777
          3DFF0000001C0000000900000000735C4AFF0000001300000009000000000000
          00000000000000000000B49D8CFFFEFCFBFFFEFCFAFFFEFBFAFFFDFCFAFFFEFB
          FAFFFDFBFAFFFDFBF9FFFEFAF9FF76614CFF0000001C00000009000000000C17
          10206BBF83FF8BF2AAFF75C68CFF81D89AFF8AE4A5FF84DD9EFF84DD9EFF2B7E
          41FF000000150000000700000000735C4AFF0000001500000011000000090000
          00090000000900000007B49D8CFFFEFCFBFFFEFCFBFFF78A52FFF78A52FFF78A
          52FFF78A52FFFEFBF9FFFEFBF9FF78614CFF0000001C00000009000000000000
          00001E38254C6BBF83FF8BF2AAFF6BBF83FF5AA66FFF5AA66FFF5AA66FFF5AA6
          6FFF000000070000000200000000735C4AFF0000001300000015000000130000
          0013000000130000000EB49D8CFFFEFCFBFFFDFCFBFFFEFCFBFFFEFCFBFFFEFB
          FBFFFEFBFBFFFEFCFAFFFDFBFAFF917C6AFF0000001500000007000000000000
          000000000000192F1F3F6BBF83FF63AE79FF0000001200000007000000000000
          0000000000000000000000000000735C4AFF735C4AFF735C4AFF735C4AFF735C
          4AFF735C4AFF00000007B49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFFB49D
          8CFFB49D8DFFB49D8DFFB49D8DFFB49D8DFF0000000700000002000000000000
          0000000000000000000015281B365AA66FFF0000000700000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000112016300000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001500000007000000000000
          0000000000000000000000000000000000000000000000000000B9A596FFAD9D
          8EFF988575FF8B7966FF857260FF83705DFF816D5AFF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000BAA596FFFCF8
          F6FFFBF8F5FFFCF6F4FFFCF6F4FFFBF5F2FFFBF4F1FFFAF3F0FFFAF3EFFFF9F2
          EEFFF9F1EDFFF9F1ECFFF9F0EBFFF8EFEAFFF9EFEAFFF8EEE9FFF7EDE8FFF7EE
          E7FFF7EDE7FFF7ECE7FFF7ECE6FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000BBA697FFFCF8
          F7FFFCF7F5FFFBF7F4FFFBF6F3FFFBF5F2FFFBF5F1FFFAF4F0FFFAF3EFFFFAF2
          EEFFFAF2EDFFF9F1ECFFF9F0EBFFF8EFEAFFF8EEE9FFF8EFE9FFF8EEE8FFF7ED
          E7FFF8EDE7FFF7EDE7FFF7ECE7FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000BBA798FFFCF9
          F6FFFCF8F5FFFCF7F4FFFCF6F3FFFBF5F2FFFBF4F1FFFAF4F1FFFAF3EFFFFAF3
          EEFFFAF2EDFFF9F1EDFFF9F0ECFFF8F0EBFFF8EFEAFFF8EFEAFFF7EEE8FFF7ED
          E8FFF8EDE7FFF7EDE7FFF7EDE7FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000BCA899FFFDF9
          F7FFFCF7F5FFCDC2B8FFCDC2B7FFCDC1B7FFCCC1B6FFCCC1B5FFCBC0B6FFCBC0
          B5FFCAC0B5FFCABFB4FFCABFB4FFC9BEB3FFC9BFB4FFC9BEB3FFC9BEB3FFC8BE
          B3FFC9BEB2FFF8EDE8FFF8EDE7FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000BDA89AFFFDF8
          F6FFFCF7F5FFFCF7F5FFFBF6F4FFFBF5F2FFFBF5F2FFFAF5F1FFFAF3EFFFFAF2
          EFFFF9F2EEFFFAF1EDFFF9F0ECFFF9F0EBFFF9EFEBFFF8EFEAFFF8EEE9FFF8EF
          E9FFF8EEE8FFF7EEE8FFF8EEE8FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000BEA99BFFFDF9
          F7FFFDF8F6FFCFC3B9FFCEC3B8FFCDC3B7FFCDC2B7FFCCC2B7FFCCC1B6FFCCC1
          B6FFCBC0B7FFCCC1B8FFCEC4BAFFD0C5BCFFCEC4BAFFCCC1B5FFC9BFB5FFCABE
          B4FFC9BEB3FFF8EFE9FFF8EEE9FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000BFAA9CFFFCF8
          F7FFFDF8F6FFFCF8F5FFFCF6F4FFFBF6F3FFFAF5F2FFFBF5F2FFFAF4F0FFFAF3
          F0FFFAF2F0FFFAF4EFFFFBF5F3FFF5ECE7FFFBF4F1FFFAF2EEFFF8F0ECFFF9EF
          EBFFF8EFEAFFF8EFE9FFF8EFE9FF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000C0AC9DFFFCF9
          F7FFFCF9F6FFD0C5BAFFD0C4B9FFCFC3B9FFCFC3B9FFCEC3B8FFCDC2B8FFCFC4
          BAFFD4CAC1FFDED8D0FFDAC9BEFFA15730FFD4BEB2FFDED7D1FFD3C9C0FFCCC2
          B7FFCAC0B5FFF9EFEAFFF8EFEAFF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000C1AD9EFFFCF9
          F7FFFCF9F7FFFCF8F6FFFCF8F5FFFBF7F4FFFBF6F3FFFBF5F2FFFAF5F1FFFCF6
          F3FFFCF8F5FFE9DAD2FFA45B34FFE19670FFA15730FFE6D5CDFFFBF6F4FFFAF2
          EFFFF9F0EDFFF8F0EBFFF8F0EBFF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000C2AE9FFFFDF9
          F8FFFCF9F8FFD1C6BBFFD0C5BBFFD0C4BBFFCFC5BBFFD1C6BBFFD6CCC3FFE1D9
          D2FFDECDC3FFA55C37FFF1B191FFF3A57DFFE19670FFA15730FFDED0C7FFDED7
          CFFFD1C8BEFFF9F1ECFFF9F1ECFF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000C3AFA0FFFDFB
          F9FFFDF9F7FFFDF9F7FFFCF8F6FFFCF8F6FFFCF7F4FFFCF7F4FFFCF8F7FFE9DB
          D3FFA75E39FFF1B191FFE99D75FFEC9E75FFE99D75FFD78D66FFA15630FFF1E6
          E0FFFCF5F3FFFAF2EFFFF9F2EDFF806D59FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000C5B1A3FFFDFA
          F8FFFCFAF8FFD4C8BDFFD4C7BDFFD3C8BCFFD4C8C0FFDCD3CBFFDACABFFFA860
          3BFFF1B191FFE1936EFFE1936EFFE1936EFFE1936EFFE1936EFFCD835FFFA256
          2FFFE1D8D1FFFAF5F2FFF9F3EFFF806D59FF0000001C00000009000000010000
          0004000000080000000900000009000000090000000900000009CEBDB1FFFDFB
          FAFFFDFBF9FFFDFBF8FFFDFAF8FFFCFAF7FFFCF9F8FFFDFAF8FFBF6B3FFFC069
          3DFFBF683CFFEDAA8AFFDA8C67FFDA8C67FFDA8C67FFCC7A4EFFB15629FFAF53
          26FFAA5327FFFBF6F3FFF9F4F1FF816E5AFF0000001C00000009000000040000
          000F000000190000001C0000001C0000001C0000001C0000001CE4DBD4FFFEFD
          FDFFFEFDFCFFEBE5E0FFEBE5E0FFE9E3DDFFE5DED8FFE0D8D2FFE3DCD5FFEAE5
          E0FFF4EFECFFC0683CFFEDAA8AFFDA8C67FFD98B64FFB65B2EFFF4F1EFFFE8E3
          DEFFDED7D0FFFBF6F2FFFAF4F1FF806D59FF0000001C000000093F21225EAE6A
          7AFFAE6A7AFFAE6A7AFFA6A3A3FFA6A3A3FFA29F9EFF9D9998FF979291FF847E
          7CFF9A4A5CFF9B4A5CFF9B495BFF9B495BFF9A495BFFFEFBFAFFFCFAF8FFFDF9
          F8FFFEFCFBFFC1693DFFF9BA99FFF6A87FFFE8956AFFB85D30FFFDFBFAFFFCF8
          F5FFFBF6F3FFFBF5F2FFFBF4F1FF806D59FF0000001C00000009AE6A7AFFB471
          7FFFBD7986FFAA6472FFEDEDECFF925864FFBA7582FFE2E0DFFFDDDBDBFFD3CF
          CFFF874353FF994E5FFFC28791FFB76F7CFF9D4D5EFFEAE4DEFFDCD3CAFFE0D8
          D1FFECE3DBFFC87246FFF8BA98FFF4A67DFFEB986CFFC66E42FFE0CABCFFD6CD
          C3FFD0C5BAFFFBF5F2FFFBF5F2FF806D59FF0000001C00000009AE6A7AFFC285
          90FFBD7A86FFAA6472FFF3F4F3FF925864FFBA7582FFE8E8E6FFE5E3E3FFD8D6
          D6FF874353FF994E5FFFC28791FFB76F7CFF9D4F5FFFFEFDFCFFFDFBFAFFFEFB
          F9FFD69A7BFFE89D77FFF1B191FFED9E75FFE18A5DFFE4BBA6FFFDF9F8FFFCF7
          F4FFFCF6F3FFFBF6F3FFFCF6F3FF806D59FF0000001C00000009AE6A7AFFC385
          91FFBE7B87FFAA6472FFF9F9F8FF925864FF925864FFEFEFEEFFECEBEAFFDFDE
          DDFF874353FF994E5FFFC28791FFB7707DFF9E5061FFEFEAE6FFE9E3DEFFE9D3
          C7FFD3845CFFEBA987FFE5946EFFE78F67FFCE7D53FFECE6E2FFDCD3CBFFD4C8
          BFFFCFC4BAFFFCF6F4FFFBF6F4FF806D59FF0000001C00000009AE6A7AFFC486
          92FFBE7B87FFAA6472FFFDFEFEFFFBFBFBFFF8F9F8FFF5F6F4FFF2F2F1FFE7E5
          E5FF874353FF874353FFC28791FFB8707EFFA05363FFFFFEFEFFF4E2D8FFD88B
          63FFEBA383FFE18E64FFDC865AFFCE7E55FFF4E4DBFFFDFAF9FFFCF9F7FFFCF8
          F6FFFCF7F5FFFCF7F5FFFCF8F5FF806D59FF0000001C00000009AE6A7AFFC487
          92FFBE7C88FFBE7C87FFAA6472FFAA6472FFAA6472FFAA6472FFAA6472FFAA64
          72FFAA6472FFC28791FFB97280FFB8717FFFA15565FFD7906BFFD98458FFE894
          6AFFDD875BFFCC7A4EFFDEAA8FFFF7EAE5FFFEFBFAFFFCF9F8FFFCF8F7FFFDF9
          F6FFFDF8F6FFFCF8F7FFFCF9F6FF806D59FF0000001800000007AE6A7AFFC488
          93FFBE7E8AFFBE7D88FFBE7B88FFBE7B87FFBD7A86FFBD7985FFBC7884FFBA75
          82FFBA7481FFBA7381FFB97380FFB9727FFFA25766FFD38257FFD28057FFD791
          6DFFEAC6B4FFFCF9F7FFFEFCFBFFFDFBFAFFC4B2A4FF9F8A78FF846F5AFF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000000E00000003AE6A7AFFC489
          93FFBF7E8AFFAE6A7AFFAE6A7AFFAE6A7AFFAE6A7AFFAE6A7AFFAE6A7AFFAE6A
          7AFFAE6A7AFFAE6A7AFFAE6A7AFFB97380FFA35968FFF5F2EFFFEDE8E4FFE8E1
          DCFFE3DBD4FFDED5CDFFFEFBFAFFFDFBF9FFBFAB9CFFFFFFFFFFEEDBCEFFECD2
          BFFFE4C5B0FFD9BAA4FF806D59FF0D0B09360000000400000000AE6A7AFFC58A
          94FFAE6A7AFFE6D1D5FFFDFEFDFFFCFDFCFFFBFCFCFFFBFCFBFFFAFBFBFFF8F9
          F8FFF8F8F7FFE3D1D5FFAE6A7AFFBA7480FFA45B6AFFFFFEFEFFFEFCFCFFFEFC
          FBFFFEFCFBFFFDFCFAFFFEFBFAFFFEFBFAFFBEAA9AFFFFFFFFFFFFEEE4FFF5E1
          D3FFE1C6B2FF806D59FF0F0D0B3B000000040000000000000000AE6A7AFFC58B
          95FFAE6A7AFFFEFFFEFFFEFFFEFFFDFEFCFFFCFDFCFFFCFCFBFFFBFBFBFFF9F9
          F9FFF8F9F8FFF8F8F8FFAE6A7AFFBA7582FFA75F6EFFEEE7E3FFDFD5CCFFD9CD
          C2FFD8CBC1FFD7CBC1FFFDFCFBFFFEFCFAFFBEAA9AFFFFFFFFFFFFEEE4FFE9D4
          C5FF806D59FF0D0B093300000004000000000000000000000000AE6A7AFFC78C
          96FFAE6A7AFFFEFFFEFFE5E6E5FFE4E4E4FFE2E3E2FFE1E2E1FFDFE0E0FFDBDC
          DBFFDADADAFFF8F8F8FFAE6A7AFFBB7683FFA8616FFFFFFFFEFFFFFDFDFFFEFD
          FCFFFEFDFCFFFEFDFCFFFEFCFBFFFEFDFBFFBEAA9AFFFFFFFFFFF3E0D5FF836E
          59FF0B09072C0000000300000000000000000000000000000000AE6A7AFFC68C
          97FFAE6A7AFFFEFFFEFFFEFFFEFFFEFEFEFFFDFEFDFFFCFDFCFFFCFDFCFFFAFB
          FAFFF9FAF9FFF9F9F8FFAE6A7AFFBB7784FFA86271FFFFFFFFFFFEFEFDFFFFFE
          FDFFFFFDFDFFFEFDFDFFFEFDFCFFFEFDFCFFBEAA9AFFF3E0D5FF9C8674FF0907
          0627000000030000000000000000000000000000000000000000AE6A7AFFC78D
          97FFAE6A7AFFFEFFFEFFE6E6E6FFE5E6E5FFE4E4E4FFE2E3E3FFE2E1E2FFDDDD
          DCFFDBDCDCFFF8F9F9FFAE6A7AFF9E5967FFA96471FFFFFFFFFFFFFEFEFFFFFF
          FEFFFFFEFEFFFFFDFDFFFEFEFDFFFEFEFDFFBEAA9AFFBDA89AFF080605240000
          0003000000000000000000000000000000000000000000000000AE6A7AFFC78D
          97FFAE6A7AFFFEFFFEFFFDFFFEFFFEFFFEFFFDFEFDFFFDFEFDFFFCFDFCFFFAFB
          FAFFF9FBF9FFF9FAF9FFAE6A7AFFBC7985FFAA6572FFE6DDD7FFD0C0B4FFC8B4
          A6FFC5B1A3FFC3AFA1FFC2AD9FFFC0AC9EFFBFAA9CFF05050419000000030000
          0000000000000000000000000000000000000000000000000000AE6A7AFFAE6A
          7AFFAE6A7AFFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6D6FFD9D6
          D6FFD9D6D6FFD9D6D6FFAE6A7AFFAE6A7AFFAE6A7AFF00000007000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000040000
          0008000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          000900000009000000090000000900000009000000090000000B000000110000
          00190000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001500000007000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C213F2A6C6AAE71FF6AAE
          71FF6AAE71FFA3A6A4FFA3A6A4FF9EA2A0FF989D9BFF919794FF7C8481FF4A9A
          53FF4A9B53FF499B52FF499B52FF499A52FF0000001C0000000996B9B1FF8EAD
          A7FF759891FF668B85FF60857EFF5D837DFF5A817AFF59807AFF59807AFF5980
          7AFF59807AFF59807AFF5C827CFF6D8F8AFFA6BBB8FF6AAE71FF71B479FF79BD
          83FF64AA6DFFECEDEDFF58925FFF75BA7FFFDFE2E1FFDBDDDCFFCFD3D0FF4387
          4AFF4E9956FF87C291FF6FB77AFF4D9D57FF0000001C0000000996BAB1FFF6FC
          FAFFF5FBFAFFF4FCF9FFF4FCF9FFF2FBF8FFF1FBF7FFF0FAF6FFEFFAF7FFEEF9
          F6FFEDF9F5FFECF9F5FFEBF9F5FFEDF9F5FFF5FCF9FF6AAE71FF85C28EFF7ABD
          84FF64AA6DFFF3F3F4FF58925FFF75BA7FFFE6E7E8FFE3E5E4FFD6D8D7FF4387
          4AFF4E9956FF87C291FF6FB77AFF4F9D59FF0000001C0000000997BBB2FFF7FC
          FAFFF5FCF9FFF4FBF9FFF3FBF9FFF2FBF8FFF1FBF8FFF0FAF7FFEFFAF7FFEEFA
          F6FFEDFAF6FFECF9F5FFEBF9F5FFEDF9F5FFF5FCF9FF6AAE71FF85C38EFF7BBE
          85FF64AA6DFFF8F9F9FF58925FFF58925FFFEEEFEFFFEAECECFFDDDFDFFF4387
          4AFF4E9956FF87C291FF70B77BFF509E59FF0000001C0000000998BBB3FFF6FC
          FBFFF5FCFAFFF4FCFAFFF3FCF9FFF2FBF8FFF1FBF7FFF1FAF7FFEFFAF7FFEEFA
          F7FFEDFAF6FFEDF9F5FFECF9F4FFEEF9F6FFF5FCF9FF6AAE71FF86C48FFF7BBE
          85FF64AA6DFFFEFDFEFFFBFBFBFFF8F8F9FFF4F4F6FFF1F2F2FFE5E7E6FF4387
          4AFF43874AFF87C291FF70B87AFF53A05DFF0000001C0000000999BCB4FFF7FD
          FBFFF5FCF9FFBBCFCCFFBED2CFFFC1D4D0FFC1D4D0FFC0D4D1FFC1D3CFFFC0D3
          CFFFBFD1CFFFBCD0CDFFB8CDCAFFBFD2CFFFDCE6E4FF6AAE71FF87C490FF7CBE
          86FF7CBE87FF64AA6DFF64AA6DFF64AA6DFF64AA6DFF64AA6DFF64AA6DFF64AA
          6DFF87C291FF72B97CFF71B87BFF55A15EFF0000001C000000099ABDB4FFF6FD
          FAFFF5FCF9FFF7FCFAFFF8FCFAFFF8FDFCFFF9FDFBFFF8FDFCFFF7FDFBFFF7FD
          FBFFF6FCFAFFF4FCF9FFF0FAF6FFEFFAF7FFF6FCF9FF6AAE71FF88C491FF7EBE
          87FF7DBE88FF7BBE84FF7BBE85FF7ABD84FF79BD84FF78BC83FF75BA7FFF74BA
          7EFF73BA7DFF73B97DFF72B97DFF57A261FF0000001C000000099BBEB5FFF7FD
          FBFFF7FDFAFFCCDCD8FF279C75FF1B996FFF1B996FFF1B996FFF1B996FFF1B99
          6FFF1B996FFF1FBD87FFD2E0DEFFC7D8D5FFDCE6E4FF6AAE71FF89C493FF7EBF
          88FF6AAE71FF6AAE71FF6AAE71FF6AAE71FF6AAE71FF6AAE71FF6AAE71FF6AAE
          71FF6AAE71FF6AAE71FF73B97DFF59A363FF0000001C000000099CBFB6FFF7FC
          FAFFF6FDFAFFF7FDFCFFDAF5EBFF43CB9EFF76E2BEFF75E2BDFF74E1BCFF74E1
          BCFF75E2BEFF2EC38FFFF6FCFAFFF1FBF8FFF6FCFAFF6AAE71FF8AC594FF6AAE
          71FFD1E6D4FFFDFDFEFFFCFCFDFFFCFBFCFFFBFBFCFFFBFAFBFFF8F8F9FFF7F8
          F8FFD1E3D3FF6AAE71FF74BA7FFF5BA464FF0000001C000000099DC0B8FFF7FC
          FBFFF6FCFBFFC7D9D6FFDEE9E7FF92DFC6FF43CB9EFF70E2BCFF6FE2BCFF6FE2
          BCFF6DE5BEFF2EC38FFFDDE7E5FFCADAD6FFDDE7E4FF6AAE71FF8BC594FF6AAE
          71FFFEFEFFFFFEFEFFFFFCFCFEFFFCFCFDFFFBFCFCFFFBFBFBFFF9F9F9FFF8F8
          F9FFF8F8F8FF6AAE71FF75BA7FFF5FA768FF0000001C000000099EC1B9FFF7FC
          FBFFF8FCFAFFF8FDFCFFF7FDFBFF79D7B8FF52C29EFF71E0BBFF7DEAC6FF7BE4
          C2FF7BE4C2FF2EC38FFFF7FDFBFFF3FBF9FFF7FCFBFF6AAE71FF8CC796FF6AAE
          71FFFEFEFFFFE5E5E6FFE4E4E4FFE2E2E3FFE1E1E2FFE0DFE0FFDBDBDCFFDADA
          DAFFF8F8F8FF6AAE71FF76BB80FF61A86BFF0000001C000000099FC2BAFFF8FD
          FBFFF9FDFBFFDAE6E5FF74D3B3FF41C79BFF69DAB4FF7DEAC6FF85F2CEFF96F8
          D6FF89EECCFF2EC38FFFDEE8E6FFCBDAD8FFDEE7E5FF6AAE71FF8CC694FF6AAE
          71FFFEFEFFFFFEFEFFFFFEFEFEFFFDFDFEFFFCFCFDFFFCFCFDFFFAFAFBFFF9F9
          FAFFF8F9F9FF6AAE71FF77BB81FF62A86AFF0000001C00000009A1C3BBFFF9FD
          FCFFFAFEFCFFA9E1CFFF43AC89FF72E1BCFF7FEBC7FF85F2CEFF99FAD9FF43CB
          9EFF99FAD9FF2EC38FFFF8FDFBFFF3FCF9FFF7FCFBFF6AAE71FF8DC796FF6AAE
          71FFFEFEFFFFE6E6E6FFE5E5E6FFE4E4E4FFE3E2E3FFE2E2E1FFDCDDDDFFDCDB
          DCFFF9F8F9FF6AAE71FF599E62FF64A96EFF0000001C00000009A3C5BCFFF9FD
          FCFFFBFDFDFF54C19DFF6CDEB8FF82EFCBFF85F2CEFF99FAD9FF46AC8AFF75C0
          A7FF43CB9EFF2EC38FFFDBE6E4FFCADBD8FFDCE6E4FF6AAE71FF8DC796FF6AAE
          71FFFEFEFFFFFFFDFFFFFEFEFFFFFDFDFEFFFDFDFEFFFCFCFDFFFAFAFBFFFAF9
          FBFFF9F9FAFF6AAE71FF79BC83FF65AA6FFF0000001500000007A4C6BDFFFAFD
          FCFFFCFEFEFF33AE86FF75E8C2FF81ECC8FF99FAD9FF48AD8AFFF9FDFCFFF6FB
          FAFF99D1BFFF2EC38FFFF6FCFAFFF3FBFAFFF6FCFAFF6AAE71FF6AAE71FF6AAE
          71FFD6D9D7FFD6D9D7FFD6D9D7FFD6D9D7FFD6D9D7FFD6D9D7FFD6D9D7FFD6D9
          D7FFD6D9D7FF6AAE71FF6AAE71FF6AAE71FF0000000700000002A5C7BFFFFBFD
          FDFFFCFEFEFF54C19DFF72E7C1FF8DF3D1FF71D9B6FFBADDD3FFD6E4E2FFCCDD
          DBFFCFDFDCFFD0DEDCFFC6D8D4FFBFD4D1FFC6D8D4FFD3E1DFFFDBE6E4FFDDE7
          E6FFDDE7E5FFF8FDFCFFF8FDFCFFB1C3C0FF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000A6C7C0FFFBFD
          FDFFFCFEFEFF6BD3B0FF6DE5BEFF80EEC8FF50D4A9FFF7FCFBFFF9FDFBFFF6FC
          FAFFF5FBF9FFF5FCFAFFF4FBF8FFF2FBF7FFF3FBF8FFF3FBF9FFF4FBF8FFF3FC
          F9FFF4FBF8FFF3FCF9FFF3FCF9FF72938EFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000A8C8C1FFFBFE
          FDFFFCFEFDFF9BDCC7FF50D4A9FF8AF5D1FF6FD7B4FFA1D2C2FFDCE8E6FFCEDF
          DCFFC8DAD7FFC5D8D4FFC2D6D3FFBFD3D0FFBCD2CCFFBCD0CCFFBAD0CCFFBAD0
          CCFFB9D0CDFFF2FBF8FFF2FBF8FF5C827CFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000A8C9C1FFFCFE
          FDFFFCFEFDFFF1FAF8FF77D5B6FF65E0B8FF99FAD9FF55C19EFF98D0BEFFECF7
          F5FFF6FCFAFFF2F9F6FFF3FAF8FFE3F5EFFFEEF8F4FFF4FBF9FFF3FBF9FFF3FC
          F9FFF3FCF9FFF3FBF9FFF3FCF9FF59807AFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000A9CAC2FFFCFE
          FEFFFBFEFDFFCFE0DDFFCEE6E0FF73D0B3FF60DDB4FF50D3A8FF43CB9EFF33AE
          86FF1B996FFF33AE86FF63C3A4FFBAD7D0FFC1D4D0FFBCD2CDFFBAD1CDFFBBD1
          CCFFBACFCBFFF4FCF9FFF4FBF8FF59807AFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000AACBC3FFFCFE
          FEFFFCFEFEFFFBFEFEFFFBFCFDFFF9FDFCFFC8EDE0FF7FD1B6FF60DDB4FF60DD
          B4FF7FD1B6FFC8ECE0FFF7FCFBFFF7FCFBFFF6FDFBFFF6FCFAFFF6FCFAFFF6FC
          FAFFF5FCF9FFF5FCF9FFF5FCFAFF59807AFF0000001C00000009000000000000
          0000000000000000000000000000000000000000000000000000ABCCC5FFFDFF
          FEFFFCFEFEFFC3D9D4FFC5DAD5FFC9DCD8FFD0E0DDFFD7E5E3FFDCE8E6FFDCE8
          E5FFD6E4E1FFCDDEDCFFF9FDFCFFF7FCFBFFF7FDFBFFF7FCFAFFF7FCFAFFF6FD
          FBFFF6FDFAFFF7FCFAFFF6FCFBFF59807AFF0000001800000007000000000000
          0000000000000000000000000000000000000000000000000000ACCDC6FFFDFE
          FEFFFDFEFEFFFDFFFEFFFCFEFEFFFBFEFDFFFBFEFDFFFBFEFDFFFBFEFCFFFBFE
          FCFFFAFDFBFFF9FDFBFFF8FDFCFFF8FDFCFF9ABEB6FF749C93FF59837CFF5980
          7AFF59807AFF59807AFF59807AFF59807AFF0000000E00000003000000000000
          0000000000000000000000000000000000000000000000000000ADCEC7FFFEFE
          FEFFFDFEFEFFC3D9D4FFC2D9D5FFC2D8D4FFC1D7D2FFC1D8D4FFC2D8D3FFC1D8
          D4FFC0D6D1FFC0D6D1FFF9FEFDFFF9FDFCFF9ABEB6FFFFFFFFFFCEEEE6FFBFEC
          E1FFB0E4D6FFA4D9CCFF59807AFF090D0C360000000400000000000000000000
          0000000000000000000000000000000000000000000000000000AECFC7FFFEFE
          FEFFFEFEFEFFFDFFFDFFFDFEFDFFFCFFFEFFFDFEFDFFFCFEFEFFFBFEFDFFFBFE
          FDFFFBFEFDFFFAFDFDFFFAFEFCFFFAFEFCFF9ABEB6FFFFFFFFFFE4FFF7FFD3F5
          ECFFB2E1D6FF59807AFF0B0F0E3B000000040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000AFD0C8FFFFFF
          FFFFFFFFFEFFC4DAD5FFC3D9D5FFC4D9D5FFC2D8D4FFC2D9D4FFC2D8D4FFC1D8
          D4FFC1D8D3FFC1D7D2FFFBFDFDFFFAFEFDFF9ABEB6FFFFFFFFFFE4FFF7FFC5E9
          E0FF59807AFF090D0C3300000004000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B0D0C9FFFFFF
          FFFFFFFFFFFFFEFFFFFFFEFEFFFFFEFFFEFFFEFEFEFFFDFFFFFFFDFFFEFFFCFE
          FEFFFCFEFEFFFCFEFEFFFBFEFDFFFBFEFEFF9ABEB6FFFFFFFFFFD5F3EAFF5983
          7CFF070B0A2C0000000300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B1D1CAFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFFFEFFFEFEFFFFFEFFFFFFFEFEFEFFFDFEFEFFFDFF
          FFFFFDFFFEFFFDFEFDFFFCFEFEFFFCFEFEFF9ABEB6FFD5F3EAFF749C93FF0609
          0827000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B1D2CAFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FFFFFEFFFEFFFDFFFEFFFDFEFEFFFDFEFEFF9ABEB6FF9ABDB4FF050807240000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B2D2CBFFB2D2
          CBFFB1D1CAFFB0D0C9FFAECFC7FFADCDC6FFABCCC4FFA9CAC2FFA7C8C0FFA5C7
          BEFFA3C5BCFFA1C3BAFF9FC2B9FF9EC0B7FF9CBFB6FF04050519000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000002000000070000000A0000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A00000007000000020000000000000000000000000000
          00000000000000000000000000000000000000000007000000150000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C00000015000000070000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFDFDFFFDFCFBFFFDFBFAFFFCF9F8FFFCF8
          F6FFFCF7F4FFFBF5F2FFFAF3F0FFF9F1EDFFF9F1ECFFF8EFEBFFF8EDE8FFF7ED
          E7FFF7EBE6FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFDFCFBFFFDFBF9FFFCF9
          F7FFFCF7F5FFFBF6F3FFFAF4F1FFFAF3EFFFF9F1EDFFF9F0EBFFF9EFEAFFF8ED
          E8FFF7ECE7FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFDBC3B4FFDAC2
          B3FFD9C0B0FFD7BEAFFFD5BCACFFD4B9A9FFD1B6A7FFCFB4A3FFCDB19FFFCAAF
          9DFFC8AB9AFFC6A997FFC4A694FFC2A492FFC0A290FFC0A18EFFBE9F8CFFF8EE
          E9FFF8EDE7FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFDFFFEFDFCFFFDFCFBFFFEFB
          F9FFFCF9F7FFFCF8F6FFFBF6F3FFFBF5F1FFFAF3F0FFFAF2EEFFF9F0ECFFF9EF
          EAFFF8EDE8FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFFFEFDFDFFFEFDFBFFFEFC
          FAFFFDF9F8FFFDF9F6FFFCF7F5FFFBF6F3FFFAF4F0FFFAF2EEFFFAF1EDFFF9F0
          EBFFF8EEE9FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFDBC3B4FFDAC2
          B2FFD8C0B1FFD7BEAFFFD5BCACFFD3B9A9FFD1B7A7FFCFB4A3FFCCB1A0FFCBAF
          9DFFC8AB9AFFC5A896FFC4A694FFC2A491FFC1A28FFFBFA08EFFBE9F8CFFF9F1
          ECFFF9EFEBFF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFEFFFEFC
          FCFFFDFCFAFFFDFAF8FFFDF8F7FFFCF7F5FFFBF5F3FFFAF4F1FFFAF3EFFFF9F1
          EDFFF9F0EBFF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFD
          FCFFFEFCFCFFFEFBFAFFFDFAF8FFFCF8F7FFFCF6F4FFFBF5F2FFFAF4F0FFFAF2
          EEFFF9F0ECFF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFDBC3B4FFDAC2
          B3FFD9C0B0FFD7BEAEFFD5BCACFFD3B9A9FFD1B7A6FFCEB4A3FFCCB1A0FFCBAF
          9DFFC8AC9AFFC6A997FFC4A694FFC2A491FFC0A28FFFBFA08EFFBE9F8CFFFAF3
          F0FFFAF2EEFF7C6651FF0000001C0000000A0000000000000000000000000000
          000300000006000000040000000100000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
          FEFFFFFEFDFFFEFDFCFFFEFBFAFFFDFAF8FFFDF8F7FFFCF7F4FFFBF5F3FFFBF4
          F0FFFAF2EEFF78614CFF0000001C0000000A0000000000000000000000000C06
          031C000000120000000F0000000500000001B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFEFFFEFDFCFFFEFCFBFFFDFBF9FFFDF9F7FFFCF8F5FFFCF6F3FFFBF5
          F1FFFBF3F0FF917C6AFF00000015000000070000000000000000000000009148
          24FF221008520000001C0000000F00000005B59E8EFFB59E8EFFB59E8EFFB59E
          8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E
          8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E8EFFB59E
          8EFFB59E8EFFB59E8EFF0000000700000002000000000000000000000000A058
          33FF914824FF2310085A0000001C0000000F0000000400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000B76E
          48FFC37044FF954B28FF2B170D52000000170000000C00000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000D087
          60FFEA9767FFD6895DFFCD6F43FF180D072D00000003000000070000000A0000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000700000002000000000000000000000000E79E
          76FFF6AD84FFDC8357FF55301D700101010E00000007000000150000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001500000007000000000000000000000000F6AD
          84FFE08B5CFF4F3324620000000600000001B59E8EFF988472FF7C6651FF7C66
          51FF7C6651FF7C6651FF7C6651FF7C6651FF7C6651FF7C6651FF7C6651FF7C66
          51FF7C6651FF7C6651FF7C6651FF7C6651FF7C6651FF7C6651FF7C6651FF7C66
          51FF7C6651FF7C6651FF0000001C0000000A000000000000000000000000E08B
          5CFF4F33245C000000050000000100000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FEFFFFFEFDFFFEFDFCFFFEFCFBFFFEFAFAFFFDFAF8FFFDF9F7FFFCF7F5FFFBF6
          F4FFFBF5F2FFFBF4F0FFFAF2EFFFFAF1EDFFF9F1EBFFF8EFEAFFF8EEE9FFF8ED
          E8FFF8ECE7FF7C6651FF0000001C0000000A0000000000000000000000003B25
          184500000001000000010000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFEFFFFFEFDFFFEFDFCFFFEFCFBFFFDFBFAFFFDFAF8FFFCF9
          F7FFFCF7F5FFFCF6F4FFFBF5F2FFFBF4F0FFFAF3EFFFF9F2EEFFF9F1EBFFF8EF
          EBFFF9EEE9FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFDBC3B4FFDAC1
          B3FFD8C0B0FFD7BEAFFFD5BCACFFD3B9A9FFD1B6A6FFCFB4A3FFCDB1A0FFCAAE
          9DFFC8AC9AFFC6A896FFC4A694FFC2A492FFC0A290FFBFA18DFFBE9F8CFFF9F0
          ECFFF9EFEAFF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFFFEFDFDFFFEFDFDFFFEFCFBFFFDFB
          FAFFFDFAF9FFFCF8F7FFFCF8F5FFFCF6F3FFFBF5F2FFFBF4F1FFFAF2EFFFFAF1
          EDFFFAF0ECFF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFEFDFFFEFDFCFFFEFB
          FBFFFDFBF9FFFDFAF8FFFCF9F7FFFCF8F5FFFCF6F4FFFBF5F2FFFBF3F1FFFAF2
          EFFFFAF1EDFF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFDBC3B4FFDAC2
          B3FFD9C0B1FFD7BEAEFFD6BBACFFD3B9A9FFD1B7A6FFCFB4A3FFCCB1A0FFCAAF
          9DFFC8AB99FFC6A897FFC4A694FFC2A491FFC1A28FFFBFA08DFFBE9F8CFFFBF4
          F1FFFAF3EFFF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFFFD
          FDFFFEFCFCFFFDFCFCFFFDFBFAFFFCFAF8FFFCF9F7FFFCF8F5FFFCF6F4FFFBF5
          F2FFFBF3F1FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FEFFFEFEFDFFFEFDFDFFFEFCFBFFFDFBFAFFFDFAF9FFFCF9F7FFFCF8F5FFFCF7
          F3FFFBF5F2FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFDBC3B4FFDAC2
          B3FFD8C0B0FFD7BEAFFFD5BCACFFD3B9A9FFD1B6A6FFCFB4A3FFCDB1A0FFCBAE
          9DFFC8AB9AFFC6A997FFC4A694FFC2A492FFC0A28FFFBFA18DFFBE9F8CFFFCF7
          F6FFFCF6F4FF7C6651FF0000001C0000000A0000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFDFCFBFFFDFBFAFFFDFAF8FFFCF8
          F7FFFCF8F5FF7C6651FF00000015000000070000000000000000000000000000
          000000000000000000000000000000000000B59E8EFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFEFDFCFFFEFBFBFFFDFBFAFFFDFA
          F8FFFDF8F7FF7C6651FF00000007000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000070000000900000009000000090000
          0009000000070000000200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000007000000150000001C0000001C0000001C0000
          001C000000150000000700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000878786FF858585FF838483FF828282FF818181FF7F7F
          7FFF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000878787FFE8E8E8FFD3D2D3FFADAEAEFF9A9A9AFF807F
          80FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000888888FFE8E8E8FFD3D2D3FFADAEAEFF9A9A9AFF8081
          80FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000898989FFE8E8E8FFD2D3D3FFAEAEADFF9A9A9AFF8181
          81FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000898A8AFFE8E8E8FFD3D3D3FFAEADADFF9A9A9AFF8182
          81FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000008A8B8AFFE8E8E8FFD3D2D3FFADADAEFF9A9A9AFF8282
          82FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000008B8B8BFFE8E8E8FFD3D3D2FFADADADFF9A9A9AFF8383
          82FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000008C8C8CFFE8E8E8FFD3D2D2FFADAEADFF9A9A9AFF8384
          84FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000008D8D8DFFE8E8E8FFD3D3D2FFAEADAEFF9A9A9AFF8584
          85FF0000001C0000000900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000008E8E8EFFE8E8E8FFD3D2D2FFADADADFF9A9A9AFF8585
          84FF0000001D0000000A00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000048F8F8EFFE8E8E8FFD3D3D2FFAEADADFF9A9A9AFF8586
          85FF000000200000001000000004000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000004161616348F8E8EFFDCDCDCFFC5C4C5FF9C9B9BFF989898FF8584
          84FF141414450000001C0000000F000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00041D1D1D3F929192FFB8B8B8FFE2E1E1FFC4C4C4FFABAAABFF9D9C9CFF8B8B
          8BFF838383FF252525620000001D000000100000000500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000052121
          2245959495FFB9B9B9FFE2E2E2FFDBDBDAFFC4C4C3FFAAAAAAFFA7A6A6FF9C9B
          9BFF999897FD808080FF2323235F0000001C0000000F00000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000005242424479998
          98FFC3C3C3FFE2E2E2FFEFEFEFFFC9C8C8FFC5C4C4FFADACACFFA6A5A6FFA4A3
          A2FF989898FF939393FE80807FFF2020205A0000001C0000000F000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001000000052626264A9B9B9BFFCBCA
          CAFFE7E6E6FFF0EFEFFFDEDEDEFFC9C8C8FFC6C5C5FFB0AFAFFFA7A7A7FFA3A2
          A1FFA09F9FFF969495FF8F8E8EFE808080FF1D1D1D550000001C0000000F0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000052929294E9F9E9FFFD3D3D3FFECEC
          ECFFEAEAEAFFF7F7F7FFCFCFCFFFC9C9C9FFC7C6C6FFB2B2B3FFA8A8A7FFA6A5
          A5FF9F9D9EFF9B9A9AFF939292FF8B8B8BFF7F7F7EFF1B1B1B520000001C0000
          000F000000040000000000000000000000000000000000000000000000000000
          00000000000000000001000000052D2D2D53A3A3A2FFD9D9D9FFF0EFEFFFE8E8
          E8FFFEFEFEFFE2E2E2FFD0CFCFFFCACACAFFC7C7C7FFB4B4B4FFA9A8A8FFA7A7
          A7FFA0A0A0FF999898FF949393FF8F8E8EFF888787FF7F7F7FFF191919500000
          001C0000000E0000000400000000000000000000000000000000000000000000
          0000000000010000000532323259A7A6A6FFDEDEDEFFF3F3F3FFEAEAEAFFFBFB
          FBFFF8F8F8FFD5D5D5FFD1D0D0FFCCCACBFFC9C9C9FFB8B7B7FFAAA9A9FFA7A7
          A7FFA3A2A2FF9A9A9AFF939293FF908F8EFF8A8989FF868585FF7E7E7EFF1616
          164B0000001C0000000E00000004000000000000000000000000000000000000
          0001000000063333335AA9A9A8FFE2E2E2FFF5F5F5FFEDEDEDFFF8F8F8FFFFFF
          FFFFE6E6E6FFD6D6D6FFD1D1D1FFCDCDCCFFCACACAFFBBBABBFFAAAAAAFFA8A8
          A7FFA4A4A3FF9D9B9BFF949494FF8D8D8DFF8A8A8AFF868585FF838282FF7E7E
          7EFF141414480000001C0000000E000000040000000000000000000000010000
          00063737375FADABACFFE6E5E6FFF6F6F6FFF0F0F0FFF5F5F5FFFFFFFFFFF8F8
          F8FFDEDDDDFFD7D7D7FFD2D2D2FFCFCDCEFFCBCACBFFBEBEBEFFACABABFFA7A7
          A7FFA3A4A3FF9E9D9DFF969696FF8F8E8EFF898888FF888787FF838282FF8281
          81FF7E7E7EFF131313450000001B0000000E0000000300000000000000043C3C
          3B65B0AFAFFFE8E8E8FFF7F7F7FFF2F2F2FFF4F4F4FFFFFFFFFFFFFFFFFFEAE9
          E9FFDEDEDEFFD8D7D6FFD3D3D3FFCFCFCEFFCCCBCCFFC1C0BFFFABABAAFFA8A7
          A7FFA3A3A3FF9F9E9FFF989797FF918F8FFF8B8A8AFF878686FF868585FF8281
          81FF818080FF7E7D7DFF101010400000001B0000000D000000034545456CB2B1
          B2FFE9E9E9FFF7F7F7FFF3F3F3FFF3F3F3FFFDFDFDFFFFFFFFFFF9F9F9FFE4E4
          E4FFDEDEDEFFD8D8D8FFD3D3D3FFD0D0CFFFCDCDCDFFC4C3C3FFACACACFFA7A7
          A7FFA4A3A3FF9E9E9EFF999898FF929191FF8C8B8BFF878787FF838282FF8483
          83FF7D7C7CFF807F7FFF7D7D7DFF0E0E0E3E0000001800000007AEADADFFF1F1
          F1FFF7F7F7FFF7F7F7FFF7F7F7FFF6F6F6FFF5F5F5FFF3F3F3FFF0F0F0FFEDED
          EDFFE8E8E8FFE5E4E4FFDDDDDDFFDBDADBFFDAD9DAFFD8D8D8FFCBCBCBFFC8C7
          C7FFC5C4C3FFC1C0C0FFBDBCBDFFB8B7B7FFB3B3B3FFB0AFAFFFAEADADFFACAB
          ABFFABABABFFABABABFF7F7E7EFF7D7D7EFF0000001500000007AEADADFFADAC
          ACFFADABABFFABAAAAFFA9A8A8FFA8A7A7FFA7A6A5FFA5A4A4FFA3A3A2FFA1A0
          A0FF9F9F9EFF9E9D9CFF9C9B9BFF999999FF979797FF969595FF939393FF9291
          91FF8F8F8FFF8D8D8DFF8B8B8BFF898989FF888787FF868685FF848484FF8283
          82FF818281FF807F80FF7F7F7EFF7E7E7EFF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010101030303030202020402020203010101030000000A0000
          00160000001C000000180000000D000000030000000000000000000000000000
          000100000005000000080000000A0000000A0000000A0000000A0000000A0000
          0009000000070000000500000007000000090000000A0000000A0000000A0000
          000900000007020202080707070F090909130707071308080816787171F87975
          74FF64615FFF1E1D1C830000000E000000030000000000000000000000000000
          0005000000110000001A0000001C0000001C0000001C0000001C0000001C0000
          001C0000001700000014000000170000001C0000001C0000001C0000001C0000
          001C010101180606061A0F0F0F26161616301A1A1A343C3839747E7979F5A4A1
          A1FF454241F2141413610000000C010101030000000100000000000000005A50
          4884988575FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF6556
          47D4352F2A6B634F3BD3806D59FF806D59FF806D59FF806D59FF806D59FF6556
          47D437312D6C6D5E4FD5AA9585FFB09D8EFFB5A295FF938786FFA39E9EFF8B88
          88FF33322FCD0C0C0C2D0606061203030306020202030000000000000003B49D
          8DFFDFD3CDFFFCF6F5FFFDF6F5FFFBF6F4FFFCF6F4FFFBF5F3FFFCF4F2FFBBAE
          A2FF806D59FFE8DCD5FFF9F0EAFFF9F0EAFFF9F0EAFFF9F0EAFFF9F2EDFFC7BB
          B2FF82705CFFE0D2CAFFEFE6DFFFF1E9E3FFEAE3DEFF938C8BFFB5B0B1FF5754
          50FF91847BFF1E1E1E3A10101023070707110202020700000001100E0D1EB49D
          8DFFFDF7F7FFFCF7F7FFFCF6F5FFFCF6F5FFFCF6F5FFFCF5F4FFFCF5F3FFFBF5
          F3FF806D59FFE8DCD5FFE8DCD5FFE8DCD5FFE8DCD5FFE8DCD5FFE8DCD5FFE8DE
          D7FF857360FFE1D4CCFFE5DAD3FFEAE0DBFFBAB1B0FFA6A0A0FF858282FF6C6A
          66FFA49280FF2F2F2F4C171717350B0B0B210303030B00000000B09A8BF9C9B9
          AEFFD8CCC0FFD8C9BFFFD7C9BEFFD7C8BEFFD7C8BEFFD6C8BDFFD5C8BEFFD5C8
          BDFFBBABA0FF806D59FF806D59FF806D59FF806E5AFF816F5BFF826F5CFF8472
          5FFF8A7967FF968776FFA79A8CFFBBB1A6FF9A9190FFB6B1B2FF545250FF9A92
          8AFFAA9E90FF988979FFC1B5A9FF080808240202020C00000000B49E8EFFFDFA
          F8FFFDF9F8FFFDF9F7FFFDF9F7FFFCF8F5FFFCF7F5FFC4B0A2FFFBF6F3FFFBF6
          F3FFFBF5F2FFFAF4F1FFFAF4F0FFFAF3F0FFC3AFA2FFF9F3EFFFF9F3EFFFFAF3
          EFFFFAF2EFFFFAF5F0FFFBF6F3FFD3C9C3FFA59D9DFF817D7DFF6F6D6BFFFBF5
          F2FFFAF2EFFFF9F0EBFF978776FF050505210000000B00000000B49D8DFFFDFA
          F9FFFCFAF8FFFDFAF7FFFDF9F7FFFCF9F6FFFCF8F5FFC4B1A2FFFBF7F4FFFBF6
          F3FFFBF6F3FFFBF5F1FFFBF4F1FFFBF4F2FFC6B2A5FFFAF3F0FFFBF4F0FFFBF4
          F0FFFAF5F0FFFBF6F2FFFCF7F4FFBAB0AEFFA49D9EFF555551FFCDCAC7FFFBF5
          F2FFF9F1EEFFF8EFEBFF816E5AFF0101011F0000000A00000000B49D8DFFFDFB
          F9FFFDFBF9FFFDFAF8FFFDFAF7FFFCF9F6FFFDF8F6FFC5B1A3FFFCF7F4FFFBF6
          F4FFFCF6F3FFFBF6F2FFFBF5F3FFFAF5F2FFC9B8AAFFFBF5F2FFFAF4F2FFFBF5
          F2FFFBF5F2FFFBF6F3FFF9F5F2FFA8A0A0FF807B7AFF767471FFFBF6F4FFFAF4
          F0FFF9F1EDFFF8EFE9FF83715EFF0101011E0000000A00000000B49D8DFFFEFB
          F9FFFEFBF9FFFDFBF9FFFCF9F8FFFDFAF8FFFCF9F7FFC5B1A2FFFBF7F5FFFCF7
          F4FFFBF6F4FFFBF6F3FFFBF6F3FFFBF6F3FFCEBEB3FFF7F3F0FFD1CCCAFFA9A4
          A0FF898481FF7B7472FF8C8785FF999290FF625E5CFFCDC9C7FFFBF5F2FFFAF3
          F0FFF9F2EDFFF9EFEAFF83705DFF0101011E0000000A00000000B49D8DFFC7B4
          A5FFC6B4A4FFC7B3A5FFC6B3A4FFC6B2A3FFC6B2A3FFCD835EFFCD835FFFCE85
          61FFCF8966FFD28E6DFFD59677FFD89F83FFCC9D87FF9A7F73FF9B8578FFB799
          89FFCEAB98FFD9B5A1FFC4A899FFB09A8EFF847D7AFFA29A95FFD5C7BEFFCFBF
          B5FFC8B6ABFFC3AEA2FF84725EFF0101011F0000000A00000000B49D8DFFFDFC
          FAFFFEFBFBFFFEFAF9FFFDFAF9FFFDF9F8FFFCF9F7FFCD835EFFFBD1BEFFFBD2
          BFFFFBD4C2FFFCD8C7FFFCDCCCFFE5CCBFFF9C8477FFD1BAABFFF4DACBFFF9E0
          D1FFFAE2D5FFFBE4D8FFFBE5D9FFE6C1AEFFEFE7E3FFB5AFABFFAEA9A6FFF6EF
          ECFFF9F2EDFFF9F0EBFF867461FF040404210000000B00000000B49D8DFFFEFC
          FBFFFDFCFAFFFEFBFAFFFDFBFAFFFDFAF9FFFDFAF8FFCD835EFFFBD2BEFFFBD3
          C0FFFBD6C5FFFCDBCBFFF1D7C9FFA99B91FFD0AE98FFF8DECCFFF5DBCAFFF5DD
          CCFFF6DECFFFF7E0D2FFF9E2D4FFE7C4AFFFFBF4EFFFFAF4F0FFAFA9A5FFBAB3
          B1FFF9F1EEFFF9F1ECFF877663FF070707230000000B00000000B49D8DFFFEFC
          FCFFFEFCFBFFFEFBFBFFFEFBFAFFFDFBF9FFFDFAF8FFCD835EFFFBD2BFFFFBD4
          C2FFFCD8C8FFFCDECFFFCDBAB0FFB9A395FFE8C6AEFFF1D5C0FFF1D7C4FFF3DA
          C7FFF5DECEFFF9E6DBFFFAEAE0FFEDD4C5FFF9F1EAFFFAF1EBFFE9DFD9FF9992
          91FFE6E0DCFFF9F1ECFF897865FF080808240202020C00000000B49D8CFFFEFC
          FCFFFEFCFCFFFDFCFBFFFDFBFAFFFDFBFAFFFDFAF8FFCD835EFFFBD2BFFFFBD6
          C3FFFCDACAFFFCE1D4FFAD9E96FFDBBFAAFFEBC9AEFFECCDB5FFEED2BBFFF0D7
          C1FFF6E4D7FFF8EADFFFF9EAE0FFF1DED0FFFAF1EBFFF9EFE7FFF7EDE5FFB0A7
          A4FFBCB6B3FFF9F2EDFF8A7866FF080808240000000B00000000B49D8DFFC8B5
          A6FFC8B5A6FFC7B5A5FFC7B4A5FFC7B4A5FFC7B3A5FFCD835EFFCF8764FFD391
          70FFD9A083FFE1B49DFF948277FFE3C1A5FFE5C0A1FFE5C2A5FFE6C3A7FFECD2
          BCFFF3E1D4FFF3E3D6FFF3E3D6FFF4E3D7FFF1E7DDFFEFE2D9FFE3D4C8FFBDAC
          9FFF8B827DFFC9B8ACFF897865FF060606220000000B00000000B39D8CFFFFFD
          FCFFFEFDFCFFFEFDFBFFFEFCFBFFFDFCFBFFFDFBFAFFCD835EFFFBD3C0FFFBD7
          C5FFFCDDCEFFFDE4D9FF8A7D71FFEAC3A3FFE2BA9AFFE7C2A3FFE8C5A7FFEFD4
          BFFFF6E5D8FFF6E8DDFFF8E9DEFFF4E4D7FFF8EDE4FFF8EEE5FFF8ECE1FFDDCA
          BAFF8B8886FFFAF2EEFF887764FF050505210000000B00000000B49D8DFFFEFE
          FDFFFFFDFCFFFEFCFCFFFEFCFCFFFEFBFBFFFDFCFBFFCD835EFFFBD3C0FFFBD6
          C5FFFCDDCEFFFDE5D9FF95877DFFE4B998FFE1B793FFE3BB9AFFE8C5A8FFECCD
          B3FFF0D6C1FFF4E2D2FFF6E9DDFFF4E5D9FFF7EDE3FFF9ECE3FFF6E8DCFFD1BF
          AFFF9C9895FFFAF2EEFF887663FF040404210000000A00000000B49D8DFFFEFE
          FDFFFEFEFDFFFFFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFCD835EFFFBD2BFFFFBD6
          C4FFFCDBCCFFFDE4D8FFAD9F95FFD9B291FFE1B38CFFE1B590FFE9C9AEFFF2DD
          CAFFF0D5BFFFEED2B9FFF1D9C4FFEFD8C5FFF3E1D1FFF4E2D3FFF1DDCCFFBEAF
          A4FFBAB5B2FFFAF2EFFF887764FF050505210000000B00000000B49D8CFFFFFE
          FEFFFEFDFDFFFFFDFDFFFEFDFDFFFEFCFCFFFEFCFBFFCD835EFFFBD2BFFFFBD5
          C3FFFCDACAFFFCE1D4FFCDBDB5FFBFA087FFE2B28AFFDEAF88FFE9C8ACFFF5E4
          D6FFF6EBE0FFF5E5D7FFF2DDCBFFEDD3BEFFEFD7C2FFF1D9C4FFE6CEB9FFA99F
          97FFE2DCDAFFFAF3EFFF887764FF060606220000000B00000000B49D8DFFC9B6
          A7FFC9B6A7FFC9B5A7FFC8B5A7FFC8B5A6FFC8B5A6FFCD835EFFCE8561FFD08B
          68FFD49575FFDBA68BFFD9B4A3FFAE9989FFD5A681FFE0AB81FFE7C5A8FFF4E2
          D2FFF6EBE0FFF8EDE4FFF8EBE2FFF6E9DFFFF6EBE1FFF1D9C5FFBFAA97FFA79D
          96FFD1C3B9FFC9B7ABFF887663FF060606220000000B00000000B49D8CFFFFFE
          FFFFFFFEFEFFFEFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFCD835EFFFBD1BEFFFBD3
          C0FFFBD6C3FFFCDACAFFFCE1D4FFE2D1C8FFB09887FFD5A780FFEAC6A6FFF4E0
          D1FFF6E9DEFFF6EADFFFF7E8DEFFF8EADEFFF0DBC7FFCAB29DFFB3ABA6FFF3EE
          ECFFFAF5F2FFFAF3F0FF867461FF040404210000000B00000000B49D8DFFFFFF
          FEFFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFCD835EFFFBD1BDFFFBD2
          BEFFFBD4C1FFFCD7C6FFFCDCCDFFFDE2D6FFD2B4A5FFB1A095FFC4A994FFE3CD
          B9FFEFDFD1FFF3E4D6FFEEDDCEFFD8C1AFFFB5A497FFBAB5AFFFF4F0EDFFFBF6
          F4FFFAF5F2FFF9F3EFFF84725FFF0101011F0000000A00000000B49D8DFFFFFE
          FFFFFFFFFEFFFFFEFEFFFEFEFEFFFFFDFDFFFFFDFDFFCD835EFFFBD1BDFFFBD1
          BEFFFBD2BFFFFBD4C2FFFCD8C7FFFCDCCDFFDFAF97FFF0DBD2FFCEBFB7FFAEA2
          9AFF958B83FF8B817AFF9C928CFFAE9B90FFDAD6D4FFF7F4F2FFFCF7F4FFFBF5
          F3FFFAF3F1FFF9F4EFFF826F5CFF0101011D0000000A00000000B49D8CFFFFFF
          FFFFFFFEFFFFFFFFFEFFFEFEFEFFFFFEFEFFFEFEFDFFCD835EFFFBD1BDFFFBD1
          BDFFFBD2BEFFFBD3BFFFFBD4C2FFFCD7C6FFD79C7FFFFCDED0FFFCE2D5FFFDE5
          D9FFFDE7DCFFFDE7DDFFFDE6DBFFE1B59EFFFCF8F7FFFCF7F5FFFCF6F4FFFAF5
          F1FFFAF5F0FFFAF3EFFF806E5AFF0000001C0000000A00000000B49D8CFFC9B6
          A7FFC9B6A7FFC9B5A7FFC8B5A7FFC8B5A6FFC8B5A6FFCD835EFFCD8058FFCD80
          58FFCD835FFFCE8460FFCF8763FFD08A67FFD28F6EFFD49576FFD79C7EFFD9A1
          85FFDBA58AFFDBA58AFFDAA388FFD89F82FFCDBCB0FFC9B7ABFFC6B3A7FFC4B1
          A2FFC1AEA0FFC0AC9EFF806D59FF0000001C0000000A00000000B49D8CFFFFFE
          FFFFFFFEFEFFFEFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFC7B4A5FFFEFCFAFFFEFC
          FBFFFDFBFAFFFDFAF9FFFDFAF8FFFDFAF7FFC8B5A7FFFCF8F8FFFCF8F7FFFCF8
          F6FFFCF7F5FFFCF7F5FFFBF7F4FFC8B6AAFFFAF6F3FFFBF5F1FFFAF3F0FFFAF2
          EFFFF9F2EEFFF9F2EEFF7E6B57FF0000001C0000000A00000000B49D8CFFFFFF
          FEFFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFC8B5A6FFFEFCFBFFFDFC
          FAFFFDFBFAFFFEFAFAFFFDFAF9FFFDFAF8FFC6B3A4FFFCF9F7FFFCF8F6FFFCF7
          F6FFFCF7F5FFFBF6F4FFFBF6F3FFC5B1A4FFFBF5F1FFFBF5F1FFFBF4F0FFFAF3
          F0FFF9F3EFFFF9F2EEFF8E7A69FF0000001C0000000A00000000B49D8CFFFFFE
          FFFFFFFFFEFFFFFEFEFFFEFEFEFFFFFDFDFFFFFDFDFFC8B5A6FFFEFDFBFFFEFC
          FBFFFDFCFBFFFDFBFAFFFDFBF9FFFDFAF8FFC6B2A4FFFCF9F7FFFCF9F7FFFCF8
          F6FFFCF7F5FFFCF7F4FFFBF7F4FFC3AFA1FFFBF5F2FFFAF5F1FFFBF4F0FFFAF3
          F0FFFAF2EFFFF9F3EEFFA49283FF0000001C0000000A00000000B49D8CFFFFFF
          FFFFFFFEFFFFFFFFFEFFFEFEFEFFFFFEFEFFFEFEFDFFC9B5A7FFFEFDFCFFFEFC
          FCFFFEFCFBFFFEFCFAFFFDFBFAFFFDFAF9FFC7B3A4FFFCF9F7FFFCF8F7FFFCF9
          F6FFFCF8F6FFFCF8F5FFFBF6F4FFC4B0A1FFFBF5F3FFFBF5F2FFFBF4F2FFFAF4
          F0FFFAF4EFFFFAF3EFFFAD9D8EFF000000150000000700000000CDBCB0FFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFFB49D8CFFB49D
          8DFFB49D8DFFB49D8DFFB49D8DFFB49D8DFFB49D8DFFB49D8DFFB49D8DFFB49D
          8DFFB49D8DFFB19D8DFFD3C5BBFF000000070000000200000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000603020D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000160D082D532F1CB1582F1CBA160C072D0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000003D23167B7B4429FF774227F65B2F1CC30704
          0210000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000005B341FBF794329FF7D462BFF180C06350000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002F1B1160703F24EE7A4328FF703D24EB000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000503020B6A3B24DE7A442AFF6C3A21E2351B0F71000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001B100A38794128FF794227FF5A2E1BC200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000E08051C552F1EB0753F26FB784227FF371C0F7700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000321E1267794328FF794327FF4A26169D0B06031900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00004D2B1B9E784126FF7A4227FF1B0E083C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002F1C
          11606E3D26E4723D25F65E3020D1110801200000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000005C36
          22C37B4223FF753A16E40C060015000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001A100A35703D
          24EB7B4122FF6F391FEB0E070320000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000704030F593321BB713A
          1AE1703F27EF5A3539F93D2B66FF01041F3D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000150D092B79432DFF8D3E
          00F2523858ED1C3CDCFF0218AFFF001193FF0205295800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000020101054527158982400DFF603E
          4EF91D3AD3FE1B39D7FF1C39DAFF0B26C1FF0B1DA2F8070D3C6200010D1F0001
          0715000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000002D1B125D753C15ED6D3100D21D3B
          D9F91B39D7FF203FDBFF2747E3FF2749E9FF172FBDFF0D1A90FF0D1A90FF0813
          6DE6030824460000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004728188F7B3E1EFF65321ADD203D
          D5FE203EDBFF2D4CE8FF2F52F4FF3B5BEBFE152EAAFE0E1D92FF0B26A4FF152E
          C3FF112596F30205142700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001D110D3C683100BD763D22FF273DCCF52645
          E0FF3354EEFF2E52F5FF4463F1FF3B56D4FF1D37B9FF152CB4FF1637D3FF193C
          DDFF1838D6FF05156AC900000206000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000056301EB07D3A00EF57384FEA1F46E9FF3053
          F3FF2F53F6FF3C5DF3FF5576EBFF2E46B6FF1632C2FF1A3DDEFF1A3CDFFF1D41
          EBFF1D44ECFF253FBCFF1B278FE81B2789DB1C2992E400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000D08051A783D0DE66A3F44FF343DAAEB2C51FAFF3356
          F7FF3457F4FF4F6CECFF4262DBFF172EABFF1230CCFF1A3DDFFF2045EFFF1F44
          EFFF1E41E8FF3A5AE9FE283DBDFF2130A4FF1F34B3FF0102388D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000402010841261A86823100DD4E418BFF204FFCFF3356F9FF2F53
          F5FF4867F1FF5A77E3FF2844C4FF1D38C9FF1C3EE1FF2045F0FF1F44EFFF1F42
          EAFF2044EBFF617EF3FF2137B2FF102BB9F8203FD4FF192EB6FC030A5AB10000
          030B000000000000000000000000000000000000000000000000000000000000
          00000000000010090520723F22EF643833E62A3ED1FC2E51F9FF2E52F5FF4664
          F3FF718EF3FF4E6BE1FF061EA5FF0928CDFF1A3DF5FF2045F1FF2044EFFF3E61
          F2FF4B6BF2FF7386E0FF1834C7F9234AF1FF456EFFFF5272FDFF2C47C3FF0511
          73DC000118390002091100000204000001020000000000000000000000000000
          00000704030F361D0C6782400DFF353EAEF4244FFAFA4063F9FF4B6CF4FF6380
          F1FF5F7DE9FF2F4FD2FF1128A8FF1F3DD7FF264AF7FF2045F0FF3A5DF1FF4F70
          F4FF6C82E5FF283CB8FF0C33EAFE4B6BF4FE6A82E9FE718AEDFF5374EFFF213B
          C2FF0E1D92FF06177DF1020D3F7D020A31610000000000000000000000000000
          0000321D13685D310FB67E3B07FA1C38DFFF3866FFFF5C78F2FF4664EDFF2C4C
          DBFF1032C3FF0E2AB7FF2C42B8FF5370E7FF3558F2FF3255F0FF5071F4FF5A79
          F3FF5E6ECFFF192DA4F1253CB0F6304BCAFF3B52C2FF374CB8FF364AB3F83044
          A7EC132065950003172E00010811000209130000000000000000000000000000
          000069381FDF6D371BE857323CE60D2CDFFF092EDDFF303E9AD5040823340103
          1016000000010204131E3548AAF34A5CCCFF6483FBFF5475F5FF6783F6FF4C5E
          C0FF2A3B90D000000102000001020407131C0306151E03040F160203090E0001
          0305000000000000000000000000000000000000000000000000000000000000
          0000522A14B2733B25FF452F58E907166C9D00041A2300000000000000000000
          000000000000000000000001070B0A123F59344AB4F03E57C0FF3B4DAEF6141B
          4160000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000251308514423159748315CEF030518290000000000000000000000000000
          00000000000000000000000000000000000001020C140D122A3D02030C150000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000020000
          00070000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001500000007000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B39F90FFAD9D
          8EFF988575FF8B7966FF857260FF83705DFF816D5AFF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49F91FFFCFA
          F9FFFCFAF8FFFBF9F7FFFCF9F7FFFBF8F6FFFBF8F5FFFBF7F4FFFBF6F4FFFBF6
          F3FFFAF5F2FFFAF4F1FFF9F4F0FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49F91FFFCFA
          F9FFFCFAF8FFFBF9F7FFFCF9F7FFFBF8F6FFFBF8F5FFFBF7F4FFFBF6F4FFFBF6
          F3FFFAF5F2FFFAF4F1FFF9F4F0FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B59F92FFFDFA
          F9FFFCFAF8FFFCF9F7FFFBF9F7FFFCF8F6FFFCF7F6FFFBF8F5FFFBF7F4FFFBF6
          F3FFFBF5F2FFFAF5F1FFFAF4F1FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B5A092FFFCFB
          F9FFFDFAF8FFFCFAF8FFFCF9F8FFFBF9F7FFFCF8F6FFFBF8F5FFFBF7F5FFFBF7
          F4FFFAF6F3FFFAF6F2FFFAF5F1FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B6A194FFFDFA
          FAFFFDFAF9FFFCFAF9FFFCFAF8FFFCF9F7FFFCF9F6FFFCF8F6FFFBF8F6FFFBF7
          F5FFFAF6F3FFFAF5F2FFFBF5F2FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B7A394FFC9B6
          A7FFC8B6A7FFC8B5A6FFC7B4A5FFC7B4A5FFC6B3A4FFC6B3A4FFC5B2A3FFC5B2
          A3FFC3B1A1FFC3B0A1FFC2AFA0FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B8A497FFFDFB
          FAFFFDFBFAFFFDFAF9FFFCFAF9FFFCFAF8FFFCF9F7FFFCF9F7FFFCF8F6FFFCF8
          F5FFFBF6F4FFFBF6F3FFFAF6F3FF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B8A497FFFDFB
          FAFFFDFBFAFFFDFAF9FFFCFAF9FFFCFAF8FFFCF9F7FFFCF9F7FFFCF8F6FFFCF8
          F5FFFBF6F4FFFBF6F3FFFAF6F3FF8B7966FF0000001F000000110000000A0000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000700000002B9A597FFFDFB
          FBFFFDFBFAFFFCFBF9FFFCFBF9FFFDFAF8FFFCF9F8FFFCF9F7FFFCF8F7FFFCF8
          F6FFFBF7F4FFFBF7F4FFFBF6F3FF8B7966FF000000240000001F0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001500000007BAA799FFFDFC
          FBFFFDFCFBFFFDFCFAFFFDFAFAFFFCFBF9FFFCFAF8FFFCF9F8FFFCF8F7FFFCF8
          F6FFFBF7F5FFFBF7F5FFFBF6F3FF8B7966FF816D5AFF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000001C0000000ABBA99AFFFDFC
          FCFFFDFCFBFFFDFCFBFFFDFBFAFFFDFAF9FFFCFAF9FFFDFAF8FFFCF9F8FFFCF9
          F7FFFCF8F6FFFBF7F4FFFBF7F4FF8C7A67FFFDFDFDFFC6B2A3FFFBF6F4FFFBF6
          F3FFFAF6F3FFFAF6F2FFFAF5F1FFFAF4F0FFBFAB9CFFF9F2EDFFF9F1EDFFF9F1
          ECFFF8F0ECFFF8F0EBFFF8F0EAFF806D59FF0000001C0000000ABDAA9BFFCBB7
          A9FFCAB8A9FFC9B7A8FFC9B6A7FFC8B6A6FFC8B5A6FFC8B5A5FFC7B4A5FFC7B4
          A5FFC7B3A4FFC7B5A7FFC8B7A9FF968675FFFDFDFDFFCDBCAEFFFBF8F5FFFBF8
          F5FFFBF6F3FFFBF5F2FFFAF5F2FFFAF5F1FFC0AB9CFFF9F2EEFFF9F1EDFFF8F1
          ECFFF8F0ECFFF8F0EBFFF8F0EBFF806D59FF0000001C0000000ABDAB9DFFFEFD
          FCFFFDFDFBFFFDFCFBFFFDFBFBFFFCFBFAFFFDFAFAFFFCFAF9FFFCF9F9FFFCF9
          F7FFFBFAF7FFFCF9F7FFFCFAF8FFB9AEA3FFFEFDFDFFE0D6CEFFFDFAF9FFFCFA
          F8FFFCF8F5FFFAF7F4FFFBF6F2FFFAF5F2FFC0AC9DFFF9F3EEFFF9F2EDFFF9F1
          EDFFF9F1ECFFF8F1EBFFF8F0EBFF806D59FF0000001C0000000ABDAB9DFFFEFD
          FCFFFDFDFBFFFDFCFBFFFDFBFBFFFCFBFAFFFDFAFAFFFCFAF9FFFCF9F9FFFCFA
          F8FFFCFAF8FFF5EAE4FFE0BFAFFFBE8061FFB26037FFB26037FFC17F5DFFDAB3
          9FFFF3E7E1FFFCF9F7FFFBF7F5FFFAF5F2FFC0AE9FFFF9F3EEFFF9F2EDFFF9F1
          EDFFF9F1ECFFF9F0ECFFF8F1EBFF806D59FF0000001C0000000ABEAD9FFFFDFD
          FDFFFDFCFCFFFEFDFBFFFDFCFBFFFDFBFAFFFDFBFAFFFCFAF9FFFCFAF9FFFCFB
          F9FFE4C6B7FFB5643CFFAD5427FFAC5328FFDE946AFFE9A278FFDD9368FFC879
          4FFFB1643DFFD6B7A6FFD8CAC0FFC8B7AAFFC1AD9FFFC0AC9DFFC0AB9CFFBFAB
          9CFFBEAB9BFFBEAA9BFFBEA99AFF806D59FF0000001C0000000AC0AE9FFFFDFD
          FCFFFDFDFCFFFEFDFCFFFDFCFBFFFDFBFAFFFDFBFAFFFDFBFAFFFCFAF9FFF4E9
          E2FFEEDCD2FFFBF7F5FFEEDDD4FFCBA08BFFBF7B5BFFD6885DFFFFB78FFFEFA5
          7BFFEEA67BFFA9552CFFF2E5DFFFFCF7F5FFC4B1A3FFFAF4EFFFFAF3EEFFFAF2
          EEFFF9F2EDFFF8F1EDFFF9F1ECFF806D59FF0000001C0000000AC1B0A0FFFEFE
          FDFFFDFDFCFFFDFDFCFFFDFCFCFFFDFCFBFFFDFBFBFFFCFBFAFFFCFBF9FFFCFA
          F9FFFCFBF8FFFCFAF7FFFDFAF8FFCDC4BBFFF1E3DDFFC68E72FFD6865BFFFFBA
          95FFF0A177FFD17F56FFCD9E86FFFCF9F7FFC7B5A8FFFAF4F0FFF9F3EFFFFAF2
          EEFFF9F2EEFFF9F1EDFFF9F1EDFF806D59FF0000001C0000000AC2B1A1FFC3B2
          A3FFC3B2A3FFC3B1A2FFC2B1A2FFC2B0A2FFC1B0A1FFC1B0A0FFC0B0A1FFC1AF
          A1FFC0AFA1FFC0AFA1FFC2B1A3FFC4B3A6FFFEFDFDFFE3D1C7FFA9542AFFFFBA
          95FFF09D72FFF09E72FFA7552CFFFDFBF9FFCAB9ABFFFAF4F0FFFAF3F0FFFAF3
          EFFFFAF3EEFFF9F2EDFFF9F1ECFF806D59FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000C5B5A6FFFEFE
          FEFFFEFDFEFFFEFEFEFFFEFEFDFFFDFDFDFFFEFEFEFFEBE4DFFFA9542BFFFFB8
          93FFF19B6FFFF19B6FFFA7552CFFFDFCFBFFD2C4B8FFFBF5F3FFFAF5F2FFF9F3
          EFFFFAF2EFFFF9F2EEFFF9F2EDFF806D59FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000C4B3A5FFCAB8
          AAFFC9B8A9FFCAB9ABFFD1C2B6FFDBCFC6FFE7DFD8FFF2EEEBFFA8542BFFFFC4
          A1FFF5AE84FFF5AE84FFAA5932FFF2EDE9FFE4DAD3FFD7CBC2FFCCBBB0FFC3B1
          A2FFC0AD9FFFC0AC9CFFBFAB9CFF806D59FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000C5B5A6FFFEFE
          FEFFFEFDFEFFFEFEFEFFF3E9E3FFA8562BFFA9542AFFA8542BFFFFC19FFFF4AB
          81FFF5AC82FFF4AB81FFEA9C73FFA6552DFFA25935FFB67655FFF3E8E1FFFAF5
          F1FFF9F3EFFFF9F3EFFFF9F2EEFF806D59FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000C7B8A9FFFEFD
          FEFFFEFEFDFFFEFEFEFFFEFEFEFFE8D5CCFF9E5D3CFFFFC19FFFF4A87EFFF3A8
          7EFFF4A97EFFF4A87EFFF4A97EFFEA9C73FFAE7252FFEBDAD0FFFBF8F5FFFAF5
          F2FFF9F4F0FFF9F3EFFFF9F2EFFF806D59FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000C7B8A9FFFEFD
          FEFFFEFEFDFFFEFEFDFFFEFDFDFFFEFDFDFFE7D7CFFFA96A4EFFF6B594FFF3A4
          7AFFF3A479FFF3A479FFEA9C73FFAD704FFFDBC8BCFFFCF9F7FFFBF6F3FFFAF4
          F1FFFAF4F1FFF9F3F0FFF9F3EFFF8B7966FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000C5B5A6FFFEFE
          FEFFFEFDFEFFFEFEFEFFFEFEFDFFFDFDFDFFFEFEFEFFDDCABEFFAD7254FFF6B5
          94FFF2A175FFF6B594FFAE6F4FFFE9DAD1FFDACEC5FFFCF7F5FFFAF5F3FFFAF5
          F2FFFAF4F1FFFAF4F0FFF9F4EFFF988575FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000DC8D63FFD985
          58FFD88052FFD77D4EFFD77C4DFFD77C4DFFD67A4BFFD57949FFE09B77FFAC71
          54FFF2AC87FFAD6E4EFFDE9671FFD1703DFFD06E3BFFD06D3AFFD06B38FFCF6B
          37FFCF6935FFCF6833FFCE6732FFCE6732FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000DC8D63FFF9AF
          8AFFF9AE86FFF9AC84FFF7AB84FFF7AA82FFF6A981FFD57949FFFFC5A7FFFFD4
          BDFFA36A4DFFFFD3BDFFFFC3A4FFFFC2A3FFD47645FFF4A076FFF3A075FFF29F
          73FFF29E72FFF29D72FFF29D71FFCE6732FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000DC8D63FFF9AE
          87FFF8AD86FFF8AC85FFF8AB83FFF8AA82FFF7A980FFD57949FFFFC5A8FFFFC5
          A7FFFFD3BDFFFFC4A4FFFFC3A4FFFFC2A4FFD47645FFF3A076FFF3A075FFF39F
          74FFF29F73FFF29D72FFF19D71FFCE6732FF0000001C0000000A000000000000
          0000000000000000000000000000000000000000000000000000DC8D63FFF9AE
          87FFF9AD86FFF8AC85FFF8AB83FFF8AA83FFF7AA81FFD57949FFFFC6A8FFFFC5
          A7FFFFC4A6FFFFC3A6FFFFC3A4FFFFC2A3FFD47645FFF4A176FFF3A075FFF39F
          74FFF39E73FFF29D73FFF29D71FFCE6732FF0000001500000007000000000000
          0000000000000000000000000000000000000000000000000000DC8D63FFDC8D
          63FFDC8B61FFDB8A5EFFDA885DFFD9865AFFD88457FFD88355FFD88053FFD77F
          50FFD67D4EFFD57C4BFFD57949FFD47847FFD47645FFD37542FFD37340FFD271
          3EFFD2703CFFD1703AFFD06E39FFCE6732FF0000000700000002}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001409041E4B2412707F3D
          1CBF9E4D24EC9F4D24ED73371AAC3E1E0E5C1108031A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000341A0D4F984B23E6C8794FFFDD93
          68FFE9A278FFDE946AFFAC5328FFAD5427FF9B4A22E6371A0C51000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000150A0521A7532AFEFFB78FFFEFA57BFFEEA6
          7BFFD6885DFF823F1EC34C2411711F0F072F040100061F0F072F1309041C0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000005E2F1890D17F56FFF0A177FFF0A177FFD686
          5BFF6B341AA31A0D062800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A7552CFFFFBA95FFF09D72FFF09E72FFA954
          2AFF1A0D06280000000000000000000000000000000000000000000000001F10
          0830994F29ED4423136A00000000000000000000000000000000000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009A7552CFFFFB893FFF19B6FFFF19B6FFFA954
          2BFF0000000900000009000000090000000900000009000000091D0F0734964D
          29EBEEA87EFFA5552DFF3C1F1064000000070000000200000000000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001CA0512AF8FFC4A1FFF5AE84FFF5AE84FFA854
          2BFF0000001C0000001C0000001C0000001C0000001C190D063F964C27EAEEA2
          79FFF4AA81FFF4AC81FFA5552DFF4020117000000007000000002498ECFF008E
          EBFF008DEBFF008CEAFF008BEAFF008AEAFF0089E9FF0088E9FF0087E9FF0086
          E8FFAC775EFFAE6039FFA6552DFFFFC19FFFF4AB81FFF5AC82FFF4AB81FFF5AC
          81FFA8542BFFA9542AFFA6552CFF007BE4FF1576CDFF95593FFFEC9E74FFF4A7
          7CFFF4A67CFFF4A77CFFF4A77DFFA5552DFF341B0E57000000002599ECFFA8EA
          FFFFA8E9FFFFA7E9FFFFA7E9FFFFA6E8FFFFA6E8FFFF3FA5EFFFA8EAFFFFBCEE
          FFFFD2DBDAFFC98360FFFFBF9CFFF4A97EFFF4A87EFFF4A97EFFF3A87EFFF4A8
          7EFFE99D73FFB26B47FFDEF7FFFFB8DBF8FFB6724DFFFFB48DFFFFBE9AFFF3A2
          78FFF4A377FFF3A278FFF3A377FFF09F74FF9F522AF622120936279BEDFFAEEC
          FFFFADEBFFFFACEBFFFFABEAFFFFABEBFFFFAAEAFFFF3FA7EFFFAEECFFFFB7ED
          FFFFCAF2FFFFD8CFC9FFCD855EFFFFBE9CFFF3A479FFF3A479FFF3A47AFFEC9E
          74FFC37C5BFFDAE3E4FFD4F4FFFFBE7652FFC07550FFBD7450FFDD8B62FFFFBA
          98FFF39F74FFF39F73FFD68359FFA6552DFFA6552CFF964E28EA279DEDFFAFED
          FFFFAFECFFFFAEECFFFFAEECFFFFADEBFFFFADEBFFFF3FA7EFFFAFEDFFFFB2ED
          FFFFBBEFFFFFCFF4FFFFD6C7BCFFCF855FFFFFBB99FFF2A175FFFFB790FFCA86
          64FFDBE4E4FFCAF2FFFFC1F0FFFF84C1F3FFDAF7FFFFAFECFFFFBA6E48FFFFB9
          96FFF39D71FFF19B6FFFA25532FF0000001C0000000900000000299EEDFFB2ED
          FFFFB1EDFFFFB0EDFFFFB0ECFFFFAFEBFFFFAEECFFFF3FA9F0FFB2EDFFFFB2ED
          FFFFB4EEFFFFBEEFFFFFD1F3FFFFD8C9BFFFD0845DFFFFB58EFFC98663FFDCE1
          E0FFCBF2FFFFB9EEFFFFB4EDFFFF59ACEFFFCAF3FFFFB1EDFFFFBB6F4BFFFFC5
          A3FFF5AE84FFF5AF84FFA7552CFF0000001C0000000900000000299EEEFFB3EE
          FFFFB3EEFFFFB2EDFFFFB2EDFFFFB1EDFFFFB0ECFFFF3FA9F0FFB3EEFFFFB3EE
          FFFFB3EDFFFFB6EEFFFFBEF0FFFFD0F4FFFFBBB2B4FFC07E5DFFD9DFDEFFCDF3
          FFFFBDF0FFFFB6EEFFFFB6EDFFFF5CADEFFFCFF4FFFFB5DBE4FFC67A53FFFFC1
          A0FFF4A980FFF4AA81FFA7552CFF0000001C000000090000000029A0EDFFB6EE
          FFFFB5EFFFFFB5EEFFFFB4EEFFFFB3EDFFFFB3EDFFFF3FA9F0FFB6EEFFFFB5EF
          FFFFB5EEFFFFB5EEFFFFB7EEFFFFBEF0FFFF75BEF3FFCEF4FFFFC8F3FFFFC2F1
          FFFFBEF0FFFFC0F0FFFFC6F1FFFF8DC6F4FFDFE4E3FFC6A18CFFDD8C62FFFFBF
          9BFFF3A47AFFD38258FF5E657CFF0000001C00000009000000002AA0EEFFB7EF
          FFFFB8EFFFFFB7EFFFFFB6EFFFFFB5EEFFFFB4EEFFFF3FABF1FFB7EFFFFFB8EF
          FFFFB7EFFFFFB6EFFFFFB6EEFFFFB6EFFFFF4BABF0FFBDF0FFFFBFF1FFFFC2E3
          EBFFC9DCDEFFD4F2FBFFD9DFDEFFC7B2ADFFD39677FFE08C63FFFFBA98FFF3A0
          74FFEC996EFFB35F36FF1677CCFF0000001C00000009000000002BA1EEFFB9F0
          FFFFBAF0FFFFB8F0FFFFB8EFFFFFB7EFFFFFB7EFFFFF3FABF1FFB9F0FFFFBAF0
          FFFFB8F0FFFFB8EFFFFFB7EFFFFFB7EFFFFF41A6EFFFBAF0FFFFBDF1FFFFC3F2
          FFFFCDCBC6FFC6805EFFCC7C53FFD8855DFFFFB189FFFFB590FFE38D62FFCD78
          4EFFB46A44FFB3BFBDFF007CE4FF0000001C00000009000000002CA2EFFFBCF1
          FFFFBCF1FFFFBBF1FFFFBAF1FFFFB9F0FFFFB9F0FFFF3FABF1FFBCF1FFFFBCF1
          FFFFBBF1FFFFBAF1FFFFB9F0FFFFB9F0FFFF3FA7EFFFBCF1FFFFBEF1FFFFC1F2
          FFFFCAF4FFFFD5E9EDFFD8CABEFFC79985FFC67E59FFC27E5AFFC0947AFFBDBB
          B2FFBAE2EAFFB9F0FFFF007CE4FF0000001C00000009000000002CA3EEFFBEF2
          FFFFBDF1FFFFBCF2FFFFBCF1FFFFBCF1FFFFBBF1FFFF3FABF1FFBEF2FFFFBDF1
          FFFFBCF2FFFFBCF1FFFFBCF1FFFFBBF1FFFF3FA7EFFFBEF2FFFFBEF1FFFFBEF2
          FFFFC2F2FFFFC8F4FFFFCFF5FFFF8BC6F5FFDCF8FFFFBDF1FFFFBCF2FFFFBCF1
          FFFFBCF1FFFFBBF1FFFF007DE4FF0000001C00000009000000002DA3EEFFC0F3
          FFFFBFF3FFFFBFF2FFFFBEF2FFFFBDF2FFFFBCF1FFFF3FADF1FFC0F3FFFFBFF3
          FFFFBFF2FFFFBEF2FFFFBDF2FFFFBCF1FFFF3FA7EFFFC0F3FFFFBFF3FFFFC0F2
          FFFFBFF2FFFFC0F3FFFFC1F2FFFF52ABEFFFC8F4FFFFBFF3FFFFBFF2FFFFBEF2
          FFFFBDF2FFFFBCF1FFFF007DE5FF0000001C00000009000000002EA4EFFFC1F3
          FFFFC0F3FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FADF1FFC1F3FFFFC0F3
          FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FA7EFFFC1F3FFFFC0F3FFFFC0F3
          FFFFC0F3FFFFBEF2FFFFBFF2FFFF41A4EEFFC2F3FFFFC0F3FFFFC0F3FFFFC0F3
          FFFFBEF2FFFFBEF2FFFF007EE5FF0000001C00000009000000002EA4EFFFC1F3
          FFFFC0F3FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FADF1FFC1F3FFFFC0F3
          FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FA7EFFFC1F3FFFFC0F3FFFFC0F3
          FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FA3EEFFC1F3FFFFC0F3FFFFC0F3FFFFC0F3
          FFFFBEF2FFFFBEF2FFFF007EE5FF0000001C00000009000000002EA4EFFFC1F3
          FFFFC0F3FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FADF1FFC1F3FFFFC0F3
          FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FA7EFFFC1F3FFFFC0F3FFFFC0F3
          FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FA3EEFFC1F3FFFFC0F3FFFFC0F3FFFFC0F3
          FFFFBEF2FFFFBEF2FFFF007EE5FF0000001C00000009000000002EA4EFFFC1F3
          FFFFC0F3FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FADF1FFC1F3FFFFC0F3
          FFFFC0F3FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FA7EFFFC1F3FFFFC0F3FFFFC0F3
          FFFFC0F3FFFFBEF2FFFFBEF2FFFF3FA3EEFFC1F3FFFFC0F3FFFFC0F3FFFFC0F3
          FFFFBEF2FFFFBEF2FFFF007EE5FF0000001C00000009000000002EA5F0FFC2F4
          FFFFC2F3FFFFC1F4FFFFC1F3FFFFC1F3FFFFC0F3FFFF3FAEF1FFC2F4FFFFC2F3
          FFFFC1F4FFFFC1F3FFFFC1F3FFFFC0F3FFFF3FA9F0FFC2F4FFFFC2F3FFFFC1F4
          FFFFC1F3FFFFC1F3FFFFC0F3FFFF3FA3EEFFC2F4FFFFC2F3FFFFC1F4FFFFC1F3
          FFFFC1F3FFFFC0F3FFFF007FE6FF0000001C00000009000000002FA5EFFFC3F4
          FFFFC3F4FFFFC3F4FFFFC2F3FFFFC2F3FFFFC2F3FFFF3FADF2FFC3F4FFFFC3F4
          FFFFC3F4FFFFC2F3FFFFC2F3FFFFC2F3FFFF3FA9F0FFC3F4FFFFC3F4FFFFC3F4
          FFFFC2F3FFFFC2F3FFFFC2F3FFFF3FA5EEFFC3F4FFFFC3F4FFFFC3F4FFFFC2F3
          FFFFC2F3FFFFC2F3FFFF007FE6FF0000001C000000090000000030A6EFFF0099
          EFFF0098F0FF0098EFFF0098EFFF0096EFFF0096EEFF0095EEFF0094EEFF0093
          EDFF0092EEFF0092EDFF0090EDFF008FECFF008EEBFF008EECFF008DEBFF008B
          EAFF008AEAFF0089E9FF0088E9FF0087E9FF0086E8FF0085E8FF0085E8FF0083
          E7FF0083E7FF0082E7FF007EE5FF0000001C000000090000000030A7EFFF84E3
          FEFF83E3FEFF81E2FEFF80E1FEFF7FE0FEFF7DE0FEFF7ADEFEFF78DDFEFF77DC
          FEFF74DBFEFF74DAFEFF71D9FEFF70D8FEFF6CD6FEFF6BD5FEFF69D4FEFF68D4
          FEFF67D4FEFF66D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3
          FEFF66D3FEFF66D3FEFF007EE5FF0000001C000000090000000031A8EFFF85E4
          FEFF84E3FEFF82E3FEFF81E1FEFF80E1FEFF7EDFFEFF7CDFFEFF7ADEFEFF78DD
          FEFF76DBFEFF74DBFEFF72DAFEFF70D8FEFF6DD7FEFF6CD6FEFF6AD5FEFF69D4
          FEFF67D4FEFF67D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3
          FEFF66D3FEFF66D3FEFF007EE5FF0000001C000000090000000031A9EFFF86E5
          FEFF85E4FEFF83E3FEFF82E2FEFF81E2FEFF7EE1FEFF7DE0FEFF7BDFFEFF79DE
          FEFF78DCFEFF75DBFEFF74DBFEFF72DAFEFF6FD7FEFF6DD6FEFF6BD6FEFF6AD5
          FEFF68D4FEFF67D4FEFF66D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3FEFF66D3
          FEFF66D3FEFF66D3FEFF007EE5FF00000015000000070000000032A8F0FF31A8
          F0FF30A7EFFF30A6F0FF2FA5EFFF2EA4EFFF2CA3EFFF2BA1EFFF2BA1EEFF299F
          EEFF289EEDFF279DEEFF279BEDFF269AEDFF2398ECFF2296ECFF2195ECFF2093
          ECFF1E93EBFF1D91EBFF1C90EBFF1A8DEAFF198CEAFF188BE9FF1789E9FF1688
          E9FF1587E9FF1485E9FF1384E8FF000000070000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000003C436FFF3B426EFF3A416DFF39406CFF383F
          6BFF363D6AFF353C69FF343B68FF333A67FF323966FF313865FF303764FF2F36
          63FF2F3662FF2E3561FF2D3460FF2C335FFF2B325EFF2B325EFF2A315DFF2930
          5CFF29305CFF0000000000000000000000000000000000000000000000000000
          000000000000000000003E4571FF737BA4FFA0AFE7FF93A4E4FF93A4E4FF93A4
          E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FF93A4
          E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FF93A4E4FFA0AF
          E7FF676F99FF29305CFF00000000000000000000000000000000000000000000
          000000000000000000003E4572FF9BA9E4FF657BD6FF5E75D4FF5E75D4FF5E75
          D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF5E75
          D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF5E75D4FF657B
          D6FF9BA9E4FF29305CFF00000000000000000000000000000000000000000000
          000000000000000000003F4673FF8796DCFF556ACEFF556ACEFF556ACEFF556A
          CEFF556ACEFF556ACEFF556ACEFF556ACEFF556ACEFF556ACEFF556ACEFF556A
          CEFF556ACEFF556ACEFF556ACEFF556ACEFF556ACEFF556ACEFF556ACEFF556A
          CEFF8796DCFF29305CFF00000000000000000000000000000000000000000000
          00000000000000000000404774FF818FD8FF4C60C8FF4C60C8FF4C60C8FF4C60
          C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60
          C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60C8FF4C60
          C8FF818FD8FF2A315DFF00000000000000000000000000000000000000000000
          00000000000000000000414874FF7A88D5FF4356C3FF4356C3FF4356C3FF4356
          C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356
          C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356C3FF4356
          C3FF7A88D5FF2A315DFF00000000000000000000000000000000000000000000
          00000000000000000000414875FF7582D3FF3B4EC0FF3B4EC0FF3B4EC0FF3B4E
          C0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4E
          C0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4EC0FF3B4E
          C0FF7582D3FF2B325EFF00000000000000000000000000000000000000000000
          00000000000000000000424976FF6D7BD3FF3044C0FF3044C0FF3044C0FF3044
          C0FF3044C0FFFFFFFFFFFFFFFFFFFDFDFDFF9AA3E0FF3044C0FF3044C0FF3044
          C0FF9AA3E0FFFFFFFFFFFFFFFFFFFFFFFFFF3044C0FF3044C0FF3044C0FF3044
          C0FF6D7BD3FF2B325EFF00000000000000000000000000000000000000000000
          00000000000000000000434A76FF6574D3FF253AC0FF253AC0FF253AC0FF253A
          C0FF253AC0FF253AC0FFFFFFFFFFFFFFFFFFFFFFFFFF949EE0FF253AC0FF949E
          E0FFFFFFFFFFFFFFFFFFFFFFFFFF253AC0FF253AC0FF253AC0FF253AC0FF253A
          C0FF6574D3FF2C335FFF00000000000000000000000000000000000000000000
          00000000000000000000434A77FF5E6ED3FF1B32C0FF1B32C0FF1B32C0FF1B32
          C0FF1B32C0FF1B32C0FF1B32C0FFFFFFFFFFFFFFFFFFFFFFFFFF8F9BE0FFFFFF
          FFFFFFFFFFFFFFFFFFFF1B32C0FF1B32C0FF1B32C0FF1B32C0FF1B32C0FF1B32
          C0FF5E6ED3FF2C3360FF00000000000000000000000000000000000000000000
          00000000000000000000444B78FF5B6CD3FF172FC0FF172FC0FF172FC0FF172F
          C0FF172FC0FF172FC0FF172FC0FF172FC0FFFDFDFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF172FC0FF172FC0FF172FC0FF172FC0FF172FC0FF172FC0FF172F
          C0FF5B6CD3FF2D3460FF00000000000000000000000000000000000000000000
          00000000000000000000454C78FF8592E1FF495CC9FF4A5CCAFF4B5DCBFF4C5F
          CCFF4D60CFFF4E61D0FF4F62D1FF5163D3FF5164D4FFFFFFFFFFFFFFFFFFFFFF
          FFFF5164D4FF5164D3FF5062D1FF4E61D0FF4E60CFFF4C5FCDFF4B5DCBFF4A5C
          CAFF8592E1FF2E3561FF00000000000000000000000000000000000000000000
          00000000000000000000454C79FFA1ACECFF6D7CD6FF6E7DD8FF6F7ED8FF7180
          DBFF7282DCFF7484DEFF7686E0FF7787E1FFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
          FDFFFFFFFFFF7888E2FF7686E0FF7484DEFF7382DDFF7181DBFF6F7ED8FF6E7D
          D8FFA1ACECFF2E3562FF00000000000000000000000000000000000000000000
          00000000000000000000464D7AFFA6B1EDFF7382D8FF7482D9FF7584DAFF7786
          DCFF7987DEFF7B8AE0FF7D8CE2FFFFFFFFFFFFFFFFFFFFFFFFFF8190E6FFFFFF
          FFFFFFFFFFFFFFFFFFFF7D8CE2FF7B8AE0FF7988DEFF7786DDFF7584DAFF7482
          D9FFA6B1EDFF2F3662FF00000000000000000000000000000000000000000000
          00000000000000000000464D7AFFABB5F0FF7987DCFF7A88DDFF7C89DEFF7D8B
          E0FF7F8DE2FF8290E4FFFFFFFFFFFFFFFFFFFFFFFFFF8896EAFF8896EAFF8896
          EAFFFFFFFFFFFFFFFFFFFFFFFFFF8290E4FF7F8DE2FF7E8BE0FF7C89DEFF7A88
          DDFFABB5F0FF303763FF00000000000000000000000000000000000000000000
          00000000000000000000474E7BFFAFB9F2FF7E8CDFFF808EDFFF818FE1FF8290
          E2FF8593E4FFFFFFFFFFFFFFFFFFFFFFFFFF8D9BECFF8D9BEDFF8E9CEDFF8D9B
          EDFF8D9BECFFFFFFFFFFFFFFFFFFFFFFFFFF8593E5FF8391E3FF818FE1FF808E
          DFFFAFB9F2FF303764FF00000000000000000000000000000000000000000000
          00000000000000000000474E7BFFB4BDF4FF8491E0FF8693E1FF8794E3FF8996
          E4FF8C99E6FF8E9BE9FF909DEAFF919EECFF94A1EEFF94A1EFFF95A2EFFF94A1
          EFFF94A1EEFF929FEDFF919EEBFF8E9BE9FF8C99E7FF8996E5FF8794E3FF8693
          E1FFB4BDF4FF313864FF00000000000000000000000000000000000000000000
          00000000000000000000484F7CFFB8C1F6FF8A96E3FF8B97E4FF8D99E5FF8F9B
          E7FF919DE9FF939FECFF95A1EDFF97A3EFFF9AA6F1FF9AA6F2FF9BA7F2FF9AA6
          F2FF9AA6F1FF98A4F0FF96A2EEFF94A0ECFF929EE9FF8F9BE7FF8D99E5FF8B97
          E4FFB8C1F6FF323965FF00000000000000000000000000000000000000000000
          00000000000000000000484F7CFFBCC4F8FF8E9AE5FF8F9BE7FF919DE8FF939F
          E9FF96A2ECFF98A4EEFF9AA6F0FF9CA8F1FF9FABF4FF9FABF4FFA0ACF5FF9FAB
          F4FF9FABF4FF9DA9F2FF9BA7F1FF99A5EFFF96A2ECFF94A0EAFF919DE8FF8F9B
          E7FFBCC4F8FF323966FF00000000000000000000000000000000000000000000
          0000000000000000000049507DFFBFC7F9FF929EE7FF949FE9FF96A1EAFF98A3
          EBFF9AA5EEFF9DA8F0FF9FAAF2FFA1ACF3FFA4AFF6FFA4AFF6FFA5B0F7FFA4AF
          F6FFA4AFF6FFA2ADF4FFA0ABF3FF9DA8F1FF9BA6EEFF98A3ECFF96A1EAFF949F
          E9FFBFC7F9FF333A67FF00000000000000000000000000000000000000000000
          0000000000000000000049507DFFC3CAFBFF97A1EAFF98A3EBFF9AA4ECFF9CA6
          EEFF9FA9F1FFA1ACF3FFA4AEF5FFA6B0F6FFA9B3F9FFA9B3F9FFAAB4FAFFA9B3
          F9FFA9B3F9FFA7B1F7FFA5AFF6FFA2ACF4FF9FAAF1FF9DA7EEFF9AA4ECFF98A3
          EBFFC3CAFBFF343B67FF00000000000000000000000000000000000000000000
          000000000000000000004A517EFFCCD3FDFF9DA8EBFF9BA6EDFF9CA8EEFF9FAA
          F0FFA1ADF3FFA4AFF5FFA7B2F7FFA9B4F8FFABB7FBFFACB7FBFFADB8FCFFACB7
          FBFFACB7FBFFAAB5F9FFA7B3F8FFA5B0F5FFA2ADF3FF9FAAF0FF9CA8EEFF9EA9
          EDFFCCD3FDFF353C68FF00000000000000000000000000000000000000000000
          000000000000000000004A517EFF878DB2FFCED5FEFFC8CFFEFFC8CFFEFFC8CF
          FEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CF
          FEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFC8CFFEFFCED5
          FEFF7C82A7FF353C69FF00000000000000000000000000000000000000000000
          00000000000000000000000000004A517EFF49507DFF49507DFF484F7CFF474E
          7BFF474E7BFF464D7AFF454C79FF444B78FF434A77FF434A76FF424975FF4148
          74FF404773FF3F4672FF3E4571FF3D4470FF3C436FFF3A416EFF39406DFF383F
          6CFF373E6BFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000200000007000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000007000000020000000000000000000000000000
          00000000000000000000000000000000000000000007000000150000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C00000015000000070000000000000000000000000000
          00000000000000000000000000000000000099887AFFA29181FF917D6CFF816C
          58FF76614CFF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF0000001C000000090000000000000000000000000000
          00000000000000000000000000000000000099887AFFF9F2EDFFF9F2EDFFF9F1
          ECFFF9F0ECFFF9EFEBFFF9EFEBFFF9EFEAFFF9EFEAFFF8EEE9FFF8EEE9FFF8EE
          E9FFF8EEE9FFF7EEE8FFF7EEE8FFF8EDE7FFF8EDE7FFF7EDE7FFF7EDE7FFF7ED
          E7FFF7EDE7FF806D59FF0000001C000000090000000000000000000000000000
          00000000000000000000000000000000000099887AFFFAF3F0FFFAF3F0FFFAF2
          EEFFFAF1EDFFF9F1ECFFF9F1ECFFF8F0EBFFF8F0EBFFF8EFEAFFF8EFEAFFF8EE
          EAFFF8EEEAFFF8EEE9FFF8EEE9FFF8EDE8FFF8EDE8FFF7EDE8FFF7EDE8FFF7ED
          E8FFF7EDE8FF806D59FF0000001C000000090000000000000000000000000000
          00000000000000000000000000000000000099887AFFFDF9F7FFF6EEEAFFD1B6
          A8FFB58B75FFB68C77FFB9917DFFB7907CFFB68F7AFFB58E78FFB58C76FFB48B
          76FFB28A74FFB28973FFAF866EFFA87C63FFBE9A87FFE6D3CAFFFFF8F4FFFBF1
          ECFFF8EDE8FF806D59FF0000001C000000090000000000000000000000000000
          00000000000000000000000000000000000099887AFFFBF5F2FFFBF5F2FFFAF3
          EFFFFAF2EEFFF9F1EDFFF9F1EDFFF9F1ECFFF9F1ECFFF9F1EBFFF9F1EBFFF8F0
          EBFFF8EFEBFFF8EFEAFFF8EFEAFFF8EEE9FFF8EEE9FFF8EEE8FFF8EEE8FFF8EE
          E8FFF8EEE8FF806D59FF0000001C000000090000000000000000000000000000
          00010000000500000008000000090000000999887AFFFAF4F0FFF3EAE4FFD1B6
          A8FFB78E79FFB88F7AFFBB947FFFBA937EFFB9927CFFB8917CFFB8907BFFB78F
          7AFFB68D78FFB58C77FFB28872FFAB7E67FFBF9C8AFFE6D5CBFFFFF8F5FFFBF2
          EDFFF8EEE9FF806D59FF0000001C000000090000000000000000000000010000
          0007000000120000001A0000001C0000001C99887AFFFBF8F6FFFBF8F5FFFAF6
          F2FFFAF3EFFFF9F2EFFFF9F2EFFFFAF2EEFFFAF2EEFFF9F2EDFFF9F2ECFFF9F1
          ECFFF9F0ECFFF8F0EBFFF8F0EBFFF8EFEAFFF8EFEAFFF8EFE9FFF8EFE9FFF8EF
          E9FFF8EFE9FF806D59FF0000001C0000000900000000000000000402020A5C42
          4392A55B6CFFA96270FFA45E6CFF934556FF99887AFFFCF7F5FFF5ECE8FFD3B9
          ABFFB9907BFFBA927DFFBD9783FFBC9682FFBC9580FFBB947FFFBA937DFFB992
          7DFFB8917CFFB7907BFFB48C76FFAD836BFFC1A08DFFE8D6CDFFFFF9F6FFFCF3
          EEFFF9EFEAFF806D59FF0000001C000000090000000000000000402F2F65B281
          81FFAF6D79FFB8717FFFB7707DFFAA6472FF99887AFFFBF7F2FFFBF7F2FFFBF6
          F1FFFBF5F1FFFBF4F0FFFBF4F0FFFAF3F0FFFAF3F0FFFAF3F0FFFAF3EFFFF9F2
          EEFFF9F2EDFFF9F1EDFFF9F1EDFFF9F0EDFFF9F0EDFFF9F0ECFFF9F0ECFFF9F0
          EBFFF9F0EBFF826F5CFF0000001C000000090000000000000000AE6A7AFFAF6D
          79FFBA7481FFB97380FFB7737FFFAA6472FF99887AFFFCF7F4FFFCF7F4FFFBF6
          F3FFFBF5F2FFFAF4F2FFFAF4F2FFFBF4F1FFFBF4F1FFFAF5F2FFFAF5F2FFFBF5
          F2FFFBF5F2FFFBF5F2FFFBF5F2FFFBF5F1FFFBF5F1FFFAF4F0FFFAF3F0FFFAF2
          EFFFFAF2EFFF8B7A68FF0000001C000000090000000000000000AE6A7AFFC38D
          95FFBA7682FFBA7481FFB97480FFAA6472FF99887AFFFCF5F2FFFCF5F2FFFBF6
          F3FFFBF6F4FFFBF7F4FFFBF7F4FFFCF8F5FFFCF9F7FFFDFAF8FFFDFAF9FFFDFB
          FAFFFDFBFAFFFEFCFBFFFEFCFBFFFDFCFAFFFDFCFAFFFDFBF9FFFDFAF8FFFCF9
          F7FFFCF9F7FFB2A69AFF0000001E0000000C0000000100000000AE6A7AFFC48E
          96FFBA7783FFBA7682FFB97481FFAA6472FF99887AFFFCF7F6FFFCF7F6FFFCF7
          F6FFFBF9F8FFFBF9F8FFFBF9F8FFFBF8F7FFFCF8F7FFFCF7F6FFFCF7F6FFFCF7
          F6FFFCF9F7FFFDFBF9FFE7CABAFFBD7B59FFA55125FFA14B20FFA04920FFA250
          26FFBE8264FFD3B6A7FF00000021000000140000000600000000AE6A7AFFC58F
          96FFBB7784FFBA7784FFBA7582FFAA6472FF99887AFFFCF7F4FFFCF7F4FFFCF7
          F4FFFCF7F8FFFCF7F8FFFCF7F8FFFCF7F7FFFCF7F6FFFCF7F5FFFCF7F4FFFCF7
          F4FFFDFBFAFFDDB29BFFBA663DFFCB7B55FFDD8D67FFE69570FFE69772FFDE8F
          6AFFCB7D57FFAF623AFF49200C9C0000001C0000000D00000002AE6A7AFFC58F
          98FFBC7886FFBB7785FFBB7783FFAA6472FF99887AFFFDFBF9FFFDFBF9FFFDFB
          F9FFFCF9F5FFFCF9F5FFFCF9F5FFFCFAF6FFFDFAF7FFFDFBF8FFFDFBF9FFFDFB
          F9FFEACBBDFFBD693FFFD78460FFD98661FFC4744DFFAC5C35FFAC5C35FFC574
          4FFFDC8A66FFD88A65FF8A4626F6180A04490000001400000005AE6A7AFFC490
          98FFBC7986FFBD7985FFBC7784FFAA6472FF99887AFFFEFCFCFFFEFCFCFFFEFC
          FCFFFDFBF8FFFDFBF8FFFDFBF8FFFDFBF9FFFEFCFAFFFEFCFBFFFEFCFCFFFEFC
          FCFFD29272FFCB7650FFD8825AFFC26B43FFBB7351FFE2BEADFFE3C0AEFFBD77
          52FFC16C46FFD88560FFC67753FF562911A40000001A00000008AE6A7AFFC791
          98FFBD7A87FFBD7987FFBC7985FFBB7785FF99887AFFFEFDFDFFFEFDFDFFFEFD
          FDFFFDFBFAFFFDFBFAFFFDFBFAFFFDFCFBFFFEFCFCFFFEFDFDFFFEFDFDFFFEFD
          FDFFB96034FFD8845EFFC36E47FFB66F4BFFFEFDFDFFFEFCFAFFFDFBFAFFFEFD
          FDFFBD7753FFC06F4AFFD3835FFF9C5230F80000001C00000009AE6A7AFFC692
          9AFFBE7B88FFBE7B87FFBC7986FFBC7886FF99887AFF99887AFF99887AFF9988
          7AFF99887AFF99887AFF99887AFF99887AFF99887AFF99887AFFA69B93FFF0EB
          E7FFB15728FFDE8962FFB15D35FFD9B5A2FFE9E1DCFFDED3CDFFE5DCD6FFEFEA
          E6FFE0BCAAFFAA5A33FFD78561FFA75B38FE0000001C00000009AE6A7AFFC692
          9AFFBE7B88FFBE7B87FFBC7986FFBC7886FFBB7785FFBB7784FFBA7682FFB974
          81FFB97480FFB87380FFB8717EFFB76F7DFFB76D7CFFB97280FFC9919CFFECD8
          DCFFAB5E38FFF9A97EFFB9673DFFD9B2A1FFDDC0C6FF07030225A85B34FF0000
          001949261479B2623CFFF2A57EFFB96D48FE0000001A00000008AE6A7AFFC693
          9BFFBE7E89FFBC7884FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E
          70FFA75E70FFA75E70FFA75E70FFA75E70FFA75E70FFAA6374FFBA818FFFE3CC
          D2FFAE643EFFF09D75FFDB8960FFB36B47FFE6D2D6FF0000001CAD5F39FF7942
          27B39C5430DAD3835CFFE89973FFA35A37F70000001500000006AE6A7AFFC794
          9CFFBF7F8AFFAF6F7BFFEBE1E1FFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFEFDFFFDFEFDFFFDFDFCFFFCFCFCFFFBFCFBFFFCFCFCFFFDFD
          FDFFCB957AFFD98960FFF9A57DFFAE5F37FFEBDADEFF0000001CAC5D36FFCC7C
          53FFD47F56FEF5A47EFFB36E4DFF5D2C15A90000000E00000002AE6A7AFFC895
          9DFFC0808BFFAF6F7BFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFEFDFFFDFEFDFFFDFDFCFFFCFCFCFFFBFCFBFFFBFBFBFFFCFC
          FCFFEDD7CCFFBC7652FFE29067FFB56740FFE6D2D7FF0000001CB0613AFFFCAB
          82FFF8A47EFFE89770FF924D2AF31C0C05480000000E00000003AE6A7AFFC998
          9FFFC2838EFFAF6F7BFFFEFFFEFFCECFCFFFCECFCFFFCECFCFFFCECFCFFFCECF
          CFFFCECFCFFFCECFCFFFCECFCFFFCECFCFFFCECFCFFFCECFCFFFCECFCFFFD4D4
          D4FFFEFEFEFFD7AC9DFFB8724FFFE4C5C0FFD8B7BEFF0000001CB2633BFFFCAB
          82FFFCAB82FFDB885FFF7B3A19D8000000190000001000000005AE6A7AFFCA98
          A0FFC2838EFFAF6F7BFFFEFFFEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFBFFFBFB
          FBFFFBFBFAFFFAFAFAFFFAFAF9FFF9F9F9FFF9F9F9FFF9F9F8FFF8F9F8FFF8F8
          F8FFFDFDFDFFCA9DA8FFDBB6BDFFD1A2ABFFC3929CFF0000001CB4643CFFB263
          3CFFB1623BFFB0613AFFAE603AFFAD6039FF0000000700000002AE6A7AFFCB98
          A0FFC2838EFFAF6F7BFFFEFFFEFFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2
          D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFD2D2D2FFCFD0
          D0FFFDFEFDFFAF6D7DFFC58994FFBD7C89FFB17280FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000AE6A7AFFCB9B
          A1FFC2838EFFAF6F7BFFFEFFFEFFFDFDFDFFFDFDFCFFFCFDFCFFFCFCFCFFFCFC
          FBFFFBFCFBFFFBFBFBFFFBFBFAFFFAFAFAFFFAFAF9FFF9F9F9FFF9F9F9FFF9F9
          F9FFFEFEFEFFA86172FFBF7C88FFB8727FFFA96573FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000AE6A7AFFCB9B
          A1FFC2838EFFAF6F7BFFFEFFFEFFD4D4D3FFD4D4D3FFD4D4D3FFD4D4D3FFD4D4
          D3FFD4D4D3FFD4D4D3FFD4D4D3FFD4D4D3FFD4D4D3FFD4D4D3FFD4D4D3FFD3D2
          D3FFFEFFFEFFA75E70FFB38088FFB8717FFFA7606EFF0000001C000000090000
          0000000000000000000000000000000000000000000000000000AE6A7AFFCB9B
          A1FFC2838EFFAF6F7BFFFEFFFEFFFEFEFDFFFEFEFDFFFDFDFCFFFDFDFCFFFCFD
          FCFFFCFCFCFFFCFCFBFFFCFCFBFFFBFBFAFFFBFBFAFFFAFAF9FFF9F9F9FFF9F9
          F9FFFEFFFEFFA75E70FF996E75FF9E5967FFA9606EFF0000001C000000090000
          0000000000000000000000000000000000000000000000000000AE6A7AFFCB9B
          A1FFC2838EFFAF6F7BFFFEFFFEFFD6D5D6FFD6D5D6FFD6D5D6FFD6D5D6FFD6D5
          D6FFD6D5D6FFD6D5D6FFD6D5D6FFD6D5D6FFD6D5D6FFD6D5D6FFD6D5D6FFD6D5
          D6FFFEFFFEFFA75E70FFB0757FFF9E5A68FFAA6471FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000AE6A7AFFCA96
          9EFFC2838EFFAF6F7BFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFFFEFF
          FEFFFEFFFEFFA75E70FFC77F8BFFC77F8BFFAA6572FF00000015000000070000
          0000000000000000000000000000000000000000000000000000AE6A7AFFAE6A
          7AFFAE6A7AFFAE6A7AFFC5C3C3FFC5C3C3FFC5C3C3FFC5C3C3FFC5C3C3FFC5C3
          C3FFC5C3C3FFC5C3C3FFC5C3C3FFC5C3C3FFC5C3C3FFC5C3C3FFC5C3C3FFC5C3
          C3FFC5C3C3FFA75E70FFA75E70FFA75E70FFA75E70FF00000007000000020000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EA8060FFBB6944DF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EE8362FFEF8361FFED8260FFBD6843DE000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EF8260FFEF8260FFF08260FFEF8260FFED815EFFC169
          44E0000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F0815EFFEF815EFFEF815EFFF0825EFFF0815EFFF081
          5EFFEE805EFFC46942E000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EF815CFFEF815CFFEF815CFFEF815CFFEF815CFFEF81
          5CFFEF805DFFEF805CFFEE805BFFC66840DF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EF7F5AFFF07F5BFFEF7F5BFFEF805AFFF07F5AFFEF80
          5BFFEF805BFFF0805AFFF0805AFFEF805AFFEE7E5AFFC6653EDD000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EF7E58FFEF7F58FFEF7F58FFF07F58FFF07E58FFEF7F
          58FFEF7F58FFF07F58FFF07F59FFF07F58FFEF7F58FFEF7F59FFEE7E57FFC866
          3DDF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EF7D56FFEF7E56FFEF7E56FFF07E56FFF07D56FFEF7E
          56FFEF7E56FFEF7D56FFF07D56FFEF7D56FFF07D56FFF07E55FFEF7D56FFEF7D
          56FFF07C55FFC6643ADB00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EF7C54FFF07D53FFF07D54FFF07D54FFF07C53FFEF7C
          54FFF07D54FFEF7D53FFF07C53FFEF7C54FFF07C53FFF07D53FFF07D53FFF07D
          54FFEF7D53FFEF7C53FFEF7D52FFCC663BE00000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EF7B51FFEF7B52FFF07B51FFEF7C51FFEF7B51FFF07B
          51FFEF7C52FFF07C51FFF07B51FFEF7C51FFF07B51FFF07B51FFF07B51FFEF7B
          51FFF07B51FFF07B52FFF07B51FFEF7C51FFF07C51FFC86237DB000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F07A4FFFF07A4EFFF07A4FFFF07A4FFFF07A4EFFF07A
          4FFFF07A4FFFF07A4EFFF07A4EFFEF7A4EFFF07A4EFFEF7A4EFFF07A4FFFEF7A
          4EFFF07A4FFFF07A4EFFEF7A4FFFF07A4EFFF07B4FFFF07A4EFFF07B4EFFC962
          36DB000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF079
          4CFFF0794CFFF0794CFFF0794CFFF0784CFFF0794CFFF0794CFFF0794CFFF079
          4CFFF0794DFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF079
          4CFFF0794BFFC86033D900000000000000000000000000000000000000000000
          00000000000000000000F5A383FFF5A383FFF5A383FFF5A383FFF5A383FFF5A2
          83FFF5A283FFF5A383FFF5A383FFF5A383FFF5A383FFF5A383FFF5A283FFF5A3
          83FFF5A383FFF5A383FFF5A383FFF5A383FFF5A283FFF5A383FFF5A383FFF5A3
          83FFF5A383FFF49F7FFE00000000000000000000000000000000000000000000
          00000000000000000000F6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF5A8
          88FE2D1B13310000000000000000000000000000000000000000000000000000
          00000000000000000000F7B296FFF7B296FFF7B297FFF7B296FFF7B296FFF7B2
          97FFF7B296FFF7B296FFF7B297FFF7B296FFF7B296FFF7B296FFF7B296FFF7B2
          97FFF7B296FFF7B296FFF7B296FFF7B296FFF7B296FFF6AF93FE311E15350000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BA
          A0FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA0FFF8BAA1FFF8BAA1FFF8BA
          A1FFF8BAA1FFF8BAA1FFF8BAA1FFF7B99FFF3320173700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F9C2ABFFF8C2ABFFF9C2ABFFF8C2ABFFF9C2ABFFF9C2
          ABFFF9C2ABFFF8C2ABFFF8C2ABFFF8C2ABFFF8C2ABFFF8C2ABFFF9C2ABFFF8C2
          ABFFF9C2ABFFF8C0A9FF38241A3C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F9CAB6FFFACAB6FFFACAB6FFF9CAB6FFFACAB6FFF9CA
          B6FFF9CAB6FFF9CAB6FFF9CAB6FFF9CAB6FFFACAB6FFF9CAB6FFFACAB6FFF9C9
          B4FF6644336C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FBD2C1FFFBD2C1FFFBD2C1FFFAD2C1FFFBD2C1FFFAD2
          C1FFFBD2C1FFFAD2C1FFFAD2C1FFFBD2C1FFFAD2C1FFFAD1BFFF6947376F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FBDACCFFFBDACCFFFBDACCFFFBDACCFFFBDACCFFFBDA
          CCFFFBDACCFFFBDACCFFFBDACCFFFBD9CBFF7451407A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2
          D6FFFCE2D6FFFCE1D5FF7652417B000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE8
          DFFF7854447D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FEF0EAFFFEF0EAFFFEF0EAFFFDEEE8FF7A57467F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FEF6F2FFFEF5F1FFA47A65AA00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000805D4D85000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EF825FFFEF82
          60FFEF8260FFEF8260FF00000000000000000000000000000000000000000000
          00000000000000000000EF825FFFEF8260FFF08260FFEF8160FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000F0815FFFEF815EFFF082
          5FFFEF825EFFEF815EFFEF825FFF000000000000000000000000000000000000
          000000000000EF825FFFEF825EFFEF815FFFEF815EFFEF815EFFEF825EFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000EF805DFFF0815DFFEF815DFFF081
          5DFFF0805DFFEF815DFFF0805DFFEF815DFF0000000000000000000000000000
          0000EF805DFFEF805DFFF0805DFFEF805DFFEF815CFFEF815DFFEF815DFFF080
          5DFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F0805BFFF07F5BFFEF805CFFF080
          5BFFEF805BFFEF805CFFEF7F5BFFEF805BFF0000000000000000000000000000
          0000F0805BFFEF805BFFF0805BFFEF805BFFEF805BFFEF805BFFF0805BFFEF80
          5BFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000EF7F59FFEF7F5AFFEF7F5AFFEF7F
          59FFEF7F5AFFEF7F59FFEF7F59FFEF7F5AFF0000000000000000000000000000
          0000F07F5AFFEF7F59FFEF7F59FFF07F59FFEF7F5AFFEF7F59FFEF7F5AFFEF7F
          59FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000EF7E58FFF07E58FFEF7E57FFEF7E
          57FFF07E57FFEF7E58FFEF7E57FFF07E57FF0000000000000000000000000000
          0000F07E58FFEF7E58FFF07E57FFF07E58FFF07E58FFEF7E58FFEF7E58FFEF7E
          58FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000EF7D55FFEF7E55FFEF7D56FFEF7D
          56FFEF7D55FFEF7D56FFF07D55FFEF7D56FF0000000000000000000000000000
          0000EF7D55FFF07D55FFEF7E56FFEF7D55FFF07D55FFEF7D55FFEF7D55FFEF7D
          55FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000EF7C54FFF07D54FFEF7C54FFF07C
          54FFF07D54FFF07D54FFEF7C53FFEF7C54FF0000000000000000000000000000
          0000F07C53FFF07C54FFF07C54FFF07C54FFF07C54FFEF7C53FFF07C54FFF07C
          53FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F07B52FFF07B51FFEF7B51FFEF7B
          51FFF07B51FFEF7C51FFF07B52FFF07B51FF0000000000000000000000000000
          0000EF7B51FFEF7B51FFF07B51FFF07B51FFEF7C51FFF07B51FFEF7B51FFEF7C
          51FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F07A4FFFF07A4FFFF07A4FFFF07A
          4FFFF07A4FFFF07A4FFFEF7A4FFFF07A4FFF0000000000000000000000000000
          0000F07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A
          4FFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F0794DFFF0794DFFF0794DFFF079
          4CFFF0794DFFF0794DFFEF794DFFEF794DFF0000000000000000000000000000
          0000F0794CFFF0794DFFF0794DFFF0794DFFF0794DFFF0794DFFF07A4CFFEF79
          4DFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F0784BFFF0784BFFF0784BFFF078
          4AFFF0784BFFF0784AFFF0784AFFF0784AFF0000000000000000000000000000
          0000F0784BFFF0784AFFF0784AFFF0784AFFF0784BFFF0784BFFF0784AFFF078
          4AFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F49A77FFF49A77FFF49A77FFF49A
          78FFF49A77FFF49A78FFF49A78FFF49A78FF0000000000000000000000000000
          0000F49977FFF49A77FFF49A77FFF49A77FFF49A77FFF49A77FFF49A77FFF49A
          77FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F5A281FFF5A281FFF5A281FFF5A2
          81FFF5A281FFF5A281FFF5A281FFF5A281FF0000000000000000000000000000
          0000F5A281FFF5A281FFF5A281FFF5A281FFF5A181FFF5A281FFF5A281FFF5A2
          81FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F6A98AFFF6A98AFFF6A98AFFF6A9
          8AFFF6A98AFFF6A98AFFF6A98AFFF6A98AFF0000000000000000000000000000
          0000F6A98AFFF6A98AFFF6A88AFFF6A98AFFF6A98AFFF6A98AFFF6A98AFFF6A9
          8AFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F7B195FFF7B195FFF7B195FFF7B0
          94FFF7B095FFF7B094FFF7B195FFF7B194FF0000000000000000000000000000
          0000F7B195FFF7B095FFF7B195FFF7B195FFF7B194FFF7B195FFF7B195FFF7B1
          95FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F8B9A0FFF8B9A0FFF8B99FFFF8B9
          A0FFF8B9A0FFF8B9A0FFF8B99FFFF8B9A0FF0000000000000000000000000000
          0000F8B99FFFF8B99FFFF8B9A0FFF8B99FFFF8B9A0FFF8B99FFFF8B9A0FFF8B9
          A0FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000F9C1AAFFF9C1AAFFF8C1AAFFF8C1
          AAFFF9C1AAFFF9C1AAFFF9C1AAFFF8C1AAFF0000000000000000000000000000
          0000F9C1AAFFF9C1AAFFF9C1AAFFF9C1AAFFF8C1AAFFF9C1AAFFF8C1AAFFF8C1
          AAFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FACAB5FFF9CAB5FFF9CAB6FFFACA
          B5FFF9CAB5FFFACAB6FFF9CAB6FFFACAB6FF0000000000000000000000000000
          0000FACAB6FFFACAB5FFFACAB6FFFACAB6FFF9CAB6FFF9CAB6FFF9CAB6FFFACA
          B5FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FAD2C0FFFBD2C0FFFBD2C0FFFAD2
          C0FFFBD2C0FFFBD2C0FFFBD2C0FFFAD2C0FF0000000000000000000000000000
          0000FBD2C0FFFAD2C0FFFBD2C0FFFAD2C0FFFBD2C0FFFBD2C0FFFAD2C0FFFAD2
          C0FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FBDACBFFFBDACBFFFBDACBFFFBDA
          CBFFFBDACCFFFBDACBFFFBDACBFFFBDACBFF0000000000000000000000000000
          0000FBDACBFFFBDACBFFFBDACBFFFBDACBFFFBDACBFFFBDACBFFFBDACBFFFBDA
          CBFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FCE2D6FFFCE2D6FFFCE2D6FFFCE1
          D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FF0000000000000000000000000000
          0000FCE2D6FFFCE1D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE1D6FFFCE1D6FFFCE1
          D6FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FDE9E0FFFDE9E0FFFDE9E0FFFDE9
          E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FF0000000000000000000000000000
          0000FDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9
          E0FF000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FEF0EAFFFEF0EAFFFEF0
          EAFFFEF0EAFFFEF0EAFFFEF0EAFF000000000000000000000000000000000000
          000000000000FEF0EAFFFEF0EAFFFEF0EAFFFEF0EAFFFEF0EAFFFEF0EAFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FEF6F2FFFEF6
          F2FFFEF6F2FFFEF6F2FF00000000000000000000000000000000000000000000
          00000000000000000000FEF6F2FFFEF6F2FFFEF6F2FFFEF6F2FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000EA8060FFEA8060FFEA8060FFEA8060FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000BB6944DFEA8060FF00000000000000000000
          000000000000EE8362FFEE8362FFEE8362FFEE8362FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000BD6843DEED8260FFEF8361FFEE8362FF00000000000000000000
          000000000000EF8260FFEF8260FFEF8260FFEF8260FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000C169
          44E0ED815EFFEF8260FFF08260FFEF8260FFEF8260FF00000000000000000000
          000000000000F0815EFFF0815EFFF0815EFFF0815EFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000C46942E0EE805EFFF081
          5EFFF0815EFFF0825EFFEF815EFFEF815EFFF0815EFF00000000000000000000
          000000000000EF815CFFEF815CFFEF815CFFEF815CFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C66840DFEE805BFFEF805CFFEF805DFFEF81
          5CFFEF815CFFEF815CFFEF815CFFEF815CFFEF815CFF00000000000000000000
          000000000000EF7F5AFFEF7F5AFFEF7F5AFFEF7F5AFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000C6653EDDEE7E5AFFEF805AFFF0805AFFF0805AFFEF805BFFEF80
          5BFFF07F5AFFEF805AFFEF7F5BFFF07F5BFFEF7F5AFF00000000000000000000
          000000000000EF7E58FFEF7E58FFEF7E58FFEF7E58FF00000000000000000000
          000000000000000000000000000000000000000000000000000000000000C866
          3DDFEE7E57FFEF7F59FFEF7F58FFF07F58FFF07F59FFF07F58FFEF7F58FFEF7F
          58FFF07E58FFF07F58FFEF7F58FFEF7F58FFEF7E58FF00000000000000000000
          000000000000EF7D56FFEF7D56FFEF7D56FFEF7D56FF00000000000000000000
          00000000000000000000000000000000000000000000C6643ADBF07C55FFEF7D
          56FFEF7D56FFF07E55FFF07D56FFEF7D56FFF07D56FFEF7D56FFEF7E56FFEF7E
          56FFF07D56FFF07E56FFEF7E56FFEF7E56FFEF7D56FF00000000000000000000
          000000000000EF7C54FFEF7C54FFEF7C54FFEF7C54FF00000000000000000000
          0000000000000000000000000000CC663BE0EF7D52FFEF7C53FFEF7D53FFF07D
          54FFF07D53FFF07D53FFF07C53FFEF7C54FFF07C53FFEF7D53FFF07D54FFEF7C
          54FFF07C53FFF07D54FFF07D54FFF07D53FFEF7C54FF00000000000000000000
          000000000000EF7B51FFEF7B51FFEF7B51FFEF7B51FF00000000000000000000
          000000000000C86237DBF07C51FFEF7C51FFF07B51FFF07B52FFF07B51FFEF7B
          51FFF07B51FFF07B51FFF07B51FFEF7C51FFF07B51FFF07C51FFEF7C52FFF07B
          51FFEF7B51FFEF7C51FFF07B51FFEF7B52FFEF7B51FF00000000000000000000
          000000000000F07A4FFFF07A4FFFF07A4FFFF07A4FFF0000000000000000C962
          36DBF07B4EFFF07A4EFFF07B4FFFF07A4EFFEF7A4FFFF07A4EFFF07A4FFFEF7A
          4EFFF07A4FFFEF7A4EFFF07A4EFFEF7A4EFFF07A4EFFF07A4EFFF07A4FFFF07A
          4FFFF07A4EFFF07A4FFFF07A4FFFF07A4EFFF07A4FFF00000000000000000000
          000000000000F0794CFFF0794CFFF0794CFFF0794CFFC86033D9F0794BFFF079
          4CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794DFFF079
          4CFFF0794CFFF0794CFFF0794CFFF0784CFFF0794CFFF0794CFFF0794CFFF079
          4CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFF00000000000000000000
          000000000000F5A383FFF5A383FFF5A383FFF5A383FFF49F7FFEF5A383FFF5A3
          83FFF5A383FFF5A383FFF5A283FFF5A383FFF5A383FFF5A383FFF5A383FFF5A3
          83FFF5A283FFF5A383FFF5A383FFF5A383FFF5A383FFF5A383FFF5A283FFF5A2
          83FFF5A383FFF5A383FFF5A383FFF5A383FFF5A383FF00000000000000000000
          000000000000F6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFF000000002D1B1331F5A8
          88FEF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFF00000000000000000000
          000000000000F7B296FFF7B296FFF7B296FFF7B296FF00000000000000000000
          0000311E1535F6AF93FEF7B296FFF7B296FFF7B296FFF7B296FFF7B296FFF7B2
          97FFF7B296FFF7B296FFF7B296FFF7B296FFF7B297FFF7B296FFF7B296FFF7B2
          97FFF7B296FFF7B296FFF7B297FFF7B296FFF7B296FF00000000000000000000
          000000000000F8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FF00000000000000000000
          0000000000000000000033201737F7B99FFFF8BAA1FFF8BAA1FFF8BAA1FFF8BA
          A1FFF8BAA1FFF8BAA1FFF8BAA0FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BA
          A0FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FF00000000000000000000
          000000000000F9C2ABFFF9C2ABFFF9C2ABFFF9C2ABFF00000000000000000000
          00000000000000000000000000000000000038241A3CF8C0A9FFF9C2ABFFF8C2
          ABFFF9C2ABFFF8C2ABFFF8C2ABFFF8C2ABFFF8C2ABFFF8C2ABFFF9C2ABFFF9C2
          ABFFF9C2ABFFF8C2ABFFF9C2ABFFF8C2ABFFF9C2ABFF00000000000000000000
          000000000000F9CAB6FFF9CAB6FFF9CAB6FFF9CAB6FF00000000000000000000
          00000000000000000000000000000000000000000000000000006644336CF9C9
          B4FFFACAB6FFF9CAB6FFFACAB6FFF9CAB6FFF9CAB6FFF9CAB6FFF9CAB6FFF9CA
          B6FFFACAB6FFF9CAB6FFFACAB6FFFACAB6FFF9CAB6FF00000000000000000000
          000000000000FBD2C1FFFBD2C1FFFBD2C1FFFBD2C1FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00006947376FFAD1BFFFFAD2C1FFFBD2C1FFFAD2C1FFFAD2C1FFFBD2C1FFFAD2
          C1FFFBD2C1FFFAD2C1FFFBD2C1FFFBD2C1FFFBD2C1FF00000000000000000000
          000000000000FBDACCFFFBDACCFFFBDACCFFFBDACCFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007451407AFBD9CBFFFBDACCFFFBDACCFFFBDACCFFFBDA
          CCFFFBDACCFFFBDACCFFFBDACCFFFBDACCFFFBDACCFF00000000000000000000
          000000000000FCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007652417BFCE1D5FFFCE2D6FFFCE2
          D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FF00000000000000000000
          000000000000FDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007854447DFDE8
          DFFFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FF00000000000000000000
          000000000000FEF0EAFFFEF0EAFFFEF0EAFFFEF0EAFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00007A57467FFDEEE8FFFEF0EAFFFEF0EAFFFEF0EAFF00000000000000000000
          000000000000FEF6F2FFFEF6F2FFFEF6F2FFFEF6F2FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A47A65AAFEF5F1FFFEF6F2FF00000000000000000000
          000000000000805D4D85805D4D85805D4D85805D4D8500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000805D4D8500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000EA80
          60FFBB6944DF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000EA80
          60FFEA8060FFEA8060FFEA8060FF00000000000000000000000000000000EE83
          62FFEF8361FFED8260FFBD6843DE000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000EE83
          62FFEE8362FFEE8362FFEE8362FF00000000000000000000000000000000EF82
          60FFEF8260FFF08260FFEF8260FFED815EFFC16944E000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000EF82
          60FFEF8260FFEF8260FFEF8260FF00000000000000000000000000000000F081
          5EFFEF815EFFEF815EFFF0825EFFF0815EFFF0815EFFEE805EFFC46942E00000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000F081
          5EFFF0815EFFF0815EFFF0815EFF00000000000000000000000000000000EF81
          5CFFEF815CFFEF815CFFEF815CFFEF815CFFEF815CFFEF805DFFEF805CFFEE80
          5BFFC66840DF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000EF81
          5CFFEF815CFFEF815CFFEF815CFF00000000000000000000000000000000EF7F
          5AFFF07F5BFFEF7F5BFFEF805AFFF07F5AFFEF805BFFEF805BFFF0805AFFF080
          5AFFEF805AFFEE7E5AFFC6653EDD000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000EF7F
          5AFFEF7F5AFFEF7F5AFFEF7F5AFF00000000000000000000000000000000EF7E
          58FFEF7F58FFEF7F58FFF07F58FFF07E58FFEF7F58FFEF7F58FFF07F58FFF07F
          59FFF07F58FFEF7F58FFEF7F59FFEE7E57FFC8663DDF00000000000000000000
          000000000000000000000000000000000000000000000000000000000000EF7E
          58FFEF7E58FFEF7E58FFEF7E58FF00000000000000000000000000000000EF7D
          56FFEF7E56FFEF7E56FFF07E56FFF07D56FFEF7E56FFEF7E56FFEF7D56FFF07D
          56FFEF7D56FFF07D56FFF07E55FFEF7D56FFEF7D56FFF07C55FFC6643ADB0000
          000000000000000000000000000000000000000000000000000000000000EF7D
          56FFEF7D56FFEF7D56FFEF7D56FF00000000000000000000000000000000EF7C
          54FFF07D53FFF07D54FFF07D54FFF07C53FFEF7C54FFF07D54FFEF7D53FFF07C
          53FFEF7C54FFF07C53FFF07D53FFF07D53FFF07D54FFEF7D53FFEF7C53FFEF7D
          52FFCC663BE0000000000000000000000000000000000000000000000000EF7C
          54FFEF7C54FFEF7C54FFEF7C54FF00000000000000000000000000000000EF7B
          51FFEF7B52FFF07B51FFEF7C51FFEF7B51FFF07B51FFEF7C52FFF07C51FFF07B
          51FFEF7C51FFF07B51FFF07B51FFF07B51FFEF7B51FFF07B51FFF07B52FFF07B
          51FFEF7C51FFF07C51FFC86237DB00000000000000000000000000000000EF7B
          51FFEF7B51FFEF7B51FFEF7B51FF00000000000000000000000000000000F07A
          4FFFF07A4EFFF07A4FFFF07A4FFFF07A4EFFF07A4FFFF07A4FFFF07A4EFFF07A
          4EFFEF7A4EFFF07A4EFFEF7A4EFFF07A4FFFEF7A4EFFF07A4FFFF07A4EFFEF7A
          4FFFF07A4EFFF07B4FFFF07A4EFFF07B4EFFC96236DB0000000000000000F07A
          4FFFF07A4FFFF07A4FFFF07A4FFF00000000000000000000000000000000F079
          4CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF079
          4CFFF0784CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794DFFF0794CFFF079
          4CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794CFFF0794BFFC86033D9F079
          4CFFF0794CFFF0794CFFF0794CFF00000000000000000000000000000000F5A3
          83FFF5A383FFF5A383FFF5A383FFF5A383FFF5A283FFF5A283FFF5A383FFF5A3
          83FFF5A383FFF5A383FFF5A383FFF5A283FFF5A383FFF5A383FFF5A383FFF5A3
          83FFF5A383FFF5A283FFF5A383FFF5A383FFF5A383FFF5A383FFF49F7FFEF5A3
          83FFF5A383FFF5A383FFF5A383FF00000000000000000000000000000000F6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFFF6AA8CFFF5A888FE2D1B133100000000F6AA
          8CFFF6AA8CFFF6AA8CFFF6AA8CFF00000000000000000000000000000000F7B2
          96FFF7B296FFF7B297FFF7B296FFF7B296FFF7B297FFF7B296FFF7B296FFF7B2
          97FFF7B296FFF7B296FFF7B296FFF7B296FFF7B297FFF7B296FFF7B296FFF7B2
          96FFF7B296FFF7B296FFF6AF93FE311E1535000000000000000000000000F7B2
          96FFF7B296FFF7B296FFF7B296FF00000000000000000000000000000000F8BA
          A1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA0FFF8BAA1FFF8BAA1FFF8BA
          A1FFF8BAA1FFF8BAA0FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BAA1FFF8BA
          A1FFF7B99FFF332017370000000000000000000000000000000000000000F8BA
          A1FFF8BAA1FFF8BAA1FFF8BAA1FF00000000000000000000000000000000F9C2
          ABFFF8C2ABFFF9C2ABFFF8C2ABFFF9C2ABFFF9C2ABFFF9C2ABFFF8C2ABFFF8C2
          ABFFF8C2ABFFF8C2ABFFF8C2ABFFF9C2ABFFF8C2ABFFF9C2ABFFF8C0A9FF3824
          1A3C00000000000000000000000000000000000000000000000000000000F9C2
          ABFFF9C2ABFFF9C2ABFFF9C2ABFF00000000000000000000000000000000F9CA
          B6FFFACAB6FFFACAB6FFF9CAB6FFFACAB6FFF9CAB6FFF9CAB6FFF9CAB6FFF9CA
          B6FFF9CAB6FFFACAB6FFF9CAB6FFFACAB6FFF9C9B4FF6644336C000000000000
          000000000000000000000000000000000000000000000000000000000000F9CA
          B6FFF9CAB6FFF9CAB6FFF9CAB6FF00000000000000000000000000000000FBD2
          C1FFFBD2C1FFFBD2C1FFFAD2C1FFFBD2C1FFFAD2C1FFFBD2C1FFFAD2C1FFFAD2
          C1FFFBD2C1FFFAD2C1FFFAD1BFFF6947376F0000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FBD2
          C1FFFBD2C1FFFBD2C1FFFBD2C1FF00000000000000000000000000000000FBDA
          CCFFFBDACCFFFBDACCFFFBDACCFFFBDACCFFFBDACCFFFBDACCFFFBDACCFFFBDA
          CCFFFBD9CBFF7451407A00000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FBDA
          CCFFFBDACCFFFBDACCFFFBDACCFF00000000000000000000000000000000FCE2
          D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE1D5FF7652
          417B000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FCE2
          D6FFFCE2D6FFFCE2D6FFFCE2D6FF00000000000000000000000000000000FDE9
          E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE9E0FFFDE8DFFF7854447D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FDE9
          E0FFFDE9E0FFFDE9E0FFFDE9E0FF00000000000000000000000000000000FEF0
          EAFFFEF0EAFFFEF0EAFFFDEEE8FF7A57467F0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FEF0
          EAFFFEF0EAFFFEF0EAFFFEF0EAFF00000000000000000000000000000000FEF6
          F2FFFEF5F1FFA47A65AA00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FEF6
          F2FFFEF6F2FFFEF6F2FFFEF6F2FF00000000000000000000000000000000805D
          4D85000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000805D
          4D85805D4D85805D4D85805D4D85000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000EF8260FFEF8260FFEF8260FFEF82
          60FFF08260FFEF8260FFEF8260FFEF8260FFEF8260FFEF8260FFEF8260FFEF82
          60FFF08260FFEF8260FFEF8260FFEF825FFFEF825FFFEF8260FFEF8260FFEF82
          5FFF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000EF825FFFEF825FFFEF815EFFEF825FFFEF82
          5FFFEF815EFFEF815FFFF0815EFFEF815EFFEF815FFFEF815EFFEF815EFFEF81
          5FFFEF825FFFEF825FFFEF815EFFEF815EFFEF815EFFEF825EFFEF815EFFEF81
          5FFFEF815FFF0000000000000000000000000000000000000000000000000000
          00000000000000000000EF805DFFEF805DFFF0815DFFEF805DFFEF805DFFF081
          5DFFEF805CFFEF805DFFEF805DFFEF815DFFF0805DFFEF815DFFEF805DFFF081
          5CFFEF805DFFEF815DFFF0805DFFF0805CFFEF815CFFEF805DFFF0815DFFEF81
          5DFFEF815CFFEF805DFF00000000000000000000000000000000000000000000
          00000000000000000000EF805BFFF0805BFFF0805CFFEF805BFFEF7F5BFFF080
          5BFFF0805BFFEF7F5BFFEF805BFFF0805BFFEF805BFFF0805CFFEF805BFFEF80
          5BFFEF805BFFF0805BFFF0805CFFF0805BFFEF805BFFEF7F5BFFF07F5CFFEF7F
          5BFFEF805BFFF0805BFF00000000000000000000000000000000000000000000
          00000000000000000000EF7F5AFFEF7F59FFEF7F59FFF07E5AFFEF7F59FFEF7F
          59FFEF7F59FFEF7F59FFEF7F59FFF07E59FFEF7F59FFF07F5AFFEF7F59FFEF7F
          59FFEF7F59FFF07F5AFFF07F5AFFEF7F59FFF07F59FFF07F5AFFEF7F59FFEF7F
          59FFF07E59FFEF7F59FF00000000000000000000000000000000000000000000
          00000000000000000000EF7E57FFEF7E57FFEF7E58FFEF7E57FFEF7E58FFEF7F
          58FFF07E58FFEF7E57FFEF7E57FFEF7E58FFEF7E57FFF07E57FFF07E57FFF07E
          57FFF07E57FFEF7F57FFF07E58FFEF7E57FFF07E58FFEF7E57FFEF7E58FFEF7E
          58FFEF7E58FFEF7E57FF00000000000000000000000000000000000000000000
          00000000000000000000F07D56FFEF7D56FFF07D56FFF07E55FFF07D56FFF07D
          55FFF07D56FFF07D55FFEF7D55FFF07D55FFEF7D55FFEF7E55FFEF7D55FFF07D
          55FFEF7D55FFF07D55FFF07D56FFEF7D56FFEF7D55FFF07D55FFF07D56FFEF7D
          55FFEF7D55FFF07D55FF00000000000000000000000000000000000000000000
          00000000000000000000F07C54FFF07C53FFF07C53FFEF7C53FFEF7C54FFF07D
          54FFEF7C54FFF07C54FFF07C54FFEF7C53FFEF7C53FFF07C53FFEF7C53FFF07C
          53FFF07D54FFEF7D53FFEF7C53FFF07C53FFF07C53FFF07C53FFF07C53FFF07C
          53FFEF7C54FFF07C53FF00000000000000000000000000000000000000000000
          00000000000000000000F07B51FFF07C51FFEF7C52FFF07B51FFEF7C52FFEF7B
          51FFF07C51FFEF7B52FFF07B51FFEF7C51FFF07B52FFF07B52FFEF7C52FFEF7B
          51FFF07B51FFEF7B51FFF07B52FFF07B52FFF07B51FFEF7B52FFF07B52FFF07B
          52FFF07B51FFEF7B51FF00000000000000000000000000000000000000000000
          00000000000000000000EF7A4FFFF07A4FFFF07A4FFFF07A4FFFF07B4FFFF07A
          4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A
          4FFFEF7A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07A4FFFF07B4FFFF07A
          4FFFF07A4FFFF07A4FFF00000000000000000000000000000000000000000000
          00000000000000000000F0794DFFF07A4DFFF0794DFFF0794DFFF0794CFFF079
          4DFFF0794DFFF0794DFFF0794DFFF0794DFFF0794DFFF0794DFFF07A4DFFF079
          4DFFF0794DFFF0794DFFF0794DFFF0794DFFF0794DFFF0794DFFEF794DFFF079
          4DFFF0794DFFF0794DFF00000000000000000000000000000000000000000000
          00000000000000000000F0784BFFF0784AFFF0784BFFF0784AFFF0784BFFF078
          4BFFF0784AFFF0784BFFF0784AFFF0784AFFF0784BFFF0784AFFF0784BFFF078
          4AFFF0784AFFF0784AFFF0784BFFF0784BFFF0784AFFF0784BFFF0784BFFF078
          4BFFF0784BFFF0784AFF00000000000000000000000000000000000000000000
          00000000000000000000F39672FFF39673FFF39672FFF39672FFF39672FFF396
          72FFF39672FFF39672FFF39673FFF39672FFF39673FFF39672FFF49672FFF396
          72FFF39672FFF39673FFF39672FFF39672FFF39672FFF39672FFF39673FFF496
          72FFF49672FFF39672FF00000000000000000000000000000000000000000000
          00000000000000000000F59F7DFFF49F7DFFF49F7DFFF49F7DFFF49F7DFFF49F
          7DFFF59F7DFFF49F7DFFF49F7DFFF49F7DFFF49F7DFFF59F7DFFF49F7DFFF49F
          7DFFF59F7DFFF49E7DFFF49F7DFFF49F7DFFF59F7DFFF49F7DFFF49F7DFFF49F
          7DFFF49F7DFFF49F7DFF00000000000000000000000000000000000000000000
          00000000000000000000F6A787FFF5A787FFF5A787FFF5A787FFF5A787FFF5A7
          87FFF5A787FFF5A787FFF6A787FFF5A787FFF5A787FFF5A787FFF6A787FFF5A7
          87FFF5A787FFF5A787FFF5A787FFF6A787FFF6A687FFF5A787FFF5A787FFF6A7
          87FFF5A787FFF6A787FF00000000000000000000000000000000000000000000
          00000000000000000000F6B094FFF6B094FFF6B094FFF7B093FFF6B093FFF7B0
          93FFF7B094FFF7B094FFF6B094FFF6B094FFF7B094FFF6AF94FFF7B094FFF6B0
          93FFF6B094FFF6B094FFF7AF94FFF6B094FFF7B094FFF6B093FFF6B094FFF7B0
          93FFF7B093FFF7B094FF00000000000000000000000000000000000000000000
          00000000000000000000F8B99FFFF8B9A0FFF8B9A0FFF8B9A0FFF8B9A0FFF8B9
          A0FFF8B9A0FFF8B99FFFF8B9A0FFF8B99FFFF8B9A0FFF8B99FFFF8B9A0FFF8B9
          9FFFF8B9A0FFF8B9A0FFF8B99FFFF8B99FFFF8B9A0FFF8B9A0FFF8B99FFFF8B9
          9FFFF8B9A0FFF8B99FFF00000000000000000000000000000000000000000000
          00000000000000000000F9C2ACFFF9C2ABFFF9C2ABFFF9C2ACFFF9C2ABFFF9C2
          ABFFF9C2ACFFF9C2ABFFF9C2ACFFF9C2ABFFF9C2ABFFF9C2ABFFF9C2ABFFF9C2
          ABFFF9C2ACFFF9C2ACFFF9C2ABFFF9C2ABFFF9C2ACFFF9C2ACFFF9C2ACFFF9C2
          ABFFF9C2ACFFF9C2ABFF00000000000000000000000000000000000000000000
          00000000000000000000FACCB9FFFACCB9FFFACCB9FFFACCB9FFFACCB8FFFACC
          B9FFFACCB9FFFACCB9FFFACCB9FFFACCB9FFFACCB9FFFACCB9FFFACCB9FFFACC
          B9FFFACCB9FFFACCB9FFFACCB9FFFACCB8FFFACCB8FFFACCB8FFFACCB9FFFACC
          B8FFFACCB9FFFACCB9FF00000000000000000000000000000000000000000000
          00000000000000000000FBD4C4FFFBD4C4FFFBD5C4FFFBD4C4FFFBD4C4FFFBD4
          C4FFFBD4C4FFFBD5C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD5
          C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD4C4FFFBD4
          C4FFFBD4C4FFFBD5C4FF00000000000000000000000000000000000000000000
          00000000000000000000FCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDE
          D1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDE
          D1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDED1FFFCDE
          D1FFFCDED1FFFCDED1FF00000000000000000000000000000000000000000000
          00000000000000000000FDE6DCFFFCE6DCFFFDE6DCFFFDE6DCFFFDE6DCFFFDE6
          DCFFFDE6DCFFFDE6DCFFFDE6DCFFFDE6DCFFFDE6DCFFFDE6DCFFFDE6DCFFFDE6
          DCFFFDE6DCFFFDE6DCFFFCE6DCFFFDE6DCFFFDE6DCFFFCE6DCFFFDE6DCFFFDE6
          DCFFFCE6DCFFFDE6DCFF00000000000000000000000000000000000000000000
          00000000000000000000FDEDE7FFFDEDE6FFFDEDE6FFFDEDE6FFFDEDE6FFFDED
          E6FFFDEDE6FFFDEDE6FFFDEDE6FFFDEDE6FFFDEDE6FFFDEDE6FFFDEDE6FFFDED
          E6FFFDEDE7FFFDEDE6FFFDEDE6FFFDEDE6FFFDEDE7FFFDEDE7FFFDEDE6FFFDED
          E6FFFDEDE6FFFDEDE6FF00000000000000000000000000000000000000000000
          0000000000000000000000000000FEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4
          F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4
          F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4F0FFFEF4
          F0FFFEF4F0FF0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FFFAF9FFFFFAF9FFFFFAF9FFFFFA
          F9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFA
          F9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFAF9FFFFFA
          F9FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0003000000040000000400000003000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000201
          000C00000010000000100000000F000000030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B06040F2A180E3C381F
          1063170C0645020100180000000F0000000C0000000500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000503020A1F120B286138
          1F7C4F2D188721110963050201270000001B0000000F00000005000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000005635
          206388512FAF733F25BE27110967070301340000001B0000000F000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002015
          0D2483533897C8744FF78A4428DF1F0D085B040101270000001B0000000F0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000D08
          06126F463379C97B56E2C87444F76E3C1CC4271408750603012D0000000F0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000402
          0106442B1F4C966144ABDD9166FEB1693FF35F341BB71C0E0665000000170000
          000B000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000030000000400000004000000040000
          0004000000040000000400000004000000040000000100000000000000000000
          0000180F0B205A3C2B6FDB9B7BFFD5835DFF9C5535E7472310A70703002F0000
          0017000000070000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000C0000001000000010000000100000
          0010000000100000001000000010000000100000000700000000000000000000
          0000030201083926194BD08F6FFFDC8963FFBF6B47FF7A3A1FD72911055F0301
          0023000000070000000000000000000000000000000000000000000000000000
          000000000000000000060C0705180403021B0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001300000008000000000000
          000000000002301F1642D08A64FFDC9673FFC57753FF914625E65123109B0703
          013B000000130000000800000000000000000000000000000000000000000000
          000000000004160D0A296E42308F4727198F351B0F8F2F160B8F2C13088F2811
          088F2411088F2310078F230F048F200E0483080300470000000B000000000000
          000002010108321D1449C17754F7DD9874FDD3835BFFA7542DEF6A3116BF0D06
          0147000000170000000B00000000000000000000000000000000000000000000
          000003020108452B1F4BE38563FFCE7045FFBF6337FFB3592FFFA74D23FF9B47
          20FF8D4320FF803E1AFD7A340FF7612A0CCA180A026300000008000000000000
          000005020112381D1257B06547F0DC9470FBDF8B5FFFB96134F4753819D31309
          034F010000190000000B00000000000000000000000000000000000000000000
          000003020108462C204BEF976FFFE38B63FFD77B4FFFCB6B3BFFBF5F2FFFB359
          23FF9F4F20FF864019F76E2F0CDF331607730904012700000000000000000000
          0000070302183D1F1360A5593BF0D08D6DFBE08B5FFFC46534F77C361ADF190B
          065B0201001F0000000B00000000000000000000000000000000000000000000
          000003020108462C204BEF9870FFEC9E76FFE99367FFE38353FFD77747FFCB6D
          3BFFA55225FF71320FE5441C08970F0601460200001B00000008000000000000
          00000E06022A4C27167BB06547F0DC8B67FBE58B59FFC86D36FD8A421DF72C13
          08730602012B0000000B00000000000000000000000000000000000000000000
          00000101000440291E45F09C78FFF0AB84FFF0A177FFEB9063FFDF8757FFD378
          4BFFAB592FFF723514E33E1B0C8F0703013B000000170000000D000000080401
          001426110658713C1FAFC97A54F7E58D62FDE78957FFCA703CFD91481DF72E14
          086D060200230000000500000000000000000000000000000000000000000000
          000000000000301E173FF0A080FFF0B592FFF0AE89FFEC9D75FFE39163FFD47F
          54FFB36239FF82401FED522716B7130904600602013504020026050200241D0D
          044B572B1399A55D33E4E49163FFED9060FFE78757FFC86F40F88D451BE3210F
          0556020100130000000000000000000000000000000000000000000000000000
          0000000000000E09073FF0A080FFF0BEA4FFF0C3A7FFF0B593FFEF976FFFD785
          57FFBF6B3FFFA3512BFF7F3E1FFE602B18C3421B0C8F2E14066B2A1506606030
          12B49F5A32E7D88658FFF0976FFFF09163FFE78757FFC16B3EEF763A16BF0E07
          013B000000070000000000000000000000000000000000000000000000000000
          0000000000000E0A073FF0A680FFF0C4ADFFF0C6B3FFF0BAA2FFEFA987FFE997
          6FFFDD8357FFC76E43FFA15636FE90482AF0843F1EE3803F1ADA854821D7A95D
          38ECC8784DF9E18D5EFFF09163FFF08B60FFDB7B51FF9E5331DD3F1C0E770402
          0020000000010000000000000000000000000000000000000000000000000000
          0000000000000E0A083FEFAC88FFE9BBA0FFD1A188E7C99277DBF7B397FFF1AD
          8BFFEB9D77FFE18A63FFCF7B57FFC36F4BFFC06C43FFC26F42FFC87848FFD481
          54FFE08A5CFFEA9262FFF09767FFF08E61FFBB6742E765341EA9140904370100
          000D000000000000000000000000000000000000000000000000000000000000
          0000000000001913103DE7A586F7BE8A6CD6895D44A37E523B94DB9881DFECB3
          94F7F1B291FFF0A583FFEF9D77FFE6916BFFE48C65FFE48D64FFE49264FFE492
          64FFEA9568FFF09B6FFFEFA377FFE99062FF915032C333190E680201000B0000
          0002000000000000000000000000000000000000000000000000000000000000
          000000000000342A2638D08A6EDF5130205B170D081F1F120D2C784C3880DB98
          75DFF7BC98FFF0BB9BFFF0AF8FFFF0A383FFF09B77FFF09870FFF09870FFF098
          70FFF0A480FFF0B090FFEFAF8FFFD7855FFF6837229F1209042F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000302020E0C08063704020116010000070302010B1F130B206846
          2F6DAC7657B7DEA17DEDF0C1A7FFF0BA9BFFF0B395FFF0B094FFF0B093FFF0B0
          8AFFF0AC85FFECA27AFFE1895EFE7C43279C1E0F08450100000B000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001E13
          0B2453372558855D468FB2826ABFB98368C5C68D72D3D3967CE1D99679E7CD89
          64DBB37453C7915A3EAD6B3B238F27140B410502011300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000805030C1D140F2A432A1F605F3A296C7C4E3884905B429A945A409F7C48
          2C8A502D1C6B26160E440A050317010000050000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000804020C160C0718190E081B0E07040F0201
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000030100000601000006000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000004000000040000000400000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000040201011002010110000000100000000B000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000060402182719104956321C945F341DB71C0F07690000000B000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000603
          02183320175C814F34A99F562FDF5F3319AF150B045300000005000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000201000008301D
          15538C523AAFC37147F29F4F27EF281308620401001900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000008120C0820965B
          3FBCCF7A4CF7B66533EF763A16BF1108043B0000000700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000201010C21140E4196603FBCC972
          4CEBC86B41FD8F4A26DA33190D6B050201260000000700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000160E09386A432D91D78653F7D677
          49FDB45D34F76A331AC112090437010000130000000300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000201010940291B6DB8744CDBDF834BFFCD70
          3FFF974D28E54824109F0402011B000000060000000000000000000000000000
          0000000000020000000400000004000000040000000400000004000000040000
          0004000000010000000000000000000000000000000000000000000000000000
          00000000000000000000000000001B130D24764E3497E08957FFDF773FFFBB5F
          33FF743A1EC729140A6F0000000F000000030000000000000000000000000000
          0000000000080000001000000010000000100000001000000010000000100000
          0010000000070000000000000000000000000000000000000000000000000000
          0000000000000000000000000000805A4590C88360DFE88E60FFDF773FFFBB5F
          2AFF633112AF190C044B0000000F000000030000000000000000000000000000
          0009000000140000001C0000001C0000001C0000001C0000001C0000001C0000
          001C000000130000000800000000000000000000000000000000000000000000
          0000000000000000000000000000A2725AB4E49972F7E99163FFD7733FFFB358
          27FF56280DA3110702390000000F00000003000000000C07051F3B271F7F371F
          1485351B0E8B34190B8F3015088F2D14048F2912048F2810048F280F048F2811
          048F0B0400530000001300000007000000010000000000000000000000000000
          00000000000000000000000000009E6F54B6E29B73F9E79366FFCF6F3FFFA850
          27FF51230DA3100602390000000F00000006010000042A1A1336B77A60CFCF80
          5AF3D1794DFFC76C3EFFBB5D2FFFAF5723FFA3501DFF9A481AFF974017FF9140
          11FF2912038F0000001B0000000F000000030000000000000000000000000000
          0000000000000000000000000000A0704CBFE8A078FFE7936FFFCF6F3FFF9F4B
          27FF56240EAF180A034B000000100000001006040310150E0A1C30201840B88A
          73CFF0AF8FFFEB9F77FFDF875FFFD38153FFC77747FFBB693BFFAF572FFF9745
          17FF3015038F0201001B0000000F000000030000000000000000000000000000
          000000000000000000030804020CA1694AC2E8A078FFEA9B72FFDB7B4BFFAB57
          2AFF6F3312CD2F14057B0201001B0000001201010010040302130D07051C533A
          2C75AD8367C9EEB18CFFEB996BFFDF8A5FFFD98359FFD07B50FFBB693BFFA353
          23FF30170A8F0100001B0000000F000000030000000000000000000000000000
          000000000000000000040B060410A76D50C3ECA880FFEDA67DFFE78B5FFFB764
          35FF8A4218EB552509B71A0B025703010027010000180301001A0B050320331D
          115C80553AB3D09069F9E79367FFE18D61FFE08C60FFD98559FFC77347FFA95E
          2FFF38201193030202210000000F000000030000000000000000000000000000
          000000000000000000030905040CA16E55B3E6A987F5F0B18DFFEF996FFFC874
          45FFA95527FF843B12EA572609AB1A0B02630D050143110601421F0D0554401C
          09837A4121C3BA7048F6E08C64FFE39264FFE19261FFDC8C5BFFD37B4FFFAF68
          3AFF42291897070503270000000F000000030000000000000000000000000000
          00000000000000000000000000007C564884CA9785D7F0B79FFFEF9F6FFFE387
          57FFC7673FFFA74B27FF8E3F0FFE7B350DDB5C280AAF5023099C6C2F0BC09146
          19EFB86838FFD48658FFE09870FFEC9E70FFE79B67FFE0935BFFDF874FFFBB6F
          43FF46291B97070403270000000F000000030000000000000000000000000000
          000000000000000000000000000036251D3C996D58A7F0B393FFEFB187FFEC99
          6FFFDF7F57FFCB683FFFB15627FEA44F26F692451DEB8C451EE69D5230EFB96A
          3DFBD68656FFE99B6DFFECA37BFFEFA072FFEDA274FFE89F73FFDF9367FFC47B
          52FF432A1A910402011E0000000F000000030000000000000000000000000000
          0000000000000000000000000000120D0A186D4F3D7BCE9B7CE1F0BF9FFFF0AD
          8DFFEB9973FFE18559FFCF7347FFC97041FFC36D40FFC26F46FFC87858FFDA8A
          5EFFE89A6CFFF0A47AFFEFA37FFFE99A73FFD78D67F7CF8C68F3DFA37FFFCD88
          61FF492E1E8F0403011B0000000F000000030000000000000000000000000000
          0000000000000000000000000000050403094230254C926F57A4DFB49CEFEBBA
          A0FBF0B18FFFECA077FFE4916BFFE4905FFFE48E62FFE4906CFFE49678FFEDA0
          78FFEDA57CFFE79F7AFDDF8F6EF7C97F5EEB9A5F44C985583FB4B1846ACFC788
          64F35638288F0704031A0000000B000000020000000000000000000000000000
          0000000000000000000000000000000000000E090710402E2548B4846CC0DFBB
          9DEFF0C3A7FFF0B59BFFF0AF8FFFF0A983FFF0A380FFF0A284FFF0A890FFF0AE
          90FFE7A787FFD38F6EF7B46744DF844D2DAF38211557110A071C34221840A772
          57CF6B49398F0B08061700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000201010417100D1843302548AB7D
          62B6DEA585EDF0B499FFF0BBA7FFF0BAA4FFF0B59DFFECB095FFE4AD8FFFE4A1
          7DFFBF7E59E78D5735B857321E731B10083A05030115020100070C0705102919
          1333110A08230000000500000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020101040D0805104E34
          255877533F83A0735BAFE7AC92F7E6AE96FDD7A087F3C08B71E1A77759CF8B5C
          42AB643E2883362012570E080427010000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000503
          0209120C0918422D2249AC785BB8AC785DC098664FB37C5039995E38247B2818
          10420F09061D0301010800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000030202040F0A07100F090710150D0818170D071B0C06030F0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000004000000040100000601000006000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFF9885
          75FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFC2B0
          A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0
          A2FFC2B0A2FFC2B0A2FFC2B0A2FFC2B0A2FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C000000090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF988575FF00000015000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFF00000009000000090000
          0009000000090000000900000009000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          0005000000020000000000000000000000000000000000000007000000150000
          001C0000001C0000001C0000001C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B88971FFA869
          4AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFFA869
          4AFFA8694AFFA8694AFFA8694AFFA8694AFFA8694AFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B88971FFE8A5
          83FFE9A482FFE9A381FFE8A380FFE9A37EFFEAA17DFFA8694AFFE99F79FFEA9E
          77FFEA9D76FFEB9D75FFEB9B73FFEB9B72FFA8694AFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B88971FFE8A5
          84FFE8A482FFE9A381FFE9A380FFE9A37FFFE9A27EFFA8694AFFEA9F7AFFEA9F
          78FFEB9D77FFEB9D75FFEA9C74FFEB9B73FFA8694AFF00000000000000000000
          0000000000000000000000000000000000060000001F0100002B000000260000
          0013000000000000000000000000000000000000000000000000B88971FFE8A5
          84FFE8A483FFE8A482FFE9A481FFE9A380FFE9A27FFFA8694AFFEAA07AFFE99F
          78FFEA9E77FFEA9D76FFEB9C75FFEA9B73FFA8694AFF00000000000000000000
          0000000000000000000A201E1B6A6D655FC2A39991E9C3B9B1FBACA29BF07A70
          6ACF2E2926890303032B00000000000000000000000000000000B88971FFE8A5
          84FFE9A583FFE9A582FFE9A481FFE9A380FFE9A27FFFA8694AFFEAA07BFFEA9F
          79FFEA9F78FFEA9E77FFEA9C75FFEA9C73FFA8694AFF00000000000000000000
          000006050523665E59BDCFC5BEFFF9EFE8FFFFFEF7FFC4BCB6FFF6EDE6FFFFFA
          F1FFD6CBC3FF877D76DF0E0C0B53000000020000000000000000B88971FFE8A5
          84FFE8A583FFE8A583FFE9A482FFE8A480FFE9A37FFFA8694AFFEAA07BFFEAA0
          7AFFEA9F79FFEA9E77FFEA9C75FFEA9C74FFA8694AFF00000000000000000202
          0217857D76CDFCF7EFFFC0B8B3FFE4DBD5FFFFF9F1FFDBD4CCFFF7EEE6FFF9F0
          E8FFB7AFA8FFFAF1E8FFAFA59BF00B09084C0000000000000000B88971FFB889
          71FFB88971FFB88971FFB88971FFB88971FFB88971FFB88971FFB88971FFB889
          71FFB88971FFB88971FFB88971FFB88971FFB88971FF0000000000000006504B
          4699DED6CFFFF5EEE7FFFFF9F2FFFEF6EEFFFDF4EDFFFFF9F1FFFDF5EDFFFCF4
          EBFFFFF7EEFFFBF4EAFFE5DCD3FF796F69D40101011D00000000000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C000000151E1B194BCBC2
          BAF2CCC6C0FFDFD8D0FFFFF9F2FFFEF5EEFFFDF5EDFFFDF5EDFFFDF5ECFFFDF4
          ECFFFFF6ECFFF3EBE2FFC0B9B2FFDED4CBFF1A17166B00000000B49D8CFF9885
          75FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF0000001C5A545090F2EB
          E3FFFFFFFBFFFFF8F1FFFDF5EEFFFDF5EDFFFDF5EDFFFFF7EFFFFFF8F0FFFEF5
          EDFFFDF4EBFFFDF4EBFFFFFDF3FFFFF8EEFF4F4744AC00000002B49D8CFFFEFA
          FCFFFEFBFBFFFEFAFBFFFEFAFAFFFDFAFAFFFEF9FAFFC2B0A3FFFEF8F8FFFDF8
          F7FFFDF8F7FFFDF7F6FFFDF7F6FFFDF6F6FF806D59FF0000001C7F7872B5F4EC
          E5FFDBD4CDFFFEF6EEFFFEF5EEFFFEF6EFFFFFF8F0FFC6BEB7FFCBC3BBFFFEF7
          EFFFFFF7EEFFFEF7EDFFE4DBD3FFF1EBE2FF706863CE00000007B49D8CFFFFFA
          FCFFFEFBFBFFFEFBFCFFFEFAFBFFFEFAFAFFFDFAFAFFC2B0A2FFFDF9F9FFFEF8
          F8FFFDF8F8FFFDF8F8FFFDF8F7FFFDF7F6FF806D59FF0000001C8B847DBBF2EA
          E2FFCBC4BCFFFFF7EFFFFFFAF4FFFFFDF8FFFFFEF9FF9D9792FFA59F9BFFB5AE
          A9FFF7F4EDFFFFFDF5FFD6CEC5FFE9E3DAFF776F69D200000007B49D8CFFFEFC
          FDFFFEFAFDFFFEFAFCFFFEFBFCFFFFFBFCFFFEFAFBFFC2B0A2FFFEFAF9FFFEF9
          F9FFFEF9F9FFFDF8F8FFFDF8F7FFFDF8F7FF806D59FF0000001C6B645F9BF8F0
          E7FFFFFFF8FFFFFEF9FFFFFFFDFFFFFFFCFFFFFFFDFFA59F9CFFEBE8E4FFD1CD
          C9FFB0A9A4FFEEE9E3FFFFFFF6FFFFFDF3FF5C5651B500000001B49D8CFFFFFC
          FDFFFFFBFDFFFEFBFDFFFFFAFCFFFFFBFCFFFFFAFCFFC3B0A3FFFEFAFAFFFEFA
          FAFFFDFAF9FFFEF9F9FFFDF9F8FFFDF8F8FF988575FF000000153835315EE3DA
          D1FBE8E2DAFFEBEAE8FFFFFFFEFFFFFFFEFFFFFFFEFFADA8A4FFECEAE7FFFFFF
          FFFFE3E1DEFFB4AEAAFFDED6CEFFF6ECE1FF2C28257900000000B49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8DFFB49D8CFFB49D8CFF00000007090808108F88
          82B6E9E2D9FFEDECECFFFFFFFFFFFFFFFFFFFFFFFFFFB8B3B0FFEDECEAFFFFFF
          FFFFFFFFFFFFF5F4F2FFE7E2DAFFB3AAA1E90403032200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000F0E
          0D2EC9C1B9E9FFFFFFFFD5D4D3FFEDEDEDFFFFFFFFFFC0BCBAFFF1F0EFFFFDFD
          FDFFCCCAC9FFFFFFFFFFF0E7DEFF1F1C1B630000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000028262343B3ACA5E2E1DCD7FFF8F8F7FFFFFFFFFFBFBBB9FFF4F4F3FFFFFF
          FFFFE1DEDAFFD9D0C7FA332F2D73000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000D0C0C237069649AC5BEB8E9EBE5E0FFD8D0C9FFE7E1DBFFD0CA
          C4F17D7770B41513124200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          00050000000D000000170000001C0000001C000000170000000D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000050000000B0706
          05215D4738DB684F3BFF624A38F947382FE00C0A08450000001B0000000E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000200000009030202193F342B87856D
          5BFFC3A895FFDFC2AFFFB89F8EFF7B6759FF3F2F24EF100B08470000001B0000
          000E000000040000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000007483A308D816A59FBA98E7CF6E0C3
          B0FFE1C3B0FFDDBFACFFC5AB9AFFAA9486FF856F60FF5E4430F6130E0A4F0000
          001C0000000F0000000400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A28674EAA68B7AFADEC1AEFFE1C4B1FFE0C2
          B0FFE5C9BAFFE9D1C4FFCEBEB3FFAF998BFFAB9587FF947C6DFF5E4531F71912
          0D5D0000001C0000000E00000004000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C8A691FFDABDAAFFE6CBBBFFE7CDBEFFEBD3
          C6FFF7EEE9FFFEFDFDFFFEFEFEFFD6CDC6FFA99384FFAB9587FF927B6BFF5E45
          32F8110D094C0000001B0000000E000000040000000000000000000000000000
          0002000000070000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000ACAA995FFE7CEC0FFE9D0C2FFF2E4DBFFFEFD
          FCFFFAF6F4FFF1E6E1FFF0E4DDFFFCFAF8FFD9D1CAFFA99384FFAB9587FF957D
          6DFF5D4430FA100C08480000001B0000000E0000000400000001000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001CC1A08BFBEFDFD7FFFEFBFAFFFFFEFEFFF9F5
          F3FFF5ECE7FFF4EAE5FFEFE2DBFFEFE3DCFFFDFBFAFFDAD3CDFFAA9485FFAB95
          87FF978071FF5D4330F9130E0A4F0000001C0000000F0000000400000000B39C
          8CFFA89787FF917D6CFF816C58FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF826D58FFA08068FFCAAC98FFF6F1EEFFFFFFFFFFFFFD
          FDFFF4ECE8FFF3EAE5FFF2E8E2FFEEE1DAFFEFE3DCFFFDFBFAFFCCC2B9FFAB95
          87FFAB9587FF957D6DFF5E4431F619120D5E0000001B0000000C00000000B49D
          8CFFFCFAF8FFFDFAF7FFFDFAF7FFFCF8F5FFFBF7F4FFFBF7F4FFFBF6F3FFFBF6
          F2FFFBF5F1FFF7EFE9FFEAD4C8FF96522FFFEBDDD4FFC8A893FFF4EEEAFFFFFF
          FFFFFFFEFDFFF9F5F2FFF3EAE5FFF4EAE4FFEFE1DAFFEFE3DCFFFDFBFAFFCEC4
          BCFFAA9485FFAB9587FF927B6BFF604532F90403022C0000001600000000B49D
          8CFFFDFBF8FFFDFBF8FFFCF9F7FFFCF8F6FFFCF7F5FFFCF7F5FFFBF7F4FFFBF6
          F3FFF8F0EBFFEAD4C8FF96522FFF985330FFFAF2EEFFECDDD4FFC8A893FFF4EE
          EAFFFFFFFFFFFFFEFDFFF9F5F2FFF3EAE5FFF2E8E3FFEFE2DBFFEFE3DCFFFDFB
          FAFFDAD2CDFFA99384FFAB9587FF8B7363FF513D2DD40000001B00000000B49D
          8CFFFDFBF8FFFDFBF8FFC5AFA0FFAB7D62FFAB7D62FFAB7D62FF9E6B4EFFF9F2
          EFFFEAD7CBFF96522FFFCD8F6EFFA25E39FFF2E5DEFFFAF1EDFFEBDDD4FFCAAB
          97FFF4EEEAFFFFFFFFFFFFFEFDFFF9F5F2FFF3EAE5FFF2E8E4FFEFE1DAFFEFE3
          DCFFFDFBFAFFDAD3CEFFAA9586FFA28C7CFF705847FC0000001C00000000B49D
          8CFFFDFBF9FFFDFBF9FFC5AFA0FFFDFAF8FFFDF9F7FFFDF9F7FFAB7D62FFF6EC
          E7FF9A5532FFCD8E6EFFD39674FFAC6741FFAA6C4CFFCBA490FFEAD9CFFFEBDC
          D3FFCAAB98FFF4EEEAFFFFFFFFFFFFFEFDFFF9F5F2FFF3EAE5FFF4EAE4FFEEE1
          DAFFEFE3DCFFFDFBFAFFCFC4BCFFA79181FF766052FB0000001C00000000B49D
          8DFFFEFCFAFFFDFBFAFFC5AFA0FFFDFAF9FFFDFAF8FFFDFAF8FFAB7D62FFFCF8
          F6FFEEDDD3FFA6623BFFDA9B78FFB36E46FFC69C86FFB57E61FFAD7050FFD4B4
          A2FFEBDDD3FFC8A995FFF4EEEAFFFFFFFFFFFFFEFDFFF9F5F2FFF3EAE5FFF3EA
          E4FFEFE1DAFFEFE2DBFFFEFEFEFFDACEC8FF786354FC0000001B00000000B59D
          8DFFFEFBFAFFFDFCFAFFC5AFA0FFFDFBF9FFFDFBF8FFFDFBF8FFAB7D62FFFCF9
          F7FFFCF8F6FFEEDDD3FFB36E46FFB36E46FFFBF5F2FFF3E8E3FFBA866BFFA96A
          49FFDFC4B6FFEBDCD3FFC8A995FFEBDFD8FFFFFFFFFFFFFEFDFFF9F5F2FFF3EA
          E5FFF3E8E3FFF6EFEBFFF9F3F3FFF3EDEBFF8E7664FC0000001700000000B59E
          8EFFFEFDFBFFFEFDFBFFC5AFA0FFFDFBF9FFFDFBF9FFFDFBF9FFAB7D62FFFCF9
          F7FFFCF9F7FFFCF8F5FFEEDDD3FFB36E46FFFCF6F3FFFBF5F2FFFAF4F1FFB884
          69FFA86846FFF1E3DCFFEBDED4FFC9AB97FFF0E8E3FFFFFFFFFFFFFEFDFFFBF9
          F7FFFCFAF9FFFAF6F6FFF3ECE9FFE6DCD5FF8F7566D10000000F00000000B69E
          8FFFFEFDFCFFFEFCFBFFC5AFA0FFFEFCFAFFFDFCFAFFFDFCFAFFAB7D62FFFDFA
          F9FFFDFAF7FFFCF9F7FFFCF8F6FFEDDED4FFFBF7F4FFFCF6F3FFFBF6F2FFF2E6
          E0FFA05B37FFD1AE9BFFF9F2EEFFE6D5CAFFD2B7A7FFDBC6BAFFF6F0ECFFFBF9
          F7FFF7F4F2FFEEE6E0FFE0D1C7FFB49F8FEA42362F640000000500000000B69F
          8FFFFEFDFCFFFEFDFCFFC5AFA0FFFEFDFBFFFEFCFBFFFEFCFBFFAB7D62FFFDFB
          F9FFFCFAF8FFFCFAF7FFFCF9F7FFFCF8F6FFFCF7F5FFFBF7F4FFFBF6F3FFFBF4
          F1FFC59882FFAA6C4CFFF8F0EBFFF9F3EEFFEDDFD7FFC3A693FFC8A894FFC6A6
          92FFC3A490FFBF9F8AFF7A6658A731292447000000030000000100000000B6A0
          90FFFEFDFCFFFEFDFCFFC5AFA0FFFEFDFBFFFEFDFBFFFEFDFBFFAB7D62FFFDFB
          FAFFFDFAF9FFFDFAF8FFFDFAF7FFFCF9F7FFFCF8F6FFECDBD0FFB36E46FFB36E
          46FFB36E46FFAC6741FFA25E39FF985330FF97532FFF8B6D55FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B6A0
          91FFFEFDFCFFFEFDFCFFC5AFA0FFFEFDFCFFFEFDFCFFFEFDFCFFAB7D62FFFDFC
          FAFFFDFBF9FFFDFAF9FFFDFAF8FFFDFAF7FFFCF9F6FFFAF4F0FFEAD6CAFFB36E
          46FFDA9B78FFD39674FFCD8F6EFF97532FFFE9D3C7FF816D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B6A0
          91FFFEFDFCFFFEFDFCFFC5AFA0FFFEFDFCFFFEFDFCFFFEFDFCFFAB7D62FFFDFC
          FBFFFDFCFBFFFEFCFAFFFDFBF9FFFDFAF8FFFCFAF7FFFCF9F6FFFAF4F1FFEAD6
          CAFFA5613AFFCD8E6EFF97532FFFE9D4C8FFF8F1ECFF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B6A2
          92FFFEFDFCFFFEFDFCFFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFFEFD
          FBFFFEFCFBFFFEFCFBFFFDFCFAFFFDFBF9FFFDFAF9FFFDFAF7FFFDF9F6FFFAF4
          F1FFEAD6CAFF9A5632FFEAD5CAFFF9F2EEFFFBF5F1FF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B7A2
          93FFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFD
          FCFFFEFDFCFFFDFCFBFFFEFCFBFFFDFBFAFFFDFAF9FFFDFAF9FFFDFAF7FFFCF9
          F6FFFAF4F0FFEFE0D6FFF9F1ECFFFBF6F3FFFBF5F2FF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B7A2
          93FFFEFDFCFFFEFDFCFFC5AFA0FFAB7D62FFAB7D62FFAB7D62FF9E6B4EFFFEFD
          FCFFC5AFA0FFAB7D62FFAB7D62FFAB7D62FFAB7D62FFAB7D62FFAB7D62FFAB7D
          62FFAB7D62FFAB7D62FF9E6B4EFFFCF7F4FFFCF6F3FF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B9A3
          93FFFEFDFCFFFEFDFCFFC5AFA0FFFEFDFCFFFEFDFCFFFEFDFCFFAB7D62FFFEFD
          FCFFC5AFA0FFFEFDFCFFFEFCFCFFFEFCFBFFFEFCFBFFFDFBFAFFFDFBF9FFFCFA
          F9FFFCF9F8FFFDF9F6FFAB7D62FFFBF7F5FFFCF7F4FF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B9A3
          95FFFEFDFCFFFDFDFCFFC5AFA0FFFEFDFCFFFEFDFCFFFEFDFCFFAB7D62FFFEFD
          FCFFC5AFA0FFFEFDFCFFFEFDFCFFFEFDFCFFFDFCFBFFFDFCFAFFFEFBFAFFFDFB
          F9FFFDFAF8FFFDFAF7FFAB7D62FFFCF8F6FFFCF7F4FF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000B9A5
          96FFFEFDFCFFFEFDFCFFC5AFA0FFFEFDFCFFFEFDFCFFFEFDFCFFAB7D62FFFEFD
          FCFFC5AFA0FFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFDFC
          FAFFFDFBF9FFFDFBF9FFAB7D62FFFCFAF7FFFCF8F5FF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000BAA5
          96FFFEFDFCFFFEFDFCFFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFFEFD
          FCFFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFC5AFA0FFC5AF
          A0FFC5AFA0FFC5AFA0FFC5AFA0FFFDFAF8FFFCF8F6FF806D59FF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000BCA8
          9AFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFD
          FCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFBFFFDFC
          FBFFFDFCFAFFFDFBFAFFFDFAF9FFFDFAF8FFFDF9F6FF917D6CFF0000001C0000
          000A00000000000000000000000000000000000000000000000000000000BCA8
          9AFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFD
          FCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFCFFFEFDFBFFFDFC
          FBFFFDFCFAFFFDFBFAFFFDFAF9FFFDFAF8FFFCF9F7FFA89787FF000000150000
          000700000000000000000000000000000000000000000000000000000000BCA8
          9AFFBCA89AFFBCA89AFFBBA79AFFBBA798FFBBA798FFBBA798FFBBA698FFBAA6
          98FFBAA597FFBAA596FFB9A496FFB9A495FFB9A394FFB8A393FFB8A293FFB8A1
          93FFB7A192FFB7A091FFB6A090FFB69F8FFFB69E8FFFB59E8FFF000000070000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0007000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000007000000070000
          00150000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001D0000002000000022000000220000002000000016BAA596FFAD9D
          8EFF988575FF8B7966FF857260FF83705DFF816D5AFF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF7F6D
          59FF7D6A57FF786754FF746351FF746351FF786653FF00000021BBA597FFFCFA
          F9FFFCFAF8FFFBF9F7FFFCF9F7FFFBF8F6FFFBF8F5FFFBF7F4FFFBF6F4FFFBF6
          F3FFC2AF9FFFF9F4F1FFFAF4F1FFF9F4F0FFF9F4F0FFF9F3EFFFF9F2EFFFF9F2
          EDFFF8F2EDFFF9F1EDFFBDAA9AFFF8F0EBFFF8F0EBFFF8EFEAFFF8EFEAFFF7EE
          EAFFF8EFE9FFF7EEE8FFF7EEE8FFF8EEE8FF806D59FF0000001CBCA698FFFDFA
          F9FFFCFAF8FFFCF9F7FFFBF9F7FFFCF8F6FFFCF7F6FFFBF8F5FFFAF6F3FFF7F2
          EFFFBDAA9BFFF3EDEAFFF5F0ECFFF9F3F0FFFAF4F0FFF9F4F0FFF9F3EEFFF9F2
          EEFFF9F2EDFFF9F1EDFFBEA99BFFF8F0ECFFF9F0EBFFF8F0EBFFF8EFEAFFF7EF
          E9FFF7EEE9FFF7EEE8FFF7EEE8FFF7EDE8FF806D59FF0000001CBCA798FFFCFB
          F9FFFDFAF8FFFCFAF8FFFCF9F8FFFBF9F7FFFCF8F6FFFBF8F5FFF5F1F1FFDEDD
          E7FFA29497FFDEDADBFFE8E5E1FFF2EDE9FFF8F3EFFFF9F3F0FFFAF3EFFFF9F2
          EEFFF9F3EEFFF9F1EDFFBFAA9BFFF9F0ECFFF8F1ECFFF8F0EBFFF8F0EAFFF8EF
          EAFFF7EFE9FFF8EEE9FFF8EEE8FFF8EEE8FF806D59FF0000001CBDA89AFFFDFA
          FAFFFDFAF9FFFCFAF9FFFCFAF8FFFCF9F7FFFCF9F6FFFCF8F6FFACB3F1FF5C6C
          EEFF515BCDFF747DD5FFDBD6D5FFE8E3E0FFF4EEEBFFFAF4F0FFF9F3F0FFF9F3
          EFFFF9F3EEFFF9F1EEFFBFAA9BFFF7F0ECFFF4EDE7FFF8F0EBFFF8F0EAFFF8F0
          EAFFF8EFEAFFF7EFE9FFF7EFE9FFF7EEE9FF806D59FF0000001CBEAA9AFFC9B6
          A7FFC8B6A7FFC8B5A6FFC7B4A5FFC7B4A5FFC6B3A4FFC6B3A4FF6C6FC5FF546B
          F2FF4C61ECFF4654DCFF6165C1FFAC9B8EFFB8A697FFBFAD9DFFC1AD9EFFC1AC
          9EFFBFAC9DFFBFAB9CFFBDAA9BFFB9A597FFAF9E95FFBEA99AFFBDA89AFFBDA8
          99FFBCA799FFBCA798FFBCA697FFBCA697FF806D59FF0000001CBFAB9DFFFDFB
          FAFFFDFBFAFFFDFAF9FFFCFAF9FFFCFAF8FFFCF9F7FFFCF9F7FF8492F4FF6B84
          FBFF4C61F0FF3F51E8FF4959E2FFC8C7D4FFE4E1DDFFF2EDE9FFF9F3EFFFF9F3
          F0FFF9F3EFFFF9F1EDFFBAA698FF8793EBFF3A56DFFFF9F1ECFFF8F0ECFFF8F0
          EBFFF8F0EAFFF8EFEAFFF7EFE9FFF8EFE8FF806D59FF0000001CC1AC9DFFFDFB
          FBFFFDFBFAFFFCFBF9FFFCFBF9FFFDFAF8FFFCF9F8FFFCF9F7FFA1AFF9FF526A
          F9FF6A84FDFF3F56F0FF3E51E8FF737EE0FFDCD7D5FFE8E4E1FFF5F0ECFFFAF4
          F1FFF9F3EEFFF2EBE8FF5663CCFF0A24E5FFB4B7DCFFF8F1ECFFF8F0ECFFF8F0
          EBFFF8F0EBFFF8F0EAFFF7EFE9FFF8EEE9FF806D59FF0000001CC2AE9FFFFDFC
          FBFFFDFCFBFFFDFCFAFFFDFAFAFFFCFBF9FFFCFAF8FFFCF9F8FFFCF8F7FF7B8F
          F9FF4159F7FF4D65FCFF253FF7FF5264EEFF9EAAE5FFDFDBD7FFEDE9E6FFF6F0
          EDFFF2EBE8FF8997E8FF0A21DDFF687BEFFFEFE9E4FFF9F1EDFFF9F1ECFFF8F1
          EBFFF8F0EBFFF8F0EAFFF8F0EAFFF7EFE9FF806D59FF0000001CC3B0A0FFFDFC
          FCFFFDFCFBFFFDFCFBFFFDFBFAFFFDFAF9FFFCFAF9FFFDFAF8FFFCF9F8FFF3F2
          F7FF5A6FEAFF0D2BFAFF4055ECFF4357ECFF5E75F9FFCACAD8FFE2DEDBFFEAE5
          E2FF5B6FF0FF0C23DFFF465AEEFFE7E3E3FFF6EFEAFFF9F1EDFFF9F1ECFFF9F0
          ECFFF8F1EBFFF8EFEBFFF8F0EAFFF8EFE9FF806D59FF0000001CCE835DFFB87C
          5CFFB87C5CFFB87C5CFFB87C5CFFB87C5CFFB87C5CFFB87C5CFFB87C5CFFB87C
          5CFFB87C5CFF6F6CC1FF3554F5FF3A4DE9FF4356EDFF6D72CBFF9F6B50FF4F4E
          B6FF091FDDFF2E43E4FFA37367FFB67B5BFFB87C5CFFB87C5CFFB87C5CFFB87C
          5CFFB87C5CFFB87C5CFFB87C5CFFB87C5CFFB87C5CFF0000001CCE835DFFF9BF
          A0FFF9BE9FFFF9BE9FFFF9BE9FFFF8BE9FFFF9BD9EFFF9BE9EFFF8BE9EFFF8BD
          9DFFF9BD9DFFF8BC9DFFA895C7FF3652F6FF3147E6FF4052E9FF4059FAFF0D24
          E1FF2B41E8FFC7A0A3FFF4B897FFF8BA99FFF7BA99FFF7BA98FFF8B998FFF7B9
          98FFF7B998FFF7B998FFF7B997FFF7B997FFB87C5CFF0000001CCE835DFFF9BF
          A0FFF9BF9FFFF9BE9FFFF9BF9EFFF8BE9EFFF9BE9EFFF9BD9EFFF8BE9DFFF8BD
          9DFFF8BD9DFFF9BD9DFFF7BB9CFFC09FB4FF3854F0FF2E42E7FF2D41E7FF384C
          EDFFC0999EFFE8AF90FFF4B897FFF8BA99FFF7BA99FFF8BA99FFF8B998FFF7B9
          98FFF7BA97FFF8B997FFF7B997FFF7B897FFB87C5CFF0000001CCE835DFFF9BE
          9FFFF9BFA0FFF9BE9FFFF9BF9FFFF9BE9EFFF8BE9EFFF9BE9EFFF9BD9DFFF8BD
          9DFFF8BD9DFFF7BC9CFFF2B898FFC9A3A5FF586BF4FF4A5BF5FF6073FCFF5770
          F4FFC69D9AFFDFA88AFFE9B091FFF2B697FFF7B998FFF7B998FFF8BA99FFF7BA
          98FFF7B998FFF7B997FFF8B997FFF7B997FFB87C5CFF0000001CCE835DFFF9BF
          9FFFF8BFA0FFF9BEA0FFF9BE9FFFF9BF9FFFF9BE9EFFF9BE9FFFF8BE9EFFF8BD
          9DFFF8BC9CFFF1B899FFAC93B4FF566FF4FF516FFFFF627AFEFF8388E3FF5772
          FDFF546FF6FFBF9AA2FFDFA88AFFE8AE90FFF2B695FFF7BA98FFF8BA98FFF7B9
          98FFF7B998FFF7B998FFF7B997FFF7B897FFB87C5CFF0000001CCE835DFFCE83
          5DFFCE835DFFCE835DFFCE835DFFCE835DFFCE835DFFCE835DFFCE835DFFCC81
          5CFFC67E59FF7866A2FF3E61FEFF5175FFFF6A7FF4FFAF7D7DFFCB815CFFA180
          98FF5E78FEFF4E69F9FF9D7686FFBB7754FFC07A57FFCE835DFFCE835DFFCE83
          5DFFCE835DFFCE835DFFCE835DFFCE835DFFCE835DFF0000001CBDAC9FFFFEFD
          FEFFFEFDFDFFFDFDFDFFFDFCFCFFFDFDFCFFFDFBFBFFFDFBFBFFFAF9F7FFF2EF
          EEFF596ED6FF6F87FDFF5A79FFFF607FFCFFCBD0EBFFF6F3F1FFFBF7F5FFFBF7
          F5FFF1EFF4FF5678FFFF4463F9FF9DAFEFFFE6E1DDFFFAF4F0FFF9F3EFFFF9F3
          EFFFF9F2EEFFF9F2EEFFF9F1EDFFF9F1ECFF806D59FF0000001CBEAEA0FFFEFD
          FEFFFEFDFDFFFEFDFDFFFEFDFDFFFEFDFCFFFDFCFCFFFDFCFBFFDCE3F6FF4665
          FBFF8099FFFF6281FFFF6887FBFFB4BDF1FFF6F2F1FFFBF7F5FFFBF8F6FFFBF7
          F5FFFBF7F4FFFBF7F4FF989BC7FF4A66FBFF96A5F4FFFAF4F1FFF9F4F0FFF9F3
          EFFFF9F2EFFFF9F2EEFFF9F2EDFFF8F1EDFF806D59FF0000001CBEAFA0FFFEFD
          FEFFFEFEFDFFFEFEFDFFFEFDFDFFFEFCFCFFFDFCFCFFFDFCFBFF7190FCFF8EA4
          FFFF6580FEFF6A87FBFFC2C9F0FFF7F4F3FFFBF8F7FFFCF9F7FFFCF8F6FFFBF7
          F6FFFBF7F5FFFBF7F4FFC3B0A1FFE3E2F4FFE8E8F1FFFAF4F1FFFAF4F1FFF9F3
          F0FFF9F3EFFFF9F3EFFFF9F2EDFFF8F2EDFF8B7966FF0000001CBFAFA1FFFEFE
          FEFFFEFDFEFFFEFEFEFFFEFEFDFFFDFDFDFFFDFDFDFFFAFAFBFF6B8CFFFF6180
          FDFF6A7CF1FFC0C7F3FFF9F6F5FFFBF9F7FFFCFAF8FFFCF8F7FFFCF9F7FFFCF8
          F5FFFBF7F5FFFCF7F4FFC3B0A1FFFBF6F3FFFAF5F2FFFAF5F2FFFAF4F1FFFAF4
          F0FFF9F4EFFFF9F3EFFFF9F2EEFFF9F2EEFF988575FF0000001CB88971FFB27C
          61FFB17B60FFB17A5FFFB17A5EFFB0795DFFB0785BFFAF765AFFA4766CFF8575
          A0FF9D726FFFAC7154FFAC7153FFAC7152FFAB6F51FFAA6E50FFAA6D4EFFA96C
          4DFFA86B4CFFA96A4AFFA86949FFA76848FFA76746FFA66646FFA66544FFA565
          43FFA56343FFA46341FFA56241FFA36140FFA4603FFF0000001CB88971FFD7BA
          ABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BA
          ABFFAE7456FFD7B9AAFFD6B9AAFFD6B8A9FFD6B8A9FFD5B7A7FFD5B6A7FFD4B5
          A6FFD3B4A4FFD3B2A3FFA86949FFD1AF9EFFD0AD9CFFCEAA9AFFCCA897FFCAA6
          94FFC9A391FFC8A08EFFC69E89FFC39A86FFA4613FFF0000001CB88971FFD7BA
          ABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BA
          ABFFAD7457FFD7B9AAFFD7B9AAFFD6B9AAFFD6B8A9FFD6B8A9FFD5B7A7FFD5B6
          A7FFD4B5A5FFD3B4A4FFA8694AFFD2B1A0FFD1AF9EFFD0AD9BFFCEAA98FFCCA8
          96FFCAA693FFC9A390FFC79F8CFFC69D88FFA4613FFF0000001CB88971FFD7BA
          ABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BAABFFD7BA
          ABFFAE7457FFD7BAABFFD7B9AAFFD6B9AAFFD6B9A9FFD6B8A9FFD6B8A8FFD5B7
          A7FFD5B6A7FFD4B5A5FFA8694AFFD2B2A2FFD1B1A0FFD0AF9DFFCFAD9BFFCEAA
          98FFCCA895FFCAA592FFC9A28FFFC79F8CFFA46140FF00000015B88971FFB888
          70FFB88870FFB78870FFB7876EFFB6866EFFB6866DFFB7866CFFB5856CFFB684
          6AFFB58469FFB58269FFB58167FFB48267FFB38066FFB37F64FFB27F63FFB27D
          63FFB27E62FFB27D61FFB17B5FFFB07B5EFFB07A5DFFB07A5DFFAF795CFFAF78
          5BFFAF785AFFAF7859FFAE7759FFAF7758FFAE7658FF00000007000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0004000000080000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000800000005000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000040000
          00100000001A0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001A00000011000000050000000000000000000000000000
          00000000000000000000000000000000000000000000000000004C423C707F66
          54FF76614CFF76614CFF76614CFF76614CFF76614CFF76614CFF76614CFF7661
          4CFF76614CFF76614CFF76614CFF76614CFF76614CFF76614CFF76614CFF7661
          4CFF76614CFF41362A9F0000001A000000080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B7A191FFFDF9
          F8FFFDF9F7FFFCF8F6FFFCF8F6FFFCF7F5FFFCF6F4FFFCF6F4FFFBF5F3FFFBF5
          F2FFFBF4F2FFFBF4F1FFFBF3F0FFFAF3F0FFFAF2EFFFFAF2EEFFFAF1EEFFFAF1
          EDFFFAF0ECFF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B7A292FFFDFA
          F8FFFDF9F8FFFDF9F7FFFCF8F6FFFCF8F6FFFCF7F5FFFCF6F4FFFCF6F4FFFBF5
          F3FFFBF5F2FFFBF4F1FFFBF4F1FFFBF3F0FFFAF3EFFFFAF2EFFFFAF2EEFFFAF1
          EEFFFAF1EDFF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B8A293FFFDFA
          F9FFFDFAF8FFFDF9F7FFFDF9F7FFFCF8F6FFFCF7F6FFFCF7F5FFFCF6F4FFFCF6
          F3FFFBF5F3FFFBF5F2FFFBF4F1FFFBF4F1FFFBF3F0FFFAF3EFFFFAF2EFFFFAF2
          EEFFFAF1EEFF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B8A393FFFDFB
          F9FFFDFAF9FFFDFAF8FFFDF9F7FFFDF9F7FFFCF8F6FFFCF7F5FFFCF7F5FFFCF6
          F4FFFCF6F3FFFBF5F3FFFBF5F2FFFBF4F1FFFBF4F1FFFBF3F0FFFAF3EFFFFAF2
          EFFFFAF2EEFF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B8A394FFFEFB
          FAFFFDFAF9FFFDFAF9FFFDFAF8FFFDF9F7FFFDF8F7FFFCF8F6FFFCF7F5FFFCF7
          F5FFFCF6F4FFFCF6F3FFFBF5F3FFFBF5F2FFFBF4F1FFFBF4F1FFFBF3F0FFFAF3
          EFFFFAF2EFFF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B9A394FFFEFB
          FAFFFEFBFAFFCFBEB2FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBD
          B1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFFBF3
          F0FFFAF3EFFF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B9A494FFFEFC
          FBFFFEFBFAFFFDFBFAFFFDFAF9FFFDFAF9FFFDF9F8FFFDF9F7FFFDF8F7FFFCF8
          F6FFFCF7F5FFFCF7F5FFFCF6F4FFFCF6F3FFFBF5F3FFFBF5F2FFFBF4F1FFFBF4
          F1FFFBF3F0FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B9A595FFFEFC
          FCFFFEFCFBFFCFC0B2FFCFBEB2FFCFBEB2FFCFBEB2FFCFBEB2FFCFBEB2FFCFBE
          B2FFCFBEB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFCFBDB1FFFBF4
          F1FFFBF4F1FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000BAA595FFFEFD
          FCFFFEFCFBFFFEFCFBFFFEFBFAFFFDFBFAFFFDFAF9FFFDFAF9FFFDF9F8FFFDF9
          F7FFFDF8F7FFFCF8F6FFFCF7F5FFFCF7F5FFFCF6F4FFFCF6F3FFFBF5F3FFFBF5
          F2FFFBF4F1FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000BAA597FFFEFD
          FCFFFEFDFCFFCEBFB3FFCFC0B2FFCFC0B2FFCFC0B2FFCFC0B2FFCFC0B2FFCFC0
          B2FFCFC0B2FFCFBEB2FFCFBEB2FFCFBEB2FFCFBEB2FFCFBEB2FFCFBEB2FFFBF5
          F3FFFBF5F2FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000BAA697FFFEFD
          FDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFBFFFEFBFAFFFDFBFAFFFDFAF9FFFDFA
          F9FFFDF9F8FFFDF9F7FFFDF8F7FFFCF8F6FFFCF7F5FFFCF7F5FFFCF6F4FFFCF6
          F3FFFBF5F3FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000BCA799FFFFFE
          FDFFFEFDFDFFAC9481FFAB9380FFAB9380FFAB9380FFAB9380FFAB9380FFAB93
          80FFAB9380FFAB9380FFAB9380FFAB9380FFAB9380FFAB9380FFAB9380FFFCF6
          F4FFFCF6F3FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000200000007C5B3A7FFFFFE
          FEFFFFFEFDFFFEFDFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFBFFFEFBFAFFFDFB
          FAFFFDFAF9FFFDFAF8FFFDF9F8FFFDF9F7FFFDF8F7FFFCF8F6FFFCF7F5FFFCF7
          F5FFFCF6F4FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000000000000700000015DED5CDFFFFFE
          FEFFFFFEFEFFBBA798FFAF9886FFAC9481FFAB9380FFAB9380FFAB9380FFAB93
          80FFAB9380FFAB9380FFAB9380FFAB9380FFAB9380FFAB9380FFAB9380FFFCF7
          F5FFFCF7F5FF76614CFF0000001C000000090000000000000000000000000000
          00000000000000000000000000000000000019869EFF17849CFF16829AFF157F
          98FF137D95FFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFC
          FBFFFEFBFAFFFDFBFAFFFDFAF9FFFDFAF8FFFDF9F8FFFDF9F7FFFDF8F6FFFCF8
          F6FFFCF7F5FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000001B87A1FF7DDBF0FF7BDBF1FF7ADA
          F0FF157E98FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFFEFCFCFFFEFC
          FBFFFEFCFBFFFEFBFAFFFDFBFAFFFDFAF9FFFDFAF8FFFDF9F8FFFDF9F7FFFDF8
          F6FFFCF8F6FF76614CFF0000001C000000090000000000000000000000000000
          0000000000000000000000000000000000001C89A2FF79DAF1FF79D9F1FF78DB
          F2FF16829AFFFFFFFFFFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFCFFFEFC
          FCFFFEFCFBFFFEFCFBFFFEFBFAFFFDFBFAFFFDFAF9FFFDFAF8FFFDF9F8FFFDF9
          F7FFFDF8F6FF76614CFF0000001C000000090000000000000000000000000000
          0002000000070000000900000009000000091D8AA3FF78DCF5FF75DBF3FF75D9
          F2FF17839BFFF1CFBEFFE4A887FFE09A74FFDF966FFFDD9068FFDB885CFFD983
          56FFD87F51FFD77D4EFFD77C4DFFD67A4BFFD57949FFD57847FFD37645FFD474
          43FFD27341FFCE6732FF0000001C000000090000000000000000000000000000
          0007000000150000001C0000001C0000001C1D8CA4FF77DEF4FF74DBF5FF73DB
          F2FF1B88A1FFFFFAF7FFFEF5F0FFFEF3EEFFFEF2ECFFFEF0E8FFFDEAE0FFFDE6
          DAFFFDE4D8FFF7AB84FFF7AA82FFF6A981FFF7A77FFFF6A77EFFF6A67CFFF5A5
          7CFFF5A47BFFCE6732FF0000001C0000000900000000000000000000000046B1
          C7FF2A99B0FF2090A9FF2090A9FF1F90A8FF3EA4BEFF77DFF5FF72DCF5FF70DC
          F5FF3CA4BDFF18869DFF17849BFF16819AFF147E97FF19869FFFFDE3D5FFFBD5
          C1FFFBCFBAFFF8AB83FFF8AA82FFF7A980FFF7A97FFFF7A77EFFF6A67DFFF5A5
          7CFFF5A57AFFCE6732FF0000001A0000000800000000000000000000000046B1
          C7FF87E9F7FF7EE2F8FF7EE2F8FF7BE0F8FF79E1F8FF77DFF8FF75DEF4FF71DD
          F4FF6FDCF5FF6ED9F4FF6DDAF5FF6CD9F4FF6BD9F4FF19869FFFFCD6C3FFFAB8
          97FFF8AD87FFF8AB83FFF8AA83FFF7AA81FFF7A880FFF6A87FFFF6A77DFFF6A5
          7CFFF5A47CFFCE6732FF000000100000000400000000000000000000000046B1
          C7FF8EEAF9FF84E5F9FF85E6F9FF84E5F9FF82E4F9FF7FE3F9FF7BE2F8FF7BE0
          F9FF78DEF7FF75DDF6FF75DEF6FF74DBF5FF73DCF5FF19869FFFEEC9B5FFE19C
          79FFDC8C61FFDA885DFFD9865AFFD88457FFD88355FFD88053FFD77F50FFD67D
          4EFFD57C4BFF562B1570000000040000000100000000000000000000000046B1
          C7FFA7F4FCFFA8F4FCFFA8F3FCFFA8F3FCFFA7F5FCFFA7F4FBFF98ECFBFF96EC
          FAFF91EAF9FF92E8FAFF90E7F9FF8DE6F8FF8CE5F7FF19869FFF000000150000
          0007000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000046B1
          C7FF46B1C7FF46B1C7FF46B1C7FF46B1C7FF67C9DFFFA9F5FCFF9CEEFCFF9AEC
          FCFF3CA4BDFF1D8BA5FF1C8AA3FF1B88A1FF19869FFF19869FFF000000070000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000046B1C7FFAAF4FCFF9EEDFCFF9AED
          FCFF2092ACFF0000001C00000009000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000046B1C7FFAAF4FCFFA1EDFCFF9FED
          FCFF2397B1FF0000001C00000009000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000046B1C7FFACF5FCFFA3EEFCFFA1EE
          FCFF259BB6FF0000001C00000009000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000046B1C7FFA9F5FCFFA9F4FBFFAAF5
          FCFF279EB9FF0000001500000007000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000046B1C7FF46B1C7FF46B1C7FF46B1
          C7FF46B1C7FF0000000700000002000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000004000000070000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000070000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A00000000000000000000000000000000000000000000
          000000000000000000000000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000700000002000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C00000000000000000000000000000000000000000000
          000000000000000000000000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C000000150000000700000000BAA7
          97FFAD9D8EFF988575FF8B7966FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000001C0000000A00000000BBA7
          98FFFCF7F5FFFBF6F5FFFBF6F4FFFBF6F3FFFBF5F2FFFBF5F2FFC4B1A2FFFAF3
          F1FFFAF2EFFFF9F2EFFFFAF2EEFFFAF1EEFFF9F1EDFFC0AC9EFFF8F0ECFFF9EF
          ECFFF9EFEBFFF8EFEBFFF8EFEAFFF8EEE9FFBDA99AFFF8EDE9FFF8EDE9FFF8ED
          E8FFF7ECE8FFF7ECE7FFF7ECE7FF806D59FF0000001C0000000A00000000BCA8
          99FFFBF7F5FFFBF6F5FFFBF6F4FFFCF6F4FFFBF5F3FFFBF5F2FFC4B1A3FFFBF4
          F1FFFAF4F0FFFAF3F0FFFAF2EFFFFAF2EEFFF9F2EEFFC0AD9EFFF8F1ECFFF9F0
          ECFFF9F0ECFFF8EFEBFFF8EFEBFFF8EEEAFFBDA99AFFF8EEE9FFF7EDE8FFF8ED
          E8FFF8EDE8FFF7ECE8FFF8EDE7FF806D59FF0000001C0000000A00000000BDA8
          9AFFFCF8F6FFFCF7F5FFFBF7F5FFFCF6F4FFFCF6F4FFFAF5F3FFC5B2A3FFFAF5
          F2FFFAF4F1FFFAF4F0FFF9F3F0FFFAF3EFFFFAF2EFFFC0AE9FFFF9F1EEFFF9F0
          EDFFF8F0ECFFF8F0ECFFF9F0EBFFF8EFEBFFBDAA9BFFF8EEE9FFF8EEE9FFF8ED
          E9FFF7EDE8FFF8EDE8FFF8EDE8FF806D59FF0000001C0000000A00000000BDA9
          9BFFFCF8F7FFFCF8F6FFFCF7F5FFFBF6F5FFFBF7F4FFFBF6F3FFC5B3A5FFFAF5
          F2FFFAF4F2FFFAF4F1FFFAF4F1FFFAF3F0FFF9F2EFFFC1AEA0FFF9F2EEFFF9F1
          EDFFF9F0EDFFF9F0ECFFF8F0ECFFF9EFEBFFBEAA9BFFF8EEEAFFF7EEEAFFF8ED
          E9FFF8EDE9FFF7EDE9FFF7EDE8FF806D59FF0000001C0000000A00000000CD83
          5EFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFBA7C5CFFB87B
          5CFFB87B5CFFB97C5DFFBA7E60FFBC8265FFBD8468FFBA7C5CFFBD8468FFBC82
          65FFBA7E60FFB97C5DFFB87B5CFFB87B5CFFBA7C5CFFB87B5CFFB87B5CFFB87B
          5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFBD7C5DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFBD7C5DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFBD7C5DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC17E5CFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC17E5CFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC17E5CFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC4805DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC4805DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC4805DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC7805DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC7805DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC7805DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFCC835EFFCC825EFFCC825EFFCB835EFFCB825EFFCA825DFFC9815DFFC881
          5DFFC7815DFFC7815EFFC6805EFFC5805DFFC47F5EFFC9815DFFC27E5DFFC17E
          5DFFC07E5DFFC07E5DFFBE7D5DFFBE7E5CFFC9815DFFBC7D5CFFBC7C5CFFBB7C
          5CFFBA7C5CFFBA7C5CFFB97B5CFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC9815DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC9815DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC9815DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCA815DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCA815DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCA815DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC9815DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC9815DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC9815DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCA815DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCA815DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCA815DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFCC835EFFCC835EFFCC825EFFCB825EFFCA825EFFCA815EFFCA815DFFC881
          5EFFC7805DFFC6805DFFC5815DFFC5805DFFC4805DFFCA815DFFC27F5DFFC17F
          5DFFC07D5CFFBF7D5DFFBE7D5DFFBD7D5CFFCA815DFFBC7C5CFFBB7C5CFFBA7C
          5DFFBA7B5DFFB97C5CFFB87B5CFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCB825DFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCB825DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCB825DFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCB825EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCB825EFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCB825EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFF0000001C0000000A00000000C3B2
          A5FFFFFEFEFFFFFEFEFFFEFEFDFFFFFDFDFFFEFDFDFFFEFDFCFFCDBCADFFFDFC
          FBFFFEFBFBFFFDFCFAFFFDFBFAFFFDFBF9FFFDFAF8FFCAB8A9FFFDF9F7FFFCF8
          F7FFFCF8F6FFFBF8F6FFFBF7F5FFFCF7F5FFC6B4A5FFFBF6F3FFFBF5F3FFFAF5
          F2FFFAF4F1FFFBF4F1FFFAF3F0FF806D59FF0000001C0000000A00000000C5B3
          A6FFFFFEFEFFFFFEFFFFFEFEFDFFFEFEFEFFFFFDFDFFFEFDFCFFCEBCAEFFFEFC
          FCFFFEFCFBFFFDFBFBFFFDFBFBFFFDFBFAFFFDFAF9FFCAB9AAFFFDF9F8FFFCF9
          F7FFFDF8F6FFFCF8F6FFFCF7F6FFFCF7F5FFC7B4A6FFFBF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFBF4F1FFFBF3F0FF8B7966FF0000001C0000000A00000000C5B4
          A6FFFFFFFFFFFEFFFFFFFFFEFEFFFEFEFEFFFFFDFEFFFEFDFDFFCEBDAEFFFEFD
          FCFFFEFDFCFFFEFCFBFFFEFBFAFFFDFBFAFFFDFBFAFFCBB9AAFFFDFAF8FFFDF9
          F8FFFDF9F8FFFCF8F7FFFCF8F6FFFCF7F5FFC7B4A6FFFCF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFAF5F2FFFBF4F1FF988575FF0000001C0000000A00000000C5B4
          A7FFFFFFFFFFFFFFFEFFFFFEFEFFFFFEFFFFFFFEFDFFFFFEFEFFCEBDAFFFFEFD
          FDFFFEFDFCFFFDFCFCFFFEFCFBFFFEFCFBFFFDFBFAFFCBBAABFFFDFAF9FFFDFA
          F9FFFDF9F8FFFCF9F7FFFCF9F7FFFCF8F6FFC7B5A7FFFCF7F5FFFCF6F4FFFBF6
          F3FFFBF5F3FFFBF5F3FFFBF5F2FFAD9D8EFF000000150000000700000000C6B5
          A8FFC5B4A7FFC4B4A7FFC4B3A7FFC4B3A6FFC3B1A4FFC2B1A4FFC1B0A3FFC0AF
          A1FFBFAEA1FFBEAC9FFFBDAB9EFFBCAA9DFFBBA99CFFBAA89AFFB9A89AFFB8A7
          98FFB8A598FFB7A597FFB6A396FFB5A295FFB4A194FFB3A193FFB29F92FFB29E
          90FFB09E90FFB09C8FFFAF9C8EFFAF9C8EFF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000070000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000700000002000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C000000150000000700000000BAA7
          97FFAD9D8EFF988575FF8B7966FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000001C0000000A00000000BBA7
          98FFFCF7F5FFFBF6F5FFFBF6F4FFFBF6F3FFFBF5F2FFFBF5F2FFC4B1A2FFFAF3
          F1FFFAF2EFFFF9F2EFFFFAF2EEFFFAF1EEFFF9F1EDFFC0AC9EFFF8F0ECFFF9EF
          ECFFF9EFEBFFF8EFEBFFF8EFEAFFF8EEE9FFBDA99AFFF8EDE9FFF8EDE9FFF8ED
          E8FFF7ECE8FFF7ECE7FFF7ECE7FF806D59FF0000001C0000000A00000000BCA8
          99FFFBF7F5FFFBF6F5FFFBF6F4FFFCF6F4FFFBF5F3FFFBF5F2FFC4B1A3FFFBF4
          F1FFFAF4F0FFFAF3F0FFFAF2EFFFFAF2EEFFF9F2EEFFC0AD9EFFF8F1ECFFF9F0
          ECFFF9F0ECFFF8EFEBFFF8EFEBFFF8EEEAFFBDA99AFFF8EEE9FFF7EDE8FFF8ED
          E8FFF8EDE8FFF7ECE8FFF8EDE7FF806D59FF0000001C0000000A00000000BDA8
          9AFFFCF8F6FFFCF7F5FFFBF7F5FFFCF6F4FFFCF6F4FFFAF5F3FFC5B2A3FFFAF5
          F2FFFAF4F1FFFAF4F0FFF9F3F0FFFAF3EFFFFAF2EFFFC0AE9FFFF9F1EEFFF9F0
          EDFFF8F0ECFFF8F0ECFFF9F0EBFFF8EFEBFFBDAA9BFFF8EEE9FFF8EEE9FFF8ED
          E9FFF7EDE8FFF8EDE8FFF8EDE8FF806D59FF0000001C0000000A00000000BDA9
          9BFFFCF8F7FFFCF8F6FFFCF7F5FFFBF6F5FFFBF7F4FFFBF6F3FFC5B3A5FFFAF5
          F2FFFAF4F2FFFAF4F1FFFAF4F1FFFAF3F0FFF9F2EFFFC1AEA0FFF9F2EEFFF9F1
          EDFFF9F0EDFFF9F0ECFFF8F0ECFFF9EFEBFFBEAA9BFFF8EEEAFFF7EEEAFFF8ED
          E9FFF8EDE9FFF7EDE9FFF7EDE8FF806D59FF0000001C0000000A00000000BEAA
          9CFFC9B7A9FFC8B6A8FFC8B6A8FFC8B5A7FFC7B5A6FFC6B4A6FFC5B3A5FFC5B3
          A4FFC4B2A4FFC4B2A3FFC3B1A2FFC3B0A2FFC3B0A0FFC2AEA0FFC1AE9FFFC1AE
          9FFFC0AD9FFFBFAD9EFFBFAC9DFFBFAC9CFFBEAB9CFFBEAA9CFFBDAA9BFFBDA9
          9BFFBCA89AFFBCA89AFFBBA899FF806D59FF0000001C0000000A00000000BFAC
          9DFFFDF9F8FFFCF9F7FFFCF8F6FFFCF8F6FFFCF7F5FFFCF7F5FFC6B4A5FFFBF5
          F3FFFBF5F3FFFAF4F2FFFAF5F2FFFAF4F1FFFAF4F0FFC2AFA1FFF9F2EFFFF9F2
          EEFFFAF1EEFFF9F1EDFFF9F1EDFFF9F0ECFFBEAB9DFFF9F0ECFFF8EFEBFFF8EE
          EAFFF8EEE9FFF8EEE9FFF7EDE9FF806D59FF0000001C0000000A00000000C0AD
          9FFFFDF9F9FFFDF9F8FFFCF9F7FFFCF9F7FFFBF8F6FFFCF7F5FFC7B5A6FFFBF6
          F4FFFCF6F3FFFAF5F3FFFBF5F2FFFBF5F2FFFAF4F1FFC3B0A2FFFAF3F0FFFAF2
          EFFFF9F2EFFFF9F2EEFFF9F1EDFFF8F0ECFFBFAC9DFFF9EFEBFFF9EFECFFF8EF
          EAFFF8EEEAFFF8EEE9FFF8EEEAFF806D59FF0000001C0000000A00000000C2AE
          A0FFFCFAF9FFFDFAF8FFFDF9F8FFFCF9F6FFFCF8F6FFFCF7F6FFC7B5A6FFFCF6
          F4FFFBF6F4FFFBF5F3FFFBF5F3FFFBF5F2FFFBF4F1FFC3B0A2FFFAF3F0FFF9F3
          F0FFF9F2EEFFFAF1EFFFF9F2EDFFF9F1EDFFBFAC9EFFF9F0ECFFF9F0ECFFF9EF
          EBFFF8EFEBFFF8EFEAFFF8EEEAFF806D59FF0000001C0000000A00000000C2AF
          A0FFFDFBF9FFFDFAF9FFFCFAF8FFFCF9F7FFFCF9F7FFFCF8F6FFC8B6A8FFFCF7
          F5FFFBF7F4FFFBF7F4FFFBF6F3FFFBF5F3FFFBF5F2FFC4B1A2FFFAF4F1FFFAF3
          F0FFFAF2F0FFF9F2EFFFF9F2EEFFF9F1EEFFC0AD9EFFF8F0EDFFF9F0ECFFF8F0
          EBFFF9EFEBFFF8EFEAFFF8EEEAFF806D59FF0000001C0000000A00000000CD83
          5EFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B
          5CFFB87B5CFFB97C5DFFBA7E60FFBC8265FFBD8468FFBD8568FFBD8468FFBC82
          65FFBA7E60FFB97C5DFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B
          5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFBD8568FFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFC1896DFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCA9377FFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFD49D81FFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCE86
          62FFD08A66FFD28E6DFFD39373FFD69879FFD79D7FFFDAA388FFD9A185FFD79E
          81FFD69A7CFFD49575FFD28F6EFFD08A68FFCE8662FFCE8662FFCB8462FFC783
          61FFC3805FFFBF7E5EFFBA7D5DFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD2BFFFFBD5C3FFFCDACAFFFCDCCDFFDAA388FFFCDCCDFFFCDA
          CAFFFBD5C3FFFBD2BFFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFAD0
          BCFFFBD1BEFFFBD4C1FFFCDACAFFBC683AFFB76133FFAB4E20FFA64719FFA242
          14FFFCDACAFFFBD4C1FFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB67A5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFF9CE
          B9FFFBD1BEFFFBD4C2FFFCDDCEFFC06C3FFFEDC8A8FFE9A378FFDD9771FFA040
          11FFFCDDCEFFFBD4C2FFFBD1BEFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFBA7C5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD2BEFFFBD5C2FFFCDDCFFFC37244FFE5B392FFD5845CFFCC7850FFA140
          12FFFCDDCFFFFBD5C2FFFBD2BEFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFC6805DFF0000001C0000000A00000000CD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD845FFFCE86
          62FFD08A67FFD49473FFDCA88EFFC8774BFFDCA07CFFD07D55FFCC7850FFA141
          12FFDCA88EFFD49473FFD08A67FFCE8662FFCD845FFFCD835EFFCD835EFFCD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFF0000001C0000000A00000000C3B2
          A5FFFFFEFEFFFFFEFEFFFEFEFDFFFFFDFDFFFEFDFDFFFEFDFCFFCEBDAFFFFDFC
          FBFFFEFCFCFFFEFDFBFFFEFDFCFFCC7C50FFECC4A3FFDC996FFFD48960FFA242
          14FFFDFBF9FFFCFAF8FFFCF8F7FFFCF8F6FFC7B5A7FFFBF6F3FFFBF5F3FFFAF5
          F2FFFAF4F1FFFBF4F1FFFAF3F0FF806D59FF0000001C0000000A00000000C5B3
          A6FFFFFEFEFFFFFEFFFFFEFEFDFFFEFEFEFFFFFDFDFFFEFDFCFFD0BEB1FFFEFC
          FCFFD38B61FFD38B61FFD3885EFFDDA27CFFECC4A3FFDC996FFFD48960FFC067
          39FFA14011FF9F3E0FFF9F3E0FFFFAF4F1FFC9B7A9FFFBF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFBF4F1FFFBF3F0FF8B7966FF0000001C0000000A00000000C5B4
          A6FFFFFFFFFFFEFFFFFFFFFEFEFFFEFEFEFFFFFDFEFFFEFDFDFFCFBFB0FFFEFD
          FCFFFAF0EAFFD38B61FFEBC3A0FFEBBC95FFEBBC95FFDC996FFFD78B5EFFD17C
          4DFFD17C4DFFB6663EFFEBD5CAFFFBF7F4FFC9B6A8FFFCF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFAF5F2FFFBF4F1FF988575FF0000001C0000000A00000000C5B4
          A7FFFFFFFFFFFFFFFEFFFFFEFEFFFFFEFFFFFFFEFDFFFFFEFEFFCEBEB0FFFEFD
          FDFFFEFDFCFFF7EAE4FFD08358FFE7BA96FFDB9D70FFDC996FFFE18758FFD47B
          4DFFAB552BFFEEDDD3FFFCFAF8FFFCF8F6FFC8B6A8FFFCF7F5FFFCF6F4FFFBF6
          F3FFFBF5F3FFFBF5F3FFFBF5F2FFAD9D8EFF000000150000000700000000C6B5
          A8FFC5B4A7FFC4B4A7FFC4B3A7FFC4B3A6FFC3B1A4FFC2B1A4FFC1B0A3FFC1B0
          A2FFC3B3A7FFC9BAAFFFCFB9ABFFCA7B4DFFE8BA95FFDD9B72FFD37A4CFFA549
          1DFFC8B1A3FFC3B3A8FFBBA99DFFB6A397FFB4A194FFB3A193FFB29F92FFB29E
          90FFB09E90FFB09C8FFFAF9C8EFFAF9C8EFF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000180B0522BA6939F8E8B998FF983B0FF30E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000016090322B96335FF090300150000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000004000000070000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000041508022E000000150000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000070000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A160903309F3E0FFF090300300000
          001B0000000F0000000A0000000A0000000A0000000A0000000A0000000A0000
          000A0000000A0000000A0000000A0000000A0000000700000002000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C180B053BC86D41FFE8B998FF9F3E0FFF0E05
          01390000001F0000001D0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C000000150000000700000000BAA7
          97FFAD9D8EFF988575FF8B7966FF806D59FF806D59FF806D59FF806D59FF826F
          5CFF887663FF968676FFAC9482FFC86D41FFE8BA95FFDD9B72FFD3794BFF9F3E
          0FFFA8907EFF968676FF887764FF826F5CFF806E5AFF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000001C0000000A00000000BBA7
          98FFFCF7F5FFFBF6F5FFFBF6F4FFFBF6F3FFFBF5F2FFFBF5F2FFC4B2A3FFFAF4
          F2FFFBF4F2FFF4E4DDFFC86D41FFE7BA96FFDB9D70FFDC996FFFDE9367FFDF86
          58FF9F3E0FFFEBD7CDFFF9F2EEFFF8EFEAFFBEAA9BFFF8EDE9FFF8EDE9FFF8ED
          E8FFF7ECE8FFF7ECE7FFF7ECE7FF806D59FF0000001C0000000A00000000BCA8
          99FFFBF7F5FFFBF6F5FFFBF6F4FFFCF6F4FFFBF5F3FFFBF5F2FFC5B3A5FFFBF5
          F3FFF7EAE3FFC86D41FFEBC3A0FFEBC3A3FFEBBC94FFD78F67FFD5875AFFD17C
          4DFFD17C4DFF9F3E0FFFE8D0C5FFF8EFEAFFBFAB9DFFF8EEE9FFF7EDE8FFF8ED
          E8FFF8EDE8FFF7ECE8FFF8EDE7FF806D59FF0000001C0000000A00000000BDA8
          9AFFFCF8F6FFFCF7F5FFFBF7F5FFFCF6F4FFFCF6F4FFFAF5F3FFC7B4A6FFFBF6
          F4FFD6875CFFD6875CFFD6875CFFD7895EFFECBA95FFCF7F59FFCD7B55FFA444
          16FFA03F10FF9F3E0FFF9F3E0FFFF7EDE9FFBFAD9FFFF8EEE9FFF8EEE9FFF8ED
          E9FFF7EDE8FFF8EDE8FFF8EDE8FF806D59FF0000001C0000000A00000000BDA9
          9BFFFCF8F7FFFCF8F6FFFCF7F5FFFBF6F5FFFBF7F4FFFBF6F3FFC6B4A7FFFAF6
          F3FFFBF6F4FFFBF7F5FFFCF8F6FFD8895EFFECB690FFCD7B55FFCD7B55FFA648
          19FFFBF6F4FFFBF4F1FFF9F3EFFFF9F0EDFFBFAC9DFFF8EEEAFFF7EEEAFFF8ED
          E9FFF8EDE9FFF7EDE9FFF7EDE8FF806D59FF0000001C0000000A00000000CD83
          5EFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFFB97C5DFFBA7E
          60FFBC8366FFC18D72FFCDA28DFFDA8B61FFEDC8A9FFDC996FFFDC996FFFAE53
          25FFCDA28DFFC18D72FFBC8366FFBA7E60FFB97C5DFFB87B5CFFB87B5CFFB87B
          5CFFB87B5CFFB87B5CFFB87B5CFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD2BEFFFBD5C2FFFCDDCFFFDF946DFFEDC8A9FFDC996FFFDC996FFFB157
          28FFFCDDCFFFFBD5C2FFFBD2BEFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFF9CE
          B9FFFBD1BEFFFBD4C2FFFCDDCEFFDF946DFFEDC8A8FFEEC3A4FFEDB691FFB55C
          2FFFFCDDCEFFFBD4C2FFFBD1BEFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFAD0
          BCFFFBD1BEFFFBD4C1FFFCDACAFFDF946DFFDF946DFFDD9268FFD17F54FFC673
          46FFFCDACAFFFBD4C1FFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD2BFFFFBD5C3FFFCDACAFFFCDCCDFFDAA388FFFCDCCDFFFCDA
          CAFFFBD5C3FFFBD2BFFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCE86
          63FFD08B69FFD2906FFFD59677FFD79B7EFFD89F83FFDAA388FFD9A084FFD89C
          7FFFD59879FFD39373FFD18E6DFFCF8966FFCE8662FFCE8662FFCB8461FFC783
          60FFC3815FFFBE7E5DFFBB7D5DFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFD79B7EFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB87B5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFD49373FFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFB67A5CFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFD18C6AFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFBE7D5DFF0000001C0000000A00000000CD83
          5EFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFD18C6AFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFFBD1BDFFCD835EFFFBD1BDFFFBD1BDFFFBD1
          BDFFFBD1BDFFFBD1BDFFFBD1BDFFCB825EFF0000001C0000000A00000000CD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD83
          5EFFCD835EFFCD845FFFCE8662FFD08A67FFD18C69FFD18C6AFFD18C69FFD08A
          67FFCE8662FFCD845FFFCD835EFFCD835EFFCD835EFFCD835EFFCD835EFFCD83
          5EFFCD835EFFCD835EFFCD835EFFCD835EFF0000001C0000000A00000000BEAD
          A0FFFEFDFDFFFDFDFCFFFEFCFBFFFEFCFBFFFEFBFAFFFDFBF9FFCBB9ABFFFDFA
          F9FFFCFAF8FFFCFAF8FFFCF8F7FFFCF8F6FFFCF8F6FFC8B5A7FFFBF7F5FFFCF6
          F4FFFBF6F4FFFBF5F3FFFAF5F2FFFBF4F1FFC3B1A2FFFAF3F0FFFAF3F0FFFAF2
          EFFFF9F2EEFFF9F1EEFFF9F1EEFF806D59FF0000001C0000000A00000000BFAE
          A1FFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFBFFFDFCFBFFCBBAACFFFDFB
          F9FFFDFAF9FFFDFAF8FFFDFAF7FFFCF9F7FFFCF8F6FFC8B5A8FFFCF8F5FFFCF7
          F4FFFBF7F4FFFBF6F3FFFBF5F3FFFBF5F2FFC4B1A2FFFAF4F1FFFAF3F0FFFAF3
          F0FFF9F2F0FFF9F2EEFFF9F2EEFF806D59FF0000001C0000000A00000000C1B0
          A2FFFEFEFEFFFFFDFDFFFFFDFCFFFEFDFCFFFEFCFBFFFDFBFBFFCCBBADFFFEFB
          FAFFFEFBF9FFFDFAF8FFFDFAF8FFFCF9F8FFFDF9F7FFC8B6A8FFFCF8F6FFFCF7
          F5FFFBF7F4FFFBF6F4FFFBF6F4FFFBF5F2FFC4B2A3FFFBF5F1FFFAF4F1FFFAF4
          F0FFFAF3EFFFFAF3EFFFFAF2EEFF806D59FF0000001C0000000A00000000C2B0
          A3FFFFFEFEFFFEFDFDFFFEFDFDFFFEFDFDFFFEFCFCFFFEFCFCFFCDBBACFFFEFB
          FBFFFDFAF9FFFDFAF9FFFCFAF9FFFDFAF8FFFCF9F8FFC9B7A9FFFCF8F6FFFCF8
          F6FFFCF7F6FFFBF6F5FFFBF6F4FFFBF6F3FFC5B3A4FFFAF5F2FFFAF5F1FFFAF4
          F1FFFAF3F0FFF9F3EFFFF9F2EFFF806D59FF0000001C0000000A00000000C2B2
          A5FFCFBEB0FFCFBEB0FFCFBDAFFFCEBDAEFFCDBCAEFFCEBCAEFFCDBBADFFCCBA
          ADFFCCBAACFFCCBAABFFCBB9ABFFCAB8AAFFC9B8AAFFC9B8A9FFC9B7A8FFC9B7
          A8FFC8B5A7FFC7B4A7FFC6B4A6FFC6B4A5FFC6B3A4FFC4B3A4FFC4B2A3FFC4B1
          A2FFC3B0A2FFC2AFA1FFC2AFA0FF806D59FF0000001C0000000A00000000C3B2
          A5FFFFFEFEFFFFFEFEFFFEFEFDFFFFFDFDFFFEFDFDFFFEFDFCFFCDBCADFFFDFC
          FBFFFEFBFBFFFDFCFAFFFDFBFAFFFDFBF9FFFDFAF8FFCAB8A9FFFDF9F7FFFCF8
          F7FFFCF8F6FFFBF8F6FFFBF7F5FFFCF7F5FFC6B4A5FFFBF6F3FFFBF5F3FFFAF5
          F2FFFAF4F1FFFBF4F1FFFAF3F0FF806D59FF0000001C0000000A00000000C5B3
          A6FFFFFEFEFFFFFEFFFFFEFEFDFFFEFEFEFFFFFDFDFFFEFDFCFFCEBCAEFFFEFC
          FCFFFEFCFBFFFDFBFBFFFDFBFBFFFDFBFAFFFDFAF9FFCAB9AAFFFDF9F8FFFCF9
          F7FFFDF8F6FFFCF8F6FFFCF7F6FFFCF7F5FFC7B4A6FFFBF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFBF4F1FFFBF3F0FF8B7966FF0000001C0000000A00000000C5B4
          A6FFFFFFFFFFFEFFFFFFFFFEFEFFFEFEFEFFFFFDFEFFFEFDFDFFCEBDAEFFFEFD
          FCFFFEFDFCFFFEFCFBFFFEFBFAFFFDFBFAFFFDFBFAFFCBB9AAFFFDFAF8FFFDF9
          F8FFFDF9F8FFFCF8F7FFFCF8F6FFFCF7F5FFC7B4A6FFFCF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFAF5F2FFFBF4F1FF988575FF0000001C0000000A00000000C5B4
          A7FFFFFFFFFFFFFFFEFFFFFEFEFFFFFEFFFFFFFEFDFFFFFEFEFFCEBDAFFFFEFD
          FDFFFEFDFCFFFDFCFCFFFEFCFBFFFEFCFBFFFDFBFAFFCBBAABFFFDFAF9FFFDFA
          F9FFFDF9F8FFFCF9F7FFFCF9F7FFFCF8F6FFC7B5A7FFFCF7F5FFFCF6F4FFFBF6
          F3FFFBF5F3FFFBF5F3FFFBF5F2FFAD9D8EFF000000150000000700000000C6B5
          A8FFC5B4A7FFC4B4A7FFC4B3A7FFC4B3A6FFC3B1A4FFC2B1A4FFC1B0A3FFC0AF
          A1FFBFAEA1FFBEAC9FFFBDAB9EFFBCAA9DFFBBA99CFFBAA89AFFB9A89AFFB8A7
          98FFB8A598FFB7A597FFB6A396FFB5A295FFB4A194FFB3A193FFB29F92FFB29E
          90FFB09E90FFB09C8FFFAF9C8EFFAF9C8EFF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000070000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C000000150000000700000000BAA7
          97FFAD9D8EFF988575FF8B7966FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D59FF806D
          59FF806D59FF806D59FF806D59FF806D59FF0000001C0000000900000000BBA7
          98FFFCF7F5FFFBF6F5FFFBF6F4FFFBF6F3FFFBF5F2FFFBF5F2FFC4B1A2FFFAF3
          F1FFFAF2EFFFF9F2EFFFFAF2EEFFFAF1EEFFF9F1EDFFC0AC9EFFF8F0ECFFF9EF
          ECFFF9EFEBFFF8EFEBFFF8EFEAFFF8EEE9FFBDA99AFFF8EDE9FFF8EDE9FFF8ED
          E8FFF7ECE8FFF7ECE7FFF7ECE7FF806D59FF0000001C0000000900000000BCA8
          99FFFBF7F5FFFBF6F5FFFBF6F4FFFCF6F4FFFBF5F3FFFBF5F2FFC4B1A3FFFBF4
          F1FFFAF4F0FFFAF3F0FFFAF2EFFFFAF2EEFFF9F2EEFFC0AD9EFFF8F1ECFFF9F0
          ECFFF9F0ECFFF8EFEBFFF8EFEBFFF8EEEAFFBDA99AFFF8EEE9FFF7EDE8FFF8ED
          E8FFF8EDE8FFF7ECE8FFF8EDE7FF806D59FF0000001C0000000900000000BDA8
          9AFFFCF8F6FFFCF7F5FFFBF7F5FFFCF6F4FFFCF6F4FFFAF5F3FFC5B2A3FFFAF5
          F2FFFAF4F1FFFAF4F0FFF9F3F0FFFAF3EFFFFAF2EFFFC0AE9FFFF9F1EEFFF9F0
          EDFFF8F0ECFFF8F0ECFFF9F0EBFFF8EFEBFFBDAA9BFFF8EEE9FFF8EEE9FFF8ED
          E9FFF7EDE8FFF8EDE8FFF8EDE8FF806D59FF0000001C0000000900000000BDA9
          9BFFFCF8F7FFFCF8F6FFFCF7F5FFFBF6F5FFFBF7F4FFFBF6F3FFC5B3A5FFFAF5
          F2FFFAF4F2FFFAF4F1FFFAF4F1FFFAF3F0FFF9F2EFFFC1AEA0FFF9F2EEFFF9F1
          EDFFF9F0EDFFF9F0ECFFF8F0ECFFF9EFEBFFBEAA9BFFF8EEEAFFF7EEEAFFF8ED
          E9FFF8EDE9FFF7EDE9FFF7EDE8FF806D59FF0000001C0000000900000000BEAA
          9CFFC9B7A9FFC8B6A8FFC8B6A8FFC8B5A7FFC7B5A6FFC6B4A6FFC5B3A5FFC5B3
          A4FFC4B2A4FFC4B2A3FFC3B1A2FFC3B0A2FFC3B0A0FFC2AEA0FFC1AE9FFFC1AE
          9FFFC0AD9FFFBFAD9EFFBFAC9DFFBFAC9CFFBEAB9CFFBEAA9CFFBDAA9BFFBDA9
          9BFFBCA89AFFBCA89AFFBBA899FF806D59FF0000001C0000000900000000BFAC
          9DFFFDF9F8FFFCF9F7FFFCF8F6FFFCF8F6FFFCF7F5FFFCF7F5FFC6B4A5FFFBF5
          F3FFFBF5F3FFFAF4F2FFFAF5F2FFFAF4F1FFFAF4F0FFC2AFA1FFF9F2EFFFF9F2
          EEFFFAF1EEFFF9F1EDFFF9F1EDFFF9F0ECFFBEAB9DFFF9F0ECFFF8EFEBFFF8EE
          EAFFF8EEE9FFF8EEE9FFF7EDE9FF806D59FF0000001C0000000900000000C0AD
          9FFFFDF9F9FFFDF9F8FFFCF9F7FFFCF9F7FFFBF8F6FFFCF7F5FFC7B5A6FFFBF6
          F4FFFCF6F3FFFAF5F3FFFBF5F2FFFBF5F2FFFAF4F1FFC3B0A2FFFAF3F0FFFAF2
          EFFFF9F2EFFFF9F2EEFFF9F1EDFFF8F0ECFFBFAC9DFFF9EFEBFFF9EFECFFF8EF
          EAFFF8EEEAFFF8EEE9FFF8EEEAFF806D59FF0000001C0000000900000000C2AE
          A0FFFCFAF9FFFDFAF8FFFDF9F8FFFCF9F6FFFCF8F6FFFCF7F6FFC7B5A6FFFCF6
          F4FFFBF6F4FFFBF5F3FFFBF5F3FFFBF5F2FFFBF4F1FFC3B0A2FFFAF3F0FFF9F3
          F0FFF9F2EEFFFAF1EFFFF9F2EDFFF9F1EDFFBFAC9EFFF9F0ECFFF9F0ECFFF9EF
          EBFFF8EFEBFFF8EFEAFFF8EEEAFF806D59FF0000001C0000000900000000C2AF
          A0FFFDFBF9FFFDFAF9FFFCFAF8FFFCF9F7FFFCF9F7FFFCF8F6FFC8B6A8FFFCF7
          F5FFFBF7F4FFFBF7F4FFFBF6F3FFFBF5F3FFFBF5F2FFC4B1A2FFFAF4F1FFFAF3
          F0FFFAF2F0FFF9F2EFFFF9F2EEFFF9F1EEFFC0AD9EFFF8F0EDFFF9F0ECFFF8F0
          EBFFF9EFEBFFF8EFEAFFF8EEEAFF806D59FF000000150000000700000000AA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0002000000070000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000900000009000000090000
          0009000000090000000900000009000000090000000700000002000000000000
          0007000000150000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C000000150000000700000000AA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FFAA4B
          19FFAA4B19FFAA4B19FFAA4B19FFAA4B19FF0000001C0000000900000000BEAD
          A0FFFEFDFDFFFDFDFCFFFEFCFBFFFEFCFBFFFEFBFAFFFDFBF9FFCBB9ABFFFDFA
          F9FFFCFAF8FFFCFAF8FFFCF8F7FFFCF8F6FFFCF8F6FFC8B5A7FFFBF7F5FFFCF6
          F4FFFBF6F4FFFBF5F3FFFAF5F2FFFBF4F1FFC3B1A2FFFAF3F0FFFAF3F0FFFAF2
          EFFFF9F2EEFFF9F1EEFFF9F1EEFF806D59FF0000001C0000000900000000BFAE
          A1FFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFBFFFDFCFBFFCBBAACFFFDFB
          F9FFFDFAF9FFFDFAF8FFFDFAF7FFFCF9F7FFFCF8F6FFC8B5A8FFFCF8F5FFFCF7
          F4FFFBF7F4FFFBF6F3FFFBF5F3FFFBF5F2FFC4B1A2FFFAF4F1FFFAF3F0FFFAF3
          F0FFF9F2F0FFF9F2EEFFF9F2EEFF806D59FF0000001C0000000900000000C1B0
          A2FFFEFEFEFFFFFDFDFFFFFDFCFFFEFDFCFFFEFCFBFFFDFBFBFFCCBBADFFFEFB
          FAFFFEFBF9FFFDFAF8FFFDFAF8FFFCF9F8FFFDF9F7FFC8B6A8FFFCF8F6FFFCF7
          F5FFFBF7F4FFFBF6F4FFFBF6F4FFFBF5F2FFC4B2A3FFFBF5F1FFFAF4F1FFFAF4
          F0FFFAF3EFFFFAF3EFFFFAF2EEFF806D59FF0000001C0000000900000000C2B0
          A3FFFFFEFEFFFEFDFDFFFEFDFDFFFEFDFDFFFEFCFCFFFEFCFCFFCDBBACFFFEFB
          FBFFFDFAF9FFFDFAF9FFFCFAF9FFFDFAF8FFFCF9F8FFC9B7A9FFFCF8F6FFFCF8
          F6FFFCF7F6FFFBF6F5FFFBF6F4FFFBF6F3FFC5B3A4FFFAF5F2FFFAF5F1FFFAF4
          F1FFFAF3F0FFF9F3EFFFF9F2EFFF806D59FF0000001C0000000900000000C2B2
          A5FFCFBEB0FFCFBEB0FFCFBDAFFFCEBDAEFFCDBCAEFFCEBCAEFFCDBBADFFCCBA
          ADFFCCBAACFFCCBAABFFCBB9ABFFCAB8AAFFC9B8AAFFC9B8A9FFC9B7A8FFC9B7
          A8FFC8B5A7FFC7B4A7FFC6B4A6FFC6B4A5FFC6B3A4FFC4B3A4FFC4B2A3FFC4B1
          A2FFC3B0A2FFC2AFA1FFC2AFA0FF806D59FF0000001C0000000900000000C3B2
          A5FFFFFEFEFFFFFEFEFFFEFEFDFFFFFDFDFFFEFDFDFFFEFDFCFFCDBCADFFFDFC
          FBFFFEFBFBFFFDFCFAFFFDFBFAFFFDFBF9FFFDFAF8FFCAB8A9FFFDF9F7FFFCF8
          F7FFFCF8F6FFFBF8F6FFFBF7F5FFFCF7F5FFC6B4A5FFFBF6F3FFFBF5F3FFFAF5
          F2FFFAF4F1FFFBF4F1FFFAF3F0FF806D59FF0000001C0000000900000000C5B3
          A6FFFFFEFEFFFFFEFFFFFEFEFDFFFEFEFEFFFFFDFDFFFEFDFCFFCEBCAEFFFEFC
          FCFFFEFCFBFFFDFBFBFFFDFBFBFFFDFBFAFFFDFAF9FFCAB9AAFFFDF9F8FFFCF9
          F7FFFDF8F6FFFCF8F6FFFCF7F6FFFCF7F5FFC7B4A6FFFBF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFBF4F1FFFBF3F0FF8B7966FF0000001C0000000900000000C5B4
          A6FFFFFFFFFFFEFFFFFFFFFEFEFFFEFEFEFFFFFDFEFFFEFDFDFFCEBDAEFFFEFD
          FCFFFEFDFCFFFEFCFBFFFEFBFAFFFDFBFAFFFDFBFAFFCBB9AAFFFDFAF8FFFDF9
          F8FFFDF9F8FFFCF8F7FFFCF8F6FFFCF7F5FFC7B4A6FFFCF7F4FFFBF6F3FFFBF5
          F3FFFBF5F2FFFAF5F2FFFBF4F1FF988575FF0000001C0000000900000000C5B4
          A7FFFFFFFFFFFFFFFEFFFFFEFEFFFFFEFFFFFFFEFDFFFFFEFEFFCEBDAFFFFEFD
          FDFFFEFDFCFFFDFCFCFFFEFCFBFFFEFCFBFFFDFBFAFFCBBAABFFFDFAF9FFFDFA
          F9FFFDF9F8FFFCF9F7FFFCF9F7FFFCF8F6FFC7B5A7FFFCF7F5FFFCF6F4FFFBF6
          F3FFFBF5F3FFFBF5F3FFFBF5F2FFAD9D8EFF000000150000000700000000C6B5
          A8FFC5B4A7FFC4B4A7FFC4B3A7FFC4B3A6FFC3B1A4FFC2B1A4FFC1B0A3FFC0AF
          A1FFBFAEA1FFBEAC9FFFBDAB9EFFBCAA9DFFBBA99CFFBAA89AFFB9A89AFFB8A7
          98FFB8A598FFB7A597FFB6A396FFB5A295FFB4A194FFB3A193FFB29F92FFB29E
          90FFB09E90FFB09C8FFFAF9C8EFFAF9C8EFF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000050000000200000000000000000000
          0000000000000000000000000000000000020000000500000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000070000000E0000000700000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000007000000150000001C0000001C0000
          001C0000000000000021736252FF0000002D000000240000001C000000150000
          000700000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFAD9D8EFF988575FF8B7966FF8572
          60FF83705DFF816D5AFF806D59FF806D59FF806D59FF806D59FF0000001C0000
          00090000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF8F7FFFCF7F5FFFBF7F4FFFBF6
          F3FFFBF5F2FFFBF5F1FFFAF4F0FFFAF3EFFFFAF2EEFF806D59FF000000240000
          00120000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF9F6FFFCF8F5FFFCF7F4FFFCF6
          F3FFFBF5F2FFFBF4F1FFFAF4F1FFFAF3EFFFFAF3EEFF806D59FF0000002D0000
          001BBB6337FFB55C2FFFA64819FFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFCF7F5FFFBF7F4FFFBF6
          F4FFFBF5F2FFFBF5F1FFFBF4F0FFFAF3F0FFFAF3EFFF806D59FF756453FF7E6C
          5AFFC67346FFF0BDA1FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF8F6FFFCF7F5FFFCF7F5FFFBF6
          F4FFFBF5F2FFFBF5F2FFFAF5F1FFFAF3EFFFFAF2EFFF806D59FF0000001C0000
          0009D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFDF8F6FFFCF8F5FFFBF6
          F4FFFBF6F3FFFBF5F2FFFAF4F1FFFAF3F0FFFAF3EFFF806D59FF0000001D0000
          0010000000070000000200000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFF000000070000
          0002000000000000000000000000D8916AFF1D140F2600000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFF000000070000
          0002000000000000000000000000D8916AFF1D140F2600000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFDF8F6FFFCF8F5FFFBF6
          F4FFFBF6F3FFFBF5F2FFFAF4F1FFFAF3F0FFFAF3EFFF806D59FF0000001D0000
          0010000000070000000200000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF8F6FFFCF7F5FFFCF7F5FFFBF6
          F4FFFBF5F2FFFBF5F2FFFAF5F1FFFAF3EFFFFAF2EFFF806D59FF0000001C0000
          0009D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFCF7F5FFFBF7F4FFFBF6
          F4FFFBF5F2FFFBF5F1FFFBF4F0FFFAF3F0FFFAF3EFFF806D59FF756453FF7E6C
          5AFFC67346FFF0BDA1FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF9F6FFFCF8F5FFFCF7F4FFFCF6
          F3FFFBF5F2FFFBF4F1FFFAF4F1FFFAF3EFFFFAF3EEFF806D59FF0000002D0000
          001BBB6337FFB55C2FFFA64819FFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF8F7FFFCF7F5FFFBF7F4FFFBF6
          F3FFFBF5F2FFFBF5F1FFFAF4F0FFFAF3EFFFFAF2EEFF806D59FF000000240000
          00120000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFAD9D8EFF988575FF8B7966FF8572
          60FF83705DFF816D5AFF806D59FF806D59FF806D59FF806D59FF0000001C0000
          00090000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000007000000150000001C0000001C0000
          001C0000000000000021736252FF0000002D000000240000001C000000150000
          000700000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFF1D140F2B00000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFEDB696FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA84A1BFFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFF1D140F2B00000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDA9871FFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFEDB696FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA84A1BFFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000070000000E0000000700000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000050000000200000000000000000000
          0000000000000000000000000000000000020000000500000003000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000050000000200000000000000000000
          0000000000000000000000000000000000020000000500000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000070000000E0000000700000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA84A1BFFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFEDB696FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDA9871FFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFF1D140F2B00000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA84A1BFFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFEDB696FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFF1D140F2B00000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000007000000150000001C0000001C0000
          001C0000000000000021736252FF0000002D000000240000001C000000150000
          000700000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFAD9D8EFF988575FF8B7966FF8572
          60FF83705DFF816D5AFF806D59FF806D59FF806D59FF806D59FF0000001C0000
          00090000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF8F7FFFCF7F5FFFBF7F4FFFBF6
          F3FFFBF5F2FFFBF5F1FFFAF4F0FFFAF3EFFFFAF2EEFF806D59FF000000240000
          00120000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF9F6FFFCF8F5FFFCF7F4FFFCF6
          F3FFFBF5F2FFFBF4F1FFFAF4F1FFFAF3EFFFFAF3EEFF806D59FF0000002D0000
          001BBB6337FFB55C2FFFA64819FFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFCF7F5FFFBF7F4FFFBF6
          F4FFFBF5F2FFFBF5F1FFFBF4F0FFFAF3F0FFFAF3EFFF806D59FF756453FF7E6C
          5AFFC67346FFF0BDA1FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF8F6FFFCF7F5FFFCF7F5FFFBF6
          F4FFFBF5F2FFFBF5F2FFFAF5F1FFFAF3EFFFFAF2EFFF806D59FF0000001C0000
          0009D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFDF8F6FFFCF8F5FFFBF6
          F4FFFBF6F3FFFBF5F2FFFAF4F1FFFAF3F0FFFAF3EFFF806D59FF0000001D0000
          0010000000070000000200000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFF000000070000
          0002000000000000000000000000D8916AFF1D140F2600000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000050000000200000000000000000000
          0000000000000000000000000000000000020000000500000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000070000000E0000000700000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000007000000150000001C0000001C0000
          001C0000000000000021736252FF0000002D000000240000001C000000150000
          000700000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFAD9D8EFF988575FF8B7966FF8572
          60FF83705DFF816D5AFF806D59FF806D59FF806D59FF806D59FF0000001C0000
          00090000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF8F7FFFCF7F5FFFBF7F4FFFBF6
          F3FFFBF5F2FFFBF5F1FFFAF4F0FFFAF3EFFFFAF2EEFF806D59FF000000240000
          00120000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF9F6FFFCF8F5FFFCF7F4FFFCF6
          F3FFFBF5F2FFFBF4F1FFFAF4F1FFFAF3EFFFFAF3EEFF806D59FF0000002D0000
          001BBB6337FFB55C2FFFA84A1BFFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFCF7F5FFFBF7F4FFFBF6
          F4FFFBF5F2FFFBF5F1FFFBF4F0FFFAF3F0FFFAF3EFFF806D59FF756453FF7E6C
          5AFFC67346FFEDB696FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF8F6FFFCF7F5FFFCF7F5FFFBF6
          F4FFFBF5F2FFFBF5F2FFFAF5F1FFFAF3EFFFFAF2EFFF806D59FF0000001C0000
          0009D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFDF8F6FFFCF8F5FFFBF6
          F4FFFBF6F3FFFBF5F2FFFAF4F1FFFAF3F0FFFAF3EFFF806D59FF000000150000
          0007000000000000000000000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFF000000070000
          0002000000000000000000000000D8916AFF1D140F2B00000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000007000000150000001C0000001C0000
          001C0000000000000021736252FF0000002D000000240000001C000000150000
          000700000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFAD9D8EFF988575FF8B7966FF8572
          60FF83705DFF816D5AFF806D59FF806D59FF806D59FF806D59FF0000001C0000
          00090000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF8F7FFFCF7F5FFFBF7F4FFFBF6
          F3FFFBF5F2FFFBF5F1FFFAF4F0FFFAF3EFFFFAF2EEFF806D59FF000000240000
          00120000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFCF9F6FFFCF8F5FFFCF7F4FFFCF6
          F3FFFBF5F2FFFBF4F1FFFAF4F1FFFAF3EFFFFAF3EEFF806D59FF0000002D0000
          001BBB6337FFB55C2FFFA64819FFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFCF7F5FFFBF7F4FFFBF6
          F4FFFBF5F2FFFBF5F1FFFBF4F0FFFAF3F0FFFAF3EFFF806D59FF756453FF7E6C
          5AFFC67346FFF0BDA1FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF8F6FFFCF7F5FFFCF7F5FFFBF6
          F4FFFBF5F2FFFBF5F2FFFAF5F1FFFAF3EFFFFAF2EFFF806D59FF0000001C0000
          0009D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFFDF9F7FFFDF8F6FFFCF8F5FFFBF6
          F4FFFBF6F3FFFBF5F2FFFAF4F1FFFAF3F0FFFAF3EFFF806D59FF0000001D0000
          0010000000070000000200000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D
          8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFFB49D8CFF000000070000
          0002000000000000000000000000D8916AFF1D140F2600000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF0000000E0000000700000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA84A1BFFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFEDB696FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDA9871FFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFF1D140F2B00000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA84A1BFFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFEDB696FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000000000000000000000000000D8916AFF1D140F2B00000006000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          000000000000000000000000000000000007000000110000000E000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA64819FFAB552BFFDA9871FF903A10E91407013D0000
          0014000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFF0BDA1FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000002000000070000
          0009000000090000000983705DFF000000180000001100000009000000090000
          0009000000070000000200000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000D8916AFF1D140F2600000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          00000000000200000007000000099F3E0FFF1F0C02490000001C0000000E0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000150000001100000009000000090000
          00090000000E000000180000001C9F3E0FFFAB552BFF190902460000001A0000
          000D000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001800000013000000130000
          0013BB6337FFB55C2FFFA64819FFAB552BFFDA9871FF903A10E91407013D0000
          0014000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF83705DFF83705DFF83705DFF83705DFF8370
          5DFFC67346FFF0BDA1FFE7A985FFE7A985FFE6A581FFEAA581FB963A10F00E05
          0124000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000130000000900000000000000000000
          0000D8916AFFD8916AFFD8916AFFDCA07BFFE6A581FFC67346FF190D062E0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000180000001100000009000000090000
          0009000000070000000200000000D8916AFFC67346FF1A0F092D000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000083705DFF000000000000000000000000000000000000
          0000000000000000000000000000D8916AFF1D140F2600000003000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          20000000000000100000000000000000000000000000000000007698B6F07194
          B3F0283B4B6B0001010100000000000000000000000000000000000000000000
          00001C2632463B6677AF289883DC15BE87FE14BE83FF17B880FF357F7FCF1D2A
          364C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000799BBAF48AAB
          C7FF7A9FC0FF2E43557800000000000000000000000000000000000000001B25
          3043349089DC13BF87FA0BC883FF0DC382FF0EC07CFF0CBC75FF0AB86FFF3886
          83DB1425293F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000425D76A57093
          B3EF82A5C4FF759ABCFC2C3F507100000000000000000000000003040507366C
          75B10EC787FF0DC686FF0FC284FF0FBF7FFF0FBC7AFF0FB874FF0CB56DFF0FB1
          6AFF36606DA50000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002130
          3D566387A7E37BA0C0FF7199BCFF2A3C4C6B0102020300000000090C1016398E
          8DDC09C984FF10C385FF0FC081FF0EBD7CFF0EBA76FF0EB670FF0DB26BFF07B0
          61FF2E8F7AE4141B233000000000000000000D121821222E3E5430515E8A3669
          73AE2C47557A222F3D550C151823000000000000000000000000000000000000
          000011181F2B4D6B87BA7299BCFF6C93B8FC3E58719C0A0E121907090C103886
          87D00AC883FF10C282FF0FBE7DFF0EBA78FF0EB772FF0DB36CFF0DAF67FF07AE
          5DFF28A37BFF19212C3B000000001D2834473A727BBE28A281F113B177F70CBE
          78FF17B07DF11EB388F9338581CC1D2834470000000000000000000000000000
          00000000000006090C103D586F996288AAEB6E92BBFF64799BCA465D769E3291
          88DA0BC580FF0FBF7FFF0EBC79FF0EB874FF0DB56EFF0DB169FF0DAE64FF09AC
          5DFF326F6EB7090C10151C263243358880D90CB56DFE09BA6FFF0CBA75FF0FBC
          7AFF0DBF7CFF0BC37EFF0DC381FE32A091EB1B252F4100000000000000000000
          00000000000000000000000000001E29364945838DCF51BCA3F535BD98F80CBF
          81FB0FC080FF0FBD7CFF0EB976FF0EB671FF0DB26BFF0DAF66FF0AAD5EFF169D
          64F0273C49681D26334432847BD209B268FE0DB56DFF0EB772FF0EBA76FF0FBC
          7AFF0FBE7DFF0FC080FF0DC482FF17BF8AFF2F48587B00000000000000000000
          000000000000000000000000000018222B3B208E76C704C680FE0DBE81FA14BD
          84FD11BA7DFB0EBB78FF0EB772FF0DB36DFF0DB067FF0CAD62FF0AAA5DFE2F68
          67AB1E263343357474BC09B163FF0CB26AFF0EB56EFF0EB772FF0EB976FF0FBC
          79FF0FBE7DFF0FC080FF0EC382FF13BA83F6355F6C9B02020304000000000000
          00000000000000000000030506082D5B62900DC786FF10C587FF10C283FF10BD
          7EFD12B67AF913B375F90FB36EFD0CB168FF0CAE64FF06AD5AFF258B6CDA2C3D
          4F6937656FA810A764F60BB065FF0DB26BFF0EB56EFF0EB772FF0EB975FF0FBB
          79FF0FBE7CFF0FC080FF0DC381FF19BF8BFF3047587900000000000000000000
          000000000000000000001319212C2A9281D10BC885FF10C484FF10C080FF0EBC
          7BFF0DBA75FF10B26FFB11AB6BF611AB68FA08AD5CFF179E65F03954688F3559
          669414A566F809AF5FFF0DAF67FF0DB16AFF0EB46EFF0EB672FF0EB975FF0FBB
          79FF0FBD7CFF0DC07EFF07C67DFF2FA18EE819222B3A00000000000000000000
          000000000000000000002E4453711DAC85E80DC584FF0FC182FF0FBE7DFF0FBA
          78FF0EB772FF0DB46CFF0DB067FF0DAC63FE1C8760D0355B669732495A7B179D
          65EE08AD5CFF0DAD63FF0CAF66FF0DB169FF0DB46CFF0EB670FF0EB874FF0EBB
          78FF0CBE7AFF13B77FF7329B8BE2354F61840000000000000000000000000000
          0000000000000000000038616F9C10BB82F40FC283FF0FBF7FFF0FBC7AFF0EB8
          74FF0EB56FFF0BB267FF08AF5FFF00AD53FF367875C2333E566A2A625E9D08AC
          5AFF0FA660F70EA761F610AC68FC10AF6CFE11B26FFE11B372FD11B675FD0EB7
          75F9239F7DE2496E86B61D253240000000000000000000000000000000000000
          00000000000000000000304A5A7A18A97EE40DC17FFF0FBD7BFF0EBA75FF0BB6
          6DFF07B464FF0FA865F62B8C73D836676EA7476D82B4486581A91B8A60D20BA8
          5BFA0FA460F40FA562F410AB66FB10AF6BFE10B16FFE11B472FE11B575FE0EB6
          75FA1FA57EE93C767FB93358658D1A242F3D0203040500000000000000000000
          00000000000000000000222D3B4C29A58AEA06C277FF0ABC74FF0AB56CFB1AA7
          72F3307B73C134546189202A3746191F2B35405E759B46647EA63A9187E505AB
          57FF0AAB5FFF0CAD62FF0CAF65FF0DB068FF0DB36BFF0DB66FFF0EB773FF0EB9
          77FF0DBD79FF0AC17AFF11BE80FC2CAC92F3385C6C94070A0C10000000000000
          00000000000000000000070A0C103C727CB222A07DE320A178E6367677B83044
          5670171D27311E27344138586890347E76C71C9265DF23A175F8496882AB357C
          75C509AC5DFF0BAD61FF0DAE65FF0DB068FF0DB36BFF0EB56FFF0EB772FF0EBA
          76FF0FBC7AFF0FBE7EFF0EC180FF09C67FFF20BC90FF2F455570000000000000
          00000000000000000000000000000B10141A253441552939485D232D3C4B2D42
          526C37636C9E32937EE314A767FB09AB5CFE0DA45DF504AC56FF2E8F78DF3745
          5E732E9279E207AD5BFF0DAE64FF0DB068FF0DB36BFF0EB46EFF0EB772FF0EB9
          76FF0EBC79FF0FBE7DFF0FC080FF0FC283FF0BC683FF3E8B8FD1000000000000
          000000000000000000000000000000000000080C0F1335506181308A7AD317A8
          6EF80DAF66FF08AF5EFF0BAE61FF0FAA63FC10AA64FC0BAC5EFF0CAB5FFF3E72
          7AB53B546A8919A96EFE08AF5FFF0DB067FF0DB36AFF0EB56EFF0EB672FF0EB9
          76FF0EBC79FF0FBE7DFF0FC080FF0FC283FF0AC682FF2FA38FE5000000000000
          00000000000000000000000000002D3E4E6438807EC517AA72F507B465FF0BB3
          68FF0CB168FF0CB067FF0DAF66FF11A966F80FAB63FB0CAD62FF09AD5EFF1AA6
          6DF93C566C8C417680BB11AD69FF09B064FF0DB26AFF0DB46DFF0EB771FF0EB9
          75FF0FBB79FF0FBE7CFF0FC080FF0FC283FF0BC582FF26A387E1000000000000
          0000000000000101020233485B752E9B84E40AB86EFF0AB76EFF0EB56FFF0DB4
          6DFF0DB36CFF0DB26BFF0DB068FD12AA6AF80DB066FF0DAE65FF0DAE64FF06AE
          5DFF2F997EE92E3E50653D7B7EC00CAF66FF0BB267FF0EB46DFF0EB671FF0EB8
          75FF0EBB78FF0FBD7CFF0FBF7FFF10C283FF09C680FF329388D2000000000000
          0000000000002D4150672D9F86E607BC71FF0DB974FF0EB874FF0EB773FF0DB6
          71FF0EB570FF0DB56EFF10B26EFD12AD6CF90DB169FF0DB069FF0DB068FF0CAF
          65FF0AAF62FF3C6F77AD19212B353F8787D10CB069FE0AB46AFF0EB670FF0EB8
          74FF0EBB78FF0FBD7BFF0FBF7FFF0BC37FFF17C08AFF3251607F000000000000
          000010151B223D918DD507C076FF0FBC7BFF0FBB79FF0EBB77FF0EBA76FF0EB9
          75FF0EB774FF0EB772FF11AF70F710B26FFB0DB56DFF0EB36CFF0DB26BFF0DB2
          6AFF08B163FF24AB7CFD28374658161D252E3B8682CC11B26FFF07B76BFF0CB8
          72FF0CBB75FF0BBE78FF07C179FF1BBC89FE3C6877A00A0E1217000000000000
          00002837465723BA8FFF0BC17CFF0FBE7EFF0FBD7CFF0FBD7BFF0FBC7AFF0FBB
          79FF0EBA77FF0EB976FF14B778FD0FB673FE0EB671FF0EB570FF0DB56FFF0EB4
          6EFF0DB36CFF07B365FF417983BB000000000F151B223D6875A127A781F212B7
          77FF16B87CFD24AF87F53D908DD43C6172960E13181E00000000000000000000
          00002B3B4C5E23BC92FF0BC37FFF10C181FF0FC080FF0FBF7FFF0FBE7DFF0FBE
          7CFF0EBC7AFF0FB878FB13B479F80EB976FF0EB974FF0EB773FF0EB772FF0EB6
          71FF0DB56FFF08B569FF329C85E71C26313C0000000002030304212E3A493557
          64863C6272972939485A12182027000000000000000000000000000000000000
          00002836465622BD93FF0BC582FF10C284FF0FC183FF0FC181FF0FC081FF0FBF
          7FFF0EBE7DFE15BA7FFC10BB7BFD0FBC7AFF0EBA78FF0EBA77FF0EB976FF0EB9
          74FF0EB873FF0AB76FFF1AB37BFF324959720000000000000000000000000001
          0101010102020000000000000000000000000000000000000000000000000000
          0000171E273033A191E208C781FF10C486FF0FC385FF0FC284FF0FC283FF07C4
          7CFF2B9983D6466E81A91BB483F40CBF7BFF0EBD7CFF0FBC7AFF0FBC79FF0EBB
          78FF0EB977FF0EB975FF0AB970FF44858CC80000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000004067799C22BF94FF0CC685FF0BC684FF0AC683FF0DC583FF2EAD
          92EF3653637F2733425026B38EF50AC27DFF0FBF7FFF0FBE7EFF0FBE7DFF0FBD
          7BFF0FBC7BFF0FBB79FF07BC72FF3F888ACA0506080A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000506080A3D576B85448890C62EB498F62EAB93EB3F858AC1384B
          6075010102022A3E4C5F20B68BF40CC381FF0FC182FF0FC081FF0FC080FF0FBF
          7FFF0FBF7EFF0FBE7CFF08BE76FF3F8689C506080B0D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000A0E11151C252F39141A22290A0E11150000
          0000000000002E40506322BA91F90CC583FF0FC385FF10C383FF10C183FF0FC1
          82FF0FC081FF0FBF7FFF08C178FF448C92CD0304050600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001E28343E32B79DFA0AC783FF10C488FF10C487FF10C486FF0FC3
          84FF10C283FF0AC37FFF1FB68AF7354F60780000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010202447987B20EC686FF0DC686FF10C588FF10C588FF0FC5
          86FF0AC582FF11C185FE478791C4090C0F120000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001B242E37449D9DDF11C588FF08C782FF09C783FF0DC6
          85FF29B393F2467A89B3171F2830000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000182A2F3D437784AE40A39DE53C9B95D94183
          8ABC3044546607090C0E00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
  object dlgOpen: TOpenDialog
    Filter = #25903#25345#30340#23383#24149#25991#20214'|*.srt;*.ssa;*.ass|'#25152#26377#25991#20214'|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 248
    Top = 184
  end
  object dlgSave: TSaveDialog
    DefaultExt = 'srt'
    Filter = 'SRT'#23383#24149'|*.srt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 296
    Top = 184
  end
  object imgSmallIcons: TcxImageList
    FormatVersion = 1
    DesignInfo = 15729040
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000B0A090FF604830FF604830FF604830FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF000000000000000000000000000000000000
          0000B0A090FFFFFFFFFFB0A090FFB0A090FFB0A090FFB0A090FFB0A090FFB0A0
          90FFB0A090FFB0A090FF604830FF000000000000000000000000000000000000
          0000B0A090FFFFFFFFFFFFFFFFFFFFF8FFFFF0F0F0FFF0E8E0FFF0E0D0FFE0D0
          D0FFE0C8C0FFB0A090FF604830FF000000000000000000000000000000000000
          0000B0A090FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFF0E0E0FFF0D8
          D0FFE0D0C0FFB0A090FF604830FF000000000000000000000000000000000000
          0000B0A090FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0FFF0E8E0FFF0E0
          E0FFE0D8D0FFB0A090FF604830FF000000000000000000000000000000000000
          0000C0A890FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFF0E8
          E0FFF0D8D0FFB0A090FF604830FF000000000000000000000000000000000000
          0000C0A8A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0E8
          E0FFF0E0E0FFB0A090FF604830FF000000000000000000000000000000000000
          0000C0B0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFFFF0F0
          F0FFF0E8E0FFB0A090FF604830FF000000000000000000000000000000000000
          0000D0B0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8
          F0FFF0F0F0FFB0A090FF604830FF000000000000000000000000000000000000
          0000D0B8A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB0A090FFB0A090FF604830FF000000000000000000000000000000000000
          0000D0B8B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0A0
          90FF604830FF604830FF604830FF000000000000000000000000000000000000
          0000D0C0B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0A8
          90FFD0C8C0FF604830FF75635A90000000000000000000000000000000000000
          0000E0C0B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0A8
          A0FF604830FF75635A9000000000000000000000000000000000000000000000
          0000E0C0B0FFE0C0B0FFE0C0B0FFE0C0B0FFE0C0B0FFD0C0B0FFD0B8B0FFD0B0
          A0FF75635A900000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000708890FF6080
          90FF607880FF507080FF506070FF405860FF404850FF303840FF203030FF2020
          30FF101820FF101010FF101020FF000000000000000000000000708890FF90A0
          B0FF70B0D0FF0090D0FF0090D0FF0090D0FF0090C0FF1088C0FF1080B0FF1080
          B0FF2078A0FF207090FF204860FF0A0F0F500000000000000000808890FF80C0
          D0FF90A8B0FF80E0FFFF60D0FFFF50C8FFFF50C8FFFF40C0F0FF30B0F0FF30A8
          F0FF20A0E0FF1090D0FF206880FF151B20B000000000000000008090A0FF80D0
          F0FF90A8B0FF90C0D0FF70D8FFFF60D0FFFF60D0FFFF50C8FFFF50C0FFFF40B8
          F0FF30B0F0FF30A8F0FF1088D0FF204860FF02050620000000008090A0FF80D8
          F0FF80C8E0FF90A8B0FF80E0FFFF70D0FFFF60D8FFFF60D0FFFF60D0FFFF50C8
          FFFF40C0F0FF40B8F0FF30B0F0FF206880FF08283690000000008098A0FF90E0
          F0FF90E0FFFF90A8B0FF90B8C0FF70D8FFFF60D8FFFF60D8FFFF60D8FFFF60D0
          FFFF50D0FFFF50C8FFFF40B8F0FF30A0E0FF3B5968F00F1215308098A0FF90E0
          F0FFA0E8FFFF80C8E0FF90A8B0FF80E0FFFF80E0FFFF80E0FFFF80E0FFFF80E0
          FFFF80E0FFFF80E0FFFF70D8FFFF70D8FFFF50A8D0FF323C46A090A0A0FFA0E8
          F0FFA0E8FFFFA0E8FFFF90B0C0FF90B0C0FF90A8B0FF90A8B0FF80A0B0FF80A0
          B0FF8098A0FF8098A0FF8090A0FF8090A0FF808890FF708890FF90A0B0FFA0E8
          F0FFA0F0FFFFA0E8FFFFA0E8FFFF80D8FFFF60D8FFFF60D8FFFF60D8FFFF60D8
          FFFF60D8FFFF60D8FFFF708890FF00000000000000000000000090A0B0FFA0F0
          F0FFB0F0F0FFA0F0FFFFA0E8FFFFA0E8FFFF70D8FFFF90A0A0FF8098A0FF8098
          A0FF8090A0FF809090FF708890FF00000000000000000000000090A8B0FFA0D0
          E0FFB0F0F0FFB0F0F0FFA0F0FFFFA0E8FFFF90A0B0FF40545880000000000000
          0000000000000000000000000000906850FF906850FF906850FF2D34375090A8
          B0FF90A8B0FF90A8B0FF90A8B0FF90A8B0FF515E639000000000000000000000
          000000000000000000000000000032252350906850FF906850FF000000000000
          0000000000000000000000000000000000000000000000000000000000009078
          60FF36271E60000000000A070710A09080FF322A2350907860FF000000000000
          0000000000000000000000000000000000000000000000000000000000002826
          2040A09080FFA08880FFB09880FF504840800000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004120
          2350C06860FFB05850FFA05050FFA05050FFA05050FF904850FF904840FF9048
          40FF804040FF803840FF803840FF703840FF703830FF0000000000000000D068
          70FFF09090FFE08080FFB04820FF403020FFC0B8B0FFC0B8B0FFD0C0C0FFD0C8
          C0FF505050FFA04030FFA04030FFA03830FF703840FF0000000000000000D070
          70FFFF98A0FFF08880FFE08080FF705850FF404030FF907870FFF0E0E0FFF0E8
          E0FF908070FFA04030FFA04040FFA04030FF803840FF0000000000000000D078
          70FFFFA0A0FFF09090FFF08880FF705850FF000000FF404030FFF0D8D0FFF0E0
          D0FF807860FFB04840FFB04840FFA04040FF804040FF0000000000000000D078
          80FFFFA8B0FFFFA0A0FFF09090FF705850FF705850FF705850FF705850FF7060
          50FF806860FFC05850FFB05050FFB04840FF804040FF0000000000000000E080
          80FFFFB0B0FFFFB0B0FFFFA0A0FFF09090FFF08880FFE08080FFE07880FFD070
          70FFD06870FFC06060FFC05850FFB05050FF904840FF0000000000000000E088
          90FFFFB8C0FFFFB8B0FFD06060FFC06050FFC05850FFC05040FFB05030FFB048
          30FFA04020FFA03810FFC06060FFC05850FF904840FF0000000000000000E090
          90FFFFC0C0FFD06860FFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFF0E8E0FFF0D8
          D0FFE0D0C0FFE0C8C0FFA03810FFC06060FF904850FF0000000000000000E098
          A0FFFFC0C0FFD07070FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFF0E8
          E0FFF0D8D0FFE0D0C0FFA04020FFD06860FFA05050FF0000000000000000F0A0
          A0FFFFC0C0FFE07870FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0F0
          F0FFF0E8E0FFF0D8D0FFB04830FFD07070FFA05050FF0000000000000000F0A8
          A0FFFFC0C0FFE08080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8
          F0FFF0F0F0FFF0E8E0FFB05030FFE07880FFA05050FF0000000000000000F0B0
          B0FFFFC0C0FFF08890FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFF8F0FFF0F0F0FFC05040FF603030FFB05850FF0000000000000000F0B0
          B0FFFFC0C0FFFF9090FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFF8F0FFC05850FFB05860FFB05860FF0000000000000000F0B8
          B0FFF0B8B0FFF0B0B0FFF0B0B0FFF0A8B0FFF0A0A0FFE098A0FFE09090FFE090
          90FFE08890FFE08080FFD07880FFD07870FFD07070FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003C1E1E50C068
          60FFB05850FFA05050FFA05050FFA05050FF904850FF904840FF904840FF8040
          40FF906060FFB08080FFB0A8B0FF303890FF303890FF12162430C06060FFF090
          90FFE08080FFB04820FF403020FFC0B8B0FFC0B8B0FFD0C0C0FFD0C8C0FF7068
          60FFC08880FFC0A8B0FF303890FF6070B0FF6078D0FF303890FFC06060FFFF98
          A0FFF08880FFE08080FF705850FF404030FF907870FFF0E0E0FFF0E8E0FFC0B0
          A0FFB0A8B0FF304850FF808880FF7088D0FF7090E0FF6070B0FFC06860FFFFA0
          A0FFF09090FFF08880FF705850FF000000FF504040FFF0E0D0FFF0E8E0FFB0B8
          C0FF304850FF5098B0FF70C8E0FFD0B8B0FF6070B0FF23283C50C06870FFFFA8
          B0FFFFA0A0FFF09090FF706050FF706050FF807060FFA09890FFB0B8B0FF3048
          50FF5098B0FF60C0D0FF90E0F0FF408090FF20282C4000000000C07070FFFFA8
          B0FFFFA0A0FFFFA0A0FFF09090FFF09890FFF0A8B0FFD0C0C0FF304850FF5098
          B0FF60C0D0FF90E0F0FF408090FFC0B8C0FF0000000000000000C07070FFFFA8
          B0FFF098A0FFD06860FFD06860FFD08880FFC0A8A0FF304850FF5098B0FF60C0
          D0FF90E0F0FF408090FFD0C0C0FFC09090FF0000000000000000C07870FFFFA8
          B0FFD06860FFFFFFFFFFFFFFFFFFFFF8FFFF304850FF5090B0FF70D0E0FF90E0
          F0FF50A0B0FFC0B8B0FFE098A0FFA06860FF0000000000000000C07880FFFFA8
          B0FFD07070FFFFFFFFFFFFFFFFFFFFFFFFFFA0A0A0FFFFFFFFFF90B8C0FF50A0
          B0FFD0D8E0FFD08870FFD08080FFA05850FF0000000000000000D08080FFFFA8
          B0FFD07070FFFFFFFFFF808080FFB0A8A0FF505850FFA0A0A0FFA0A0A0FFE0E0
          E0FFF0E8E0FFC06050FFE07880FFA05050FF0000000000000000D08080FFFFA8
          B0FFE07870FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFFFFFF0
          F0FFF0E8E0FFB05840FFE07880FFA05050FF0000000000000000D08880FFFFA8
          B0FFE08080FFFFFFFFFFC0B8B0FFC0B8B0FFC0C0C0FFC0C0C0FFC0C0C0FFC0B8
          C0FFF0F0F0FFC05040FF603030FFB05850FF0000000000000000D08880FFFFA8
          B0FFF08890FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFF8F0FFC05850FFB05860FFB05860FF0000000000000000D08890FFD088
          80FFD08880FFD08080FFD08080FFD07880FFC07880FFC07870FFC07070FFC070
          70FFC06870FFC06870FFC06860FFC06860FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0A090FF6040
          30FF604030FF604030FF604030FF604030FF604030FF604030FF604030FF6040
          30FF604030FF604030FF604030FF604030FF604030FF00000000C0A090FFFFFF
          FFFFFFE0D0FFFFD8C0FFFFD8C0FFFFD8C0FFFFD8C0FFC0B0A0FFFFFFFFFFFFD8
          C0FFFFD8C0FFFFD8C0FFFFD8C0FFFFD8C0FF604030FF00000000C0A890FFFFFF
          FFFFA07060FF805840FF705040FFFFFFFFFFFFD8C0FFC0B0A0FFFFFFFFFFC0A0
          90FFC0A090FFC09880FFC09880FFFFE8D0FF604030FF00000000C0A890FFFFFF
          FFFFF0D8D0FFA06850FFF0D0D0FFFFFFFFFFFFD8C0FFC0B0A0FFFFFFFFFFFFF8
          F0FFFFF8F0FFFFF0F0FFFFF0F0FFFFE8D0FF604030FF00000000C0A8A0FFFFFF
          FFFFFFFFFFFFF0D8D0FF906050FFFFFFFFFFF0E0D0FFC0B0A0FFFFFFFFFFC0A8
          90FFC0A890FFC0A090FFC0A090FFFFE8D0FF604030FF00000000C0A8A0FFFFFF
          FFFFC08060FFB07860FFF0D8D0FFFFFFFFFFF0E0D0FFC0B0A0FFFFFFFFFFFFF8
          F0FFFFF8F0FFFFF8F0FFFFF8F0FFFFE8E0FF604030FF00000000C0A8A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0B0A0FFFFFFFFFFC0B0
          A0FFC0A8A0FFC0A890FFC0A890FFFFE8E0FF604030FF00000000C0A8A0FFC0B0
          A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0
          A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FF604030FF00000000C0B0A0FFFFFF
          FFFFFFF0E0FFFFE8E0FFFFE8E0FFFFE8D0FFFFD8C0FFC0B0A0FFFFFFFFFFC0B0
          A0FFC0B0A0FFC0B0A0FFC0A8A0FFFFF0E0FF604030FF00000000C0B0A0FFFFFF
          FFFFA06850FF805840FF705040FFFFFFFFFFFFD8C0FFC0B0A0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0FF604030FF00000000C0B0A0FFFFFF
          FFFFF0D8D0FF906050FFF0D0D0FFFFFFFFFFFFD8C0FFC0B0A0FFFFFFFFFFC0B0
          A0FFC0B0A0FFC0B0A0FFC0B0A0FFF0F8F0FF604030FF00000000C0B0A0FFFFFF
          FFFFC08070FFA07060FFFFF8F0FFFFFFFFFFF0E0D0FFC0B0A0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F8F0FF604030FF00000000D0B0A0FFFFFF
          FFFFF0D8D0FFB07860FFFFF8FFFFFFFFFFFFF0E0D0FFC0B0A0FFFFFFFFFFC0B0
          A0FFC0B0A0FFC0B0A0FFC0B0A0FFF0F8F0FF604030FF00000000D0B0A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0B0A0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF604030FF00000000D0B8A0FFD0B8
          A0FFD0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0A8A0FFC0A8
          A0FFC0A8A0FFC0A8A0FFC0A890FFC0A890FFC0A090FF00000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000585858FF404040FF404040FF404040FF404040FF404040FF4040
          40FF404040FF404040FF404040FF404040FF585858FF00000000000000000000
          0000C4B1B4FFA7958DFFA7958DFFA7958DFFA7958DFFA7958DFFA7958DFFA795
          8DFFA7958DFFA7958DFFA7958DFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFCF6EBFFFAF3E6FFFAF0E3FFF8EFE0FFF7ECDCFFF6EAD9FFF5E8
          D4FFF4E6D2FFF4E6CFFFF6E6CDFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFDF7EDFFFCF5EAFFFBF2E7FFFAF0E3FFF8EEDFFFF6EBDBFFF5E8
          D8FFF4E6D4FFF3E6D2FFF4E6CFFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFFFAF2FF8A8784FF6B6A67FF837F79FFA7A097FFF9EFE0FFF8EC
          DDFFF7EADAFFF6EAD7FFF7EAD4FFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFFFCF7FFEAE8E2FFEDEFEEFFFFFFFFFF949699FF2D2D2EFF5655
          55FF8B8783FF7A736AFFD3C8B8FFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFFFBF3FFEECC90FF4B4233FFF2E0BFFFF5ECD9FFD4CFC5FFFEFE
          F8FFFFFFFFFFDCDFE1FFB4B3AFFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFF3DBABFFD0951FFF85621BFFE8B34AFFDAB060FFC7A259FFEFCD
          93FF847762FFA2957DFFF8EFDCFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFCFBFA4FFB98F48FF8E5E0BFFD18F17FFA47112FF956A16FFE1A4
          28FF5A4312FFD2A03AFFFAEDDAFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFB7B9BCFF2B2C2EFF626469FFDED9D5FFEBE2D5FFA18A66FFDFB2
          64FF9B6E25FFCA9027FFFCF6EEFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFFFFFFFFFFFFFFFFFFFFFFFF6B6B6BFF212121FF555658FF5F60
          63FF626468FF767677FFFDF7EDFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFDFFFFFEFAFFFFFDF9FFFFFB
          F6FFFFF9F4FFFCF8F1FFFDF8EFFFA7958DFF585858FF00000000000000000000
          0000C4B1B4FFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFDFFFEFCFAFFFDFBF7FFFDF9
          F5FFFCF9F4FFF6F5F2FFF6F2E9FFA7958DFF707070FF00000000000000000000
          0000C4B1B4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFFFDFBF9FFFEFB
          F8FFFEFCF9FFD8AF74FFDCA12DFFA7958DFF707070FF00000000000000000000
          0000C4B1B4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
          FCFFFFFFFEFFE2CEBBFFD6B68EFFA7958DFF0000000000000000000000000000
          0000C4B1B4FFC4B1B4FFC4B1B4FFC4B1B4FFC4B1B4FFC4B1B4FFC4B1B4FFC4B1
          B4FFC4B1B4FFD9C7C1FFA7958DFF000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000808080FF010180FF808080FF000000000000000000000000030411101D43
          F0F01130FFFF011DF0F0010528300114B6E00120E0FF0116C3F0000000000000
          0000010101FF808080FF808080FF808001FF010180FF010101FF808080FF7477
          8FFF3E52E7FF1138FFFF0120F0FF011DE1F00113A9D001041920000000000000
          0000808080FFFFFFFFFF808080FFBFBFBFFFFFFFFFFFFFFFFFFFBFBFBFFFBFBF
          BFFF979ED3FF2048FFFF1130FFFF0825E8FF686C8EFF00000000000000000000
          0000808080FFBFBFBFFF808080FF0101FFFFBFBFBFFF808080FF808001FF7078
          9FFF4060FFFF3050FFFF2847FBFF1138FFFF0120F0FF01051F20000000000000
          0000BFBFBFFFFFFFFFFFBFBFBFFF018080FFBFBFBFFFFFFFFFFF747A31FF4060
          FFFF4058FFFF4A70FFFF117641FF4765FBFF2040FFFF0120F0FF00000000BFBF
          BFFFBFBFBFFFFFFFFFFF018080FF808080FFFFFFFFFFE7EBFFFF5070FFFF5078
          FFFF7089FFFFAFB3C7FF808001FFA7ABCBFF4361F7FF3048FFFF000000008080
          80FFBFBFBFFFBFBFBFFFBFBFBFFF0101FFFFF3F5FFFF6078FFFF6078FFFF6870
          F0FFF3F5FFFF808080FF018001FFFFFFFFFF097B8FFF3B59F0F0000000008080
          80FFBFBFBFFFBFBFBFFF0101FFFFBFBFBFFFFFFFFFFF787C8FFF6886F0FFB7B9
          C3FFFFFFFFFF808080FF01FF01FFFFFFFFFF800101FF090C2120000000008080
          80FF808080FFBFBFBFFFFFFFFFFFFFFFFFFFBFBFBFFFFFFFFFFFB7B9C3FFFFFF
          FFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFF018080FF00000000000000000101
          01FF010101FF010101FF010101FF808080FF808080FF808080FF808080FF8080
          80FF808080FFBFBFBFFFBFBFBFFFBFBFBFFF808080FF00000000000000008080
          80FF010101FF808080FF010101FFBFBFBFFFBFBFBFFF010101FF808080FFBFBF
          BFFF808080FF010101FF808080FF808080FF808080FF00000000000000008080
          80FF010101FF010101FF00000000000000000000000000000000808080FFBFBF
          BFFF808080FF010101FFBFBFBFFFBFBFBFFFBFBFBFFF0000000000000000BFBF
          BFFF808080FF010101FF808080FFBFBFBFFFBFBFBFFF808080FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000BFBFBFFF808080FF010101FFBFBFBFFFFFFF
          FFFF808080FF808080FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008080
          80FF010101FF808080FFFFFFFFFF808080FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00C7000000C0000000C0010000C0000000C000000080000000800000008000
          00008001000080010000800100008F01000080FF0000F80F0000FF830000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B09890FF8068
          50FF705840FF705040FF604830FF604830FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0A8A0FFFFF8
          F0FFF0E1E1F0C3B6B6D0A5948FB0604830FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0B0A0FFFFFF
          FFFFF0F0F0F0D0D0D0D0908B8790807060FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A08880FF8068
          50FF705840FF705040FF604830FF705840FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0A8A0FFFFF8
          F0FFF0E1E1F0C3B6B6D0A5948FB0604830FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0B0A0FFFFFF
          FFFFF0F0F0F0D0D0D0D0908B8790807060FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000D0B8B0FFC0B8
          B0FFC0B0A0FFC0A8A0FFB0A090FFB09890FF0000000000000000000000000000
          0000907870FF604830FF604830FF604830FF604830FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          0000908870FFFFE0D0FFF0C0A0FFF0A080FFE08850FF604830FF000000000000
          00000000000000000000000000000000000000000000000000FF000000FF0000
          00FFA09080FFFFE8E0FFFFD8C0FFF0B890FFF09870FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          0000B09890FFA09890FFA09080FF908870FF907870FF807060FFB09890FF8068
          50FF705840FF705840FF604830FF604830FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0A8A0FFFFF8
          F0FFF0E1E1F0F0D9D2F0D2C4B6E0705040FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0B0A0FFFFFF
          FFFFFFFFFFFFE0D9D2E0D0C3B6D0907060FF0B09091000000000000000000000
          0000000000000000000000000000000000000000000000000000D0B8B0FFC0B8
          B0FFC0B0A0FFC0A8A0FFB0A090FFB09890FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000140032000000000000000000000000B09890FF8068
          50FF705840FF705040FF604830FF604830FF0000000000000000000000000000
          00000000000000000000066906E6000E00230000000000000000C0A8A0FFFFF8
          F0FFF0E1E1F0C3B6B6D0A5948FB0604830FF0000000000000000000000000000
          000000000000000000001FA620FF147C14DD0224024E00000000C0B0A0FFFFFF
          FFFFF0F0F0F0D0D0D0D0908B8790807060FF0000000000000000000000000000
          0000000000000000000030C32CFF44C543FF0848088F00000000A08880FF8068
          50FF705840FF705040FF604830FF705840FF0000000000000000000000000000
          0000000000000000000034B032FF033C036D0000000100000000C0A8A0FFFFF8
          F0FFF0E1E1F0C3B6B6D0A5948FB0604830FF0000000000000000000000000000
          000000000000000000000046007E000000000000000000000000C0B0A0FFFFFF
          FFFFF0F0F0F0D0D0D0D0908B8790807060FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000D0B8B0FFC0B8
          B0FFC0B0A0FFC0A8A0FFB0A090FFB09890FF0000000000000000000000000000
          0000907870FF604830FF604830FF604830FF604830FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          0000908870FFFFE0D0FFF0C0A0FFF0A080FFE08850FF604830FF000000000000
          00000000000000000000000000000000000000000000000000FF000000FF0000
          00FFA09080FFFFE8E0FFFFD8C0FFF0B890FFF09870FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          0000B09890FFA09890FFA09080FF908870FF907870FF807060FFB09890FF8068
          50FF705840FF705840FF604830FF604830FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0A8A0FFFFF8
          F0FFF0E1E1F0F0D9D2F0D2C4B6E0705040FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0B0A0FFFFFF
          FFFFFFFFFFFFE0D9D2E0D0C3B6D0907060FF0B09091000000000000000000000
          0000000000000000000000000000000000000000000000000000D0B8B0FFC0B8
          B0FFC0B0A0FFC0A8A0FFB0A090FFB09890FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001E16145036241B90403326D0503830FF4A342CF03B29
          23C00F0A09300000000000000000000000000000000000000000000000000000
          0000000000003126237078594AF0C08870FFE0A080FFF0A880FFD09870FFB078
          50FF705030FF2318157000000000000000000000000000000000000000000000
          0000201A1840877059F0E0B090FFFFC8B0FFFFC8B0FFFFC8B0FFFFC0A0FFF0A8
          80FFD08860FF705030FF0F0A0930000000000000000000000000000000000000
          000050463CA0D0C0B0FFFFE0D0FFFFE0D0FFFFE0D0FFFFD8C0FFFFD0B0FFFFC8
          B0FFF0A880FFB07850FF32231DA0000000000000000000000000000000000000
          000075685BD0F0E8E0FFFFF0F0FFFFF0E0FF504840FF504840FF807060FFC098
          90FFFFC0A0FFE09870FF453029E0000000000000000000000000000000000000
          0000A08880FFFFFFFFFFFFF8FFFFFFF8F0FF606060FFC0B0A0FFFFE0D0FFFFD8
          C0FFFFC8B0FFF0B090FF503830FF000000000000000000000000000000000000
          0000827568D0FFFFFFFFFFFFFFFFFFFFFFFF908080FFD0C8C0FFFFF0E0FFFFE8
          E0FFFFD8C0FFE0B090FF4A3B2CF0000000000000000000000000000000000000
          00005A515190F0F0F0FFFFFFFFFFFFFFFFFFA098A0FFF0E8E0FFFFF8F0FFFFF0
          E0FFFFE0D0FFC09880FF3C281DA0000000000000000000000000000000000000
          000028242440A8A89AE0FFF8F0FFFFFFFFFFC0B8C0FFFFF8FFFFFFF8F0FFFFF0
          F0FFE0C0B0FF786159F018121040000000000000000000000000000000000000
          0000000000003C363660A8A19AE0F0E8E0FFF0F0F0FFFFFFFFFFF0F0F0FFD0C0
          C0FF877868F02A211E6000000000000000000000000000000000000000000000
          00000000000000000000282424405A515190827568D0A08880FF75685BD05046
          3CA028201E500000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000040B0510205028803860408000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000236C2FC0208830FF4AA559F01C302040000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000061308202C873BF040C860FF40C860FF30A850FF408F5BD0030A05100000
          0000000000000000000000000000000000000000000000000000000000000613
          0820309840FF40C860FF60E880FF59D278F060D070FF299345E0153F1C700000
          0000000000000000000000000000000000000000000000000000000000002C96
          3BF050D060FF60E880FF4D9A58B00A1A0C2054A860C060C870FF2C9D3BF00411
          0620000000000000000000000000000000000000000000000000000000007ECB
          8CE080E890FF5BB668D00000000000000000050D061054A860C060C870FF2985
          37E06E9A79B00000000000000000000000000000000000000000000000001B2D
          21303865467000000000000000000000000000000000050D061068D278F050C0
          60FF4A9D59F05070588000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000050D061068D2
          78F040B860FF68AC78F050705880000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000050D
          071062C470E040B860FF78B487F0324337500000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000050D0710589A63B040B860FF304B36600000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000121D142051825A9028733CA00000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000153B5B703088D0FF2977B6E0103C
          6080000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000050A0E10285070802098FFFF2078
          C0FF175A84C00000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000459AE0E02098
          FFFF2078C0FF081E304000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003773A5B050A8
          FFFF3088D0FF1B486C9000000000000000000000000000000000000000000000
          00000000000000000000062A426000000000000000000C1924304090E0FF50A8
          FFFF4090E0FF1024344000000000000000000000000000000000000000000000
          000000000000060B10104088D0FF183C60800C1824303088D0FF3098FFFF30A0
          FFFF2F6CA8C00610182000000000000000000000000000000000000000000000
          000000000000050B10104098E0FF4090D0FF4088C0FF40A0F0FF40A8FFFF3080
          D0FF040E18200000000000000000000000000000000000000000000000000000
          000000000000050B101050A0F0FFA0D0FFFF80C0FFFF70B8FFFF4090D0FF1233
          4E60000000000000000000000000000000000000000000000000000000000000
          000000000000050B101050A8F0FFC0E0FFFFC0E0FFFF90C8FFFF266DA9D00000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000070B101060A8F0FFE0F0FFFFD0E8FFFFC0E0FFFF50A0E0FF2451
          7590000000000000000000000000000000000000000000000000000000000000
          0000000000000000000060B0F0FF60B0F0FF60B0F0FF60B0F0FF60B0F0FF50A0
          E0FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000040A0F100000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000003182000010C100000000000000000000000000000
          00000000000000021010001BD2E0000210100000000000000000000000000000
          00000000000000042430000D9AE0000324300000000000000000000000000000
          000001031010172FC0C01030FFFF0E25F0F00002101000000000000000000000
          000000031820001190C00018C0FF000EA5F00003182000000000000000000000
          000000000000020310101D43F0F01030FFFF0028FFFF00063030000000000004
          24300014A8E00018D0FF0016B4F0000630400000000000000000000000000000
          00000000000000000000020310101D43F0F01030FFFF001DF0F0000427300014
          B6E00020E0FF0016C3F000031820000000000000000000000000000000000000
          000000000000000000000000000004072020263AD0D01038FFFF0020F0FF001D
          E1F00013A9D00003182000000000000000000000000000000000000000000000
          000000000000000000000000000000000000141B50502048FFFF1030FFFF001D
          E1F0000427300000000000000000000000000000000000000000000000000000
          0000000000000000000000000000101840404060FFFF3050FFFF1D3BF0F01038
          FFFF0020F0FF00041E2000000000000000000000000000000000000000000000
          000000000000000000000C1230304060FFFF4058FFFF3B61F0F0101640403B59
          F0F02040FFFF0020F0FF00062D30000000000000000000000000000000000000
          000000000000080C20205070FFFF5078FFFF405BD0D0080C2020000000000C10
          30303B59F0F03048FFFF0020F0FF00062D300000000000000000000000000000
          0000040610106078FFFF6078FFFF5970F0F00406101000000000000000000000
          0000080B20203B59F0F03050FFFF0028FFFF00041E2000000000000000000000
          000000000000080C20205978F0F0040610100000000000000000000000000000
          000000000000080B20203753E0E0020620200000000000000000000000000000
          0000000000000000000004061010000000000000000000000000000000000000
          00000000000000000000080B2020000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000001300300000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000006800FF0015003000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000007000FF007000FF00130030000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000007000FF30B030FF006800FF001500300000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000007800FF30B830FF30B030FF007000FF0016
          0030000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000007800FF20D020FF20C820FF70E070FF0078
          00FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000007800FF40D830FF80E080FF008800FF0016
          0030000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000008000FF80E080FF009000FF001900300000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000008800FF009800FF001B0030000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000008800FF001C003000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000001900300000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000010101010202020C0101011201010112010101110303
          020A020202030000000000000000000000000000000000000000000000000000
          000000000000000000002018132DAE7653E9C57345FFB86F45FF875941DC3727
          1F950404042F0303030700000000000000000000000000000000010101010202
          0104020201040202010402020102352C2743B57E5DF8BA7D5CFFB47855FFA37C
          66FF364552D501020322020202020000000000000000000000002B1B0F522E1E
          118A2F1E128D2F1E118B030202070000000052423971AA7657FF9D806EFF707F
          88FF376481FF0B12187802020207000000000000000000000000050302087E4E
          2DB489532ECC070503130000000000000000040403064B45428C638FAAFF4B81
          A5FF306280FF343D40C80101011702020202000000000000000000000000120B
          0720925B35DA01010113020101010000000000000000251A134389796EFF6579
          83FF688A9FFD8C8F90FA171716800202020E0000000000000000000000000000
          00007B4E2EB3180F085401010113010101110101011160381AA9DB8150FFD68C
          62FF908A87ECB6B6B6FE7D7D7DFA0B0B0B5C0202020A00000000000000000000
          000028190F48BF794BFE8C5025E18C5125E0A05F30EDCC7743FFD7804CFFB078
          53E614131236969695F1B6B6B6FF636363EA0404043603030306000000000000
          000000000000915C35D80503022402020102492C1879E89F72FFF0B089FF3E28
          17860202020237353465D9D9D9FDB4B4B4FF434343D003030310000000000000
          0000000000004C301B772D1B0E76040302099D6139D8EC9F72FFCF956EF80C08
          06240000000003020203636262AFE5E5E5FE9A9A9AF40303030F000000000000
          000000000000100A061881502DC8180E0650D7895BFFEEA57DFF6E482DAF0303
          0205000000000000000011100F1E9D9C9BE29B9997B903030307000000000000
          0000000000000000000074492BAB86522FCAE69263FFDE9B73FF140D07440201
          01010000000000000000000000002524243C3332325902020201000000000000
          000000000000000000002E1E1244E29E77FFE8986CFF966542D1040302080000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A77351D7E7A37DFF2B1B0E6F020101010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000004D321D73B87B54EC09060412000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000E0A0714412B1B6102010101000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00F01F0000F00F000000070000040700000C03000086030000C0010000C000
          0000E0000000E0200000E0300000F0380000F07F0000F87F0000F8FF0000F8FF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000D7BCB60000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000EFE1DA00DDAE8C00EFD3B500EBDBC700E1C8B500C397
          9700D8B9BA000000000000000000000000000000000000000000000000000000
          000000000000EADAD200E7BE9800EEC29200F1D8BD00F1E6D500EEE0CA00A06E
          6E00B0827D00E3CCA800D1B39500000000000000000000000000000000000000
          000000000000EDCEA400F3D3A100F1CB9900F4DFC700F4E9DC00F1E6D5009967
          6700A3757100EFD3B500E6D0AA00000000000000000000000000000000000000
          000000000000F1D8AC00F5D8A600F0C79500F6E4CE00F7F0E800F4E9DC009967
          6700A3757100EBDBC700E8D5B400000000000000000000000000000000000000
          000000000000F1D6AD008EB4CF003A96F500BACEE600F9F5F100F7F0E700E9DA
          CF00DAC4B800EBDBC700EADABF00000000000000000000000000000000000000
          0000000000007EC3E6005ABFFF0044A9FF003A99F900C0DBF800F9F5F100F7EF
          E600F4E9DC00F1E6D500DFCDBD00D8B9BA000000000000000000000000000000
          000087CAFF0056BBFF0063C8FF005DC2FF0048ADFF00419FFC00CEE4FA00F9F4
          EF00F7EFE600DAD2CF00A298B300000000000000000000000000000000000000
          0000000000006EBDFF0050B5FF0061C6FF005DC2FF004DB2FF0043A2FD00CEE4
          FA00DDE1E9007A8EC20000000000000000000000000000000000000000000000
          0000000000000000000073BFFF004DB2FF0061C6FF0061C6FF004DB2FF0043A2
          FD00579BEE000000000000000000000000000000000000000000000000000000
          000000000000000000000000000079C0FE004DB2FF0061C6FF008ED7FF00406F
          D4004770E3000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007070
          BD007B8CD5000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FDFF0000F01F0000E0070000E0070000E0070000E007
          0000E0030000C0070000E00F0000F01F0000F81F0000FF9F0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000080A0B0FF306070FF306070FF305870FF305060FF304860FF304860FF3040
          50FF304050FF303840FF203040FF202830FF202030FF101820FF0000000080A0
          B0FF306070FF306070FF305870FF305060FF304860FF304860FF304050FF3040
          50FF303840FF304850FF306880FF307890FF207890FF202030FF0000000080A0
          B0FF90E0F0FF60B8C0FF60B0C0FF60A8C0FF50A0B0FF50A0B0FF4098B0FF4090
          A0FF3088A0FF506070FF303840FF307890FF3080A0FF202830FF0000000080A8
          C0FF90E0F0FF80D8F0FF80D8F0FF70D8F0FF70C8E0FF60C8E0FF60C0D0FF50B8
          D0FF50B0D0FF607880FF70D0E0FF303840FF307080FF203040FF0000000080B0
          C0FFA0E8F0FF80E0F0FF80D8F0FF80D8F0FF70D8F0FF70D0E0FF70C8E0FF60C0
          E0FF60B8D0FF708890FF607880FF506070FF304850FF303840FF0000000080B0
          C0FFA0E8F0FF90E0F0FF80E0F0FF80D8F0FF80D8F0FF80D8F0FF70D0E0FF70D0
          E0FF70C8E0FF60C0E0FF60B8D0FF4098B0FF303840FF304050FF000000007595
          9CD0B0F0F0FF90E8F0FF904820FF90C0C0FF80E0F0FF80D8F0FF80D8F0FF70D8
          F0FF70D0E0FF70D0E0FF60C8E0FF50A0B0FF304050FF304050FF000000005A73
          82A0B0F0FFFFA0F0F0FFA05020FFA05030FF90C0D0FF80E0F0FF80E0F0FF80D8
          F0FF80D8F0FF70D8F0FF70D0E0FF60A8C0FF304050FF304860FFD08060FF9048
          20FFA04820FFA05020FFB05830FFC06840FFA05030FF90C8D0FF90E0F0FF80E0
          F0FF80D8F0FF80D8F0FF80D8F0FF60B0C0FF304860FF404860FFD08860FFFFB0
          90FFF09060FFF08850FFE08050FFD07850FFC07040FFA05830FF90C8D0FF90E0
          F0FF80E0F0FF80E0F0FF80D8F0FF60B8C0FF404860FF404860FFE09070FFFFB8
          90FFFFA880FFFF9060FFF09060FFE08850FFE07850FFC07040FFB05830FF90E8
          F0FF90E0F0FF80E0F0FF80E0F0FF80D8F0FF404860FF708890FFE09870FFFFC0
          A0FFFFC0A0FFFFB080FFFFA880FFFFA070FFE08050FFB05830FF515151906882
          8FD07098A0FF7090A0FF7090A0FF708890FF708890FF00000000F0A070FFE098
          70FFE09070FFD08860FFE09070FFF09870FFB05830FF160B0620000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000E09870FFC07850FF160B062000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000F0A070FF1A100C200000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000908890FF807880FF7F787FFF7B777BFF7473
          74FF676867FF5A5A5AFF505050FF505050FF505050FF00000000000000000000
          00FF00000000000000FF00000000A098A0FFE0E8E0FFDFE7DFFFDAE2DAFFD3DB
          D3FFCBD3CBFFC4CCC4FFC0C8C0FFC0C8C0FF505050FF00000000000000FF0000
          00FF000000FF000000FF000000FFB0A8B0FFF0E8F0FFEFE8EFFFEAE6EAFFE3E2
          E3FFDBDCDBFFD4D5D4FFD0D0D0FFD0D0D0FF505050FF00000000000000000000
          00FF00000000000000FF00000000C0B8C0FFF0F0F0FFF0EFF0FFF1EDF1FFF1EA
          F1FFECE5ECFFE5E2E5FFE0E0E0FFE0E0E0FF505050FF00000000000000000000
          0000000000000000000000000000C0B8C0FFB0B0B0FFAFB0AFFFAAAEAAFFA3AB
          A3FF9BA29BFF949894FF909090FF909090FF808080FF00000000000000FF0000
          00000000000000000000000000FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          00000000000000000000000000FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B0A090FF8068
          60FF705840FF705840FF705840FF705840FF705840FF705040FF705040FF6050
          40FF604830FF604830FF604830FF605040FF604830FF00000000C0B0B0FFFFFF
          FFFFFFF8F0FFF0F0F0FFF0E8E0FFF0E0E0FFF0E0D0FFF0D8D0FFF0D8D0FFF0D8
          C0FFE0D0C0FFE0D0C0FFE0D0C0FFF0D0C0FF705840FF00000000C0B8B0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFF8FFFFFFF8F0FFFFF8F0FFFFF8F0FFFFF8F0FFFFF0
          E0FFFFF0E0FFFFE8E0FFF0E0D0FFF0D8D0FF806050FF00000000C0B8B0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFFFFFF8F0FFFFF8
          F0FFFFF8F0FFFFF0F0FFFFE8E0FFF0E0D0FF807060FF00000000C0B8B0FFC0B0
          B0FFC0B0A0FFC0B0A0FFC0B0A0FFC0A8A0FFB0A8A0FFB0A090FFB0A090FFB098
          90FFB09890FFB09080FFA09080FFA08870FF908070FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000505050FF5050
          50FF505050FF5A5A5AFF676867FF747374FF7B777BFF7F787FFF807880FF9088
          90FF000000000000000000000000000000000000000000000000505050FFC0C8
          C0FFC0C8C0FFC4CCC4FFCBD3CBFFD3DBD3FFDAE2DAFFDFE7DFFFE0E8E0FFA098
          A0FF00000000000000FF00000000000000FF0000000000000000505050FFD0D0
          D0FFD0D0D0FFD4D5D4FFDBDBDBFFE3E2E3FFEAE6EAFFEFE8EFFFF0E8F0FFB0A8
          B0FF000000FF000000FF000000FF000000FF000000FF00000000505050FFE0E0
          E0FFE0E0E0FFE5E2E5FFECE5ECFFF1E9F1FFF1EDF1FFF0EFF0FFF0F0F0FFC0B8
          C0FF00000000000000FF00000000000000FF0000000000000000808080FF9090
          90FF909090FF949894FF9BA29BFFA3ABA3FFAAAEAAFFAFB0AFFFB0B0B0FFC0B8
          C0FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000FF000000000000000000000000000000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000FF000000000000000000000000000000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000604830FF6050
          40FF604830FF604830FF604830FF605040FF705040FF705040FF705840FF7058
          40FF705840FF705840FF705840FF806860FFB0A090FF00000000705840FFF0D0
          C0FFE0D0C0FFE0D0C0FFE0D0C0FFF0D8C0FFF0D8D0FFF0D8D0FFF0E0D0FFF0E0
          E0FFF0E8E0FFF0F0F0FFFFF8F0FFFFFFFFFFC0B0B0FF00000000806050FFF0D8
          D0FFF0E0D0FFFFE8E0FFFFF0E0FFFFF0E0FFFFF8F0FFFFF8F0FFFFF8F0FFFFF8
          F0FFFFF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0B8B0FF00000000807060FFF0E0
          D0FFFFE8E0FFFFF0F0FFFFF8F0FFFFF8F0FFFFF8F0FFFFF8FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0B8B0FF00000000908070FFA088
          70FFA09080FFB09080FFB09890FFB09890FFB0A090FFB0A090FFB0A8A0FFC0A8
          A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0B0FFC0B8B0FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004634
          2A70703020FF31150E7000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000A078
          60FFC07050FF703020FF0000000000000000000000FF000000FF000000FF0000
          00FF000000FF000000FF000000FF000000FF0000000000000000000000004634
          2A70A07860FF46342A7000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000543B
          3170803820FF38180E7000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000C088
          70FFE08850FF803820FF0000000000000000000000FF000000FF000000FF0000
          00FF000000FF000000FF000000FF000000FF000000000000000000000000543B
          3170C08870FF543B317000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000543B
          3170803820FF38180E7000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000C088
          70FFE08850FF803820FF0000000000000000000000FF000000FF000000FF0000
          00FF000000FF000000FF000000FF000000FF000000000000000000000000543B
          3170C08870FF543B317000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000090E797D141EF8FF6E67ACFF917D6CFF816C58FF7661
          4CFF76614CFF76614CFF76614CFF76614CFF0000001C00000000000000000000
          0000000000000000000000000000615CC4FF151FF9FFADACF5FFFCF5F2FFFBF4
          F1FFFAF3EFFFF9F2EEFFF9F2EDFF76614CFF0000001C00000000000000000000
          0000000000000000000000000000B49F91FF8083F8FF1620F9FFB0B0F5FFFBF6
          F2FFFBF4F1FFFBF4F0FFFAF3EFFF76614CFF0000001C00000000000000000000
          0000000000000000000000000000B49F91FFFDF9F7FF7D80F8FF151FF9FFB6B5
          F5FFFBF6F3FFFAF4F1FFFAF4F0FF76614CFF0000001C00000000000000000000
          0000000000000000000000000000B8A395FFFDF9F8FFFDF9F8FF797CF8FF1620
          F9FFBAB9F5FFFCF6F2FFFBF5F1FF76614CFF0000001C00000000000000000000
          0000000000000000000000000000BCA798FFFEFBFAFFFDF9F8FFFEF9F7FF7176
          F8FF1720F9FFBFBEF5FFFBF6F3FF76614CFF0000001C00000000000000000000
          0001000000010000000100000000BFAA9BFFFEFBFAFFFEFAF9FFFDFAF9FFFEFA
          F8FF6D72F8FF1620F9FFC3C2F5FF78614CFF0000001D00000000000000010000
          00060000000D0A2210561B5C2CFFA99789FFEEEBEAFFF5F2F1FFF4F2F1FFF6F3
          F1FFFDF9F8FF6A6FF8FF1720F9FF756789FF0000001B00000000000000060720
          0E5B0E2A16661F6030FF348149FFA59386FFA9978AFFAB998CFFAC998DFFB09E
          90FFC0AB9CFFBEAA9BFF4E4DD8FF161FF7FF030531400000000001020108112E
          185B2A753EFF6CBC83FF519F67FF287B3FFD1C6832FA1A5B2BFF1D622FFF0000
          00220000000E00000007000000000C13A6AA090E797E000000001B3523524595
          5AFF7ACE91FF63B279FF63B279FF63B279FF67B77EFF67B77EFF236D36FF6652
          42FF0000001300000009000000000000000000000000000000005EB376FF8BF2
          AAFF75C68CFF75C68CFF75C68CFF75C68CFF75C68CFF75C68CFF27773DFF6652
          42FF0000001300000009000000000000000000000000000000000C1710206BBF
          83FF8BF2AAFF75C68CFF81D89AFF8AE4A5FF84DD9EFF84DD9EFF2B7E41FF6A54
          44FF000000130000000900000000000000000000000000000000000000001E38
          254C6BBF83FF8BF2AAFF6BBF83FF5AA66FFF5AA66FFF5AA66FFF5AA66FFF7059
          48FF000000130000000900000000000000000000000000000000000000000000
          0000192F1F3F6BBF83FF63AE79FF00000012000000070000000700000000735C
          4AFF000000150000001100000009000000090000000900000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0A090FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FF000000000000
          0000000000000000000000000000B0A090FFFFFFFFFFB0A090FFB0A090FFB0A0
          90FFB0A090FFB0A090FFB0A090FFB0A090FFB0A090FF604830FF000000000000
          0000000000000000000000000000B0A090FFFFFFFFFFFFFFFFFFFFF8FFFFF0F0
          F0FFF0E8E0FFF0E0D0FFE0D0D0FFE0C8C0FFB0A090FF604830FF000000000000
          0000000000000000000000000000B0A090FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8
          F0FFF0F0F0FFF0E0D0FF806850FFE0D0D0FFB0A090FF604830FF000000000000
          0000000000000000000000000000B0A090FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF806050FF806050FF806860FFB0A090FF604830FF48272460A552
          59F0B05860FFB05850FFA05050FF904850FF904840FF804040FF703840FF7038
          40FFFFFFFFFFF0E8E0FF806850FFE0D8D0FFB0A090FF604830FFC06870FFD088
          80FFC06050FF504840FF808080FFE0D8D0FFB0B8B0FF504040FFA04840FF8040
          40FFFFFFFFFFE0E0D0FF907860FFF0E0E0FFB0A090FF604830FFC07070FFE090
          90FFD08880FF605040FF706060FFB0B0A0FFD0D0C0FF605850FFA04840FF8040
          40FFFFFFFFFF806850FFD0B8B0FFF0E8E0FFB0A090FF604830FFC07880FFE098
          A0FFE09090FF605040FF605040FF605040FF605040FF605040FFB05850FF9048
          50FFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFB0A090FF604830FFD08080FFF0A0
          A0FFE098A0FFE09090FFD08880FFD07880FFC07070FFC06860FFB06050FFA050
          50FFFFFFFFFFFFFFFFFFFFFFFFFFB0A090FFB0A090FF604830FFD08890FFF0A8
          B0FFD07870FFD06060FFC05850FFB05040FFB04020FFB04830FFC06860FFA050
          50FFFFFFFFFFFFFFFFFFB0A090FF604830FF604830FF604830FFD09090FFF0B0
          B0FFE07070FFFFFFFFFFFFF8F0FFF0E8E0FFE0D8D0FFB05040FFC07070FFB058
          50FFFFFFFFFFFFFFFFFFC0A890FFD0C8C0FF604830FF75635A90E098A0FFFFB8
          C0FFF08880FFFFFFFFFFFFFFFFFFFFF8F0FFF0E8E0FFC05850FFA06060FFB058
          60FFFFFFFFFFFFFFFFFFC0A8A0FF604830FF75635A9000000000E0A0A0FFFFC0
          C0FFFF9090FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFD06060FF804040FFB058
          60FFD0C0B0FFD0B8B0FFD0B0A0FF75635A900000000000000000E0A8A0FFE0A0
          A0FFE098A0FFD09090FFD08890FFD08080FFC07880FFC07070FFC06870FFC068
          60FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000002448336052A566F058B06DFF50B0
          78FF50A06BFF489058FF409063FF408055FF387043FF387043FF90B0ABFF3060
          58FF306058FF306058FF306058FF306058FF68C07DFF80D0A3FF50C085FF4050
          4DFF808080FFD0E0DDFFB3B0B8FF405045FF40A068FF408055FF90B0ABFFFFFF
          FFFF90B0ABFF90B0ABFF90B0ABFF90B0ABFF70C08BFF90E0ABFF80D0A3FF4060
          5BFF607065FFA0ABB0FFC0CBD0FF50605DFF40A068FF408055FF90B0ABFFFFFF
          FFFFFFFFFFFFFDFFF8FFF0F0F0FFE0F0EDFF78C088FF98E0A8FF90E0ABFF4060
          5BFF40605BFF40605BFF40605BFF40605BFF50B078FF489058FF90B0ABFFFFFF
          FFFFFFFFFFFFFFFFFFFFF0FFFDFFF0F0F0FF80D09BFFA0F0BBFF98E0A8FF90E0
          ABFF80D0A3FF78D08DFF70C08BFF60C088FF50B080FF50A06BFF90B0ABFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF5FF88D098FFA8F0B8FF70D098FF60D0
          85FF50C07DFF40B075FF20B070FF30B072FF60C088FF50A06BFF90C0B8FFFFFF
          FFFFFDFFF8FFE0F0E5FF609080FFD0E0DDFF90D0A5FFB0F0C5FF70E095FFFFFF
          FFFFF0FFFDFFE0F0EDFFD0E0DDFF40B075FF70C08BFF50B078FFA0C0B3FFFDFF
          F8FF90C0B8FF508078FF508070FF608073FF98E0A8FFB8FFC8FF80F0ADFFFFFF
          FFFFFFFFFFFFF0FFFDFFE0F0EDFF50C07DFF60A075FF58B06DFFA0C0BBFFFDFF
          F8FF508078FFF0F0F0FF609080FFF0FFFDFFA0E0B5FFC0FFD5FF90FFB5FFFFFF
          FFFFFFFFFFFFFFFFFFFFF0FFFDFF60D085FF408055FF58B06DFFA0D0C0FFFFFF
          FFFF508070FFFDFFF8FFFFFFFFFFFFFFFFFFA0E0BDFFA0E0B5FF98E0A8FF90D0
          A5FF88D098FF80D09BFF78C088FF70C08BFF68C07DFF60C088FFA0D0C8FFFFFF
          FFFF408075FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90B0ABFF90B0
          ABFF306058FF0000000000000000000000000000000000000000B0D0C3FFFFFF
          FFFF90B0ABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90B0ABFF306058FF3060
          58FF306058FF0000000000000000000000000000000000000000B0D0CBFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF90C0B8FFC0D0CDFF3060
          58FF5A756C900000000000000000000000000000000000000000B0E0D0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA0C0B3FF306058FF5A75
          6C90000000000000000000000000000000000000000000000000B0E0D0FFB0E0
          D0FFB0E0D0FFB0E0D0FFB0E0D0FFB0D0CBFFB0D0C3FFA0D0C0FF5A756C900000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000907860FFFFFFFFFFF0E0E0FFE0D8D0FFD0D0C0FFD0D0C0FFE0D0
          C0FFE0D0C0FFE0D0C0FFE0C8C0FFE0C8B0FFE0C8B0FF604830FF000000000000
          000000000000908070FFFFFFFFFFD0C0B0FFD0B8A0FFD0B0A0FFC0A890FFC0A0
          90FFC09880FFC09080FFC09070FFC08870FFF0D0C0FF604830FF000000000000
          000000000000A08870FFFFFFFFFFFFFFFFFFFFF8FFFFFFF8F0FFFFF8F0FFFFF0
          F0FFFFF0F0FFFFF0E0FFFFE8E0FFFFE8E0FFF0D0C0FF604830FF000000000000
          000000000000A09080FFFFFFFFFFE0C8C0FFE0C8C0FFD0C0B0FFD0B8B0FFD0B0
          A0FFD0A8A0FFC0A090FFC09880FFC09080FFF0E0D0FF604830FF3C2319500000
          000000000000B09880FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFFFF8F0FFFFF8
          F0FFFFF0F0FFFFF0F0FFFFF0E0FFFFE8E0FFFFE8E0FF604830FFA05020FF4E33
          246000000000B09890FFB09890FFB09890FFB09880FFA09080FFA09080FFA088
          80FFA08870FF908070FF907870FF907870FF907870FF907870FFC07850FFA050
          20FF462D23500000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000D07850FFC070
          40FFA05020FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000D07850FFC068
          40FF3C281C400000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000B06840FF4128
          1E5000000000C0A890FF604830FF604830FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FF41281E500000
          000000000000C0A8A0FFFFFFFFFFF0E0E0FFE0D8D0FFD0D0C0FFD0D0C0FFE0D0
          C0FFE0D0C0FFE0D0C0FFE0C8C0FFE0C8B0FFE0C8B0FF604830FF000000000000
          000000000000C0B0A0FFFFFFFFFFD0C0B0FFD0B8A0FFD0B0A0FFC0A890FFC0A0
          90FFC09880FFC09080FFC09070FFC08870FFF0D0C0FF604830FF000000000000
          000000000000C0B0A0FFFFFFFFFFFFFFFFFFFFF8FFFFFFF8F0FFFFF8F0FFFFF0
          F0FFFFF0F0FFFFF0E0FFFFE8E0FFFFE8E0FFF0D0C0FF604830FF000000000000
          000000000000D0B8A0FFFFFFFFFFE0C8C0FFE0C8C0FFD0C0B0FFD0B8B0FFD0B0
          A0FFD0A8A0FFC0A090FFC09880FFC09080FFF0E0D0FF604830FF000000000000
          000000000000D0B8A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFFFF8F0FFFFF8
          F0FFFFF0F0FFFFF0F0FFFFF0E0FFFFE8E0FFFFE8E0FF604830FF}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000707070FF404040FF000800FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000707070FF505050FF100810FF00000000000000000000
          0000000000FF000000FF000000FF000000FF000000FF00000000000000000000
          00000000000000000000707070FF707070FF404040FF00000000000000000000
          0000000000FF000000FF000000FF000000FF000000FF00000000000000000000
          00000000000000000000707070FF909890FF504850FF08090820000000000000
          0000090909100807081000000000000000000000000000000000000000000000
          00000000000036393660909090FFD0C8C0FF505850FF1E1B1E60000000000000
          0000000000FF000000FF000000FF000000FF000000FF00000000000000000000
          00002D2D2D50808880FFF0F0F0FFE0D8D0FF909890FF404840FF141414400000
          0000000000FF000000FF000000FF000000FF000000FF00000000000000002826
          2840707070FFF0E8F0FFFFF8FFFFF0E8E0FFD0D0D0FF707070FF404840FF0F0F
          0F30000000000000000000000000000000000000000000000000322F32507070
          70FFC0C8C0FFFFFFFFFFF0F8F0FFF0F0F0FFD0D8D0FFB0A8B0FF606060FF4048
          40FF141414400000000000000000000000000000000000000000C0B8C0FFC0C0
          C0FFD0C8D0FFC0C0C0FFB0B8B0FFB0B0B0FFB0B0B0FFA0A0A0FF808080FF6060
          60FF404840FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000040404100000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000F192350203040FF0A11195000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000F192350305060FF4078C0FF304860FF0F191E50000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000F192350305070FF3080D0FF4098E0FF50B0F0FF506870FF000000000000
          000000000000000000000000000000000000080F10203C413CA0535353E0454C
          45E0505860FF407090FF40A0E0FF60C8FFFF7090A0FF232A2D50000000000000
          00000000000000000000000000001215153068605BD0B0A8A0FFD0C0B0FFD0B0
          A0FF807870FF505850FF6090B0FF7098B0FF3844488000000000000000000000
          0000000000000000000000000000505050A0C0B8B0FFFFF0E0FFFFE8E0FFF0D8
          C0FFF0D0B0FF807870FF36313690384448800000000000000000604830FF6048
          30FF604830FF604830FF604830FF808070FFF0E8E0FFFFF8F0FFFFF0F0FFFFE8
          E0FFF0D8D0FFD0B0A0FF605850FF000000000000000000000000B09070FFFFE8
          E0FFF0C8B0FFF0C0B0FFA07860FFA09890FFF0E8E0FFFFFFFFFFFFF8F0FFFFF0
          F0FFFFE8E0FFE0C0B0FF605850FF000000000000000000000000B09080FFFFF0
          E0FFFFE8E0FFF0C8B0FFA08060FFC0B8B0FFC0C0C0FFFFFFFFFFFFFFFFFFFFF8
          F0FFFFF0E0FFB0A090FF605850FF000000000000000000000000A06840FFB080
          60FFA07860FFA07050FFA06040FF906850FFA09890FFC0C0C0FFF0E8E0FFF0E8
          E0FFB0B0A0FF807060FF604840FF000000000000000000000000A07050FFFFC8
          B0FFFFC0A0FFFFC0A0FFA07050FFFFB890FFF0B890FFC0A890FF908880FFA090
          90FFB09880FFE0A080FF604830FF000000000000000000000000A07860FFFFC8
          B0FFFFC8B0FFFFC8B0FFA07860FFFFC0A0FFFFB8A0FFFFB890FF905830FFF0D8
          D0FFF0D8C0FFF0B090FF604830FF000000000000000000000000B07860FFC098
          80FFC09080FFB08870FFB08060FFA07860FFA07050FFA06040FF906040FFA078
          60FFA07860FFA07050FF604830FF000000000000000000000000B08070FFFFD0
          B0FFFFD0B0FFFFD0B0FFB08870FFFFC8B0FFFFC8B0FFFFC0A0FFA06040FFFFE0
          D0FFF0C0A0FFF0B8A0FF604830FF000000000000000000000000B08870FFFFD0
          B0FFFFD0B0FFFFD0B0FFC09080FFFFD0B0FFFFC8B0FFFFC8B0FFA07050FFFFE8
          E0FFFFE0D0FFF0C0A0FF604830FF000000000000000000000000B09070FFB088
          70FFB08070FFB08060FFA07860FFA07050FFA06850FFA06040FF906040FFB088
          70FFB08070FFA08060FF604830FF000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E07042068341DF0070302100000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000382010805B3319D0000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000008040310804020FF381C0E70000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000502D28A068432CF0080E2440000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000805040FF101880FF000870FF050728500000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000048302080704850FF1038F0FF2038C0FF001070FF0008
          70FF000418300000000000000000000000000000000000000000000000000000
          00000000000000000000805840FF3038B0FF1038F0FF2038C0FF1030D0FF1028
          C0FF000870FF0006204000000000000000000000000000000000000000000000
          0000000000003C2A1E60906050FF3050F0FF3050F0FF1030A0FF1030E0FF3050
          F0FF2040B0FF000870FF00062040000000000000000000000000000000000000
          000000000000805840FF7070A0FF3050B0FF2048B0FF2040C0FF3050F0FF2040
          A0FF103090FF002090FF102080FF000000000000000000000000000000000000
          0000000000007E5A45E05068B0FF05060B10060914204058B0FF3050B0FF0206
          1820000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000604830FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000C0B0A0FFFFFF
          FFFFF0E8E0FFF0E0D0FFF0E0D0FFF0D8C0FFFFD0B0FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000C0B0A0FFFFFF
          FFFFFFFFFFFFFFF8F0FFFFF0F0FFFFF0E0FFF0D8C0FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000D0B0A0FFD0B0
          A0FFC0A890FFC09880FFB09080FFB08870FFA07860FF604830FF000000000000
          0000000000000000000000000000000000000000000000000000D0B8A0FFFFFF
          FFFFF0E8E0FFF0E0D0FFF0E0D0FFF0D8C0FFFFD0C0FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FFD0B8B0FFFFFF
          FFFFFFFFFFFFFFF8F0FFFFF8F0FFFFF0F0FFFFE0D0FF807060FFFFE8D0FFF0E0
          D0FFF0D8C0FFC09880FFF0D0C0FFF0D0B0FFF0D0B0FF604830FFD0B8B0FFD0C0
          B0FFD0C0B0FFD0C0B0FFE0C8C0FFE0D0C0FFE0C8C0FFB0A8A0FFD0C0B0FFD0B8
          B0FFC0A890FFB09080FFC0B0A0FFC0A890FFC0A090FF604830FFD0C0B0FFFFFF
          FFFFF0E8E0FFF0E8E0FF805840FF604030FF503830FF605850FFA09890FFE0D8
          D0FFFFE0D0FFC0A080FFF0D8C0FFF0D0C0FFF0D0B0FF604830FFD0C0B0FFFFF8
          FFFFFFF8F0FFFFF0F0FFFFF0F0FFE0D0C0FFD0B8B0FFA09890FF605040FFA090
          90FFD0C0B0FFC0A090FFC0B0A0FFC0A890FFC0A090FF604830FFD0C0B0FFC0A8
          A0FFB09880FFA08870FF907870FF907870FF907870FFA09080FFA08880FF7058
          50FFF0E8E0FFC0A890FFFFE0D0FFF0D8C0FFF0D8C0FF604830FF000000000000
          000000000000A08870FFC0A8A0FFC0A8A0FFC0B0A0FFD0B8B0FFC0B0A0FF5040
          30FFE0D0D0FFE0C8C0FFD0B8B0FFC0B0A0FFC0A090FF604830FF000000000000
          000000000000A09080FFFFF8F0FFFFF8FFFFF0E8E0FFC07850FF906040FF5038
          20FF403020FF503820FFF0D0C0FFF0E0D0FFF0E0D0FF604830FF000000000000
          000000000000B09080FFB09070FFC09880FFC0A890FFE0B8A0FFD08050FF9058
          40FF704830FFD0B0A0FFB08870FFA07050FFA06040FF705040FF000000000000
          000000000000B0A090FFD0C0C0FFB0B0B0FFB0A8A0FFC0A890FFE0C0B0FFD078
          50FFD0B8B0FFC0A090FFC0B0A0FFB0A0A0FFA09890FF604830FF000000000000
          000000000000C0A8A0FFD0C8C0FFC0C0B0FFC0B0B0FFC09880FFE0D0D0FFE0C0
          B0FFD0C0C0FFB09080FFC0B0B0FFC0B0B0FFB0A0A0FF604830FF000000000000
          000000000000C0B0A0FFC0A890FFC0A890FFC0A090FFB09880FFA09080FFA088
          80FFA08070FFA08070FFA08060FFA07860FFA07860FFA08070FF}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000634C369078522CF0804800FF9058
          00FF804800FF684719D000000000000000000000000000000000000000000000
          00000000000000000000000000005A482D90906020FFC07820FFA05800FF8460
          2FC0000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A070310A06820FFE09830FFB06000FF785423C00A07
          0310000000000000000000000000000000000000000000000000000000000000
          000000000000E09030FFD08830FFE09840FFF0A840FFA05810FF704000FF6030
          00FF0000000000000000503000FF000000000000000000000000000000000000
          00000000000000000000E09830FFFFC880FFF0B050FFD08030FF905810FF0000
          000000000000805020FFB06810FF804000FF000000000000000020A0F0FF4078
          E0FF4078E0FF4078E0FF4078E0FFE09830FFFFC070FFA06820FF4070E0FF4070
          E0FF906830FFF0A850FFE09020FFB06000FF704000FF0000000020A0F0FF50C0
          FFFF50C0FFFF4078E0FF50C0FFFF50C0FFFFC08030FF50C0FFFF50C0FFFFD088
          40FFE09030FFFFB860FFFFB040FFC07810FF904800FF704000FF20A0F0FF70D8
          FFFF50C8FFFF3080E0FF70D8FFFFC0B0A0FFC0B0A0FFC0B0A0FFC0B8B0FFD0C8
          C0FFD0B8A0FFD07820FFFFB050FFA06010FF463823700000000020A8F0FF80D8
          FFFF60D0FFFF3080E0FF80D8FFFF60D0FFFFA098A0FF90E0FFFFD0A880FFC090
          60FFC07830FFFFB050FFB06820FF6E5032A0000000000000000020A8F0FF80E0
          FFFF70D0F0FF3080F0FF80E0FFFF70D0F0FFA098A0FFD08040FFD07030FFD070
          30FFD07830FFD08040FFD08860FF00000000000000000000000020A8F0FF90E8
          FFFF70D8F0FF3080F0FF90E8FFFF70D8F0FFA098A0FF90E8FFFFE0C8B0FFF0D0
          D0FFF0C8B0FFFFD8C0FFD0A080FF0000000000000000000000001D9DE1F087D9
          F0F068CAE1F02C78E1F087D9F0F068CAE1F02C78E1F087D9F0F068CAE1F02C78
          E1F087D9F0F068CAE1F03B70D2F000000000000000000000000020A8F0FFB0F0
          FFFFB0F0FFFF3088F0FFB0F0FFFFB0F0FFFF3088F0FFB0F0FFFFB0F0FFFF3088
          F0FFB0F0FFFFB0F0FFFF4078E0FF00000000000000000000000020A8F0FF3090
          F0FF3088F0FF3088F0FF3088F0FF3088F0FF3080F0FF3080F0FF3080F0FF3080
          F0FF4080E0FF4078E0FF4078E0FF00000000000000000000000020A8FFFF80E8
          FFFF80E8FFFF80E8FFFF80E8FFFF70E0FFFF70D8FFFF70D0FFFF60C8FFFF60C0
          FFFF50B8FFFF50B0FFFF4078E0FF00000000000000000000000030A8FFFF30A8
          FFFF30A8FFFF30A8FFFF30A8FFFF30A0FFFF30A0FFFF30A0FFFF3098F0FF3098
          F0FF3090F0FF3090F0FF3090F0FF000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000004040
          70FF404070FF404070FF304070FF304070FF303860FF303860FF303860FF3038
          60FF303860FF303060FF303060FF303060FF0000000000000000404870FF7078
          A0FFA0A8E0FF90A0E0FF90A0E0FF90A0E0FF90A0E0FF90A0E0FF90A0E0FF90A0
          E0FF90A0E0FF90A0E0FFA0A8E0FF7070A0FF303060FF00000000404870FF90A0
          E0FF6070D0FF5070D0FF5070D0FF5070D0FF5070D0FF5070D0FF5070D0FF5070
          D0FF5070D0FF5070D0FF6070D0FF90A0E0FF303060FF00000000404870FF8090
          E0FF5060D0FF5060D0FF5060D0FF5060D0FF5060D0FF5060D0FF5060D0FF5060
          D0FF5060D0FF5060D0FF5060D0FF8090E0FF303060FF00000000404870FF8088
          D0FF4058C0FFFFFFFFFFFFFFFFFFFFFFFFFFB0B8E0FF4058C0FFB0B8E0FFFFFF
          FFFFFFFFFFFFFFFFFFFF4058C0FF8088D0FF303060FF00000000404870FF7080
          D0FF4050C0FF4050C0FFFFFFFFFFFFFFFFFFFFFFFFFFA0A8E0FFFFFFFFFFFFFF
          FFFFFFFFFFFF4050C0FF4050C0FF7080D0FF303860FF00000000404880FF7078
          D0FF3040C0FF3040C0FF3040C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF3040C0FF3040C0FF3040C0FF7078D0FF303860FF00000000405080FF7078
          D0FF3040C0FF3040C0FF3040C0FF3040C0FFFFFFFFFFFFFFFFFFFFFFFFFF3040
          C0FF3040C0FF3040C0FF3040C0FF7078D0FF303860FF00000000405080FF9098
          E0FF5068D0FF5068D0FF5068D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF5068D0FF5068D0FF5068D0FF9098E0FF303860FF00000000405080FF90A0
          E0FF6078E0FF6078E0FFFFFFFFFFFFFFFFFFFFFFFFFF6078E0FFFFFFFFFFFFFF
          FFFFFFFFFFFF6078E0FF6078E0FF90A0E0FF303860FF00000000405080FFA0A8
          F0FF7088E0FFFFFFFFFFFFFFFFFFFFFFFFFF7088E0FF7088E0FF7088E0FFFFFF
          FFFFFFFFFFFFFFFFFFFF7088E0FFA0A8F0FF303860FF00000000505080FFB0B8
          F0FF9098F0FF9098F0FF9098F0FF9098F0FF9098F0FF9098F0FF9098F0FF9098
          F0FF9098F0FF9098F0FF9098F0FFB0B8F0FF303860FF00000000505080FFC0C8
          F0FFA0A8F0FFA0A8F0FFA0A8F0FFA0A8F0FFA0A8F0FFA0A8F0FFA0A8F0FFA0A8
          F0FFA0A8F0FFA0A8F0FFA0A8F0FFC0C8F0FF303860FF00000000505080FF8088
          B0FFD0D0FFFFC0C8FFFFC0C8FFFFC0C8FFFFC0C8FFFFC0C8FFFFC0C8FFFFC0C8
          FFFFC0C8FFFFC0C8FFFFD0D0FFFF8080A0FF304070FF00000000000000005050
          80FF505080FF405080FF405080FF405080FF404870FF404870FF404870FF4048
          70FF404870FF404070FF404070FF404070FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000A07060FF3B341DF0403020FF3B2C1DF03B2C1DF03B2C1DF03B2C
          1DF0403020FF3B2C1DF03B2C1DF03B2C1DF03B2C1DF0403020FF000000000000
          000000000000967059F0E0B8A0FFE0B8A0FFE0B8A0FFE0B8A1FFE0B9A1FFE0B5
          99FFE0B090FFE0AF8FFFE0B090FFE0B090FFE0B090FF403020FF000000000000
          000000000000A07860FFF0E0D0FFF3E0D0FFFDE1D2FFFCDECBFFF2D9C2FFEED7
          BEFFF0D8C0FFF2DCC5FFEFD5BCFFE5BD9FFFE0B090FF403020FF41202350C068
          60FFA05050FFA07860FFFFE8E0FFF5DDD4FFCFB2A4FFB99683FFBF9881FFC098
          7EFFC09880FFBD907DFFC39383FFD6AB96FFE0B8A0FF403020FFC07070FFD070
          70FFB04820FFA07860FFFFF0E0FFFFEFE0FFFFEBE2FFFFE5DBFFFFE1D2FFF9E0
          D0FFF0E0D0FFF0DFCBFFEFD5BCFFE5C2A9FFE0B8A0FF403020FFC07070FFD070
          70FFD07070FFA07860FFFFF8F0FFF5EAE1FFCFB5A8FFB99481FFBF9881FFC098
          7EFFC09880FFBE8F7BFFC19484FFCBB0A1FFD0C0B0FF403020FFC07870FFE078
          80FFE07070FFA07860FFFFF8F0FFFFF8F0FFFFF9F2FFFFF6EBFFFFF1E2FFFFF0
          E0FFFFF0E0FFFFEBDBFFFBDCCCFFDFC9B9FFD0C0B0FF403020FFC07870FFE080
          80FFE07880FFA07860FFFFFFFFFFFFF5F0FFFFD0B8FFFFB892FFFFB992FFFFB1
          88FFFFA880FFFFA47AFFFBAA84FFDFB8A1FFD0C0B0FF403020FFC08070FFE090
          90FFE08880FFA07860FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC
          FFFFFFF8FFFFFFF7FFFFFFF9FFFFFFFDFFFFFFFFFFFF403020FFD08080FFF0A0
          A0FFF09890FFA07860FFA07860FFA07860FFA07860FFA07860FFA07860FFA078
          60FFA07860FFA07860FFA07860FFA07860FFA07860FFA07050FFD08880FFF0A8
          B0FFD06860FFFFF8F0FFFFF8F0FFF0F0F0FFF0E8E0FFF0D8D0FFE0D0C0FFE0C8
          C0FFB04020FFC06060FF904850FF000000000000000000000000D08880FFFFB8
          B0FFD07070FFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFF0E8E0FFF0D8D0FFE0D0
          C0FFA04020FFD06860FFA05050FF000000000000000000000000D09080FFFFC0
          C0FFE07870FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFF0E8E0FFF0D8
          D0FFB04830FFD07070FFA05050FF000000000000000000000000D09890FFFFC0
          C0FFE08080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0F0F0FFF0E8
          E0FFB05030FFE07880FFA05050FF000000000000000000000000D09890FFFFC0
          C0FFF08890FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FFF0F0
          F0FFC05040FF603030FFB05850FF000000000000000000000000D0A090FFE0A8
          A0FFE0A8A0FFE0A0A0FFE09890FFE09090FFD09090FFD08880FFD08080FFD080
          80FFD07870FFD07870FFC07070FF000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000C46C4FD76F3D2B7C0E080510000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E87E5DF7EF815FFFD27251E46D3B28780E08051000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E17856F0EF805CFFF0815CFFF0805CFFD3714EE46D3927770F0805100000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E17752F0F07E57FFF07E57FFEF7F57FFF07E58FFEF7E57FFD46F4BE46E38
          24770F0805100000000000000000000000000000000000000000000000000000
          0000E1744DF0EF7B52FFEF7B52FFEF7C52FFEF7B51FFF07B51FFF07C52FFEF7B
          51FFD56D46E46F3822770F070410000000000000000000000000000000000000
          0000E2744BF0F07B50FFF07B50FFF07B50FFF07C50FFF07B50FFF07B50FFF07B
          50FFF17C51FFF07C50FFD97047E87B3E25840000000000000000000000000000
          0000E79A7CF0F5A484FFF5A384FFF5A384FFF5A484FFF5A484FFF5A484FFF5A4
          84FFF5A485FFF6A586FFED9D7CF78F5B45960000000000000000000000000000
          0000E9AC93F0F7B79CFFF8B79CFFF8B79CFFF7B79CFFF7B79CFFF8B79DFFF8B8
          9FFFE9AB90F2875D4B8D1D130F1F000000000000000000000000000000000000
          0000EBBAA7F0F9C6B1FFF9C6B1FFF9C6B1FFF9C7B2FFF9C8B3FFF1BDA7F7936C
          5C981F1611210000000000000000000000000000000000000000000000000000
          0000ECC9BAF0FBD6C7FFFBD7C8FFFBD8CAFFF4CEBCF999796B9D291F1A2B0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000F1DCD3F3FDE8DFFFF6DCCFF99B81769E2D231F2E00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000EBDDD7EDA69289A92E26222F000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000594C465B100D0C1000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000048271C4DDD7858EBDC7858EB48271C4D01010001010100014827
          1C4DDC7858EBDC7858EB48271C4D000000000000000000000000000000000000
          000000000000E27957F1F0815CFFEF805CFFE27957F10905040A0905040AE279
          57F1EF805CFFEF805CFFE37A57F1000000000000000000000000000000000000
          000000000000E37954F2EF7F59FFEF7F58FFE37954F20A05040B0A05040BE479
          54F2EF7F58FFEF7F59FFE37954F2000000000000000000000000000000000000
          000000000000E17650F0EF7D55FFEF7D54FFE17650F00A05040B0A05040BE176
          4FF0F07D55FFF07D55FFE1764FF0000000000000000000000000000000000000
          000000000000E2734BF0F07A4FFFF07A4FFFE2734AF00A05030B0A05030BE273
          4AF0F07A4FFFF07A4FFFE2734AF0000000000000000000000000000000000000
          000000000000E27349F0F07A4EFFF07A4DFFE27348F00A05030B0A05030BE273
          48F0F07A4DFFF07A4EFFE27348F0000000000000000000000000000000000000
          000000000000E69372F0F49C79FFF49C79FFE69372F00B07050B0B07050BE692
          72F0F49C79FFF49C79FFE69372F0000000000000000000000000000000000000
          000000000000E8A488F0F7AD90FFF7AD90FFE8A488F00B08060B0B08060BE8A4
          88F0F7AE90FFF7AE90FFE8A488F0000000000000000000000000000000000000
          000000000000E9B29BF0F8BDA5FFF9BDA5FFEAB29BF00B08070B0B08070BEAB2
          9BF0F9BDA5FFF8BDA5FFE9B29BF0000000000000000000000000000000000000
          000000000000EBC2AFF0FACEBAFFFACEBAFFEBC2B0F00B09080B0B09080BEBC2
          B0F0FACEBBFFFACEBBFFEAC2B0F0000000000000000000000000000000000000
          000000000000F7DACDFAFCDED1FFFCDED1FFF7DACDFA0C0A0A0C0C0A0A0CF7DA
          CDFAFBDED0FFFCDED0FFF7DACDFA000000000000000000000000000000000000
          000000000000B1A49EB2FEEDE6FFFEEDE6FFB1A49EB20605050606050506B1A4
          9EB2FEEDE6FFFEEDE6FFB1A49EB2000000000000000000000000000000000000
          00000000000022201F22B9B3B0BAB9B3B0BA22201F2200000000000000002220
          1F22B9B3B0BAB9B3B0BA22201F22000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000A75C
          44B4EC8261FF6F3D2E7800000000000000000000000000000000000000000000
          0000000000000000000000000000341C133AB46447C8A75C44B600000000B461
          47BFF0825FFF7841308000000000000000000000000000000000000000000000
          000000000000351D133BA75A40B7EC805DFEF08260FFB46147BF00000000B360
          44BFEF805BFF78402E800000000000000000000000000000000000000000351C
          123AA8593DB6ED7F5AFEF0805CFFF0815BFFEF805BFFB36044BF00000000B35E
          41BFEF7D57FF783F2C80000000000000000000000000351B113AA85739B6ED7E
          55FEF07F58FFF07E58FFEF7E57FFF07E57FFEF7E57FFB35E41BF00000000B35C
          3DBFEF7B52FF773D297F00000000361B103AAA5636B7ED7A50FEF07B52FFF07B
          51FFF07B51FFEF7C51FFEF7C52FFEF7B52FFEF7B52FFB35C3DBF00000000B45C
          3CBFF07B51FFAF5836BBA55434B0EF7B4FFEF07C50FFF07C50FFF07B50FFF07B
          50FFF07B4FFFF07B4FFFF07B50FFF07B50FFF07B50FFB45C3CBF00000000B87B
          63BFF5A585FFB9785FC1BC795DC5F6A485FFF5A586FFF5A484FFF5A484FFF5A4
          84FFF5A484FFF5A484FFF5A384FFF5A484FFF5A484FFB87B63BF00000000BA89
          75BFF8B89DFF7A584A7E000000004A32274EC28A72C9F8B79DFFF8B89FFFF8B7
          9CFFF7B79CFFF8B79CFFF8B79CFFF8B79CFFF8B79CFFBA8975BF00000000BB94
          85BFF9C6B1FF7D645A800000000000000000020101024E382E52CE9E89D4F9C7
          B2FFF9C8B4FFF9C6B1FFF9C6B1FFFAC6B1FFF9C6B1FFBB9485BF00000000BCA0
          95BFFBD6C7FF7E6C648000000000000000000000000000000000050403055A45
          3B5DD1AC9BD6FBD7C8FFFBD9CAFFFBD6C7FFFBD6C7FFBCA095BF00000000BEAD
          A6BFFDE7DDFF7F746F8000000000000000000000000000000000000000000000
          0000060404065D4B435FD3B7AAD6FDE6DCFFFDE8DFFFBEADA5BF00000000BEB3
          AFBFFDEFE9FF7F78758000000000000000000000000000000000000000000000
          000000000000000000000605040661524B63EDDAD1F0BFB6B2C1000000005F4B
          42628F7264943F322C4100000000000000000000000000000000000000000000
          0000000000000000000000000000000000003D35313E52453F54000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A75C44B6B464
          47C8341C133A0000000000000000000000000000000000000000000000000000
          000000000000000000006F3D2E78EC8261FFA75C44B400000000B46147BFF082
          60FFEC805DFEA75A40B7351D133B000000000000000000000000000000000000
          0000000000000000000078413080F0825FFFB46147BF00000000B36044BFEF80
          5BFFF0815BFFF0805CFFED7F5AFEA8593DB6351C123A00000000000000000000
          0000000000000000000078402E80EF805BFFB36044BF00000000B35E41BFEF7E
          57FFF07E57FFEF7E57FFF07E58FFF07F58FFED7E55FEA85739B6351B113A0000
          00000000000000000000783F2C80EF7D57FFB35E41BF00000000B35C3DBFEF7B
          52FFEF7B52FFEF7C52FFEF7C51FFF07B51FFF07B51FFF07B52FFED7A50FEAA56
          36B7361B103A00000000773D297FEF7B52FFB35C3DBF00000000B45C3CBFF07B
          50FFF07B50FFF07B50FFF07B4FFFF07B4FFFF07B50FFF07B50FFF07C50FFF07C
          50FFEF7B4FFEA55434B0AF5836BBF07B51FFB45C3CBF00000000B87B63BFF5A4
          84FFF5A484FFF5A384FFF5A484FFF5A484FFF5A484FFF5A484FFF5A484FFF5A5
          86FFF6A485FFBC795DC5B9785FC1F5A585FFB87B63BF00000000BA8975BFF8B7
          9CFFF8B79CFFF8B79CFFF8B79CFFF7B79CFFF8B79CFFF8B89FFFF8B79DFFC28A
          72C94A32274E000000007A584A7EF8B89DFFBA8975BF00000000BB9485BFF9C6
          B1FFFAC6B1FFF9C6B1FFF9C6B1FFF9C8B4FFF9C7B2FFCE9E89D44E382E520201
          010200000000000000007D645A80F9C6B1FFBB9485BF00000000BCA095BFFBD6
          C7FFFBD6C7FFFBD9CAFFFBD7C8FFD1AC9BD65A453B5D05040305000000000000
          000000000000000000007E6C6480FBD6C7FFBCA095BF00000000BEADA5BFFDE8
          DFFFFDE6DCFFD3B7AAD65D4B435F060404060000000000000000000000000000
          000000000000000000007F746F80FDE7DDFFBEADA6BF00000000BFB6B2C1EDDA
          D1F061524B630605040600000000000000000000000000000000000000000000
          000000000000000000007F787580FDEFE9FFBEB3AFBF0000000052453F543D35
          313E000000000000000000000000000000000000000000000000000000000000
          000000000000000000003F322C418F7264945F4B426200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000048271C4DDA7758E9E17A5AF0E1795AF0E17959F0E17A59F0E17A59F0E17A
          5AF0E17958F0E17A59F0DA7758E948271C4D0000000000000000000000000000
          0000E27957F1EF805CFFEF805CFFEF805CFFEF805CFFF0805CFFEF805CFFF081
          5CFFF0805CFFEF805CFFEF805CFFE27957F10000000000000000000000000000
          0000E37954F2EF7F59FFEF7F59FFEF7F58FFEF7F58FFF07F58FFEF7F58FFF07E
          58FFF07F59FFF07E58FFEF7E58FFE37954F20000000000000000000000000000
          0000E2764FF0EF7D55FFEF7D55FFF07D54FFF07D54FFEF7D54FFF07D54FFEF7D
          54FFEF7D55FFF07D54FFF07D54FFE2764FF00000000000000000000000000000
          0000E2734AF0EF7A50FFEF7A4FFFF07A4FFFF07A4FFFF07A50FFF07A50FFEF7A
          4FFFF07A50FFF07A50FFF07A50FFE2734AF00000000000000000000000000000
          0000E27248F0F0794DFFF0794DFFF07A4DFFF07A4DFFF07A4DFFF07A4DFFF079
          4DFFF07A4DFFF07A4DFFF07A4DFFE27348F00000000000000000000000000000
          0000E68F6DF0F39874FFF39874FFF39874FFF39875FFF39874FFF39874FFF398
          74FFF39874FFF39874FFF49875FFE68F6EF00000000000000000000000000000
          0000E8A286F0F6AC8EFFF6AC8EFFF6AC8EFFF6AC8EFFF6AC8EFFF6AC8EFFF6AC
          8EFFF6AC8EFFF6AC8EFFF6AC8DFFE8A285F00000000000000000000000000000
          0000E9B39CF0F8BEA6FFF9BEA6FFF9BEA6FFF8BEA5FFF9BEA6FFF8BEA5FFF8BE
          A6FFF9BEA5FFF8BEA6FFF8BEA6FFEAB39BF00000000000000000000000000000
          0000ECC4B3F0FBD0BEFFFBD0BEFFFBD0BFFFFBD0BFFFFBD0BEFFFBD0BFFFFBD0
          BFFFFBD0BEFFFBD0BEFFFBD0BFFFECC5B4F00000000000000000000000000000
          0000F7DED3FAFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2D6FFFCE2
          D6FFFCE2D7FFFCE2D7FFFCE2D6FFF7DED3FA0000000000000000000000000000
          0000B1A7A3B2FEF1ECFFFEF1ECFFFEF1ECFFFEF1ECFFFEF1ECFFFEF1ECFFFEF1
          ECFFFEF1ECFFFEF1ECFFFEF1ECFFB1A7A3B20000000000000000000000000000
          000022202022B8B4B3B8BFBBBABFBFBBBABFBFBBBABFBFBBBABFBFBBBABFBFBB
          BABFBFBBBABFBFBBBABFB8B4B3B822201F220000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000005A362460683C
          20800B0503100000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000381E1440F088
          50FFD07040FF321B0F5000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001E120C204E2A
          1860F08050FFC3684AF000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009C5333D0F08850FF603C2880000000000000000000000000000000000000
          0000000000000000000000000000000000000F0C0B1000000000000000000000
          000024120840E07840FFE08860FF000000000000000000000000000000000000
          0000D07040FFD07040FFC06840FFB06030FFB05830FF905030FF000000000000
          0000160C0620C06840FFE08050FF684430800000000000000000000000000000
          0000D07850FFE07030FFF08050FFF09870FFE09060FF3C282040000000000000
          000000000000C06840FFD2703BF0A87254C00000000000000000000000000000
          0000D08050FFE08050FFF09060FFF0A070FF633020B02C180C40000000000000
          000000000000C06840FFC3683BF0B67A5BD00000000000000000000000000000
          0000D08860FFE09060FFF09870FFE1804AF0B06040FFB06040FF4D2A15700000
          000016100A2096522CF0C3703BF0B6755BD00000000000000000000000000E0B
          0910C08060FFB68562E0754C3690D09070FFD08050FFC07040FFC06840FF8448
          23C09A6237E0A06040FFE08050FFB68C70E00000000000000000000000000000
          0000C08860FF34282040000000009C6648C0D08860FFC3784AF0C06840FFB068
          40FFA5592CF0D2703BF0D29678F0B68570E00000000000000000000000000000
          000000000000000000000000000000000000B47E60C0E09880FFD28E68F0D287
          68F0C47E62E0D29678F0D29D87F01E1714200000000000000000000000000000
          000000000000000000000000000000000000000000005A453660B4846CC0D2A1
          8CE0B49078C0876C5A901E181620000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000005B311C706234237000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000A050310A85337E0C06840FF4827186000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000A5613BF0C06840FF6030188027160F3000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00006C3F2490D07040FF9A4C29E0000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000E09
          0710D07850FFC06840FF361B0C600000000000000000000000000F0C0B100000
          0000000000000000000000000000000000000000000000000000000000007548
          3690E08050FFB05830FF080402100000000000000000D07040FFC06840FFC068
          40FFC06030FFB06030FFB06030FF00000000000000000000000000000000A872
          54C0D2784AF07E3E29E00000000000000000000000002D211B30F0A890FFF090
          60FFF08050FFE07030FFC06840FF00000000000000000000000000000000B478
          54C0D2703BF06C3517C00000000000000000000000002C180C40633020B0E098
          70FFF09060FFF08050FFC06840FF00000000000000000000000000000000B478
          54C0D2703BF07E3E1BE016100A20000000004D2A157096522CF0A05030FFE180
          4AF0E09060FFE08860FFD07040FF00000000000000000000000000000000C49A
          7EE0F08850FFA05830FF9A6237E0844823C0A5592CF0B06030FFF09060FFE187
          59F054362460B68562E0C06840FF0F0B0A10000000000000000000000000C49A
          7EE0D29D78F0D2703BF0A5592CF0B06840FFB4683BF0E18759F0E18E68F04B2F
          2350000000002D241E30D07040FF000000000000000000000000000000001E18
          1620D2A587F0D29678F0C48562E0C48562E0D28C70E0E19668F05A3C30600000
          0000000000000000000000000000000000000000000000000000000000000000
          00001E181620876C6390B49078C0D2A18CE05A4236600F0A0810000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000000000000000000000000000000000002B26227F2723
          1E8D28231E8D27221D8D27221D8D28231E8D2A241F8D0A090752000000020000
          0000000000000000000000000000000000000000000000000000E2D3CAFFFFFF
          FDFFFFFEFDFFE7DBD3FFF9F0EBFFFFFCFAFFFFF8F2FF2D272296000000000000
          0000000000000000000000000000000000000000000000000000C9BCB5F0F9F7
          F6FFF9F7F6FFE2D6D0FFF1EAE7FFF9F5F4FFF9F1EDFF28231E8D000000000000
          0000000000000000000000000000000000000000000000000000C0B1A6F0E5DD
          D7FFE4DCD6FFD5C7BEFFDFD3CCFFE5DBD4FFE6D9D0FF27221D8D000000000000
          0000000000000000000000000000000000000000000000000000D3C8C2F4FFFF
          FFFFFFFFFFFFE9E0DCFFFFFBFBFFFFFFFFFFFFFFFFFF28231F8F000000000000
          0000000000000000000000000000000000000000000000000000A3968CD1BDB6
          AFDEBEB6AFDEB1A59DDEA59E98DEB2ACA6DEC4BAB2DE27232077000000030000
          00080000000600000000000000000000000000000000000000005E493D876047
          398F62483A8F5E46388F352217914A34278F6B50408F1A1410490000000B0000
          000F0000000A0000000000000000000000000000000000000000CC9274FDE79E
          7BFFE79E7AFFCA8361FFE79973FFED9C73FFE49269FF58372686000000000000
          0000060605210F0E0D510E0C0B470302020E0000000000000000CC9479F4F4AC
          88FFF4AC87FFD28C6AFFE89C76FFF4A47CFFED9C74FF5D3A298200000000100E
          0D3F7B756FC0BFB7B1F2B3ABA5E84C46429E02020119000000008E6753D2A576
          5EE1A6765EE1986952E1A17057E1A6735AE1A97459E134221A710A090935C6BD
          B7EFFBF4EDFFFDF6EEFFFFF6EEFFF7F0E6FF69625DBC0000000B352E298D302B
          259C302A249B312A249B302B249B302B249B342E279C0C0A09597B756FAEF4F0
          EAFFFFF8F0FFFEF6EDFFFFF8F0FFFFF8EEFFE9E1D8FF100F0E4DDCCFC6FCFEFD
          FBFFFEFCFBFFE4D9D2FFF6EEEAFFFEFAF8FFFEF6F0FF2B252094B4ADA5D4F4EE
          E8FFFFFDF9FFD8D3CDFFC7BFB9FFF8F4EBFFF5EEE5FF23222071D8CAC3FFFDFC
          FBFFFDFBFBFFE3D8D2FFF4EDEBFFFDF9F9FFFDF6F3FF2D282394948E87B6F8F5
          F1FFFFFFFFFFD6D4D1FFD7D3D1FFD9D4D1FFF1ECE3FF1514135386776DB49588
          7FBF95887FBF908176BF94857CBF95887EBF9D8E83BF231F1B6429262443DFDA
          D5F9FAFAFBFFE0DEDEFFF1F0F0FFFBFBFAFF8E8782CA0201010E000000000000
          000000000000000000000000000000000000000000000000000000000000302E
          2B55B4AFABD3D8D4D1F0DBD9D5EF807B77B40C0B0B2200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000060403108F8275D0907860FF9078
          60FF7E7062E00604031000000000000000000000000000000000000000000000
          000000000000000000000000000006040310B08880FFC0A090FFF0E0D0FFD0C8
          C0FFB09890FF705840FF635A5A90000000000000000000000000000000000000
          0000000000001010101000000000C0A890FFF0E0E0FFF0D8D0FFF0F0F0FFFFFF
          FFFFD0C8C0FFB09890FF705850FF6E645AA00000000000000000A59687F04A34
          1DF059432CF0684A3BF0877059F0D0A890FFF0F0F0FFFFFFFFFFFFFFFFFFF0E8
          E0FFFFF8F0FFD0C8C0FFB09890FF705850FF6E6964A000000000C0A890FFFFF8
          F0FFF0D8D0FFF0D8C0FFF0D8D0FFE0C8C0FFD0A8A0FFF0F0F0FFFFFFFFFFF0F0
          F0FFF0E0D0FFFFF8F0FFD0C8C0FFB09890FF705840FF78736EA0C0A890FFFFF8
          F0FFC0A890FF906040FFA07050FFF0E8E0FFE0C8C0FFD0A8A0FFF0F0F0FFFFFF
          FFFFF0F0F0FFF0E0D0FFFFF8F0FFD0C8C0FFB09890FF604840FFC0A890FFFFF8
          F0FFC0A8A0FFFFFFFFFFA07050FFF0E8E0FFA09080FFD0B0A0FFD0B0A0FFF0F0
          F0FFFFFFFFFFF0F0F0FFF0D8D0FFFFF8F0FFD0C8C0FF806850FFC0A890FFFFF8
          F0FFC0B0A0FFFFFFFFFFA07860FFF0E8E0FFE0D8D0FFA09080FFE0D0C0FFD0B0
          A0FFF0F0F0FFFFFFFFFFF0F0F0FFF0E8E0FFFFFFFFFFB09880FFC0A890FFFFF8
          FFFFC0B0A0FFFFFFFFFFB08060FFF0E8E0FFF0E8E0FFF0E8E0FFA09080FFC0B0
          A0FFD0A8A0FFF0F0F0FFFFFFFFFFFFF8FFFFC0A8A0FFA98F82D0C0A8A0FFFFFF
          FFFFC0B0A0FFC0B0A0FFC0B0A0FFF0F0E0FFF0E8E0FFF0E8E0FFF0E0E0FFA090
          80FFE0C8C0FFD0B0A0FFC0A090FFB09880FF84736EB000000000C0A8A0FFFFFF
          FFFFFFF8F0FFFFF8F0FFFFF0F0FFFFF0F0FFF0F0E0FFF0E8E0FFF0E8E0FFF0E8
          E0FFF0E8E0FFF0E0D0FF877868F0000000000000000000000000C0B0A0FFFFFF
          FFFFC0A8A0FFA07860FFA06040FFFFF0F0FFC0B0A0FFB07860FFA07050FFA068
          50FFA06850FFF0D8D0FF684A3BF0101010101010101000000000D0B0A0FFFFFF
          FFFFC0B0A0FFFFFFFFFFA07050FFFFF8F0FFC0B0A0FFFFFFFFFFFFFFFFFFFFFF
          FFFF905840FFF0D8D0FF59432CF0000000000000000000000000D0B8A0FFFFFF
          FFFFC0B0A0FFC0B0A0FFC0B0A0FFFFF8F0FFC0B0A0FFC0B0A0FFC0B0A0FFC0A8
          A0FFC0A8A0FFF0D8D0FF593B2CF0000000000000000000000000D0B8A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFFFFFF8FFFFFFF8
          F0FFFFF8F0FFFFF8F0FF4A341DF0000000000000000000000000D0B0A0FFD0B8
          A0FFD0B8B0FFD0B8A0FFD0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0A8A0FFC0A8
          A0FFC0A890FFC0A890FFA59687F0000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000B09880FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FFB09880FFFFF8
          FFFFFFF8F0FFFFF0F0FFFFF0F0FFC0A8A0FFFFE8E0FFFFE8E0FFFFE8E0FFFFE8
          D0FFC0A090FFFFE0D0FFF0E0D0FFF0D0C0FFE0C0B0FF604830FFB09890FFFFFF
          FFFFFFF8F0FFFFF8F0FFFFF8F0FFC0B0A0FFFFF0F0FFFFF0E0FFFFE8E0FFFFE8
          E0FFC0A090FFFFE8E0FFFFE0D0FFFFE0D0FFE0C8B0FF604830FFB0A090FFD0C0
          C0FFD0B8B0FFD0B8A0FF8F88BEFF1020E0FF9C96B2FFC0B0A0FFC0A8A0FFC0A8
          90FFC0A890FF8478AEFFC0A090FFC0A090FFC0A080FF604830FFC0A090FFFFFF
          FFFFFFFFFFFFF1F3FFFF0028FFFF1028F0FF4050D0FFFFF8F0FFFFF8F0FFFFF8
          F0FF2F41CBFF0010B0FFFFF0E0FFFFE8E0FFE0D0C0FF604830FFC0A890FFFFFF
          FFFFFFFFFFFFFFFFFFFF2C52FFFF1030FFFF0028FFFFF0EBFFFFFFF8F0FF7F8B
          F8FF0018C0FF6F7DEFFFFFF0F0FFFFE8E0FFE0D0C0FF604830FFF0A880FFE0A0
          80FFE0A080FFD09880FFBE8A82FF3050FFFF2040FFFF7763B0FF936992FF0028
          F0FF4058F0FFC07040FFC06840FFC06030FFC06030FFC05830FFF0A880FFFFC0
          A0FFFFC0A0FFFFC0A0FFFFC0A0FFDEAFB2FF4A64F8FF3050FFFF2040FFFF3050
          FFFFE39279FFF09860FFF09860FFF09860FFF09860FFC05830FFF0A890FFFFC0
          A0FFFFC0A0FFFFC0A0FFFFC0A0FFFFC0A0FFD3ACB8FF4068FFFF4060FFFFCB94
          A0FFF0A070FFF09870FFF09860FFF09860FFF09860FFC06030FFF0A890FFF0A8
          80FFF0A080FFF0A080FFF0A070FFB38D9DFF5078FFFF5078FFFF6173ECFF4869
          F6FFCC7C66FFE08050FFE08050FFE07840FFE07840FFE07840FFC0B0A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFB2C3FFFF5078FFFF5078FFFFBDCCFFFFFFFFFFFF6488
          FFFF5773FAFF6070E0FFFFFFFFFFFFF8F0FFF0E0D0FF604830FFC0B0A0FFFFFF
          FFFFFFFFFFFF86A0FFFF5078FFFF5078FFFFD3DDFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF979FDCFF6070E0FFFFFFFFFFFFF8F0FFFFF0F0FF604830FFC09080FFB080
          60FFB08060FF6D79CEFF5B78ECFF9E7E7EFFB07860FFB07860FFB07860FFB078
          60FFB07860FFB07860FFB07860FFB07860FFA07860FFA07860FFC09080FFE0C8
          B0FFE0C0B0FFE0C0B0FFE0C0B0FFB08060FFE0C0B0FFE0C0B0FFE0C0B0FFE0C0
          B0FFB07860FFE0B8B0FFE0B8B0FFE0B8A0FFD0B8A0FFA07860FFC09880FFC090
          80FFC09080FFC09070FFC08870FFB08870FFB08070FFB08060FFB08060FFB080
          60FFB08060FFB08060FFB08060FFB08060FFB08060FFB08060FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000C0AEA2FF917C6AFF78614CFF6A533CFF604830FF754D
          2EFF6F4B2FFF604830FF604830FF604830FF604830FF604830FF000000000000
          00000000000000000000C0B0A0FFFFF0F0FFE0D8D0FFE0D8D0FFE0D0D0FFE0D0
          C0FFE0C8C0FFE0C0B0FFD0B8B0FFD0B8A0FFD0B090FF604830FF000000000000
          00000000000000000000C0B0A0FFFFF8F0FFFFF8F0FFFFF0E0FFFFE8E0FFFFE8
          D0FFFFE0D0FFFFD8C0FFFFD8C0FFFFD8C0FFD0B0A0FF604830FF000000000000
          00000000000000000000C0B0A0FFFFFFFFFFFFF8F0FFFFF8F0FFFFF0E0FFFFE8
          E0FFFFE8D0FFFFD8C0FFFFD8C0FFFFD8C0FFD0B0A0FF604830FF000000000000
          00000000000000000000C0B0A0FFFFFFFFFFD0B0A0FFD0B0A0FFD0B0A0FFD0B0
          A0FFC0A8A0FFC0A890FFC0A090FFC0A090FFD0B8B0FF805840FF000000000000
          00000000000000000000D0B0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFFFFFF8
          FFFFFFF8F0FFFFF8F0FFFFF0F0FFFFF0F0FFE0C8C0FF906850FF000000000000
          00000000000000000000D0B8A0FFFFFFFFFFA08870FFA08870FFA08870FFA088
          70FFA08070FF907870FF907870FF907860FFE0C8C0FF806050FF000000000000
          00000000000000000000D0B8A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFF8F0FFFFF8F0FFFFF0E0FFE0C8C0FF806850FF000000000000
          0000107090FF106070FF104860FF104850FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFF8F0FFFFF8F0FFFFF0F0FF806850FF000000000000
          00002088A0FF50D8F0FF20B8D0FF102830FFE08860FFE08860FFE08050FFD080
          50FFD07840FFD07040FFD07040FFD06830FFD06030FFD06030FF1098C0FF2098
          B0FF2090B0FF60E8F0FF40D8F0FF105060FF103040FF104050FFFFE8E0FFFFE8
          E0FFFFE8E0FFFFE0D0FFF0A070FFF09870FFF09060FFD06030FF40B0D0FFA0F0
          FFFF90E8FFFF80F0FFFF60E8FFFF40D8F0FF30C0D0FF104850FFF0C8B0FFF0C8
          B0FFF0C0A0FFF0B8A0FFF0B890FFF0B090FFF09870FFD06030FF60B8D0FFD0F8
          FFFFD0F8FFFFB0F8FFFF80F0FFFF60E0F0FF40D8F0FF206880FFE08860FFE088
          60FFE08050FFE08050FFD08050FFD07850FFD07850FFD07840FF50B8E0FF70C0
          E0FF40C0D0FFC0F8FFFF80F0FFFF30A0B0FF108090FF107890FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000060C0E0FFD0F8FFFFB0F8FFFF30A8C0FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000060C0E0FF60B8E0FF40B0D0FF20A8D0FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000B098
          80FF604830FF604830FF604830FF604830FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF000000000000000000000000B098
          80FFFFF0F0FFF0E0E0FFC0A090FFF0E0E0FFF0E0E0FFC0A090FFF0E0E0FFF0E0
          E0FFC0A090FFF0E0E0FFF0E0E0FF604830FF000000000000000000000000B098
          80FFFFF8F0FFFFF8F0FFC0A090FFFFF8F0FFFFF8F0FFC0A090FFFFF8F0FFFFF8
          F0FFC0A090FFFFF0F0FFFFF8FFFF604830FF000000000000000000000000E088
          60FFB06040FFB06040FFB06040FFB06040FFB06030FFB06030FFB06030FFB058
          30FFB05830FFB05830FFB05830FFB05830FF000000000000000000000000E088
          60FFF0B090FFF0A080FFF0A080FFF0A080FFF0A070FFF0A070FFF09870FFF098
          70FFF09070FFF09060FFF09060FFB45C34FF000000000000000000000000E088
          60FFF0B090FFF0B090FFF0B090FFF0A890FFF0A880FFF0A880FFF0A880FFF0A8
          80FFF0A080FFF0A070FFF0A070FFBA623AFF000000000000000000000000E088
          60FFE08860FFDF865EFFDC845BFFD98058FFD67D56FFD27A51FFCE774EFFCB72
          4BFFC86F47FFC46D44FFC26A42FFC06840FF000000000000000000000000E088
          60FFF0B090FFF0A080FFF0A080FFF0A080FFF0A070FFF0A070FFF09870FFF098
          70FFF09070FFF09060FFF09060FFC76F46FF000000000000000000000000E088
          60FFF0B090FFF0B090FFF0B090FFF0A890FFF0A880FFF0A880FFF0A880FFF0A8
          80FFF0A080FFF0A070FFF0A070FFCC744CFF000000000000000000000000E088
          60FFE08860FFE08860FFE08860FFE08860FFE08860FFE08860FFE08860FFE088
          60FFE08860FFE08860FFE08860FFD07850FF000000000000000000000000C0B0
          A0FFFFFFFFFFFFFFFFFFC0A890FFFFF0F0FFFFF0E0FFC0A890FFFFF0F0FFFFF0
          E0FFC0A890FFFFF0F0FFF0E0E0FF604830FF000000000000000000000000C0B0
          A0FFFFFFFFFFFFFFFFFFC0A8A0FFFFFFFFFFFFFFFFFFC0A8A0FFFFFFFFFFFFFF
          FFFFC0A8A0FFFFFFFFFFFFF8FFFF604830FF000000000000000000000000C0B0
          A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0A8A0FFC0A890FFC0A890FFB0A090FFB0A0
          90FFB0A090FFB09890FFB09880FFB09880FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          200000000000000400000000000000000000000000000000000000000000B098
          80FF604830FF604830FF604830FF604830FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF000000000000000000000000B098
          80FFFFF0F0FFF0E0E0FFC0A090FFF0E0E0FFF0E0E0FFC0A090FFF0E0E0FFF0E0
          E0FFC0A090FFF0E0E0FFF0E0E0FF604830FF000000000000000000000000B098
          80FFFFF8F0FFFFF8F0FFC0A090FFFFF8F0FFFFF8F0FFC0A090FFFFF8F0FFFFF8
          F0FFC0A090FFFFF0F0FFFFF8FFFF604830FF000000000000000000000000E088
          60FFB06040FFB06040FFB06040FFB06040FFB06030FFB06030FFB06030FFB058
          30FFB05830FFB05830FFB05830FFB05830FF000000000000000000000000E088
          60FFF0B090FFF0A080FFF0A080FFF0A080FFF0A070FFF0A070FFF09870FFF098
          70FFF09070FFF09060FFF09060FFB05830FF000000000000000000000000E088
          60FFF0B090FFF0B090FFF0B090FFF0A890FFF0A880FFF0A880FFF0A880FFF0A8
          80FFF0A080FFF0A070FFF0A070FFB05830FF000000000000000000000000E088
          60FFE08860FFDD865EFFDA815AFFD57E56FFD17951FFCC744CFFC76E47FFC169
          41FFBC643CFFB76037FFB35B34FFAF5730FF000000000000000000000000E088
          60FFF0B090FFF0A080FFF0A080FFF0A080FFF0A070FFF0A070FFF09870FFF098
          70FFF09070FFF09060FFF09060FFB25A32FF000000000000000000000000E088
          60FFF0B090FFF0B090FFF0B090FFF0A890FFF0A880FFF0A880FFF0A880FFF0A8
          80FFF0A080FFF0A070FFF0A070FFC56D45FF000000000000000000000000E088
          60FFE08860FFE08860FFE08860FFE08860FFE08860FFE08860FFE08860FFE088
          60FFE08860FFE08860FFE08860FFD07850FF000000000000000000000000C0A8
          A0FFFFFFFFFFFFFFFFFFC0A890FFFFF0F0FFFFF0E0FFC0A890FFFFF0F0FFFFF0
          E0FFC0A890FFFFF0F0FFF0E0E0FF604830FF000000000000000000000000C0B0
          A0FFFFFFFFFFFFFFFFFFC0A8A0FFFFF8F0FFFFF8F0FF604830FFFFF0F0FFFFF8
          F0FFC0A8A0FFFFF0F0FFFFF8FFFF604830FF000000000000000000000000C0B0
          A0FFC0B0A0FFC0A8A0FFC0A8A0FFC0A890FFC0A890FF604830FFB0A090FFB0A0
          90FFB09890FFB09890FFB09880FFB09880FF0000000000000000000000000000
          00000000000000000000000000000000000000000000604830FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000807060FF604830FF604830FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000120D0930806860FF18120C400000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000060585080705040FF908478C00000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000907060FF705040FF705040FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000604830FF000000000000
          000000000000000000000000000000000000000000000000000000000000B098
          80FF604830FF604830FF604830FF604830FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF000000000000000000000000B098
          80FFFFF0F0FFF0E0E0FFC0A090FFF0E0E0FFF0E0E0FF604830FFF0E0E0FFF0E0
          E0FFC0A090FFF0E0E0FFF0E0E0FF604830FF000000000000000000000000B098
          90FFFFF8F0FFFFF8F0FFC0A090FFFFF8F0FFFFF8F0FFC0A090FFFFF8F0FFFFF8
          F0FFC0A090FFFFF0F0FFFFF8FFFF604830FF000000000000000000000000E088
          60FFB06040FFB06040FFB06040FFB06040FFB06030FFB06030FFB06030FFB058
          30FFB05830FFB05830FFB05830FFB05830FF000000000000000000000000E088
          60FFF0B090FFF0A080FFF0A080FFF0A080FFF0A070FFF0A070FFF09870FFF098
          70FFF09070FFF09060FFF09060FFB05830FF000000000000000000000000E088
          60FFF0B090FFF0B090FFF0B090FFF0A890FFF0A880FFF0A880FFF0A880FFF0A8
          80FFF0A080FFF0A070FFF0A070FFB05830FF000000000000000000000000E088
          60FFDE855DFFDA815AFFD67E56FFD17950FFCC744CFFC76F46FFC16941FFBC65
          3CFFB85F37FFB35B33FFB05830FFAD552DFF000000000000000000000000E088
          60FFF0B090FFF0A080FFF0A080FFF0A080FFF0A070FFF0A070FFF09870FFF098
          70FFF09070FFF09060FFF09060FFB45C34FF000000000000000000000000E088
          60FFF0B090FFF0B090FFF0B090FFF0A890FFF0A880FFF0A880FFF0A880FFF0A8
          80FFF0A080FFF0A070FFF0A070FFC87048FF000000000000000000000000E088
          60FFE08860FFE08860FFE08860FFE08860FFE08860FFE08860FFE08860FFE088
          60FFE08860FFE08860FFE08860FFD07850FF000000000000000000000000C0B0
          A0FFFFFFFFFFFFFFFFFFC0A890FFFFF0F0FFFFF0E0FFC0A890FFFFF0F0FFFFF0
          E0FFC0A890FFFFF0F0FFF0E0E0FF604830FF000000000000000000000000C0B0
          A0FFFFFFFFFFFFFFFFFFC0A8A0FFFFFFFFFFFFFFFFFFC0A8A0FFFFFFFFFFFFFF
          FFFFC0A8A0FFFFFFFFFFFFF8FFFF604830FF000000000000000000000000C0B0
          A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0A8A0FFC0A890FFC0A890FFB0A090FFB0A0
          90FFB0A090FFB09890FFB09880FFB09880FF0000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000B0A090FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FF604830FF6048
          30FF604830FF604830FF604830FF604830FF604830FF604830FFC0A090FFFFF8
          F0FFF0E0E0FFF0E0D0FFE0D8D0FFD0B8B0FFF0D8D0FFE0D8D0FFE0D0C0FFE0D0
          C0FFC0A8A0FFE0D0C0FFE0C8B0FFE0C8B0FFE0C8B0FF604830FFC0A890FFFFF8
          F0FFFFF8F0FFFFF8F0FFFFF0F0FFD0B8B0FFFFF0E0FFFFE8E0FFFFE8E0FFFFE8
          D0FFC0A8A0FFFFE0D0FFF0E0D0FFF0D8D0FFE0C8B0FF604830FFC0A890FFE0D8
          D0FFD0C0B0FFD0C0B0FFD0B8B0FFD0B8B0FFD0B8B0FFC0B0A0FFC0B0A0FFC0B0
          A0FFC0B0A0FFC0A8A0FFC0A8A0FFC0A8A0FFC0A8A0FF604830FFC0A8A0FFFFF8
          FFFFFFF8FFFFFFF8F0FFFFF8F0FFD0B8B0FFFFF0F0FFFFF0E0FFFFF0E0FFFFE8
          E0FFC0B0A0FFFFE8E0FFFFE0D0FFFFE0D0FFE0D0C0FF604830FFC0A8A0FFFFFF
          FFFFFFFFFFFFFFF8FFFFFFF8F0FFD0C0B0FFFFF8F0FFFFF0F0FFFFF0E0FFFFF0
          E0FFC0B0A0FFFFE8E0FFFFE8E0FFFFE0D0FFE0D0C0FF604830FFE0A080FFE098
          70FFE09870FFE09870FFE09070FFE09060FFE08860FFE08860FFE08060FFE080
          50FFD08050FFD07850FFD07850FFD07840FFD07840FFD07840FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000E0A080FFE0A0
          80FFE0A080FFE09870FFE09870FFE09870FFE09070FFE09060FFE08860FFE088
          60FFE08060FFE08050FFD08050FFD07850FFD07850FFD07840FFC0B0A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD0C8C0FFFFFFFFFFFFFFFFFFFFF8FFFFFFF8
          F0FFD0C0B0FFFFF8F0FFFFF0F0FFFFF0E0FFE0D8D0FF604830FFC0B0A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0C8C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8
          FFFFD0C0B0FFFFF8F0FFFFF8F0FFFFF0F0FFF0D8D0FF604830FFD0B0A0FFE0D8
          D0FFE0D0C0FFE0D0C0FFE0D0C0FFE0C8C0FFE0C8C0FFD0C8C0FFD0C8C0FFD0C0
          C0FFD0C0B0FFD0C0B0FFD0C0B0FFD0B8B0FFD0B8B0FF604830FFD0B8A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0D0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD0C0C0FFFFF8F0FFFFF8F0FFFFF8F0FFF0E0E0FF604830FFD0B8A0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0D8D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE0D8D0FFFFFFFFFFFFFFFFFFFFF8F0FFFFF0F0FF604830FFD0B8A0FFD0B8
          A0FFD0B8A0FFD0B0A0FFD0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0B0A0FFC0A8
          A0FFC0A8A0FFC0A8A0FFC0A890FFC0A890FFC0A890FFC0A090FF}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000806850FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000806850FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000907060FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000907060FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000907860FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000907860FF00000000000000000000000000000000000000000000
          00000000000000000000A04010FF00000000000000000000000000000000B098
          90FF806050FF806050FF806040FF705840FF0000000000000000000000000000
          00000000000000000000A04010FFB05830FF000000000000000000000000C0A8
          90FFF0E0D0FFF0D0C0FFE0C8B0FF705840FF000000000000000000000000C060
          30FFB06030FFB04820FFB05830FFE09870FFA05020FF0000000000000000C0A8
          A0FFFFF0F0FFF0E8E0FFE0C8B0FF705840FF807060FF807060FF807060FFC070
          40FFF0B890FFE0A880FFE0A880FFE0A880FFF0A880FFA04820FF00000000D0B0
          A0FFC0A8A0FFB09890FFA09080FFA08070FF000000000000000000000000E090
          70FFE09070FFE09070FFE09870FFE0A880FFC07040FF00000000000000000000
          000000000000908070FF00000000000000000000000000000000000000000000
          00000000000000000000E09070FFC07040FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000E09070FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000806850FF00000000000000000000000000000000000000000000
          00000000000000000000A04010FF00000000000000000000000000000000B098
          90FF806050FF806050FF806040FF705840FF0000000000000000000000000000
          00000000000000000000A04010FFB05830FF000000000000000000000000C0A8
          90FFF0E0D0FFF0D0C0FFE0C8B0FF705840FF000000000000000000000000C060
          30FFB06030FFB04820FFB05830FFE09870FFA05020FF0000000000000000C0A8
          A0FFFFF0F0FFF0E8E0FFE0C8B0FF705840FF807060FF807060FF807060FFC070
          40FFF0B890FFE0A880FFE0A880FFE0A880FFF0A880FFA04820FF00000000D0B0
          A0FFC0A8A0FFB09890FFA09080FFA08070FF000000000000000000000000E090
          70FFE09070FFE09070FFE09870FFE0A880FFC07040FF00000000000000000000
          000000000000907760FF00000000000000000000000000000000000000000000
          00000000000000000000E09070FFC07040FF0000000000000000000000000000
          00000000000090795EFF00000000000000000000000000000000000000000000
          00000000000000000000E09070FF000000000000000000000000000000000000
          000000000000907766FF00000000000000000000000000000000000000000000
          0000A04010FF0000000000000000000000000000000000000000000000000000
          000000000000907971FF00000000000000000000000000000000000000000000
          0000A04010FFB05830FF00000000000000000000000000000000000000000000
          000000000000908070FF000000000000000000000000C06030FFB06030FFB048
          20FFB05830FFE09870FFA05020FF000000000000000000000000000000000000
          0000000000008E8070FF8E8070FF807060FF807060FFC07040FFF0B890FFE0A8
          80FFE0A880FFE0A880FFF0A880FFA04820FF0000000000000000000000000000
          0000000000009A8070FF000000000000000000000000E09070FFE09070FFE090
          70FFE0A080FFE0A880FFC07040FF000000000000000000000000000000000000
          0000000000009D8070FF00000000000000000000000000000000000000000000
          0000E09070FFC07040FF00000000000000000000000000000000000000000000
          000000000000908070FF00000000000000000000000000000000000000000000
          0000E09070FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A04010FF000000000000000000000000000000000000
          000000000000806850FF00000000000000000000000000000000000000000000
          00000000000000000000A04010FFB05830FF0000000000000000000000000000
          0000000000007E674EFF0000000000000000000000000000000000000000C060
          30FFB06030FFB04820FFB05830FFE09870FFA05020FF00000000000000000000
          000000000000886C58FF807060FF807060FF807060FF807060FF807060FFC070
          40FFF0B890FFE0A880FFE0A880FFE0A880FFF0A880FFA04820FF000000000000
          000000000000927162FF0000000000000000000000000000000000000000E090
          70FFE09070FFE09070FFE09870FFE0A880FFC07040FF00000000000000000000
          000000000000907160FF00000000000000000000000000000000000000000000
          00000000000000000000E09070FFC07040FF0000000000000000000000000000
          000000000000907760FF00000000000000000000000000000000000000000000
          0000A04010FF00000000E09070FF00000000000000000000000000000000B098
          90FF806050FF806050FF806040FF705840FF0000000000000000000000000000
          0000A04010FFB05830FF0000000000000000000000000000000000000000C0A8
          90FFF0E0D0FFF0D0C0FFE0C8B0FF705840FF00000000C06030FFB06030FFB048
          20FFB05830FFE09870FFA05020FF00000000000000000000000000000000C0A8
          A0FFFFF0F0FFF0E8E0FFE0C8B0FF705840FF807060FFC07040FFF0B890FFE0A8
          80FFE0A880FFE0A880FFF0A880FFA04820FF000000000000000000000000D0B0
          A0FFC0A8A0FFB09890FFA09080FFA08070FF00000000E09070FFE09070FFE090
          70FFE0A080FFE0A880FFC07040FF000000000000000000000000000000000000
          00000000000090785EFF00000000000000000000000000000000000000000000
          0000E09070FFC07040FF00000000000000000000000000000000000000000000
          0000000000009C8070FF00000000000000000000000000000000000000000000
          0000E09070FF0000000000000000000000000000000000000000000000000000
          0000000000009B8070FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000908070FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000806850FF00000000000000000000000000000000000000000000
          00000000000000000000A04010FF00000000000000000000000000000000B098
          90FF806050FF806050FF806040FF705840FF0000000000000000000000000000
          00000000000000000000A04010FFB05830FF000000000000000000000000C0A8
          90FFF0E0D0FFF0D0C0FFE0C8B0FF705840FF000000000000000000000000C060
          30FFB06030FFB04820FFB05830FFE09870FFA05020FF0000000000000000C0A8
          A0FFFFF0F0FFF0E8E0FFE0C8B0FF705840FF807060FF807060FF807060FFC070
          40FFF0B890FFE0A880FFE0A880FFE0A880FFF0A880FFA04820FF00000000D0B0
          A0FFC0A8A0FFB09890FFA09080FFA08070FF000000000000000000000000E090
          70FFE09070FFE09070FFE09870FFE0A880FFC07040FF00000000000000000000
          000000000000907260FF00000000000000000000000000000000000000000000
          00000000000000000000E09070FFC07040FF0000000000000000000000000000
          000000000000907860FF00000000000000000000000000000000000000000000
          0000A04010FF00000000E09070FF00000000000000000000000000000000B098
          90FF806050FF806050FF806040FF705840FF0000000000000000000000000000
          0000A04010FFB05830FF0000000000000000000000000000000000000000C0A8
          90FFF0E0D0FFF0D0C0FFE0C8B0FF705840FF00000000C06030FFB06030FFB048
          20FFB05830FFE09870FFA05020FF00000000000000000000000000000000C0A8
          A0FFFFF0F0FFF0E8E0FFE0C8B0FF705840FF807060FFC07040FFF0B890FFE0A8
          80FFE0A880FFE0A880FFF0A880FFA04820FF000000000000000000000000D0B0
          A0FFC0A8A0FFB09890FFA09080FFA08070FF00000000E09070FFE09070FFE090
          70FFE0A080FFE0A880FFC07040FF000000000000000000000000000000000000
          0000000000008E8070FF00000000000000000000000000000000000000000000
          0000E09070FFC07040FF00000000000000000000000000000000000000000000
          000000000000998070FF00000000000000000000000000000000000000000000
          0000E09070FF0000000000000000000000000000000000000000000000000000
          0000000000009E8070FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000908070FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A04010FF000000000000000000000000000000000000
          000000000000806850FF00000000000000000000000000000000000000000000
          00000000000000000000A04010FFB05830FF0000000000000000000000000000
          0000000000007E674EFF0000000000000000000000000000000000000000C060
          30FFB06030FFB04820FFB05830FFE09870FFA05020FF00000000000000000000
          000000000000876B57FF807060FF807060FF807060FF807060FF807060FFC070
          40FFF0B890FFE0A880FFE0A880FFE0A880FFF0A880FFA04820FF000000000000
          000000000000927162FF0000000000000000000000000000000000000000E090
          70FFE09070FFE09070FFE09870FFE0A880FFC07040FF00000000000000000000
          000000000000907060FF0000000000000000000000000000000000000000A040
          10FF0000000000000000E09070FFC07040FF0000000000000000000000000000
          000000000000907660FF0000000000000000000000000000000000000000A040
          10FFB05830FF00000000E09070FF000000000000000000000000000000000000
          00000000000090795EFF0000000000000000C06030FFB06030FFB04820FFB058
          30FFE09870FFA05020FF00000000000000000000000000000000000000000000
          000000000000907864FF807060FF807060FFC07040FFF0B890FFE0A880FFE0A8
          80FFE0A880FFF0A880FFA04820FF000000000000000000000000000000000000
          000000000000907871FF0000000000000000E09070FFE09070FFE09070FFE0A0
          80FFE0A880FFC07040FF00000000000000000000000000000000000000000000
          000000000000907F70FF0000000000000000000000000000000000000000E090
          70FFC07040FF00000000A04010FF000000000000000000000000000000000000
          0000000000008E8170FF0000000000000000000000000000000000000000E090
          70FF0000000000000000A04010FFB05830FF0000000000000000000000000000
          000000000000968070FF0000000000000000000000000000000000000000C060
          30FFB06030FFB04820FFB05830FFE09870FFA05020FF00000000000000000000
          0000000000009F8070FF807060FF807060FF807060FF807060FF807060FFC070
          40FFF0B890FFE0A880FFE0A880FFE0A880FFF0A880FFA04820FF000000000000
          000000000000918070FF0000000000000000000000000000000000000000E090
          70FFE09070FFE09070FFE09870FFE0A880FFC07040FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000E09070FFC07040FF0000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000000000000000000000000000000000007C9FBFFE3449
          5B7E0000000000000000080B0E142C6B6CA919B687F713BB7EFF2A726AB50608
          0B0F00000000000000000000000000000000000000000000000035495B7D789D
          BFFF354A5D8100000000213B44640DC786FF0CC27EFF0CBA73FF0BB367FF2443
          4972000000000C11161F1D2F385116232A3C0203040600000000000000001C28
          32455D81A2DE43556F953161689A0CC381FF0EBB78FF0DB46DFF08AF5EFF2648
          4D791D2C364D229A78E611BA7AFF15BA82FC2B7971B6070A0D12000000000000
          00000F161C2637A995ED1BC28CFF0EBC7CFE0EB671FF08B161FF1D8E64DB2A42
          4F7119A36FF007B86BFF0DBB77FF0BC17CFF12C286FF1D2E374D000000000000
          0000213F45640BC583FE0DC07EFF11B575FB0DB069FD0FA661F73460689F2190
          6BDD07B263FF0EB570FF0EBA77FF08C27AFF1CBA8AFC141E2532000000000000
          00002A7B73B40AC47FFF0DBC77FF07B567FF09AE60FF34666BA7297667BB07AE
          5CFF0EAF69FE0FB470FF0DB975FE279E83E3263C486200000000000000000000
          00002D6F6DA604C274FF12B172F9277E6AC130535D87436179A220976BE808AC
          5DFE0FAE68FE0FB46FFF0DB875FE23A480E82D6B6AA1121B212C000000000000
          0000141F25322E62659731515E83304F5B812A7E6BC51B9D69EF3B767ABD19A1
          68F109B062FF0DB46DFF0EB874FF0BBE79FF06C67CFF2E8C80C9000000000000
          0000090C0F132E5A5F8E1D9D70E80BB064FF09AE60FF07AE5CFF25926FDF3B73
          78B60BAF63FF0BB46AFF0EB874FF0FBD7BFF0BC27FFF1DB78BF6000000000D11
          161C2D897ACA08B96EFF0AB66CFF0CB46CFF10AE6AFB0DAF68FE07AF5FFF3477
          72B9346E6EAB08B367FF0AB86FFF0CBD78FF06C37AFF29927FCD000000002F60
          659008C078FF0EBC79FF0FBA77FF0EB974FF10B473FC0DB56FFF0BB369FF14AC
          6DF923333E502C60609317B478FD17B980FF2D9482D218232B37000000003278
          76AE07C57DFF10C081FF0CBF7CFF11BD7DFF11BA7AFD0EB976FF0EB873FF07B7
          6AFF2E68679D0000000019262D3B1D2B344306080A0C00000000000000002947
          506A15C38BFF08C681FF1ABE8AFD376B739F1CB282F10CBE7BFF0EBC7BFF08BC
          72FF2A9A7FDD0405070800000000000000000000000000000000000000000203
          0304263B465A2F65689122343E4F19222B351BBD8BFB0CC381FF10C081FF08C1
          79FF29A184E10507090B00000000000000000000000000000000000000000000
          00000000000000000000000000000A0D11142CAB90E806C781FF09C682FF0DC3
          82FF3061668E0001010100000000000000000000000000000000000000000000
          000000000000000000000000000000000000223840532AA38BDD29A58ADF315B
          63840507090B0000000000000000000000000000000000000000}
      end>
  end
  object dlgOpenVideoFile: TOpenDialog
    Filter = #35270#39057#25991#20214'|*.avi;*.rm;*.rmvb;*.mpg;*.mpeg;*.wmv;*.mkv;*.ogm|'#25152#26377#25991#20214'|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = #25171#24320#35270#39057
    Left = 248
    Top = 280
  end
  object syncTimer: TTimer
    Enabled = False
    Interval = 300
    Left = 16
    Top = 184
  end
  object dlgImport: TOpenDialog
    Filter = #25903#25345#30340#23383#24149#25991#20214'|*.srt;*.ssa;*.ass|'#25152#26377#25991#20214'|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = #23548#20837
    Left = 248
    Top = 232
  end
  object findGallery: TdxRibbonDropDownGallery
    BarManager = barManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'btnFindPrior'
      end
      item
        Visible = True
        ItemName = 'btnFindNext'
      end
      item
        Visible = True
        ItemName = 'btnFindFirst'
      end
      item
        Visible = True
        ItemName = 'btnFindLast'
      end>
    ItemOptions.Size = misNormal
    Ribbon = dxRibbon
    UseOwnFont = False
    Left = 136
    Top = 336
  end
  object filterGallery: TdxRibbonDropDownGallery
    BarManager = barManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarSeparator1'
      end
      item
        Position = ipContinuesRow
        Visible = True
        ItemName = 'btnFilter'
      end
      item
        Position = ipContinuesRow
        Visible = True
        ItemName = 'btnClearFilter'
      end
      item
        Visible = True
        ItemName = 'dxBarSeparator2'
      end
      item
        UserDefine = [udWidth]
        UserWidth = 23
        Visible = True
        ItemName = 'chkRapidFilter'
      end>
    ItemOptions.Size = misNormal
    Ribbon = dxRibbon
    UseOwnFont = False
    Left = 88
    Top = 336
  end
  object appMenu: TdxBarApplicationMenu
    BarManager = barManager
    Buttons = <
      item
        Item = btnExit
      end>
    ExtraPane.Header = #26368#36817#25171#24320#30340#39033#30446
    ExtraPane.Items = <>
    ExtraPane.WidthRatio = 1.600000000000000000
    ItemLinks = <
      item
        Visible = True
        ItemName = 'btnNewFile'
      end
      item
        Visible = True
        ItemName = 'btnOpenFile'
      end
      item
        Visible = True
        ItemName = 'btnSaveFile'
      end
      item
        Visible = True
        ItemName = 'btnSaveFileAs'
      end
      item
        Visible = True
        ItemName = 'btnOpenVideoFile'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'btnImport'
      end
      item
        Visible = True
        ItemName = 'btnExport'
      end
      item
        Visible = True
        ItemName = 'barMergeMenu'
      end>
    UseOwnFont = False
    Left = 88
    Top = 288
  end
  object dlgExport: TSaveDialog
    DefaultExt = 'srt'
    Filter = 'SRT'#23383#24149'|*.srt|SSA'#23383#24149'|*.ssa'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = #23548#20986
    Left = 296
    Top = 232
  end
  object displayingSubTimer: TTimer
    Interval = 250
    Left = 16
    Top = 248
  end
  object dscHistory: TDataSource
    AutoEdit = False
    Left = 544
    Top = 304
  end
  object newRecGallery: TdxRibbonDropDownGallery
    BarManager = barManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'btnNewRec2'
      end
      item
        Visible = True
        ItemName = 'btnNewRecBefore'
      end>
    Ribbon = dxRibbon
    UseOwnFont = False
    Left = 112
    Top = 392
  end
end
