unit SplitOptions;

interface

uses
    Generics.Collections,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinOffice2007Black, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxContainer, cxTextEdit, cxMemo, cxSplitter, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, dxSkinsdxBarPainter, StdCtrls, dxBar, ExtCtrls, dxmdaset,
  dxSkinsdxStatusBarPainter, dxStatusBar, cxBarEditItem, ImgList;

type
  TfrmSplitOptions = class(TForm)
    grdSplitSubs: TcxGridDBTableView;
    cxgrdlvlGrid1Level1: TcxGridLevel;
    gridSplitSubs: TcxGrid;
    split1: TcxSplitter;
    pnl1: TPanel;
    edtSourceSub: TcxMemo;
    dock1: TdxBarDockControl;
    barManager1: TdxBarManager;
    barManager1Bar1: TdxBar;
    btnAddSplitByCursor: TdxBarButton;
    btnRemoveSplit: TdxBarButton;
    btnOK: TdxBarButton;
    btnCancel: TdxBarButton;
    mem1: TdxMemData;
    fieldContent: TMemoField;
    dscSplit: TDataSource;
    colSplitSubsRecId: TcxGridDBColumn;
    colSplitSubsContent: TcxGridDBColumn;
    status1: TdxStatusBar;
    dock2: TdxBarDockControl;
    barManager1Bar2: TdxBar;
    cxBarEditItem1: TcxBarEditItem;
    btnSplitByLine: TdxBarButton;
    edtCustomSeparator: TdxBarEdit;
    btnSplitByCustom: TdxBarButton;
    dock3: TdxBarDockControl;
    barManager1Bar3: TdxBar;
    btnSplitAllByLine: TdxBarButton;
    btnSplitAllByCustom: TdxBarButton;
    cxImageList1: TcxImageList;
    btnRemoveAllSplit: TdxBarButton;
    dock4: TdxBarDockControl;
    barManager1Bar4: TdxBar;
    procedure btnAddSplitByCursorClick(Sender: TObject);
    procedure btnRemoveSplitClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSplitByLineClick(Sender: TObject);
    procedure btnSplitAllByLineClick(Sender: TObject);
    procedure btnSplitByCustomClick(Sender: TObject);
    procedure btnSplitAllByCustomClick(Sender: TObject);
    procedure btnRemoveAllSplitClick(Sender: TObject);
  private
    { Private declarations }
    const contentFieldName:string='content';
    procedure splitOnce(const separator:string);
    procedure splitAll(const separator:string);
    function getText: string;
    procedure setText(const Value: string);
  public
    { Public declarations }
    property Text:string read getText write setText;

    function GetSplitedSub:TList<string>;
  end;

implementation
uses CommonDefs,StrUtils;

{$R *.dfm}

procedure TfrmSplitOptions.FormCreate(Sender: TObject);
begin
    mem1.Open;
end;

procedure TfrmSplitOptions.splitOnce(const separator: string);
var
    sepPos:integer;
    sepLength:Integer;
begin
    if Length(edtSourceSub.Text)>0 then begin
        sepPos:=Pos(separator,edtSourceSub.Text);
        if sepPos>0 then begin
            sepLength:=Length(separator);
        end else begin
            sepPos:=Length(edtSourceSub.Text);
            sepLength:=1;
        end;

        with mem1 do begin
            Append;
            FieldByName(contentFieldName).AsString:=LeftStr(edtSourceSub.Text,sepPos+sepLength-1);
            Post;
        end;

        with edtSourceSub do begin
            Text:=RightStr(Text,Length(Text)-sepPos-sepLength+1);
        end;
    end;
end;

procedure TfrmSplitOptions.splitAll(const separator: string);
var
    sepPos:integer;
    sepLength:Integer;
begin
    while Length(edtSourceSub.Text)>0 do begin
        sepPos:=Pos(separator,edtSourceSub.Text);
        if sepPos>0 then begin
            sepLength:=Length(separator);
        end else begin
            sepPos:=Length(edtSourceSub.Text);
            sepLength:=1;
        end;

        with mem1 do begin
            Append;
            FieldByName(contentFieldName).AsString:=LeftStr(edtSourceSub.Text,sepPos+sepLength-1);
            Post;
        end;

        with edtSourceSub do begin
            Text:=RightStr(Text,Length(Text)-sepPos-sepLength+1);
        end;

        if sepPos<1 then break;
    end;
end;

procedure TfrmSplitOptions.btnAddSplitByCursorClick(Sender: TObject);
begin
    if edtSourceSub.SelStart>0 then begin
        with mem1 do begin
            Append;
            FieldByName(contentFieldName).AsString:=LeftStr(edtSourceSub.Text,edtSourceSub.SelStart);
            Post;
        end;

        with edtSourceSub do begin
            Text:=RightStr(Text,Length(Text)-SelStart);
        end;
    end;
end;

procedure TfrmSplitOptions.btnRemoveSplitClick(Sender: TObject);
begin
    if mem1.RecordCount>0 then begin
        with edtSourceSub do begin
            mem1.Last;
            Text:=mem1.FieldByName(contentFieldName).AsString + Text;
        end;

        with mem1 do begin
            mem1.Delete;
        end;
    end;
end;

procedure TfrmSplitOptions.btnRemoveAllSplitClick(Sender: TObject);
begin
    with mem1 do begin
        Last;
        while not Bof do begin
            edtSourceSub.Text:=FieldByName(contentFieldName).AsString + edtSourceSub.Text;
            Delete;
        end;
    end;
end;

procedure TfrmSplitOptions.btnSplitByLineClick(Sender: TObject);
begin
    splitOnce(strNewLine);
end;

procedure TfrmSplitOptions.btnSplitAllByLineClick(Sender: TObject);
begin
    splitAll(strNewLine);
end;

procedure TfrmSplitOptions.btnSplitByCustomClick(Sender: TObject);
begin
    splitOnce(edtCustomSeparator.CurText);
end;

procedure TfrmSplitOptions.btnSplitAllByCustomClick(Sender: TObject);
begin
    splitAll(edtCustomSeparator.CurText);
end;

function TfrmSplitOptions.getText: string;
begin
    exit(edtSourceSub.Text);
end;

procedure TfrmSplitOptions.setText(const Value: string);
begin
    edtSourceSub.Text:=Value;
end;

function TfrmSplitOptions.GetSplitedSub: TList<string>;
var
    list:TList<string>;
begin
    list:=TList<string>.Create;
    with mem1 do begin
        First;
        while not Eof do begin
            list.Add(FieldByName(contentFieldName).AsString);
            Next;
        end;
    end;
    if edtSourceSub.Text<>'' then list.Add(edtSourceSub.Text);

    Exit(list);
end;

procedure TfrmSplitOptions.btnOKClick(Sender: TObject);
begin
    self.ModalResult:=mrOk;
    Self.Hide;
end;

procedure TfrmSplitOptions.btnCancelClick(Sender: TObject);
begin
    Self.ModalResult:=mrCancel;
    Self.Hide;
end;

end.
