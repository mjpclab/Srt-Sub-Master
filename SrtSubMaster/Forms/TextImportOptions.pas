unit TextImportOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2007Black, Menus, StdCtrls,
  cxButtons, cxGroupBox, cxTextEdit, cxRadioGroup, cxMemo;

type
  TfrmTextImportOptions = class(TForm)
    grp1: TcxGroupBox;
    grp2: TcxGroupBox;
    grp3: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    edtImportText: TcxMemo;
    btnLoadFromFile: TcxButton;
    optSingleLine: TcxRadioButton;
    optEmptyLineAsSeparator: TcxRadioButton;
    optCustomSeparator: TcxRadioButton;
    edtCustomSeparator: TcxMemo;
    dlgOpenFile: TOpenDialog;
    procedure optSeparatorClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnLoadFromFileClick(Sender: TObject);
  private
    function getSeparator: string;
    function getText: string;
    { Private declarations }
  public
    { Public declarations }
    property Text:string read getText;
    property Separator:string read getSeparator;
  end;

implementation

{$R *.dfm}

function TfrmTextImportOptions.getText: string;
begin
    Exit(edtImportText.Text);
end;

function TfrmTextImportOptions.getSeparator: string;
const
    newLine:string=#13#10;
begin
    if optSingleLine.Checked then begin
        Exit(newLine);
    end else if optEmptyLineAsSeparator.Checked then begin
        Exit(newLine+newLine);
    end else if optCustomSeparator.Checked then begin
        Exit(edtCustomSeparator.Text);
    end;
end;

procedure TfrmTextImportOptions.optSeparatorClick(Sender: TObject);
begin
    edtCustomSeparator.Enabled:=optCustomSeparator.Checked;
end;

procedure TfrmTextImportOptions.btnLoadFromFileClick(Sender: TObject);
begin
    dlgOpenFile.FileName:=ExtractFileName(dlgOpenFile.FileName);
    if dlgOpenFile.Execute then begin
        edtImportText.Lines.LoadFromFile(dlgOpenFile.FileName);
    end;
end;

procedure TfrmTextImportOptions.btnOKClick(Sender: TObject);
var
    buffer:string;
    currentLine:string;
    emptyFound:boolean;
begin
    //如果按空行分割，将多个空行合并为一个
    if optEmptyLineAsSeparator.Checked then begin
        emptyFound:=false;
        for currentLine in edtImportText.Lines do begin
            if (currentLine<>'') or (emptyFound=false) then begin
                if buffer<>'' then buffer:=buffer + edtImportText.Lines.LineBreak;
                buffer:=buffer + currentLine;
            end;

            emptyFound:=(currentLine='');
        end;

        edtImportText.Text:=buffer;
    end;
end;

end.
