unit ExportOptions;

interface

uses
  CommonDefs,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,dxRibbonForm, dxSkinsCore, dxSkinBlue, dxSkinCaramel,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, cxLookAndFeelPainters, Menus,
  StdCtrls, cxButtons, cxGroupBox, cxRadioGroup, jpeg, cxControls, cxContainer,
  cxEdit, cxImage, cxTextEdit, cxGraphics, cxLookAndFeels;

type
  TfrmExportOptions = class(TdxRibbonForm)
    grpScope: TcxRadioGroup;
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    edtFileName: TcxTextEdit;
    btnBrowse: TcxButton;
    procedure btnBrowseClick(Sender: TObject);
  private
    FExportDialog:TSaveDialog;

    function getExportScope: ActionScope;
    function getFileName: string;
    { Private declarations }
  public
    property ExportDialog:TSaveDialog read FExportDialog write FExportDialog;
    property Scope:ActionScope read getExportScope;
    property FileName:string read getFileName;
  end;

implementation

{$R *.dfm}

procedure TfrmExportOptions.btnBrowseClick(Sender: TObject);
begin
    if ExportDialog.Execute then edtFileName.Text:=ExportDialog.FileName;
end;

function TfrmExportOptions.getExportScope: ActionScope;
begin
    case grpScope.ItemIndex of
        0: exit(asSelected);
        1: exit(asEntire);
        2: exit(asFirstToCurrent);
        3: Exit(asCurrentToLast);
        else raise Exception.Create('Unexpected Scope');
    end;
end;

function TfrmExportOptions.getFileName: string;
begin
    exit(edtFileName.Text);
end;

end.
